def test_set_person(self):
        self.client.identify('bob')
        response = self.client.set({'cool': 1}, path='get', resp=True)
        data = json.loads(response.data.decode())
        assert data['args']['_p'] == 'bob'
