@patch('stackify.transport.default.DefaultTransport.create_message')
    @patch('stackify.transport.default.DefaultTransport.create_group_message')
    @patch('stackify.transport.default.http.HTTPClient.POST')
    def test_send_group_if_needed(self, post, logmsggroup, logmsg):
        '''The listener sends groups of messages'''
        listener = StackifyListener(queue_=Mock(), max_batch=3, config=self.config)
        listener.transport._transport.identified = True

        listener.handle(1)
        self.assertFalse(post.called)
        listener.handle(2)
        self.assertFalse(post.called)
        self.assertEqual(len(listener.messages), 2)
        listener.handle(3)
        self.assertTrue(post.called)
        self.assertEqual(len(listener.messages), 0)
        listener.handle(4)
        self.assertEqual(post.call_count, 1)
        self.assertEqual(len(listener.messages), 1)
