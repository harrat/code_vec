def test_seafloor_age():
    "Sanity checks for the data"
    grid = fetch_seafloor_age()
    assert set(grid.data_vars) == {"age", "uncertainty"}
    assert tuple(grid.dims) == ("latitude", "longitude")
    assert grid.age.shape == (1801, 3601)
    assert grid.uncertainty.shape == (1801, 3601)
    npt.assert_allclose([grid.age.min(), grid.age.max()], [0, 280])
    npt.assert_allclose([grid.uncertainty.min(), grid.uncertainty.max()], [0, 15])

