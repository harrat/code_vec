def test_show_local_alias(cli, config):
    config.set('local_aliases', '__pingpong', '')
    output = cli('show', ['__pingpong'])
    assert output == "Your search string __pingpong is a local alias.\n"

