def test_observable_object_no_type(self):
        observed_data = copy.deepcopy(self.valid_observed_data)
        del observed_data['objects']['0']['type']
        self.assertFalseWithOptions(observed_data)
