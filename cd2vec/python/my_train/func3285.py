def test_rotate(self):
        ways_to_rotate_around_y_a_quarter_turn = [
            np.array([[0, 0, 1], [0, 1, 0], [-1, 0, 0]]),
            np.array([0, np.pi/2, 0]),
            np.array([[0, np.pi/2, 0]]),
            np.array([[0], [np.pi/2], [0]]),
            [0, np.pi/2, 0],
        ]
        for rot in ways_to_rotate_around_y_a_quarter_turn:
            transform = CompositeTransform()
            transform.rotate(rot)
            cube_v = self.cube_v.copy()

            # Sanity checking.
            np.testing.assert_array_equal(cube_v[0], [1., 0., 0.])
            np.testing.assert_array_equal(cube_v[6], [5., 4., 4.])

            cube_v = transform(cube_v)

            np.testing.assert_array_almost_equal(cube_v[0], [0., 0., -1.])
            np.testing.assert_array_almost_equal(cube_v[6], [4, 4., -5.])
