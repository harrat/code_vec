@pytest.mark.vcr()
def test_workbench_vulns_authenticated_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vulns(authenticated='nope')
