def test_preamble_meta_no_dataset(self):
        """Test reading only preamble and meta elements"""
        preamble = b"\x00" * 128
        prefix = b"DICM"
        meta = (
            b"\x02\x00\x00\x00\x55\x4C\x04\x00\x0A\x00\x00\x00"
            b"\x02\x00\x02\x00\x55\x49\x16\x00\x31\x2e\x32\x2e"
            b"\x38\x34\x30\x2e\x31\x30\x30\x30\x38\x2e\x35\x2e"
            b"\x31\x2e\x31\x2e\x39\x00\x02\x00\x10\x00\x55\x49"
            b"\x12\x00\x31\x2e\x32\x2e\x38\x34\x30\x2e\x31\x30"
            b"\x30\x30\x38\x2e\x31\x2e\x32\x00"
        )

        bytestream = preamble + prefix + meta
        fp = BytesIO(bytestream)
        ds = dcmread(fp, force=True)
        assert b"\x00" * 128 == ds.preamble
        assert "TransferSyntaxUID" in ds.file_meta
        assert Dataset() == ds[:]
