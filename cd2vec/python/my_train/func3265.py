@pytest.mark.high
    def test_to_yaml(self):
        expected_result = yaml.safe_dump({
            'consumers': {
                'consumer_name': {
                    'description': 'docstring',
                    'module': 'module',
                    'name': 'qualname',
                },
            },
            'producers': {
                'producer_name': {
                    'description': 'description',
                    'module': 'module',
                    'name': 'qualname',
                }
            }
        }, default_flow_style=False)

        self.register.register(self.tasks['consumer'])
        self.register.register(self.tasks['producer'])

        result = self.register.to_yaml()

        self.assertEqual(expected_result, result)
