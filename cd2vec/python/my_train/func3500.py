@staticmethod
    @pytest.mark.usefixtures('json', 'default-snippets', 'export-time')
    def test_cli_export_snippet_020(snippy):
        """Export defined snippet with content data.

        Export defined snippet based on content data. File name is defined in
        command line as json file.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Snippet.REMOVE
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-c', 'docker rm --volumes $(docker ps --all --quiet)', '-f', 'defined-snippet.json'])  # pylint: disable=line-too-long
            assert cause == Cause.ALL_OK
            Content.assert_json(json, mock_file, 'defined-snippet.json', content)
