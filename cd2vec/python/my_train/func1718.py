def test_command_add(self, centreon_con):
        values = [
            'command_test',
            'check',
            '/my/plugins my command'
        ]
        data = {}
        data['action'] = 'add'
        data['object'] = 'CMD'
        data['values'] = values

        with patch('requests.post') as patched_post:
            centreon_con.commands.add("command_test",
                                          "check",
                                          "/my/plugins my command",
                                          post_refresh=False
                                          )
            patched_post.assert_called_with(self.clapi_url, headers=self.headers, data=json.dumps(data), verify=True)
