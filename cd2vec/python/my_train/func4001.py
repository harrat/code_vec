@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_persistence_assumptions(fixture, request):
    # Object references are not persisted across function calls by default.
    # This is the control experiment prior to the following tests
    testobj = MyRefObj()
    testobj.o = MyRefObj()
    testobj.o.o = testobj

    engine = request.getfixturevalue(fixture)
    pycall = pyccc.PythonCall(testobj.identity)

    # First the control experiment - references are NOT persisted
    job = engine.launch(PYIMAGE, pycall, interpreter=PYVERSION)
    print(job.rundata)
    job.wait()
    result = job.result
    assert result is not testobj
    assert result.o is not testobj.o
    assert result.o.o is result

