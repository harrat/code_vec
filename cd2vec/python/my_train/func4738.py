def test_openapi_schema(api):
    with open(os.path.join(os.path.dirname(__file__), "assets", "schema.yml"), "r") as schema_yml:
        expected = schema_yml.read()
    r = api.session().get("http://;/schema.yml")
    actual = r.text

    assert actual == expected

