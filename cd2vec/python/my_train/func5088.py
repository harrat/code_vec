def test_push_empty(saga_folder):
    run_cmd("saga remote https://sagalab.org")
    # TODO: add fake credentials not on git
    out = run_cmd_with_login("saga push", "narush", "123")
    assert f"Pushed {Path(saga_folder).name} to https://sagalab.org" in out

def test_push_and_clone_empty(tmpdir, saga_folder):
    run_cmd("saga remote https://sagalab.org")
    # TODO: add fake credentials not on git
    out = run_cmd_with_login("saga push", "narush", "123")
    os.chdir(tmpdir)
    out = run_cmd(f"saga clone https://sagalab.org/{Path(saga_folder).name}.saga")
    assert f"Cloned https://sagalab.org/{Path(saga_folder).name}.saga" in out
    assert Path(Path(saga_folder).name).exists()
