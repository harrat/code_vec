def test_discover_versions(self):
        """
        Test that a DiscoverVersions request can be processed correctly for
        different inputs.
        """
        e = engine.KmipEngine()

        # Test default request.
        e._logger = mock.MagicMock()
        payload = payloads.DiscoverVersionsRequestPayload()

        result = e._process_discover_versions(payload)

        e._logger.info.assert_called_once_with(
            "Processing operation: DiscoverVersions"
        )
        self.assertIsInstance(
            result,
            payloads.DiscoverVersionsResponsePayload
        )
        self.assertIsNotNone(result.protocol_versions)
        self.assertEqual(6, len(result.protocol_versions))
        self.assertEqual(
            contents.ProtocolVersion(2, 0),
            result.protocol_versions[0]
        )
        self.assertEqual(
            contents.ProtocolVersion(1, 4),
            result.protocol_versions[1]
        )
        self.assertEqual(
            contents.ProtocolVersion(1, 3),
            result.protocol_versions[2]
        )
        self.assertEqual(
            contents.ProtocolVersion(1, 2),
            result.protocol_versions[3]
        )
        self.assertEqual(
            contents.ProtocolVersion(1, 1),
            result.protocol_versions[4]
        )
        self.assertEqual(
            contents.ProtocolVersion(1, 0),
            result.protocol_versions[5]
        )

        # Test detailed request.
        e._logger = mock.MagicMock()
        payload = payloads.DiscoverVersionsRequestPayload([
            contents.ProtocolVersion(1, 0)
        ])

        result = e._process_discover_versions(payload)

        e._logger.info.assert_called_once_with(
            "Processing operation: DiscoverVersions"
        )
        self.assertIsNotNone(result.protocol_versions)
        self.assertEqual(1, len(result.protocol_versions))
        self.assertEqual(
            contents.ProtocolVersion(1, 0),
            result.protocol_versions[0]
        )

        # Test disjoint request.
        e._logger = mock.MagicMock()
        payload = payloads.DiscoverVersionsRequestPayload([
            contents.ProtocolVersion(0, 1)
        ])

        result = e._process_discover_versions(payload)

        e._logger.info.assert_called_once_with(
            "Processing operation: DiscoverVersions"
        )
        self.assertEqual([], result.protocol_versions)
