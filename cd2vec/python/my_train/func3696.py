def test_masked_middle_length_no_explicit_run_vad(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        self.assertNotEqual(audiofile.masked_middle_length, 0)
