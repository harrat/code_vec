def test_run_venv(d):
    PATH = pjoin(d, 'test-venv')
    os.mkdir(PATH)
    os.mkdir(pjoin(PATH, 'bin'))

    def test_exec(_file, _args):
        assert pjoin(PATH, 'bin') in os.environ['PATH'].split(':')
    kern = install(d, "conda %s"%PATH)
    run(d, kern, test_exec)

# Requires lmod installed... and a module to exist
#def test_run_lmod(d):
