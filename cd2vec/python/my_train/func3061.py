def test_unsigned_legacy_tx(self):
        outpoint = simple.outpoint(
            tx_id=helpers.P2PKH['human']['ins'][0]['hash'],
            index=helpers.P2PKH['human']['ins'][0]['index'])
        tx_in = simple.unsigned_input(
            outpoint=outpoint,
            sequence=helpers.P2PKH['human']['ins'][0]['sequence'])
        tx_out = simple.output(
            helpers.P2PKH['human']['outs'][0]['value'],
            helpers.P2PKH['human']['outs'][0]['addr'])
        tx_return_output = txn.make_op_return_output(
            helpers.P2PKH['human']['outs'][1]['memo'])
        tx = simple.unsigned_legacy_tx(
            tx_ins=[tx_in],
            tx_outs=[tx_out, tx_return_output])

        self.assertEqual(tx, helpers.P2PKH['ser']['tx']['unsigned'])
