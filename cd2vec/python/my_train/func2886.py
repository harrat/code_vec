@patch('virga.providers.abstract.AbstractProvider._lookup')
    def test_assertion_call_lookup(self, mock_lookup):
        mock_lookup.return_value = "AnyKey=='any-value'"
        self.provider.assertion("AnyKey=='any-value'", 'Context', {}, 'resource-id')
        mock_lookup.assert_called_once_with("AnyKey=='any-value'")
