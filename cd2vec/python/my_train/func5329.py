def test_file_data_interactive():
    """Verify the data written via mock std in"""
    filename = 'wso_interactive.json'
    assert AUTH.check_file_exists(filename) is True

    assert AUTH.verify_config(filename, 'authorization',
                              AUTH.encode(RANDOM_USERNAME,
                                          RANDOM_PASSWORD)) is True
    assert AUTH.verify_config(filename, 'url', RANDOM_URL) is True
    assert AUTH.verify_config(filename, 'aw-tenant-code',
                              RANDOM_TENANTCODE) is True
