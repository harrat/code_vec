def test_simple_forward(tmpdir):
    conf_filename = str(tmpdir.join("conf.yaml"))
    pull_url = tmpdir.join("input.socket")
    push_url = tmpdir.join("output.socket")
    db_url = tmpdir.join("db.sqlite3")
    inbound = tmpdir.join("inbound")
    outbound = tmpdir.join("outbound")
    stdout = tmpdir.join("stdout")
    stderr = tmpdir.join("stderr")
    with open(conf_filename, "w") as f:
        f.write("inputs:\n")
        f.write("- class: ZMQPull\n")
        f.write("  name: testing-input\n")
        f.write("  options:\n")
        f.write("    url: ipc://%s\n" % pull_url)
        f.write("db:\n")
        f.write("  url: sqlite:///%s\n" % db_url)
        f.write("core:\n")
        f.write("  inbound: ipc://%s\n" % inbound)
        f.write("  outbound: ipc://%s\n" % outbound)
        f.write("outputs:\n")
        f.write("- class: ZMQPush\n")
        f.write("  name: testing-out\n")
        f.write("  options:\n")
        f.write("    url: ipc://%s\n" % push_url)
    args = [
        "python3",
        "-m",
        "reactobus",
        "--conf",
        conf_filename,
        "--level",
        "DEBUG",
        "--log-file",
        "-",
    ]
    proc = subprocess.Popen(
        args, stdout=open(str(stdout), "w"), stderr=open(str(stderr), "w")
    )

    ctx = zmq.Context.instance()
    in_sock = ctx.socket(zmq.PUSH)
    in_sock.connect("ipc://%s" % pull_url)
    out_sock = ctx.socket(zmq.PULL)
    out_sock.bind("ipc://%s" % push_url)

    # Allow the process sometime to setup and connect
    time.sleep(1)

    # Send many messages
    for i in range(0, 1000):
        data = [
            b"org.reactobus.test.py",
            b(str(uuid.uuid1())),
            b(datetime.datetime.utcnow().isoformat()),
            b"py.test",
            b(json.dumps({"pipeline": True})),
        ]
        in_sock.send_multipart(data)
        assert out_sock.recv_multipart() == data

    time.sleep(3)

    proc.terminate()
    proc.wait()

    # Check the database
    Base = declarative_base()
    engine = create_engine("sqlite:///%s" % db_url)
    Base.metadata.create_all(engine)
    sessions = sessionmaker(bind=engine)

    session = sessions()
    assert session.query(Message).count() == 1000
    assert (
        session.query(Message).filter_by(topic="org.reactobus.test.py").count() == 1000
    )
