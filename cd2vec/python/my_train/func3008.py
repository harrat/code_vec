def test_pheno_objects():
    results = search_associations(subject=TWIST_ZFIN,
                                  fetch_objects=True,
                                  rows=0,
                                  object_category='phenotype'
    )
    objs = results['objects']
    print(str(objs))
    assert len(objs) > 1
    assert 'ZP:0007631' in objs
    
def test_func_objects():
    results = search_associations(subject=TWIST_ZFIN,
                                  fetch_objects=True,
                                  rows=0,
                                  object_category='function'
    )
    objs = results['objects']
    print(objs)
    assert DVPF in objs
    assert len(objs) > 1
