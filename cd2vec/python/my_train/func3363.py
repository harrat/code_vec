def test_get_relevant_policy_section_groups_not_supported(self):
        """
        Test that the lookup for a group-less policy with a group specified is
        handled correctly.
        """
        e = engine.KmipEngine()
        e._logger = mock.MagicMock()
        e._operation_policies = {
            'test_policy': {
                'preset': {
                    enums.ObjectType.SYMMETRIC_KEY: {
                        enums.Operation.GET: enums.Policy.ALLOW_OWNER
                    }
                }
            }
        }

        result = e.get_relevant_policy_section('test_policy', 'test_group_A')

        e._logger.debug.assert_called_once_with(
            "The 'test_policy' policy does not support groups."
        )
        self.assertIsNone(result)
