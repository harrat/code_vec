@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_python_instance_method(fixture, request):
    engine = request.getfixturevalue(fixture)
    obj = function_tests.Cls()
    pycall = pyccc.PythonCall(obj.increment, by=2)
    job = engine.launch(PYIMAGE, pycall, interpreter=PYVERSION)
    print(job.rundata)
    job.wait()

    assert job.result == 2
    assert job.updated_object.x == 2

