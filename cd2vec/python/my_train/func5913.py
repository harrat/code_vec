def test_pack_unpack_auth(alice, bob):
    """ Test the pack-unpack loop with authcrypt. """
    msg = Message({'@type': 'doc;protocol/1.0/name'})
    packed_msg = alice.pack(msg)
    assert isinstance(packed_msg, bytes)

    unpacked_msg = bob.unpack(packed_msg)
    assert isinstance(unpacked_msg, Message)
    assert hasattr(unpacked_msg, 'mtc')
    assert unpacked_msg.mtc.is_authcrypted()
    assert unpacked_msg.mtc.sender == alice.verkey_b58
    assert unpacked_msg.mtc.recipient == bob.verkey_b58

