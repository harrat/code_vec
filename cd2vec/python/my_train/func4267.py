def test_set_contacts(self):
        result = api.set_contacts(sample1.domainName, sample1.contacts)

        domain = result.domain
        self.assertDictEqual(domain.contacts.to_dict(), sample1.contacts.to_dict())
