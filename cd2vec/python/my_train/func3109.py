def test_get_metas_with_callback_collect_ids(self, dcard):
        forum = dcard.forums(_forum_name)

        def collect_ids(metas):
            return [meta['id'] for meta in metas]

        ids = forum(_forum_name).get_metas(callback=collect_ids)
        assert len(ids) == 30
        assert all([isinstance(x, int) for x in ids])
