@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_reference_022(snippy):
        """Try to import references wihtout UUID.

        Try to import two references without UUID. When the UUID is missing, it
        must be automatically allocated
        """

        content = {
            'data': [
                Content.deepcopy(Reference.GITLOG),
                Content.deepcopy(Reference.REGEXP)
            ]
        }
        content['data'][0]['uuid'] = ''
        content['data'][1]['uuid'] = ''
        file_content = Content.get_file_content(Content.MKDN, content)
        content['data'][0]['uuid'] = 'a1cd5827-b6ef-4067-b5ac-3ceac07dde9f'
        content['data'][1]['uuid'] = 'a2cd5827-b6ef-4067-b5ac-3ceac07dde9f'
        with mock.patch('snippy.content.migrate.io.open', file_content, create=True) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'reference'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './references.mkdn', mode='r', encoding='utf-8')
