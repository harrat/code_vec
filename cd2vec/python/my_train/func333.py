def test_hybrid_udf_get_from_iso_zero_udf_file_entry(tmpdir):
    indir = tmpdir.mkdir('udfzerofileentry')
    outfile = str(indir)+'.iso'

    with open(os.path.join(str(indir), 'foo'), 'wb') as outfp:
        outfp.write(b'foo\n')

    subprocess.call(['genisoimage', '-v', '-v', '-no-pad', '-iso-level', '3',
                     '-udf', '-o', str(outfile), str(indir)])

    # Now open up the ISO and zero out the UDF File Entry
    with open(str(outfile), 'r+b') as fp:
        fp.seek(261*2048)
        fp.write(b'\x00'*2048)

    iso = pycdlib.PyCdlib()

    iso.open(str(outfile))

    out = BytesIO()
    with pytest.raises(pycdlib.pycdlibexception.PyCdlibInvalidInput) as excinfo:
        iso.get_file_from_iso_fp(out, udf_path='/foo')
    assert(str(excinfo.value) == 'Cannot get the contents of an empty UDF File Entry')

    iso.close()

def test_hybrid_boot_record_retain_system_use(tmpdir):
    indir = tmpdir.mkdir('bootrecordretainsystemuse')
    outfile = str(indir)+'.iso'
