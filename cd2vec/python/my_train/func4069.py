def test_server_side_etag(self):
        fetcher = ConfigFetcher(sdk_key='PKDVCLf-Hq-h-kCzMp-L7Q/HhOWfwVtZ0mb30i9wi17GQ',
                                mode='m', base_url='https://cdn-li-fra-1.configcat.com')
        fetch_response = fetcher.get_configuration_json()
        self.assertTrue(fetch_response.is_fetched())
        self.assertFalse(fetch_response.is_not_modified())

        fetch_response = fetcher.get_configuration_json()
        self.assertFalse(fetch_response.is_fetched())
        self.assertTrue(fetch_response.is_not_modified())

        fetch_response = fetcher.get_configuration_json(force_fetch=True)
        self.assertTrue(fetch_response.is_fetched())
        self.assertFalse(fetch_response.is_not_modified())
