def test_ufactor_material(self):
        self.idf.initreadtxt(single_layer)
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        expected = 1 / (m.Thickness / m.Conductivity)
        assert m.ufactor == expected
        assert m.ufactor == 1 / 0.2
