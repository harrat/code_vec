def test_trace_return_values(trace_return_val):
    tracer = ThundraTracer.get_instance()
    nodes = tracer.get_spans()
    count = 0
    for key in nodes:
        if key.operation_name == 'func_return_val':
            count += 1

    assert count == 0

    traceable_trace_return_val, func_return_val = trace_return_val
    response = func_return_val()

    active_span = None
    nodes = tracer.get_spans()
    for key in nodes:
        if key.operation_name == 'func_return_val':
            count += 1
            active_span = key

    args = active_span.get_tag('method.args')
    assert args is None

    return_value = active_span.get_tag('method.return_value')
    assert return_value['type'] == type(response).__name__
    assert return_value['value'] == response

    error = active_span.get_tag('error')
    assert error is None

    assert count == 1
    assert traceable_trace_return_val.trace_args is False
    assert traceable_trace_return_val.trace_return_value is True
    assert traceable_trace_return_val.trace_error is True

