def test_get_url_match_header_overrides(self):
        self.client.set_header_overrides([['host1', {
            'User-Agent': 'Test_User_Agent_String'
        }], ['host2', {'User-Agent2': 'Test_User_Agent_String'
                       }]])

        self.assertEqual([
            ['host1', {'User-Agent': 'Test_User_Agent_String'}],
            ['host2', {'User-Agent2': 'Test_User_Agent_String'}]
        ], self.client.get_header_overrides())
