def test_read_OD_explicit_little(self):
        """Check creation of OD DataElement from byte data works correctly."""
        ds = dcmread(self.fp_ex, force=True)
        ref_elem = ds.get(0x7FE00009)
        elem = DataElement(
            0x7FE00009,
            "OD",
            b"\x00\x01\x02\x03\x04\x05\x06\x07"
            b"\x01\x01\x02\x03\x04\x05\x06\x07",
        )
        assert ref_elem == elem
