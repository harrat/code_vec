def test_map_resp_to_metadata(talker, metron_response):
    meta_data = talker.map_metron_data_to_metadata(metron_response)
    assert meta_data is not None
    assert meta_data.title == metron_response["name"][0]
    assert meta_data.story_arc == metron_response["arcs"][0]["name"]
    assert meta_data.series == metron_response["series"]["name"]
    assert meta_data.volume == metron_response["volume"]
    assert meta_data.publisher == metron_response["publisher"]["name"]
    assert meta_data.issue == metron_response["number"]
    assert meta_data.teams == metron_response["teams"][0]["name"]
    assert meta_data.year == "1994"

