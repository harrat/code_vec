def test_process_close_hung(self):
        p = Process(('sleep', '5'))
        with self.assertRaises(Exception):
            p.close1(timeout=1, terminate=False)
        p = Process(('sleep', '5'))
        p.close1(timeout=1, terminate=True)
        self.assertTrue(p.closed)
