def test_remove_trusted_beneficiary(self):
        self.member2.add_trusted_beneficiary(self.member1.member_id)
        self.member2.add_trusted_beneficiary(self.member3.member_id)
        beneficiaries = self.member2.get_trusted_beneficiaries()
        beneficiary_id = [beneficiary.payload.member_id for beneficiary in beneficiaries]
        assert [self.member1.member_id, self.member3.member_id] == beneficiary_id

        self.member2.remove_trusted_beneficiary(self.member3.member_id)
        beneficiaries = self.member2.get_trusted_beneficiaries()
        beneficiary_id = [beneficiary.payload.member_id for beneficiary in beneficiaries]
        assert [self.member1.member_id] == beneficiary_id

        self.member2.remove_trusted_beneficiary(self.member1.member_id)
        beneficiaries = self.member2.get_trusted_beneficiaries()
        assert len(beneficiaries) == 0
