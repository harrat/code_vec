def test_to_dict_from_dict(self):
        oldseed = agents.seed
        agents.seed = 123
        p1 = PpoAgent(gym_env_name=_line_world_name, fc_layers=(10, 20, 30), backend='tfagents')
        d = p1._to_dict()
        agents.seed = oldseed
        p2: EasyAgent = EasyAgent._from_dict(d)
        self.assert_are_equal(p1, p2)
