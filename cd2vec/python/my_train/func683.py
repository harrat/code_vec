def test_return_num_handlers(self):
        ''' Returned number of handlers. '''
        hdls = barchart.draw(self.axes, _data())
        self.assertEqual(len(hdls), len(_data()[0]))
