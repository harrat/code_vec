@pytest.mark.high
    def test_register_producer(self):
        self.register.register(self.tasks['producer'])

        self.assertIn(self.tasks['producer'], self.register.producers.values())
