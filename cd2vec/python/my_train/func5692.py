def test_port_functions():
    port = utils.get_available_port()
    assert not utils.is_port_in_use(port)
    sock = socket.socket()
    sock.bind(('', port))
    sock.listen(1)
    assert utils.is_port_in_use(port)
    sock.close()
    assert not utils.is_port_in_use(port)

