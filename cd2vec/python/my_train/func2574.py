def test_disease_assocs():
    payload = search_associations(subject=HUMAN_SHH,
                                  object_category='disease'
    )
    print(str(payload))
    assocs = payload['associations']
    assert len(assocs) > 0

def test_disease2gene():
    payload = search_associations(subject=LSD,
                                  subject_category='disease',
                                  object_category='gene')
    assocs = payload['associations']
    for a in assocs:
        print(str(a))
    assert len(assocs) > 0
