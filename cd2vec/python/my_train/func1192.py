@mock.patch('kmip.services.server.auth.get_certificate_from_connection')
    @mock.patch('kmip.core.messages.messages.RequestMessage')
    def test_handle_message_loop_with_authentication_failure(self,
                                                             request_mock,
                                                             cert_mock):
        """
        Test that the correct logging and error handling occurs when an
        authentication error is generated while processing a request.
        """
        data = utils.BytearrayStream(())

        cert_mock.return_value = 'test_certificate'
        kmip_engine = engine.KmipEngine()
        kmip_engine._logger = mock.MagicMock()
        kmip_session = session.KmipSession(
            kmip_engine,
            None,
            None,
            name='name',
            enable_tls_client_auth=False
        )
        kmip_session.authenticate = mock.MagicMock()
        kmip_session.authenticate.side_effect = exceptions.PermissionDenied(
            "Authentication failed."
        )
        kmip_session._engine = mock.MagicMock()
        kmip_session._engine.default_protocol_version = \
            kmip_engine.default_protocol_version
        kmip_session._logger = mock.MagicMock()
        kmip_session._connection = mock.MagicMock()
        kmip_session._receive_request = mock.MagicMock(return_value=data)
        kmip_session._send_response = mock.MagicMock()
        fake_version = contents.ProtocolVersion(1, 2)
        fake_credential = objects.Credential(
            credential_type=enums.CredentialType.USERNAME_AND_PASSWORD,
            credential_value=objects.UsernamePasswordCredential(
                username="John Doe",
                password="secret"
            )
        )
        fake_header = messages.RequestHeader(
            protocol_version=fake_version,
            authentication=contents.Authentication(
                credentials=[fake_credential]
            )
        )
        fake_request = messages.RequestMessage()
        fake_request.request_header = fake_header
        fake_request.read = mock.MagicMock()
        request_mock.return_value = fake_request

        kmip_session._handle_message_loop()

        kmip_session._receive_request.assert_called_once_with()
        fake_request.read.assert_called_once_with(
            data,
            kmip_version=enums.KMIPVersion.KMIP_1_2
        )
        kmip_session.authenticate.assert_called_once_with(
            "test_certificate",
            fake_request
        )
        kmip_session._logger.warning.assert_called_once_with(
            "Authentication failed."
        )
        kmip_session._engine.build_error_response.assert_called_once_with(
            fake_version,
            enums.ResultReason.AUTHENTICATION_NOT_SUCCESSFUL,
            "An error occurred during client authentication. "
            "See server logs for more information."
        )
        kmip_session._logger.exception.assert_not_called()
        self.assertTrue(kmip_session._send_response.called)
