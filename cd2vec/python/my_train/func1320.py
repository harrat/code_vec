def test_ttl_gt0(tmpdir):
    storage = SQLiteStorage(
        filepath=f'{tmpdir}/cache',
        ttl=0.001,
        maxsize=100,
    )
    storage[b'1'] = b'one'
    time.sleep(0.0011)
    assert storage.get(b'1') is None

    storage = SQLiteStorage(
        filepath=f'{tmpdir}/cache',
        ttl=99,
        maxsize=100,
    )
    storage[b'x'] = b'x'
    assert storage[b'x'] == b'x'

