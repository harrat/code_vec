def test_02_read_w_rw_flags(self):
        with AIOContext(1) as ctx, open(self._TEST_FILE_NAME) as fp:
            block = ReadBlock(fp, bytearray(64))
            block.rw_flag |= IOCBRWFlag.HIPRI
