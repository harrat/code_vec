def test_working_dir():
    working_dir = sys.exec_prefix.strip(os.pathsep)
    p = Process(
        sys.executable,
        ["-c", """import os; print(os.getcwd())"""],
        working_dir=working_dir,
    )
    p.start()
    p.wait()
    assert p.read().decode("ascii").strip() == working_dir

