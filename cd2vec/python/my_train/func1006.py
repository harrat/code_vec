def test_4_is_locked_by_another_user(self):
        assert is_locked_by_another_user('a_non_existent_file', this_user='not_test_dummy') is False
        assert is_locked_by_another_user(TEST_FILE_PATH, this_user='not_test_dummy')
        assert is_locked_by_another_user(TEST_FILE_PATH, this_user='test_dummy') is False
