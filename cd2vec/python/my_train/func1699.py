@pytest.mark.vcr()
def test_workbench_vulns_filter_type_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.vulns(filter_type='NOT')
