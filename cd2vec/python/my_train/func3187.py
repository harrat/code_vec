def test_cli_read_settings_corrupted(monkeypatch):
    # run cli with corrupt settings file
    mock_get_settings = Mock(side_effect=YeahYeahPersistenceException(
        "Could not read settings. These settings are weird"))
    monkeypatch.setattr("yeahyeah.core.YeahYeah.get_settings",  mock_get_settings)
    runner = CliRunner()
    yeahyeah_lib = importlib.import_module('yeahyeah.cli')
    result = runner.invoke(yeahyeah_lib.yeahyeah, catch_exceptions=False)
    assert result.exit_code == 0
#    assert 'Please check' in result.output
