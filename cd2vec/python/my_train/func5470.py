def test_loads_each_split(self):
        train = Wmt14(split='train')
        self.assertEqual(len(train), 4_500_966)
        dev = Wmt14(split='dev')
        self.assertEqual(len(dev), 3_000)
        test = Wmt14(split='test')
        self.assertEqual(len(test), 3_003)
