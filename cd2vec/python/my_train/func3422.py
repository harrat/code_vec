@pytest.mark.parametrize('kernels', list_to_test)
@pytest.mark.parametrize('diff_cutoff', multi_cut)
def test_force_en(kernels, diff_cutoff):
    """Check that the analytical force/en kernel matches finite difference of
    energy kernel."""

    delta = 1e-5
    d1 = 1
    d2 = 2
    cell = 1e7 * np.eye(3)
    np.random.seed(0)

    cutoffs, hyps, hm = generate_diff_hm(kernels, diff_cutoff)
    args = from_mask_to_args(hyps, cutoffs, hm)

    env1 = generate_mb_envs(cutoffs, cell, delta, d1, hm)
    env2 = generate_mb_envs(cutoffs, cell, delta, d2, hm)

    _, _, en_kernel, force_en_kernel, _, _, _ = \
        str_to_kernel_set(kernels, "mc", hm)

    kern_analytical = force_en_kernel(env1[0][0], env2[0][0], d1, *args)

    kern_finite_diff = 0
    if ('manybody' in kernels):
        kernel, _, enm_kernel, efk, _, _, _ = \
            str_to_kernel_set(['manybody'], "mc", hm)

        calc = 0
        for i in range(len(env1[0])):
            calc += enm_kernel(env1[1][i], env2[0][0], *args)
            calc -= enm_kernel(env1[2][i], env2[0][0], *args)

        kern_finite_diff += (calc)/(2*delta)

    if ('twobody' in kernels or 'threebody' in kernels):
        args23 = from_mask_to_args(hyps, cutoffs, hm)

    if ('twobody' in kernels):
        kernel, _, en2_kernel, efk, _, _, _ = \
            str_to_kernel_set(['2b'], 'mc', hm)
        calc1 = en2_kernel(env1[1][0], env2[0][0], *args23)
        calc2 = en2_kernel(env1[2][0], env2[0][0], *args23)
        diff2b = 4 * (calc1 - calc2) / 2.0 / delta / 2.0

        kern_finite_diff += diff2b

    if ('threebody' in kernels):
        kernel, _, en3_kernel, efk, _, _, _ = \
            str_to_kernel_set(['3b'], 'mc', hm)
        calc1 = en3_kernel(env1[1][0], env2[0][0], *args23)
        calc2 = en3_kernel(env1[2][0], env2[0][0], *args23)
        diff3b = 9 * (calc1 - calc2) / 3.0 / delta / 2.0

        kern_finite_diff += diff3b


    tol = 1e-3
    print("\nforce_en", kernels, kern_finite_diff, kern_analytical)
    assert (isclose(-kern_finite_diff, kern_analytical, rtol=tol))

