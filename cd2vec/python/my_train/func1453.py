def test_biz_factory_undefined(self):
        fact = BizFactory('sfn_name')
        fact.__sfn_client__ = Mock()
        fact.sfn_client.list_state_machines = \
            MagicMock(return_value={'stateMachines': [
                {'name': 'sfn_name1', 'stateMachineArn': 'arn1'},
                {'name': 'sfn_name2', 'stateMachineArn': 'arn2'}
            ], 'nextToken': None})

        with pytest.raises(Exception) as execinfo:
            fact({'detail-type': 'Scheduled Event',
                  'resources': ['arn:aws:events:eu-west-1:123456789:rule/sfn_name-every1']}, {})
        assert str(execinfo.value.args[0]) == 'BadRequestError: Undefined step function : sfn_name'

        fact = BizFactory('sfn_name')
        fact.__sfn_client__ = Mock()
        fact.sfn_client.list_state_machines = \
            MagicMock(return_value={'stateMachines': [
                {'name': 'sfn_name', 'stateMachineArn': 'arn1'},
                {'name': 'sfn_name2', 'stateMachineArn': 'arn2'}
            ], 'nextToken': None})
        with pytest.raises(Exception) as execinfo:
            fact({'detail-type': 'Scheduled Event',
                  'resources': ['arn:aws:events:eu-west-1:123456789:rule/sfn_name-every1']}, {})
        assert str(execinfo.value.args[0]) == 'BadRequestError: Unregistered biz : every1'

        fact.create('every', Every(5, Every.MINUTES), data={"key1": "value1"})
        with pytest.raises(Exception) as execinfo:
            fact({'detail-type': 'Scheduled Event',
                  'resources': ['arn:aws:events:eu-west-1:123456789:rule/sfn_name-every1']}, {})
        assert str(execinfo.value.args[0]) == 'BadRequestError: Unregistered biz : every1'
