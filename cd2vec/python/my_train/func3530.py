def test_getallobjlists():
    """py.test for getallobjlists"""
    tdata = (
        (
            "TransformerNames",
            [("ElectricLoadCenter:Distribution".upper(), "TransformerNames", [10])],
        ),  # refname, objlists
    )
    for refname, objlists in tdata:
        fhandle = StringIO("")
        idf = IDF(fhandle)
        result = modeleditor.getallobjlists(idf, refname)
        assert result == objlists

