def test_geo_search(self):
        """ Perform simple query """
        with open(os.path.join(self.path, 'aoi1.geojson')) as f:
            aoi = json.load(f)
        search = Search(datetime='2020-06-07', intersects=aoi['geometry'])
        assert(search.found() == 12)
        items = search.items()
        assert(len(items) == 12)
        assert(isinstance(items[0], Item))
