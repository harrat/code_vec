def test_get_set_permissions(self):
        path = self.root.make_file(permissions='rw')
        assert PermissionSet('rw') == get_permissions(path)
        set_permissions(path, 'wx')
        assert PermissionSet('wx') == get_permissions(path)
