@patch.object(pysyncdroid.sync.Sync, "copy_file")
    def test_do_sync(self, mock_copy_file):
        """
        Test 'do_sync' copies source files to their destination and updates
        destination files list.
        """
        sync = Sync(FAKE_MTP_DETAILS, "/tmp", "Card/Music")
        sync.set_source_abs()
        sync.set_destination_abs()
        sync.do_sync(FAKE_SYNC_DATA)

        self.assertEqual(
            FAKE_SYNC_DATA["dst_dir_fls"],
            [
                "/run/user/<user>/gvfs/mtp:host=%5Busb%3A002%2C003%5D/Card/Music/testdir/oldsong.mp3"
            ],  # noqa
        )

        mock_copy_file.assert_called_once_with(
            "/tmp/testdir/song.mp3",
            "/run/user/<user>/gvfs/mtp:host=%5Busb%3A002%2C003%5D/Card/Music/testdir/song.mp3",  # noqa
        )
