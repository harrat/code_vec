def test_xopen_invalid(self):
        # invalid mode
        with self.assertRaises(ValueError):
            xopen('foo', 'z')
        with self.assertRaises(ValueError):
            xopen('foo', 'rz')
        with self.assertRaises(ValueError):
            xopen('foo', 'rU', newline='\n')
        with self.assertRaises(ValueError):
            xopen(STDOUT, 'w', compression=True)
        with self.assertRaises(ValueError):
            xopen('foo.bar', 'w', compression=True)
        with self.assertRaises(ValueError):
            xopen('foo', file_type=FileType.STDIO)
        with self.assertRaises(ValueError):
            xopen(STDOUT, file_type=FileType.LOCAL)
        with self.assertRaises(ValueError):
            xopen('foo', file_type=FileType.URL)
        with self.assertRaises(IOError):
            xopen('http://foo.com', file_type=FileType.LOCAL)
        with self.assertRaises(ValueError):
            xopen('xyz', file_type=FileType.FILELIKE)
        path = self.root.make_file(contents='foo')
        with open(path, 'r') as fh:
            with self.assertRaises(ValueError):
                xopen(fh, 'w')
            f = xopen(fh, context_wrapper=True)
            assert 'r' == f.mode
        f = xopen(path, context_wrapper=True)
        f.close()
        with self.assertRaises(IOError):
            with f:
                pass
        with self.assertRaises(ValueError):
            with open(path, 'rt') as fh:
                xopen(fh, 'rt', compression=True)
        # can't guess compression without a name
        with self.assertRaises(ValueError):
            b = BytesIO()
            b.mode = 'wb'
            xopen(cast(IOBase, b), 'wt')
        # can't read from stderr
        with self.assertRaises(ValueError):
            xopen(STDERR, 'rt')
