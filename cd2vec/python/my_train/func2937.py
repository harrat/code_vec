def test_operational_error():
    assert issubclass(py2jdbc.DataError, py2jdbc.DatabaseError), \
        "DataError is not a subclass of DatabaseError"
