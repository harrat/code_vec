def test_getobject_use_prevfield():
    """py.test for getobject_use_prevfield"""
    idf = IDF(StringIO(""))
    branch = idf.newidfobject(
        "BRANCH",
        Name="CW Pump Branch",
        Component_1_Object_Type="Pump:VariableSpeed",
        Component_1_Name="CW Circ Pump",
    )
    pump = idf.newidfobject("PUMP:VARIABLESPEED", Name="CW Circ Pump")
    foundobject = idf_helpers.getobject_use_prevfield(idf, branch, "Component_1_Name")
    assert foundobject == pump
    # test for all times it should return None
    foundobject = idf_helpers.getobject_use_prevfield(idf, branch, "Name")
    foundobject = None  # prev field not end with Object_Type
    foundobject = idf_helpers.getobject_use_prevfield(
        idf, branch, "Component_11_Object_Type"
    )
    foundobject = None  # field does not end with "Name"
    foundobject = idf_helpers.getobject_use_prevfield(idf, branch, "Component_3_Name")
    foundobject = None  # bad idfobject key

