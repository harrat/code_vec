def test_register(self):
        context = Context()
        dbm_backend.register(context)
        self._store.save(self._person)

        # creating a different connection to test...
        connection = context.get_connection("dbm://%s" % self._tempfile)
        store = connection.get_store()
        person = store.fetch(Person, "foobar")
        self.assertEqual(person, self._person)
