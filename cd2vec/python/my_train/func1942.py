def test_models_regression():
    """Assert that the fit method works with all models for regression."""
    for model in [m for m in MODEL_NAMES if m not in ONLY_CLASSIFICATION]:
        atom = ATOMRegressor(X_reg, y_reg, test_size=0.24, random_state=1)
        atom.run(models=model,
                 metric='neg_mean_absolute_error',
                 n_calls=2,
                 n_random_starts=1,
                 bo_params={'base_estimator': 'gbrt', 'cv': 1})
        assert not atom.errors
        assert hasattr(atom, model) and hasattr(atom, model.lower())

