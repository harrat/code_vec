def test_submit_timelimit(minimal_job, local_only):
    minimal_job.time_limit = '2s'
    prepare_job(minimal_job, 'sleep 10')
    t_job = datetime.now()
    minimal_job.submit()
    assert minimal_job.jobid is not None
    minimal_job.wait()
    t_job = datetime.now() - t_job
    assert t_job.total_seconds() >= 2
    assert t_job.total_seconds() < 3
    with open(minimal_job.stdout) as fp:
        assert re.search('postrun', fp.read()) is None

    assert minimal_job.state == 'TIMEOUT'

