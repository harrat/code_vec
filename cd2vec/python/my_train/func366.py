def test_log_error():
    logger_name = 'test_name'
    TestMagicLogger = MagicLogger(logger_name=logger_name, file_name='test_output.txt', host=None)
    test_message = 'test_message'
    TestMagicLogger.error(test_message)

    with open('test_output.txt', 'r') as f:
        log_split = f.read().split(' - ')
        assert log_split[3].rstrip() == test_message
        assert log_split[2] == 'ERROR'
        assert log_split[1] == logger_name

    os.remove('test_output.txt')

def test_logger_extra():
    logger_name = 'test_name'
    TestMagicLogger = MagicLogger(logger_name=logger_name, file_name='test_output.txt', host=None, extra={"test": "test"})
    assert TestMagicLogger.extra.get("test")
    assert TestMagicLogger.extra["test"] == "test"
