@pytest.mark.parametrize('label', [
    'label1',
    'label2',
    'label3',
])
def test_watchers(real_world_spec, label):
    original_data = {
        'database': {
            'host': '1.2.3.4',
            'name': 'myapp_prod',
            'port': 3307,
            'verbose': False,
        },
        'emoji': u'?',
        'file': '/path/to/file.yaml',
        'ssl': {
            'private_key': 'blah',
            'public_key': 'blah',
        },
        'web_port': 443,
    }
    safe_data = copy.deepcopy(original_data)
    flags = {'overall': True, 'individual': True}
    real_world_path = os.path.join(current_dir, 'files', 'real_world')

    def overall_handler(old_config, new_config):
        flags['overall'] = False

    def indivual_handler(old_value, new_value):
        flags['individual'] = False

    def change_config(label):
        if label == 'label1':
            config = real_world_spec.load_config('label1')
            config.database.port += 1
            yapconf.dump_data(
                config.to_dict(),
                filename=yaml_filename,
                file_type='yaml'
            )

        elif label == 'label2':
            config = real_world_spec.load_config('label2')
            config.database.port += 1
            yapconf.dump_data(
                config.to_dict(),
                filename=json_filename,
                file_type='json'
            )

        elif label == 'label3':
            safe_data['database']['port'] += 1

    item = real_world_spec.find_item('database.port')
    item.watch_target = indivual_handler

    yaml_filename = os.path.join(real_world_path, 'config_to_change.yaml')
    json_filename = os.path.join(real_world_path, 'config_to_change.json')

    real_world_spec.add_source('label1', 'yaml', filename=yaml_filename)
    real_world_spec.add_source('label2', 'json', filename=json_filename)
    real_world_spec.add_source('label3', 'dict', data=safe_data)

    real_world_spec.spawn_watcher(label, target=overall_handler)
    time.sleep(0.1)

    change_config(label)

    wait_time = 0.0

    while any(flags.values()) and wait_time <= 3:
        time.sleep(0.25)
        wait_time += 0.25

    for flag in flags.values():
        assert not flag

