@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'import-remove', 'update-remove-utc')
    def test_cli_import_snippet_013(snippy):
        """Import defined snippet.

        Import defined snippet based on message digest. File name is defined
        from command line as text file which contain one snippet. Content
        links were updated. The file extension is '*.text' in this case.
        """

        content = {
            'data': [
                Content.deepcopy(Snippet.REMOVE)
            ]
        }
        content['data'][0]['links'] = ('https://new.link', )
        content['data'][0]['digest'] = '7681559ca5c001e204dba8ccec3fba3067049692de33a35af4a4647ec2addace'
        file_content = Content.get_file_content(Content.TEXT, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '-d', '54e41e9b52a02b63', '-f', 'one-snippet.text'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-snippet.text', mode='r', encoding='utf-8')
