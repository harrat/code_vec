def test_pre_receive(git_dir):
    git_dir.chdir()
    revisions = "{} {} ".format(
        "4b825dc642cb6eb9a060e54bf8d69288fbee4904",
        dzonegit.get_head(),
    )
    stdin = StringIO(revisions + "refs/heads/slave\n")
    with pytest.raises(SystemExit):
        dzonegit.pre_receive(stdin)
    stdin = StringIO(revisions + "refs/heads/master\n")
    dzonegit.pre_receive(stdin)

