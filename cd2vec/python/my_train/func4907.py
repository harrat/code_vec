def test_non_int_id(self):
        subject = '#x will not match the regex.'
        result = self._call_function_under_test(subject)
        self.assertIsNone(result)
