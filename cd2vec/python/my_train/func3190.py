@freeze_time('2014-01-21')
def test_commit_previous_files_previous_months(cli, data_dir, config):
    config.set('taxi', 'nb_previous_files', '2')

    efg = EntriesFileGenerator(data_dir, '%m_%Y.tks')
    efg.expand(datetime.date(2013, 11, 1)).write(
        "01/11/2013\nalias_1 2 november"
    )
    efg.expand(datetime.date(2013, 12, 1)).write(
        "01/12/2013\nalias_1 2 december"
    )
    efg.expand(datetime.date(2014, 1, 1)).write(
        "01/01/2014\nalias_1 4 january"
    )
    efg.patch_config(config)

    stdout = cli('commit', args=['--yes'])

    assert 'november' in stdout
    assert 'december' in stdout
    assert 'january' in stdout

