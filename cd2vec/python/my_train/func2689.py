def test_sequence(table):
    for name in ['Python', 'Ruby']:
        Category.create(name=name)
        Author.create(name=name + 'er')
    category_1, category_2 = Category.select()
    author_1, author_2 = Author.select()
    for title in ['Step1', 'Step2', 'Step3']:
        Course.create(title=category_1.name + title,
            category_id=category_1.id, author_id=author_1.id)
        Course.create(title=category_2.name + title,
            category_id=category_2.id, author_id=author_2.id)

    c = category_1.courses.first()
    assert c.sequence, 1

    current = 1
    for course in Course.select().order_by(Course.id)[1:]:
        assert course.sequence > current
        current = course.sequence

    c.change_sequence(new_sequence=2)
    assert c.sequence, 2
    c.change_sequence(1)
    assert c.sequence, 1

    courses = list(Course.select().order_by(Course.id))
    pre_course = courses[0]
    for c in courses[1:]:
        if c.category_id == pre_course.category_id:
            assert c.sequence > pre_course.sequence
        if c.author_id == pre_course.author_id:
            assert c.sequence > pre_course.sequence
        pre_course = c

    b = Book.create()
    assert Book.select().count() == 1
    assert b.sequence, 1
    b.change_sequence(1)
    assert b.sequence, 1
    with pytest.raises(ValueError):
        b.change_sequence(0)
    with pytest.raises(ValueError):
        b.change_sequence(2)
    Book.create()
    b.change_sequence(2)
    assert b.sequence, 2

