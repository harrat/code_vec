def test_parse_response(self):
        """ Should parse WebTest response. """
        res = self.client.post_json('/', params={'message': 'foo'})
        autodoc.parse('POST /', res)

        var = {
            'describe': 'POST /',
            'describe_separators': '======',
            'target_url': 'http://localhost:80',
            'status_code': 200,
            'request': 'POST /',
            'response_body': '{\n  "response": "create"\n}',
            'response_content_type': 'application/json',
            'params': '{\n  "message": "foo"\n}'
        }
        for k, v in iteritems(autodoc.vars[0]):
            self.assertEqual(v, var[k])

        autodoc.clear()
