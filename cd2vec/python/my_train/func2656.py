def test_git_describe(self):
        runner('git tag -a 1.0.0 -m "test message"')
        desc = relic.git.git_describe()
        assert isinstance(desc, str)
