def test_csv():
    reader = csv.DictReader(open(os.path.join(output_dir, 'data.csv')))

    row = next(reader)
    assert row['path'] == '1.png'
    assert row['sha256'] == '1e89b90b5973baad2e6c3294ffe648ff53ab0b9d75188e9fbb8b38deb9ba3341'
    assert row['original_paths'] == 'test-data/a.png'

    row = next(reader)
    assert row['path'] == '2.jpg'
    assert row['sha256'] == '45d257c93e59ec35187c6a34c8e62e72c3e9cfbb548984d6f6e8deb84bac41f4'
    assert row['original_paths'] == '"test-data/b.jpg","test-data/c/b.jpg"'

