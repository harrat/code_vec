@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_bash_exitcode(fixture, request):
    engine = request.getfixturevalue(fixture)
    job = pyccc.Job(image='python:2.7-slim',
                    command='sleep 5 && exit 35',
                    engine=engine,
                    submit=True)
    print(job.rundata)
    with pytest.raises(pyccc.JobStillRunning):
        job.exitcode
    job.wait()
    assert job.wait() == 35
    assert job.exitcode == 35

