def test_e2e_scenario_2(self):
        """
        Testing for privacyPolicy, false value of adSupported, containsAds, offersIAP
        that excluded from scenario 1
        """
        res = app("com.google.android.calendar")

        self.assertEqual("http://www.google.com/policies/privacy", res["privacyPolicy"])
        self.assertIsNone(res["adSupported"])
        self.assertFalse(res["containsAds"])
        self.assertFalse(res["offersIAP"])
