def test_is_allowed_by_operation_policy_groups_empty(self):
        """
        Test that access by operation policy is processed correctly when the
        provided set of user groups is empty.

        Note that _is_allowed will always return True here, but because there
        are no groups to check, access is by default denied.
        """
        e = engine.KmipEngine()
        e.is_allowed = mock.Mock(return_value=True)

        result = e._is_allowed_by_operation_policy(
            'test_policy',
            ['test_user', []],
            'test_user',
            enums.ObjectType.SYMMETRIC_KEY,
            enums.Operation.GET
        )

        e.is_allowed.assert_not_called()
        self.assertFalse(result)
