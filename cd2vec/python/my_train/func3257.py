def test_parse_no_joliet_name(tmpdir):
    indir = tmpdir.mkdir('nojolietname')
    outfile = str(indir)+'.iso'

    with open(os.path.join(str(indir), 'foo'), 'wb') as outfp:
        outfp.write(b'foo\n')

    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-J', '-hide-joliet', 'foo', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_onefile_joliet_no_file)

def test_parse_joliet_isolevel4_nofiles(tmpdir):
    indir = tmpdir.mkdir('jolietisolevel4nofiles')
    outfile = str(indir)+'.iso'
