def test_delete_can_timeout(self):
        rc = RestClient(jwt='a-token', telemetry=False, timeout=0.00001)

        with self.assertRaises(requests.exceptions.Timeout):
            rc.delete('http://google.com')
