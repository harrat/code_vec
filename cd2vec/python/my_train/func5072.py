def test_gaussian_distribution_mixture_log():
    distribution_amplitudes = [1, 1, 1]
    means = [-2, 0, 2]
    stds = [1, 1, 1]
    distribution_mins = [None for _ in range(len(means))]
    distribution_max = [None for _ in range(len(means))]

    hd = DistributionMixture.build_gaussian_mixture(distribution_amplitudes, means, stds, distribution_mins,
                                                    distribution_max, use_logs=True)

    samples = get_many_samples_for(hd)

    samples_median = np.median(samples)
    assert 0.5 < samples_median < 1.5
    samples_std = np.std(samples)
    assert 1 < samples_std < 4
    assert abs(hd.pdf(-2.) - 0.) < 1e-6
    assert abs(hd.pdf(1.) - 0.24377901627294607) < 1e-6
    assert abs(hd.pdf(5.) - 0.03902571107126729) < 1e-6
    assert abs(hd.cdf(-2.) - 0.) < 1e-6
    assert abs(hd.cdf(1.) - 0.5) < 1e-6
    assert abs(hd.cdf(5.) - 0.8720400927468334) < 1e-6

    assert hd.min() == 0
    assert hd.max() == np.inf
    assert abs(hd.mean() - 2.225189976999746) < 1e-6
    assert abs(hd.var() - 9.916017516376925) < 1e-6
    assert abs(hd.std() - 3.1489708662318434) < 1e-6
    # Verify that hd mean and variance also correspond to mean and variance of sampling.
    assert abs(hd.mean() - np.mean(samples)) < 1e-1
    assert abs(hd.var() - np.var(samples)) < 5e-1

