@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'import-netcat')
    def test_api_create_snippet_016(server):
        """Update snippet with POST that maps to DELETE.

        Send POST /snippets with the ``X-HTTP-Method-Override`` header to
        delete a snippet. In this case the resource exists and the content is
        deleted.
        """

        storage = {
            'data': [
                Storage.remove,
                Storage.forced
            ]
        }
        expect_headers = {}
        result = testing.TestClient(server.server.api).simulate_post(
            path='/api/snippy/rest/snippets/f3fd167c64b6f97e',
            headers={'accept': 'application/json', 'X-HTTP-Method-Override': 'DELETE'})
        assert result.status == falcon.HTTP_204
        assert result.headers == expect_headers
        assert not result.text
        Content.assert_storage(storage)
