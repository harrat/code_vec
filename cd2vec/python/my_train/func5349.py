def test_layer_4_checksums(self):

        #: :type port: xenavalkyrie.xena_port.XenaPort
        port = self.xm.session.reserve_ports([self.port1], force=False, reset=True)[self.port1]

        #: :type tcp_stream: xenavalkyrie.xena_stream.XenaStream
        tcp_stream = port.add_stream('tcp stream')

        eth = Ethernet(src_s='22:22:22:22:22:22')
        eth.dst_s = '11:11:11:11:11:11'
        vlan = Dot1Q(vid=17, prio=3)
        eth.vlan.append(vlan)
        ip = IP()
        tcp = TCP()
        headers = eth + ip + tcp
        tcp_stream.set_packet_headers(headers, l4_checksum=False)
        headerprotocol = tcp_stream.get_attribute('ps_headerprotocol')
        assert 'tcpcheck' not in headerprotocol.lower()
        tcp_stream.set_packet_headers(headers, l4_checksum=True)
        headerprotocol = tcp_stream.get_attribute('ps_headerprotocol')
        assert 'tcpcheck' in headerprotocol.lower()
        resulting_headers = tcp_stream.get_packet_headers()
        l4 = resulting_headers.upper_layer.upper_layer
        assert l4.sum == 0

        #: :type udp_stream: xenavalkyrie.xena_stream.XenaStream
        udp_stream = port.add_stream('udp stream')

        eth = Ethernet(src_s='44:44:44:44:44:44')
        eth.dst_s = '33:33:33:33:33:33'
        ip6 = IP6()
        udp = UDP()
        headers = eth + ip6 + udp
        udp_stream.set_packet_headers(headers, l4_checksum=False)
        headerprotocol = udp_stream.get_attribute('ps_headerprotocol')
        assert 'udpcheck' not in headerprotocol.lower()
        udp_stream.set_packet_headers(headers, l4_checksum=True)
        headerprotocol = udp_stream.get_attribute('ps_headerprotocol')
        assert 'udpcheck' in headerprotocol.lower()
        resulting_headers = udp_stream.get_packet_headers()
        l4 = resulting_headers.upper_layer.upper_layer
        assert l4.sum == 0
