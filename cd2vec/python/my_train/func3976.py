def test_invalid_mac_exception(self):
        """Test writing data to handle of the sensor."""
        bluepy.RETRY_LIMIT = 1
        with self.assertRaises(BluetoothBackendException):
            self.backend.connect(TEST_MAC)
            self.backend.read_handle(HANDLE_READ_NAME)
        bluepy.RETRY_LIMIT = 3
