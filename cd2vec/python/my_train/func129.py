def test_away_shutout_multiple_goalies_empty_field(self):
        shutout = ['', '1', '0']

        fake_shutout = PropertyMock(return_value=shutout)
        fake_num_goalies = PropertyMock(return_value=2)
        type(self.boxscore)._away_shutout = fake_shutout
        type(self.boxscore)._away_goalies = fake_num_goalies

        assert self.boxscore.away_shutout == 1
