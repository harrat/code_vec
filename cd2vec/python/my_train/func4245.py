def test_start_b_end_ub(self):
        self.grange.start = 'A2'
        self.grange.end = None
        end_addr = pygsheets.Address((self.worksheet.rows, self.worksheet.cols))
        assert self.grange.start == pygsheets.Address('A2')
        assert self.grange.end == end_addr
        assert self.grange.label == self.worksheet.title + '!' + 'A2' + ':' + end_addr.label
