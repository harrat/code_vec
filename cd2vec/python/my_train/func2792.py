@pytest.mark.parametrize("params", ["-m 1 -u 20".split()])
def test_new(params):
    mock_message = MockMessage(mock_user, mock_chat.id, "/new")
    mock_update = MockUpdate(mock_message)
    get_new(mock_bot, mock_update, params)
    received = mock_user.look_received()
    for res in received:
        if requests.get(res["photo"], proxies=proxy).status_code == 200:
            return
    assert False

