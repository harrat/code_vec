def test_get_trending_videos_with_min_likes(self):
        videos1 = get_trending_videos()
        videos2 = get_trending_videos(min_likes=100)
        self.assertGreater(len(videos1), len(videos2))
