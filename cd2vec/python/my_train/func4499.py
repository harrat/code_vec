def test_sess_rx_interval_get(session):
    """Attempt to get the Required Min Rx Interval"""
    assert session.required_min_rx_interval == 1000000

