def test_LaplacianThickness_defaults(change_dir, create_lt):
    lt = create_lt
    base_cmd = "LaplacianThickness functional.nii diffusion_weighted.nii functional_thickness.nii"
    assert lt.cmdline == base_cmd
    lt.inputs.smooth_param = 4.5
    assert lt.cmdline == base_cmd + " 4.5"
    lt.inputs.prior_thickness = 5.9
    assert lt.cmdline == base_cmd + " 4.5 5.9"

