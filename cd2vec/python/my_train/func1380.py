def test_clean_cache_wipes_database(self):
    """test_clean_cache_wipes_database ensures calls to clean_cache will
    clear out documents from the database"""
    self.func.maybe_insert_into_cache(self.string_to_coordinatesresult(
        """[{"coordinates":"pkg:conda/pycrypto@2.6.1",
        "reference":"https://ossindex.sonatype.org/component/pkg:conda/pycrypto@2.6.1",
        "vulnerabilities":[]}]"""))
    self.assertEqual(self.func.clean_cache(), True)
