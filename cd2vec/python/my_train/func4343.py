def test_makeductbranch():
    """py.test for makeductbranch"""
    tdata = (
        (
            "d_branch",
            [
                "BRANCH",
                "d_branch",
                0.0,
                "",
                "duct",
                "d_branch_duct",
                "d_branch_duct_inlet",
                "d_branch_duct_outlet",
                "Bypass",
            ],
            ["DUCT", "d_branch_duct", "d_branch_duct_inlet", "d_branch_duct_outlet"],
        ),  # db_name, branch_obj, duct_obj
    )
    for db_name, branch_obj, duct_obj in tdata:
        fhandle = StringIO("")
        idf = IDF(fhandle)
        result = hvacbuilder.makeductbranch(idf, db_name)
        assert result.obj == branch_obj
        theduct = idf.getobject("DUCT", result.Component_1_Name)
        assert theduct.obj == duct_obj

