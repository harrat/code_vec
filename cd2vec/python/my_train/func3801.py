def test_generate_on_none_vcf(cli_runner):
    result = cli_runner.invoke(generate, [OUT_TXT_FILE])
    assert result.exit_code == 1
    assert isinstance(result.exception, TypeError) is True

