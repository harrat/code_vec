def test_add_creates_alias(cli, config, projects_db):
    cli('project', ['alias', 'active'], input='1\n0\nfoobar')

    with open(config.path, 'r') as f:
        config_lines = f.readlines()

    assert 'foobar = 43/1\n' in config_lines

