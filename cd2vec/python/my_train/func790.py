@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_api_delete_snippet_004(server):
        """Delete snippet with UUID.

        Send DELETE /snippets/{id} to delete one snippet. The ``id`` in
        URI matches to one resource that is deleted.
        """

        content = {
            'data': [
                Storage.remove
            ]
        }
        expect_headers = {}
        result = testing.TestClient(server.server.api).simulate_delete(
            path='/api/snippy/rest/snippets/' + Snippet.FORCED_UUID,
            headers={'accept': 'application/json'})
        assert result.status == falcon.HTTP_204
        assert result.headers == expect_headers
        assert not result.text
        Content.assert_storage(content)
