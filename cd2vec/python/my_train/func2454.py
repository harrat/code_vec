def test_init_with_exclude_arg(self, bids_layout):
        root = join(DIRNAME, 'data', '7t_trt')
        config = join(DIRNAME, 'specs', 'test.json')
        layout = Layout([(root, config)], regex_search=True, exclude='sub-\d*')
        target = join(root, "dataset_description.json")
        assert target in bids_layout.files
        assert target in layout.files
        sub_file = join(root, "sub-01", "sub-01_sessions.tsv")
        assert sub_file in bids_layout.files
        assert sub_file not in layout.files
