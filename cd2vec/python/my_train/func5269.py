def test_marking_definition_invalid_definition(self):
        marking_definition = copy.deepcopy(self.valid_marking_definition)
        marking_definition['definition']['tlp'] = 21
        self.assertFalseWithOptions(marking_definition)
