def test_pickle_dumpsloads_simple():
    orig = udict({'one': 1, 'two': 2})
    unpickled = pickle.loads(pickle.dumps(orig))
    assert items(unpickled) == items(orig)
    assert isinstance(unpickled, udict)

