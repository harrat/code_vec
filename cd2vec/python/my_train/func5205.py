def test_on_subscribe_success(self, mocker):
        error = mocker.patch(
            'bitmex_websocket.BitMEXWebsocket.on_error')
        socket = BitMEXWebsocket()
        message = {
            "success": "true",
            "subscribe": "instrument:XBTH17",
            "request": {
                "op": "subscribe",
                "args": ["instrument:XBTH17"]
            }
        }
        subscribe_handler = mocker.stub()
        socket.on('subscribe', subscribe_handler)
        socket.emit('subscribe', message)

        error.assert_not_called()
        subscribe_handler.assert_called_once_with(message)
