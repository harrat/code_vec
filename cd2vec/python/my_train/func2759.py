@pytest.mark.parametrize("exporters", ["html", "asciidoc", "rst"])
def test_nbconv(exporters, tmp_path: Path) -> None:
    """Convert a temporary notebook with each exporter in ``exporters``."""
    nb = make_notebook(tmp_path)
    assert nbconv(in_file=nb, exporter=exporters)[0].endswith("." + exporters)
    assert nbconv(in_file=nb)[0].endswith(".html")

