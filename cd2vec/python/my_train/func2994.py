def test_create_and_get(self):
        random_data = generate_nonce(100).encode()
        attachment = self.member.create_blob(self.member.member_id, self.file_type, self.file_name, random_data)
        blob_payload = Blob.Payload(owner_id=self.member.member_id, name=self.file_name, type=self.file_type,
                                    data=random_data, access_mode=Blob.DEFAULT)
        blob_hash = hash_proto_message(blob_payload)
        assert attachment.blob_id == "b:{}:{}".format(blob_hash, blob_payload.owner_id.split(':')[-1])
        assert attachment.type == self.file_type
        assert attachment.name == self.file_name

        out_blob = self.member.get_blob(attachment.blob_id)
        assert out_blob.id == attachment.blob_id
        assert out_blob.payload.data == random_data
        assert out_blob.payload.owner_id == self.member.member_id
