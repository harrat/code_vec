def test_makecondenserloop():
    """pytest for makecondenserloop"""
    tdata = (
        (
            "",
            "c_loop",
            ["sb0", ["sb1", "sb2", "sb3"], "sb4"],
            ["db0", ["db1", "db2", "db3"], "db4"],
            """BRANCH, sb0, 0.0, , Pipe:Adiabatic, sb0_pipe,
        c_loop Cond_Supply Inlet, sb0_pipe_outlet, Bypass;  BRANCH, sb1, 0.0,
        , Pipe:Adiabatic, sb1_pipe, sb1_pipe_inlet, sb1_pipe_outlet,
        Bypass;  BRANCH, sb2, 0.0, , Pipe:Adiabatic, sb2_pipe,
        sb2_pipe_inlet, sb2_pipe_outlet, Bypass;  BRANCH, sb3, 0.0, ,
        Pipe:Adiabatic, sb3_pipe, sb3_pipe_inlet, sb3_pipe_outlet,
        Bypass;  BRANCH, sb4, 0.0, , Pipe:Adiabatic, sb4_pipe,
        sb4_pipe_inlet, c_loop Cond_Supply Outlet, Bypass;  BRANCH,
        db0, 0.0, , Pipe:Adiabatic, db0_pipe, c_loop Demand Inlet,
        db0_pipe_outlet, Bypass;  BRANCH, db1, 0.0, , Pipe:Adiabatic, db1_pipe,
        db1_pipe_inlet, db1_pipe_outlet, Bypass;  BRANCH, db2, 0.0, ,
        Pipe:Adiabatic, db2_pipe, db2_pipe_inlet, db2_pipe_outlet, Bypass;
        BRANCH, db3, 0.0, , Pipe:Adiabatic, db3_pipe, db3_pipe_inlet,
        db3_pipe_outlet, Bypass;  BRANCH, db4, 0.0, , Pipe:Adiabatic,
        db4_pipe, db4_pipe_inlet, c_loop Demand Outlet, Bypass;
        BRANCHLIST, c_loop Cond_Supply Branchs, sb0, sb1, sb2, sb3, sb4;
        BRANCHLIST, c_loop Condenser Demand Branchs, db0, db1, db2, db3,
        db4;  CONNECTOR:SPLITTER, c_loop_supply_splitter, sb0, sb1,
        sb2, sb3;  CONNECTOR:SPLITTER, c_loop_demand_splitter, db0, db1, db2,
        db3;  CONNECTOR:MIXER, c_loop_supply_mixer, sb4, sb1, sb2, sb3;
        CONNECTOR:MIXER, c_loop_demand_mixer, db4, db1, db2, db3;
        CONNECTORLIST, c_loop Cond_Supply Connectors, Connector:Splitter,
        c_loop_supply_splitter, Connector:Mixer, c_loop_supply_mixer;
        CONNECTORLIST, c_loop Condenser Demand Connectors,
        Connector:Splitter, c_loop_demand_splitter, Connector:Mixer,
        c_loop_demand_mixer;  PIPE:ADIABATIC, sb0_pipe,
        c_loop Cond_Supply Inlet, sb0_pipe_outlet;  PIPE:ADIABATIC,
        sb1_pipe, sb1_pipe_inlet, sb1_pipe_outlet;  PIPE:ADIABATIC, sb2_pipe,
        sb2_pipe_inlet, sb2_pipe_outlet;  PIPE:ADIABATIC, sb3_pipe,
        sb3_pipe_inlet, sb3_pipe_outlet;  PIPE:ADIABATIC, sb4_pipe,
        sb4_pipe_inlet, c_loop Cond_Supply Outlet;  PIPE:ADIABATIC,
        db0_pipe, c_loop Demand Inlet, db0_pipe_outlet;  PIPE:ADIABATIC,
        db1_pipe, db1_pipe_inlet, db1_pipe_outlet;  PIPE:ADIABATIC,
        db2_pipe, db2_pipe_inlet, db2_pipe_outlet;  PIPE:ADIABATIC,
        db3_pipe, db3_pipe_inlet, db3_pipe_outlet;  PIPE:ADIABATIC, db4_pipe,
        db4_pipe_inlet, c_loop Demand Outlet;  CONDENSERLOOP, c_loop, Water, ,
        , , , , , 0.0, Autocalculate, c_loop Cond_Supply Inlet,
        c_loop Cond_Supply Outlet, c_loop Cond_Supply Branchs,
        c_loop Cond_Supply Connectors, c_loop Demand Inlet,
        c_loop Demand Outlet, c_loop Condenser Demand Branchs,
        c_loop Condenser Demand Connectors, Sequential, None;  """,
        ),  # blankidf, loopname, sloop, dloop, nidf
    )
    for blankidf, loopname, sloop, dloop, nidf in tdata:

        fhandle = StringIO("")
        idf1 = IDF(fhandle)
        loopname = "c_loop"
        sloop = ["sb0", ["sb1", "sb2", "sb3"], "sb4"]
        dloop = ["db0", ["db1", "db2", "db3"], "db4"]
        hvacbuilder.makecondenserloop(idf1, loopname, sloop, dloop)
        idf2 = IDF(StringIO(nidf))
        assert str(idf1.model) == str(idf2.model)

