def test_flex_alloc_sched_access_partition(make_flexible_job):
    job = make_flexible_job('all', sched_access=['--partition=p1'])
    prepare_job(job)
    assert job.num_tasks == 16

