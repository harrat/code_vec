def test_interface_error():
    assert issubclass(py2jdbc.InterfaceError, py2jdbc.Error), \
        "InterfaceError is not a subclass of Error"
