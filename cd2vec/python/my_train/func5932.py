def test_unrar_nested_cbr():
    file = "samples/nested.cbr"
    extracted_files = extract(file, EXTRACT_TO_DIR)
    assert extracted_files == ['a', 'b'], "extracts a nested cbr into a flat hierarchy"
    assert os.path.isfile(os.path.join(EXTRACT_TO_DIR, 'a'))
    assert os.path.isfile(os.path.join(EXTRACT_TO_DIR, 'b'))

