def test_impostor_destroy():
    with Mountebank() as mb:
        imposter = mb.add_imposter_simple()
        imposter.destroy()
        with pytest.raises(requests.exceptions.ConnectionError):
            requests.get('http://localhost:{}'.format(imposter.port))
