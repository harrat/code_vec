def test_translate_failsafe(client):
    eng.fail_safe = True
    eng.service_urls = ['fail_translation_test.not']

    resp = eng.translate(
        text=text,
        src=src,
        dest=['en']
    )

    assert eng.errors != []
    assert resp == text

