def test_polygon(self):
        poly = Polygon.parse('(fp_poly (pts (xy 0 0) (xy 1 0)) (width 0.01))')
        assert poly.pts == [(0, 0), (1, 0)]
        assert poly.width == 0.01
        assert Polygon.parse(poly.to_string()) == poly
