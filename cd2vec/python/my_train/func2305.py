def test_unicode():
    translator = Translator.translator(src='ko', dest='ja')
    result = translator.translate('?????.')
    assert result == '??????'

