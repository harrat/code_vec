def test_flex_alloc_not_enough_idle_nodes(make_flexible_job):
    job = make_flexible_job('idle')
    job.num_tasks = -12
    with pytest.raises(JobError):
        prepare_job(job)
