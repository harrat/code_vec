def test_iter_results(capsys, tmpdir, example_results):
    dst = os.path.join(six.text_type(tmpdir), 'example_results')
    shutil.copytree(example_results, dst)

    path = os.path.join(dst, 'cheetah')

    skip_list = [
        'machine.json',
        'aaaaaaaa-py2.7-Cython-numpy1.8.json', # malformed file
        'bbbbbbbb-py2.7-Cython-numpy1.8.json', # malformed file
        'cccccccc-py2.7-Cython-numpy1.8.json', # malformed file
    ]

    files = [f for f in os.listdir(path) if f.endswith('.json') and f not in skip_list]
    res = list(results.iter_results(path))
    assert len(res) == len(files)
    out, err = capsys.readouterr()
    assert skip_list[1] in out
    assert skip_list[2] in out
    assert skip_list[3] in out
    assert skip_list[0] not in out

    # The directory should be ignored without machine.json
    os.unlink(os.path.join(path, 'machine.json'))
    res = list(results.iter_results(path))
    assert len(res) == 0
    out, err = capsys.readouterr()
    assert "machine.json" in out

