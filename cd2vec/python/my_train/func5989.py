def test_consultar_cep_integration(self):

        # Aqui realizamos um teste de integra��o
        # pra garantir o funcionamento do todo

        # Realizamos a consulta de CEP
        try:
            endereco = consultar_cep('37.503-130')
        except excecoes.ExcecaoPyCEPCorreios as exc:
            print(exc.message)

        assert endereco['bairro'] == 'Santo Ant�nio'
        assert endereco['cep'] == '37503130'
        assert endereco['cidade'] == 'Itajub�'
        assert endereco['complemento2'] == '- at� 214/215'
        assert endereco['end'] == 'Rua Geraldino Campista'
        assert endereco['uf'] == 'MG'
        assert endereco['unidadesPostagem'] == []
