def test_batteriesCommon():
    d1 = ReplDict()
    l1 = ReplLockManager(autoUnlockTime=30.0)

    d2 = ReplDict()
    l2 = ReplLockManager(autoUnlockTime=30.0)

    a = [getNextAddr(), getNextAddr()]

    o1 = TestObj(a[0], [a[1]], TEST_TYPE.AUTO_TICK_1, consumers=[d1, l1])
    o2 = TestObj(a[1], [a[0]], TEST_TYPE.AUTO_TICK_1, consumers=[d2, l2])

    doAutoTicks(10.0, stopFunc=lambda: o1.isReady() and o2.isReady())

    assert o1.isReady() and o2.isReady()

    d1.set('testKey', 'testValue', sync=True)
    doAutoTicks(3.0, stopFunc=lambda: d2.get('testKey') == 'testValue')

    assert d2['testKey'] == 'testValue'

    d2.pop('testKey', sync=True)
    doAutoTicks(3.0, stopFunc=lambda: d1.get('testKey') == None)

    assert d1.get('testKey') == None

    assert l1.tryAcquire('test.lock1', sync=True) == True
    assert l2.tryAcquire('test.lock1', sync=True) == False
    assert l2.isAcquired('test.lock1') == False

    l1id = l1._ReplLockManager__selfID
    l1._ReplLockManager__lockImpl.prolongate(l1id, 0, _doApply=True)

    l1.release('test.lock1', sync=True)
    assert l2.tryAcquire('test.lock1', sync=True) == True

    assert d1.setdefault('keyA', 'valueA', sync=True) == 'valueA'
    assert d2.setdefault('keyA', 'valueB', sync=True) == 'valueA'
    d2.pop('keyA', sync=True)
    assert d2.setdefault('keyA', 'valueB', sync=True) == 'valueB'

    o1.destroy()
    o2.destroy()

    l1.destroy()
    l2.destroy()

