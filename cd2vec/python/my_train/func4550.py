@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_snippet_001(snippy):
        """Import all snippet resources.

        Import all snippets. File name is not defined in command line. This
        must result tool internal default file name and format being used.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.NETCAT
            ]
        }
        file_content = Content.get_file_content(Content.MKDN, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './snippets.mkdn', mode='r', encoding='utf-8')
