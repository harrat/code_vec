def test_get_snli_twice(self):
        get_snli()
        with mock.patch('lineflow.datasets.snli.pickle', autospec=True) as mock_pickle:
            get_snli()
        mock_pickle.dump.assert_not_called()
        self.assertEqual(mock_pickle.load.call_count, 1)
