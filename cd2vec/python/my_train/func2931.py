def test_df_upload(sp_upload):
    """Test DataFrame upload."""
    data_file = Path(_TEST_DATA).joinpath("syslog_data.csv")
    data = pd.read_csv(data_file, parse_dates=["TimeGenerated"])
    sp_upload.upload_df(data, "test_upload", "test_upload")

