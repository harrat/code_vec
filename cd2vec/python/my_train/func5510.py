def test_save_with_lineendings_and_encodings():
    """
    Test the IDF.save() function with combinations of encodings and line
    endings.

    """
    file_text = "Material,TestMaterial,  !- Name"
    idf = IDF(StringIO(file_text))
    lineendings = ("windows", "unix", "default")
    encodings = ("ascii", "latin-1", "UTF-8")

    for le, enc in product(lineendings, encodings):
        file_handle = StringIO()
        idf.save(file_handle, encoding=enc, lineendings=le)
        file_handle.seek(0)
        result = file_handle.read().encode(enc)
        if le == "windows":
            assert b"\r\n" in result
        elif le == "unix":
            assert b"\r\n" not in result
        elif le == "default":
            assert os.linesep.encode(enc) in result
