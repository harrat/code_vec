def test_get_popular():
    shows = isle.show.get_popular()
    assert inspect.isgenerator(shows)
    show = next(shows)
    assert isinstance(show, Show)

