@freeze_time("2018-01-01")
def test_profile_increment() -> None:
    """Test the profile_increment method."""
    m = MixpanelTrack(
        settings={
            "mixpanel.profile_properties": "pyramid_mixpanel.tests.test_track.FooProfileProperties"
        },
        distinct_id="foo",
    )

    m.profile_increment(props={FooProfileProperties.foo: 1})
    assert len(m.api._consumer.mocked_messages) == 1
    assert m.api._consumer.mocked_messages[0].endpoint == "people"
    assert m.api._consumer.mocked_messages[0].msg == {
        "$token": "testing",
        "$time": 1514764800000,
        "$distinct_id": "foo",
        "$add": {"Foo": 1},
    }
