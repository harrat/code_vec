def test_regression_parameterized(generate_result_dir):
    before = {"params": [["a", "b", "c", "d"]], "result": [5, 1, 1, 10]}
    after = {"params": [["a", "b", "c", "d"]], "result": [6, 1, 10, 1]}
    conf, repo, commits = generate_result_dir(5 * [before] + 5 * [after])
    tools.run_asv_with_conf(conf, "publish")
    regressions = util.load_json(join(conf.html_dir, "regressions.json"))
    expected = {'regressions': [[
        'time_func(a)',
        _graph_path(repo.dvcs),
        {},
        0,
        6.0, 5.0, [[None, 5, 5.0, 6.0]],
    ], [
        'time_func(c)',
        _graph_path(repo.dvcs),
        {},
        2,
        10.0, 1.0, [[None, 5, 1.0, 10.0]],
    ]]}
    assert regressions == expected

