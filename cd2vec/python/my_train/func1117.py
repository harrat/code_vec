def test_breadth(self):
		x = model.TransferOfCustody()
		e = model.Activity()
		fr = model.Group()
		to = model.Group()
		w = model.HumanMadeObject()
		fr._label = "From"
		to._label = "To"
		x.transferred_custody_of = w
		x.transferred_custody_from = fr
		x.transferred_custody_to = to
		e.used_specific_object = w
		e.carried_out_by = to
		w.current_owner = fr
		x.specific_purpose = e
		js = model.factory.toJSON(x)
		# Okay ... if we're breadth first, then custody_from is a resource
		# And now it's the first in the list
		self.assertTrue(isinstance(js['transferred_custody_from'][0], OrderedDict))
