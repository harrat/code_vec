def test_signal_handler(self):
        """
        Test that the signal handler for SIGINT and SIGTERM correctly stops
        the monitor.
        """
        m = monitor.PolicyDirectoryMonitor(
            self.tmp_dir,
            multiprocessing.Manager().dict()
        )
        m.stop = mock.MagicMock()
        handler = signal.getsignal(signal.SIGINT)

        m.stop.assert_not_called()
        handler(None, None)
        m.stop.assert_called()
