def test_defined():
    eta._NOW = lambda: 1411868721.5
    progress_bar = ProgressBarWget(2000)

    assert ' 0% [                  ] 0           --.-KiB/s              ' == str(progress_bar)
    assert ' 0% [                  ] 0           --.-KiB/s              ' == str(progress_bar)
    assert ' 0% [                  ] 0           --.-KiB/s              ' == str(progress_bar)

    eta._NOW = lambda: 1411868722.0
    progress_bar.numerator = 102
    assert ' 5% [                  ] 102         --.-KiB/s              ' == str(progress_bar)
    assert ' 5% [                  ] 102         --.-KiB/s              ' == str(progress_bar)

    eta._NOW = lambda: 1411868722.5
    progress_bar.numerator = 281
    assert '14% [=>                ] 281            358B/s  eta 5s      ' == str(progress_bar)

    eta._NOW = lambda: 1411868723.0
    progress_bar.numerator = 593
    assert '29% [====>             ] 593            491B/s  eta 3s      ' == str(progress_bar)

    eta._NOW = lambda: 1411868723.5
    progress_bar.numerator = 1925
    assert '96% [================> ] 1,925       1.13KiB/s  eta 1s      ' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 1999
    assert '99% [================> ] 1,999       1.06KiB/s  eta 1s      ' == str(progress_bar)

    eta._NOW = lambda: 1411868724.5
    progress_bar.numerator = 2000
    assert '100%[=================>] 2,000          666B/s   in 3s      ' == str(progress_bar)

