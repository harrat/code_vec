def test_peek(self):
        path = self.root.make_file()
        with self.assertRaises(IOError):
            with open_(path, 'w') as o:
                o.peek()
        path = self.root.make_file(contents='foo')
        with open_(path, 'rb') as i:
            assert b'f' == i.peek(1)
            assert b'foo' == next(i)
        with open_(path, 'rt') as i:
            assert 'f' == i.peek(1)
            assert 'foo' == next(i)
        with intercept_stdin('foo'):
            with open_(STDIN, validate=False, compression=False) as i:
                assert 'f' == i.peek(1)
                assert 'foo\n' == next(i)
