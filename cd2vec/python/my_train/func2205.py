@pytest.mark.vcr()
def test_access_groups_list_filter_value_typeerror(api):
    with pytest.raises(TypeError):
        api.access_groups.list(('name', 'match', 1))
