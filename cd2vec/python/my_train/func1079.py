def test_lines_read_compressed(tmpdir, random_lines):
    """Test that wkr.lines can read compressed files."""
    # make a compressed file
    path = tmpdir.join('text.gz').ensure().strpath
    with wkr.open(path, 'wb') as output_file:
        for line in random_lines:
            output_file.write(line.encode('utf-8') + b'\n')
    # now read it back in with wkr.lines()
    loaded_lines = list(wkr.lines(path))
    assert [[line.strip() for line in loaded_lines] == random_lines]

