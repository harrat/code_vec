def test_get_balance(self):
        balance_dict = {
            'confirming': {},
            'confirmed': {
                'GAS': '90000000.0',
                'SWTH': '73580203956.0',
                'NEO': '113073528.0'},
            'locked': {
                'GAS': '9509259.0',
                'NEO': '4000000.0'}}
        balance_dict_set = set(balance_dict.keys())
        first_address = ['ca7316f459db1d3b444f57fe1ab875b3a607c200']
        second_address = ['fea2b883725ef2d194c9060f606cd0a0468a2c59']
        all_addresses = ['ca7316f459db1d3b444f57fe1ab875b3a607c200', 'fea2b883725ef2d194c9060f606cd0a0468a2c59']
        contracts = pc.get_contracts()
        all_contracts = []
        for chain in contracts:
            for contract in contracts[chain]:
                if chain == 'NEO':
                    all_contracts.append(contracts[chain][contract])
        first_balance_dict = pc.get_balance(addresses=first_address, contracts=all_contracts)
        first_balance_dict_set = set(first_balance_dict.keys())
        second_balance_dict = pc.get_balance(addresses=second_address, contracts=all_contracts)
        second_balance_dict_set = set(second_balance_dict.keys())
        all_balance_dict = pc.get_balance(addresses=all_addresses, contracts=all_contracts)
        all_balance_dict_set = set(all_balance_dict.keys())
        self.assertTrue(first_balance_dict_set.issubset(balance_dict_set))
        self.assertTrue(second_balance_dict_set.issubset(balance_dict_set))
        self.assertTrue(all_balance_dict_set.issubset(balance_dict_set))

        sum_balance_dict = {'confirmed': {
                                'GAS': str(int(float(first_balance_dict['confirmed']['GAS'])) + int(
                                    float(second_balance_dict['confirmed']['GAS']))),
                                'NEO': str(int(float(first_balance_dict['confirmed']['NEO'])) + int(
                                    float(second_balance_dict['confirmed']['NEO']))),
                                'SWTH': str(int(float(first_balance_dict['confirmed']['SWTH'])) + int(
                                    float(second_balance_dict['confirmed']['SWTH']))),
                            }}
        self.assertDictEqual(all_balance_dict['confirmed'], sum_balance_dict['confirmed'])
