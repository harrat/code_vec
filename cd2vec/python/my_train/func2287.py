def test_cli_pypi_package(self):
        test_package = "flask-ask"
        with self.capture_output() as (out, err):
            main([test_package])
        output = out.getvalue().strip()
        self.assertEqual(
            output.split("\n")[0],
            f"get_version_from_pkg_resources: The '{test_package}' distribution was not found and is required by the application",
        )

        with self.capture_output() as (out, err):
            main([test_package, "increment"])
        output_inc = out.getvalue().strip()
        self.assertEqual(output_inc.split("\n")[1][:-1], output.split("\n")[1][:-1])
