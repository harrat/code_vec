def test_import_after_remove(mbed, testrepos):
    test_remove(mbed, testrepos)
    mkcommit('test1')

    test1 = testrepos[0]
    popen(['python', mbed, 'import', test1, 'testimport'])

    assertls(mbed, 'testimport', [
        "[mbed]",
        "testimport",
    ])
