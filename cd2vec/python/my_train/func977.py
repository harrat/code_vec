def test_list_my_props(self):
		p1 = model.Person()
		p1.classified_as = model.Type()
		props = p1.list_my_props()
		self.assertEqual(set(props), set(['classified_as', 'id']))
		props = p1.list_my_props(filter=model.Type)
		self.assertEqual(props, ['classified_as'])
