def test_guard_change_text_file(self):

        with guard(TestFileGuardContextManager.TEST_TEXT_FILE_PATH):
            lines_to_write = ['of course\n', 'I would\n']
            self._assert_file_content_equals(TestFileGuardContextManager.TEST_FILE_CONTENTS)

            with open(TestFileGuardContextManager.TEST_TEXT_FILE_PATH, 'w') as file:
                file.writelines(lines_to_write)

            self._assert_file_content_equals(lines_to_write)

        self._assert_file_content_equals(TestFileGuardContextManager.TEST_FILE_CONTENTS)
