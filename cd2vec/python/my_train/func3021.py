def test_fake_watcher(self):
        d = "test"
        g = "DEFAULT_GROUP"

        class Share:
            content = None
            count = 0

        cache_key = "+".join([d, g, NAMESPACE])

        def test_cb(args):
            print(args)
            Share.count += 1
            Share.content = args["content"]

        client.add_config_watcher(d, g, test_cb)
        client.add_config_watcher(d, g, test_cb)
        client.add_config_watcher(d, g, test_cb)
        time.sleep(1)
        client.notify_queue.put((cache_key, "xxx", "md51"))
        time.sleep(1)
        self.assertEqual(Share.content, "xxx")
        self.assertEqual(Share.count, 3)

        client.remove_config_watcher(d, g, test_cb)
        Share.count = 0
        client.notify_queue.put((cache_key, "yyy", "md52"))
        time.sleep(1)
        self.assertEqual(Share.content, "yyy")
        self.assertEqual(Share.count, 2)

        client.remove_config_watcher(d, g, test_cb, True)
        Share.count = 0
        client.notify_queue.put((cache_key, "not effective, no watchers", "md53"))
        time.sleep(1)
        self.assertEqual(Share.content, "yyy")
        self.assertEqual(Share.count, 0)

        Share.count = 0
        client.add_config_watcher(d, g, test_cb)
        time.sleep(1)
        client.notify_queue.put((cache_key, "zzz", "md54"))
        time.sleep(1)
        self.assertEqual(Share.content, "zzz")
        self.assertEqual(Share.count, 1)

        Share.count = 0
        client.notify_queue.put((cache_key, "not effective, md5 no changes", "md54"))
        time.sleep(1)
        self.assertEqual(Share.content, "zzz")
        self.assertEqual(Share.count, 0)
