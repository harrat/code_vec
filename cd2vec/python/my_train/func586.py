@given(
    floats(allow_nan = False, allow_infinity = False),
    floats(allow_nan = False, allow_infinity = False),
    floats(allow_nan = False, allow_infinity = False),
)
def test_spherical_position_roundtrip(n, e, v):
    n, e, v = normalize(n, e, v)

    inc, azi = spherical(n, e, v)
    V, N, E = direction_vector(inc, azi)

    if np.isnan(V):
        assert np.isnan(v)
    else:
        assert V == approx(v, abs = 1e-9)

    if np.isnan(N):
        assert np.isnan(n)
    else:
        assert N == approx(n, abs = 1e-9)

    if np.isnan(E):
        assert np.isnan(e)
    else:
        assert E == approx(e, abs = 1e-9)
