def test_decompress_path_error(self):
        path = self.root.make_file()
        with gzip.open(path, 'wt') as o:
            o.write('foo')
        with self.assertRaises(Exception):
            fmt = get_format('.gz')
            fmt.decompress_file(path)
