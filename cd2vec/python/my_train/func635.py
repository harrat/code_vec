def test_transcode(self):
        path = self.root.make_file()
        gzfile = Path(str(path) + '.gz')
        with gzip.open(gzfile, 'wt') as o:
            o.write('foo')
        bzfile = Path(str(path) + '.bz2')
        transcode_file(gzfile, bzfile)
        with bz2.open(bzfile, 'rt') as i:
            assert 'foo' == i.read()
