def test_name(self):
        noop = nodal.nodes.NoOp()
        self.assertEqual('NoOp1', noop.name)
        noop.name = 'FooOp'
        self.assertEqual('FooOp', noop.name)
