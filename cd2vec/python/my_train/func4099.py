def test_pheno_assocs_compact():
    assocs = search_associations_compact(subject=TWIST_ZFIN,
                                         rows=1000,
                                         object_category='phenotype'
    )
    assert len(assocs) == 1
    a = assocs[0]
    assert a['subject'] == TWIST_ZFIN
    assert 'ZP:0007631' in a['objects']
    
def test_pheno_objects():
    results = search_associations(subject=TWIST_ZFIN,
                                  fetch_objects=True,
                                  rows=0,
                                  object_category='phenotype'
    )
    objs = results['objects']
    print(str(objs))
    assert len(objs) > 1
    assert 'ZP:0007631' in objs
