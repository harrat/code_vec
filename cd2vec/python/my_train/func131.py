def test_net_auto_numbering(self):
        n1, n2, n3 = Net(), Net(), Net()
        assert n1.code == 1
        assert n2.code == 2
        assert n3.code == 3
