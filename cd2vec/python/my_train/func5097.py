def test_get_now_playing():
    movies = isle.movie.get_now_playing()
    assert inspect.isgenerator(movies)
    movie = next(movies)
    assert isinstance(movie, Movie)

