def test_aggregator_set_method(mocked_aggregated):
    """Test aggregator set method."""

    res = mocked_aggregated.set('value', 'pattern')
    assert res is True
    for node in mocked_aggregated._aggregator._redis_nodes:
        assert node.value == 'pattern'

