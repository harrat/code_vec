def test_h5set_attr(self):
        h5create_file(main_path, 'test_deleting3')
        file = '{}/test_deleting3.h5'.format(test_path)

        h5create_group(file, 'testing_group')
        h5set_attr(file, 'testing_group', 'attr_test', 'sup')

        self.assertEqual(h5read_attr(file, 'testing_group', 'attr_test'), 'sup')

        h5delete_file(file)
