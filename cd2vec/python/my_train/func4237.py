@given(dataframes=n_time_series_with_same_index(min_n=5))
    def test_fit_predict_on_subset_of_time_series(
        self, dataframes, hierarchical_basic_bottom_up_model
    ):
        key = np.random.choice(list(dataframes.keys()), 1)[0]
        hierarchical_basic_bottom_up_model.fit(dataframes)
        hierarchical_basic_bottom_up_model.predict({key: dataframes[key]})
