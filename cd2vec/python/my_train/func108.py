def test_report(handler_with_profile, mock_context, mock_event):
    thundra, handler = handler_with_profile

    trace_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, TracePlugin):
            trace_plugin = plugin

    handler(mock_event, mock_context)

    assert trace_plugin.trace_data['startTimestamp'] is not None
    assert trace_plugin.trace_data['finishTimestamp'] is not None

    start_time = trace_plugin.trace_data['startTimestamp']
    end_time = trace_plugin.trace_data['finishTimestamp']

    duration = end_time - start_time

    assert trace_plugin.trace_data['duration'] == duration

    assert trace_plugin.trace_data['applicationName'] == 'test_func'
    assert trace_plugin.trace_data['applicationId'] == 'aws:lambda:us-west-2:123456789123:test'
    assert trace_plugin.trace_data['applicationInstanceId'] == 'id'
    assert trace_plugin.trace_data['applicationVersion'] == 'version'
    assert trace_plugin.trace_data['applicationRuntime'] == 'python'

