def test_iter():
    d = dagger.dagger()
    d.add('1', ['2', '3', '4'])
    d.add('4', ['5', '6'])
    d.stale('6')
    d.run(allpaths=True)

    iter = d.iter()
    ldict = iter.ldict

    for name in '1 4 6'.split():
        assert ldict.get(d.nodes[name])

    for name in '2 3 5'.split():
        assert ldict.get(d.nodes[name])

