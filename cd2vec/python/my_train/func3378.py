def test_name():
    global dir
    a = aud.Dir(dir)

    a.config_set_extensions(["txt"])
    assert a.config_get_extensions() == [".txt"]
    assert sorted(a.get_all()) == ["abc.txt", "test.txt"]

    assert a.name_upper()
    assert sorted(a.get_all()) == ["ABC.txt", "TEST.txt"]

    assert a.name_lower()
    assert sorted(a.get_all()) == ["abc.txt", "test.txt"]

    assert a.name_prepend("abc_")
    assert sorted(a.get_all()) == ["abc_abc.txt", "abc_test.txt"]

    assert a.name_append("_test")
    assert sorted(a.get_all()) == ["abc_abc_test.txt", "abc_test_test.txt"]

    assert a.name_replace("_", "-")
    assert sorted(a.get_all()) == ["abc-abc-test.txt", "abc-test-test.txt"]

    # doesn't work because its not sorted
    assert a.name_iterate(4, "  ")
    assert sorted(a.get_all()) == ["0001  abc-abc-test.txt", "0002  abc-test-test.txt"]

    assert a.name_replace_spaces("_")
    assert sorted(a.get_all()) == ["0001__abc-abc-test.txt", "0002__abc-test-test.txt"]

