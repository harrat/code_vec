def test_should_build_script_contains_right_statement(testassets_path,
                                                      partial_path,
                                                      generated_path,
                                                      expected_generated_scripts_dir):
    rm_rf(os.path.join(testassets_path, generated_path))
    invoke_generate(testassets_path, partial_path, generated_path)

    with open(os.path.join(expected_generated_scripts_dir,
                           "build_tf_bazel_builder_base.sh"), 'r') as f:
        script_content = f.read()
        expected_build_statement = \
            "docker build"\
            " --pull"\
            " -t registry.navercorp.com/mtengine/tf_bazel_builder_base:cuda10.0_cudnn7.4_python2.7.15_tf1.12.0"\
            " -f ../dockerfiles/registry.navercorp.com/mtengine/tf_bazel_builder_base/Dockerfile.cuda10.0_cudnn7.4_python2.7.15_tf1.12.0"\
            " ."

        assert expected_build_statement in script_content

    with open(os.path.join(expected_generated_scripts_dir,
                           "build_tf_bazel_builder.sh"), 'r') as f:
        script_content = f.read()
        expected_build_statement = \
            "docker build" \
            " -t registry.navercorp.com/mtengine/tf_bazel_builder:cuda10.0_cudnn7.4_python2.7.15_tf1.12.0_capability3.5_7.0_xla" \
            " -f ../dockerfiles/registry.navercorp.com/mtengine/tf_bazel_builder/Dockerfile.cuda10.0_cudnn7.4_python2.7.15_tf1.12.0_capability3.5_7.0_xla" \
            " ."
