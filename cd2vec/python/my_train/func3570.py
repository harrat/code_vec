def test_rvalue_material(self):
        self.idf.initreadtxt(single_layer)
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        expected = m.Thickness / m.Conductivity
        assert m.rvalue == expected
        assert m.rvalue == 0.2
