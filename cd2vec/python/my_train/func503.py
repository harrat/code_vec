def test_describe_process_group(self):
        self.create_default_experiment()
        cluster = Cluster.new('tmux', server_name=_TEST_SERVER)

        with self.assertRaises(ValueError):
            cluster.describe_process_group('bad_exp', 'group')
            cluster.describe_process_group('exp', 'bad_group')
        group_dict = cluster.describe_process_group('exp', 'group')
        self.assertDictEqual(group_dict,
                {
                    'hello': {
                        'status': 'live'
                    }
                }
        )
        group_dict = cluster.describe_process_group('exp', None)
        self.assertDictEqual(group_dict,
                    {
                        'alone': {
                            'status': 'live'
                        },
                    }
        )
