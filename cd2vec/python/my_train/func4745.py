def test_invalid(self):
        with self.assertRaises(ValueError):
            get_format('gz').open_file(Path('foo'), 'n')
