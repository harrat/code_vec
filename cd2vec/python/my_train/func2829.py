def test_email_works(tmpdir):
    '''
    This is a basic test of the email server working OK.

    '''

    email_server, maildir = generate_email_server(tmpdir)
    email_server.start()
    time.sleep(3.0)

    # send a test email
    email_sent = actions.send_email(
        'test@test.org',
        'Hello',
        'this is a test',
        ['test@test.org'],
        '127.0.0.1',
        None,
        None,
        'salt',
        port=2587
    )

    assert email_sent is True

    # check if it was received
    mailbox = Maildir(maildir)

    for _, message in mailbox.items():

        if message['Subject'] == 'Hello':
            assert message['From'] == 'test@test.org'
            assert message['To'] == 'test@test.org'
            assert 'this is a test' in message.as_string()

    email_server.stop()

