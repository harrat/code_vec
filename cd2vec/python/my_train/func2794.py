def test_uniform_color(clean):
    shutil.copyfile('tests/data/config_2.ini', clean)
    from pdir.format import doc_color, category_color, attribute_color, comma

    assert doc_color == COLORS['white']
    assert category_color == COLORS['white']
    assert comma == '\033[0;37m, \033[0m'
    assert attribute_color == COLORS['white']

