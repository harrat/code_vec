def test_true_cycle_graph(self):
        self.graph.add_vertex("c")
        self.graph.add_edge("b", "c", 2)
        self.graph.add_edge("c", "a", 3)
        assert self.graph.has_cycle() is True
        assert self.graph.has_loop() is False
