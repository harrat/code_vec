@pytest.mark.parametrize('error_type', [ImportError, TypeError])
def test_ignore_errors_in_start(error_type):
    with assert_module_with_raised_error(error_type):
        freezer = freeze_time(datetime.datetime(2019, 1, 11, 9, 34))
