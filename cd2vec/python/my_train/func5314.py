def test_set_attribute_with_multivalued_attribute(self):
        """
        Test that a KmipError is raised when attempting to set the value of
        a multivalued attribute.
        """
        e = engine.KmipEngine()
        e._protocol_version = contents.ProtocolVersion(2, 0)
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        args = (
            payloads.SetAttributeRequestPayload(
                unique_identifier="1",
                new_attribute=objects.NewAttribute(
                    attribute=primitives.TextString(
                        value="New Name",
                        tag=enums.Tags.NAME
                    )
                )
            ),
        )

        self.assertRaisesRegex(
            exceptions.KmipError,
            "The 'Name' attribute is multi-valued. Multi-valued attributes "
            "cannot be set with the SetAttribute operation.",
            e._process_set_attribute,
            *args
        )
