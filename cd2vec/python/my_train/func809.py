def test_prompt_symbol_style_is_danger_color_when_last_command_failed():
    assert prompt_symbol.style(last_command_status=constants.FAIL) == colors.style('danger')

