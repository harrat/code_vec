@pytest.mark.core
def testConfigSetTreeColWidths(nwTemp,nwRef):
    tmpConf = path.join(nwTemp,"novelwriter.conf")
    refConf = path.join(nwRef, "novelwriter.conf")
    assert theConf.setTreeColWidths([0, 0, 0])
    assert theConf.confChanged
    assert theConf.setTreeColWidths([120, 30, 50])
    assert theConf.setProjColWidths([140, 55, 140])
    assert theConf.saveConfig()
    assert cmpFiles(tmpConf, refConf, [2])
    assert not theConf.confChanged

@pytest.mark.core
def testConfigSetPanePos(nwTemp,nwRef):
    tmpConf = path.join(nwTemp,"novelwriter.conf")
    refConf = path.join(nwRef, "novelwriter.conf")
    assert theConf.setMainPanePos([0, 0])
    assert theConf.confChanged
    assert theConf.setMainPanePos([300, 800])
    assert theConf.setDocPanePos([400, 400])
    assert theConf.setViewPanePos([500, 150])
    assert theConf.setOutlinePanePos([500, 150])
    assert theConf.saveConfig()
    assert cmpFiles(tmpConf, refConf, [2])
    assert not theConf.confChanged
