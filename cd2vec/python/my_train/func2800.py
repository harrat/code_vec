def test_deprecated_expression_serialization(valid_deprecated_expression_predictor_data):
    """Ensure that a serialized DeprecatedExpressionPredictor looks sane."""
    predictor = DeprecatedExpressionPredictor.build(valid_deprecated_expression_predictor_data)
    serialized = predictor.dump()
    serialized['id'] = valid_deprecated_expression_predictor_data['id']
    assert serialized == valid_serialization_output(valid_deprecated_expression_predictor_data)

