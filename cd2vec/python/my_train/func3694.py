@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'yaml')
    def test_cli_import_reference_002(snippy):
        """Import all reference resources.

        Import all references from yaml file. File name and format are extracted
        from command line ``--file`` option. In this case the content category is
        explicitly defined from command line.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '--scat', 'reference', '-f', './all-references.yaml'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './all-references.yaml', mode='r', encoding='utf-8')
