def test_expire(self) -> None:
        with tempfile.NamedTemporaryFile() as f:
            cachestore = SQLiteCacheBackend(f.name)
            cachestore.save(b'1', b'2', expire_seconds=1)
            self.assertEqual(cachestore.load(b'1'), b'2')
            time.sleep(2)
            self.assertEqual(cachestore.load(b'1'), None)
