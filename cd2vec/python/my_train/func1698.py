@given(x=forecast_input(50))
    def test_fit_predict(self, x):
        df, horizon, _ = x
        model = Naive(horizon=horizon)
        model.fit(df)
        y_pred = model.predict()
        assert y_pred.shape == (horizon, horizon)
        res = np.broadcast_to(df.iloc[-horizon:], (horizon, horizon))
        y_cols = ["y_" + str(x + 1) for x in range(horizon)]
        expected_df = pd.DataFrame(res, index=model.X_test_.index, columns=y_cols)
        testing.assert_frame_equal(y_pred, expected_df)
