def test_circulant_precond_noncart(self):
        nc = 4
        n = 10
        shape = [nc, n]
        mps = np.ones(shape, dtype=np.complex)
        mps /= np.linalg.norm(mps, axis=0, keepdims=True)
        coord = sp.randn([n, 1], dtype=np.float)

        A = linop.Sense(mps, coord=coord)
        F = sp.linop.FFT([n])

        p_expected = np.zeros(n, np.complex)
        for i in range(n):
            x = np.zeros(n, np.complex)
            x[i] = 1.0
            p_expected[i] = 1 / F(A.H(A(F.H(x))))[i]

        p = precond.circulant_precond(mps, coord=coord)
        npt.assert_allclose(p, p_expected, atol=1e-1, rtol=1e-1)
