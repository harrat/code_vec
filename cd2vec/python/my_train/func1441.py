def test_set_querystring_overrides_filters(self):
        self.client.set_querystring_overrides('')  # Empty string to filter a querystring (not None)

        self._make_request('https://httpbin.org/?foo=bar&spam=eggs')

        last_request = self.client.get_last_request()

        query = urlsplit(last_request['url'])[3]
        self.assertEqual('', query)
