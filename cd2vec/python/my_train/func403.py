def test_extract_start(self):
		e = extract_monetary_amount({
			'start_price': '8.5',
			'currency': 'pounds'
		})
		self.assertEqual(e.value, 8.5)
		c = e.currency
		self.assertEqual(e.classified_as[0]._label, 'Starting Price')
		self.assertEqual(e.currency._label, 'British Pounds')
