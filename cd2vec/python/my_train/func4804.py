def test_markets_endpoints():
    ## Testing getting one market
    market = cryptowatch.markets.get("kraken:btcusd")
    # Test market object structure
    assert hasattr(market, "market")
    assert hasattr(market.market, "price")
    assert hasattr(market.market, "volume")
    assert hasattr(market.market, "volume_quote")
    assert hasattr(market.market.price, "change")
    assert hasattr(market.market.price, "change_absolute")
    assert hasattr(market.market.price, "last")
    assert hasattr(market.market.price, "high")
    assert hasattr(market.market.price, "low")
    # test market market values
    assert market.market.price.last >= 0
    assert market.market.price.low <= market.market.price.high
    # Testing listing all markets on all exchange
    markets = cryptowatch.markets.list()
    assert hasattr(markets, "markets")
    assert type(markets.markets) == type(list())
    assert type(markets.markets[0]) == cryptowatch.resources.markets.MarketResource
    # Testing lst
    kraken_markets = cryptowatch.markets.list("kraken")
    assert len(kraken_markets.markets) < len(markets.markets)
    # This should raise an APIResourceNotFoundError Exception
    with pytest.raises(cryptowatch.errors.APIResourceNotFoundError):
        cryptowatch.markets.get("marketthatdoesntexists:shitcoinusd")
def test_markets_endpoints():
    ## Testing getting one market
    candles = cryptowatch.markets.get("kraken:btcusd", ohlc=True)
    # Test candles object structure
    assert hasattr(candles, "of_1m")
    assert hasattr(candles, "of_3m")
    assert hasattr(candles, "of_5m")
    assert hasattr(candles, "of_15m")
    assert hasattr(candles, "of_30m")
    assert hasattr(candles, "of_1h")
    assert hasattr(candles, "of_2h")
    assert hasattr(candles, "of_4h")
    assert hasattr(candles, "of_6h")
    assert hasattr(candles, "of_12h")
    assert hasattr(candles, "of_1d")
    assert hasattr(candles, "of_1w")
    assert hasattr(candles, "of_1w_monday")
    # test candles type
    assert type(candles.of_1m) == type(list())
    assert type(candles.of_1w) == type(list())
    assert type(candles.of_1w_monday) == type(list())
    # This should raise an APIResourceNotFoundError Exception
    with pytest.raises(cryptowatch.errors.APIResourceNotFoundError):
        cryptowatch.markets.get("candlesthatdoesntexists:shitcoinusd", ohlc=True)
