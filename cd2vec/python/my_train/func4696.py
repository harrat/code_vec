def test_fifo(self):
        with TempDir() as temp:
            with self.assertRaises(Exception):
                _ = temp.make_fifo(contents='foo')
            path = temp.make_fifo()
            p = subprocess.Popen('echo foo > {}'.format(path), shell=True)
            with open(path, 'rt') as i:
                assert i.read() == 'foo\n'
            p.communicate()
