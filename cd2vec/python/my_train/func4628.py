def test_valid_params(self):
        text_processor = TextPreprocessor(spacy_model_id='es', remove_stop_words=True,
                                          lemmatize=True, additional_pipes=[pipe_sample])
        text_processor.fit(self.sample_en)
        transformed_text = text_processor.transform(self.sample_en)
        self.assertEqual(len(self.sample_en), len(transformed_text))
        for doc in transformed_text:
            self.assertTrue(len(doc) != 0)
