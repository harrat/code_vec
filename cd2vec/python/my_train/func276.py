def test_shapely_conversion(self):
        for i, row in self.all.iterrows():
            name, answer, test, degenerate = row
            converted = parse_polygon(dict(coordinates=test))
            approx = answer.almost_equals(converted)
            exact = answer.equals(converted)
            self.assertTrue(
                approx or exact, msg="Conversion fails on test shape {}".format(name)
            )
            if degenerate is not None:
                converted2 = parse_polygon(dict(coordinates=degenerate))
                approx = answer.almost_equals(converted2)
                exact = answer.equals(converted2)
                self.assertTrue(
                    approx or exact,
                    msg="Conversion fails on degenerate test shape " "{}".format(name),
                )
