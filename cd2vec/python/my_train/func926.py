def test_variance_2d():
    x = np.linspace(-3, 3, 400)
    X, Y = np.meshgrid(x, x)
    r2 = X ** 2 + Y ** 2
    P = np.ones_like(r2)
    P += np.random.normal(scale=1, size=(len(x), len(x)))
    ave, coord, var = angular_average(P, np.sqrt(r2), bins=np.linspace(0,x.max(), 20), get_variance=True)
    print(np.diff(var))
    assert np.all(np.diff(var)<=0)

