def test_relationship_types_invalid_target(self):
        relationship = copy.deepcopy(self.valid_relationship)
        relationship['target_ref'] = "report--af0976b2-e8f3-4646-8026-1cf4d0ce4d8a"
        self.assertFalseWithOptions(relationship)
