def test_issue1062_issue1097(self):
        # Must not be used by any other tests
        assert "nanometer" not in ureg._units
        for i in range(5):
            ctx = Context.from_lines(["@context _", "cal = 4 J"])
            with ureg.context("sp", ctx):
                q = ureg.Quantity(1, "nm")
                q.to("J")
