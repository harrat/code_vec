def test_agent() -> None:
    """Test that the agent starts and runs its task three times."""

    # clear previous data
    datfile = f'{DemoAgent.pid_dir}/{DemoAgent.name}.dat'
    if os.path.exists(datfile):
        os.remove(datfile)

    # start the daemon
    log.debug(f'starting agent')
    p = Process(target=DemoAgent.test, args=('start', ))
    p.start(); p.join()

    # verify exists and scrape PID
    pidfile = f'{DemoAgent.pid_dir}/{DemoAgent.name}.pid'
    with open(pidfile, mode='r') as f:
        PID = int(f.read().strip())
        log.debug(f'agent had pid={PID}')

    # verify that daemon process is running
    check_call(['ps', str(PID)])  # raises CalledProcessError if `PID` invalid

    # allow for at least three tasks to execute
    time.sleep(3)

    log.debug(f'stopping agent')
    p = Process(target=DemoAgent.test, args=('stop', ))
    p.start(); p.join()

    assert not os.path.exists(pidfile)
    with pytest.raises(CalledProcessError):
        check_call(['ps', str(PID)])

    # check data from task
    assert os.path.exists(datfile)
    with open(datfile, mode='r') as f:
        lines = [line.strip() for line in f.readlines()]

    assert len(lines) >= 3
    assert all(line == 'task complete' for line in lines)
