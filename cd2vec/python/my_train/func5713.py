@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'import-netcat')
    def test_cli_search_snippet_014(snippy, capsys):
        """Search snippets with ``stag`` option.

        Search snippets from tag field. The match is made from one snippet.
        """

        output = (
            '1. Test if specific port is open @linux [f3fd167c64b6f97e]',
            '',
            '   $ nc -v 10.183.19.189 443',
            '   $ nmap 10.183.19.189',
            '',
            '   # linux,netcat,networking,port',
            '   > https://www.commandlinux.com/man-page/man1/nc.1.html',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--stag', 'netcat', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
