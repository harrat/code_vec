def test_get_altered_files(git_dir):
    git_dir.chdir()
    git_dir.join("dummy").write("dummy2\n")
    git_dir.join("new").write("newfile\n")
    subprocess.call(["git", "add", "dummy", "new"])
    files = set(dzonegit.get_altered_files("HEAD", "AM"))
    assert files == {Path("dummy"), Path("new")}
    # Refers to test_check_whitespace_errors
    files = set(dzonegit.get_altered_files("HEAD~", "D", "HEAD"))
    assert files == {Path("whitespace")}
    subprocess.call(["git", "checkout", "-f", "HEAD"])
    assert set(dzonegit.get_altered_files("HEAD", "AM")) == set()

