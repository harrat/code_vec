def test_open_vocab_format_underscore(self):
        malware = copy.deepcopy(self.valid_malware)
        malware['labels'] += "ransom_ware"
        self.assertFalseWithOptions(malware,
                                    disabled='malware-label')
