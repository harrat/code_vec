def test_load_file_caches_file_load(self, monkeypatch, http_folder):
        with monkeypatch.context() as ctx:
            http_folder.load_file(file_name)
            http_folder.load_file(file_name)

        loaders[file_name].assert_called_once_with(http_folder.local_dir / file_name)
