def test_validate():
    """
    SCENARIO_VALID should validate
    """
    runner = CliRunner()
    result = runner.invoke(cli, ["validate", SCENARIO_VALID["file"]])
    if result.output != "":
        logging.error(result.output)
    assert result.exit_code == 0

