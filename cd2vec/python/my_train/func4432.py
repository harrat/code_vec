def test_nbless_three_cells(tmp_path: Path) -> None:
    """Run ``nbless`` to create and execute a 3-cell notebook file."""
    files = make_files(tmp_path)
    cells = nbless(files).cells
    assert [c.cell_type for c in cells] == ["markdown", "code", "markdown"]
    for cell, tempfile in zip(cells, files):
        assert cell.source == Path(tempfile).read_text()

