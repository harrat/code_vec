def test_get_config_from_std_in(self):
    """test_get_config_from_std_in verifies the IQConfig class"""
    testinput = """test@me.com
password
http://localhost:8070/"""
    sys.stdin = io.StringIO(testinput)
    result = self.func.get_config_from_std_in()
    self.assertEqual(result, True)
    # reread stored config
    results = self.func.get_config_from_file(".iq-server-config")
    self.assertEqual(results["Username"], "test@me.com")
    self.assertEqual(results["Token"], "password")
    self.assertEqual(results["Server"], "http://localhost:8070")
