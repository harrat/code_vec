@pytest.mark.vcr()
def test_workbench_export_filter_type_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.export(filter_type=1)
