def test_existing_file():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open('hello.txt', 'w') as f:
            f.write('Hello World!')
        thread = threading.Thread(target=runner.invoke, args=[localshare.download, ["hello.txt"]])
        thread.start()
        result = runner.invoke(localshare.share, ['hello.txt'])
        assert result.exit_code == 0
        assert 'Sharing hello.txt' in result.output
