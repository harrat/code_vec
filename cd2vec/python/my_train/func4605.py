def test_opera_driver_manager_with_wrong_version():
    with pytest.raises(ValueError) as ex:
        driver_path = OperaDriverManager("0.2").install()
        ff = webdriver.Opera(executable_path=driver_path)
        ff.quit()
    assert "There is no such driver by url " \
           "https://api.github.com/repos/operasoftware/operachromiumdriver/" \
           "releases/tags/0.2" in ex.value.args[0]
