def test_get_squad_v1_twice(self):
        get_squad(version=1)
        with mock.patch('lineflow.datasets.squad.pickle', autospec=True) as mock_pickle:
            get_squad(version=1)
        mock_pickle.dump.assert_not_called()
        self.assertEqual(mock_pickle.load.call_count, 1)
