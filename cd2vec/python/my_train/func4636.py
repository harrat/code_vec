def test_activate_on_static_object(self):
        """
        Test that the right error is generated when an activation request is
        received for an object that cannot be activated.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        managed_object = pie_objects.OpaqueObject(
            b'',
            enums.OpaqueDataType.NONE
        )
        e._data_session.add(managed_object)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        object_id = str(managed_object.unique_identifier)

        # Test by specifying the ID of the object to activate.
        payload = payloads.ActivateRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(object_id)
        )

        args = (payload,)
        name = enums.ObjectType.OPAQUE_DATA.name
        regex = "An {0} object has no state and cannot be activated.".format(
            ''.join(
                [x.capitalize() for x in name.split('_')]
            )
        )
        self.assertRaisesRegex(
            exceptions.IllegalOperation,
            regex,
            e._process_activate,
            *args
        )
