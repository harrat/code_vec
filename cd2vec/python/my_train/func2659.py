def test_threading(self, rnt):
        prev_time = time.time()
        rnt.new_task(sleepy_task).run(every=0.1, times=2).result()
        time_elapsed = time.time() - prev_time
        is_ok = (time_elapsed < 0.51) and (time_elapsed > 0.2)
        assert is_ok is True
