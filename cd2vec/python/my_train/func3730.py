def test_create_user():
    '''
    This runs through various iterations of creating a user.

    '''
    try:
        os.remove('test-creation.authdb.sqlite')
    except Exception:
        pass
    try:
        os.remove('test-creation.authdb.sqlite-shm')
    except Exception:
        pass
    try:
        os.remove('test-creation.authdb.sqlite-wal')
    except Exception:
        pass

    get_test_authdb()

    # 1. dumb password
    payload = {'full_name':'Test User',
               'email':'testuser@test.org',
               'password':'password',
               'reqid':1,
               'pii_salt':'super-random-salt'}
    user_created = actions.create_new_user(
        payload,
        override_authdb_path='sqlite:///test-creation.authdb.sqlite'
    )
    assert user_created['success'] is False
    assert user_created['user_email'] == 'testuser@test.org'
    assert user_created['user_id'] is None
    assert user_created['send_verification'] is False
    assert ('Your password is too short. It must have at least 12 characters.'
            in user_created['messages'])
    assert ('Your password is too similar to either '
            'the domain name of this server or your '
            'own name or email address.' in user_created['messages'])
    assert ('Your password is not complex enough. '
            'One or more characters appear appear too frequently.'
            in user_created['messages'])
    assert ('Your password is on the list of the most common '
            'passwords and is vulnerable to guessing.'
            in user_created['messages'])

    # 2. all numeric password
    payload = {'full_name':'Test User',
               'email':'testuser@test.org',
               'password':'239420349823904802398402375025',
               'reqid':1,
               'pii_salt':'super-random-salt'}
    user_created = actions.create_new_user(
        payload,
        override_authdb_path='sqlite:///test-creation.authdb.sqlite'
    )
    assert user_created['success'] is False
    assert user_created['user_email'] == 'testuser@test.org'
    assert user_created['user_id'] is None
    assert user_created['send_verification'] is False
    assert ('Your password cannot be all numbers.' in user_created['messages'])

    # 3a. password ~= email address
    payload = {'full_name': 'Test User',
               'email':'testuser@test.org',
               'password':'testuser',
               'reqid':1,
               'pii_salt':'super-random-salt'}
    user_created = actions.create_new_user(
        payload,
        override_authdb_path='sqlite:///test-creation.authdb.sqlite'
    )
    assert user_created['success'] is False
    assert user_created['user_email'] == 'testuser@test.org'
    assert user_created['user_id'] is None
    assert user_created['send_verification'] is False
    assert ('Your password is not complex enough. '
            'One or more characters appear appear too frequently.'
            in user_created['messages'])
    assert ('Your password is too similar to either '
            'the domain name of this server or your '
            'own name or email address.' in user_created['messages'])

    # 3b. password ~= full name
    payload = {'full_name': 'Test User',
               'email':'testuser@test.org',
               'password':'TestUser123',
               'reqid':1,
               'pii_salt':'super-random-salt'}
    user_created = actions.create_new_user(
        payload,
        override_authdb_path='sqlite:///test-creation.authdb.sqlite'
    )
    assert user_created['success'] is False
    assert user_created['user_email'] == 'testuser@test.org'
    assert user_created['user_id'] is None
    assert user_created['send_verification'] is False
    assert ('Your password is too similar to either '
            'the domain name of this server or your '
            'own name or email address.' in user_created['messages'])

    # 4. password is OK
    payload = {'full_name': 'Test User',
               'email':'testuser@test.org',
               'password':'aROwQin9L8nNtPTEMLXd',
               'reqid':1,
               'pii_salt':'super-random-salt'}
    user_created = actions.create_new_user(
        payload,
        override_authdb_path='sqlite:///test-creation.authdb.sqlite'
    )
    assert user_created['success'] is True
    assert user_created['user_email'] == 'testuser@test.org'
    assert user_created['user_id'] == 4
    assert user_created['send_verification'] is True
    assert ('User account created. Please verify your email address to log in.'
            in user_created['messages'])

    # 5. try to create a new user with an existing email address
    payload = {'full_name': 'Test User',
               'email':'testuser@test.org',
               'password':'aROwQin9L8nNtPTEMLXd',
               'reqid':1,
               'pii_salt':'super-random-salt'}
    user_created = actions.create_new_user(
        payload,
        override_authdb_path='sqlite:///test-creation.authdb.sqlite'
    )
    assert user_created['success'] is False
    assert user_created['user_email'] == 'testuser@test.org'
    assert user_created['user_id'] == 4

    # we should not send a verification email because the user already has an
    # account or if the account is not active yet, the last verification email
    # was sent less than 24 hours ago
    assert user_created['send_verification'] is False
    assert ('User account created. Please verify your email address to log in.'
            in user_created['messages'])

    try:
        os.remove('test-creation.authdb.sqlite')
    except Exception:
        pass
    try:
        os.remove('test-creation.authdb.sqlite-shm')
    except Exception:
        pass
    try:
        os.remove('test-creation.authdb.sqlite-wal')
    except Exception:
        pass
