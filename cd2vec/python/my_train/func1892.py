def test_read_profile_not_yours(self):
        in_profile = Profile(display_name_first='Ming', display_name_last='Xiao')
        back_profile = self.member.set_profile(in_profile)
        other_member = self.client.create_member(utils.generate_alias())
        out_profile = other_member.get_profile(self.member.member_id)
        assert in_profile == back_profile == out_profile
