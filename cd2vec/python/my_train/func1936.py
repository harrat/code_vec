@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'import-kafka', 'import-pytest')
    def test_api_search_groups_001(server):
        """Get unique content based on ``groups`` attribute.

        Send GET /groups to get unique groups from all content categories.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '82'
        }
        expect_body = {
            'data': {
                'type': 'groups',
                'attributes': {
                    'groups': {
                        'docker': 3,
                        'python': 1
                    }
                }
            }
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/groups',
            headers={'accept': 'application/vnd.api+json'})
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
