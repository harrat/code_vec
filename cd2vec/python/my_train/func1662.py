def test_derive_key_invalid_derivation_type(self):
        """
        Test that the right error is thrown when an invalid derivation type
        is provided with a DeriveKey request.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        payload = payloads.DeriveKeyRequestPayload(
            object_type=enums.ObjectType.CERTIFICATE
        )

        args = (payload, )
        self.assertRaisesRegex(
            exceptions.InvalidField,
            "Key derivation can only generate a SymmetricKey or SecretData "
            "object.",
            e._process_derive_key,
            *args
        )
