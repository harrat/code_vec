def test_loads_v2_each_split(self):
        train = Squad(split='train', version=2)
        self.assertEqual(len(train), 130_319)
        dev = Squad(split='dev', version=2)
        self.assertEqual(len(dev), 11_873)
