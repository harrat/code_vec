@pytest.mark.vcr()
def test_workbench_export_format_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.export(format=1234)
