def test_unsupported_get_resource(self):
        """Test GETing a resource for an endpoint that doesn't support it."""
        self.get_response('/playlists', 403, False)
