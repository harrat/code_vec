def test_defined_hour():
    progress_bar = ProgressBarWget(2000)

    assert ' 0% [                  ] 0           --.-KiB/s              ' == str(progress_bar)

    eta._NOW = lambda: 1411868722.0
    progress_bar.numerator = 1
    assert ' 0% [                  ] 1           --.-KiB/s              ' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 2
    assert ' 0% [                  ] 2             0.50B/s  eta 1h 6m   ' == str(progress_bar)

