@staticmethod
    @pytest.mark.usefixtures('import-gitlog', 'update-regexp-utc', 'caller')
    def test_api_update_reference_007(server):
        """Update one reference with PUT request.

        Try to update resource ``uuid`` attribute with PUT /references/{id}.
        This must not work because the ``uuid`` attribute cannot be changed by
        client.
        """

        storage = {
            'data': [
                Storage.gitlog
            ]
        }
        request_body = {
            'data': {
                'type': 'reference',
                'attributes': {
                    'links': storage['data'][0]['links'],
                    'uuid': '11111111-1111-1111-1111-111111111111'
                }
            }
        }
        expect_headers_p3 = {'content-type': 'application/vnd.api+json; charset=UTF-8', 'content-length': '1120'}
        expect_headers_p2 = {'content-type': 'application/vnd.api+json; charset=UTF-8', 'content-length': '1159'}
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '400',
                'statusString': '400 Bad Request',
                'module': 'snippy.testing.testing:123',
                'title': 'json media validation failed'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_put(
            path='/api/snippy/rest/references/5c2071094dbfaa33',
            headers={'accept': 'application/vnd.api+json; charset=UTF-8'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_400
        assert result.headers in (expect_headers_p2, expect_headers_p3)
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
