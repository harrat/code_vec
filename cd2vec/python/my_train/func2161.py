@pytest.mark.parametrize('method', [
    'average',
    'mean',
    'max_intensity',
    'max_variance',
    'pc_loading',
    'corr_intensity',
    'corr_variance',
    'diff_stability',
])
def test_collapse_probes(testfiles, method):
    # we've aleady tested the underlying methods so here we just want to do
    # some smoke tests to make sure the function returns what we expected
    # regardless of the provided method
    out = probes_.collapse_probes(flatten_dict(testfiles, 'microarray'),
                                  flatten_dict(testfiles, 'annotation'),
                                  first_entry(testfiles, 'probes'),
                                  method=method)

    out = list(out.values())
    assert len(out) == 2  # number of donors
    assert np.all([len(exp) == n_samp for exp, n_samp in zip(out, [363, 470])])
    assert np.all([len(exp.columns) == 29131 for exp in out])

