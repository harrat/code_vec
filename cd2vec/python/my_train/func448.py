def test_reannotate_probes(testfiles):
    # set up a few useful variables
    probe_file = first_entry(testfiles, 'probes')

    # should work with either a filename _or_ a dataframe
    reannot = probes_.reannotate_probes(probe_file)
    probe_df = abagen.io.read_probes(probe_file)
    pd.testing.assert_frame_equal(reannot, probes_.reannotate_probes(probe_df))

    # expected output
    cols = ['probe_name', 'gene_symbol', 'entrez_id']
    assert np.all(reannot.columns == cols)
    assert reannot.index.name == 'probe_id'
    assert reannot.shape == (45821, 3)

