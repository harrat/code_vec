def test_sanity_of_checks(run_reframe, tmp_path, logfile):
    # This test will effectively load all the tests in the checks path and
    # will force a syntactic and runtime check at least for the constructor
    # of the checks
    returncode, *_ = run_reframe(
        action='list',
        more_options=['--save-log-files'],
        checkpath=[]
    )
    assert returncode == 0
    os.path.exists(tmp_path / 'output' / logfile)

