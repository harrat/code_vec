@staticmethod
    def test_flights():
        fr = flightradar24.Api()
        flights = fr.get_flights('THY')
        assert flights['full_count'] is not None
        assert flights['full_count'] > 100  # Expect more than 100 airports
