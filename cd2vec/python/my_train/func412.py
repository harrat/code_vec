def test_invocation_support_error_set_to_root_span(handler_with_user_error, mock_context, mock_event):
    thundra, handler = handler_with_user_error

    trace_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, TracePlugin):
            trace_plugin = plugin

    handler(mock_event, mock_context)

    assert trace_plugin.root_span.get_tag('error') is True
    assert trace_plugin.root_span.get_tag('error.kind') == 'Exception'
    assert trace_plugin.root_span.get_tag('error.message') == 'test'

