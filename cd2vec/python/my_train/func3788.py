def test_set_param_overrides_filters_out_param(self):
        self.client.set_param_overrides({'foo': None})

        self._make_request('https://httpbin.org/?foo=bar&spam=eggs')

        last_request = self.client.get_last_request()

        query = urlsplit(last_request['url']).query
        self.assertEqual('spam=eggs', query)
