def test_masked_mfcc_no_explicit_run_vad(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        self.assertIsNotNone(audiofile.masked_mfcc)
