def test_new_checkbox_checked_list(self):
        md_file = MdUtils(file_name="Test_file", title="")
        md_file.new_checkbox_list(self.complex_items, checked=True)
        md_file.create_md_file()

        self.assertEqual(self.expected_list.replace('-', '- [x]'), MarkDownFile.read_file('Test_file.md'))
