def test_get_default_config_file_path(self):
        """Test getting default config file path."""
        real_home = os.environ.get("HOME")

        os.environ["HOME"] = "/some/test/value"
        expected_path = "/some/test/value/.config/lazy_pr.ini"
        assert get_default_config_file_path() == expected_path

        if real_home is not None:
            os.environ["HOME"] = real_home
