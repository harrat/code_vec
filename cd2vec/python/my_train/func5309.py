def test_open_for_write(s3_mock):
    s3 = boto3.resource('s3')
    s3.create_bucket(Bucket='test-bucket')
    bucket = s3.Bucket('test-bucket')
    assert sum(1 for _ in bucket.objects.all()) == 0

    path = S3Path('/test-bucket/directory/Test.test')
    file_obj = path.open(mode='bw')
    assert file_obj.writable()
    file_obj.write(b'test data\n')
    file_obj.writelines([b'test data'])

    assert sum(1 for _ in bucket.objects.all()) == 1

    object_summary = s3.ObjectSummary('test-bucket', 'directory/Test.test')
    streaming_body = object_summary.get()['Body']

    assert list(streaming_body.iter_lines()) == [
        b'test data',
        b'test data'
    ]
