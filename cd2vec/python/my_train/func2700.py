def test_read_chunked(self):
        self.assertListEqual([], list(read_bytes(Path('foobar'), errors=False)))
        path = self.root.make_file()
        with open(path, 'wt') as o:
            o.write("1234567890")
        chunks = list(read_bytes(path, 3))
        self.assertListEqual([b'123', b'456', b'789', b'0'], chunks)
