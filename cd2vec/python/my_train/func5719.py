def test_invalid_contents_subkey(self):
        lang_content = copy.deepcopy(self.valid_language_content)
        lang_content['contents']['es'] = {
            "a": "boo"
        }
        self.assertFalseWithOptions(lang_content)
