def test_fake_uses_real_when_ignored():
    real_time_before = time.time()
    with freeze_time('2012-01-14', ignore=['tests.fake_module']):
        real_time = fake_time_function()
    real_time_after = time.time()
    assert real_time_before <= real_time <= real_time_after

