def test_forcing_level_with_kwargs_by_level(propagation_logger, handler):

    msg1 = "TEST1"

    msg2 = "TEST2"

    propagation_logger.setLevel(logging.INFO)

    propagation_logger.force_level(logger_1=logging.DEBUG)

    additional_logger = logging.getLogger('logger_1')

    additional_logger.setLevel(logging.INFO)

    additional_logger.addHandler(handler)

    propagation_logger.debug(msg1)

    additional_logger.debug(msg2)

    record = handler.pop()

    assert record.msg == msg2

    assert record.levelname == logging.getLevelName(logging.DEBUG)

    with pytest.raises(IndexError):

        handler.pop()
def test_forcing_level_with_kwargs_by_level(propagation_logger, handler):

    msg1 = "TEST1"

    msg2 = "TEST2"

    propagation_logger.setLevel(logging.INFO)

    propagation_logger.force_level(logger_1="DEBUG")

    additional_logger = logging.getLogger('logger_1')

    additional_logger.setLevel(logging.INFO)

    additional_logger.addHandler(handler)

    propagation_logger.debug(msg1)

    additional_logger.debug(msg2)

    record = handler.pop()

    assert record.msg == msg2

    assert record.levelname == logging.getLevelName(logging.DEBUG)

    with pytest.raises(IndexError):

        handler.pop()
