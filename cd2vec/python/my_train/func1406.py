def test_process_communicate(self):
        with Process('cat', stdin=PIPE, stdout=PIPE, stderr=PIPE) as p:
            self.assertTupleEqual((b'foo\n', b''), p.communicate(b'foo\n'))
