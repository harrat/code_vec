def test_resolve_missing(self):
        with self.assertRaises(IOError):
            resolve_path(Path('foo'))
