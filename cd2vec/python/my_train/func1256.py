def test_parse(self):
        nc_string = "(net_class name description (add_net GND))"
        nc = NetClass.parse(nc_string)
        assert nc.name == 'name'
        assert nc.description == 'description'
        assert nc.nets == ['GND']
