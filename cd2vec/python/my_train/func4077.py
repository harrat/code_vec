@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_api_search_reference_003(server):
        """Search reference with GET.

        Send GET /references and search keywords from all attributes. The
        search query matches to two references but only one of them is returned
        because the limit parameter was set to one. In this case the sort is
        descending and the last match must be returned. The resulting
        attributes are limited to brief and category.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '217'
        }
        expect_body = {
            'meta': {
                'count': 1,
                'limit': 1,
                'offset': 0,
                'total': 2
            },
            'data': [{
                'type': 'reference',
                'id': Reference.REGEXP_UUID,
                'attributes': {field: Reference.REGEXP[field] for field in ['brief', 'category']}
            }]
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/references',
            headers={'accept': 'application/json'},
            query_string='sall=howto&limit=1&sort=-brief&fields=brief,category')
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
