def test_ground_survey():
    """
    Test if the sythetic ground survey returns the expected survey
    """
    # Expected region for the default data_region
    expected_region = (13.60833, 20.28333, -24.2, -17.50333)
    survey = ground_survey()
    assert set(survey.columns) == set(["longitude", "latitude", "height"])
    assert survey.longitude.size == 963
    npt.assert_allclose(survey.longitude.min(), expected_region[0])
    npt.assert_allclose(survey.longitude.max(), expected_region[1])
    npt.assert_allclose(survey.latitude.min(), expected_region[2])
    npt.assert_allclose(survey.latitude.max(), expected_region[3])
    npt.assert_allclose(survey.height.min(), 0.0)
    npt.assert_allclose(survey.height.max(), 2052.2)

