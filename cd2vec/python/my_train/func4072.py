def test_git_version_info(self):
        runner('git tag -a 1.0.0 -m "test message"')
        v = relic.git.git_version_info()
        assert isinstance(v, relic.git.GitVersion)
