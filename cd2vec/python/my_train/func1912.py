@pytest.mark.parametrize("modname", tuple(random_modname(i) for i in range(3))
                                    + NAMES_EXISTING)
def test_lazyload(modname):
    _check_lazy_loading(modname)

def test_presentload():
    import os.path
    mod = lazy_import.lazy_module("os")
    assert mod is os
    mod = lazy_import.lazy_module("os.path")
    assert mod is os.path
    mod = lazy_import.lazy_module("os.path", level="base")
    assert mod is os
    assert not isinstance(mod, lazy_import.LazyModule)
