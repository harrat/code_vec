@freeze_time('2014-01-20')
def test_local_alias(cli, config, entries_file):
    config.set('local_aliases', '_pingpong', '')

    entries_file.write("""20/01/2014
_pingpong 0800-0900 Play ping-pong
""")
    stdout = cli('status')

    assert line_in("_pingpong  ( 1.00)  Play ping-pong", stdout)

