def test_logged_condition_waited_for():
    cond = LoggedCondition('test', log_interval=15)
    progress = 0
    executed = []

    def wait_then_set_to(wait_to, set_to):
        nonlocal progress
        with cond.waited_for(lambda: progress == wait_to, 'progress to be %s', wait_to):
            executed.append('%s -> %s' % (progress, set_to))
            progress = set_to

    with concurrent(wait_then_set_to, 10, 20), concurrent(wait_then_set_to, 20, 30), concurrent(wait_then_set_to, 30, 40):
        with cond.notifying_all('setting progress to 10'):
            progress = 10

    assert executed == ['10 -> 20', '20 -> 30', '30 -> 40']

