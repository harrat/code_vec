def test_wtypes_list():
    apic = APIClient(ignore_env=True)
    res = apic.get_wtypes()
    assert len(res) > 0
    # len(res) == 0 is also correct in general. However, that would
    # imply that rerobots has no active workspace deployments, which
    # should be rare or never.
