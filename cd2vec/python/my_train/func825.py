def test_parent_item():
    """Test parent item is returned if present."""
    data = MOCK_DATA
    data["included"][0]["attributes"]["parent"] = MOCK_PARENT
    result = media.parse_data(data, MOCK_TITLE_ID, MOCK_REGION_NAME)
    assert result.name == MOCK_PARENT_NAME
    assert result.sku_id == MOCK_TITLE_ID
    assert result.game_type == MOCK_TYPE
    assert result.cover_art.split("/image")[0] == MOCK_PARENT_URL

