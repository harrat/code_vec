def test_space_list(self):
        cf = ConfigManager('cfitall')
        cf.set_default('test.list.withspaces', ['flenderson, toby', 'martin, angela'])
        self.assertEqual(cf.get('test.list.withcommas'), ['flenderson, toby', 'martin, angela'])
        os.environ['CFITALL__TEST__LIST'] = 'hello world    melting	 antarctica   broadway'
        cf.env_value_split_space = True
        self.assertEqual(cf.get('test.list'), ['hello', 'world', 'melting', 'antarctica', 'broadway'])
        cf.env_value_split_space = False
        self.assertEqual(cf.get('test.list'), 'hello world    melting	 antarctica   broadway')
