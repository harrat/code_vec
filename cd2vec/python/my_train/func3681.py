def test_b_decrypt(self):
       decrypt_file(self.file_name, self.password, self.salt)
       with open(self.file_name) as test_file:
          self.assertEqual(self.contents, test_file.read())
