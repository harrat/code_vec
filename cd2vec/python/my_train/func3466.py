@pytest.mark.parametrize('id,answer', [(1, '11'), (2, 42), (2, '42')])
def test_update_card(id: int, answer: str) -> None:
    card = spacedr.get_card_by_id(id)
    old_data = {'due_date': card.due_date, 'reviews_count': card.reviews_count}

    spacedr.update_card(card, answer)

    assert card.due_date != old_data['due_date']
    assert card.reviews_count == old_data['reviews_count'] + 1

