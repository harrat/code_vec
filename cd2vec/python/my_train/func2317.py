def test_default_plots_False_nocallback(self):
        agent = agents.PpoAgent("CartPole-v0")
        p = plot.Loss()
        c = agent._add_plot_callbacks([], False, [p])
        assert not p in c
