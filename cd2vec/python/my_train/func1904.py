def test_agentcontext_play(self):
        b = easyagents.backends.debug.DebugAgentFactory()
        a = b.create_agent(PpoAgent, ModelConfig(_stepcount_name))
        c = AgentContextTest.PlayCallback()
        pc = PlayContext()
        pc.num_episodes = 10
        pc.max_steps_per_episode = 10
        a.play(callbacks=[Fast(), c], play_context=pc)
        assert not c.train_called
        assert c.play_called
