def test_http_protocol_stripped_on_gethostbyname(self):
        with vcr.use_cassette("cassettes/TestTracer.yml") as cass:
            result = Tracer._ipaddr(cass.requests[1].uri)
            expected = Tracer._ipaddr(cass.requests[1].uri)
            self.assertEqual(result, expected)
