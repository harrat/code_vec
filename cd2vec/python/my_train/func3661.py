def test_render_index():
    le, all_entries = _get_last_entries(DB, 10)
    update_index(le)
    with open(os.path.join(CONFIG['output_to'], 'index.html')) as html_index:
        soup = BeautifulSoup(html_index.read(), 'html.parser')
        assert len(soup.find_all(class_='clearfix entry')) == 10
