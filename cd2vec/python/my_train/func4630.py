@mock.patch('wsstat.main.WSStatConsoleApplication')
@mock.patch('wsstat.main.WebsocketTestingClient')
def test_client_setup(console_class, client_class):
    wsstat_console()

@mock.patch('wsstat.gui.urwid.MainLoop')
def test_client_run(mainloop):
    client = WebsocketTestingClient(
        websocket_url='wss://testserver/',
        total_connections=1,
        max_connecting_sockets=1,
        setup_tasks=False
    )
    console = WSStatConsoleApplication(client)
