def test_build_deps_unknown_test(loader, exec_ctx):
    checks = loader.load_all()

    # Add some inexistent dependencies
    test0 = find_check('Test0', checks)
    for depkind in ('default', 'fully', 'by_env', 'exact'):
        test1 = find_check('Test1_' + depkind, checks)
        if depkind == 'default':
            test1.depends_on('TestX')
        elif depkind == 'exact':
            test1.depends_on('TestX', rfm.DEPEND_EXACT, {'e0': ['e0']})
        elif depkind == 'fully':
            test1.depends_on('TestX', rfm.DEPEND_FULLY)
        elif depkind == 'by_env':
            test1.depends_on('TestX', rfm.DEPEND_BY_ENV)

        with pytest.raises(DependencyError):
            dependency.build_deps(executors.generate_testcases(checks))
