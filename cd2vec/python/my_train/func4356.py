@mock.patch('google.cloud.pubsub_v1.PublisherClient')
    def test_default_client(self, mock_client):
        Publisher()
        expected = [mock.call()]
        self.assertEquals(mock_client.mock_calls, expected)
