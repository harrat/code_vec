def test_fetchmany():
    global cu
    cu.execute("delete from tests")
    cu.execute("insert into tests(id, name, text_field) values (18, 'test_fetchmany', 'foo')")
    cu.execute("select text_field from tests")
    res = cu.fetchmany(100)
    assert len(res) == 1
    res = cu.fetchmany(100)
    assert len(res) == 0

