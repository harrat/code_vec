def test_indices_and_numbers(self):
        locale.setlocale(locale.LC_ALL, '')
        gp = parser.GamryParser(filename='tests/cv_data.dta')
        gp.load()
        indices = gp.get_curve_indices()
        self.assertEqual(indices, (0, 1, 2, 3, 4))
        numbers = gp.get_curve_numbers()
        self.assertEqual(numbers, (1, 2, 3, 4, 5))
