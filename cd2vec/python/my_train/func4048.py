def test_load_shared_table(engine, session, caplog):
    """Two different models backed by the same table try to load the same hash key.
    They share the column "shared" but load the content differently
    """
    class FirstModel(BaseModel):
        class Meta:
            table_name = "SharedTable"
        id = Column(String, hash_key=True)
        range = Column(String, range_key=True)
        first = Column(String)
        as_date = Column(DateTime, dynamo_name="shared")

    class SecondModel(BaseModel):
        class Meta:
            table_name = "SharedTable"

        id = Column(String, hash_key=True)
        range = Column(String, range_key=True)
        second = Column(String)
        as_string = Column(String, dynamo_name="shared")
    engine.bind(BaseModel)

    id = "foo"
    range = "bar"
    now = datetime.datetime.now(datetime.timezone.utc)
    now_str = now.isoformat()
    session.load_items.return_value = {
        "SharedTable": [{
            "id": {"S": id},
            "range": {"S": range},
            "first": {"S": "first"},
            "second": {"S": "second"},
            "shared": {"S": now_str}}]
    }

    first = FirstModel(id=id, range=range)
    second = SecondModel(id=id, range=range)

    caplog.clear()
    engine.load(first, second)

    expected_first = FirstModel(id=id, range=range, first="first", as_date=now)
    expected_second = SecondModel(id=id, range=range, second="second", as_string=now_str)

    missing = object()
    for attr in (c.name for c in FirstModel.Meta.columns):
        assert getattr(first, attr, missing) == getattr(expected_first, attr, missing)
    for attr in (c.name for c in SecondModel.Meta.columns):
        assert getattr(second, attr, missing) == getattr(expected_second, attr, missing)
    assert not hasattr(first, "second")
    assert not hasattr(second, "first")

    assert caplog.record_tuples == [
        ("bloop.engine", logging.INFO, "successfully loaded 2 objects")
    ]
