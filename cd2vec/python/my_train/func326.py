def test_get_config_from_file(self):
    """test_config_object_saves_config_file ensures config objs are being written to file"""
    self.func.save_config_to_file(
        {"Username": "test@me.com", "Token": "password"},
        ".oss-index-config")

    results = self.func.get_config_from_file(".oss-index-config")
    self.assertEqual(results["Username"], "test@me.com")
    self.assertEqual(results["Token"], "password")
