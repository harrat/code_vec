@freeze_time('2014-01-21')
def test_post_push_fail(cli, entries_file):
    entries_file.write("""21/01/2014
post_push_fail     1  Repair coffee machine
alias_1     2  Repair coffee machine
""")
    stdout = cli('commit')

    assert 'Failed entries\n\npost_push_fail' in stdout

