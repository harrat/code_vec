def test_get_wrapped_key(self):
        """
        Test that a Get request for a wrapped key can be processed correctly.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        wrapping_key = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            128,
            (
                b'\x00\x01\x02\x03\x04\x05\x06\x07'
                b'\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F'
            ),
            [enums.CryptographicUsageMask.WRAP_KEY]
        )
        wrapping_key.state = enums.State.ACTIVE

        unwrapped_key = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            128,
            (
                b'\x00\x11\x22\x33\x44\x55\x66\x77'
                b'\x88\x99\xAA\xBB\xCC\xDD\xEE\xFF'
            ),
            [enums.CryptographicUsageMask.ENCRYPT]
        )

        e._data_session.add(wrapping_key)
        e._data_session.add(unwrapped_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        wrapping_key_uuid = str(wrapping_key.unique_identifier)
        unwrapped_key_uuid = str(unwrapped_key.unique_identifier)

        cryptographic_parameters = attributes.CryptographicParameters(
            block_cipher_mode=enums.BlockCipherMode.NIST_KEY_WRAP
        )
        payload = payloads.GetRequestPayload(
            unique_identifier=unwrapped_key_uuid,
            key_wrapping_specification=objects.KeyWrappingSpecification(
                wrapping_method=enums.WrappingMethod.ENCRYPT,
                encryption_key_information=objects.EncryptionKeyInformation(
                    unique_identifier=wrapping_key_uuid,
                    cryptographic_parameters=cryptographic_parameters
                ),
                encoding_option=enums.EncodingOption.NO_ENCODING
            )
        )

        response_payload = e._process_get(payload)

        e._logger.info.assert_any_call("Processing operation: Get")
        e._logger.info.assert_any_call(
            "Wrapping SymmetricKey 2 with SymmetricKey 1."
        )
        self.assertEqual(
            enums.ObjectType.SYMMETRIC_KEY,
            response_payload.object_type
        )
        self.assertEqual(
            unwrapped_key_uuid,
            response_payload.unique_identifier
        )
        self.assertIsInstance(
            response_payload.secret,
            secrets.SymmetricKey
        )
        self.assertEqual(
            (
                b'\x1F\xA6\x8B\x0A\x81\x12\xB4\x47'
                b'\xAE\xF3\x4B\xD8\xFB\x5A\x7B\x82'
                b'\x9D\x3E\x86\x23\x71\xD2\xCF\xE5'
            ),
            response_payload.secret.key_block.key_value.key_material.value
        )
        self.assertIsInstance(
            response_payload.secret.key_block.key_wrapping_data,
            objects.KeyWrappingData
        )
        k = response_payload.secret.key_block.key_wrapping_data
        self.assertEqual(
            enums.WrappingMethod.ENCRYPT,
            k.wrapping_method
        )
        self.assertIsInstance(
            k.encryption_key_information,
            objects.EncryptionKeyInformation
        )
        self.assertEqual(
            '1',
            k.encryption_key_information.unique_identifier
        )
        self.assertIsInstance(
            k.encryption_key_information.cryptographic_parameters,
            attributes.CryptographicParameters
        )
        c = k.encryption_key_information.cryptographic_parameters
        self.assertEqual(
            enums.BlockCipherMode.NIST_KEY_WRAP,
            c.block_cipher_mode
        )
        self.assertEqual(
            enums.EncodingOption.NO_ENCODING,
            k.encoding_option
        )
