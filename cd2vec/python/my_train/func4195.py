@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'export-time')
    def test_cli_export_solution_005(snippy):
        """Export all solutions.

        Export all solutions into defined text file with file extension
        'text'. File name and format are defined in command line.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Content.deepcopy(Solution.BEATS),
                Content.deepcopy(Solution.NGINX)
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '-f', './all-solutions.text'])
            assert cause == Cause.ALL_OK
            Content.assert_text(mock_file, './all-solutions.text', content)
