def test_size_calculator(self):
        # total = 23334 = 22.6796875k
        r1 = ruler.DirSizeRule("<= 22.6796875k")
        r2 = ruler.DirSizeRule("<= 22.6796874k")

        self.assertTrue(r1.match(self.dir))
        self.assertFalse(r2.match(self.dir))
