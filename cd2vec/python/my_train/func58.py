def test_positional_list(self):
        opts = self.get_opts('test', '1', '2', '3')
        self.assertEqual(opts.positional, 'test')
        self.assertCollectionEqual(opts.positional_list, [1, 2, 3])
