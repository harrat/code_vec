@pytest.mark.vcr()
def test_workbench_export_missing_chapters(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.export(format='html')
