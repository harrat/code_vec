def testCmdTestRTAllVariants(self, projects):

        self._checkFeatureRTAllVariants([
            ['test', '--with-tests', 'no', '--run-tests', 'all'],
            ['test', '--with-tests', 'yes', '--run-tests', 'all'],
            ['test', '--with-tests', 'yes', '--run-tests', 'on-changes'],
        ])
