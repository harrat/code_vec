def test_credit_created_from_person_object():
    person = Person(PERSON_ID)
    all_person_data = person.get_all()
    _, credit = person.cast[0]
    assert isinstance(credit, Credit)
    assert credit._person_data == person.data == all_person_data

