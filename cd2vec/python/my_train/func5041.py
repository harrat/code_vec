def it_can_disable_ssl_validation(self, response):
        h = RequestHandler('', verify=False)
        with patch('ubersmith.api.RequestHandler.session') as session:
            session.post.return_value = response
            h.process_request('uber.method_list')
            assert session.post.call_args[1]['verify'] is False
