def test_incorrect_cve_format_letter(self):
        vulnerability = copy.deepcopy(self.valid_vulnerability)
        ext_refs = vulnerability['external_references']
        ext_refs[0]['external_id'] = "CVE-2016-abc"
        self.assertFalseWithOptions(vulnerability)
