def test_getfieldnamesendswith():
    """py.test for getfieldnamesendswith"""
    idftxt = """PIPE:ADIABATIC,
        np2,                      !- Name
        np1_np2_node,             !- Inlet Node Name
        np2_outlet;               !- Outlet Node Name

    """
    tdata = (
        ("Inlet_Node_Name", ["Inlet_Node_Name"]),  # endswith, fieldnames
        ("Node_Name", ["Inlet_Node_Name", "Outlet_Node_Name"]),  # endswith, fieldnames
        (
            "Name",
            ["Name", "Inlet_Node_Name", "Outlet_Node_Name"],
        ),  # endswith, fieldnames
    )
    fhandle = StringIO(idftxt)
    idf = IDF(fhandle)
    idfobject = idf.idfobjects["PIPE:ADIABATIC"][0]
    for endswith, fieldnames in tdata:
        result = hvacbuilder.getfieldnamesendswith(idfobject, endswith)
        assert result == fieldnames

