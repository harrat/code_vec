def test_ripple_carry(self):
        q2 = qudot.QuState.init_from_state_list(["10"])
        q1 = qudot.QuState.init_from_state_list(["01"])
        r1 = qudot.QuState.init_from_state_list(["011"])
        self.assertEqual(r1, qulib.ripple_adder(q2, q1))

        q3 = qudot.QuState.init_from_state_list(["11"])
        r2 = qudot.QuState.init_from_state_list(["110"])
        self.assertEqual(r2, qulib.ripple_adder(q3, q3))
