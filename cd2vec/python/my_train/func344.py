def test_invalid_xticks(self):
        ''' Invalid xticks. '''
        with self.assertRaisesRegexp(ValueError, r'\[barchart\] .*xticks.*'):
            barchart.draw(self.axes, _data(), xticks=['x'])
