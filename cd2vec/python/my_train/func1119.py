def test_train(self):
        model_config = core.ModelConfig(_lineworld_name)
        tc = core.EpisodesTrainContext()
        reinforce_agent = tfagents.TfReinforceAgent(model_config=model_config)
        reinforce_agent.train(train_context=tc, callbacks=[duration.Fast(), log.Iteration()])
        assert tc.episodes_done_in_iteration == tc.num_episodes_per_iteration > 0
        assert tc.iterations_done_in_training == tc.num_iterations > 0
