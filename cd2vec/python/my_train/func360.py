def test_extension_registration_if_app_has_no_extensions(app):
    """Test that the constructor is able to register FlaskMultiRedis
    as an extension even if app has no extensions attribute."""
    delattr(app, 'extensions')
    redis = FlaskMultiRedis(app)
    assert hasattr(app, 'extensions')
    assert 'redis' in app.extensions
    assert app.extensions['redis'] == redis

