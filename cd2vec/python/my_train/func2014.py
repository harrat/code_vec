def test_get_relevant_policy_section_policy_missing(self):
        """
        Test that the lookup for a non-existent policy is handled correctly.
        """
        e = engine.KmipEngine()
        e._operation_policies = {}
        e._logger = mock.MagicMock()

        result = e.get_relevant_policy_section('invalid')

        e._logger.warning.assert_called_once_with(
            "The 'invalid' policy does not exist."
        )
        self.assertIsNone(result)
