def test_run_with_policy_overloading_reserved(self):
        """
        Test that the PolicyDirectoryMonitor can load policy files and track
        them properly, even when one policy can't be loaded properly.
        """
        m = monitor.PolicyDirectoryMonitor(
            self.tmp_dir,
            multiprocessing.Manager().dict()
        )
        m.logger = mock.MagicMock(logging.Logger)
        m.halt_trigger = mock.MagicMock(multiprocessing.synchronize.Event)
        m.halt_trigger.is_set.side_effect = [False, True]

        write_file(self.tmp_dir, "policy_3.json", POLICY_3)
        write_file(self.tmp_dir, "policy_4.json", POLICY_4)

        self.assertEqual({}, m.file_timestamps)
        self.assertEqual({}, m.policy_cache)
        self.assertEqual([], m.policy_files)
        self.assertEqual({}, m.policy_map)
        self.assertEqual([], m.policy_store.keys())

        m.run()

        m.logger.info.assert_any_call(
            "Starting up the operation policy file monitor."
        )
        m.logger.info.assert_any_call(
            "Loading policies for file: {}".format(
                os.path.join(self.tmp_dir, "policy_3.json")
            )
        )
        m.logger.info.assert_any_call("Loading policy: policy_B")
        m.logger.info.assert_any_call(
            "Loading policies for file: {}".format(
                os.path.join(self.tmp_dir, "policy_4.json")
            )
        )
        m.logger.info.assert_any_call("Loading policy: default")
        m.logger.warning.assert_any_call(
            "Policy 'default' overwrites a reserved policy and will be "
            "thrown out."
        )
        m.logger.info.assert_any_call(
            "Stopping the operation policy file monitor."
        )

        self.assertEqual(2, len(m.policy_files))
        path = os.path.join(self.tmp_dir, "policy_3.json")
        self.assertEqual(
            os.path.getmtime(path),
            m.file_timestamps.get(path, None)
        )
        self.assertIn(path, m.policy_files)
        self.assertEqual(path, m.policy_map.get("policy_B", None))

        path = os.path.join(self.tmp_dir, "policy_4.json")
        self.assertEqual(
            os.path.getmtime(path),
            m.file_timestamps.get(path, None)
        )
        self.assertIn(path, m.policy_files)

        self.assertEqual(
            {
                "policy_B": []
            },
            m.policy_cache
        )

        self.assertEqual(1, len(m.policy_store.keys()))
        self.assertEqual(
            {
                "groups": {
                    "group_B": {
                        enums.ObjectType.SYMMETRIC_KEY: {
                            enums.Operation.GET: enums.Policy.DISALLOW_ALL,
                            enums.Operation.LOCATE: enums.Policy.DISALLOW_ALL,
                            enums.Operation.DESTROY: enums.Policy.DISALLOW_ALL
                        }
                    }
                }
            },
            m.policy_store.get("policy_B", None)
        )
