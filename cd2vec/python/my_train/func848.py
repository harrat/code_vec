def test_InsightProvider(self):
        provider = InsightProvider("http://insight.bitpay.com/api/")
        self.check_provider_tx_for_tx_hash(lambda x: provider, ["BTC"])
        provider.get_blockchain_tip()
        h = provider.get_blockheader(BLOCK_1_HASH)
        assert h.previous_block_hash == BLOCK_0_HASH
        height = provider.get_block_height(BLOCK_1_HASH)
        assert height == 1
