def test_parse_unicode_name_two_byte_udf(tmpdir):
    indir = tmpdir.mkdir('unicodeudftwobyte')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'f?o'), 'wb') as outfp:
        outfp.write(b'foo\n')

    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-udf',
                     '-no-pad', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_unicode_name_two_byte_udf)

def test_parse_eltorito_get_bootcat(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('eltoritogetbootcat')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'boot'), 'wb') as outfp:
        outfp.write(b'boot\n')
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-c', 'boot.cat', '-b', 'boot', '-no-emul-boot',
                     '-o', str(outfile), str(indir)])
