@staticmethod
    def test_cli_import_snippet_007(snippy):
        """Import all snippet resources.

        Try to import snippet from file that is not existing. The file
        extension is one of the supported file formats.
        """

        with mock.patch('snippy.content.migrate.os.path.isfile', return_value=False):
            cause = snippy.run(['snippy', 'import', '-f', './foo.yaml'])
            assert cause == 'NOK: cannot read file ./foo.yaml'
            Content.assert_storage(None)
