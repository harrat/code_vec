def test_strange_test_names():
    class C:
        def __init__(self, a):
            self.a = a

        def __repr__(self):
            return 'C(%s)' % self.a

    class MyTest(rfm.RegressionTest):
        def __init__(self, a, b):
            self.a = a
            self.b = b

    test = MyTest('(a*b+c)/12', C(33))
    assert ('test_strange_test_names.<locals>.MyTest__a_b_c__12_C_33_' ==
            test.name)
