@freeze_time('2017-06-21')
def test_used_option_only_shows_used_aliases(cli, entries_file, alias_config):
    entries_file.write("""20.06.2017
    active1 1 Play ping-pong
    """)

    output = cli('alias', ['list', '--used'])

    assert 'active2' not in output
    assert 'active1' in output

