def test_impostor_get_matches_timeout():
    with Mountebank() as mb:
        imposter = mb.add_imposter_simple()
        with pytest.raises(TimeoutError):
            imposter.wait_for_requests(timeout=0.001)
