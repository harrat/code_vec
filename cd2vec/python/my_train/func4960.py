def test_process_objects(self, app, register_hook: RegisterExtensionsHook):
        register_hook.process_objects(app, FAKE_EXTENSIONS)

        registered = list(register_hook.unchained.extensions.keys())
        assert registered == ['four', 'two', 'three', 'one']
        for name, ext in register_hook.unchained.extensions.items():
            assert name == ext.name
            assert ext.app is None
