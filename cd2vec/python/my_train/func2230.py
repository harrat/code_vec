def test_multivariate_draw_sample(self):
        """
        Tests the draw_sample() function of MulvariateDistribution.
        """
        n=10000
        sample = self.mul_var_dist.draw_sample(n)
        self.assertEqual(n, sample[0].size)
        self.assertEqual(n, sample[1].size)

        # Fit the sample to the correct model structure and compare the
        # estimated parameters with the true parameters.
        dist_description_0 = {'name': 'Weibull',
                              'dependency': (None, None, None),
                              'width_of_intervals': 0.5}
        dist_description_1 = {'name': 'Lognormal',
                              'dependency': (0, None, 0),
                              'functions': ('exp3', None, 'power3')}
        fit = Fit(sample, [dist_description_0, dist_description_1])
        fitted_dist0 = fit.mul_var_dist.distributions[0]
        fitted_dist1 = fit.mul_var_dist.distributions[1]
        self.assertAlmostEqual(fitted_dist0.shape(0), self.shape(0), delta=0.15)
        self.assertAlmostEqual(fitted_dist0.loc(0), self.loc(0), delta=0.15)
        self.assertAlmostEqual(fitted_dist0.scale(0), self.scale(0), delta=0.15)
        self.assertAlmostEqual(fitted_dist1.shape.a, 0.04, delta=0.1)
        self.assertAlmostEqual(fitted_dist1.shape.b, 0.1748, delta=0.1)
        self.assertAlmostEqual(fitted_dist1.shape.c, -0.2243, delta=0.15)
