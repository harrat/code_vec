def test_subjects(capsys):
    context = new_context()
    context = shell.parse(context, input_file)
    context = shell.subjects(context, '')
    (out, err) = capsys.readouterr()
    # Skip first line (result of parse) and last line (blank)
    subjects = out.split('\n')[1:-1]
    assert len(subjects) == 11
    for subject in subjects:
        assert subject.startswith('http://example.org/social/')

