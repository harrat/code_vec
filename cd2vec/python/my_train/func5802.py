def test_insert_overwrite(mi, emp_seq):
    assert emp_seq[3] == mi.get('first_name', 'Shawn')
    assert emp_seq[0] == mi.get('emp_id', 786)
    assert mi.get('emp_id', 321) is None
    assert emp_seq[0] == mi.get('first_name', 'Steve')
    assert emp_seq[1] == mi.get('last_name', 'Hart')
    assert emp_seq[2] == mi.get('last_name', 'Ramon')
    mi.insert(Employee('Steve', 'Austin', 123), overwrite=True)
    assert mi.get('first_name', 'Steve').emp_id == 123
    mi.insert(Employee('Razor', 'Topaz', 8732), overwrite=True)
    assert mi.get('emp_id', 8732).last_name == 'Topaz'
    assert mi.get('first_name', 'Razor').emp_id == 8732

