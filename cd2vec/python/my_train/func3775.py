@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_search_reference_002(snippy, capsys):
        """Search references with ``sall`` option.

        Try to search references with keyword that cannot be found.
        """

        output = 'NOK: cannot find content with given search criteria' + Const.NEWLINE
        cause = snippy.run(['snippy', 'search', '--scat', 'reference', '--sall', 'notfound', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == 'NOK: cannot find content with given search criteria'
        assert out == output
        assert not err
