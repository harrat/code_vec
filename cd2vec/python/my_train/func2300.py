def test_predictions(self):

        nlp = load_spacy_model()
        some_text = "Jeg gik en tur med Lars"
        doc = nlp(some_text)
        self.assertTrue(doc.is_parsed)
        self.assertTrue(doc.is_nered)
        self.assertTrue(doc.is_tagged)
