def test_find_lut_indices(class_objects):
    d_source, d_class, s_width = class_objects
    obj = DrizzleSolving(d_source, d_class, s_width)
    ind = (1, 2)
    dia_init = np.array([[1, 3, 2], [3, 1, 2]])
    n_dia = 1
    n_width = 2
    ind_d = bisect_left(obj.data.mie['Do'], dia_init[ind], hi=n_dia - 1)
    ind_w = bisect_left(obj.width_lut[:, ind_d], -obj.width_ht[ind], hi=n_width - 1)
    compare = (ind_w, ind_d)
    testing.assert_almost_equal(obj._find_lut_indices
                                (ind, dia_init, n_dia, n_width), compare)
