@given(NON_EMPTY_TEXT_ITERABLES)
    def test_accepts_individual_strings(self, expected_strings):
        for string in expected_strings:
            self._tree.add(string)

        assert_strings_can_be_matched(
            self, self._tree.to_regexp(PythonFormatter), expected_strings
        )
