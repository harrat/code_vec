def test_discover_celery_queues(self):
        celery_plugins = [x for x in plugin_dir._registry if issubclass(x[0], CeleryHealthCheck)]
        assert len(celery_plugins) == len(current_app.amqp.queues)
