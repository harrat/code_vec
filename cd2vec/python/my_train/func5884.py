def test_executemany_not_iterable():
    with pytest.raises(TypeError):
        cu.executemany("insert into tests(id, name, real_field) values (?, ?, ?)",
                       11, 'test_executemany_not_iterable', 42)
