@pytest.mark.skipif(
    "TRAVIS" in os.environ and os.environ["TRAVIS"] == "true",
    reason="doesn't work with Travis",
)
def test_get_ydl_dict():
    info_dict = ydl_utils.get_ydl_dict(SEARCH_TERM, 1)
    if not isinstance(info_dict, dict):
        raise AssertionError()

    if not ydl_utils.dict_is_song(info_dict):
        raise AssertionError()

