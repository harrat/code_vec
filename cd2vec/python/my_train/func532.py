def test_average_state(self):
        path = os.path.join(os.path.dirname(__file__), '.', 'data', '')
        avgs = average_state(["6-19-2013", "6-20-2013"], 1, 28, "mL/s", path,
                             extension=".xls")
        avgs = np.round(avgs, 5)
        self.assertSequenceEqual(
        avgs.tolist(),
        [5.5, 5.5, 5.5, 5.43125, 5.42094, 5.40908, 5.39544, 5.37976, 5.36172,
        5.34098, 5.31712, 5.28969, 5.5, 5.5, 5.5]*u.mL/u.s
        )
