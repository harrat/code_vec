@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_nhl_goalie_returns_player_season_stats(self, *args, **kwargs):
        # Request the 2017 stats
        player = Player('howarja02')
        player = player('2017-18')

        for attribute, value in self.goalie_results_2017.items():
            assert getattr(player, attribute) == value
