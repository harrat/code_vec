def test_extract_price_with_citation(self):
		d = {
			'price': '7',
			'currency': 'pounds',
			'citation': 'crom test suite'
		}
		e = extract_monetary_amount(d, add_citations=True)
		self.assertEqual(e.value, 7)
		self.assertEqual(e.currency._label, 'British Pounds')
		self.assertEqual(e.referred_to_by[0].content, 'crom test suite')
