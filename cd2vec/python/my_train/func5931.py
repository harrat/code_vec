@pytest.mark.trylast
@needinternet
def test_run_lock_update(fixture_update_setup):
    """Checks the ability of program to upload new code"""
    assert os.path.isfile(Launcher.file_list)
    launch = Launcher('extradir/blah.py',
                      r'http://rlee287.github.io/pyautoupdate/'
                      '_static/testing/',
                      'project.zip', DEBUG)
    launch.run(True)
    while not launch.process_code_running:
        pass
    could_update_while_run = launch.update_code()
    assert not could_update_while_run
    launch.process_join()
    assert launch.process_exitcode == 0
    could_update = launch.update_code()
    assert could_update
    assert not os.path.isfile(Launcher.queue_update)
    assert os.path.isfile("extradir/blah.py")

