@mock_ssm
def test_it_deletes_a_missing_key_from_ssm(key):
    assert ParamStore.get(key, store=ParamStore.Stores.SSM).value is None
    assert ParamStore.delete(key, store=ParamStore.Stores.SSM) is True

