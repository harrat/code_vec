def test_text_with_hide(self):
        text = Text.parse(
            '(fp_text user text (at 0.0 0.0) (layer F.SilkS) hide)')
        assert text.hide == True
        assert Text.parse(text.to_string()) == text
