def test_incompatible_units():
    with pytest.raises(EIncompatibleUnits) as E:
        print(2*m)
        x = 2.0*m + 3.0*kg
    print(str(E.value))
    assert str(E.value) == 'Incompatible units: 2 m and 3 kg'

