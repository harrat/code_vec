def test_excel_process_with_sheet_names(self):
        excel = self.process_excel(show_row=False, name_sheets={u'??': {
                u'??': 'header3'
            }}, patch_sheet_alias=True)
        excel(10)
        # json file name should be sheet name
        assert os.path.exists(get_data_path(u'??.json'))
        assert os.path.exists(get_data_path('Sheet2.json'))
        assert os.path.exists(get_data_path('Sheet3.json'))
