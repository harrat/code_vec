def test_stac_entry_constructor():
    key = "B1"
    item = {
        "href": "https://landsat-pds.s3.amazonaws.com/c1/L8/120/046/LC08_L1GT_120046_20181012_20181012_01_RT/LC08_L1GT_120046_20181012_20181012_01_RT_B1.TIF",  # noqa: E501
        "type": "image/x.geotiff",
        "eo:bands": [0],
        "title": "Band 1 (coastal)",
    }

    entry = StacEntry(key, item)

    d = entry.describe()

    assert d["name"] == key
    assert d["container"] == "xarray"
    assert d["plugin"] == ["rasterio"]
    assert d["args"]["urlpath"] == item["href"]
    assert d["description"] == item["title"]
    assert d["metadata"] == item

