def test_get_filepath(self):
        """Test _get_filepath method."""
        conf = TESTConfig()
        with mock.patch('yamlconf.config.os.path.exists') as mock_exists:
            # no filename, not found in basepath
            mock_exists.side_effect = [False, True]
            expected = os.path.join(TEST_CONFIG_ROOT, 'config.yaml')
            result = conf._get_filepath()
            self.assertEqual(expected, result)
