def test_base_parse_filter_colon(api, fitem, fset):
    assert {
        'f': ['distro:match:win']
    } == api.agents._parse_filters(fitem, fset, rtype='colon')
