@pytest.mark.parametrize('orders, out, exception', [
		({'@#$':None}, None, FieldParseError),
		(OrderedDict([
			('f1|count', True),
			('s.t.f', None),
			('s.t.f4', False),
			('f2', 'asc'),
			('f3', 'desc')
		]), 'COUNT("f1") ASC,"s"."t"."f" ASC,"s"."t"."f4" DESC,"f2" ASC,"f3" DESC', None)
	])
	def testStr(self, orders, out, exception):
		if exception:
			with pytest.raises(exception):
				str(Order(orders))
		else:
			assert str(Order(orders)) == out
