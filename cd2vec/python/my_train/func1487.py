def test_dont_install_in_non_git(self):
        result = self.runner.invoke(install)
        assert_that(result.exit_code != 0)

        for hook in self.hooks:
            filename = os.path.join(self.path, HOOK_PATH, hook)
            assert_that(not os.path.exists(filename), filename)

        result = self.runner.invoke(uninstall)
        assert_that(result.exit_code != 0)
