@project_setup_py_test("hello-cython", ["bdist_wheel"])
def test_hello_cython_wheel():
    expected_content = [
        'hello_cython/_hello%s' % get_ext_suffix(),
        'hello_cython/__init__.py',
        'hello_cython/__main__.py'
    ]

    expected_distribution_name = 'hello_cython-1.2.3'

    whls = glob.glob('dist/*.whl')
    assert len(whls) == 1
    check_wheel_content(whls[0], expected_distribution_name, expected_content)

