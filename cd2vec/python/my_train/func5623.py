def test_iscomplete(spylon_kernel):
    result = spylon_kernel.do_is_complete('val foo = 99')
    assert result['status'] == 'complete'

    result = spylon_kernel.do_is_complete('val foo = {99')
    assert result['status'] == 'incomplete'

    result = spylon_kernel.do_is_complete('val foo {99')
    assert result['status'] == 'invalid'

