def test_multiexception_types():

    class OK(Exception):
        pass

    class BAD(object):
        pass

    class OKBAD(OK, BAD):
        pass

    with pytest.raises(AssertionError):
        MultiException[BAD]

    def raise_it(typ):
        raise typ()

    with pytest.raises(MultiException[OK]):
        MultiObject([OK]).call(raise_it)

    with pytest.raises(MultiException[OKBAD]):
        MultiObject([OKBAD]).call(raise_it)

    with pytest.raises(MultiException[OK]):
        MultiObject([OKBAD]).call(raise_it)
