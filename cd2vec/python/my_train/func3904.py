@staticmethod
    @pytest.mark.usefixtures('edit-unknown-solution-template')
    def test_cli_create_solution_005(snippy):
        """Try to create solution from CLI.

        Try to create new solution with a template that cannot be identified.
        In this case the user has changed the input template completely and
        it has lost tags that identify it as a solution content.
        """

        cause = snippy.run(['snippy', 'create', '--scat', 'solution', '--format', 'text'])
        assert cause == 'NOK: could not identify content category - please keep template tags in place'
        Content.assert_storage(None)
