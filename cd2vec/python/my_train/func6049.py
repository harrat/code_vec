def test_logged_condition():
    cond = LoggedCondition('test', log_interval=.1)

    progress = 0
    executed = []

    def wait_for_progress_to(progress_to):
        cond.wait_for(lambda: progress_to <= progress, 'progress to %s', progress_to)
        executed.append(progress_to)

    with concurrent(wait_for_progress_to, 10), concurrent(wait_for_progress_to, 20), concurrent(wait_for_progress_to, 30):
        with patch("easypy.sync._logger") as _logger:
            sleep(0.3)
