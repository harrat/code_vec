@pytest.mark.parametrize('field, out', [
		(Field('s.t.f'), '"s"."t"."f"'),
		(Field('t.f', schema = 's'), '"s"."t"."f"'),
		(Field('f', schema = 's', table = 't'), '"s"."t"."f"'),
	])
	def testStr(self, field, out):
		assert str(field) == out
