def test_filespec(self):
        null = FileSpec()
        assert '{file}' == null.template
        assert 'file' in null.path_vars

        path = self.root.make_file(name='ABC123.txt')
        base = path.name

        spec = FileSpec(
            StrPathVar('id', pattern='[A-Z0-9_]+', invalid=('XYZ999',)),
            StrPathVar('ext', pattern='[^\.]+', valid=('txt', 'exe')),
            template='{id}.{ext}')

        # get a single file
        pathinst = spec(id='ABC123', ext='txt')
        assert path_inst(base, dict(id='ABC123', ext='txt')) == pathinst
        assert 'ABC123' == pathinst['id']
        assert 'txt' == pathinst['ext']

        with self.assertRaises(ValueError):
            spec(id='abc123', ext='txt')

        with self.assertRaises(ValueError):
            spec(id='ABC123', ext='foo')

        with self.assertRaises(ValueError):
            spec(id='XYZ999', ext='txt')

        pathinst = spec.parse(path, fullpath=True)
        assert path_inst(path.name, dict(id='ABC123', ext='txt')) == pathinst

        path2 = self.root.make_file(name='abc123.txt')
        with self.assertRaises(ValueError):
            spec.parse(path2)

        all_paths = spec.find(self.root.absolute_path)
        assert 1 == len(all_paths)
        assert path_inst(path, dict(id='ABC123', ext='txt')) == all_paths[0]
