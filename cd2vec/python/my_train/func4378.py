def test_run_internal(self):
        self.env.argument = self._arg('googkit.py _candidates deps')
        with mock.patch('sys.stdout', new_callable=StubStdout) as mock_stdout:
            self.cmd.run_internal()
        candidates = mock_stdout.getvalue().split('\n')
        self.assertFalse('deps' in candidates)
        self.assertTrue('update' in candidates)
        self.assertFalse('--verbose' in candidates)

        self.env.argument = self._arg('googkit.py _candidates deps update')
        with mock.patch('sys.stdout', new_callable=StubStdout) as mock_stdout:
            self.cmd.run_internal()
        candidates = mock_stdout.getvalue().split('\n')
        self.assertFalse('deps' in candidates)
        self.assertFalse('update' in candidates)
        self.assertTrue('--verbose' in candidates)

        self.env.argument = self._arg('googkit.py _candidates deps update --verbose')
        with mock.patch('sys.stdout', new_callable=StubStdout) as mock_stdout:
            self.cmd.run_internal()
        candidates = mock_stdout.getvalue().split('\n')
        self.assertFalse('deps' in candidates)
        self.assertFalse('update' in candidates)
        self.assertFalse('--verbose' in candidates)
