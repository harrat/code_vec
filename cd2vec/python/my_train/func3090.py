def test_rolling_fileoutput_write(self):
        path = str(self.root.make_file())
        with textoutput(
                path + '{index}.txt', file_output_type=RollingFileOutput,
                lines_per_file=3) as out:
            for i in range(6):
                out.write(i, False)
            for ch in ('a', 'b', 'c'):
                out.write(ch, False)
            out.write("d\ne\nf")
        with open(path + '0.txt', 'rt') as infile:
            assert '0\n1\n2\n' == infile.read()
        with open(path + '1.txt', 'rt') as infile:
            assert '3\n4\n5\n' == infile.read()
        with open(path + '2.txt', 'rt') as infile:
            assert 'a\nb\nc\n' == infile.read()
        with open(path + '3.txt', 'rt') as infile:
            assert 'd\ne\nf\n' == infile.read()
