def test_curve(self):
        curve = Curve.parse(
            '(fp_curve (pts (xy 0 0) (xy 1 1) (xy 2 2) (xy 3 3)))')
        assert curve.start == (0, 0)
        assert curve.bezier1 == (1, 1)
        assert curve.bezier2 == (2, 2)
        assert curve.end == (3, 3)
