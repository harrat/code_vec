def test_idletime(self):
        sync()
        s = topic("testA")
        self.assertLess(s.idle_time, 0.1)
        time.sleep(0.2)
        self.assertGreater(s.idle_time, 0.1)
        s.send("event")
        self.assertLess(s.idle_time, 0.1)
