def test_get_unsupported_key_compression(self):
        """
        Test that the right error is generated when key compression is
        provided in a Get request.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        # Test that specifying the key compression type generates an error.
        k = enums.KeyCompressionType.EC_PUBLIC_KEY_TYPE_UNCOMPRESSED
        payload = payloads.GetRequestPayload(key_compression_type=k)

        args = (payload, )
        regex = "Key compression is not supported."
        six.assertRaisesRegex(
            self,
            exceptions.KeyCompressionTypeNotSupported,
            regex,
            e._process_get,
            *args
        )
        e._logger.info.assert_any_call(
            "Processing operation: Get"
        )
