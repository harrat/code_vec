def test_invalidate(region):
    region['key'] = 'value'
    region.invalidate()
    assert 'key' not in region
    assert region._region_cache.conn.hget(region.name, 'key') is None

    sb = region.region('sub')
    sb['key2'] = 'value'
    region.invalidate()

    assert region._region_cache.conn.hget(sb.name, 'key2') is None
    assert 'key2' not in sb

