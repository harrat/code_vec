@pytest.mark.remote
def test_sieve(gw150914_strain):
    nfiles = len(gw150914_strain)
    sieved = list(gwosc_urls.sieve(
        gw150914_strain,
        detector='L1',
    ))
    assert len(sieved) == nfiles // 2
    sieved = list(gwosc_urls.sieve(
        gw150914_strain,
        detector='L1',
        sampling_rate=4096,
    ))
    assert len(sieved) == nfiles // 4

