def test_flex_alloc_sched_access_idle(make_flexible_job):
    job = make_flexible_job('idle', sched_access=['--constraint=f1'])
    prepare_job(job)
    assert job.num_tasks == 8

