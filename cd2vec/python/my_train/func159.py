def test_h5group_list(self):
        file = test_file_path
        result1 = h5group_list(file)[0][0]
        result2 = h5group_list(file, 'group1')[0][0]
        self.assertEqual(result1, 'group1')
        self.assertEqual(result2, 'group2')
