def test_momentacc_norm():
    ma = MomentAccumulator()
    for v in [random.gauss(10, 4) for i in range(5000)]:
        ma.add(v)
    _assert_round_cmp(10, abs(ma.mean), mag=1)
    _assert_round_cmp(4, ma.std_dev, mag=1)
    _assert_round_cmp(0, ma.skewness, mag=1)
    _assert_round_cmp(3, ma.kurtosis, mag=1)

