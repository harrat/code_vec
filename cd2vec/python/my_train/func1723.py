def test_jointplot_one2one(jp_data):
    jg1 = viz.jointplot(x="B", y="C", data=jp_data, one2one=True)
    nptest.assert_array_equal(numpy.round(jg1.ax_joint.get_xlim()), [0, 23])
    nptest.assert_array_equal(numpy.round(jg1.ax_joint.get_ylim()), [0, 23])
    return jg1.fig

