def test_game_reject_map():
    """ Test a game where the client rejects the map """
    _ = Server()            # Initialize cache to prevent timeouts during tests
    game_path = os.path.join(FILE_FOLDER_NAME, 'game_data_1_reject_map.csv')
    run_with_timeout(lambda: run_game_data(1, ['NO_PRESS', 'IGNORE_ERRORS', 'POWER_CHOICE'], game_path), 60)

def test_game_1():
    """ Test a complete 1 player game """
    _ = Server()            # Initialize cache to prevent timeouts during tests
    game_path = os.path.join(FILE_FOLDER_NAME, 'game_data_1.csv')
    run_with_timeout(lambda: run_game_data(1, ['NO_PRESS', 'IGNORE_ERRORS', 'POWER_CHOICE'], game_path), 60)
