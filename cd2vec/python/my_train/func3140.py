@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_reference_018(snippy):
        """Import reference from text template.

        Try to import reference template without any changes. This should result
        error text for end user and no files should be read. The error text must
        be the same for all content types.
        """

        file_content = mock.mock_open(read_data=Const.NEWLINE.join(Reference.TEMPLATE_TEXT))
        with mock.patch('snippy.content.migrate.io.open', file_content, create=True) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'reference', '--template', '--format', 'text'])
            assert cause == 'NOK: content was not stored because it was matching to an empty template'
            Content.assert_storage(None)
            assert mock_file.call_args == mock.call('./reference-template.text', mode='r', encoding='utf-8')
