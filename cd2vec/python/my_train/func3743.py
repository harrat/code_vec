def test_write_dendrogram_no_stats(self, caplog, tmp_dir):
        res = wrapper.Output(target_root_name)
        res.extract_results()
        res.aggregate_input_data()
        res.write_dendrogram()
        statfile = res.root_out_name + "_stats.tsv"
        assert "Cannot find {}".format(statfile) in caplog.text
