def test_mock_queue_get_put_same_task():
    mock_queue.clear()

    task = MockTask()
    task.test = 'my_test'
    mock_queue.put(task)

    assert mock_queue.qsize() == 1

    my_task = mock_queue.get()
    assert my_task.test == 'my_test'
    assert mock_queue.qsize() == 0

    mock_queue.put(my_task)
    assert mock_queue.qsize() == 1

