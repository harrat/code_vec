def test_connect_should_connect_ws(self, mocker):
        socket = BitMEXWebsocket()

        assert socket.gen_url() == \
            'wss://www.bitmex.com/realtime?heartbeat=true'
