def test_02_read_w_io_priority(self):
        with AIOContext(1) as ctx, open(self._TEST_FILE_NAME) as fp:
            block = ReadBlock(fp, bytearray(64), priority_class=IOCBPriorityClass.RT, priority_value=1)
            self.assertEqual(IOCBPriorityClass.RT, block.priority_class)
            self.assertEqual(1, block.priority_value)
            block.set_priority(IOCBPriorityClass.IDLE, 2)
            self.assertEqual(IOCBPriorityClass.IDLE, block.priority_class)
            self.assertEqual(2, block.priority_value)
            block.priority_value = 3
            block.priority_class = IOCBPriorityClass.BE
            self.assertEqual(IOCBPriorityClass.BE, block.priority_class)
            self.assertEqual(3, block.priority_value)
