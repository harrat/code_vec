def test_sequence():
    s = AvroSequenceSource(urlpath=path)
    disc = s.discover()
    assert disc['dtype'] is None
    assert s.container == 'python'
    out = s.read()
    assert out == expected.to_dict('records')

    s = AvroSequenceSource(urlpath=[path, path2])
    out = s.read()
    assert out == pd.concat([expected, expected],
                            ignore_index=True).to_dict('records')

    s = AvroSequenceSource(urlpath=pathstar)
    out = s.read()
    assert out == pd.concat([expected, expected],
                            ignore_index=True).to_dict('records')
