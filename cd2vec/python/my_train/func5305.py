def test_main(self):
        args = stowage.parse_args([
            '--source', self.dir,
            '--destination', self.out_dir,
            '--backup', join(self.dir, 'path/to/bup'),
            'vim',
        ])
        stowage.main(args)
        assert exists(join(self.out_dir, '.vimrc'))
        assert exists(join(self.out_dir, '.config', 'openbox', 'openbox.xml'))
        contents = open(join(self.out_dir, '.vimrc')).read()
        assert contents == '%s contents' % join(self.dir, 'vim', '_vimrc')
