def test_benchmark(self):
    net = nets.resnet18()
    if not os.path.exists('/tmp/imgnet/'):
      r = requests.get(TestAttacks.url_dset)
      z = zipfile.ZipFile(io.BytesIO(r.content))
      z.extractall('/tmp/')

    #atks = [attacks.Noise, attacks.Semantic, attacks.FGM, attacks.PGD, 
            #attacks.DeepFool]
    atks = [attacks.Noise]
    for atk in atks: attacks.benchmark_atk(atk, net, root='/tmp/imgnet/')
