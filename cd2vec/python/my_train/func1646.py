def testLoad(capsys, monkeypatch, tmpdir):
    import sys
    from zm.waf.assist import isBuildConfFake

    buildconf = bconfloader.load()
    bconfloader.validate(buildconf)
    # It should be fake
    assert isBuildConfFake(buildconf)

    nonpath = os.path.join(cmn.randomstr(), cmn.randomstr())
    assert not os.path.exists(nonpath)
    buildconf = bconfloader.load(dirpath = nonpath)
    # It should be fake
    assert isBuildConfFake(buildconf)

    # invalidate conf
    monkeypatch.setattr(buildconf, 'tasks', 'something')
    with pytest.raises(SystemExit) as cm:
        buildconf = bconfloader.load()
        bconfloader.validate(buildconf)
    captured = capsys.readouterr()
    assert cm.value.code
    assert captured.err

    # find first real buildconf.py
    prjdir = None
    for dirpath, _, filenames in os.walk(cmn.TEST_PROJECTS_DIR):
        if 'buildconf.py' in filenames:
            prjdir = dirpath
            break

    buildconf = bconfloader.load(dirpath = prjdir)
    bconfloader.validate(buildconf)
    assert not isBuildConfFake(buildconf)

    monkeypatch.syspath_prepend(os.path.abspath(prjdir))
    buildconf = bconfloader.load()
    bconfloader.validate(buildconf)
    assert not isBuildConfFake(buildconf)

    # find first real buildconf.yaml
    prjdir = None
    for dirpath, _, filenames in os.walk(cmn.TEST_PROJECTS_DIR):
        if 'buildconf.yaml' in filenames:
            prjdir = dirpath
            break

    buildconf = bconfloader.load(dirpath = prjdir)
    bconfloader.validate(buildconf)
    assert not isBuildConfFake(buildconf)

    monkeypatch.syspath_prepend(os.path.abspath(prjdir))
    buildconf = bconfloader.load()
    bconfloader.validate(buildconf)
    assert not isBuildConfFake(buildconf)

    testdir = tmpdir.mkdir("load.yaml")
    yamlconf = testdir.join("buildconf.yaml")
    yamlconf.write("invalid data = {")
    with pytest.raises(ZenMakeConfError):
        buildconf = bconfloader.load(dirpath = str(testdir.realpath()))
    yamlconf.write("invalid data: {")
    with pytest.raises(ZenMakeConfError):
        buildconf = bconfloader.load(dirpath = str(testdir.realpath()))
