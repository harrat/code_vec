def test_reporthook20percent(self):
        f = io.StringIO()
        start = _current_milli_time() - 4000
        with contextlib.redirect_stdout(f):
            _report_hook(2, 1024*1024, 1024*1024*10, start, start)
        assert_that(f.getvalue(), contains_string("20%, 2 MB, 512 KB/s, 4 seconds passed"))
