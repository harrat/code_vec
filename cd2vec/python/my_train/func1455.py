@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_search_snippet_044(snippy, capsys):
        """Search snippets with search shortcut.

        Search snippets with the search shortcut. In this case the search
        matches to two resources but the ``--limit`` option limits printed
        resources to one.
        """

        output = (
            '1. Remove all docker containers with volumes @docker [54e41e9b52a02b63]',
            '',
            '   $ docker rm --volumes $(docker ps --all --quiet)',
            '',
            '   # cleanup,container,docker,docker-ce,moby',
            '   > https://docs.docker.com/engine/reference/commandline/rm/',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '.', '--limit', '1', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
