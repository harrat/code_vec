@mock.patch('buildozer.targets.android.os.path.isfile')
    @mock.patch('buildozer.targets.android.os.path.exists')
    @mock.patch('buildozer.targets.android.open', create=True)
    def test_p4a_recommended_android_ndk_found(
            self, mock_open, mock_exists, mock_isfile
    ):
        self.set_specfile_log_level(self.specfile.name, 1)
        buildozer = Buildozer(self.specfile.name, 'android')
        expected_ndk = '19b'
        recommended_line = 'RECOMMENDED_NDK_VERSION = {expected_ndk}\n'.format(
            expected_ndk=expected_ndk)
        mock_open.return_value = StringIO(recommended_line)
        ndk_version = buildozer.target.p4a_recommended_android_ndk
        p4a_dir = os.path.join(
            buildozer.platform_dir, buildozer.target.p4a_directory_name)
        mock_open.assert_called_once_with(
            os.path.join(p4a_dir, "pythonforandroid", "recommendations.py"), 'r'
        )
        assert ndk_version == expected_ndk

        # now test that we only read one time p4a file, so we call again to
        # `p4a_recommended_android_ndk` and we should still have one call to `open`
        # file, the performed above
        ndk_version = buildozer.target.p4a_recommended_android_ndk
        mock_open.assert_called_once()
