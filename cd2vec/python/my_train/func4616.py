def test_load_counter_2(tmpdir):
    """Test the wkr.io.load_counter method."""
    # write the counter out to file
    filename = tmpdir.join('counts.tsv').ensure().strpath
    with open(filename, 'wb') as output_file:
        output_file.write(b'2\ta\n')
        output_file.write(b'4\tb\n')
        output_file.write(b'1\tc\n')
        output_file.write(b'3\ta\n')
    # read it back in
    assert wkr.io.load_counter(filename) == Counter('aabbbbcaaa')

