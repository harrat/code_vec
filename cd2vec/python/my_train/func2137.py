@pytest.mark.skipif(not (HAS_PYTHON_VER2 or HAS_CONDA),
                    reason="Requires two usable Python versions")
def test_matrix_environments(tmpdir, dummy_packages):
    conf = config.Config()

    conf.env_dir = six.text_type(tmpdir.join("env"))

    conf.pythons = [PYTHON_VER1, PYTHON_VER2]
    conf.matrix = {
        "asv_dummy_test_package_1": [DUMMY1_VERSION, None],
        "asv_dummy_test_package_2": DUMMY2_VERSIONS
    }
    environments = list(environment.get_environments(conf, None))

    assert len(environments) == 2 * 2 * 2

    # Only test the first two environments, since this is so time
    # consuming
    for env in environments[:2]:
        env.create()

        output = env.run(
            ['-c', 'import asv_dummy_test_package_1 as p, sys; sys.stdout.write(p.__version__)'],
            valid_return_codes=None)
        if 'asv_dummy_test_package_1' in env._requirements:
            assert output.startswith(six.text_type(env._requirements['asv_dummy_test_package_1']))

        output = env.run(
            ['-c', 'import asv_dummy_test_package_2 as p, sys; sys.stdout.write(p.__version__)'])
        assert output.startswith(six.text_type(env._requirements['asv_dummy_test_package_2']))

