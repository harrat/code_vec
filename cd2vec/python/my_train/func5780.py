def test_build_from_dict_without_name(self):
        """
        The only required argument to create a task template is the task
        name.
        """
        task_dict = {
            "id": "1234",
        }
        with self.assertRaises(KeyError):
            TaskTemplate.from_dict(task_dict)
