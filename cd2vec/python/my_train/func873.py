@mock.patch(
        'kmip.services.server.auth.get_extended_key_usage_from_certificate'
    )
    @mock.patch('kmip.services.server.auth.get_certificate_from_connection')
    @mock.patch('kmip.core.messages.messages.RequestMessage')
    def test_handle_message_loop_invalid_certificate_extension(self,
                                                               request_mock,
                                                               cert_mock,
                                                               ext_mock):
        """
        Test that the correct logging and error handling occurs when an
        invalid certificate is encountered while processing a request.
        """
        data = utils.BytearrayStream(())

        cert_mock.return_value = 'test_certificate'
        ext_mock.return_value = []
        kmip_engine = engine.KmipEngine()
        kmip_engine._logger = mock.MagicMock()
        kmip_session = session.KmipSession(
            kmip_engine,
            None,
            None,
            name='name',
            enable_tls_client_auth=True
        )
        kmip_session.authenticate = mock.MagicMock()
        kmip_session._engine = mock.MagicMock()
        kmip_session._logger = mock.MagicMock()
        kmip_session._connection = mock.MagicMock()
        kmip_session._receive_request = mock.MagicMock(return_value=data)
        kmip_session._send_response = mock.MagicMock()

        kmip_session._handle_message_loop()

        kmip_session._receive_request.assert_called_once_with()
        kmip_session._logger.warning(
            "Failure verifying the client certificate."
        )
        kmip_session._logger.exception.assert_called_once_with(
            exceptions.PermissionDenied(
                "The extended key usage extension is not marked for client "
                "authentication in the client certificate."
            )
        )
        kmip_session._engine.build_error_response.assert_called_once_with(
            contents.ProtocolVersion(1, 0),
            enums.ResultReason.AUTHENTICATION_NOT_SUCCESSFUL,
            "Error verifying the client certificate. "
            "See server logs for more information."
        )
        self.assertTrue(kmip_session._send_response.called)
