def test_invalid_observable_embedded_timestamp(self):
        observed_data = copy.deepcopy(self.valid_observed_data)
        observed_data['objects']['2'] = {
            "type": "x509-certificate",
            "x509_v3_extensions": {
              "private_key_usage_period_not_before": "2016-11-31T08:17:27.000000Z"
            }
        }
        self.assertFalseWithOptions(observed_data)
