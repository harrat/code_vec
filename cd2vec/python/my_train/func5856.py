def test_list_iter_mapping(self):
        # arrange
        self.stub_request('get', 'checkouts', {'data': [{'resource': 'checkout'} for _ in range(25)]})
        # act
        for checkout in self.client.checkout.list_paging_iter():
            # assert
            self.assertIsInstance(checkout, Checkout)
