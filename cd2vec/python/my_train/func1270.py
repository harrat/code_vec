def test_request_omitting_secret(self):
        """
        Test that an InvalidField error is generate when trying to register
        a secret in absentia.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        object_type = enums.ObjectType.SYMMETRIC_KEY
        payload = payloads.RegisterRequestPayload(object_type=object_type)

        args = (payload, )
        regex = "Cannot register a secret in absentia."
        six.assertRaisesRegex(
            self,
            exceptions.InvalidField,
            regex,
            e._process_register,
            *args
        )
