@pytest.mark.vcr()
def test_workbench_asset_vulns_age_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.asset_vulns(str(uuid.uuid4()), age='none')
