def test_configure_translation(translator):
    rdftools.configure_translation()
    import i18n
    import locale
    # Ensure the messages files are added to the path
    assert len(i18n.load_path) == 1
    assert i18n.load_path[-1].endswith('rdftools/messages')
    # Ensure the locale is picked up from the system
    (lang, encoding) = locale.getlocale()
    assert lang.startswith(i18n.get('locale'))
    # Ensure the translation actually works!
    import sys
    name = sys._getframe().f_code.co_name
    assert i18n.t('test.test_case', name=name) == 'test %s?' % name

