def test_toJSON(self):
		# model.factory.context_uri = 'http://lod.getty.edu/context.json'
		expect = OrderedDict([
			('@context', model.factory.context_uri),
			('id', u'http://lod.example.org/museum/InformationObject/collection'), 
			('type', 'InformationObject'), ('_label', 'Test Object')])
		outj = model.factory.toJSON(self.collection)
		self.assertEqual(expect, outj)
