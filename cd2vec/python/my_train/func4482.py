def test_agentcontext_train(self):
        b = easyagents.backends.debug.DebugAgentFactory()
        a = b.create_agent(PpoAgent, ModelConfig(_stepcount_name))
        c = AgentContextTest.TrainCallback()
        a.train(callbacks=[Fast(), c], train_context=PpoTrainContext())
        assert c.train_called
        assert c.play_called
