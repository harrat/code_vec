@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_roster_from_team_class(self, *args, **kwargs):
        flexmock(Team) \
            .should_receive('_parse_team_data') \
            .and_return(None)
        team = Team(team_data=None, rank=1, year='2018')
        mock_abbreviation = mock.PropertyMock(return_value='NOR')
        type(team)._abbreviation = mock_abbreviation

        assert len(team.roster.players) == 5

        for player in team.roster.players:
            assert player.name in ['Drew Brees', 'Demario Davis',
                                   'Tommylee Lewis', 'Wil Lutz',
                                   'Thomas Morstead']
        type(team)._abbreviation = None
