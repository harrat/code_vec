@pytest.mark.parametrize('key', ['Do', 'mu', 'S', 'beta_corr'])
def test_init_variables(class_objects, key):
    d_source, d_class, s_width = class_objects
    obj = DrizzleSolving(d_source, d_class, s_width)
    result, x = obj._init_variables()
    assert key in result.keys()

