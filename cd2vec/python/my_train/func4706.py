def test_debug1_logs(tier1_logger, tier2_logger, tier3_logger):
    """Tier 1 loggers use DEBUG if --debug is provided"""

    task = MyTask()
    sys.argv = ["my-task", "--debug"]
    task.main()

    assert tier1_logger.getEffectiveLevel() == logging.DEBUG
    assert tier2_logger.getEffectiveLevel() == logging.INFO
    assert tier3_logger.getEffectiveLevel() == logging.INFO

