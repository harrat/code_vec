def test_host_setcontact(self, host_load_data):
        host = host_load_data
        with open(resource_dir / 'test_host_contact.json') as c:
            cs = Contact(json.load(c))

        data = dict()
        data['action'] = 'setcontact'
        data['object'] = 'HOST'
        data['values'] = ["mail-uranus-frontend", "g15x"]

        with patch('requests.post') as patched_post:
            host.setcontact(cs)
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
