def test_acc_random():
    data = test_sets['random.random 0.0-1.0']

    qa = ReservoirAccumulator(data)
    capqa = ReservoirAccumulator(data, cap=True)
    p2qa = P2Accumulator(data)
    for acc in (qa, capqa, p2qa):
        for qp, v in acc.get_quantiles():
            if qp > 0:
                assert 0.95 < (v / qp) < 1.05

