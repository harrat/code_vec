def test_register_unsupported_object_type(self):
        """
        Test that an InvalidField error is generated when attempting to
        register an unsupported object type.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        object_type = enums.ObjectType.TEMPLATE
        payload = payloads.RegisterRequestPayload(object_type=object_type)

        args = (payload, )
        regex = "The Template object type is not supported."
        six.assertRaisesRegex(
            self,
            exceptions.InvalidField,
            regex,
            e._process_register,
            *args
        )
