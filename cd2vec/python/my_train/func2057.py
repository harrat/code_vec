@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_api_search_reference_field_009(server):
        """Get resource attribute.

        Send GET /references/{id}/source for existing resource.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '227'
        }
        expect_body = {
            'data': {
                'type': 'reference',
                'id': Reference.GITLOG_UUID,
                'attributes': {
                    'source': Storage.gitlog['source']
                }
            },
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/references/31cd5827-b6ef-4067-b5ac-3ceac07dde9f/source'
            }
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/references/5c2071094dbfaa33/source',
            headers={'accept': 'application/vnd.api+json'})
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
