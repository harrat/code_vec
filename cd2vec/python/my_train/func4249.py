def test_extract_est(self):
		e = extract_monetary_amount({
			'est_price': '12.0',
			'currency': 'pounds'
		})
		self.assertEqual(e.value, 12)
		c = e.currency
		self.assertEqual(e.classified_as[0]._label, 'Estimated Price')
		self.assertEqual(e.currency._label, 'British Pounds')
