def test_insert_into_cache(self):
    """test_insert_into_cache ensures the results from an OSSIndex call
    are inserted into cache if they do not yet exist in cache"""
    file = Path(__file__).parent / "ossindexresponse.txt"
    with open(file, "r") as stdin:
      response = json.loads(stdin.read(), cls=ResultsDecoder)
      (cached, num_cached) = self.func.maybe_insert_into_cache(response)
    self.assertEqual(num_cached, 32)
    self.assertEqual(cached, True)
