def test_container_display_name(self):
        """
        Tests the dockerprettyps.container_display_name() method to see if we create the right console formatting for a
        container, with potential bolding to highlight search items.

        """
        containers = test_ps_data.ps_containers
        container = containers[0]
        container_display = dockerprettyps.container_display_name(container, CliArgs())
        assert container_display == container["color"] + container["name"] + dockerprettyps.ENDC

        # Test that we bold the portion of a container name that matches a search if we have one.
        args = CliArgs()
        args.search = ["post"]
        for container in containers:
            if container["name"] == "some-postgres":
                assert dockerprettyps.container_display_name(container, args) == \
                    "\x1b[91msome-\x1b[1m\x1b[91mpost\x1b[0m\x1b[91mgres\x1b[0m"
