@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_update_reference_011(snippy, edited_gitlog):
        """Update reference with ``content`` option.

        Try to update reference with empty content links. Nothing must be
        updated in this case because links are mandatory item in reference
        content.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        updates = Content.deepcopy(Reference.GITLOG)
        updates['links'] = ()
        edited_gitlog.return_value = Content.dump_text(updates)
        cause = snippy.run(['snippy', 'update', '--scat', 'reference', '-d', '5c2071094dbfaa33'])
        assert cause == 'NOK: content was not stored because mandatory content field links is empty'
        Content.assert_storage(content)
