@pytest.mark.vcr()
def test_agents_list_filter_operator_typeerror(api):
    with pytest.raises(TypeError):
        api.agents.list(('distro', 1, 'win'))
