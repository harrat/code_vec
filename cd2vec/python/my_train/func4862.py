def test_verify_failure_unsupported_algorithm(self):
        """
            Test verifying a token with an incompatible algorithm.

            Expected Result: An `UnsupportedAlgorithmError` is raised.
        """

        # Save the default algorithm to restore it later.
        encoding_algorithm = EasyJWT.algorithm

        easyjwt_creation = EasyJWT(self.key)
        token = easyjwt_creation.create()

        # Change the algorithm for now so that the one used for creation is not supported.
        EasyJWT.algorithm = Algorithm.HS512
        self.assertNotEqual(encoding_algorithm, EasyJWT.algorithm)

        # Try to verify the token.
        with self.assertRaises(UnsupportedAlgorithmError):
            easyjwt_verification = EasyJWT.verify(token, self.key)
            self.assertIsNone(easyjwt_verification)

        # Restore the default algorithm on the class to prevent side effect on other parts of the tests.
        EasyJWT.algorithm = encoding_algorithm
