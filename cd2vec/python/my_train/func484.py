def test_get_translations(empty_person: Person):
    translations = empty_person.get_translations()
    assert translations == empty_person.data["translations"]

