def test_hellocheck_local_prepost_run(hellotest, local_exec_ctx):
    @sn.sanity_function
    def stagedir(test):
        return test.stagedir

    # Test also the prebuild/postbuild functionality
    hellotest.prerun_cmds = ['echo prerun: `pwd`']
    hellotest.postrun_cmds = ['echo postrun: `pwd`']
    pre_run_path = sn.extractsingle(r'^prerun: (\S+)', hellotest.stdout, 1)
    post_run_path = sn.extractsingle(r'^postrun: (\S+)', hellotest.stdout, 1)
    hellotest.sanity_patterns = sn.all([
        sn.assert_eq(stagedir(hellotest), pre_run_path),
        sn.assert_eq(stagedir(hellotest), post_run_path),
    ])
    _run(hellotest, *local_exec_ctx)

