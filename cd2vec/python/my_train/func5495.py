@staticmethod
    @pytest.mark.usefixtures('import-gitlog', 'caller')
    def test_api_update_reference_003(server):
        """Update one reference with PUT request.

        Try to send PUT /references/{id} to update resource with ``id`` in
        URI path that is not found.
        """

        storage = {
            'data': [
                Storage.gitlog
            ]
        }
        request_body = {
            'data': {
                'type': 'snippet',
                'attributes': {
                    'links': Request.regexp['links'],
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '377'
        }
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '404',
                'statusString': '404 Not Found',
                'module': 'snippy.testing.testing:123',
                'title': 'cannot find content with content identity: 101010101010101'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_put(
            path='/api/snippy/rest/references/101010101010101',
            headers={'accept': 'application/json'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_404
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
