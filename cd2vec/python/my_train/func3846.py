@responses.activate
    def test_resourcecfg_setparam(self):
        with open(resource_dir / 'test_resourcecfg_1.json') as data:
            res = ResourceCFG(json.load(data))
        values = [
            res.id,
            'instance',
            'Central',
        ]
        data = {}
        data['action'] = 'setparam'
        data['object'] = 'RESOURCECFG'
        data['values'] = values

        with patch('requests.post') as patched_post:
            res.setparam('instance', 'Central')
            patched_post.assert_called_with(self.clapi_url, headers=self.headers, data=json.dumps(data), verify=True)
