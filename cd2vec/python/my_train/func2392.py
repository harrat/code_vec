@staticmethod
    @pytest.mark.usefixtures('import-gitlog', 'import-remove', 'import-beats', 'export-time')
    def test_cli_export_reference_022(snippy):
        """Export content with search keyword.

        Export content from two categories with search keyword. In this case
        -f|--file option is not used and the content must be stored into a
        single default file and format.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Reference.GITLOG,
                Snippet.REMOVE
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'reference,snippet', '--sall', 'volumes,git'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './content.mkdn', content)
