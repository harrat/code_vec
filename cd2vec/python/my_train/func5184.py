def test_mask_body(monkeypatch):
    ConfigProvider.set(config_names.THUNDRA_TRACE_INTEGRATIONS_ELASTICSEARCH_BODY_MASK, 'true')
    try:
        es = Elasticsearch([{'host': 'test', 'port': 3737}], max_retries=0)
        author1 = {"name": "Sidney Sheldon", "novels_count": 18}
        es.index(index='authors', doc_type='authors', body=author1, id=1)
    except ElasticsearchException:
        pass
    finally:
        tracer = ThundraTracer.get_instance()
        span = tracer.get_spans()[0]
        tracer.clear()
