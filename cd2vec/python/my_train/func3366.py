@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_update_solution_006(snippy):
        """Update solution with ``--digest`` option.

        Try to update solution with message digest that cannot be found. No
        changes must be made to stored content.
        """

        content = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        cause = snippy.run(['snippy', 'update', '--scat', 'solution', '-d', '123456789abcdef0'])
        assert cause == 'NOK: cannot find content with message digest: 123456789abcdef0'
        Content.assert_storage(content)
