@given(
        value=integers()
    )
    def test_database_retrieve(self, value):
        class_choice = self.class_choices[value % 3]
        with pytest.raises(IndexError):
            self.database.select(class_choice).retrieve(bad_index=value)
        if class_choice == BadObject:
            with pytest.raises(IndexError):
                self.database.select(class_choice).retrieve(good_index=value)
        elif class_choice == GoodObject:
            results = self.database.select(class_choice).retrieve(good_index=GoodIndex(value))
            if results:
                for result in results:
                    assert isinstance(result, GoodObject)
                    assert result.good_index == GoodIndex(value)
        elif class_choice == BadAndGoodObject:
            results = self.database.select(class_choice).retrieve(good_index=value)
            if results:
                for result in results:
                    assert isinstance(result, BadAndGoodObject)
                    assert result.good_index == value
