def test_show(capsys):
    context = new_context()
    context = shell.parse(context, input_file)
    context = shell.show(context, 'nt')
    (out, err) = capsys.readouterr()
    lines = [line for line in out.split('\n')[1:] if not line == '']
    assert len(lines) == input_file_count
    for line in lines:
        if not line == '':
            assert line.startswith('<')
            assert line.endswith('> .')

