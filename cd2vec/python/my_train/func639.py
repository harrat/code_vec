def test_gaf():
    """
    Test loading from gaf
    """
    ofactory = OntologyFactory()
    afactory = AssociationSetFactory()
    ont = ofactory.create('go')
    aset = afactory.create_from_gaf(open(POMBASE,"r"),
                                    ontology=ont)
    print(str(aset))
    genes = aset.query([INTRACELLULAR])
    for g in genes:
        print("G={} '{}'".format(g, aset.label(g)))
    assert G1 in genes

def test_create_from_file_no_fmt():
    """
    Test loading from gaf while setting fmt to None
    """
    ont = OntologyFactory().create('go')
    f = AssociationSetFactory()
    aset = f.create(ontology=ont, fmt=None, file=POMBASE)
    print("SUBJS: {}".format(aset.subjects))
    assert len(aset.subjects) > 100
