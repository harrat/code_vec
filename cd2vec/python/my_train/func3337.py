def test_mac_with_missing_fields(self):
        """
        Test that the right errors are generated when required fields
        are missing.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        key = (b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
               b'\x00\x00\x00\x00')
        data = (b'\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B'
                b'\x0C\x0D\x0E\x0F')
        algorithm = enums.CryptographicAlgorithm.AES
        obj_no_key = pie_objects.OpaqueObject(b'', enums.OpaqueDataType.NONE)
        obj_no_algorithm = pie_objects.OpaqueObject(
            key, enums.OpaqueDataType.NONE)

        e._data_session.add(obj_no_key)
        e._data_session.add(obj_no_algorithm)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        uuid_no_key = str(obj_no_key.unique_identifier)
        uuid_no_algorithm = str(obj_no_algorithm.unique_identifier)

        cryptographic_parameters = attributes.CryptographicParameters(
            cryptographic_algorithm=algorithm
        )

        payload_no_key = payloads.MACRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(uuid_no_key),
            cryptographic_parameters=cryptographic_parameters,
            data=objects.Data(data)
        )

        args = (payload_no_key, )
        regex = "A secret key value must be specified"
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            regex,
            e._process_mac,
            *args
        )

        payload_no_algorithm = payloads.MACRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(uuid_no_algorithm),
            cryptographic_parameters=None,
            data=objects.Data(data)
        )

        args = (payload_no_algorithm, )
        regex = "The cryptographic algorithm must be specified"
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            regex,
            e._process_mac,
            *args
        )

        payload_no_data = payloads.MACRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(uuid_no_algorithm),
            cryptographic_parameters=cryptographic_parameters,
            data=None
        )

        args = (payload_no_data, )
        regex = "No data to be MACed"
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            regex,
            e._process_mac,
            *args
        )
