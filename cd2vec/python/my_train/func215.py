def test_load_local_data_from_local_with_cache_failed(self):
        with UseModel(self.foo_model, cache_problem=True) as model:
            with self.assertRaises(cherry.exceptions.NotSupportError) as notFoundError:
                res = cherry.base._load_data_from_local(self.foo_model)
            self.assertEqual(
                str(notFoundError.exception),
                'Can\'t load cached data from foo. Please try again after delete cache files.')
