@pytest.mark.vcr()
def test_access_groups_list_sort_field_typeerror(api):
    with pytest.raises(TypeError):
        api.access_groups.list(sort=((1, 'asc'),))
