def test_app_with_correct_credentials_but_with_no_port(self):
        default_port = handler.DEFAULT_PORT
        handler.DEFAULT_PORT = self.sshserver_port

        # with no port value
        body = self.body.replace(str(self.sshserver_port), '')
        response = self.sync_post('/', body)
        self.assert_status_none(json.loads(to_str(response.body)))

        # with no port argument
        body = body.replace('port=&', '')
        response = self.sync_post('/', body)
        self.assert_status_none(json.loads(to_str(response.body)))

        handler.DEFAULT_PORT = default_port
