def test_home_shutout_multiple_goalies(self):
        shutout = ['0', '0', '1']

        fake_shutout = PropertyMock(return_value=shutout)
        fake_num_goalies = PropertyMock(return_value=1)
        type(self.boxscore)._home_shutout = fake_shutout
        type(self.boxscore)._away_goalies = fake_num_goalies

        assert self.boxscore.home_shutout == 1
