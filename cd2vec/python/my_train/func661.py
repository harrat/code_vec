@mock.patch("IPython.display.display")
def test_scraps_report_with_scrap_list_names(mock_display, notebook_collection):
    notebook_collection.scraps_report(scrap_names=["output"])
    mock_display.assert_has_calls(
        [
            mock.call(AnyMarkdownWith("### result1")),
            mock.call(AnyMarkdownWith("#### output")),
            mock.call({"text/plain": "'Hello World!'"}, metadata={}, raw=True),
            mock.call(AnyMarkdownWith("<hr>")),
            mock.call(AnyMarkdownWith("### result2")),
            mock.call(AnyMarkdownWith("#### output")),
            mock.call({"text/plain": "'Hello World 2!'"}, metadata={}, raw=True),
        ]
    )
