def test_vocab_identity_class(self):
        identity = copy.deepcopy(self.valid_identity)
        identity['identity_class'] = "corporation"
        results = validate_parsed_json(identity, self.options)
        self.assertEqual(results.is_valid, False)
