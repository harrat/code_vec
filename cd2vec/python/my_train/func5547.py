def test_move_notify(self):
        class LocationNotify(Location):
            def notify_npc_left(self, npc: Living, target_location: Location) -> None:
                self.npc_left = npc
                self.npc_left_target = target_location

            def notify_npc_arrived(self, npc: Living, previous_location: Location) -> None:
                self.npc_arrived = npc
                self.npc_arrived_from = previous_location

            def notify_player_left(self, player: Player, target_location: Location) -> None:
                self.player_left = player
                self.player_left_target = target_location

            def notify_player_arrived(self, player: Player, previous_location: Location) -> None:
                self.player_arrived = player
                self.player_arrived_from = previous_location

        npc = Living("rat", "m", race="rodent")
        room1 = LocationNotify("room1")
        room2 = LocationNotify("room2")
        room1.insert(npc, None)
        npc.move(room2)
        pubsub.sync()
        self.assertEqual(room2, npc.location)
        self.assertEqual(npc, room1.npc_left)
        self.assertEqual(room2, room1.npc_left_target)
        self.assertEqual(npc, room2.npc_arrived)
        self.assertEqual(room1, room2.npc_arrived_from)
