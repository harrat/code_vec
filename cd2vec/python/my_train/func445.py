@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_solution_032(snippy):
        """Import solutions from Markdown template.

        Try to import solution template without any changes. This should result
        error text for end user and no files should be read. The error text must
        be the same for all content types.
        """

        file_content = mock.mock_open(read_data=Const.NEWLINE.join(Solution.TEMPLATE_MKDN))
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '--template', '--format', 'mkdn'])
            assert cause == 'NOK: content was not stored because it was matching to an empty template'
            Content.assert_storage(None)
            Content.assert_arglist(mock_file, './solution-template.mkdn', mode='r', encoding='utf-8')
