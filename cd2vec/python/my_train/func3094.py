@staticmethod
    @pytest.mark.usefixtures('create-remove-utc')
    def test_cli_create_snippet_007(snippy, capsys):
        """Create snippet with unicode characters from CLI.

        Every field that can be given from command line contains unicode
        characters. The same content must be found by searching it with
        keyword that contains unicode characters.
        """

        content = {
            'data': [{
                'category': 'snippet',
                'data': (u'S�ne kl�wen durh die wolken sint geslagen', u'er st�get �f mit gr�zer kraft'),
                'brief': u'Tagelied of Wolfram von Eschenbach S�ne kl�wen',
                'description': '',
                'name': '',
                'groups': (u'D�sseldorf',),
                'tags': (u'??????', u'??????', u'????????'),
                'links': (u'http://www.???????.edu/~fdc/utf8/',),
                'source': '',
                'versions': (),
                'languages': (),
                'filename': '',
                'created': Content.REMOVE_TIME,
                'updated': Content.REMOVE_TIME,
                'uuid': Content.UUID1,
                'digest': 'a74d83df95d5729aceffc472433fea4d5e3fd2d87b510112fac264c741f20438'
            }]
        }
        data = Const.DELIMITER_DATA.join(content['data'][0]['data'])
        brief = content['data'][0]['brief']
        groups = Const.DELIMITER_GROUPS.join(content['data'][0]['groups'])
        tags = Const.DELIMITER_TAGS.join(content['data'][0]['tags'])
        links = Const.DELIMITER_LINKS.join(content['data'][0]['links'])
        cause = snippy.run(['snippy', 'create', '--content', data, '--brief', brief, '--groups', groups, '--tags', tags, '--links', links])  # pylint: disable=line-too-long
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)

        output = (
            u'1. Tagelied of Wolfram von Eschenbach S�ne kl�wen @D�sseldorf [a74d83df95d5729a]',
            u'',
            u'   $ S�ne kl�wen durh die wolken sint geslagen',
            u'   $ er st�get �f mit gr�zer kraft',
            u'',
            u'   # ??????,??????,????????',
            u'   > http://www.???????.edu/~fdc/utf8/',
            u'',
            u'OK',
            u''
        )
        out, err = capsys.readouterr()
        cause = snippy.run(['snippy', 'search', '--sall', 'kl�wen', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
