@mock.patch("IPython.display.display")
def test_scraps_report_with_scrap_string_name(mock_display, notebook_collection):
    notebook_collection.scraps_report(scrap_names="one_only")
    mock_display.assert_has_calls(
        [
            mock.call(AnyMarkdownWith("### result1")),
            mock.call(AnyMarkdownWith("#### one_only")),
            mock.call({"text/plain": "'Just here!'"}, metadata={}, raw=True),
            mock.call(AnyMarkdownWith("<hr>")),
            mock.call(AnyMarkdownWith("### result2")),
            mock.call(AnyMarkdownWith("#### one_only")),
            mock.call("No scrap found with name 'one_only' in this notebook"),
        ]
    )
