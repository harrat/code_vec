@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_delete_snippet_009(snippy):
        """Delete snippet with data.

        Try to delete snippet with content data that does not exist. In this
        case the content data is not truncated.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--content', 'not found content'])
        assert cause == 'NOK: cannot find content with content data: not found content'
        Content.assert_storage(content)
