@staticmethod
    @pytest.mark.usefixtures('exists_true', 'access_false')
    def test_export_shell_completion_005(snippy, capsys):
        """Try to export shell completion script.

        Try to export unknown shell completion script.
        """

        output = (
            'usage: snippy [-v, --version] [-h, --help] <operation> [<options>] [-vv] [-q]',
            "snippy: error: argument --complete: invalid choice: 'notfound' (choose from 'bash')",
            ''
        )
        with mock.patch('snippy.content.migrate.io.open', mock.mock_open(), create=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--complete', 'notfound'])
            out, err = capsys.readouterr()
            assert not out
            assert err == Const.NEWLINE.join(output)
            assert cause == Cause.ALL_OK
            mock_file.assert_not_called()
