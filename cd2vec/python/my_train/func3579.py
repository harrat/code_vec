def test_alias_without_parameter_forwards_to_list(cli, alias_config):
    output = cli('alias')
    lines = output.splitlines()

    assert lines == [
        "[test] active1 -> 43/1 (active project, activity 1)",
        "[test] active2 -> 43/2 (active project, activity 2)",
        "[test] inactive1 -> 42/1 (not started project, activity 1)",
        "[test] inactive2 -> 42/2 (not started project, activity 2)",
        "[test] p2_active -> 44/1 (2nd active project, activity 1)",
    ]
