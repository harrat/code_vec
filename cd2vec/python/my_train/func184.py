@mock.patch('requests.head', side_effect=mock_request)
    @mock.patch('requests.get', side_effect=mock_pyquery)
    def test_invalid_default_year_reverts_to_previous_year(self,
                                                           *args,
                                                           **kwargs):
        flexmock(utils) \
            .should_receive('_find_year_for_season') \
            .and_return('2019')

        roster = Roster('PURDUE')

        assert len(roster.players) == 2
        for player in roster.players:
            assert player.name in ['David Blough', 'Rondale Moore']
