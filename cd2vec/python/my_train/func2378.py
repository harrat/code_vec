def test_sect_tid_1004_invalid_url():
    test_url = 'http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_TID_1004.html#sect_TID_1004'
    status_code = requests.get(test_url).status_code
    assert status_code == 404, 'Section TID 1004 now has a URL format consistent with the other sections'

