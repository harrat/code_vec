def test_version_handle_purge_v_and_release_prefix(self):
        runner('git tag -a release_v1.0.0 -m "test message"')
        v = relic.release.get_info()
        assert 'release_' not in v.short
        assert 'v' not in v.short
        assert v.short == '1.0.0'
