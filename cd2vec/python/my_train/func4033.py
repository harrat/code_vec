def test_variable_string(good_data):
    """ Test unsized (variable) string fields """
    string = six.text_type(good_data, encoding='utf8')
    packet = VariablePacket(string=string)
    raw    = packet.pack()

    assert packet['string'] == string
    assert packet.pack()    == raw

def test_static_string(good_data):
    """ Test statically sized string fields """
    string = six.text_type(good_data, encoding='utf8')
    packet1 = StaticPacket(string=string)
    raw1    = packet1.pack()
    packet2 = StaticPacket.from_raw(raw1)
    raw2    = struct.pack('>256s', good_data)
    packet3 = StaticPacket()
    packet3.unpack(raw2)
