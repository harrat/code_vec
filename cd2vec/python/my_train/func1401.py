def test_cancel_with_invalid_uuid(cert_and_key):
    c = bankid.BankIDJSONClient(certificates=cert_and_key, test_server=True)
    invalid_order_ref = uuid.uuid4()
    with pytest.raises(bankid.exceptions.InvalidParametersError):
        cancel_status = c.cancel(str(invalid_order_ref))
