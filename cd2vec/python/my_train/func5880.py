def test_partially_needed_symlink_walk(self):
        os.symlink(__file__, join(self.dir, '.vimrc'))
        results = list(stowage.needed_symlink_walk(self.dir, self.dir))
        results_set = set(results)
        assert len(results_set) == len(results)  # ensure no dupes
        assert results_set == set([
            (join(self.dir, '_config/openbox/openbox.xml'),
                join(self.dir, '.config/openbox/openbox.xml')),
        ])
