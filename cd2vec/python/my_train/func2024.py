def test_is_allowed_allow_owner_not_owner(self):
        """
        Test that an access check resulting in an "Allow Owner" policy is
        processed correctly when the user requesting access is not the owner.
        """
        e = engine.KmipEngine()
        e.get_relevant_policy_section = mock.Mock(
            return_value={
                enums.ObjectType.SYMMETRIC_KEY: {
                    enums.Operation.GET: enums.Policy.ALLOW_OWNER
                }
            }
        )

        result = e.is_allowed(
            'test_policy',
            'test_user_A',
            'test_group',
            'test_user_B',
            enums.ObjectType.SYMMETRIC_KEY,
            enums.Operation.GET
        )
        self.assertFalse(result)
