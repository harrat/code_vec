def test_save_load():

    ds.save(file_path=out_file)
    reloaded_ds = ds.__class__(dataset_path=out_file)

    if ds != reloaded_ds:
        raise IOError('Error in save/load implementation!')

    must_have_attr = ('_data', '_targets',
                      '_dtype', '_target_type', '_description',
                      '_num_features', '_feature_names',
                      '_attr', '_attr_dtype', '_dataset_attr')

    for attr in must_have_attr:
        if not hasattr(reloaded_ds, attr):
            raise AttributeError('Attribute {} missing after reload from disk'
                                 ''.format(attr))

        orig_val = getattr(ds, attr)
        reloaded = getattr(reloaded_ds, attr)

        not_equal = False
        try:
            if isinstance(orig_val, dict):
                for key, val in orig_val.items():
                    if _not_equal(val, reloaded[key]):
                        warn('Values differ for attr {} in samplet {}'
                                         ' when reloaded from disk'.format(attr, key))
                        not_equal = True
                        break
            elif is_iterable_but_not_str(orig_val):
                for aa, bb in zip(orig_val, reloaded):
                    if aa != bb:
                        not_equal= True
                        break
                # not_equal = any(np.not_equal(orig_val, reloaded))
            elif np.issubdtype(type(orig_val), np.generic):
                not_equal = reloaded != orig_val
            else:
                raise TypeError('Unrecognized type {} for attr {}'
                                ''.format(type(orig_val), attr))
        except:
            raise

        if not isinstance(not_equal, bool):
            not_equal = any(not_equal)

        if not_equal:
            raise AttributeError('Attribute {} differs between the reloaded'
                                 ' and the original datasets'.format(attr))
