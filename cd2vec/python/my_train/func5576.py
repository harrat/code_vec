@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_unicode_stdout_and_return(fixture, request):
    engine = request.getfixturevalue(fixture)
    fn = pyccc.PythonCall(function_tests.fn_prints_unicode)
    job = engine.launch(image=PYIMAGE, command=fn, interpreter=PYVERSION)
    print(job.rundata)
    job.wait()
    assert job.result == u'�'
    assert job.stdout.strip() == u'�'
    assert job.stderr.strip() == u'�'

