def test_noccr_commands():
    Breathe.add_commands(
        AppContext("firefox"),
        {"dictation <text>": DoNothing(), "testing static": DoNothing()},
        ccr=False,
    )
    engine.mimic(["testing", "static"], executable="firefox")
    with pytest.raises(MimicFailure):
        engine.mimic(["dictation", "TESTING"])
        engine.mimic(["testing", "static", "testing", "static"], executable="firefox")
    engine.mimic(["dictation", "TESTING"], executable="firefox")

