def test_app_404(empty_module):
    """
    Test the scenario where your handler is wrapped by another function, such as an access or capability checker.

    E.g.

    @lambda_proxy.get
    @check_access
    def get_handler(*args, **kwargs):
        pass

    We want the wrapper to be able to return a response.

    :param empty_module:
    :return:
    """
    def test_wrapper(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            lambda_proxy.exit_with_response(404, {'body': 'Not found.', 'headers': {'Content-Type': 'text/html'}})
            return f(*args, **kwargs)
        return wrapper

    @test_wrapper
    def mock_function(*args, **kwargs):
        return {
            'statusCode': 200,
            'body': 'GOOD TO GO'
        }

    setattr(empty_module, 'mock_function', mock_function)
    app.create(empty_module)
    assert hasattr(empty_module, 'empty_controller'), 'The empty module has controller attributes.'
    empty_controller = importlib.import_module('fixtures.lambda_app.functions.empty_controller.handler')
    setattr(empty_controller, 'mock_get_handler', mock_function)
    setattr(empty_controller.mock_get_handler, '__module__', 'fixtures.lambda_app.functions.empty_controller.handler')
    lambda_proxy.get(empty_controller.mock_get_handler)
    result = empty_module.empty_controller({'body': None, 'httpMethod': 'GET'}, {})
    assert result == {'body': 'Not found.', 'statusCode': 404, 'headers': {'Content-Type': 'text/html', 'Access-Control-Allow-Origin': '*'}}

