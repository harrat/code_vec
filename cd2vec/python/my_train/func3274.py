def test_check_failure(run_reframe):
    returncode, stdout, _ = run_reframe(
        checkpath=['unittests/resources/checks/frontend_checks.py'],
        more_options=['-t', 'BadSetupCheck']
    )
    assert 'FAILED' in stdout
    assert returncode != 0

