def test_metrics(metrics_fxt):
    single_metrics, multiple_metrics = metrics_fxt

    s_metrics = single_metrics.compute_metrics()
    assert isinstance(s_metrics, np.ndarray)
    assert np.shape(s_metrics) == (4,)

    m_metrics = multiple_metrics.compute_metrics()
    assert isinstance(m_metrics, np.ndarray)
    assert np.shape(m_metrics) == (4, 5)

def test_linear():
    a = np.array([1,1])
    b = np.array([2,2])
