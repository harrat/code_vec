def test_defined_wont_fit():
    progress_bar = ProgressBar(2000, max_width=33)
    assert '  0% (    0/2,000) [] eta --:-- |' == str(progress_bar)

    progress_bar = ProgressBar(2000, max_width=30)
    assert '  0% (    0/2,000) [] eta --:-- /' == str(progress_bar)

