def test_1():
    """Test_1"""
    subprocess.run(["python3", "./GC_analysis/GC_analysis.py",
                    "-i", "./tests/ex2.fasta",
                    "-o", "./tests/ex2_5_3_test",
                    "-w", "5",
                    "-s", "3"])
    assert filecmp.cmp("./tests/ex2_5_3_test.wig", "./tests/ex2_5_3.wig")

