def test_dispatch__http_response(self):
        def callback(request):
            return HttpResponse("eek")

        target = containers.ApiInterfaceBase()
        operation = Operation(callback)
        actual = target.dispatch(operation, MockRequest())

        assert actual.body == 'eek'
        assert actual.status == 200
