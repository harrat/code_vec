def test_calc_density(class_objects, params_objects):
    d_source, d_class, s_width = class_objects
    obj = CalculateProducts(d_source, params_objects)
    obj.data.z = np.array([1, 2, 3])
    compare = obj.data.z * 3.67 ** 6 / obj.parameters['Do'] ** 6
    testing.assert_array_almost_equal(obj._calc_density(), compare)

