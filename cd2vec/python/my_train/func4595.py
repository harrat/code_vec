def test_process(self):
        """Test process method."""
        self.skill.logic = {}
        @self.skill.launch
        def sample_func():
            """Decorated function."""
            self.skill.response.set_speech_text('Welcome to skillful.')
            self.skill.response.set_reprompt_ssml('<speak>Hello.</speak>')
        actual = self.skill.process(data.SAMPLE_LAUNCH_REQUEST)
        self.assertRegexpMatches(actual, '"version": "1.0"')
        self.assertRegexpMatches(actual, '"text": "Welcome to skillful."')
        self.assertRegexpMatches(actual, '"shouldEndSession": false')
        self.assertRegexpMatches(actual, '"ssml": "<speak>Hello.</speak>"')
