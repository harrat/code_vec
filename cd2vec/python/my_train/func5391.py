def test_add_samples_reverse_file(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE, rs=True)
        data = audiofile.audio_samples
        old_length = audiofile.audio_length
        audiofile.add_samples(data, reverse=True)
        new_length = audiofile.audio_length
        self.assertAlmostEqual(new_length, 2 * old_length, places=1)
