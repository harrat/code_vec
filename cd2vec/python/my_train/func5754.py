@staticmethod
    @pytest.mark.usefixtures('template-utc')
    def test_cli_export_solution_028(snippy):
        """Export solution template.

        Export solution template by explicitly defining content category.
        This must result file name and format based on the tool internal
        default settings.
        """

        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '--template'])
            assert cause == Cause.ALL_OK
            mock_file.assert_called_once_with('./solution-template.mkdn', mode='w', encoding='utf-8')
            file_handle = mock_file.return_value.__enter__.return_value
            file_handle.write.assert_called_with(Const.NEWLINE.join(Solution.TEMPLATE_MKDN))
