def test_create_payment_ecommerce(self):
        payer_alias = '467%i' % randint(1000000, 9999999)
        payment = self.client.create_payment(
            payee_payment_reference='0123456789',
            callback_url='https://example.com/api/swishcb/paymentrequests',
            payer_alias=payer_alias,
            amount=100,
            currency='SEK',
            message='Kingston USB Flash Drive 8 GB'
        )
        self.assertIsNotNone(payment.id)
        self.assertIsNotNone(payment.location)
