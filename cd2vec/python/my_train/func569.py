def test_invalid_contents_key(self):
        lang_content = copy.deepcopy(self.valid_language_content)
        lang_content['contents']['espa�ol'] = {
            "identity_class": u"organizaci�n"
        }
        self.assertFalseWithOptions(lang_content)
