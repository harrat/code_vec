@pytest.mark.core
def testConfigSetDataPath(nwTemp):
    assert theConf.setDataPath(None)
    assert not theConf.setDataPath(path.join("somewhere","over","the","rainbow"))
    assert theConf.setDataPath(nwTemp)
    assert theConf.dataPath == nwTemp
    assert not theConf.confChanged

@pytest.mark.core
def testConfigLoad():
    assert theConf.loadConfig()
    assert not theConf.confChanged
