def test_tee_fileoutput_no_newline(self):
        file1 = self.root.make_file(suffix='.gz')
        file2 = self.root.make_file()
        with textoutput((file1, file2)) as o:
            o.writeline('foo')
            o.writeline('bar')
            assert 2 == o.num_lines
        with gzip.open(file1, 'rb') as i:
            assert b'foo\nbar\n' == i.read()
        with open(file2, 'rb') as i:
            assert b'foo\nbar\n' == i.read()
