def test_request_should_not_include_token(self):
        """
        Test that the access token is not included in the call to
        requests if it isn't actually set.
        """
        fake_client = Client()

        with patch("requests.request") as request:
            request.return_value.json.return_value = {}

            fake_client.request("GET", "http://www.google.com/")

        request.assert_called_once_with(
            "GET",
            "http://www.google.com/",
            headers={},
            json=None,
            params=b"per_page=100",
        )
