@staticmethod
    @pytest.mark.usefixtures('exists_true', 'access_false')
    def test_export_shell_completion_004(snippy):
        """Try to export bash completion script.

        Try to export Bash completion script. In this case the ``--file``
        option is set to a file path is not writable.
        """

        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            cause = snippy.run(['snippy', 'export', '--complete', 'bash', '-f', '/root/noaccess/snippy'])
            assert cause == 'NOK: cannot export bash completion file because path is not writable /root/noaccess/snippy'
            mock_file.assert_not_called()
