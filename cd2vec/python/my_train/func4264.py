@skipIf(no_internet(), "No internet connection")
    def test_xopen_url(self):
        badurl = 'http://google.com/__badurl__'
        with self.assertRaises(ValueError):
            xopen(badurl)
        url = 'https://github.com/jdidion/xphyle/blob/master/tests/foo.gz?raw=True'
        with self.assertRaises(ValueError):
            xopen(url, 'w')
        with open_(url, 'rt') as i:
            assert 'gzip' == i.compression
            assert 'foo\n' == i.read()
