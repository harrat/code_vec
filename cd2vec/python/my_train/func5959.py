@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'default-solutions', 'import-netcat')
    def test_cli_search_snippet_022(snippy, capsys):
        """Search snippets with ``filter`` option.

        Search all content with a regexp filter. The filter removes all
        content from the search result returned with the `sall` option
        that do not match to the regexp in any of the resource field. In
        this case none of the resulting content match to the filter and
        no content is found.
        """

        output = 'NOK: cannot find content with given search criteria\n'
        cause = snippy.run(['snippy', 'search', '--sall', '.', '--filter', 'not-found'])
        out, err = capsys.readouterr()
        assert cause == 'NOK: cannot find content with given search criteria'
        assert out == output
        assert not err
