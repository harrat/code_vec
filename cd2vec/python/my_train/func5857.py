@mock.patch("IPython.display.display")
def test_reglue_scrap(mock_display, notebook_result):
    notebook_result.reglue("one")
    mock_display.assert_called_once_with(
        {
            "application/scrapbook.scrap.json+json": {
                "name": "one",
                "data": 1,
                "encoder": "json",
                "version": 1,
            }
        },
        metadata={"scrapbook": {"name": "one", "data": True, "display": False}},
        raw=True,
    )
