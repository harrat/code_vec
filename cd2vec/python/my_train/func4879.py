def test_new_task_bad_inputs(self):
        """
        Trying to create a new asyncio task with invalid inputs must raise
        a `TypeError` exception.
        """
        # 1 (mandatory) positional argument (None) passed to the coroutine
        # whereas the coroutine takes no argument
        with self.assertRaisesRegex(TypeError, 'positional argument'):
            new_task('task-bad-coro')
