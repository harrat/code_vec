def test_file_invalid_is_encrypted(self):
        observed_data = {
            "type": "artifact",
            "id": "artifact--ff1e0780-358c-5808-a8c7-d0fca4ef6ef4",
            "mime_type": "application/zip",
            "payload_bin": "VBORw0KGgoAAAANSUhEUgAAADI==",
            "decryption_key": "My voice is my passport"
        }
        self.assertFalseWithOptions(observed_data)
