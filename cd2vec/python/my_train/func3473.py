@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_reference_001(snippy):
        """Import all reference resources.

        Import all references. File name is not defined in command line. This
        should result tool internal default file name and format being used.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        file_content = Content.get_file_content(Content.MKDN, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'reference'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './references.mkdn', mode='r', encoding='utf-8')
