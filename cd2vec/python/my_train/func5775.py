@check_all_backends
def test_sort_by_keys(backend):
    check_import(backend)

    # Given
    nparr1 = np.random.randint(0, 100, 16, dtype=np.int32)
    nparr2 = np.random.randint(0, 100, 16, dtype=np.int32)
    dev_array1, dev_array2 = array.wrap(nparr1, nparr2, backend=backend)

    # When
    out_array1, out_array2 = array.sort_by_keys([dev_array1, dev_array2])

    # Then
    order = np.argsort(nparr1)
    act_result1 = np.take(nparr1, order)
    act_result2 = np.take(nparr2, order)
    assert np.all(out_array1.get() == act_result1)
    assert np.all(out_array2.get() == act_result2)

