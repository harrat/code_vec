@pytest.mark.vcr()
def test_scan_export_format_typeerror(api):
    with pytest.raises(TypeError):
        api.scans.export(1, format=1)
