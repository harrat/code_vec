@pytest.mark.parametrize('fieldstr,context,outfield,exception', [
		(Raw('s.t.f'), None, 's.t.f', None),
		('s.t.f', None, '"s"."t"."f"', None),
		('s.t.f', "group", '"s"."t"."f"', None),
		('s.t.f', "group2", None, FieldParseError),
		('s.t.f|count(f1)', 'select', 'COUNT("s"."t"."f") AS "f1"', None),
		('s.t.f|.count(f1)', 'select', 'COUNT(DISTINCT "s"."t"."f") AS "f1"', None),
		('s.t.f|c.ount(f1)', 'select', None, FieldParseError)
	])
	def testParse(self, fieldstr, context, outfield, exception):
		if exception:
			with pytest.raises(exception):
				Field.parse(fieldstr, context)
		else:
			assert Field.parse(fieldstr, context) == outfield
