def test_compress_file(self):
        b = (True, False) if gz_path else (False,)
        for use_system in b:
            with self.subTest(use_system=use_system):
                path = self.root.make_file()
                with open(path, 'wt') as o:
                    o.write('foo')
                with open(path, 'rb') as i:
                    fmt = get_format('.gz')
                    dest = fmt.compress_file(i, use_system=use_system)
                gzfile = Path(str(path) + '.gz')
                assert dest == gzfile
                self.assertTrue(os.path.exists(gzfile))
                with gzip.open(gzfile, 'rt') as i:
                    assert i.read() == 'foo'
