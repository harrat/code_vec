@patch('yaml.safe_load')
@patch('sys.stderr', new_callable=StringIO)
@patch.object(safe_print, 'open', create=True)
def test_safe_print_color(open: MagicMock, stderr: MagicMock, yaml_load: MagicMock):
    yaml_load.return_value = {'colors': True}
    with patch.object(os, 'name', 'nt'):
        safe_print('Text', color=colorama.Fore.RED)
    assert colorama.Fore.RED not in stderr.getvalue()
    with patch.object(os, 'name', 'darwin'):
        safe_print('Text', color=colorama.Fore.RED)
    assert colorama.Fore.RED in stderr.getvalue()

