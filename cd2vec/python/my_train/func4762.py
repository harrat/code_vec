def test_text(self):
        text = Text.parse('(fp_text user text (at 0.0 0.0) (layer F.SilkS))')
        assert text.type == 'user'
        assert text.text == 'text'
        assert text.at == [0.0, 0.0]
        assert text.layer == 'F.SilkS'
        assert Text.parse(text.to_string()) == text
