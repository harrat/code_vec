def test_invalid_property_prefix(self):
        attack_pattern = copy.deepcopy(self.valid_attack_pattern)
        attack_pattern['x-something'] = "some value"
        results = validate_parsed_json(attack_pattern, self.options)
        self.assertEqual(results.is_valid, False)

        self.assertFalseWithOptions(attack_pattern, enabled='custom-prefix-lax')
