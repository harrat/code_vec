@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_delete_solution_011(snippy):
        """Delete solution with data.

        Try to delete solution with content data that does not exist. In this
        case the content data is truncated.
        """

        content = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        data = Content.dump_text(Solution.KAFKA)
        cause = snippy.run(['snippy', 'delete', '--scat', 'solution', '--content', data])
        assert cause == 'NOK: cannot find content with content data: ##############################...'
        Content.assert_storage(content)
