def test_sqlalchemy_data_layer_get_relationship_field_not_found(session, person_model):
    with pytest.raises(RelationNotFound):
        dl = SqlalchemyDataLayer(dict(session=session, model=person_model))
        dl.get_relationship('error', '', '', dict(id=1))
