def test_evaluate(self):
        random_agent = RandomAgent(_line_world_name)
        num_episodes = 5
        metrics = random_agent.evaluate(num_episodes=num_episodes)
        self.assert_properties_for_metric(metrics.steps, num_episodes)
        self.assert_properties_for_metric(metrics.rewards, num_episodes)
