def test_unique_check():
	import copy
	from io import StringIO
	header_descriptor_ = copy.copy(header_descriptor)
	header_descriptor_["name"]["modifiers"] = ["unique"]
	header = ljson.base.generic.Header(header_descriptor_)
	table = ljson.base.mem.Table(header, data)

	fio = StringIO()

	table.save(fio)
	fio.seek(0)

	table = ljson.base.disk.Table.from_file(fio)


	table.additem(item_meg)

	import pytest

	with pytest.raises(ValueError):
		table.additem({"age": 12, "name": "chris",
				"lname": "griffin"})
