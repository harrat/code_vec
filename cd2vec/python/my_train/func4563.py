def test_region_context_manager(region):
    with region as r:
        r['key1'] = 0
        r['key2'] = 1

    assert 'key1' in region
    assert 'key2' in region
    assert region._region_cache.conn.hget(region.name, 'key1') is not None
    assert region._region_cache.conn.hget(region.name, 'key2') is not None

