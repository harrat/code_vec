def test_is_terminal(self):
        self.graph.add_vertex("a")
        edge = self.graph.add_edge("a", "b", "ab")
        assert self.graph.is_terminal(edge, 'a') and \
            self.graph.is_terminal(edge, 'b')
