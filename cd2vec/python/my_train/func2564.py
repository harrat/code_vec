def test_importanize_invalid_python(self) -> None:
        result = next(
            run_importanize_on_source(self.invalid, RuntimeConfig(_config=self.config))
        )

        assert not result.is_success
