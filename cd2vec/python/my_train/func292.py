def test_add(mbed, testrepos):
    test3 = testrepos[2]

    with cd('test1'):
        popen(['python', mbed, 'add', test3, "-vv"])

    assertls(mbed, 'test1', [
        "[mbed]",
        "test1",
        "|- test2",
        "|  `- test3",
        "|     `- test4",
        "`- test3",
        "   `- test4",
    ])
