def test_scan_bad_archive(mkv):
    with pytest.raises(ValueError) as excinfo:
        scan_archive(mkv['test1'])
    assert excinfo.value.args == ("'.mkv' is not a valid archive", )

