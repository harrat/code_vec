def test_defined():
    progress_bar = ProgressBarBits(2000)

    assert '  0% (0.00/2.00 kb) [                ] eta --:-- /' == str(progress_bar)
    assert '  0% (0.00/2.00 kb) [                ] eta --:-- -' == str(progress_bar)
    assert '  0% (0.00/2.00 kb) [                ] eta --:-- \\' == str(progress_bar)

    eta._NOW = lambda: 1411868722.0
    progress_bar.numerator = 102
    assert '  5% (0.10/2.00 kb) [                ] eta --:-- |' == str(progress_bar)
    assert '  5% (0.10/2.00 kb) [                ] eta --:-- /' == str(progress_bar)

    eta._NOW = lambda: 1411868722.5
    progress_bar.numerator = 281
    assert ' 14% (0.28/2.00 kb) [##              ] eta 00:05 -' == str(progress_bar)

    eta._NOW = lambda: 1411868723.0
    progress_bar.numerator = 593
    assert ' 29% (0.59/2.00 kb) [####            ] eta 00:03 \\' == str(progress_bar)

    eta._NOW = lambda: 1411868723.5
    progress_bar.numerator = 1925
    assert ' 96% (1.92/2.00 kb) [############### ] eta 00:01 |' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 1999
    assert ' 99% (1.99/2.00 kb) [############### ] eta 00:01 /' == str(progress_bar)

    eta._NOW = lambda: 1411868724.5
    progress_bar.numerator = 2000
    assert '100% (2.00/2.00 kb) [################] eta 00:00 -' == str(progress_bar)
    assert '100% (2.00/2.00 kb) [################] eta 00:00 \\' == str(progress_bar)
    assert '100% (2.00/2.00 kb) [################] eta 00:00 |' == str(progress_bar)

