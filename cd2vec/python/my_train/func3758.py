def test_index_dir(tmpdir):
    tmpdir.mkdir('spam').join('foo.txt').write('foo')
    tmpdir.mkdir('index')
    with tmpdir.as_cwd():
        commands.index('spam')
        assert os.path.samefile(
            'index/2c/26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae.txt',
            'spam/foo.txt')
