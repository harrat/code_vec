def test_undefined():
    misc.terminal_width = lambda: 40
    progress_bar = ProgressBar(None, max_width=30)

    assert '0 [?             ] eta --:-- /' == str(progress_bar)
    assert '0 [ ?            ] eta --:-- -' == str(progress_bar)
    assert '0 [  ?           ] eta --:-- \\' == str(progress_bar)

    eta._NOW = lambda: 1411868722.0
    progress_bar.numerator = 10
    assert '10 [   ?         ] eta --:-- |' == str(progress_bar)
    assert '10 [    ?        ] eta --:-- /' == str(progress_bar)

    eta._NOW = lambda: 1411868722.5
    progress_bar.numerator = 100
    assert '100 [     ?      ] eta --:-- -' == str(progress_bar)

    eta._NOW = lambda: 1411868723.0
    progress_bar.numerator = 1954727
    assert '1,954,727 [    ? ] eta --:-- \\' == str(progress_bar)
    assert '1,954,727 [   ?  ] eta --:-- |' == str(progress_bar)

