def test_valid_user(self):
        '''
        Check with an authorized user.
        '''
        with self.login(username=self.user1.username):
            self.assertGoodView(self.view_string, **self.url_kwargs)
            assert self.last_response.get('Content-Disposition') == 'attachment; filename="dark-embrace.opml"'
            assert self.get_context('outline') == self.o1
            print(self.last_response.content)
            try:
                f = io.BytesIO(self.last_response.content)
                ET.ElementTree(file=f)
            except ET.ParseError as PE:
                assert not str(PE)  # Failing the test a bit more gracefully.
