@mock.patch('requests.get', side_effect=mock_pyquery)
    @mock.patch('requests.head', side_effect=mock_request)
    def test_invalid_default_year_reverts_to_previous_year(self,
                                                           *args,
                                                           **kwargs):
        results = {
            'week': 2,
            'boxscore_index': '201709170nor',
            'day': 'Sun',
            'date': 'September 17',
            'type': REGULAR_SEASON,
            'datetime': datetime(2017, 9, 17, 0, 0),
            'result': WIN,
            'overtime': 0,
            'location': AWAY,
            'opponent_abbr': 'NOR',
            'opponent_name': 'New Orleans Saints',
            'points_scored': 36,
            'points_allowed': 20,
            'pass_completions': 30,
            'pass_attempts': 39,
            'pass_yards': 436,
            'pass_touchdowns': 3,
            'interceptions': 0,
            'times_sacked': 2,
            'yards_lost_from_sacks': 11,
            'pass_yards_per_attempt': 11.2,
            'pass_completion_rate': 76.9,
            'quarterback_rating': 138.4,
            'rush_attempts': 31,
            'rush_yards': 119,
            'rush_yards_per_attempt': 3.8,
            'rush_touchdowns': 1,
            'field_goals_made': 3,
            'field_goals_attempted': 3,
            'extra_points_made': 3,
            'extra_points_attempted': 4,
            'punts': 3,
            'punt_yards': 111,
            'third_down_conversions': 6,
            'third_down_attempts': 12,
            'fourth_down_conversions': 0,
            'fourth_down_attempts': 0,
            'time_of_possession': '35:06'
        }
        flexmock(utils) \
            .should_receive('_find_year_for_season') \
            .and_return(2018)
        flexmock(Boxscore) \
            .should_receive('_parse_game_data') \
            .and_return(None)
        flexmock(Boxscore) \
            .should_receive('dataframe') \
            .and_return(pd.DataFrame([{'key': 'value'}]))

        schedule = Schedule('NWE')

        for attribute, value in results.items():
            assert getattr(schedule[1], attribute) == value
