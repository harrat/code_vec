@staticmethod
    @pytest.mark.usefixtures('create-remove-utc')
    def test_cli_create_snippet_010(snippy, editor_data):
        """Try to create snippet with editor.

        Try to create a new snippet by using the prefilled default Markdown
        template in editor. In this case there are no any changes made in
        editor on top of the displayed template.
        """

        template = (
            '# Add brief title for content @groups',
            '',
            '> Add a description that defines the content in one chapter.',
            '',
            '> [1] https://www.example.com/add-links-here.html',
            '',
            '`$ Markdown commands are defined between backtics and prefixed by a dollar sign`',
            '',
            '## Meta',
            '',
            '> category  : snippet  ',
            'created   : 2017-10-14T19:56:31.000001+00:00  ',
            'digest    : 8d5193fea452d0334378a73ded829cfa27debea7ee87714d64b1b492d1a4601a  ',
            'filename  : example-content.md  ',
            'languages : example-language  ',
            'name      : example content handle  ',
            'source    : https://www.example.com/source.md  ',
            'tags      : example,tags  ',
            'updated   : 2017-10-14T19:56:31.000001+00:00  ',
            'uuid      : a1cd5827-b6ef-4067-b5ac-3ceac07dde9f  ',
            'versions  : example=3.9.0,python>=3  ',
            ''
        )
        edited = template
        editor_data.return_value = Const.NEWLINE.join(edited)
        cause = snippy.run(['snippy', 'create', '--editor'])
        assert cause == 'NOK: content was not stored because it was matching to an empty template'
        editor_data.assert_called_with(Const.NEWLINE.join(template))
        Content.assert_storage(None)
