def test_errors_are_updated():
    """Assert that the found exceptions are updated in the errors attribute."""
    atom = ATOMRegressor(X_reg, y_reg, random_state=1)
    atom.run(['XGB', 'LGB'], n_calls=2, n_random_starts=2)  # No errors
    atom.run(['XGB', 'LGB'], n_calls=2, n_random_starts=(2, -1))  # Produces an error
    assert atom.errors.get('LGB')

