@pytest.mark.high
    def test_register_unknown(self):
        self.assertRaises(TypeError, self.register.register, False)
