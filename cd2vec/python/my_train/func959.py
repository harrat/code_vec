def test_pushannos():
    imageset=1
    product_id=1

    client = ApiClient(configuration)
    apis = ExactAPIs(client)

    randstr = ''.join(['{:02x}'.format(x) for x in np.random.randint(0,255,6)])
    imagename = f'dummy{randstr}.png'

    imageset =  apis.image_sets_api.list_image_sets().results[0].id

    # generate dummy image
    dummy=np.random.randint(0,255, (200,200,3))
    cv2.imwrite(imagename, dummy)

    apis.images_api.create_image(file_path=imagename, image_type=0, image_set=imageset).results
    imageset_details = apis.image_sets_api.retrieve_image_set(id=imageset, expand='images')

    imageid=imageset_details.images[0]['id']

    DB = Database().create(':memory:')

    # Add slide to database
    slideuid = DB.insertNewSlide(imagename,'')
    DB.insertClass('BB')
    DB.insertAnnotator('sliderunner_unittest')
    DB.insertAnnotator('otherexpert') # we will only send annotations of the marked expert
    DB.insertClass('POLY')

    coords = np.array([[100,200],[150,220],[180,250]])
    
    DB.insertNewPolygonAnnotation(annoList=coords, slideUID=1, classID=2, annotator=1)

    coords = np.array([[150,250],[350,220],[0,250]])
    DB.insertNewPolygonAnnotation(annoList=coords, slideUID=1, classID=2, annotator=1)

    coords = np.array([[150,255],[350,210],[50,250]])
    DB.insertNewPolygonAnnotation(annoList=coords, slideUID=1, classID=2, annotator=2)

    DB.setExactPerson(1)
    #empty image
    annos = apis.annotations_api.list_annotations(id=imageid, pagination=False).results

    for anno in annos:
        apis.annotations_api.destroy_annotation(id=anno.id)

#    for anno in exm.retrieve_annotations(imageid):
#        print(anno) 
    # All annotations have been removed
    assert(len(apis.annotations_api.list_annotations(id=imageid, pagination=False).results)==0)

    exm = ExactManager(username=configuration.username, password=configuration.password, serverurl=configuration.host)
    exm.sync(imageid, imageset_id=imageset, product_id=product_id, slideuid=slideuid, database=DB)

    # Only 2 annotations have been inserted
    assert(len(apis.annotations_api.list_annotations(imageid).results)==2)

    uuids = [x.unique_identifier for x in apis.annotations_api.list_annotations(imageid).results]
    # All were created with correct guid
    for dbanno in list(DB.annotations.keys())[:-1]:
        assert(DB.annotations[dbanno].guid in uuids)

    print('--- resync ---')

    # Sync again
    exm.sync(imageid, imageset_id=imageset, product_id=product_id, slideuid=slideuid, database=DB)

    # No change
    print('Length is now: ',len(apis.annotations_api.list_annotations(imageid).results))
    assert(len(apis.annotations_api.list_annotations(imageid).results)==2)

    # All were created with correct guid
    uuids = [x.unique_identifier for x in apis.annotations_api.list_annotations(imageid).results]
    for dbanno in list(DB.annotations.keys())[:-1]:
        assert(DB.annotations[dbanno].guid in uuids)

    print('--- local update created ---')

    # Now let's create a local update - keep same exact_id (crucial!)
    DB.loadIntoMemory(1)
    DB.setAnnotationLabel(classId=1, person=1, annoIdx=1, entryId=DB.annotations[1].labels[0].uid, exact_id=DB.annotations[1].labels[0].exact_id)

    # Sync again
    exm.sync(imageid, imageset_id=imageset, product_id=product_id, slideuid=slideuid, database=DB)

    # check if remote has been updated
#    annos = apis.annotations_api.list_annotations(id=imageid, pagination=False).results
    annos = np.array(apis.annotations_api.list_annotations(imageid, expand='annotation_type').results)

    assert(len(annos)>0)

    for anno in annos:
        if (anno.id==DB.annotations[1].labels[0].exact_id):
            assert(anno.annotation_type['name']=='BB')
            annotype_id = anno.annotation_type['id']

    # Now update remotely and see if changes are reflected
    newguid = str(uuid.uuid4())
    vector = list_to_exactvector([[90,80],[20,30]])
    vector['frame']=2
    lastModified=datetime.datetime.fromtimestamp(time.time()).strftime( "%Y-%m-%dT%H:%M:%S.%f")
    annotation = Annotation(annotation_type=annotype_id, vector=vector, image=imageid, unique_identifier=newguid, last_edit_time=lastModified, time=lastModified, description='abcdef')
    created = apis.annotations_api.create_annotation(body=annotation )

    exm.sync(imageid, imageset_id=imageset, product_id=product_id, slideuid=slideuid, database=DB)
    DB.loadIntoMemory(1, zLevel=2)
    found=False
    for annoI in DB.annotations:
        anno=DB.annotations[annoI]
        if (anno.guid == newguid):
            found=True
            assert(anno.annotationType==AnnotationType.POLYGON)
            assert(anno.text=='abcdef')

            assert(anno.labels[0].exact_id==created.id)
    
    assert(found)
    
    # also check in stored database
    DB.loadIntoMemory(1)
    for annoI in DB.annotations:
        anno=DB.annotations[annoI]
        if (anno.guid == newguid):
            found=True
            assert(anno.annotationType==AnnotationType.POLYGON)
            assert(anno.labels[0].exact_id==created.id)


    annos = apis.annotations_api.list_annotations(id=imageid, pagination=False).results
    for anno in annos:
        apis.annotations_api.destroy_annotation(anno.id, keep_deleted_element=False)


    # All gone
    assert(len(apis.annotations_api.list_annotations(id=imageid, pagination=False).results)==0)

    # Now delete image
    apis.images_api.destroy_image(id=imageid)

    os.remove(imagename)
    exm.terminate()

if __name__ == "__main__":
    test_setup()
    cleanup()
    test_images() 
    test_pushannos()    
    cleanup()
