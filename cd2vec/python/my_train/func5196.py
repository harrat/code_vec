@staticmethod
    @pytest.mark.usefixtures('default-references', 'export-time')
    def test_cli_export_reference_029(snippy):
        """Export all references.

        Export all references in Markdown format.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'reference', '--file', 'references.mkdn'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, 'references.mkdn', content)
