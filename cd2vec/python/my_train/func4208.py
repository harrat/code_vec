def test_patch_new_resource(self):
        """Send HTTP PATCH for a resource which doesn't exist (should be
        created)."""
        response = self.app.patch('/artists/276',
                content_type='application/json',
                data=json.dumps({u'Name': u'Jeff Knupp'}))
        assert response.status_code == 201
        assert json.loads(response.get_data(as_text=True))['Name'] == u'Jeff Knupp'
        assert json.loads(response.get_data(as_text=True))['self'] == '/artists/276'
