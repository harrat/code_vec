def test_parse_comes_from(chdir):
    forig = "tests/fixtures/requirements.txt"
    fname, line = file_utils.parse_comes_from("-r {} (line 11)".format(forig), "dev")
    assert fname == forig
    assert line == 11

