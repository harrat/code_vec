@pytest.mark.vcr()
def test_agents_list_sort_direction_unexpectedvalue(api):
    with pytest.raises(UnexpectedValueError):
        api.agents.list(sort=(('uuid', 'nope'),))
