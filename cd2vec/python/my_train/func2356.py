@mark.parametrize(('input_', 'expected'), test_pairs.items())
def test_parse(input_, expected):
    with open(os.path.join(test_suite_dir, input_), 'rb') as f:
        xml = f.read()
        if IRON_PYTHON:
            xml = bytes(xml)
    parse = get_format(xml)
    assert callable(parse)
    uri_filename = input_.rstrip('.xml') + '.uri.txt'
    try:
        with open(os.path.join(test_suite_dir, uri_filename)) as f:
            base_uri = f.read().strip()
    except (IOError, OSError):
        base_uri = 'http://example.com/'
    parsed_feed, _ = parse(xml, feed_url=base_uri)
    parsed_tree = fromstringlist(
        write(parsed_feed, canonical_order=True, hints=False)
    )
    if IRON_PYTHON:
        open_ = functools.partial(io.open, encoding='utf-8')
    elif PY3:
        open_ = functools.partial(open, encoding='utf-8')
    else:
        open_ = open
    with open_(os.path.join(test_suite_dir, expected)) as f:
        expected_tree = fromstringlist(f.read() if IRON_PYTHON else f)
    compare_tree(expected_tree, parsed_tree)

