def test_args_override_env(config, mock_args, mock_env):
    correct, incorrect = 'correct', 'incorrect'
    mock_env({'ATTRIBUTE_STR': incorrect})
    assert config.ATTRIBUTE_STR == incorrect
    mock_args(f'--attribute-str {correct}')
    assert config.ATTRIBUTE_STR == correct

