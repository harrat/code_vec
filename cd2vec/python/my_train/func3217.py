def test_pack_unpack_anon(alice, bob):
    """ Test the pack-unpack loop with anoncrypt. """
    msg = {'@type': 'doc;protocol/1.0/name'}
    packed_msg = alice.pack(msg, anoncrypt=True)
    assert isinstance(packed_msg, bytes)

    unpacked_msg = bob.unpack(packed_msg)
    assert isinstance(unpacked_msg, Message)
    assert hasattr(unpacked_msg, 'mtc')
    assert unpacked_msg.mtc.is_anoncrypted()
    assert unpacked_msg.mtc.sender is None
    assert unpacked_msg.mtc.recipient == bob.verkey_b58

