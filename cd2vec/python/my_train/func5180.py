def test_modify_attribute_kmip_2_0_with_multivalued_no_current(self):
        """
        Test that a KmipError is raised when attempting to modifyg a
        multivalued attribute with no current attribute.
        """
        e = engine.KmipEngine()
        e._protocol_version = contents.ProtocolVersion(2, 0)
        e._attribute_policy._version = e._protocol_version
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        args = (
            payloads.ModifyAttributeRequestPayload(
                unique_identifier="1",
                new_attribute=objects.NewAttribute(
                    attribute=attributes.Name(
                        name_value=attributes.Name.NameValue("Modified Name")
                    )
                )
            ),
        )
        self.assertRaisesRegex(
            exceptions.KmipError,
            "The 'Name' attribute is multivalued so the current attribute "
            "must be specified.",
            e._process_modify_attribute,
            *args
        )
