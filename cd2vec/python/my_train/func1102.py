def test_xopen_compressed_stream(self):
        # Try autodetect compressed
        with intercept_stdin(gzip.compress(b'foo\n'), is_bytes=True):
            with xopen(
                    STDIN, 'rt', compression=True, context_wrapper=True) as i:
                assert cast(StdWrapper, i).compression == 'gzip'
                assert i.read() == 'foo\n'
