def test_post_unsupported_content_type(self):
        """Test POSTing with an unsupported Content-type."""
        response = self.app.post('/artists',
                content_type='foobar',
                data={'Foo': 'bar'})
        assert response.status_code == 415
