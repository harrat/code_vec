def test_unregister():
    f1 = NodeFactory()
    class TestWriter(AbstractFileWriter):
        def __init__(self, configuration, instance_name):
            pass
        def write(self, data_dict):
            pass

    n_before = len(set(f1.name_dict.keys()))
    f1.register("TestWriter", TestWriter)
    n_after = len(set(f1.name_dict.keys()))
    assert n_after == n_before + 1

    f1.unregister('TestWriter')

    assert len(set(f1.name_dict.keys())) == n_before

    with pytest.raises(Exception) as e:
        f1.unregister('doesnotexist')
    assert 'Key not found doesnotexist' in str(e)

def test_valid_configuration():
    f = NodeFactory()
    class TestWriter(AbstractFileWriter):
        def __init__(self, configuration, instance_name):
            super(TestWriter, self).__init__(configuration, instance_name)
        @staticmethod
        def necessary_config(node_config):
            return set(['key1', 'key2'])
        def write(self, data_dict):
            pass
