def test_get_with_limit(self):
        """Test simple HTTP GET"""
        response = self.get_response('/artists', 200, params={'limit': 10})
        assert len(json.loads(response.get_data(as_text=True))[u'resources']) == 10
