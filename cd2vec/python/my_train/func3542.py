def test_aggregate_input_data(self, caplog, tmp_dir):
        res = wrapper.Output(target_root_name)
        res.extract_results()
        res.aggregate_input_data()
        assert "Aggregating input data" in caplog.text
        assert os.path.isfile(res.root_out_name+".tsv")
        ref_file = target_root_path + "_out.tsv"
        assert filecmp.cmp(ref_file, res.root_out_name + ".tsv", shallow=False)
