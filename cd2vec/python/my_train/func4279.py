def test_get_sdk_instance_without_init_throws(self):
        with self.assertRaises(SecureNativeSDKIllegalStateException):
            SecureNative.get_instance()
