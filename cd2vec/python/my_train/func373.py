@pytest.mark.parametrize('launch_method', [
    'spawn',
    pytest.param('forkserver', marks=needs_unix_socket_mark)
])
def test_run_import_failure(capsys, benchmarks_fixture, launch_method):
    conf, repo, envs, commit_hash = benchmarks_fixture

    with open(os.path.join('benchmark', 'unimportable.py'), 'w') as f:
        f.write('def track_unimportable(): pass')

    b = benchmarks.Benchmarks.discover(conf, repo, envs, [commit_hash])

    skip_names = [name for name in b.keys()
                  if name not in ('time_secondary.track_value', 'unimportable.track_unimportable')]
    b2 = b.filter_out(skip_names)

    #
    # Module with import raising an exception
    #

    with open(os.path.join('benchmark', 'unimportable.py'), 'w') as f:
        f.write('import sys; sys.stderr.write("hello import"); sys.stderr.flush()\n')
        f.write('raise SystemExit(0)')

    results = runner.run_benchmarks(b2, envs[0], show_stderr=False, launch_method=launch_method)
    times = ResultsWrapper(results, b2)

    assert times['time_secondary.track_value'].errcode == 0
    assert times['time_secondary.track_value'].result == [42]

    assert times['unimportable.track_unimportable'].errcode != 0
    err = times['unimportable.track_unimportable'].stderr
    assert 'hello import' in err

    text, err = capsys.readouterr()
    assert 'hello import' not in text

    #
    # Module with import crashing the process
    #

    clear_pyc('benchmark')

    with open(os.path.join('benchmark', 'unimportable.py'), 'w') as f:
        f.write('import sys; sys.stderr.write("hello import"); sys.stderr.flush()\n')
        f.write('import os; os._exit(0)')

    results = runner.run_benchmarks(b2, envs[0], show_stderr=True, launch_method=launch_method)
    times = ResultsWrapper(results, b2)

    assert times['unimportable.track_unimportable'].errcode != 0
    assert times['unimportable.track_unimportable'].stderr

    # track_value may run (spawn) or not (forkserver), so don't check for it

    text, err = capsys.readouterr()

    #
    # Module with import printing output
    #

    clear_pyc('benchmark')

    with open(os.path.join('benchmark', 'unimportable.py'), 'w') as f:
        f.write('import sys; sys.stderr.write("hello import"); sys.stderr.flush()\n')

    results = runner.run_benchmarks(b2, envs[0], show_stderr=True, launch_method=launch_method)
    times = ResultsWrapper(results, b2)

    assert times['time_secondary.track_value'].errcode == 0
    assert times['unimportable.track_unimportable'].errcode != 0

    text, err = capsys.readouterr()
    assert 'hello import' in text

