@unittest.skipIf(platform.system() == "Windows" or platform.system() == "windows", "test not supported on windows")
    def test_parse_config_file_correctly(self):
        try:
            os.remove(self.config_file_path)
            del os.environ["SECURENATIVE_API_KEY"]
            del os.environ["SECURENATIVE_API_URL"]
            del os.environ["SECURENATIVE_INTERVAL"]
            del os.environ["SECURENATIVE_MAX_EVENTS"]
            del os.environ["SECURENATIVE_TIMEOUT"]
            del os.environ["SECURENATIVE_AUTO_SEND"]
            del os.environ["SECURENATIVE_DISABLE"]
            del os.environ["SECURENATIVE_LOG_LEVEL"]
            del os.environ["SECURENATIVE_FAILOVER_STRATEGY"]
        except FileNotFoundError:
            pass
        except KeyError:
            pass

        config = {
            "SECURENATIVE_API_KEY": "SOME_API_KEY",
            "SECURENATIVE_APP_NAME": "SOME_APP_NAME",
            "SECURENATIVE_API_URL": "SOME_API_URL",
            "SECURENATIVE_INTERVAL": "1000",
            "SECURENATIVE_HEARTBEAT_INTERVAL": "5000",
            "SECURENATIVE_MAX_EVENTS": "100",
            "SECURENATIVE_TIMEOUT": "1500",
            "SECURENATIVE_AUTO_SEND": "True",
            "SECURENATIVE_DISABLE": "False",
            "SECURENATIVE_LOG_LEVEL": "Critical",
            "SECURENATIVE_FAILOVER_STRATEGY": "fail-closed"
        }

        self.create_ini_file(config)
        options = ConfigurationManager.load_config(None)

        self.assertIsNotNone(options)
        self.assertEqual(options.api_key, "SOME_API_KEY")
        self.assertEqual(options.api_url, "SOME_API_URL")
        self.assertEqual(options.auto_send, "True")
        self.assertEqual(options.disable, "False")
        self.assertEqual(options.fail_over_strategy, FailOverStrategy.FAIL_CLOSED.value)
        self.assertEqual(options.interval, "1000")
        self.assertEqual(options.log_level, "Critical")
        self.assertEqual(options.max_events, "100")
        self.assertEqual(options.timeout, "1500")
