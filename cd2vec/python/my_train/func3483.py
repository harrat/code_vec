def test_register_driver():
    """ We are testing the registerDriver hook"""
    import pytest
    from zope.interface import implementer
    from zope.interface import Interface
    from pypom.driver import registerDriver
    from pypom.interfaces import IDriver

    class IFakeDriver(Interface):
        """ A fake marker interface"""

    @implementer(IFakeDriver)
    class FakeDriver:
        """ A fake driver """

        def __init__(self, driver):
            self.driver = driver

    fake_driver = FakeDriver(None)

    # no register driver, look up error
    with pytest.raises(TypeError):
        IDriver(fake_driver)

    registerDriver(IFakeDriver, FakeDriver)

    # driver implementation available after registerDriver
    adapted_driver = IDriver(fake_driver)

    # same instance of adapted_driver
    assert isinstance(adapted_driver, FakeDriver)

