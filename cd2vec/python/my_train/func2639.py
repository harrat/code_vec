@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_search_reference_004(snippy, capsys):
        """Search references with ``uuid`` option.

        Search reference by explicitly defining full length content uuid.
        """

        output = (
            '1. How to write commit messages @git [5c2071094dbfaa33]',
            '',
            '   > https://chris.beams.io/posts/git-commit/',
            '   # commit,git,howto',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--scat', 'reference', '--uuid', '31cd5827-b6ef-4067-b5ac-3ceac07dde9f', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
