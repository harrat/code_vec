def test_append_last_line(tmpdir):
    p = tmpdir.mkdir("sub").join("hello.txt")
    p.write("content")
    file_utils._new_line(str(p))
    assert p.read() == "content\n"

