@freeze_time("2018-01-01")
def test_people_append() -> None:
    """Test the people_append method."""
    m = MixpanelTrack(settings={}, distinct_id="foo")

    m.people_append({ProfileProperties.dollar_name: "FooBar"})
    assert len(m.api._consumer.mocked_messages) == 1
    assert m.api._consumer.mocked_messages[0].endpoint == "people"
    assert m.api._consumer.mocked_messages[0].msg == {
        "$token": "testing",
        "$time": 1514764800000,
        "$distinct_id": "foo",
        "$append": {"$name": "FooBar"},
    }

    # with meta properties
    m.people_append(
        {ProfileProperties.dollar_name: "FooBar2"},
        meta={ProfileMetaProperties.dollar_ip: "1.1.1.1"},
    )
    assert len(m.api._consumer.mocked_messages) == 2
    assert m.api._consumer.mocked_messages[1].endpoint == "people"
    assert m.api._consumer.mocked_messages[1].msg == {
        "$token": "testing",
        "$time": 1514764800000,
        "$distinct_id": "foo",
        "$append": {"$name": "FooBar2"},
        "$ip": "1.1.1.1",
    }
