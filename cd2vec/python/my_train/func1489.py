def test_increment_major(self):
        os.environ["RELEASE_TYPE"] = "major"
        v1 = VersionUtils.increment(self.v1)
        v2 = VersionUtils.increment(self.v2)
        v3 = VersionUtils.increment(self.v3)
        v4 = VersionUtils.increment(self.v4)
        v5 = VersionUtils.increment(self.v5)
        v6 = VersionUtils.increment(self.v6)
        self.assertEqual(v1, "1!2.0.0")
        self.assertEqual(v2, "2.0.0")
        self.assertEqual(v3, "2.0.0")
        self.assertEqual(v4, "2.0")
        self.assertEqual(v5, "2015")
        self.assertEqual(v6, "3.0.0")
