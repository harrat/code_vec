def test_away_saves_multiple_goalies_empty_field(self):
        saves = ['29', '3', '30']

        fake_saves = PropertyMock(return_value=saves)
        fake_num_goalies = PropertyMock(return_value=2)
        type(self.boxscore)._away_saves = fake_saves
        type(self.boxscore)._away_goalies = fake_num_goalies

        assert self.boxscore.away_saves == 32
def test_away_saves_multiple_goalies_empty_field(self):
        saves = ['29', '', '30']

        fake_saves = PropertyMock(return_value=saves)
        fake_num_goalies = PropertyMock(return_value=2)
        type(self.boxscore)._away_saves = fake_saves
        type(self.boxscore)._away_goalies = fake_num_goalies

        assert self.boxscore.away_saves == 29
