def test_MVN_MLE_small_alpha():
    """
    Assigning truncation or detection limits that correspond to very small
    probability densities shall raise warning messages. Test if the messages
    are raised.

    """

    # use a univariate case
    ref_mean = 0.5
    ref_std = 0.25
    ref_var = ref_std ** 2.
    tr_lower = -1.0
    tr_upper = -0.2
    det_upper = -0.4
    det_lower = tr_lower

    # generate samples of a TMVN distribution
    # (assume the tmvn_rvs function works properly)
    samples = tmvn_rvs(ref_mean, ref_var, lower=tr_lower, upper=tr_upper,
                       size=1000)

    # censor the samples
    good_ones = np.all([samples > det_lower, samples < det_upper], axis=0)
    c_samples = samples[good_ones]
    c_count = 1000 - sum(good_ones)
    print(c_count)

    # warning about truncation limits
    with pytest.warns(UserWarning) as e_info:
        tmvn_MLE(c_samples, tr_lower=tr_lower, tr_upper=tr_upper - 0.6,
                 censored_count=c_count, det_lower=det_lower,
                 det_upper=det_upper)

    # warning about detection limits
    with pytest.warns(UserWarning) as e_info:
        tmvn_MLE(c_samples, tr_lower=tr_lower, tr_upper=tr_upper,
                 censored_count=c_count, det_lower=det_lower,
                 det_upper=det_upper - 0.6)
    print('----------------------------')
    # warning about alpha being smaller than the specified limit
    with pytest.warns(UserWarning) as e_info:
        tmvn_MLE(c_samples, tr_lower=tr_lower, tr_upper=tr_upper,
                 censored_count=c_count, det_lower=det_lower,
                 det_upper=det_upper, alpha_lim=0.2)
