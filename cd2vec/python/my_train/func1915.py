@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_api_search_snippet_012(server):
        """Search snippet with digets.

        Send GET /snippets/{id} to read a snippet based on digest. In this
        case the snippet is found. In this case the URI path contains 15 digit
        digest. The returned self link must be the 16 digit link.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '871'
        }
        expect_body = {
            'meta': {
                'count': 1,
                'limit': 20,
                'offset': 0,
                'total': 1
            },
            'data': {
                'type': 'snippet',
                'id': Snippet.REMOVE_UUID,
                'attributes': Storage.remove
            },
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/snippets/' + Snippet.REMOVE_UUID
            }
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/snippets/54e41e9b52a02b6',
            headers={'accept': 'application/json'})
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
