def test_workflow(tmp_dtool_server):  # NOQA
    example_identifier = "1c10766c4a29536bc648260f456202091e2f57b4"

    with tmp_directory() as cache_dir:
        with tmp_env_var("DTOOL_CACHE_DIRECTORY", cache_dir):
            dataset = DataSet.from_uri(tmp_dtool_server)
