def test_rvalue_nomass_construction(self):
        self.idf.initreadtxt(no_mass)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        n = self.idf.getobject("MATERIAL:NOMASS", "NoMass")
        expected = (
            INSIDE_FILM_R
            + m.Thickness / m.Conductivity
            + n.Thermal_Resistance
            + m.Thickness / m.Conductivity
            + OUTSIDE_FILM_R
        )
        assert almostequal(c.rvalue, expected, places=2)
        assert almostequal(c.rvalue, 0.65, places=2)
