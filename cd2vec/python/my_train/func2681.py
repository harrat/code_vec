def test_load(self):
        gp = parser.VFP600()
        self.assertFalse(gp.loaded)

        gp.load('tests/vfp600_data.dta')
        self.assertEqual(gp.fname, 'tests/vfp600_data.dta')
        self.assertTrue('VFP600' in gp.get_header()['TAG'])
        # data file acq frequency = 15hz
        self.assertEqual(gp.get_sample_time(), 1 / 15)
        self.assertEqual(gp.get_curve_count(), 1)
        self.assertEqual(gp.get_sample_count(), 20)
        self.assertTrue(gp.loaded)
