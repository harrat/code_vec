def test_trace_error(trace_error):
    tracer = ThundraTracer.get_instance()
    nodes = tracer.get_spans()
    count = 0
    for key in nodes:
        if key.operation_name == 'func_with_error':
            count += 1

    assert count == 0

    traceable, func_with_error = trace_error
    try:
        func_with_error()
    except:
        active_span = None
        nodes = tracer.get_spans()
        for key in nodes:
            if key.operation_name == 'func_with_error':
                count += 1
                active_span = key
