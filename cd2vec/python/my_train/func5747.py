def test_get(self):
        """
        This method will test domain2idna.get
        """

        expected = self.converted
        actual = domain2idna(self.domains_to_test)
        self.assertEqual(expected, actual)

        expected = self.empty_inputs
        actual = domain2idna(self.empty_inputs)
        self.assertEqual(expected, actual)

        expected = None
        actual = domain2idna(None)
        self.assertEqual(expected, actual)
