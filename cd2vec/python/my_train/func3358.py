def test_demonstrations():
    nb_demonstrations = 3
    nb_points = 24
    average_cost = False
    converter = CostmapToSparseGraph(
        np.ones((nb_points, nb_points)), average_cost)
    converter.convert()
    trajectories = [None] * nb_demonstrations
    t_start = time.time()
    demos.MAX_ITERATIONS = 2
    for k in range(nb_demonstrations):
        trajectories[k] = demos.compute_demonstration(
            sample_circle_workspaces(nb_circles=3),
            converter, nb_points=nb_points,
            show_result=False, average_cost=average_cost, verbose=True,
            no_linear_interpolation=True)
        # TODO fix this test, it takes too long for now.
        if trajectories[k] is not None:
            assert trajectories[k].n() == 2
            assert trajectories[k].T() == demos.TRAJ_LENGTH - 1
    print("time : {} sec.".format(time.time() - t_start))

