def test_getitem(test_setting, test_key, actual_value):
    """ Ensure Sensitive objects return their underlying value """
    assert test_setting[test_key] == actual_value

