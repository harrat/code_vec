def test_compute_path_synt_mfcc(self):
        af = AudioFileMFCC(self.AUDIO_FILE)
        aligner = DTWAligner(synt_wave_mfcc=af)
        with self.assertRaises(DTWAlignerNotInitialized):
            aligner.compute_path()
