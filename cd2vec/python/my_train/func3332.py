def test_spectral_solver_known_solution_spg_problem_2(self):
        problem = skp.ProcrustesProblem((4, 4, 2, 2), problemnumber=2)
        mysolver = skp.SPGSolver(verbose=0)
        result = mysolver.solve(problem)
        assert_allclose(result.solution, problem.Xsol, atol=1e-3)
