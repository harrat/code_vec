def test_custom_default_ssl(app_custom_default_ssl):
    """Test that we can pass a custom default ssl configuration."""

    redis = FlaskMultiRedis(app_custom_default_ssl)
    kwargs = redis.connection_pool.connection_kwargs
    assert kwargs['ssl_keyfile'] == 'ssl/rediskey.pem'
    assert kwargs['ssl_certfile'] == 'ssl/rediscert.pem'
    assert kwargs['ssl_ca_certs'] == 'ssl/rediscert.pem'
    assert kwargs['ssl_cert_reqs'] == 'required'

