def test_get_html_collection(self):
        """Test getting HTML version of a collection rather than JSON."""
        response = self.get_response('/artists',
                200,
                headers={'Accept': 'text/html'})
        assert self.is_html_response(response)
        assert 'Aerosmith' in response.get_data(as_text=True)
