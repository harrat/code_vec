def test_write(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE, rs=True)
        data = audiofile.audio_samples
        handler, output_file_path = gf.tmp_file(suffix=".wav")
        audiofile.write(output_file_path)
        audiocopy = self.load(output_file_path)
        datacopy = audiocopy.audio_samples
        self.assertTrue((datacopy == data).all())
        gf.delete_file(handler, output_file_path)
