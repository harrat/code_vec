def test_pheno_objects_shh_2():
    """
    Equivalent to above, using convenience method
    """
    objs = get_objects_for_subject(subject=HUMAN_SHH,
                                   object_category='phenotype')
    print(objs)
    assert HOLOPROSENCEPHALY in objs
    assert len(objs) > 50

def test_pheno2gene():
    """
    given a phenotype term, find genes
    """
    subjs = get_subjects_for_object(object=HOLOPROSENCEPHALY,
                                    subject_category='gene',
                                    subject_taxon='NCBITaxon:9606')
    print(subjs)
    print(len(subjs))
    assert HUMAN_SHH in subjs
    assert len(subjs) > 50
