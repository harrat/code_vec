@patch('sys.stderr', new_callable=StringIO)
@patch.object(safe_print, 'open', create=True)
def test_safe_print_end(open: MagicMock, stderr: MagicMock):
    safe_print('Text', end='')
    assert '\n' not in stderr.getvalue()

