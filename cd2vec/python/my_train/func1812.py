def test_dry_run(self):
        cmd_line = ["python", "--dry-run", "-D", self.__db_url, "-w", self.__example_def_file1, "-v", "-d",
                    self.test_path]
        with self.assertRaises(SystemExit) as se:
            WopMars().run(cmd_line)
        # The tests is that these files do not exist
        self.assertFalse(os.path.exists(os.path.join(self.test_path, 'outdir/output_file1.txt')))
        self.assertFalse(os.path.exists(os.path.join(self.test_path, 'outdir/output_file2.txt')))
        self.assertFalse(os.path.exists(os.path.join(self.test_path, 'outdir/output_file7.txt')))
        self.assertEqual(se.exception.code, 0)
