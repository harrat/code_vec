def test_layers(self):
        pcb = Pcb()
        pcb.layers.append(Layer('B.Cu'))
        assert Pcb.parse(pcb.to_string()) == pcb
        pcb.layers.append(Layer('F.Cu'))
        assert Pcb.parse(pcb.to_string()) == pcb
