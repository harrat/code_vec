def test_framework_dir():
    os.chdir(TESTSDIR)
    tests_curdir = os.getcwd()
    fw_dir = framework_dir()
    os.chdir(fw_dir)
    prod_curdir = os.getcwd()
    assert tests_curdir == prod_curdir

