def test_is_allowed_allow_owner(self):
        """
        Test that an access check resulting in an "Allow Owner" policy is
        processed correctly.
        """
        e = engine.KmipEngine()
        e.get_relevant_policy_section = mock.Mock(
            return_value={
                enums.ObjectType.SYMMETRIC_KEY: {
                    enums.Operation.GET: enums.Policy.ALLOW_OWNER
                }
            }
        )

        result = e.is_allowed(
            'test_policy',
            'test_user',
            'test_group',
            'test_user',
            enums.ObjectType.SYMMETRIC_KEY,
            enums.Operation.GET
        )
        self.assertTrue(result)
