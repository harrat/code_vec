def test_xiami_music_id():

    result = XiaMi(music_id="1459299", use_id=True).__repr__()

    assert "Jo Dee Messina" in result

    assert "I Know a Heartache When I See One" in result

    assert 'http://m128.xiami.net' in result  # domain

