def test_execute_arg_int():
    global cu
    cu.execute("insert into tests(id, name, integer_field) values (?, ?, ?)",
               (2, 'test_execute_arg_int', 42))
    cu.execute("select integer_field from tests where id = ?", (2,))
    row = cu.fetchone()
    assert row[0] == 42

