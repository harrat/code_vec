@mark.django_db
def test_show_detail_from_previous_version(django_user_model):
    alpha = Dragonfly.objects.create(name="alpha", age=47)
    sighting = Sighting.objects.create(name="Berlin", dragonfly=alpha)

    request = RequestFactory().get("/", {})
    request.user = user_with_perms(django_user_model, ["testapp.view_dragonfly"])

    with VersionedDragonflyViewSet().create_revision(request):
        alpha.save()

    version = Version.objects.get_for_object_reference(Dragonfly, alpha.pk).latest(
        "revision__created_date"
    )

    alpha.name = "beta"
    alpha.save()

    sighting.name = "Tokyo"
    sighting.save()

    detail_view = VersionedDragonflyViewSet()._get_view(
        VersionedDragonflyViewSet().components["detail"]
    )
    detail_content = detail_view(request, pk=alpha.pk).rendered_content

    assert "beta" in detail_content
    assert "Tokyo" in detail_content

    version_view = VersionedDragonflyViewSet()._get_view(
        VersionedDragonflyViewSet().components["version_detail"]
    )

    version_content = version_view(
        request, pk=alpha.pk, version_id=version.pk
    ).content.decode("utf-8")

    assert "alpha" in version_content
    assert "Berlin" in version_content

    assert "beta" not in version_content
    assert "Tokyo" not in version_content

