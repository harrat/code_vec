def test_invalid_cluster_bar_shrink(self):
        ''' Invalid cluster_bar_shrink. '''
        with self.assertRaisesRegexp(ValueError,
                                     r'\[barchart\] .*cluster_bar_shrink.*'):
            barchart.draw(self.axes, _data(), breakdown=False,
                          cluster_bar_shrink=1.2)
