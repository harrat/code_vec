def test_h5images_wra(self):
        h5create_file(main_path, 'test_deleting4')
        file = '{}/test_deleting4.h5'.format(test_path)
        images_path = '{}/test_images'.format(test_path)
        scan = '65'

        import_images(file, images_path, import_type='uint64')

        output1 = h5grab_data(file, 'images/0065/041930')

        self.assertEqual(np.shape(output1), (516, 516))

        import_images(file, images_path, force_reimport=True, import_type='float32')
        output2 = h5grab_data(file, 'images/0065/041930')

        t_or_f = np.array_equal(output1, output2)

        self.assertTrue(output1.dtype == 'uint64')

        h5delete_file(file)
