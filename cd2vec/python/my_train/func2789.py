@patch(SPLUNK_CLI_PATCH)
def test_splunk_connect_no_params(splunk_client):
    """Check failure with no args."""
    splunk_client.connect = cli_connect

    sp_driver = SplunkDriver()
    check.is_true(sp_driver.loaded)

    with pytest.raises(MsticpyUserConfigError) as mp_ex:
        sp_driver.connect()
        check.is_false(sp_driver.connected)
    check.is_in("no Splunk connection parameters", mp_ex.value.args)

