def test_deletion_after_multiple_insertion(self):
        ''' verify (insert a, insert a, delete a) vs. (insert_a) '''

        kvs_empty = dict(self.kvs)
        ref_kvs_empty = dict(self.ref_kvs)

        ''' insert some content and record data structure state '''
        content_a = bytes([self.random.randint(0, 255)
                           for _ in range(2 * 1024 * 1024)])
        k_a = self.seccs.put_content(content_a)

        kvs_with_a = dict(self.kvs)
        ref_kvs_with_a = dict(self.ref_kvs)

        ''' delete other content and ensure that data structure is empty again '''
        self.seccs.delete_content(k_a)
        self.assertEqual(self.kvs, kvs_empty)
        self.assertEqual(self.ref_kvs, ref_kvs_empty)

        ''' insert some content a, then a again, then delete a '''
        k_a = self.seccs.put_content(content_a)

        k_a2 = self.seccs.put_content(content_a)
        self.seccs.delete_content(k_a2)

        ''' data structure must be in the same state as it would have been if only a was inserted '''
        self.assertEqual(self.kvs, kvs_with_a)
        self.assertEqual(self.ref_kvs, ref_kvs_with_a)
