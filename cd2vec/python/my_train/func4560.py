def test_get_word_vectors(self):
        words = ['good', 'test']
        vectors = self.embedding.get_word_vectors(words)
        self.assertTupleEqual(vectors.shape, (len(words), 300))
        # self.embedding.get_word_vectors.assert_called_once_with(words)
