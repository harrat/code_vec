def test_pheno2gene():
    """
    given a phenotype term, find genes
    """
    subjs = get_subjects_for_object(object=HOLOPROSENCEPHALY,
                                    subject_category='gene',
                                    subject_taxon='NCBITaxon:9606')
    print(subjs)
    print(len(subjs))
    assert HUMAN_SHH in subjs
    assert len(subjs) > 50
    
def test_disease_assocs():
    payload = search_associations(subject=HUMAN_SHH,
                                  object_category='disease'
    )
    print(str(payload))
    assocs = payload['associations']
    assert len(assocs) > 0
