def test_prem_file_name_only():
    "Only fetch the file name."
    fname = fetch_prem(load=False)
    assert fname.endswith("PREM_1s.csv")

