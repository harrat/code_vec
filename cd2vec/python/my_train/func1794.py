def test_parser_read_all_configuration_variables_raise_allowed_types_error(self):
        parser = ParseIt(config_location=test_files_location)
        with self.assertRaises(TypeError):
            parser.read_all_configuration_variables(allowed_types={"file_type": [bool, dict]})
