def it_skips_installation_when_no_git_dir(
    tmp_path: Path, capsys: CaptureFixture, monkeypatch: MonkeyPatch
) -> None:
    monkeypatch.setattr(Path, "cwd", lambda: tmp_path)

    console.run(["install"])
    captured = capsys.readouterr()

    assert "skipping hooks installation" in captured.out

