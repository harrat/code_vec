def test_no_network(self, model):
        from yadll.exceptions import NoNetworkFoundException
        with pytest.raises(NoNetworkFoundException):
            model.compile(compile_arg='train')
        with pytest.raises(NoNetworkFoundException):
            model.pretrain()
        with pytest.raises(NoNetworkFoundException):
            model.train(unsupervised_training=False)
