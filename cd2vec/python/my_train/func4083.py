def test_pycdlib_genisoimage_bootfile_bad(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('onedir')
    outfile = str(indir) + '.iso'

    def _do_test(binary):
        try:
            run_process([binary, '-v', '-iso-level', '1', '-no-pad', '-b', 'boot',
                         '-c', 'boot.cat', '-no-emul-boot', '-o', str(outfile),
                         str(indir)])
        except ProcessException as e:
            err1 = "Uh oh, I cant find the boot image 'boot'" in str(e)
            assert(err1)

    _do_test('genisoimage')
    _do_test(pycdlib_exe)

