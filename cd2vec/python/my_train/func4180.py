@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'export-time')
    def test_cli_export_snippet_010(snippy):
        """Export defined snippets.

        Try to export defined snippet based on message digest that cannot be
        found.
        """

        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-d', '123456789abcdef0', '-f', 'defined-snippet.txt'])
            assert cause == 'NOK: cannot find content with message digest: 123456789abcdef0'
            Content.assert_text(mock_file, None, None)
