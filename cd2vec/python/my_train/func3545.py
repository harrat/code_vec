def test_robot_attack_good(self):
        # Validate the bug fix for https://github.com/nabla-c0d3/sslyze/issues/282
        # Given a server to scan that is not vulnerable to ROBOT
        server_location = ServerNetworkLocationViaDirectConnection.with_ip_address_lookup("guide.duo.com", 443)
        server_info = ServerConnectivityTester().perform(server_location)

        result: RobotScanResult = RobotImplementation.scan_server(server_info)
        assert result.robot_result == RobotScanResultEnum.NOT_VULNERABLE_NO_ORACLE

        # And a CLI output can be generated
        assert RobotImplementation.cli_connector_cls.result_to_console_output(result)
