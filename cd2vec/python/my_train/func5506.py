def test_esm_group_repr(group_args, capsys):
    source = ESMGroupDataSource(**group_args)
    print(repr(source))
    captured = capsys.readouterr()
    assert 'assets:' in captured.out

