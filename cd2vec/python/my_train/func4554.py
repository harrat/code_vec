def test_happy_path(valid_template, namespace):
    template_path, obj, metadata = handler.load(valid_template, namespace)
    assert obj
    assert metadata['Version'] == '1.0'
    assert metadata['DeployBucket'] == 'sample-deploy-bucket'
    assert metadata['StackName'] == 'MyStackName'
    assert metadata['Variables'] == [
        OrderedDict({'PreparedVar': 'PreparedValue'}),
        {'SAMWise::AccountId': "{SAMWise::AccountId}"},
        {'SAMWise::Namespace': namespace},
        {'SAMWise::StackName': 'MyStackName'}
    ]
    assert obj['Parameters']['Namespace']['Default'] == namespace

