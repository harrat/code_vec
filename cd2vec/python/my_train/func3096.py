def test_which():
  assert "/bin/bash" == emu.which("bash")
  assert "/bin/bash" == emu.which("/bin/bash")
  assert None == emu.which("not-a-real-command")

