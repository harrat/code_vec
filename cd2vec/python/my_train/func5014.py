def test_stats(capsys, interp):
    interp.do_cd("Group1")
    interp.do_cd("Subgroup1")
    interp.do_stats("field1")
    out, err = capsys.readouterr()
    assert out == """\
Type           mean +/- std*2       [        min, max        ] (Shape)
--------------------------------------------------------------------------
int64  4.9500e+01 +/-  5.7732e+01 [ 0.0000e+00,  9.9000e+01] (100,)
"""
