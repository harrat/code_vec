def test_query_parsing(self):
        """Test query parsing."""

        from cylleneus.corpus import Corpus
        from cylleneus.engine.qparser.default import CylleneusQueryParser

        queries = [
            ("perseus", "{=Anatomy}"),
            ("dcs", "[=the Sustainer]"),
            ("perseus", "(<gelidus> OR <gelida>) AND <pruina>"),
            ("latin_library", "'sed'"),
            ("proiel", ":ACC.PL."),
            ("proiel", "<habeo>"),
            ("agldt", "<animus>|ABL.SG."),
            ("camena", "[en?war]"),
            ("camena", "[it?guerra]"),
            ("digiliblt", "{611}"),
            ("perseus", '"cum clamore"'),
            ("perseus", '"cum <clamor>"'),
            ("perseus", '"cum <clamor>|ABL."'),
            ("perseus", '"cum magno" <calor>'),
            ("lasla", '":VB. milites"'),
            ("lasla", '":VB. <miles>"'),
            ("proiel", "</::bellum>"),
            ("latin_library", "[!::en?cowardice]"),
            ("perseus_xml", "[en?courage]|ABL.PL."),
            ("perseus", "[@::n#04478900]"),
            ("latin_library", "opt*"),
            ("atlas", "<?????>"),
            ("diorisis", "<????????>"),
            ("lasla", "/ablative absolute/"),
            ("lasla", "/interrogative/"),
            ("lasla", "/QVOMINVS/"),
            ("proiel", "/predicate/"),
            ("lasla", "/subordinating conjunction/"),
            ("proiel", "/adverbial/"),
            ("proiel", "/adnominal argument/"),
        ]

        for corpus, query in queries:
            c = Corpus(corpus)
            p = CylleneusQueryParser("form", c.schema)
            q = p.parse(query)
            assert q
