def test_specnorm(self):
        layer_test(
            DynastesConv1D, kwargs={'filters': 3, 'kernel_size': 3,
                                    'kernel_normalizer': 'spectral',
                                    'use_wscale': True,
                                    'kernel_regularizer': 'orthogonal'}, input_shape=(5, 32, 3))
