def test_flag(self):
        self.assertIs(self.get_opts('test').flag, False)
        self.assertIs(self.get_opts('test', '--flag').flag, True)
