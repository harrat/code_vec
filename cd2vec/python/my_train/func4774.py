def test_length(api, api_assert):
    r = api.default_normal()

    data = r['samples']

    assert len(data) == 10

