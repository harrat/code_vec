@pytest.mark.vcr()
def test_scan_export_was_typeerror(api):
    with pytest.raises(UnexpectedValueError):
      api.scans.export(SCAN_ID_WITH_RESULTS, scan_type='bad-value')
