@pytest.mark.parametrize('table, out', [
		(Table('s.t'), '"s"."t"'),
		(Table('t'), '"t"'),
		(Table('t', schema = 's'), '"s"."t"'),
	])
	def testStr(self, table, out):
		assert str(table) == out
