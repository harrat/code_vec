def test_middle_begin_end(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        audiofile.middle_begin = 100
        audiofile.middle_end = 400
        self.assertEqual(audiofile.all_length, 1331)
        self.assertEqual(audiofile.head_length, 100)
        self.assertEqual(audiofile.middle_length, 300)
        self.assertEqual(audiofile.tail_length, 931)
