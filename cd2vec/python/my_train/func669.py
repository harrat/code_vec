def test_delete_resource_violating_constraint(self):
        """Test DELETEing a resource which violates a foreign key
        constraint (i.e. the record is still referred to in another table)."""
        response = self.app.delete('/artists/275')
        assert response.status_code == 422
