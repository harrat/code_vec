def test_raises_exception_no_channel_subscribed(self):
        with pytest.raises(SubscribeToAtLeastOneChannelException):
            Instrument()
