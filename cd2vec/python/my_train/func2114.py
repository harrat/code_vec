def test_on_subscribe_called_on_sub_error_message(self, mocker):
        """
        on_message should call on_subscribe when subscription error is received
        """
        error = mocker.patch(
            'bitmex_websocket.BitMEXWebsocket.on_error')
        socket = BitMEXWebsocket()
        message = {
            "status": 400,
            "error": "Unknown table: instrument_",
            "meta": {},
            "request": {
                "op": "subscribe",
                "args": ["instrument_:XBTH17"]
            }
        }

        socket.on_message(json.dumps(message))
        error.assert_called_with("Unknown table: instrument_")
