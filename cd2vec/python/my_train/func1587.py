def test_unsupported_version_raises_exception(self):
        with self.assertRaises(IdentixOneException):
            Client(token=self.token, version=None)
