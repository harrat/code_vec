def test_run_continuous_hang(
    selector: Type[MockSelector], proc: ContinuousSSH, caplog: Any, popen: Type[MockPopen]
) -> None:
    popen._success = True
    selector.events = [selectors.EVENT_READ] * 3
    popen._stdout = b"Entering interactive session\ndebug1: some other message\ndone"
    proc._run_once()

    assert list(get_transitions(proc)) == ["connecting", "connected", "disconnected"]

