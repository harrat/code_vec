@unittest.skipIf(platform.system() == "Windows" or platform.system() == "windows", "test not supported on windows")
    def test_handle_invalid_config_file(self):
        try:
            os.remove(self.config_file_path)
            del os.environ["SECURENATIVE_API_KEY"]
            del os.environ["SECURENATIVE_API_URL"]
            del os.environ["SECURENATIVE_INTERVAL"]
            del os.environ["SECURENATIVE_MAX_EVENTS"]
            del os.environ["SECURENATIVE_TIMEOUT"]
            del os.environ["SECURENATIVE_AUTO_SEND"]
            del os.environ["SECURENATIVE_DISABLE"]
            del os.environ["SECURENATIVE_LOG_LEVEL"]
            del os.environ["SECURENATIVE_FAILOVER_STRATEGY"]
        except FileNotFoundError:
            pass
        except KeyError:
            pass

        config = {"bla": "bla"}

        self.create_ini_file(config)
        options = ConfigurationManager.load_config(None)

        self.assertIsNotNone(options)
