def test_n_calls_lower_n_random_starts():
    """Assert than an error is raised when n_calls<n_random_starts."""
    atom = ATOMClassifier(X_bin, y_bin, random_state=1)
    atom.run(['LR', 'LDA'], n_calls=(5, 2), n_random_starts=(2, 3))
    assert atom.errors.get('LDA')

