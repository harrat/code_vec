def test_read_samples_from_empty(self):
        with self.assertRaises(AudioFileUnsupportedFormatError):
            audiofile = self.load(self.AUDIO_FILE_EMPTY, rs=True)
