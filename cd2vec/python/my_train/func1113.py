def test_importanize_no_changes(self) -> None:
        result = next(
            run_importanize_on_source(
                self.output_grouped,
                RuntimeConfig(formatter_name="grouped", _config=self.config),
            )
        )

        assert result.organized == self.output_grouped.read_text()
        assert not result.has_changes
        assert result.is_success
