def test_datasets_locate():
    "Make sure the data cache location has the right package name"
    path = locate()
    assert os.path.exists(path)
    # This is the most we can check in a platform independent way without
    # testing appdirs itself.
    assert "harmonica" in path

