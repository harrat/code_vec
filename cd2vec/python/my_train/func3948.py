def test_search_attribute_get(self, mocker, atlas_client, search_attribute_response):
        mocker.patch.object(atlas_client.search_attribute.client, 'get')
        atlas_client.search_attribute.client.get.return_value =  search_attribute_response 
        params = {'attrName': 'attrName', 'attrValue': 'attrVal', 'offset': '1'}
        search_results = atlas_client.search_attribute(**params) 
        for s in search_results:
            assert s.queryType == 'GREMLIN'
            atlas_client.search_attribute.client.get.assert_called_with(s.url, params=params)
        for s in search_results:
            for e in s.entities:
                assert e.attributes['property1'] == {}
