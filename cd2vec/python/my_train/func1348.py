def test_exist_nocompile(fcentral):
    mfile = fcentral[MAIN_D]
    ofile = fcentral[MAIN_O]
    hfile = fcentral[HELPER1_H]
    assert hfile < mfile
    assert hfile < ofile
    dfile = DFile.parse(fcentral, mfile)
    assert not dfile.should_compile()

def test_exist_srcnew(fcentral):
    mfile = fcentral[HELPER1_D]
    cfile = fcentral[HELPER1_C]
    assert mfile < cfile
    assert cfile.exists
    dfile = DFile.parse(fcentral, mfile)
    assert dfile.should_compile()
