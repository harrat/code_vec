def test_clean_duplicates(self):
        # Create Cam3

        location = [4.28, 3.58, 0]
        rotation = [-90, 180, -52.2]
        focal_length = 35
        sensor_width = 32
        sensor_height = 18
        screen_width = 1920
        screen_height = 1080

        cam3 = Camera(focal_length, screen_width, screen_height, sensor_width, sensor_height, location, rotation,
                      "EULER")

        texture3 = Texture('/fake_texture.png', cam3)
        self.mapper.textures.append(texture3)
        self.mapper.start_visibility_analysis()
        self.mapper.clean_duplicates()

        expected_tri_ids_0 = [5, 11]
        expected_tri_ids_1 = [8]
        expected_tri_ids_2 = [2]

        try:
            np.testing.assert_equal(self.mapper.textures[0].vis_triangle_indices, expected_tri_ids_0)
            np.testing.assert_equal(self.mapper.textures[1].vis_triangle_indices, expected_tri_ids_1)
            np.testing.assert_equal(self.mapper.textures[2].vis_triangle_indices, expected_tri_ids_2)
            res = True
        except AssertionError as err:
            res = False
            print(err)
        self.assertTrue(res)

        try:
            np.testing.assert_equal(self.mapper.textures[0].duplicate_triangle_indices, [])
            np.testing.assert_equal(self.mapper.textures[1].duplicate_triangle_indices, [])
            np.testing.assert_equal(self.mapper.textures[2].duplicate_triangle_indices, [])
            res = True
        except AssertionError as err:
            res = False
            print(err)
        self.assertTrue(res)
