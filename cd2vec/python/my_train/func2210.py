@pytest.mark.vcr()
def test_workbench_assets_filter_tyype_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.assets(filter_type=1)
