@pytest.mark.parametrize('params, expected',
                             [(('a', 1), True),
                              (('a', 3), False)])
    @mock.patch.object(NamedEnumMeta, 'gen',
                       return_value=(item for item in MockColor))
    def test__has_field(self, mocked_gen, params, expected):
        result = NamedEnumMeta._has_field(NamedEnumMeta, *params)
        assert result == expected
        mocked_gen.assert_called_once_with(name_value_pair=False)
