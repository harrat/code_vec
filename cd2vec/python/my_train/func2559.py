def test_trigonometric_functions():

    f = Arccos()

    print("Check Tanh (J implementation) : ")
    assert check_jacobian_against_finite_difference(f)

    print("Check Tanh (H implementation) : ")
    assert check_hessian_against_finite_difference(f)

