@pytest.mark.vcr()
def test_workbench_export_filter_type_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.export(filter_type='NOT')
