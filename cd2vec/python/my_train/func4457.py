def test_machine(tmpdir):
    tmpdir = six.text_type(tmpdir)

    m = machine.Machine.load(
        interactive=False,
        machine="orangutan",
        os="BeOS",
        arch="MIPS",
        cpu="10 MHz",
        ram="640k",
        _path=join(tmpdir, 'asv-machine.json'))

    m = machine.Machine.load(
        _path=join(tmpdir, 'asv-machine.json'), interactive=False)

    assert m.machine == 'orangutan'
    assert m.os == 'BeOS'
    assert m.arch == 'MIPS'
    assert m.cpu == '10 MHz'
    assert m.ram == '640k'

