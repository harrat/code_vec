def test_http_path_depth():
    ConfigProvider.set(config_names.THUNDRA_TRACE_INTEGRATIONS_HTTP_URL_DEPTH, '2')
    try:
        url = 'https://jsonplaceholder.typicode.com/asd/qwe/xyz'
        parsed_url = urlparse(url)
        normalized_path = "/asd/qwe"
        path = parsed_url.path
        query = parsed_url.query
        host = parsed_url.netloc
