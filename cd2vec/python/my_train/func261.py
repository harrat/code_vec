def test_invalid_extension_timestamp(self):
        observed_data = copy.deepcopy(self.valid_object)
        observed_data['extensions'] = {'windows-pebinary-ext': {
            "pe_type": "dll",
            "time_date_stamp": "2016-11-31T08:17:27Z",
        }}
        self.assertFalseWithOptions(observed_data)
