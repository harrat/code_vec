def test_process(self):
        with Process('cat', stdin=PIPE, stdout=PIPE, stderr=PIPE) as p:
            self.assertIsNotNone(p.get_writer())
            self.assertIsNotNone(p.get_reader('stdout'))
            self.assertIsNotNone(p.get_reader('stderr'))
            self.assertFalse(p.seekable())
            assert (p.stdout, p.stderr) == p.get_readers()
            p.write(b'foo\n')
            p.flush()
        assert b'foo\n' == p.stdout
        self.assertFalse(p.stderr)

        # wrap pipes
        with Process(('zcat', '-cd'), stdin=PIPE, stdout=PIPE) as p:
            self.assertTrue(p.readable())
            self.assertTrue(p.writable())
            with self.assertRaises(ValueError):
                p.is_wrapped('foo')
            with self.assertRaises(ValueError):
                p.wrap_pipes(foo=dict(mode='wt'))
            p.wrap_pipes(stdin=dict(mode='wt', compression='gzip'))
            self.assertTrue(p.is_wrapped('stdin'))
            p.write('foo')
        assert b'foo' == p.stdout
