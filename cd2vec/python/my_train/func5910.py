@pytest.mark.vcr()
def test_workbench_vuln_outputs(api):
    outputs = api.workbenches.vuln_outputs(19506)
    assert isinstance(outputs, list)
    o = outputs[0]
    check(o, 'plugin_output', str)
    check(o, 'states', list)
    for i in o['states']:
        check(i, 'name', str)
        check(i, 'results', list)
        for j in i['results']:
            check(j, 'application_protocol', str, allow_none=True)
            check(j, 'assets', list)
            for k in j['assets']:
                check(k, 'first_seen', 'datetime')
                check(k, 'fqdn', str, allow_none=True)
                check(k, 'hostname', str)
                check(k, 'id', 'uuid')
                check(k, 'ipv4', str, allow_none=True)
                check(k, 'last_seen', 'datetime')
                check(k, 'netbios_name', str, allow_none=True)
                check(k, 'uuid', 'uuid')
            check(j, 'port', int)
            check(j, 'severity', int)
            check(j, 'transport_protocol', str)

@pytest.mark.vcr()
def test_workbenches_asset_delete_asset_uuid_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.asset_delete(1)
