def test_compute_acm_synt_path(self):
        aligner = DTWAligner(synt_wave_path=self.AUDIO_FILE)
        with self.assertRaises(DTWAlignerNotInitialized):
            aligner.compute_accumulated_cost_matrix()
