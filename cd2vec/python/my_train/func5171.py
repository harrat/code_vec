def test_store_document(self, connected_adapter):
        """Test storing document on Ceph."""
        document, key = {'thoth': 'is awesome! ;-)'}, 'my-key'
        assert not connected_adapter.document_exists(key)

        connected_adapter.store_document(document, key)
        assert connected_adapter.retrieve_document(key) == document
