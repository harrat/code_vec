def test_start_with_description_as_multiple_params(cli, entries_file):
    """
    Description can either be entered as a single quoted param or as multiple params, in which case it will be joined
    with space characters.
    """
    expected = """20/01/2014

alias_1 09:00-? Play ping-pong
"""

    with freeze_time('2014-01-20 09:00:00'):
        cli('start', ['alias_1', 'Play', 'ping-pong'])
    assert entries_file.read() == expected

