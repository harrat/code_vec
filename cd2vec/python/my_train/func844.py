def test_toposort_subgraph(make_test, exec_ctx):
    #
    #       t0
    #       ^
    #       |
    #   +-->t1<--+
    #   |        |
    #   t2<------t3
    #   ^        ^
    #   |        |
    #   +---t4---+
    #
    t0 = make_test('t0')
    t1 = make_test('t1')
    t2 = make_test('t2')
    t3 = make_test('t3')
    t4 = make_test('t4')
    t1.depends_on('t0')
    t2.depends_on('t1')
    t3.depends_on('t1')
    t3.depends_on('t2')
    t4.depends_on('t2')
    t4.depends_on('t3')
    full_deps = dependency.build_deps(
        executors.generate_testcases([t0, t1, t2, t3, t4])
    )
    partial_deps = dependency.build_deps(
        executors.generate_testcases([t3, t4]), full_deps
    )
    cases = dependency.toposort(partial_deps, is_subgraph=True)
    assert_topological_order(cases, partial_deps)

