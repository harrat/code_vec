def test_retries_bad_check(make_runner, make_cases, common_exec_ctx):
    runner = make_runner(max_retries=2)
    runner.runall(make_cases([BadSetupCheck(), BadSetupCheckEarly()]))

    # Ensure that the test was retried #max_retries times and failed
    assert 2 == runner.stats.num_cases()
    assert_runall(runner)
    assert runner.max_retries == rt.runtime().current_run
    assert 2 == len(runner.stats.failures())

    # Ensure that the report does not raise any exception
    runner.stats.retry_report()

