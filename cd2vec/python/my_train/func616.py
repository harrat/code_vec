def test_support_custom_callback_name(self, app):
        serializer = JSONPSerializer(callback_name='jsonp')
        with app.test_request_context('?jsonp=console.log'):
            assert serializer('42') == 'console.log(42);'
