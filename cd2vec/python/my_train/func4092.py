def test_file_empty(self):
        with self.assertRaises(FFPROBEUnsupportedFormatError):
            self.load(self.EMPTY_FILE_PATH)
