def test_trace_args(trace_args):
    tracer = ThundraTracer.get_instance()
    nodes = tracer.get_spans()
    count=0
    for key in nodes:
        if key.operation_name == 'func_args':
            count += 1

    assert count == 0

    traceable_trace_args, func_args = trace_args
    func_args('arg1', arg2='arg2')

    active_span = None
    nodes = tracer.get_spans()
    for key in nodes:
        if key.operation_name == 'func_args':
            count += 1
            active_span = key

    args = active_span.get_tag('method.args')
    assert len(args) == 2
    assert args[0]['value'] == 'arg1'
    assert args[0]['name'] == 'arg-0'
    assert args[0]['type'] == 'str'
    assert args[1]['value'] == 'arg2'
    assert args[1]['name'] == 'arg2'
    assert args[1]['type'] == 'str'

    return_value = active_span.get_tag('method.return_value')
    assert return_value is None

    error = active_span.get_tag('error')
    assert error is None

    assert count == 1
    assert traceable_trace_args.trace_args is True
    assert traceable_trace_args.trace_return_value is False
    assert traceable_trace_args.trace_error is True

