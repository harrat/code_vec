@responses.activate
    def test_command_setparam(self):
        with open(resource_dir / 'test_commands_1.json') as data:
            cmd = Command(json.load(data))
        values = [
            cmd.name,
            'type',
            'notif',
        ]
        data = {}
        data['action'] = 'setparam'
        data['object'] = 'CMD'
        data['values'] = values

        with patch('requests.post') as patched_post:
            cmd.setparam('type', 'notif')
            patched_post.assert_called_with(self.clapi_url, headers=self.headers, data=json.dumps(data), verify=True)
