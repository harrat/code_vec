def test_color_print_nofail(capfd):
    # Try out color print

    color_print("hello", "red")
    color_print("indeed?", "blue")
    with pytest.raises(ValueError):
        color_print(b"really\xfe", "green", "not really")

    out, err = capfd.readouterr()
    assert 'hello' in out
    assert 'indeed' in out

