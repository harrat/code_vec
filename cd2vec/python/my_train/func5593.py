def test_create_document(self):
        """ Should create reST document. """
        res = self.client.get('/')
        autodoc.parse('GET /', res)
        autodoc.create_document(os.path.join(self.root_path,
                                             'var/test_autodoc.rst'))
        self.assertTrue(os.path.exists(os.path.join(self.root_path,
                                                    'var/test_autodoc.rst')))
        autodoc.clear()
