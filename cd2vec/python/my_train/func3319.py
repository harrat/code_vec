def test_make_sh_output_script(self):
        self.assertEqual(
            tb.make_sh_output_script('OP_IF'),
            helpers.OP_IF['output_script'])
        self.assertEqual(
            tb.make_sh_output_script(
                helpers.P2WSH['human']['witnesses'][0]['wit_script'],
                witness=True),
            helpers.P2WSH['ser']['ins'][0]['pk_script'])

        riemann.select_network('bitcoin_cash_main')
        with self.assertRaises(ValueError) as context:
            tb.make_sh_output_script(
                helpers.P2WSH['human']['witnesses'][0]['wit_script'],
                witness=True)

        self.assertIn(
            'Network bitcoin_cash_main does not support witness scripts.',
            str(context.exception))
