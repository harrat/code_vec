def test_default_settings():
	assert gpc.yes == False
	assert gpc.verbose == False
	assert gpc.progress_bar == False
	assert gpc.device_codename == 'bacon'

def test_connection_credentials():
	try: # You are travis
		if os.environ['TRAVIS_PULL_REQUEST'] != "false": # If current job is a Pull Request
			print("Job is pull request. Won't check credentials")
			return
	except KeyError: # You are not travis
		pass
	gpc.token_enable = False
	gpc.gmail_address  = os.environ['GMAIL_ADDR']
	gpc.gmail_password = os.environ['GMAIL_PWD']
	success, error = gpc.connect()
	assert error is None
	assert success == True
