@freeze_time('2017-06-21')
def test_search_string_can_be_used_with_used_flag(cli, entries_file, alias_config):
    entries_file.write("""20.06.2017
    p2_active 1 Play ping-pong
    active2 1 Play ping-pong
    """)

    stdout = cli('alias', ['list', '--used', 'active2'])

    assert 'active2' in stdout
    assert 'p2_active' not in stdout

