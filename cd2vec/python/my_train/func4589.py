def test_find_term_asthma(ontclient):
    assert ontclient.find_term('asthma') == 'http://www.ebi.ac.uk/efo/EFO_0000270'

def test_is_included(ontclient):
    assert ontclient._is_included('http://www.orpha.net/ORDO/Orphanet_354')
    assert not ontclient._is_included('http://purl.obolibrary.org/obo/UBERON_0000310')
