def test_group():
    pidom = PiDom()
    device_name1 = 'foo'
    device_name2 = 'bar'
    group_name = 'foobar'
    pidom.synchronize(device_name1)
    pidom.synchronize(device_name2)
    pidom.switch_on(device_name1)
    assert pidom.state(device_name1) is True
    pidom.new_group(group_name, [device_name1, device_name2])
    assert pidom.state(device_name1) is False
    assert pidom.state(device_name2) is False
    assert device_name1 in pidom._groups[group_name]
    assert device_name2 in pidom._groups[group_name]

    pidom.switch_on(group_name)
    assert pidom.state(device_name1) is True
    assert pidom.state(device_name2) is True

    pidom.switch_off(group_name)
    assert pidom.state(device_name1) is False
    assert pidom.state(device_name2) is False

    pidom.toggle(group_name)
    assert pidom.state(device_name1) is True
    assert pidom.state(device_name2) is True

    pidom.toggle(group_name)
    assert pidom.state(device_name1) is False
    assert pidom.state(device_name2) is False

    pidom.switch_on(group_name)

    pidom.rm_group(group_name)
    assert pidom.state(device_name1) is False
    assert pidom.state(device_name2) is False
    assert group_name not in pidom._groups.keys()

