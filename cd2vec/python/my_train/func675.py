def test_flight_two_way(self):
        self.fp.add_flight(a="BRU", b="BCN", two_way=True)
        self.assertEqual(len(self.fp.steps), 3)
        self.assertAlmostEqual(self.fp.emissions, 116 * 3, delta=10)
