def test_flex_alloc_valid_constraint_opt(make_flexible_job):
    job = make_flexible_job('all')
    job.options = ['-C f1']
    prepare_job(job)
    assert job.num_tasks == 12

