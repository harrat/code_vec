def test_less(self):
        r = ruler.FileSizeRule("< 1m")
        self.assertTrue(r.match(self.f1))
        self.assertFalse(r.match(self.f2))

        r = ruler.FileSizeRule("<1024k")
        self.assertTrue(r.match(self.f1))
        self.assertFalse(r.match(self.f2))

        r = ruler.FileSizeRule("< 0.0009765625g")
        self.assertTrue(r.match(self.f1))
        self.assertFalse(r.match(self.f2))
