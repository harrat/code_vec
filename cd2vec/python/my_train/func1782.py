@pytest.mark.parametrize("feed_type", ["modified"])
def test_feed_processor_modified_feed_success(feed_type, feed_url_modified):
    feed_processor = core.FeedProcessor()
    feed_processor(feed_type=feed_type)
    assert isinstance(feed_processor.feed_json, dict)
    assert feed_processor.feed_resource_url == feed_url_modified

