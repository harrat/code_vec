def test_mlb_limited_game_info(self):
        fields = {
            'attendance': 19043,
            'date': 'Sunday, June 28, 1970',
            'time': None,
            'venue': 'Robert F. Kennedy Stadium',
            'duration': '3:43',
            'time_of_day': 'Day'
        }

        mock_field = """Sunday, June 28, 1970
Attendance: 19,043
Venue: Robert F. Kennedy Stadium
Game Duration: 3:43
Day Game, on grass
"""

        m = MockBoxscoreData(MockField(mock_field))

        self.boxscore._parse_game_date_and_location(m)
        for field, value in fields.items():
            assert getattr(self.boxscore, field) == value
