def test_notifier_loaded(self):
        data = {
            'pre_script': [],
            'post_script': [],
            'remote': 'origin',
            'branch': 'master',
            'notifier': {
                'type': 'discord',
                'receiver': 'something'
            }
        }

        with open(self.filename, 'w') as f:
            json.dump(data, f)

        chain = CommandChain.load_from_config(self.filename)
        self.assertIsNotNone(chain.notifier)
