def test_properties(tmpvenv):
    script_basedir = "Scripts" if os.name == "nt" else "bin"
    assert script_basedir == tmpvenv.script_basedir
    scripts_dir = tmpvenv.venv_dir.joinpath(script_basedir).as_posix()
    assert scripts_dir == vistir.compat.Path(tmpvenv.scripts_dir).as_posix()
    python = "{0}/python".format(tmpvenv.venv_dir.joinpath(script_basedir).as_posix())
    assert python == tmpvenv.python
    libdir = "Lib" if os.name == "nt" else "lib"
    libdir = tmpvenv.venv_dir.joinpath(libdir).as_posix().lower()
    assert any(
        vistir.compat.Path(pth).as_posix().lower().startswith(libdir)
        for pth in tmpvenv.sys_path
    ),  list(tmpvenv.sys_path)
    assert tmpvenv.sys_prefix == tmpvenv.venv_dir.as_posix()

