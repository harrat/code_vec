def test_children_are_added(self):
        """ Ensure that, on registration, children are added """
        cls = MappedClass("TestDOM", (self.DataObject,), dict())
        self.mapper.add_class(cls)
        self.assertIn(
            cls,
            self.DataObject.children,
            msg="The test class is a child")
