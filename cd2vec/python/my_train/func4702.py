def test_file_manager(self):
        paths12 = dict(
            path1=self.root.make_empty_files(1)[0],
            path2=self.root.make_empty_files(1)[0])
        with FileManager(paths12, mode='wt') as f:
            paths34 = self.root.make_empty_files(2)
            for p in paths34:
                f.add(p, mode='wt')
                self.assertTrue(p in f)
                self.assertFalse(f[p].closed)
            path5 = self.root.make_file()
            path5_fh = open(path5, 'wt')
            f.add(path5_fh)
            path6 = self.root.make_file()
            f['path6'] = path6
            assert path6 == f.get_path('path6')
            all_paths = list(paths12.values()) + list(paths34) + [path5, path6]
            self.assertListEqual(all_paths, f.paths)
            assert len(f) == 6
            for key, fh in f.iter_files():
                self.assertFalse(fh.closed)
            assert f['path2'] is not None
            assert f.get('path2') is not None
            assert f['path6'] == f.get(5)
            with self.assertRaises(KeyError):
                _ = f['foo']
            assert f.get('foo') is None
        assert len(f) == 6
        for key, fh in f.iter_files():
            self.assertTrue(fh.closed)
