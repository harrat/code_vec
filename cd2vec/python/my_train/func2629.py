def test_train(self):
        model_config = core.ModelConfig(_mountaincar_continuous_name)
        tc = core.StepsTrainContext()
        dqn_agent = tfagents.TfSacAgent(model_config=model_config)
        dqn_agent.train(train_context=tc, callbacks=[duration.Fast(), log.Iteration(), log.Agent()])
