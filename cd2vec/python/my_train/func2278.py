def test_dispatch__error_handled_by_middleware(self):
        class ErrorMiddleware(object):
            def handle_500(self, request, exception):
                assert isinstance(exception, ValueError)
                return Error.from_status(HTTPStatus.SEE_OTHER, 0,
                                         "Quick over there...")

        def callback(request):
            raise ValueError()

        target = containers.ApiInterfaceBase(middleware=[ErrorMiddleware()])
        operation = Operation(callback)

        actual = target.dispatch(operation, MockRequest())
        assert actual.status == 303
