def test_new_list(self):
        md_file = MdUtils(file_name="Test_file", title="")
        md_file.new_list(self.complex_items)
        md_file.create_md_file()

        self.assertEqual(self.expected_list, MarkDownFile.read_file('Test_file.md'))
