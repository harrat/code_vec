def test_reserved_object_type_incident(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['type'] = "incident"
        results = validate_parsed_json(indicator, self.options)
        self.assertEqual(results.is_valid, False)
