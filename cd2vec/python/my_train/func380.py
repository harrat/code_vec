def test_observer_content_aware_subjective_model_nocontent(self):
        subjective_model = MaximumLikelihoodEstimationModelContentOblivious.from_dataset_file(
            self.dataset_filepath)
        result = subjective_model.run_modeling(force_subjbias_zeromean=False)

        self.assertAlmostEqual(float(np.sum(result['observer_bias'])), -0.038360699965624648, places=4)
        self.assertAlmostEqual(float(np.var(result['observer_bias'])), 0.095605013092265753, places=4)

        self.assertAlmostEqual(float(np.sum(result['observer_inconsistency'])), 15.81030572681315, places=4)
        self.assertAlmostEqual(float(np.var(result['observer_inconsistency'])), 0.014607671806207895, places=4)

        self.assertAlmostEqual(float(np.sum(result['quality_scores'])), 177.92139983454805, places=4)
        self.assertAlmostEqual(float(np.var(result['quality_scores'])), 1.4830610442685492, places=4)
