def test_custom_singleton_key(self):
        self._do_singleton_test(NumberSum, (1, 2, 3), (1, 2, 3), (3, 3), (1, 2, 3), (3, 4, 5), (3, 4, 5))
