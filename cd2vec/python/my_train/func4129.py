def test_WarpImageMultiTransform_invaffine_1(change_dir, create_wimt):
    wimt = create_wimt
    wimt.inputs.invert_affine = [1]
    assert (
        wimt.cmdline
        == "WarpImageMultiTransform 3 diffusion_weighted.nii diffusion_weighted_wimt.nii -R functional.nii \
-i func2anat_coreg_Affine.txt func2anat_InverseWarp.nii.gz dwi2anat_Warp.nii.gz dwi2anat_coreg_Affine.txt"
    )
