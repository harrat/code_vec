@pytest.mark.vcr()
def test_workbench_vulns_invalid_filter(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.vulns(('nothing here', 'contains', 'Linux'))
