def test_response(api, api_assert):
    r = api.default_uniform()

    api_assert.assertResponse(r, 200)

    samples = r['samples']

    assert samples is not None

def test_data(api, api_assert):
    r = api.default_uniform()
