def test_mountebank_simple_impostor():
    test_port = port_for.select_random()
    test_response = 'Just some reponse body (that I used to know)'
    test_body = 'Some test message body'

    with Mountebank() as mb:
        imposter = mb.add_imposter_simple(
            port=test_port,
            method='POST',
            path='/some-path',
            status_code=201,
            response=test_response
        )
