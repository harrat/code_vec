def test_default_logs(tier1_logger, tier2_logger, tier3_logger):
    """All loggers use INFO by default."""

    task = MyTask()
    sys.argv = ["my-task"]
    task.main()

    assert tier1_logger.getEffectiveLevel() == logging.INFO
    assert tier2_logger.getEffectiveLevel() == logging.INFO
    assert tier3_logger.getEffectiveLevel() == logging.INFO

