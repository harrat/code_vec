def test_refresh():
    ConfigProvider.set(config_names.THUNDRA_TRACE_INTEGRATIONS_ELASTICSEARCH_PATH_DEPTH, '2')
    try:
        es = Elasticsearch([{'host': 'test', 'port': 3737}], max_retries=0)
        res = es.indices.refresh(index='test-index')
        print(res)
    except ElasticsearchException as e:
        pass
    finally:
        tracer = ThundraTracer.get_instance()
        span = tracer.get_spans()[0]
        tracer.clear()
