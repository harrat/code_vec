@freeze_time('2014-01-21')
def test_not_today_option(cli, entries_file):
    entries_file.write("""20/01/2014
alias_1 2 Play ping-pong

21/01/2014
alias_1     1  Repair coffee machine
alias_1     2  Repair coffee machine
""")
    stdout = cli('commit', args=['--not-today'])

    assert 'coffee' not in stdout

