def test_search():
    q = GolrSearchQuery("abnormal")
    print("Q={}".format(q))
    params = q.solr_params()
    print("PARAMS={}".format(params))
    results = q.search()
    print("RESULTS={}".format(results))
    docs = results.docs
    for r in docs:
        print(str(r))
    assert len(docs) > 0

