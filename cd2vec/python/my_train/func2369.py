def test_encrypt_inactive_encryption_key(self):
        """
        Test that the right error is thrown when an inactive encryption key
        is specified with an Encrypt request.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        encryption_key = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.TRIPLE_DES,
            128,
            (
                b'\x01\x23\x45\x67\x89\xAB\xCD\xEF'
                b'\xF0\xE1\xD2\xC3\xB4\xA5\x96\x87'
            ),
            [enums.CryptographicUsageMask.ENCRYPT]
        )

        e._data_session.add(encryption_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        unique_identifier = str(encryption_key.unique_identifier)
        cryptographic_parameters = attributes.CryptographicParameters(
            block_cipher_mode=enums.BlockCipherMode.CBC,
            padding_method=enums.PaddingMethod.PKCS5,
            cryptographic_algorithm=enums.CryptographicAlgorithm.BLOWFISH
        )
        data = (
            b'\x37\x36\x35\x34\x33\x32\x31\x20'
            b'\x4E\x6F\x77\x20\x69\x73\x20\x74'
            b'\x68\x65\x20\x74\x69\x6D\x65\x20'
            b'\x66\x6F\x72\x20\x00'
        )
        iv_counter_nonce = b'\xFE\xDC\xBA\x98\x76\x54\x32\x10'

        payload = payloads.EncryptRequestPayload(
            unique_identifier,
            cryptographic_parameters,
            data,
            iv_counter_nonce
        )

        args = (payload,)
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            "The encryption key must be in the Active state to be used "
            "for encryption.",
            e._process_encrypt,
            *args
        )
