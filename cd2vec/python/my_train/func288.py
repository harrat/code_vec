def test_version_commit(self):
        runner('git tag -a 1.0.0 -m "test message"')
        v = relic.release.get_info()
        assert v.commit
