@pytest.mark.parametrize('id', [3])
def test_delete_card(id: int) -> None:
    card = spacedr.get_card_by_id(id)
    spacedr.delete_card(card)

    with pytest.raises(spacedr.errors.CardNotFoundError):
        spacedr.get_card_by_id(id)
