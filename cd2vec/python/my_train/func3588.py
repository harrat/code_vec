def test_empty_object():
    plot = Plot()
    # instance variables
    assert plot.data_dir == ''
    assert plot.path_resolvers == []
    # traitlets
    assert plot.command_and_arguments == {}
    assert plot.plot_options == {}
    assert plot.progress == 0.0
    assert not plot.done

