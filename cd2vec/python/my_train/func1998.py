def test_get_operations_change(setup_binary_files):
    binary_file0 = parse_binary_file("id", "name", "temp/binary0")
    create_binary_file("temp/binary0", "54321")
    binary_file1 = parse_binary_file("id", "name", "temp/binary0")
    ops = binary_file0.get_operations(binary_file1)
    assert len(ops["changed"]) == 1
    assert ops["changed"][0] == [0]
