def test_fileguarded_callables_calling_fileguarded_callables(self):
        """
        Make sure that the when a decorated callable calls a decorated callable
        within a class everything works as expectected. Internally, a stack is
        used.
        """
        the_codeumentary = TestFileGuardClassDecorator.TheCodeumentary('value_1', 'value_2', self)
        the_codeumentary.nested_write_call()
        self._assert_file_content_equals(TestFileGuardClassDecorator.TEST_FILE_CONTENTS)
