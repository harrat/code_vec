def test_hybrid_modify_in_place_dirrecord_spillover2(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('modifyinplaceonefile')
    outfile = str(indir)+'.iso'
    dir1 = indir.mkdir('dir1')
    for i in range(1, 49):
        fname = os.path.join(str(dir1), 'foo%.2d' % (i))
        with open(fname, 'wb') as outfp:
            outfp.write(b'f\n')
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-o', str(outfile), str(indir)])

    # Now open up the ISO with pycdlib and check some things out.
    iso = pycdlib.PyCdlib()
    iso.open(str(outfile), 'r+b')
    foostr = b'foo\n'
    iso.modify_file_in_place(BytesIO(foostr), len(foostr), '/DIR1/FOO40.;1')
    iso.close()

    # Now open up the ISO with pycdlib and modify it.
    open_and_check(outfile, check_modify_in_place_spillover)

def test_hybrid_shuffle_deep(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('rrdeepreshuffle')
    outfile = str(indir)+'.iso'
    indir.mkdir('dir1').mkdir('dir2').mkdir('dir3').mkdir('dir4').mkdir('dir5').mkdir('dir6').mkdir('dir7').mkdir('dir8')
    with open(os.path.join(str(indir), 'dir1', 'dir2', 'dir3', 'dir4', 'dir5', 'dir6', 'dir7', 'dir8', 'foo'), 'wb') as outfp:
        outfp.write(b'foo\n')
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-rational-rock',
                     '-o', str(outfile), str(indir)])
