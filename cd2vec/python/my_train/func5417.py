def test_function_ln(self):
        """Test ln function."""
        self.assertEqual(ucal.evaluate('ln(exp(1))'), '1')
        self.assertEqual(ucal.evaluate('ln(1)'), '0')
        self.assertRaises(ucal.QuantityError, ucal.evaluate, 'ln(1m)')
