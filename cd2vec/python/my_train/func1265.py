@responses.activate
    def test_get_timeline_medias(self):
        self.bot.max_likes_to_like = TEST_PHOTO_ITEM["like_count"] + 1
        results = 8
        responses.add(
            responses.POST,
            "{api_url}feed/timeline/".format(api_url=API_URL),
            json={
                "auto_load_more_enabled": True,
                "num_results": results,
                "is_direct_v2_enabled": True,
                "status": "ok",
                "next_max_id": None,
                "more_available": False,
                "feed_items": [TEST_TIMELINE_PHOTO_ITEM for _ in range(results)],
            },
            status=200,
        )
        responses.add(
            responses.POST,
            "{api_url}feed/timeline/".format(api_url=API_URL),
            json={"status": "fail"},
            status=400,
        )

        medias = self.bot.get_timeline_medias()

        assert medias == [TEST_PHOTO_ITEM["id"] for _ in range(results)]
        assert len(medias) == results

        medias = self.bot.get_timeline_medias()

        assert medias == []
        assert len(medias) == 0
