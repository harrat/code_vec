def test_custom_property_name_invalid_character(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['my_new_property!'] = "abc123"
        results = validate_parsed_json(indicator, self.options)
        self.assertEqual(results.is_valid, False)
