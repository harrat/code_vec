def test_destination(mock_dng, tmp_path):
    tmp_dest = tmp_path / 'dest'
    tmp_dest.mkdir()
    dng = pydng.DNGConverter(tmp_path)
    assert dng.dest == ("", "")
    dng = pydng.DNGConverter(tmp_path, dest=tmp_dest)
    assert dng.dest == ("-d", str(tmp_dest.absolute()))

