def test_sigterm_handling(make_runner, make_cases, common_exec_ctx):
    runner = make_runner()
    with pytest.raises(ReframeForceExitError,
                       match='received TERM signal'):
        runner.runall(make_cases([SelfKillCheck()]))

    assert_all_dead(runner)
    assert runner.stats.num_cases() == 1
    assert len(runner.stats.failures()) == 1

