def test_set_head2(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        audiofile.set_head_middle_tail(head_length=TimeValue("1.000"))
        self.assertEqual(audiofile.all_length, 1331)
        self.assertEqual(audiofile.head_length, 25)
        self.assertEqual(audiofile.middle_length, 1306)
        self.assertEqual(audiofile.tail_length, 0)
