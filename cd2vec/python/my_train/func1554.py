def test_build_notice_from_exception_cython():
    notifier = Notifier()

    err = get_exception_in_cython()
    notice = notifier.build_notice(err)

    errors = notice["errors"]
    assert len(errors) == 1

    error = errors[0]
    assert error["type"] == "TypeError"

    message = error["message"]
    if message.startswith('unorderable'):
        # python 3.5
        assert error["message"] == 'unorderable types: str() < int()'
    else:
        # python 3.6 and above
        assert error["message"] == "'<' not supported between instances of 'str' and 'int'"
