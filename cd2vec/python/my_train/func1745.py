def test_parse_responses(self):
        """ Should stack responses. """
        res = self.client.get('/')
        autodoc.parse('GET /', res)
        autodoc.parse('GET /', res)
        var = {
            'response_content_type': 'application/json',
            'response_body': '{\n  "response": "index"\n}',
            'describe': 'GET /',
            'request': 'GET /',
            'params': '',
            'status_code': 200,
            'target_url': 'http://localhost:80',
            'describe_separators': '====='
        }
        vars = [var, var]
        self.assertEqual(autodoc.vars, vars)
        autodoc.clear()
