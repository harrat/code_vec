def test_extract_results(self, caplog, tmp_dir):
        res = wrapper.Output(target_root_name)
        res.extract_results()
        assert "Found 600 cases classified in 3 classes" in caplog.text
        assert res.stats["main-class"].nunique() == 3
        assert res.stats.shape == (600, 5)
