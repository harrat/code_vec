@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_pulling_team_directly(self, *args, **kwargs):
        hou = Team('HOU')

        for attribute, value in self.results.items():
            assert getattr(hou, attribute) == value
