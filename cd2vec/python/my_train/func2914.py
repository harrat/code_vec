def test_parse_no_file(capsys):
    context = new_context()
    context = shell.parse(context, input_file + '_noexist')
    (out, err) = capsys.readouterr()
    assert out.index('Error, unexpected problem reading file.') == 0

