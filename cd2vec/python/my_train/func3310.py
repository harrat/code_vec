@pytest.mark.vcr()
def test_workbench_vuln_outputs_age_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vuln_outputs(19506, age='none')
