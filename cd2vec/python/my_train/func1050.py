@pytest.mark.remote
def test_get_event_urls():
    urls = locate.get_event_urls("GW150914-v3", sample_rate=4096)
    assert len(urls) == 4
    for url in urls:
        assert "_4KHZ" in url

