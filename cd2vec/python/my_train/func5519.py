def test_is_alias_exist(self):
        alias = utils.generate_alias(type=Alias.DOMAIN)
        assert not self.client.is_alias_exists(alias)

        self.client.create_member(alias)
        assert self.client.is_alias_exists(alias)
