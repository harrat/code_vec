def test_app_store_center_popup_displayed(instagram):
    """ Verify the center app store popup display check works """
    instagram.driver.find_elements_by_xpath.side_effect = [[], [1, 2]]

    assert instagram.app_store_center_popup_displayed is False
    assert instagram.app_store_center_popup_displayed is True

    exp_call = call.find_elements_by_xpath(CENTER_POPUP_DISPLAYED_XPATH)
    assert exp_call in instagram.driver.method_calls

