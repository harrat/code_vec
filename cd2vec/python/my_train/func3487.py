def test_reporthook10percent(self):
        f = io.StringIO()
        start = _current_milli_time() - 1000
        with contextlib.redirect_stdout(f):
            _report_hook(1, 1024*1024, 1024*1024*10, start, start)
        assert_that(f.getvalue(), contains_string("10%, 1 MB, 1024 KB/s, 1 seconds passed"))
