def test_list_experiment(self):
        self.create_default_experiment()
        cluster = Cluster.new('tmux', server_name=_TEST_SERVER)

        experiments = cluster.list_experiments()
        self.assertListEqual(experiments, ['exp'])
