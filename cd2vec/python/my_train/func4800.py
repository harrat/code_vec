def test_base_parse_filter_accessgroup(api, fitem, fset):
    fitem = [(fitem[0][0], fitem[0][1], [fitem[0][2]])]
    assert {'rules': [{
        'type': 'distro',
        'operator': 'match',
        'terms': ['win',]
    }]} == api.agents._parse_filters(fitem, fset, rtype='accessgroup')
