def test_ds_joint_rug_smoke(ds_NDs):
    with seaborn.axes_style("ticks"):
        fig3 = _do_jointplots(ds_NDs, rug=True)
        return fig3
