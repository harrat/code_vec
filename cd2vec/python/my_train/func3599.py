def test_guess_num_tasks(minimal_job, scheduler):
    minimal_job.num_tasks = 0
    if scheduler.registered_name == 'local':
        # We want to trigger bug #1087 (Github), that's why we set allocation
        # policy to idle.
        minimal_job.num_tasks = 0
        minimal_job._sched_flex_alloc_nodes = 'idle'
        prepare_job(minimal_job)
        minimal_job.submit()
        minimal_job.wait()
        assert minimal_job.num_tasks == 1
    elif scheduler.registered_name in ('slurm', 'squeue'):
        minimal_job.num_tasks = 0
        minimal_job._sched_flex_alloc_nodes = 'all'
