@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_function_with_closure_func(fixture, request):
    result = _runcall(fixture, request, function_tests.fn_withfunc, [1, 2], [3, 4])
    assert result == [1,2,3,4]

