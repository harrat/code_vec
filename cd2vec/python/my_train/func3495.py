def test_defined_wont_fit():
    progress_bar = ProgressBarBits(2000, max_width=33)
    assert '  0% (0.00/2.00 kb) [] eta --:-- |' == str(progress_bar)

    progress_bar = ProgressBarBits(2000, max_width=30)
    assert '  0% (0.00/2.00 kb) [] eta --:-- /' == str(progress_bar)

