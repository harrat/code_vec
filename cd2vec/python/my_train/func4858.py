@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_mlb_integration_returns_correct_attributes_for_team(self,
                                                                 *args,
                                                                 **kwargs):
        teams = Teams()

        houston = teams('HOU')

        for attribute, value in self.results.items():
            assert getattr(houston, attribute) == value
