def test_pipe_scoped(self):
		x = model.Activity()
		y = model.Activity()
		x.part = y
		model.factory.pipe_scoped_contexts = True
		js = model.factory.toJSON(x)
		self.assertTrue('part|crm:P9_consists_of' in js)
		model.factory.pipe_scoped_contexts = False
		js = model.factory.toJSON(x)		
		self.assertTrue('part|crm:P9_consists_of' not in js)		
		self.assertTrue('part' in js)
