@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_api_search_snippet_field_005(server):
        """Get specific snippet field.

        Send GET /snippets/{id}/links for existing snippet.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '279'
        }
        expect_body = {
            'data': {
                'type': 'snippet',
                'id': Snippet.REMOVE_UUID,
                'attributes': {
                    'links': Storage.remove['links']
                }
            },
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/snippets/11cd5827-b6ef-4067-b5ac-3ceac07dde9f/links'
            }
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/snippets/54e41e9b52/links',
            headers={'accept': 'application/vnd.api+json'})
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
