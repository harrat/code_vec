def test_zipped_folder_archive_has_folder_files() -> None:
    archive = zip_dirs([list_dir_files(TEST_DATA_DIR)], "sample.zip")
    files = [file.name for file in list_zip_files(archive)]
    assert files == ["response.zip", "text.txt"]

