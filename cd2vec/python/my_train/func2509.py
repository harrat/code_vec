@pytest.mark.parametrize('card_id', [(2)])
def test_error_card_already_exists(card_id: int) -> None:
    card = spacedr.get_card_by_id(card_id)
    deck = spacedr.get_deck_by_id(card.deck_id)

    with pytest.raises(spacedr.errors.CardAlreadyExists):
        try:
            spacedr.create_card(deck, question=card.question,
                                answers=card.answers)
        except spacedr.errors.CardAlreadyExists:
            raise
