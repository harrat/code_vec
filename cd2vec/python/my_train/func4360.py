def test_extract_file_zip(self):
        d = SampleDataset()
        path = d.save_dataset(DATA_ROOT)
        extracteds = d.extract_file(path, ["chazutsu-master/README.md", "chazutsu-master/docs/chazutsu.png"])
        for e in extracteds:
            os.remove(e)
        self.assertEqual(len(extracteds), 2)
