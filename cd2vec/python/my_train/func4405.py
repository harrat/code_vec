@mock.patch('thundra.opentracing.recorder.ThundraRecorder')
def test_finish(mock_recorder):
    tracer = ThundraTracer.get_instance()
    with tracer.start_active_span(operation_name='operation name', finish_on_close=True) as scope:
        span = scope.span
        assert span.finish_time == 0
