def test_date_not_present_entry_not_present(cli, entries_file):
    entries = """19/01/2014
alias_1 2 foo
"""
    expected = """20/01/2014

alias_1 00:00-? ?

19/01/2014
alias_1 2 foo
"""

    entries_file.write(entries)
    with freeze_time('2014-01-20'):
        cli('start', ['alias_1'])

    assert entries_file.read() == expected

