def test_power3d():
    pb = PowerBox(50, dim=3, pk=lambda k: 1.0 * k ** -2., boxlength=1.0, b=1)
    p, k = get_power(pb.delta_x(), pb.boxlength, b=1)

    print(p / (1.0 * k ** -2.))
    assert np.allclose(p, 1.0 * k ** -2., rtol=2)

