def test_create(c):
    with pytest.raises(TypeError):
        uname = str(uuid.uuid4())
        c.create_env(uname, packages='numpy')

    env_path = os.path.join(c.conda_envs, env_name)
    assert env_path == c._create_env(env_name, packages=[
        'python=3', 'numpy', 'nomkl'], remove=True)
