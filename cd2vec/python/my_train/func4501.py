@patch_ensure_connection_contextual()
def test_reconnect_failed(unknown_host):
    """For an unknown host, connection has to be retried.

    Behavior validation is done by intercepting the logging messages
    """
    io = StringIO()
    LOGGER.addHandler(StreamHandler(io))
    with pytest.raises(OperationalError):
        backend = load_backend(unknown_host["ENGINE"])
        conn = backend.DatabaseWrapper(unknown_host, "unknown_host")
        conn.ensure_connection()
    # test that retrying has taken place (DNS errors might have been fixed)
    assert "trial 0" in io.getvalue()

