@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_roster_from_team_class(self, *args, **kwargs):
        flexmock(Team) \
            .should_receive('_parse_team_data') \
            .and_return(None)
        team = Team(None, 1, '2018')
        mock_abbreviation = mock.PropertyMock(return_value='HOU')
        type(team)._abbreviation = mock_abbreviation

        assert len(team.roster.players) == 4

        for player in team.roster.players:
            assert player.name in ['James Harden', 'Tarik Black',
                                   'Ryan Anderson', 'Trevor Ariza']

        type(team)._abbreviation = None
