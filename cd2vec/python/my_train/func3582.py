def test_c2d14(self):
        from phenum.vector_utils import cartesian2direct
        case = 14
        path = "tests/vector_utils/"
        
        sLV = self._read_array_float(path+"cartesian2direct_sLV.in."+str(case))
        aBas = self._read_array_float(path+"cartesian2direct_aBas.in."+str(case))
        eps = self._read_float(path+"cartesian2direct_eps.in."+str(case))

        out = self._read_array_float(path+"cartesian2direct_aBas.out."+str(case))

        self._compare(out,cartesian2direct(sLV,aBas,eps))
