def test_pdf(capsys, interp):
    interp.do_cd("Group1")
    interp.do_cd("Subgroup1")
    interp.do_pdf("field1")
    out, err = capsys.readouterr()
    assert out == """\
Min         Max         | Pdf (10 buckets)
----------------------------------------------
 0.0000e+00  9.9000e+01 | [10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
"""
