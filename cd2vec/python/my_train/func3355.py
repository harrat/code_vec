def test_default_plots_None_durationcallback(self):
        agent = agents.PpoAgent("CartPole-v0")
        p = plot.Loss()
        c = agent._add_plot_callbacks([duration.Fast()], None, [p])
        assert p in c
