def test_mac(self):
        """
        Test that a MAC request can be processed correctly.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        key = (b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
               b'\x00\x00\x00\x00\x00')
        data = (b'\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A'
                b'\x0B\x0C\x0D\x0E\x0F')
        algorithm_a = enums.CryptographicAlgorithm.AES
        algorithm_b = enums.CryptographicAlgorithm.HMAC_SHA512
        obj = pie_objects.SymmetricKey(
            algorithm_a, 128, key, [enums.CryptographicUsageMask.MAC_GENERATE])
        obj.state = enums.State.ACTIVE

        e._data_session.add(obj)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        uuid = str(obj.unique_identifier)

        cryptographic_parameters = attributes.CryptographicParameters(
            cryptographic_algorithm=algorithm_b
        )

        # Verify when cryptographic_parameters is specified in request
        payload = payloads.MACRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(uuid),
            cryptographic_parameters=cryptographic_parameters,
            data=objects.Data(data)
        )

        response_payload = e._process_mac(payload)

        e._logger.info.assert_any_call(
            "Processing operation: MAC"
        )
        e._cryptography_engine.logger.info.assert_any_call(
            "Generating a hash-based message authentication code using {0}".
            format(algorithm_b.name)
        )
        e._cryptography_engine.logger.reset_mock()
        self.assertEqual(str(uuid), response_payload.unique_identifier.value)
        self.assertIsInstance(response_payload.mac_data, objects.MACData)

        # Verify when cryptographic_parameters is not specified in request
        payload = payloads.MACRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(uuid),
            cryptographic_parameters=None,
            data=objects.Data(data)
        )

        response_payload = e._process_mac(payload)

        e._cryptography_engine.logger.info.assert_any_call(
            "Generating a cipher-based message authentication code using {0}".
            format(algorithm_a.name)
        )
        self.assertEqual(str(uuid), response_payload.unique_identifier.value)
        self.assertIsInstance(response_payload.mac_data, objects.MACData)
