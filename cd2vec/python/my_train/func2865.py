@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_update_snippet_007(snippy):
        """Update snippet with ``--digest`` option.

        Try to update snippet with empty message digest. Nothing should be
        updated in this case because the empty digest matches to more than
        one snippet. Only one content can be updated at the time.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        cause = snippy.run(['snippy', 'update', '-d', '', '--format', 'text'])
        assert cause == 'NOK: cannot use empty message digest for update operation'
        Content.assert_storage(content)
