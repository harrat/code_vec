def test_inf_logprob(self):
        """
        If a walker has any parameter negative, ``logprobfn`` returns
        ``-np.inf``.  Start the ensembles in the all-positive part of the
        parameter space, then run for long enough for sampler to migrate into
        negative parts.  (We can't start outside the posterior support, or the
        sampler will fail).  The sampler should be happy with this; otherwise,
        a FloatingPointError will be thrown by Numpy.  Don't bother checking
        the results because this posterior is difficult to sample.

        """
        sampler = Sampler(self.nwalkers, self.ndim,
                          LogLikeGaussian(self.icov_unit, test_inf=True),
                          LogPriorGaussian(self.icov_unit, cutoff=self.cutoff),
                          betas=make_ladder(self.ndim, self.ntemps, np.inf))

        self.check_sampler(sampler, p0=np.abs(self.p0_unit), weak=True)
