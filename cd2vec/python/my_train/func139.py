def test_get_querystring_overrides(self):
        self.client.set_querystring_overrides('foo=bar')

        self.assertEqual('foo=bar', self.client.get_querystring_overrides())
