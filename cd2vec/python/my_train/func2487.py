def test_is_initialized(self, stash_path):
        storage = ghost.TinyDBStorage(stash_path)
        stash = ghost.Stash(storage)
        assert storage.is_initialized is False
        stash.init()
        assert storage.is_initialized is True
        storage_tester.is_initialized(storage.is_initialized)
