def test_errors(self):

        # Test invalid chassis IP and port number.

        with pytest.raises(Exception) as excinfo:
            self.xm.session.add_chassis('InvalidIp')
        assert('IOError' in repr(excinfo.value) or 'OSError' in repr(excinfo.value))
        assert(len(self.xm.session.chassis_list) == 0)

        if self.api == ApiType.socket:
            with pytest.raises(Exception) as excinfo:
                self.xm.session.add_chassis(self.chassis, -17)
            assert('IOError' in repr(excinfo.value) or 'OSError' in repr(excinfo.value))
            assert(len(self.xm.session.chassis_list) == 0)

        # Reserve port to continue testing...

        self.xm.session.add_chassis(self.chassis)
        port = self.xm.session.reserve_ports([self.port1], True)[self.port1]

        # Test attributes errors.

        with pytest.raises(XenaAttributeError) as _:
            port.get_attribute('InvalidAttribute')
        with pytest.raises(XenaAttributeError) as _:
            port.set_attributes(p_reservation=17)
        with pytest.raises(XenaAttributeError) as _:
            port.set_attributes(p_reservedby=17)
