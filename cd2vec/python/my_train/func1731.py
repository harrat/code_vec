def test_experiment_attributes(fake_exp):
    attrs = ['label', 'abspath', 'metadata', 'datatype', 'filetype',
             'containers']
    for attr in attrs:
        assert hasattr(fake_exp, attr)

