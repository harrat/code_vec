def test_threading(self, rnt):
        prev_time = time.time()
        rnt.new_task(simple_task).run(start_in=0.1).result()
        time_elapsed = time.time() - prev_time
        is_ok = (time_elapsed < 0.11) and (time_elapsed > 0.09)
        assert is_ok is True
