def test_get_wrapped_key_non_wrapping_key(self):
        """
        Test that the right error is thrown when key wrapping is requested
        with a non-wrapping key in a Get request.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        wrapping_key = pie_objects.SecretData(
            (
                b'\x00\x01\x02\x03\x04\x05\x06\x07'
                b'\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F'
            ),
            enums.SecretDataType.SEED,
            masks=[enums.CryptographicUsageMask.WRAP_KEY]
        )
        wrapping_key.state = enums.State.ACTIVE

        unwrapped_key = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            128,
            (
                b'\x00\x11\x22\x33\x44\x55\x66\x77'
                b'\x88\x99\xAA\xBB\xCC\xDD\xEE\xFF'
            ),
            [enums.CryptographicUsageMask.ENCRYPT]
        )

        e._data_session.add(wrapping_key)
        e._data_session.add(unwrapped_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        wrapping_key_uuid = str(wrapping_key.unique_identifier)
        unwrapped_key_uuid = str(unwrapped_key.unique_identifier)

        cryptographic_parameters = attributes.CryptographicParameters(
            block_cipher_mode=enums.BlockCipherMode.NIST_KEY_WRAP
        )
        payload = payloads.GetRequestPayload(
            unique_identifier=unwrapped_key_uuid,
            key_wrapping_specification=objects.KeyWrappingSpecification(
                wrapping_method=enums.WrappingMethod.ENCRYPT,
                encryption_key_information=objects.EncryptionKeyInformation(
                    unique_identifier=wrapping_key_uuid,
                    cryptographic_parameters=cryptographic_parameters
                ),
                encoding_option=enums.EncodingOption.NO_ENCODING
            )
        )

        args = (payload, )
        self.assertRaisesRegex(
            exceptions.IllegalOperation,
            "The wrapping encryption key specified by the encryption key "
            "information is not a key.",
            e._process_get,
            *args
        )
