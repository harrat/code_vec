def test_wd_ontol_eager():
    """
    test Eager implementation
    """
    xrefs = wd.fetchall_xrefs('HP')
    print("XRs: {}".format(list(xrefs.items())[:10]))
    [doid] = wd.map_id(PTSD, 'DOID')
    logging.info("ID={}".format(doid))
    ont = EagerWikidataOntology('wdq:'+doid)
    logging.info("ONT={}".format(ont))
    for n in ont.nodes():
        logging.info("N={}".format(n))
        logging.info("N={} {}".format(n, ont.label(n)))
        for a in ont.ancestors(n):
            logging.info("A={} {}".format(a, ont.label(a)))        

def test_factory():
    """
    test ontology factory using wikidata as source and using PTSD.
