@autodoc.describe('POST /')
    def test_post(self):
        """ POST / """
        params = {'id': 1, 'message': 'foo'}
        req = self.create_request('http://localhost:5000/', 'POST', params)
        res = self.send(req, params, '../tests/data/post.json')

        self.assertEqual(res.status_code, 200)

        return res
