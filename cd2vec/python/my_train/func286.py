@pytest.mark.last
def test_req_log(client):
    resp = client.get('/req-log/')

    assert resp.status_code == 200
    assert type(resp.get_json().get('data')) is list

