def test_task_should_be_interrupted(self):
        pid = os.getpid()
        bumper = InterruptBumper(3)
        bucket = {}

        def stop_signal():
            for i in range(3):
                time.sleep(1)
                os.kill(pid, signal.SIGINT)
        thread = threading.Thread(target=stop_signal)
        thread.daemon = True
        thread.start()

        with self.assertRaises(KeyboardInterrupt):
            bumperred_task(bumper, bucket)

        self.assertEqual(bumper.attempts, 0)
        self.assertFalse('return' in bucket)
