def test_regression_test_name():
    class MyTest(rfm.RegressionTest):
        def __init__(self, a, b):
            self.a = a
            self.b = b

    test = MyTest(1, 2)
    assert os.path.abspath(os.path.dirname(__file__)) == test.prefix
    assert 'test_regression_test_name.<locals>.MyTest_1_2' == test.name

