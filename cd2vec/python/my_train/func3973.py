def test_h5del_group(self):
        h5create_file(main_path, 'test_deleting2')
        file = '{}/test_deleting2.h5'.format(test_path)


        h5create_group(file, 'testing_group')
        self.assertTrue(h5path_exists(file, 'testing_group'))

        h5del_group(file, 'testing_group')
        self.assertFalse(h5path_exists(file, 'testing_group'))

        h5delete_file(file)
