@responses.activate
    def test_hosts_list(self, centreon_con):
        with open(resource_dir / 'test_hosts_list.json') as data:
            wsresponses = json.load(data)
        responses.add(
            responses.POST,
            self.clapi_url,
            json=wsresponses, status=200, content_type='application/json')
        _, res = centreon_con.hosts.get('mail-uranus-frontend')
        assert res.id == "12"
