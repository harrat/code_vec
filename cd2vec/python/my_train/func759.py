def test_create_from_url(self):
        instance = Paper.create_from_page("https://aclweb.org/anthology/papers/D/D18/D18-1003/", with_arxiv=True)
        self._validate(instance)
