@pytest.mark.parametrize('py_obj, json_obj', [
    (dict_flat, json_flat),
    (dict_nested, json_nested),
])
def test_load(py_obj, json_obj):
    i = io.StringIO(json_obj)
    assert jsonextra.load(i) == py_obj

