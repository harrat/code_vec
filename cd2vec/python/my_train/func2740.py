def test_convert_std_placeholder(self):
        assert STDIN == convert_std_placeholder("-", "r")
        assert STDOUT == convert_std_placeholder("-", "w")
        assert STDERR == convert_std_placeholder("_", "w")
        assert "foo" == convert_std_placeholder("foo")
