def test_get_etag_header(self):
        """Does GETing a resource with the ETag header return a 304?"""
        response = self.get_response('/tracks/1', 200)
        assert 'ETag' in response.headers
        etag_value = response.headers['ETag']
        cached_response = self.get_response('/tracks/1', 304, headers={'If-None-Match': etag_value}, has_data=False)
