@pytest.mark.parametrize("key", ["BUILDING", "Building", "building", "BuIlDiNg"])
def test_contains(base_idf, key):
    idf = base_idf
    idf.newidfobject(key, Name="Building")
    assert key in idf.idfobjects

