@unittest.skipIf(platform.system() == "Windows" or platform.system() == "windows", "test not supported on windows")
    def test_get_config_from_env_variables(self):
        try:
            os.remove(self.config_file_path)
            del os.environ["SECURENATIVE_API_KEY"]
            del os.environ["SECURENATIVE_API_URL"]
            del os.environ["SECURENATIVE_INTERVAL"]
            del os.environ["SECURENATIVE_MAX_EVENTS"]
            del os.environ["SECURENATIVE_TIMEOUT"]
            del os.environ["SECURENATIVE_AUTO_SEND"]
            del os.environ["SECURENATIVE_DISABLE"]
            del os.environ["SECURENATIVE_LOG_LEVEL"]
            del os.environ["SECURENATIVE_FAILOVER_STRATEGY"]
        except FileNotFoundError:
            pass
        except KeyError:
            pass

        os.environ["SECURENATIVE_API_KEY"] = "SOME_ENV_API_KEY"
        os.environ["SECURENATIVE_API_URL"] = "SOME_API_URL"
        os.environ["SECURENATIVE_INTERVAL"] = "6000"
        os.environ["SECURENATIVE_MAX_EVENTS"] = "700"
        os.environ["SECURENATIVE_TIMEOUT"] = "1700"
        os.environ["SECURENATIVE_AUTO_SEND"] = "False"
        os.environ["SECURENATIVE_DISABLE"] = "True"
        os.environ["SECURENATIVE_LOG_LEVEL"] = "Debug"
        os.environ["SECURENATIVE_FAILOVER_STRATEGY"] = "fail-closed"

        options = ConfigurationManager.load_config(None)

        self.assertEqual(options.api_key, "SOME_ENV_API_KEY")
        self.assertEqual(options.api_url, "SOME_API_URL")
        self.assertEqual(options.interval, "6000")
        self.assertEqual(options.timeout, "1700")
        self.assertEqual(options.max_events, "700")
        self.assertEqual(options.auto_send, "False")
        self.assertEqual(options.disable, "True")
        self.assertEqual(options.log_level, "Debug")
        self.assertEqual(options.fail_over_strategy, FailOverStrategy.FAIL_CLOSED.value)
