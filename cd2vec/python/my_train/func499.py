def test_on_next_drop_new_message_when_buffer_full(self):
        mock_observer: MockObserver = MockObserver(include_sleeps=True)
        buffer: SizedBufferBackPressureStrategy = BackPressure.SIZED_BUFFER(
            mock_observer, cache_size=2
        )
        messages = [
            {"id": 0, "payload": "OK"},
            {"id": 1, "payload": "OK"},
            {"id": 2, "payload": "OK"},
            {"id": 3, "payload": "OK"},
            {"id": 4, "payload": "OK"},
            {"id": 5, "payload": "OK"},
            {"id": 6, "payload": "OK"},
            {"id": 7, "payload": "OK"},
        ]

        buffer.on_next(messages[0])
        buffer.on_next(messages[1])
        buffer.on_next(messages[2])
        buffer.on_next(messages[3])

        while buffer.is_locked():
            sleep(0.25)

        buffer.on_next(messages[4])
        buffer.on_next(messages[5])
        buffer.on_next(messages[6])
        buffer.on_next(messages[7])

        while buffer.is_locked():
            sleep(0.25)

        mock_observer.on_next_mock.assert_has_calls(
            [
                call(messages[0]),
                call(messages[1]),
                call(messages[1]),
                call(messages[4]),
                call(messages[5]),
                call(messages[5]),
            ]
        )
        self.assertEqual(4, buffer.counter.get_stats().get("successful_events"))
        self.assertEqual(4, buffer.counter.get_stats().get("dropped_events"))
