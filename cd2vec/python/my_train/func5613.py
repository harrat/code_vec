def functional_test_git_add(self):
        git_file, git_dir = get_unstaged_dir()
        repo = dulwich.repo.Repo(git_dir.name)
        index = repo.open_index()
        try:
            assert list(index) == []
            pwstore.git_add(git_dir.name, git_file.name)
            index = repo.open_index()
            assert list(index) == [os.path.basename(git_file.name).encode()]
        finally:
            git_dir.cleanup()
