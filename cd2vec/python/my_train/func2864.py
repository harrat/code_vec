def test_aa_check(self):

		# Make sure that some other test hasn't set it
		try:
			del model.AttributeAssignment.set_assigned_property
		except:
			pass

		t = model.Type()
		aa = model.AttributeAssignment()
		# First check that aa accepts a type
		aa.assigned_property = t
		# And will not accept a string
		self.assertRaises(model.DataError, aa.__setattr__, "assigned_property", "classified_as")

		# Check we can set anything to assigned / assigned_to
		aa.assigned_property = None
		aa.assigned = aa
		aa.assigned_to = aa
		self.assertEqual(aa.assigned, aa)
		self.assertEqual(aa.assigned_to, aa)

		vocab.add_attribute_assignment_check()

		# This should fail right now as can't classify as an AA
		self.assertRaises(model.DataError, aa.__setattr__, "assigned_property", "classified_as")
		aa.assigned = None
		aa.assigned_to = None
		aa.assigned = t
		aa.assigned_to = t
		aa.assigned_property = "classified_as"
		self.assertEqual(aa.assigned_property, 'classified_as')
