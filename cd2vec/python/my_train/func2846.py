@pytest.mark.parametrize("skill, result",

                         [

                             ('woodcutting', 2),

                             ('wOoDcutTing', 2),

                             ('atTaCk', 1),

                             ('attack', 1),

                             ('ConstItuTion', 10),

                             ('constitution', 10),

                             (3, 10),

                             (8, 2),

                             (0, 1),

                             (26, 1),

                             (13, 1),

                         ])

def test_skill_level(valid_player, skill, result):

    assert valid_player.skill(skill).level == result

