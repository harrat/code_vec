def test_loads_each_split(self):
        train = CnnDailymail(split='train')
        self.assertEqual(len(train), 287_227)
        dev = CnnDailymail(split='dev')
        self.assertEqual(len(dev), 13_368)
        test = CnnDailymail(split='test')
        self.assertEqual(len(test), 11_490)
