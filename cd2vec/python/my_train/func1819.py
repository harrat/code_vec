def testSysInfoCmd(self, tmpdir):
        cmdLine = ['sysinfo']
        self.cwd = str(tmpdir.realpath())
        exitcode, stdout, _ = runZm(self, cmdLine)
        assert exitcode == 0
        assert 'information' in stdout
