@unittest.skipIf(not is_linux(), "Skipping because Signal is not available on non unix systems!")
    def test_tcp_stream(self):
        # limit execution time to 1 second in case of potential deadlocks -> prevent test from running forever
        with time_limit(1):
            self._spawn_test_server()
