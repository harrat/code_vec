@pytest.mark.parametrize('table, alias, out', [
		('s.t', None, '"s"."t"'),
		('t', None, '"t"'),
		('s.t', 't1', '"s"."t" AS "t1"'),
		('t', "t1", '"t" AS "t1"'),
	])
	def testTableFrom(self, table, alias, out):
		assert str(TableFrom(table, alias)) == out
