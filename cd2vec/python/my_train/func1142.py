def test_calc_lwc(class_objects, params_objects):
    d_source, d_class, s_width = class_objects
    obj = CalculateProducts(d_source, params_objects)
    dia, mu, s = [obj.parameters.get(key) for key in ('Do', 'mu', 'S')]
    gamma_ratio = gamma(4 + mu) / gamma(3 + mu) / (3.67 + mu)
    compare = 1000 / 3 * obj.data.beta * s * dia * gamma_ratio
    testing.assert_array_almost_equal(obj._calc_lwc(), compare)

