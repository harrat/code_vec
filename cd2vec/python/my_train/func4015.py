def test_blnet_web_log_in(self):
        """
        Test logging in via different urls
        and that log out after a with clause happens correctly
        """
        with BLNETWeb(self.url, password=PASSWORD, timeout=10) as blnet:
            self.assertTrue(blnet.logged_in())
        # test without http
        blnet = BLNETWeb("{}:{}".format(ADDRESS, self.port), password=PASSWORD, timeout=10)
        with blnet as blnet:
            self.assertTrue(blnet.logged_in())
