def test_requests_match_url(mock):
    body = {'foo': 'bar'}
    mock.get('http://foo.com').reply(200).json(body)

    res = requests.get('http://foo.com')
    assert res.status_code == 200
    assert res.headers == {'Content-Type': 'application/json'}
    assert res.json() == body
    assert pook.isdone() is True

