def test_core_commands():
    Breathe.add_commands(
        None,
        {
            "test one": DoNothing(),
            "test two": DoNothing(),
            "test three": DoNothing(),
            "banana [<n>]": DoNothing() * Repeat("n"),
        },
        [IntegerRef("n", 1, 10, 1)],
    )
    engine.mimic(["test", "three", "test", "two", "banana", "five"])

