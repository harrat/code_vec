def test_aggregator_delete_method(mocked_aggregated):
    """Test aggregator delete method."""

    res = mocked_aggregated.delete('pattern')
    assert type(res) is int
    for node in mocked_aggregated._aggregator._redis_nodes:
        assert not hasattr(node, 'name')

