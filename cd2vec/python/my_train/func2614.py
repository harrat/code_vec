def test_inside_nonspeech_no_explicit_run_vad(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        for index in [
            -2,
            -1,
            audiofile.all_length,
            audiofile.all_length + 1,
            audiofile.all_length + 2
        ]:
            self.assertIsNone(audiofile.inside_nonspeech(index))
        for begin, end in audiofile.intervals(False, False):
            self.assertIsNone(audiofile.inside_nonspeech(begin - 1))
            self.assertEqual(audiofile.inside_nonspeech(begin), (begin, end))
            self.assertEqual(audiofile.inside_nonspeech(begin + 1), (begin, end))
            self.assertEqual(audiofile.inside_nonspeech(end - 1), (begin, end))
            self.assertIsNone(audiofile.inside_nonspeech(end))
            self.assertIsNone(audiofile.inside_nonspeech(end + 1))
