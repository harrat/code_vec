def test_mipgen(capfd):
    "Check that we can convert a MIPGEN table"

    wd_path = os.path.join(packagedir, "sample_data", "wd_mipgen")
    init_wd(wd_path, os.path.join(packagedir, "sample_data", "sample_reads_in"))

    amplimap.run.main(['--working-directory={}'.format(wd_path), 'pileups'])
    captured = capfd.readouterr()
    assert '{} {} dry run successful.'.format(amplimap.run.__title__, amplimap.run.__version__) in captured.err.strip()

    amplimap.run.main(['--run', '--working-directory={}'.format(wd_path), 'pileups'])
    captured = capfd.readouterr()

    assert '2/3 probes left after merging probes by ID' in captured.err.strip()
    assert 'ABORTED: Did not find any read pairs with the expected primers sequences' in captured.err.strip()
    assert 'The UMI length settings are currently {} bp for read one and {} bp for read two.'.format(
        0, 0
    ) in captured.err.strip()
