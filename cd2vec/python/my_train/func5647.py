def test_duplicate_names(self):
        self.create_default_experiment()

        # Attempt creating a session with duplicate name
        cluster = Cluster.new('tmux', server_name=_TEST_SERVER)
        dupe = cluster.new_experiment('exp')

        with self.assertRaises(errors.ResourceExistsError):
            cluster.launch(dupe)

        # Attempt creating a process with duplicate name
        dupe = cluster.new_experiment('exp')
        dupe.new_process('alone', ['echo Do I exist already?'])

        with self.assertRaises(errors.ResourceExistsError):
            cluster.launch(dupe)
