def test_raises_exception_if_secure_channels_are_specified_without_auth(
            self):
        with pytest.raises(SubscribeToSecureChannelException):
            channels = [
                Channels.connected, SecureChannels.margin]
