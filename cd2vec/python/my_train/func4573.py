@pytest.mark.vcr()
def test_workbench_vulns_age_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vulns(age='none')
