def test_indent_around_ctx(get_log):

    @logger.indented("hey")
    @contextmanager
    def ctx():
        logger.info("001")
        yield
        logger.info("003")

    with ctx():
        logger.info("002")

    assert get_log() == "hey\n001\n002\n003\nDONE in no-time (hey)\n"

