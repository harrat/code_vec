def test_ufactor_2_layer_construction(self):
        self.idf.initreadtxt(double_layer)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        expected = 1 / (
            INSIDE_FILM_R
            + m.Thickness / m.Conductivity
            + m.Thickness / m.Conductivity
            + OUTSIDE_FILM_R
        )
        assert c.ufactor == expected
        assert c.ufactor == 1 / 0.55
