def test_execute_wrong_no_of_args1():
    global cu
    with pytest.raises(py2jdbc.ProgrammingError):
        cu.execute("insert into tests(id, name) values (?, ?)",
                   (6, 'test_execute_wrong_no_of_args1', "Egon"))
