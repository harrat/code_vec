def test_data_store_fails_at_the_beginning(self):
        temp_output = StringIO()
        with pytest.raises(SystemExit), redirect_stdout(temp_output):
            DataStore(
                db_name="test",
                db_host="localhost",
                db_username="TEST",
                db_password="TEST",
                db_port=55527,
                db_type="postgres",
            )
        output = temp_output.getvalue()
        assert "ERROR: SQL error when communicating with database" in output
        assert "Please check your database file and the config file's database section." in output
        assert (
            "Current database URL: 'postgresql+psycopg2://TEST:TEST@localhost:55527/test'" in output
        )
