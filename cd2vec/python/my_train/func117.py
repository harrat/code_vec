@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_delete_snippet_008(snippy):
        """Delete snippet with data.

        Delete snippet based on content data.
        """

        content = {
            'data': [
                Snippet.FORCED
            ]
        }
        Content.assert_storage_size(2)
        cause = snippy.run(['snippy', 'delete', '--content', 'docker rm --volumes $(docker ps --all --quiet)'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
