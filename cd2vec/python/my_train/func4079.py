def test_run_twice(self):
        """Test that runs code in the background twice.

        ASCII art depicting timeline shown below:
          0        1        2        3        4 seconds|
        --+--------+--------+--------+--------+--------+
          ^        ^        ^    ^       ^    ^        |Spawned
        "start"    |      "end"  |       |    |        |process
                   |             |Windows|    |        |-------
           "try_to_run_fail"     |Kills  | "run_twice" |Test
                                 |Process|             |Checks

        """
        filetwice = 'test_run_base_back.py'
        launch = Launcher(filetwice, 'URL')
        launch.run(True)
        time.sleep(1)
        #Process is still alive
        with pytest.raises(ProcessRunningException):
            launch.run(True)
        launch.process_join()
        assert launch.process_exitcode == 0
        #Process is dead now, can run again
        launch.run()
        assert launch.process_exitcode == 0
