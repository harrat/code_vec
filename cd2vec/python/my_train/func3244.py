@staticmethod
    @pytest.mark.usefixtures('yaml')
    def test_cli_import_solution_025(snippy):
        """Import solutions defaults.

        Import solution defaults. All solutions should be imported from
        predefined file location under tool data folder from yaml format.
        """

        content = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '--defaults'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            defaults_solutions = pkg_resources.resource_filename('snippy', 'data/defaults/solutions.yaml')
            Content.assert_arglist(mock_file, defaults_solutions, mode='r', encoding='utf-8')
