def test_stock_code_with_prefix(self):
        cases = ["sina", "qq"]
        for src in cases:
            q = easyquotation.use(src)
            data = q.market_snapshot(prefix=True)
            for k in data.keys():
                self.assertRegex(k, r"(sh|sz)\d{6}")
