def test_parser_definition_type_option(self):
        self.assertIs(
            self.get_opts('test').def_env_type,
            None
        )
        self.assertEqual(
            self.get_opts(
                'test', '--def-env-type', '255'
            ).def_env_type,
            '0xff'
        )
