def test_pytest(test, result):
    if result:
        test(result)
    else:
        test()
