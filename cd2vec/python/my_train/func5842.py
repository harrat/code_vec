@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_delete_reference_007(snippy):
        """Delete reference with uuid.

        Try to delete content with uuid that matches to more than one content.
        In this case nothing should get deleted because the operatione permits
        only one content to be deleted in one operation.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        cause = snippy.run(['snippy', 'delete', '-u', '1'])
        assert cause == 'NOK: cannot find content with content uuid: 1'
        Content.assert_storage(content)
