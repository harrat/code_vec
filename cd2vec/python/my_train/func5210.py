@pytest.mark.parametrize(('data_type','debug_level'), memory_leak_matrix)
def test_long_run_for_memory_leak(data_type, debug_level):
    """
    Simulate long running process with and without debug
    and control memory usage
    """
    initial_memory, final_memory = long_run(data_type, debug_level)
    assert initial_memory == final_memory

