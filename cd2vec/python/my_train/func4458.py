def test_user_cannot_get_other_user_detail(self):
        login_user(self.client, self.user)
        response = self.client.get(reverse("user-detail", args=[self.superuser.pk]))

        self.assert_status_equal(response, status.HTTP_404_NOT_FOUND)
