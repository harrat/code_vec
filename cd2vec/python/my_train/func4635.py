@staticmethod
    def test_help_option_001(capsys, caplog):
        """Test printing help from console.

        Print help with long option.
        """

        snippy = Snippy(['snippy', '--help'])
        snippy.run()
        snippy.release()
        out, err = capsys.readouterr()
        assert out == Const.NEWLINE.join(TestCliOptions.HELP)
        assert not err
        assert not caplog.records[:]
        Content.delete()
