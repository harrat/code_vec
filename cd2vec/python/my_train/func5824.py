def test_flex_alloc_no_num_tasks_per_node(make_flexible_job):
    job = make_flexible_job('all')
    job.num_tasks_per_node = None
    job.options = ['-C f1&f2', '--partition=p1,p2']
    prepare_job(job)
    assert job.num_tasks == 1

