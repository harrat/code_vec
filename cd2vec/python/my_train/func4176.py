@pytest.mark.skipif(sys.version_info < (3, 6), reason="requires python3.6 or higher")
    def test_verify_ca_bundle(self, request, httpbin_secure, httpbin_ca_bundle):
        """Test quiet_urllib=False no warning from urllib3 when using ca bundle."""
        url = httpbin_secure.url
        http = Http(url=url, certwarn=False)
        response = http()
        assert response.status_code == 200
