def test_parses_correct_schedule(self):
        """Expected behavior: finds file with preset filename, opens and parses,
        returning object containing schedule dicts / lists"""
        # self.assert? schedule dicts / lists produced properly
        parsed_schedule = cycle_calendar_generator.parseTeacherSchedule(
                self.wb_schedule_good,
                self.setupData)
        self.assertIsInstance(parsed_schedule,
                              cycle_calendar_generator.ScheduleData)
        self.assertEqual(self.parsed_userSchedule.userSchedule,
                         parsed_schedule.userSchedule)
