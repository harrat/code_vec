def test_h5create_group(self):
        file = test_file_path
        h5create_group(file,'group1/group2')
        result1 = h5path_exists(file, '/group1/')
        result2 = h5path_exists(file, '/group1/group2')
        self.assertTrue(result1)
        self.assertTrue(result2)
