@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_mlb_integration_dataframe_returns_dataframe(self, *args,
                                                         **kwargs):
        teams = Teams()
        df = pd.DataFrame([self.results], index=['HOU'])

        houston = teams('HOU')
        # Pandas doesn't natively allow comparisons of DataFrames.
        # Concatenating the two DataFrames (the one generated during the test
        # and the expected one above) and dropping duplicate rows leaves only
        # the rows that are unique between the two frames. This allows a quick
        # check of the DataFrame to see if it is empty - if so, all rows are
        # duplicates, and they are equal.
        frames = [df, houston.dataframe]
        df1 = pd.concat(frames).drop_duplicates(keep=False)

        assert df1.empty
