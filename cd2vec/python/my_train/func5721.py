def test_task_should_finished(self):
        pid = os.getpid()
        bumper = InterruptBumper(3)
        bucket = {}

        def stop_signal():
            time.sleep(1)
            os.kill(pid, signal.SIGINT)
        thread = threading.Thread(target=stop_signal)
        thread.daemon = True
        thread.start()

        with self.assertRaises(KeyboardInterrupt):
            bumperred_task(bumper, bucket)

        self.assertEqual(bumper.attempts, 2)
        self.assertEqual(bucket['return'], 1)
