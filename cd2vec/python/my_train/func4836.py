def test_add_and_get_address(self):
        member = self.client.create_member(utils.generate_alias())
        name = generate_nonce()
        payload = utils.generate_address()
        address = member.add_address(name, payload)
        result = member.get_address(address.id)
        assert name == address.name
        assert payload == address.address
        assert address == result
