def test_compress_file_no_dest(self):
        path = self.root.make_file()

        with self.assertRaises(ValueError):
            compress_file(path, compression=True, keep=True)

        with open(path, 'wt') as o:
            o.write('foo')
        gzfile = compress_file(path, compression='gz', keep=False)
        assert gzfile == Path(str(path) + '.gz')
        assert not path.exists()
        assert gzfile.exists()
        with gzip.open(gzfile, 'rt') as i:
            assert i.read() == 'foo'
