def test_gbb_solver_known_solution_problem_1(self):
        problem = skp.ProcrustesProblem((4, 4, 2, 2), problemnumber=1)
        mysolver = skp.GBBSolver(verbose=0)
        result = mysolver.solve(problem)
        assert_allclose(result.solution, problem.Xsol, atol=1e-3)
        assert_allclose(result.fun, 1e-6, atol=1e-2)
