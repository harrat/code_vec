@pytest.mark.parametrize("node, right",
    ((node_12, leaf_2), (node_34, leaf_4), (root, node_34)))
def test_right_parent(node, right):
    """
    Tests left parent for all valid cases
    """
    assert node.right is right

