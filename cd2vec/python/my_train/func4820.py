def test_match(self):
        self.assertIsNotNone(self.hockey_match)
        self.assertEqual(str(self.hockey_match), 'Pittsburgh Penguins 2-0 Nashville Predators')
        self.assertEqual(repr(self.hockey_match), 'Pittsburgh Penguins 2-0 Nashville Predators')

        self.assertIsNotNone(sports.all_matches())
