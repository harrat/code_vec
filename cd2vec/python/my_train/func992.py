def test_expression_serialization(valid_expression_predictor_data):
    """Ensure that a serialized ExpressionPredictor looks sane."""
    predictor = ExpressionPredictor.build(valid_expression_predictor_data)
    serialized = predictor.dump()
    serialized['id'] = valid_expression_predictor_data['id']
    assert serialized == valid_serialization_output(valid_expression_predictor_data)

