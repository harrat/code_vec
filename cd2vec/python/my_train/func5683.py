def test_pick_all():
    vals = optimiser.pickUnique(10,10,[])
    # Should be a scrambled list of [0...10]

    assert vals != list(range(0,10)) # Probably not sorted
    assert sorted(vals) == list(range(0,10))

def test_pick_different():
    v1 = optimiser.pickUnique(20, 3, [])
    v2 = optimiser.pickUnique(20, 3, [])
    assert v1 != v2
