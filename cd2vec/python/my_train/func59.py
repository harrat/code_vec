@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'export-time')
    def test_cli_export_solution_007(snippy):
        """Export solution with ``--digest`` option.

        Export resource based on message digest. The File name is defined in
        solution ``filename`` attribute but not with ``--file``command line
        option.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Content.deepcopy(Solution.BEATS)
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '-d', '4346ba4c79247430'])
            assert cause == Cause.ALL_OK
            Content.assert_text(mock_file, 'howto-debug-elastic-beats.txt', content)
