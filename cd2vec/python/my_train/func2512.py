@pytest.mark.parametrize("col", ["resolution", "duration", "fps"])
def test_video_info_has_columns(col, video_info):
    assert col in video_info.columns

