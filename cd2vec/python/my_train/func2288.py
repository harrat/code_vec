def test_start_with_description_uses_description(cli, entries_file):
    expected = """20/01/2014

alias_1 09:00-? Play ping-pong
"""

    with freeze_time('2014-01-20 09:00:00'):
        cli('start', ['alias_1', 'Play ping-pong'])
    assert entries_file.read() == expected

