@mark.django_db
def test_list(client_with_perms):
    client = client_with_perms("testapp.view_dragonfly")
    Dragonfly.objects.create(name="alpha", age=12)
    Dragonfly.objects.create(name="omega", age=99)
    response = client.get(DragonflyViewSet().links["list"].reverse())
    assert b"alpha" in response.content
    assert b"omega" in response.content

