def test_basic_site(basic_config):
    strange_case(basic_config)

    path_contents = {
        '001_2012_01_16_file.html': '1. 2012-01-16',
        'index.html': """<doctype html>
<body>
<h1 id="welcome-to-my-blog">Welcome to my blog!</h1>

<p>It is pretty great.</p>
</body>""",
            'blogs': {
                'index.html': """<doctype html>
<body>
<p>My blogs:</p>

<ol>
<li><p><a href="/blogs/2012_01_01_post1.html">Post1</a></p></li>
<li><p><a href="/blogs/2012_01_02_post2.html">Post2</a></p></li>
</ol>
<p>Hi!</p>

</body>""",
            '2012_01_01_post1.html': """<doctype html>
<body>
<p>My first post, on 2012-01-01.</p>
</body>""",
            '2012_01_02_post2.html': """<doctype html>
<body>
<p>My second post, on 2012-01-02.</p>
</body>""",
        },
    }
    check_path_contents(basic_config['deploy_path'], path_contents)

