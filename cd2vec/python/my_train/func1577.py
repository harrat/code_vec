@can_only_run_on_linux_64
    def test_fallback_bad(self):
        # Given a server that does NOT support SCSV
        with LegacyOpenSslServer() as server:
            server_location = ServerNetworkLocationViaDirectConnection(
                hostname=server.hostname, ip_address=server.ip_address, port=server.port
            )
            server_info = ServerConnectivityTester().perform(server_location)

            # When testing for SCSV, it succeeds
            result: FallbackScsvScanResult = FallbackScsvImplementation.scan_server(server_info)

        # And the server is reported as NOT supporting SCSV
        assert not result.supports_fallback_scsv
