@pytest.mark.parametrize(
        "to_register, output",
        [
            # With default "priority" - in order they were added
            (
                (
                    {"lambda": lambda event: print("First"), "priority": 0},
                    {"lambda": lambda event: print("Second"), "priority": 0},
                ),
                "First\nSecond\n",
            ),
            # Based on priority
            (
                (
                    {"lambda": lambda event: print("First"), "priority": 0},
                    {"lambda": lambda event: print("Second"), "priority": -100},
                ),
                "Second\nFirst\n",
            ),
        ],
    )
    def test_listeners_executed_in_order(self, to_register, output, capsys):
        py_event_dispatcher = EventDispatcher()
        for register in to_register:
            py_event_dispatcher.register(
                "foo.bar", register["lambda"], register["priority"]
            )
        py_event_dispatcher.dispatch(Event("foo.bar", {"a": "b"}))

        captured = capsys.readouterr()

        assert captured.out == output
