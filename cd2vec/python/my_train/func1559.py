def test_calc_dia(class_objects):
    d_source, d_class, s_width = class_objects
    obj = DrizzleSolving(d_source, d_class, s_width)
    beta_z = np.array([1, 2, 3])
    compare = (gamma(3) / gamma(7) * 3.67 ** 4 / beta_z) ** (1 / 4)
    testing.assert_array_almost_equal(obj._calc_dia(beta_z), compare)

