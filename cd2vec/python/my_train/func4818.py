def test_nginx_check_banner(self):
        res = nginx.check_banner("nginx/1.0.0", "head_data", "http://example.com")

        self.assertTrue(any("Nginx Version Exposed" in r.message for r in res))
