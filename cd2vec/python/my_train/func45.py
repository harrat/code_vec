@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_nhl_skater_returns_player_season_stats(self, *args, **kwargs):
        # Request the 2017 stats
        player = Player('zettehe01')
        player = player('2017-18')

        for attribute, value in self.skater_results_2017.items():
            assert getattr(player, attribute) == value
