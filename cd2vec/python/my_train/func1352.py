def test_set_head_bad1(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        with self.assertRaises(TypeError):
            audiofile.set_head_middle_tail(head_length=0.000)
