def test(self):
        self.task_file = "/tmp/tasks.yaml"
        with open(self.task_file, "w") as f:
            f.write("""
            tasks:
              packa:
                directory: /tmp/testpacker1
                ignore:
                  - name: b.txt
              
              packb:
                directory: /tmp/testpacker2
            """)

        t = packer.Packer(self.task_file)
        collection_path = t.collect()
        print(collection_path)
        struct = [
            "tasks.yml",
            {
                "packa": [
                    "1.txt",
                    {
                        "a": ["a.txt"]
                    }
                ]
            },
            {
                "packb": [
                    "2.txt",
                    {
                        "b": ["b.txt"]
                    }
                ]
            }
        ]
        self.assertTrue(utils.compare_struct_tree_and_dirs(struct, collection_path))
        
        os.system("rm -rf %s" % collection_path)
