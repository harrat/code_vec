def test_find_indices(class_objects, params_objects):
    d_source, d_class, s_width = class_objects
    obj = CalculateProducts(d_source, params_objects)
    obj.parameters['Do'] = np.array([[0.0, 1.0, 1.0],
                                     [1.0, 1.0, 0.0]])
    x, y = obj._find_indices()
    compare = (np.array([0, 0, 1, 1]),
               np.array([1, 2, 0, 1]))
    testing.assert_array_almost_equal(x, compare)

