def test_data_region_airborne_survey():
    """
    Test if a different cut_region produces a different airborne survey
    """
    data_region = (-7, -2, 53, 58)  # a bigger data_region than the default one
    region = (-10.1, 9.7, -20.3, -10.5)  # a random region to scale the survey
    survey = airborne_survey(region=region, data_region=data_region)
    assert survey.longitude.size > 5673
    npt.assert_allclose(survey.longitude.min(), region[0])
    npt.assert_allclose(survey.longitude.max(), region[1])
    npt.assert_allclose(survey.latitude.min(), region[2])
    npt.assert_allclose(survey.latitude.max(), region[3])
    assert survey.height.min() <= 359.0
    assert survey.height.max() >= 1255.0

