def test_log_plugin_with_initialization():
    logger = logging.getLogger('test_handler')
    log_handler = ThundraLogHandler()
    logger.addHandler(log_handler)
    logger.setLevel(logging.INFO)
    logger.info("This is an info log")

    assert len(logs) == 1
    log = logs[0]

    assert log['logMessage'] == "This is an info log"
    assert log['logContextName'] == 'test_handler'
    assert log['logLevel'] == "INFO"
    assert log['logLevelCode'] == 2
    del logs[:]

