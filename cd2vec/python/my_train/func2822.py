@staticmethod
    def test_cli_search_snippet_053(snippy, capsys):
        """Search snippets with ``--sall`` option.

        Search snippets from all resource attributes. A ``created`` attribute
        of a single snippet produces a match. The match must be made from a
        partial timestamp based on year-month-date.
        """

        Content.store({
            'category': Const.SNIPPET,
            'data': [
                'tar cvfz mytar.tar.gz --exclude="mytar.tar.gz" ./'],
            'brief': 'Manipulate compressed tar files',
            'groups': ['linux'],
            'tags': ['howto', 'linux', 'tar', 'untar'],
            'created': '2019-03-02T02:02:02.000001+00:00'
        })
        output = (
            '1. Manipulate compressed tar files @linux [89f7fc446f0214d4]',
            '',
            '   $ tar cvfz mytar.tar.gz --exclude="mytar.tar.gz" ./',
            '',
            '   # howto,linux,tar,untar',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--all', '2019-03-02', '--no-ansi'])
        out, err = capsys.readouterr()
        out = Helper.remove_ansi(out)
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
