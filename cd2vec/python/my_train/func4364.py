def test_remove_good_profiles():
    top_c = np.asarray([[1, 1, 0], [1, 0, 1]], dtype=bool)
    compare = np.asarray([[1, 1, 0], [0, 0, 0]], dtype=bool)
    assert_array_equal(STATUS_OBJ._remove_good_profiles(top_c), compare)

