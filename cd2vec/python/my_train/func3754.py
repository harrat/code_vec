def test_lookup():
    res = powo.lookup('urn:lsid:ipni.org:names:320035-2')
    assert res['name'] == 'Poa annua'

def test_lookup_with_extra_fields():
    res = powo.lookup('urn:lsid:ipni.org:names:320035-2', include=['distribution', 'descriptions'])
    assert 'distribution' in res
    assert 'descriptions' in res
