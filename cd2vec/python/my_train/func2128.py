def test_element_function_names():
    # run a simple check to ensure there are no integers
    # in the function names
    letters = string.ascii_uppercase + string.ascii_lowercase

    for elem in circuit_elements.keys():
        for char in elem:
            assert char in letters, \
                f'{char} in {elem} is not in the allowed set of {letters}'
