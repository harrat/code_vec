def test_base_parse_filter_sjson(api, fitem, fset):
    assert {
        'filter.0.filter': 'distro',
        'filter.0.quality': 'match',
        'filter.0.value': 'win'
    } == api.agents._parse_filters(fitem, fset, rtype='sjson')
