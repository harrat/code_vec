def test_clientx_error(tcp_server):
    """
    Test raising error.

    :param tcp_server: tuple with host and port of testing server
    """
    host, port = tcp_server

    loop = asyncio.get_event_loop()

    # testing connection to port 0
    with pytest.raises((OSError, asyncio.TimeoutError)):
        loop.run_until_complete(clientx(host, 0, b"", 1, max_try=1))

    # testing connection to not exist port
    with pytest.raises((OSError, asyncio.TimeoutError)):
        loop.run_until_complete(clientx(host, 65432, b"", 1, max_try=1))

    # testing connection to not exist address
    with pytest.raises((OSError, asyncio.TimeoutError)):
        loop.run_until_complete(clientx("hola", port, b"", 1, max_try=1))

    # testing time out
    with pytest.raises((OSError, asyncio.TimeoutError)):
        loop.run_until_complete(clientx(host, port, b"123", 16, time_out=0.005, max_try=1))
