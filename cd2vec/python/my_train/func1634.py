@pytest.mark.vcr()
def test_workbench_vuln_outputs_filter_type_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.vuln_outputs(19506, filter_type='NOT')
