def test_get_modeIDs():
    """Test for function that gets the mode ID from asy_fit"""
    
    # setup
    st = cs.st
    norders = cs.pars['norders']
    func = st.asy_fit.get_modeIDs 
    
    # simple tests
    pbt.does_it_return(func, [st.asy_fit.fit, norders])
    pbt.right_type(func, [st.asy_fit.fit, norders], pd.DataFrame)
    
    # check that median absolute deviation is zero for all parameters
    df = st.asy_fit.get_modeIDs(st.asy_fit.fit, norders)
    assert(all(df['nu_mad'].values == np.zeros(2*norders)))
    
    # check that median values are the same as for test setup
    assert_allclose(df['nu_med'], np.array([89.05, 90.05, 99., 100.]))
    
def test_get_summary_stats():
    """Test for method for getting summary stats from asy_fit"""
