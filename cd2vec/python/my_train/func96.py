def test_authentication_and_cancel(cert_and_key, ip_address):
    """Authenticate call and then cancel it"""

    c = bankid.BankIDJSONClient(certificates=cert_and_key, test_server=True)
    out = c.authenticate(ip_address, _get_random_personal_number())
    assert isinstance(out, dict)
    # UUID.__init__ performs the UUID compliance assertion.
    order_ref = uuid.UUID(out.get("orderRef"), version=4)
    collect_status = c.collect(out.get("orderRef"))
    assert collect_status.get("status") == "pending"
    assert collect_status.get("hintCode") in ("outstandingTransaction", "noClient")
    success = c.cancel(str(order_ref))
    assert success
    with pytest.raises(bankid.exceptions.InvalidParametersError):
        collect_status = c.collect(out.get("orderRef"))
