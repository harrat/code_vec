@mark.django_db
def test_delete_protected_not_allowed(client_with_perms):
    client = client_with_perms("testapp.delete_dragonfly")
    alpha = Dragonfly.objects.create(name="alpha", age=47)
    ProtectedSighting.objects.create(dragonfly=alpha, name="A related sighting")

    delete_url = DragonflyViewSet().links["delete"].reverse(alpha)
    response = client.get(delete_url)

    assert b"You can't delete" in response.content
    assert b"the following objects depend" in response.content
    assert b"sighting" in response.content

    response = client.post(delete_url)

    assert response.status_code == 403

    assert Dragonfly.objects.filter(name="alpha").exists()

