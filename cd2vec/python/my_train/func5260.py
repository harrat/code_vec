@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_python_exitcode(fixture, request):
    engine = request.getfixturevalue(fixture)
    fn = pyccc.PythonCall(function_tests.sleep_then_exit_38)
    job = engine.launch(image=PYIMAGE, command=fn, interpreter=PYVERSION)
    print(job.rundata)

    with pytest.raises(pyccc.JobStillRunning):
        job.exitcode

    job.wait()
    assert job.wait() == 38
    assert job.exitcode == 38

