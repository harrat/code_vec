def test_prompt_return_json_with_segments():
    class CommandArguments(object):
        last_command_status = constants.SUCCESS
        json = True

    json = prompt.prompt(args=CommandArguments)

    assert len(json) == 5
    
