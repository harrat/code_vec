def test_preamble_commandset(self):
        """Test written OK with preamble/command set"""
        ds = dcmread(ct_name)
        del ds[:]
        del ds.file_meta
        ds.CommandGroupLength = 8
        ds.MessageID = 1
        ds.MoveDestination = 'SOME_SCP'
        ds.Status = 0x0000
        ds.save_as(self.fp, write_like_original=True)
        self.fp.seek(0)
        assert ds.preamble == self.fp.read(128)
        assert b'DICM' == self.fp.read(4)
        # Ensure Command Set Elements written as little endian implicit VR
        assert (b'\x00\x00\x00\x00\x04\x00\x00\x00\x08\x00\x00\x00' ==
                self.fp.read(12))

        fp = BytesIO(self.fp.getvalue())  # Workaround to avoid #358
        ds_out = dcmread(fp, force=True)
        assert Dataset() == ds_out.file_meta
        assert 'Status' in ds_out
        assert 'PatientID' not in ds_out
        assert Dataset() == ds_out[0x00010000:]
