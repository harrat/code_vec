def test_find(self):
        self.spreadsheet.add_worksheet('testFind1', 10, 10)
        self.spreadsheet.add_worksheet('testFind2', 10, 10)
        self.spreadsheet.worksheet('title', 'testFind1').update_row(1, ['test', 'test'])
        self.spreadsheet.worksheet('title', 'testFind2').update_row(1, ['test', 'test'])

        cells = self.spreadsheet.find('test')

        assert isinstance(cells, list)
        assert len(cells) == 3
        assert len(cells[0]) == 0
        assert len(cells[1]) == 2

        self.spreadsheet.del_worksheet(self.spreadsheet.worksheet('title', 'testFind1'))
        self.spreadsheet.del_worksheet(self.spreadsheet.worksheet('title', 'testFind2'))
