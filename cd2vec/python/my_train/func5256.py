def test_invalid_config_1(clean):
    shutil.copyfile('tests/data/error_config_1.ini', clean)
    with pytest.raises(ValueError, match='Invalid key: doc-color1'):
        import pdir
