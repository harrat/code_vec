def test_coverage_not_achieved() -> None:
    reportfilepath = "/tmp/reportfile_{}".format(randint(0, 9999))

    with open(reportfilepath, "w") as f:
        f.write("49 50")

    p = Popen(
        ["python", "-m", "typecov", "98.001", reportfilepath], stdout=PIPE, stderr=PIPE
    )
    stdout, stderr = p.communicate()

    os.remove(reportfilepath)

    assert p.returncode == 1
    assert stdout.decode() == ""
    assert stderr.decode() == (
        "fail: Required minimum type coverage of 98.001% not achieved."
        " Total coverage: 98.0%\n"
    )
