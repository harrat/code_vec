def test_make_p2wpkh_output_script(self):
        self.assertEqual(
            tb.make_p2wpkh_output_script(helpers.PK['ser'][0]['pk']),
            helpers.PK['ser'][0]['pkh_p2wpkh_output'])
