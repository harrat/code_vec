def test_load_custom_default(cellpy_data_instance):
    from cellpy import prms

    file_name = fdv.custom_file_paths
    prms.Instruments.custom_instrument_definitions_file = None
    cellpy_data_instance.set_instrument("custom")
    cellpy_data_instance.from_raw(file_name)
    cellpy_data_instance.make_step_table()
    cellpy_data_instance.make_summary()
    summary = cellpy_data_instance.cell.summary
    val = summary.loc[2, "shifted_discharge_capacity_u_mAh_g"]
    assert 593.031 == pytest.approx(val, 0.1)

