def test_target_bundle(self):
        relationship = copy.deepcopy(self.valid_relationship)
        relationship['target_ref'] = "bundle--31b940d4-6f7f-459a-80ea-9c1f17b5891b"
        results = validate_parsed_json(relationship, self.options)
        self.assertEqual(results.is_valid, False)
        self.assertEqual(len(results.errors), 1)
