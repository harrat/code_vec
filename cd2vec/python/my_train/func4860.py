def test_ufactor_airgap_construction(self):
        self.idf.initreadtxt(air_gap)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        a = self.idf.getobject("MATERIAL:AIRGAP", "AirGap")
        expected = 1 / (
            INSIDE_FILM_R
            + m.Thickness / m.Conductivity
            + a.Thermal_Resistance
            + m.Thickness / m.Conductivity
            + OUTSIDE_FILM_R
        )
        assert almostequal(c.ufactor, expected, places=2)
        assert almostequal(c.ufactor, 1 / 0.65, places=2)
