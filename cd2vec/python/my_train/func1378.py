def test_parse_udf_iso_hidden(tmpdir):
    indir = tmpdir.mkdir('udfisohidden')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'foo'), 'wb') as outfp:
        outfp.write(b'foo\n')

    subprocess.call(['genisoimage', '-v', '-v', '-no-pad', '-iso-level', '1',
                     '-udf', '-hide', 'foo', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_udf_iso_hidden)

@pytest.mark.slow
def test_parse_udf_very_large(tmpdir):
    indir = tmpdir.mkdir('udfverylarge')
    outfile = str(indir)+'.iso'
    largefile = os.path.join(str(indir), 'foo')
    with open(largefile, 'wb') as outfp:
        outfp.truncate(1073739776+1)
