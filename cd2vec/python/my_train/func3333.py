def test_long_specific_char_set(self, allow_invalid_values):
        """Test that specific character set is read even if it is longer
         than defer_size"""
        ds = Dataset()

        long_specific_char_set_value = ["ISO 2022IR 100"] * 9
        ds.add(DataElement(0x00080005, "CS", long_specific_char_set_value))

        msg = (
            r"Unknown encoding 'ISO 2022IR 100' - using default encoding "
            r"instead"
        )

        fp = BytesIO()
        file_ds = FileDataset(fp, ds)
        with pytest.warns(UserWarning, match=msg):
            file_ds.save_as(fp, write_like_original=True)

        with pytest.warns(UserWarning, match=msg):
            ds = dcmread(fp, defer_size=65, force=True)
            assert long_specific_char_set_value == ds[0x00080005].value
