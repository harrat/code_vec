def test_get_attribute_from_managed_object(self):
        """
        Test that an attribute can be retrieved from a given managed object.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        symmetric_key = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b'',
            masks=[enums.CryptographicUsageMask.ENCRYPT,
                   enums.CryptographicUsageMask.DECRYPT]
        )
        certificate = pie_objects.X509Certificate(
            b''
        )
        opaque_object = pie_objects.OpaqueObject(
            b'',
            enums.OpaqueDataType.NONE
        )

        e._data_session.add(symmetric_key)
        e._data_session.add(certificate)
        e._data_session.add(opaque_object)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Unique Identifier'
        )
        self.assertEqual('1', result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Name'
        )
        self.assertEqual(
            [attributes.Name(
                attributes.Name.NameValue('Symmetric Key'),
                attributes.Name.NameType(
                    enums.NameType.UNINTERPRETED_TEXT_STRING
                )
            )],
            result
        )

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Object Type'
        )
        self.assertEqual(enums.ObjectType.SYMMETRIC_KEY, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Cryptographic Algorithm'
        )
        self.assertEqual(enums.CryptographicAlgorithm.AES, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Cryptographic Length'
        )
        self.assertEqual(0, result)

        result = e._get_attribute_from_managed_object(
            certificate,
            'Cryptographic Parameters'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            certificate,
            'Cryptographic Domain Parameters'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            certificate,
            'Certificate Type'
        )
        self.assertEqual(enums.CertificateType.X_509, result)

        result = e._get_attribute_from_managed_object(
            certificate,
            'Certificate Length'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            certificate,
            'X.509 Certificate Identifier'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            certificate,
            'X.509 Certificate Subject'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            certificate,
            'X.509 Certificate Issuer'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            certificate,
            'Certificate Identifier'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            certificate,
            'Certificate Subject'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            certificate,
            'Certificate Issuer'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            certificate,
            'Digital Signature Algorithm'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            opaque_object,
            'Digest'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Operation Policy Name'
        )
        self.assertEqual('default', result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Cryptographic Usage Mask'
        )
        self.assertEqual(
            [enums.CryptographicUsageMask.ENCRYPT,
             enums.CryptographicUsageMask.DECRYPT],
            result
        )

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Lease Time'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Usage Limits'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'State'
        )
        self.assertEqual(enums.State.PRE_ACTIVE, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Initial Date'
        )
        self.assertIsNotNone(result)
        self.assertIsInstance(result, six.integer_types)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Activation Date'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Process Start Date'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Protect Stop Date'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Deactivation Date'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Destroy Date'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Compromise Occurrence Date'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Compromise Date'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Revocation Reason'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Archive Date'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Object Group'
        )
        self.assertEqual([], result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Fresh'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Link'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Application Specific Information'
        )
        self.assertEqual([], result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Contact Information'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'Last Change Date'
        )
        self.assertEqual(None, result)

        result = e._get_attribute_from_managed_object(
            symmetric_key,
            'invalid'
        )
        self.assertEqual(None, result)
