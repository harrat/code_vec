def test_get_small_parallel_enja_twice(self):
        get_small_parallel_enja()
        with mock.patch('lineflow.datasets.small_parallel_enja.pickle', autospec=True) as mock_pickle:
            get_small_parallel_enja()
        mock_pickle.dump.assert_not_called()
        mock_pickle.load.assert_called_once()
