@pytest.mark.parametrize('query, cols, rows', queries)
def test_query(capsys, query, cols, rows):
    context = new_context()
    context = shell.parse(context, input_file)
    context = shell.query(context, query)
    (out, err) = capsys.readouterr()
    lines = [line for line in out.split('\n')[1:] if not line == '']
    header = [col.strip() for col in lines[0].split('|')
              if not col.strip() == '']
    assert set(header) == set(cols.split(','))
    assert set(lines[1]) == {'|', '='}
    assert lines[-1].startswith('%d rows returned' % rows)

