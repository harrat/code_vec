def test_document_exists(self, connected_adapter):
        """Test document presents on Ceph."""
        assert connected_adapter.document_exists('foo') is False
        connected_adapter.store_document({'Hello': 'Thoth'}, 'foo')
        assert connected_adapter.document_exists('foo') is True
