def test_dump(interp):
    fname = "field1.npy"
    interp.do_cd(" Group2")
    interp.do_dump("field1")
    data = np.load(fname)
    assert np.allclose(data, np.zeros(10))
    os.remove(fname)

