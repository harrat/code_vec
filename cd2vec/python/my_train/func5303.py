def test_process_group_preamble(self):
        self.create_default_experiment(exp_preamble=['echo exp preamble'],
                                       group_preamble=['echo group preamble'])
        cluster = Cluster.new('tmux', server_name=_TEST_SERVER)

        l = cluster.get_log('exp', 'hello', process_group='group')
        self.assertIn('exp preamble', l)
        self.assertIn('group preamble', l)
        self.assertLess(l.index('exp preamble'), l.index('group preamble'))

        l = cluster.get_log('exp', 'alone')
        self.assertIn('exp preamble', l)
        self.assertNotIn('group preamble', l)
