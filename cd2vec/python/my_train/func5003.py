def test_get_metas_with_callback_store_db(self, dcard):
        forum = dcard.forums(_forum_name)

        def simulate_store_into_db(metas):
            some_ids = [9487] * len(metas)
            return some_ids

        result_ids = forum.get_metas(callback=simulate_store_into_db)
        assert len(result_ids) == 30
