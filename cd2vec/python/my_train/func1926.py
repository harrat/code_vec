def test_read():
	from io import StringIO, BytesIO


	header = ljson.base.generic.Header(header_descriptor)
	table = ljson.base.mem.Table(header, data)

	fio = StringIO()

	table.save(fio)
	fio.seek(0)

	table_in = ljson.base.disk.Table.from_file(fio)

	assert list(table_in) == data

	import pytest

	with pytest.raises(KeyError):
		assert table_in[{"foo": "bar"}]

	assert table_in[{"lname": "griffin"}]["name"] == [d["name"] for d in data]

	fio.seek(0)
	fio2 = BytesIO(fio.read().encode("utf-8"))
	table_in = ljson.base.disk.Table.from_file(fio2)

	assert list(table_in) == data

