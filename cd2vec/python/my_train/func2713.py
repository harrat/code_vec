def test_get_url_tlsv1():
    """
    Check that `get_url` can get data from a website using an older encryption
    protocol (TLSv1).
    """
    test_site = 'http://www.metal-archives.com'
    if not check_site_available(test_site):
        pytest.skip('Test site not available')

    soup = get_url(test_site, parser='html')
    assert soup
    assert hasattr(soup, 'html')
    assert hasattr(soup, 'head')
    assert hasattr(soup, 'body')
    assert soup.get_text() != ''

