def test_e2e_scenario_5(self):
        """
        Testing for free, offersIAP, inAppProductPrice of free app
        """

        res = app("com.nhn.android.search")

        self.assertTrue(res["free"])
        self.assertFalse(res["offersIAP"])
        self.assertFalse(res["inAppProductPrice"])
