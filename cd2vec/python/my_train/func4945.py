def test_observer_content_aware_subjective_model_missingdata(self):

        dataset = import_python_file(self.dataset_filepath)

        np.random.seed(0)
        info_dict = {
            'missing_probability': 0.1,
        }
        dataset_reader = MissingDataRawDatasetReader(dataset, input_dict=info_dict)

        subjective_model = MaximumLikelihoodEstimationModel(dataset_reader)
        result = subjective_model.run_modeling(force_subjbias_zeromean=False)

        self.assertAlmostEqual(float(np.sum(result['content_ambiguity'])), 3.9104244772977128, places=4)
        self.assertAlmostEqual(float(np.var(result['content_ambiguity'])), 0.0037713583509767193, places=4)

        self.assertAlmostEqual(float(np.sum(result['observer_bias'])), -0.21903272050455846, places=4)
        self.assertAlmostEqual(float(np.var(result['observer_bias'])), 0.084353684687185043, places=4)

        self.assertAlmostEqual(float(np.sum(result['observer_inconsistency'])), 9.8168943054654481, places=4)
        self.assertAlmostEqual(float(np.var(result['observer_inconsistency'])), 0.028159236075789944, places=4)

        self.assertAlmostEqual(float(np.sum(result['quality_scores'])), 280.05548186797336, places=4)
        self.assertAlmostEqual(float(np.var(result['quality_scores'])), 1.4339487982797514, places=4)

        np.random.seed(0)
        info_dict = {
            'missing_probability': 0.5,
        }
        dataset_reader = MissingDataRawDatasetReader(dataset, input_dict=info_dict)

        subjective_model = MaximumLikelihoodEstimationModel(dataset_reader)
        result = subjective_model.run_modeling(force_subjbias_zeromean=False)

        self.assertAlmostEqual(float(np.sum(result['content_ambiguity'])), 2.63184284168883, places=4)
        self.assertAlmostEqual(float(np.var(result['content_ambiguity'])), 0.019164097909450246, places=4)

        self.assertAlmostEqual(float(np.sum(result['observer_bias'])), 0.2263148440748638, places=4)
        self.assertAlmostEqual(float(np.var(result['observer_bias'])), 0.070613033112114504, places=4)

        self.assertAlmostEqual(float(np.sum(result['observer_inconsistency'])), 12.317917502439435, places=4)
        self.assertAlmostEqual(float(np.var(result['observer_inconsistency'])), 0.029455722248727296, places=4)

        self.assertAlmostEqual(float(np.sum(result['quality_scores'])), 280.29962156788139, places=4)
        self.assertAlmostEqual(float(np.var(result['quality_scores'])), 1.4717366222424826, places=4)
