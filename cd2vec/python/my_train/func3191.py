def test_extract_subjectivity(self):
        d = chazutsu.datasets.MovieReview.subjectivity()
        dataset_root, extracted = d.save_and_extract(DATA_ROOT)
        path = d.prepare(dataset_root, extracted)

        sub = 0
        obj = 0

        try:
            with open(path, encoding="utf-8") as f:
                for ln in f:
                    els = ln.strip().split("\t")
                    if len(els) != 2:
                        raise Exception("data file is not constructed by label and text.")
                    if els[0] == "1":
                        sub += 1
                    else:
                        obj += 1
        except Exception as ex:
            d.clear_trush()
            self.fail(ex)
        count = d.get_line_count(path)

        d.clear_trush()
        # sub=5000, obj=5000
        self.assertEqual(count, 5000*2)
        self.assertEqual(sub, 5000)
        self.assertEqual(obj, 5000)
