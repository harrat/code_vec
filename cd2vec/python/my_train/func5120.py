@pytest.mark.vcr()
def test_workbench_export(api):
    fobj = api.workbenches.export()
    assert isinstance(fobj, BytesIO)

@pytest.mark.vcr()
def test_workbench_export_plugin_id(api):
    fobj = api.workbenches.export(plugin_id=19506)
    assert isinstance(fobj, BytesIO)
