def test_register(self):
        self.response = self.mpesa_c2b_object.register(
            shortcode="601526",
            response_type="Completed",
            confirmation_url="https://novapi.herokuapp.com/v1/confirm",
            validation_url="https://novapi.herokuapp.com/v1/validate",
        )
        print(str(self.response))
        assert self.response.get("ResponseDescription", None) is not None
