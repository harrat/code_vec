def test_init_app(app):
    c = RegionCache()
    c.init_app(app)
    assert c.conn
    assert c.conn.ping()
    assert c._root
    assert c._root_name in c._regions
    assert c._regions[c._root_name] is c._root
    assert len(c._regions) == 1

