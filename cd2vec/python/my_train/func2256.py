def test_fetch_iter():
    global cu
    cu.execute("delete from tests")
    cu.execute("insert into tests(id, name) values (?, ?)", (12, 'test_fetch_iter'))
    cu.execute("insert into tests(id, name) values (?, ?)", (13, 'test_fetch_iter'))
    cu.execute("select id from tests order by id")
    lst = [row[0] for row in cu]
    assert lst[0] == 12
    assert lst[1] == 13

