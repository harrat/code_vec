def test_rfc3339_timezone_extension(self):
        self.formatter = rlog.RFC3339Formatter(
            fmt=('[%(asctime)s] %(levelname)s: %(check_name)s: '
                 'ct:%(check_job_completion_time)s: %(message)s'),
            datefmt='%FT%T%:z')
        self.handler.setFormatter(self.formatter)
        self.logger_with_check.info('foo')
        self.logger_without_check.info('foo')
        assert not self.found_in_logfile(r'%%:z')
        assert self.found_in_logfile(r'\[.+(\+|-)\d\d:\d\d\]')
        assert self.found_in_logfile(r'ct:.+(\+|-)\d\d:\d\d')
