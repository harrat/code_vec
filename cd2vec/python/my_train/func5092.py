def test_version_long_is_long(self):
        runner('git tag -a 1.0.0 -m "test message"')
        touch('testfile3')
        runner('git add testfile3')
        runner('git commit -m "add testfile"')
        v = relic.release.get_info()
        assert isinstance(v.long, str)
        assert '.' in v.long
        assert '-' in v.long
