@given(
        lists(helpers.cashAmounts(), min_size=1, max_size=20).flatmap(
            lambda ds: tradesForAmounts(amounts=ds, symbol="SPY").map(
                lambda ts: (ds, ts)
            )
        )
    )
    def test_realizedBasisAddsUp(
        self, args: Tuple[List[Decimal], Iterable[Trade]]
    ) -> None:
        amounts = args[0]
        trades = list(args[1])

        summed = sum(amounts)
        realizedBasis = realizedBasisForSymbol("SPY", trades)
        self.assertIsNotNone(realizedBasis)

        if realizedBasis:
            self.assertEqual(realizedBasis.quantity, -summed)
