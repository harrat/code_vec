def test_run_conda(d):
    PATH = pjoin(d, 'test-conda')
    os.mkdir(PATH)
    os.mkdir(pjoin(PATH, 'bin'))

    def test_exec(_file, _args):
        assert pjoin(PATH, 'bin') in os.environ['PATH'].split(':')
        assert pjoin(PATH, 'include') in os.environ['CPATH'].split(':')
        assert pjoin(PATH, 'lib') in os.environ['LD_LIBRARY_PATH'].split(':')
        assert pjoin(PATH, 'lib') in os.environ['LIBRARY_PATH'].split(':')
    kern = install(d, "conda %s"%PATH)
    run(d, kern, test_exec)

def test_run_venv(d):
    PATH = pjoin(d, 'test-venv')
    os.mkdir(PATH)
    os.mkdir(pjoin(PATH, 'bin'))
