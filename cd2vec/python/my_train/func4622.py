def test_utility(cache_obj):
    for _ in range(10):
        cache_obj.test1(8, 0)
        cache_obj.test1(8, 2)
        cache_obj.test1(8, 2)  # Already cached
        cache_obj.test2()
        cache_obj.test3(8, 2)

    assert len(c) == 4

    assert c.dump() != '{}'

    key = list(c.keys())[0]
    c.pop(key)
    assert len(c) == 3
    assert key not in c

    assert len(c.keys()) == 3
    assert len(c.values()) == 3

    assert c.items()

    c.clear()

    assert not c.items()
    assert not c.keys()
    assert not c.values()
    assert not len(c)
    assert c.dump() == '{}'

