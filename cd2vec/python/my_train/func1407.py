def test_installed_commit_hash(tmpdir):
    tmpdir = six.text_type(tmpdir)

    dvcs = generate_test_repo(tmpdir, [0], dvcs_type='git')
    commit_hash = dvcs.get_branch_hashes()[0]

    conf = config.Config()
    conf.env_dir = os.path.join(tmpdir, "env")
    conf.pythons = [PYTHON_VER1]
    conf.repo = os.path.abspath(dvcs.path)
    conf.matrix = {}
    conf.build_cache_size = 0

    repo = get_repo(conf)

    def get_env():
        return list(environment.get_environments(conf, None))[0]

    env = get_env()
    env.create()

    # Check updating installed_commit_hash
    assert env.installed_commit_hash == None
    assert env._global_env_vars.get('ASV_COMMIT') == None
    env.install_project(conf, repo, commit_hash)
    assert env.installed_commit_hash == commit_hash
    assert env._global_env_vars.get('ASV_COMMIT') == commit_hash

    env = get_env()
    assert env.installed_commit_hash == commit_hash
    assert env._global_env_vars.get('ASV_COMMIT') == commit_hash

    # Configuration change results to reinstall
    env._project = "something"
    assert env.installed_commit_hash == None

    # Uninstall resets hash (but not ASV_COMMIT)
    env = get_env()
    env._uninstall_project()
    assert env.installed_commit_hash == None
    assert env._global_env_vars.get('ASV_COMMIT') != None

    env = get_env()
    assert env.installed_commit_hash == None
    assert env._global_env_vars.get('ASV_COMMIT') == None

