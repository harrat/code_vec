def test_login_member(self):
        alias = utils.generate_alias(type=Alias.DOMAIN)
        member = self.client.create_member(alias)
        logged_in = self.client.get_member(member.member_id)

        expected_aliases = utils.repeated_composite_container_to_list(member.get_aliases())
        actual_aliases = utils.repeated_composite_container_to_list(logged_in.get_aliases())
        assert sorted(expected_aliases) == sorted(actual_aliases)
