def test_realign_list_outputs(create_files_in_directory):
    filelist, outdir = create_files_in_directory
    rlgn = spm.Realign(in_files=filelist[0])
    assert rlgn._list_outputs()["realignment_parameters"][0].startswith("rp_")
    assert rlgn._list_outputs()["realigned_files"][0].startswith("r")
    assert rlgn._list_outputs()["mean_image"].startswith("mean")

