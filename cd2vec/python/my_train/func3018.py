def test_get_does_does_not_modfiy_year_if_ok(fixture):
    text = license.get(fixture.author, fixture.year, fixture.kind)

    assert fixture.year in text

def test_get_gets_right_license(fixture):
    result = license.get(fixture.author, fixture.year, fixture.kind)
    assert result == fixture.expected
