@pytest.mark.parametrize('conditions,out,exception', [
		({Raw('f=1'):None}, 'f=1', None),
		({'and #': OrderedDict([
			(Raw('f=1'), None),
			(Raw('f=2'), None)
		])}, 'f=1 AND f=2', None),
		({'AND #': [
			Raw('f=1'),	Raw('f=2')
		]}, 'f=1 AND f=2', None),
		({'and #': (
			Raw('f=1'),	Raw('f=2')
		)}, 'f=1 AND f=2', None),
		({'or #': OrderedDict([
			(Raw('f=1'), None),
			(Raw('f=2'), None)
		])}, 'f=1 OR f=2', None),
		({'or #': [
			Raw('f=1'),	Raw('f=2')
		]}, 'f=1 OR f=2', None),
		({'OR #': (
			Raw('f=1'),	Raw('f=2')
		)}, 'f=1 OR f=2', None),
		({'f':1}, '"f" = 1', None),
		(OrderedDict([
			('or # or1', OrderedDict([
				('f3[~]', ('a', 'b', 'c')),
				('!f4[~]', ('a', 'b', 'c'))
			]))
		]), '("f3" LIKE \'%a%\' OR "f3" LIKE \'%b%\' OR "f3" LIKE \'%c%\') OR NOT ("f4" LIKE \'%a%\' OR "f4" LIKE \'%b%\' OR "f4" LIKE \'%c%\')', None),
		(OrderedDict([
			('t.f1[>]', 1),
			('t.f2[= any]', Builder()._select()),
			('or # or1', OrderedDict([
				('f3[~]', ('a', 'b', 'c')),
				('!f4[~]', ('a', 'b', 'c'))
			])),
			('and #', OrderedDict([
				(Raw('f=1'), None),
				(Raw('f=2'), None)
			]))
		]), '"t"."f1" > 1 AND "t"."f2" = ANY (SELECT *) AND (("f3" LIKE \'%a%\' OR "f3" LIKE \'%b%\' OR "f3" LIKE \'%c%\') OR NOT ("f4" LIKE \'%a%\' OR "f4" LIKE \'%b%\' OR "f4" LIKE \'%c%\')) AND (f=1 AND f=2)', None),
	])
	def testWhere(self, conditions, out, exception):
		if exception:
			with pytest.raises(exception):
				str(Where(conditions))
		else:
			assert str(Where(conditions)) == out
