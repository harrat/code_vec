def test_read_config(clean):
    # 'clean' is the DEFAULT_CONFIG_FILE yielded from fixture.
    shutil.copyfile('tests/data/config_1.ini', clean)
    from pdir.format import doc_color, category_color, attribute_color, comma

    assert doc_color == COLORS['white']
    assert category_color == COLORS['bright yellow']
    assert comma == '\033[1;32m, \033[0m'
    assert attribute_color == COLORS['cyan']

