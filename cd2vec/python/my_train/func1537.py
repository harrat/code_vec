def test_assets_endpoints():
    ## Testing getting one asset
    bitcoin = cryptowatch.assets.get("btc")
    # Test Asset object structure
    assert hasattr(bitcoin, "asset")
    assert hasattr(bitcoin.asset, "id")
    assert hasattr(bitcoin.asset, "fiat")
    assert hasattr(bitcoin.asset, "name")
    assert hasattr(bitcoin.asset, "symbol")
    assert hasattr(bitcoin.asset, "markets")
    assert type(bitcoin.asset.markets) == type(dict())
    assert bitcoin.asset.markets.get("base") is not None
    assert bitcoin.asset.markets.get("quote") is not None
    assert type(bitcoin.asset.markets.get("base")) == type(list())
    # test bitcoin Asset values
    assert bitcoin.asset.id == 60
    assert bitcoin.asset.name == "Bitcoin"
    assert bitcoin.asset.symbol == "btc"
    assert bitcoin.asset.fiat == False
    # Testing listing all assets
    assets = cryptowatch.assets.list()
    assert hasattr(assets, "assets")
    assert type(assets.assets) == type(list())
    assert type(assets.assets[0]) == cryptowatch.resources.assets.AssetResource
    # This should raise an APIResourceNotFoundError Exception
    with pytest.raises(cryptowatch.errors.APIResourceNotFoundError):
        cryptowatch.assets.get("shitcointhatdoesntexists")
