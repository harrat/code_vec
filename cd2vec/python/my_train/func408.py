def test_stransfer(self):
    """
    Ensures the stransfer function is working properly.
    """
    tt = T.ToTensor()
    imgshape = tt(imgutils.load_img(TestOneCalls.url_1)).shape
    simgshape = tt(one_call.stransfer(TestOneCalls.url_1, show=False)).shape
    self.assertTrue(imgshape == simgshape, 'doesn\'t look good')
