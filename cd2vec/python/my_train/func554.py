def test_http_context_singleton_clear(self):
        ctxt_singleton = HttpRequestContextSingleton.get_instance()
        ctxt_singleton.method = "POST"
        self.assertEqual("POST", ctxt_singleton.method)
        ctxt_singleton.clear()
        self.assertNotEqual("POST", ctxt_singleton.method)

        ctxt_singleton2 = HttpRequestContextSingleton.get_instance()
        self.assertNotEqual("POST", ctxt_singleton2.method)
