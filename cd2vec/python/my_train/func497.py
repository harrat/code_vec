def test_read_OV_explicit_little(self):
        """Check reading element with VR of OV encoded as explicit"""
        ds = dcmread(self.fp_ex, force=True)
        val = (
            b"\x00\x00\x00\x00\x00\x00\x00\x00"
            b"\x01\x02\x03\x04\x05\x06\x07\x08"
        )
        elem = ds["ExtendedOffsetTable"]
        assert "OV" == elem.VR
        assert 0x7FE00001 == elem.tag
        assert val == elem.value

        new = DataElement(0x7FE00001, "OV", val)
        assert elem == new
