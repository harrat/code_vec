def test_args(mock_dng, tmp_path):
    dng = pydng.DNGConverter(tmp_path,
                             jpeg_preview=pydng.JPEGPreview.FULL,
                             compressed=pydng.Compression.YES,
                             camera_raw=pydng.CRawCompat.ELEVEN_TWO,
                             dng_version=pydng.DNGVersion.ONE_FOUR,
                             fast_load=True, linear=True)
    expect_args = [mock_dng, "-c", "-cr11.2",
                   "-dng1.4", "-fl", "-l", "-p2"]
    assert sorted(dng.args) == sorted(expect_args)

