@pytest.mark.remote
def test_fetch_event_json_version():
    out = api.fetch_event_json("GW150914-v3")["events"]["GW150914-v3"]
    assert out["version"] == 3
    assert out["catalog.shortName"] == "GWTC-1-confident"

