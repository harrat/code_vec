@skipIf(bz_path is None, "'bzip2' not available")
    def test_system_bzip(self):
        self.write_read_file('.bz2', True)
