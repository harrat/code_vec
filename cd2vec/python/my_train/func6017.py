def test_deploy_by_weekday(self):
        """
        deploy_by_weekday
        """
        cronpi.run_every_week("ls", isOverwrite=True).on("saturday", time="7:30")
        self.assertEqual(get_job_list()[0], "30 7 * * 6 ls")

        cronpi.run_every_week("ls", isOverwrite=True).on("saturday", time="7:30am")
        self.assertEqual(get_job_list()[0], "30 7 * * 6 ls")

        cronpi.run_every_week("ls", isOverwrite=True).on(["sat", "friday"], time="7:30pm")
        self.assertEqual(get_job_list()[0], "30 19 * * 6,5 ls")

        cronpi.run_every_week("ls", isOverwrite=True).on(["sat", "friday"], time="17:30")
        self.assertEqual(get_job_list()[0], "30 17 * * 6,5 ls")
