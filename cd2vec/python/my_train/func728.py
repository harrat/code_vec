def test_mp3_properties(self):
        properties = self.load("res/audioformats/p001.mp3")
        self.assertIsNotNone(properties["bit_rate"])
        self.assertIsNotNone(properties["channels"])
        self.assertIsNotNone(properties["codec_name"])
        self.assertIsNotNone(properties["duration"])
        self.assertIsNotNone(properties["sample_rate"])
