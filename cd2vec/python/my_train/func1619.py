def test_checks_noqa():
    """Test checks on a gettext file ignoring `noqa`-commented lines."""
    po_check = PoCheck()
    po_check.set_check('skip_noqa', True)
    result = po_check.check_files([local_path('fr_errors.po')])

    # be sure we have one file in result
    assert len(result) == 1

    # the file has 9 errors (`noqa` was skipped)
    assert len(result[0][1]) == 9

