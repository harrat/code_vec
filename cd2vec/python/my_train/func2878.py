@patch(
    'confluent_kafka.avro.CachedSchemaRegistryClient.register',
    MagicMock(return_value=1)
)
@patch(
    'confluent_kafka.avro.CachedSchemaRegistryClient.get_latest_schema',
    MagicMock(return_value=(1, AvroStringKeySerializer.KEY_SCHEMA, 1))
)
@patch(
    'confluent_kafka.avro.CachedSchemaRegistryClient.get_by_id',
    MagicMock(return_value=AvroStringKeySerializer.KEY_SCHEMA)
)
def test_avro_producer_produce(avro_producer):
    key = 'a'
    value = 'a'
    topic = 'c'
    avro_producer.produce(topic, key=key, value=value)

    producer_produce_mock.assert_called_once_with(
        topic,
        b'\x00\x00\x00\x00\x01\x02a',
        value
    )
