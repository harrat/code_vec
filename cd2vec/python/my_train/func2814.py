def test_postscript_only(self):
        data = {
            'post_script': [
                "echo " + self.first_out,
                "echo " + self.second_out,
            ],
            'pre_script': [],
            'remote': 'origin',
            'branch': 'master'
        }

        with open(self.filename, 'w') as f:
            json.dump(data, f)

        chain = CommandChain.load_from_config(self.filename)
        chain.commands[1].execute()
        chain.commands[2].execute()
        self.assertEqual(len(chain.commands), 3)
        self.assertEqual(chain.commands[0].cmd, 'git pull origin master')
        self.assertIn(self.first_out, chain.commands[1].out)
        self.assertIn(self.second_out, chain.commands[2].out)
