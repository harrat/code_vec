def test_decompress_file(self):
        b = (True, False) if gz_path else (False,)
        for use_system in b:
            with self.subTest(use_system=use_system):
                path = self.root.make_file()
                gzfile = Path(str(path) + '.gz')
                with gzip.open(gzfile, 'wt') as o:
                    o.write('foo')
                with open(gzfile, 'rb') as i:
                    fmt = get_format('.gz')
                    dest = fmt.decompress_file(i, use_system=use_system)
                assert Path(dest) == path
                self.assertTrue(os.path.exists(path))
                self.assertTrue(os.path.exists(gzfile))
                with open(path, 'rt') as i:
                    assert i.read() == 'foo'
