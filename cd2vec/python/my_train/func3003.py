def test_truncated_matches_full(bank):
    for filt_idx in range(bank.num_filts):
        left_hz, right_hz = bank.supports_hz[filt_idx]
        left_samp, right_samp = bank.supports[filt_idx]
        dft_size = int(max(
            (right_samp - left_samp) * (1 + np.random.random()),
            2 * bank.sampling_rate / (right_hz - left_hz),
            1,
        ))
        left_period = int(np.floor(left_hz / bank.sampling_rate))
        right_period = int(np.ceil(right_hz / bank.sampling_rate))
        full_response = bank.get_frequency_response(filt_idx, dft_size)
        bin_idx, truncated = bank.get_truncated_response(filt_idx, dft_size)
        challenge = np.zeros(dft_size, dtype=truncated.dtype)
        wrap = min(bin_idx + len(truncated), dft_size) - bin_idx
        challenge[bin_idx:bin_idx + wrap] = truncated[:wrap]
        challenge[:len(truncated) - wrap] = truncated[wrap:]
        if bank.is_real:
            challenge[
                len(challenge) - bin_idx - len(truncated) + 1:
                len(challenge) - bin_idx + 1
            ] = truncated[:None if bin_idx else 0:-1].conj()
        bad_idx = np.where(np.logical_not(np.isclose(
            full_response, challenge, atol=EFFECTIVE_SUPPORT_THRESHOLD)))
        assert np.allclose(
            full_response, challenge,
            atol=(right_period - left_period) * EFFECTIVE_SUPPORT_THRESHOLD
        ), 'idx: {} threshold:{} full:{} challenge:{}'.format(
            filt_idx, EFFECTIVE_SUPPORT_THRESHOLD,
            full_response[bad_idx], challenge[bad_idx]
        )
