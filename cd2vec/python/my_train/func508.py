def test_cancel_term_ignore(minimal_job, scheduler, local_only):
    # This test emulates a descendant process of the spawned job that
    # ignores the SIGTERM signal:
    #
    #   reframe --- local job script --- sleep_deeply.sh --- sleep
    #                                      (TERM IGN)
    #
    #  Since the "local job script" does not ignore SIGTERM, it will be
    #  terminated immediately after we cancel the job. However, the deeply
    #  spawned sleep will ignore it. We need to make sure that our
    #  implementation grants the sleep process a grace period and then
    #  kills it.
    minimal_job.time_limit = '1m'
    minimal_job.scheduler._cancel_grace_period = 2
    prepare_job(minimal_job,
                command=os.path.join(fixtures.TEST_RESOURCES_CHECKS,
                                     'src', 'sleep_deeply.sh'),
                pre_run=[''],
                post_run=[''])
    minimal_job.submit()

    # Stall a bit here to let the the spawned process start and install its
    # signal handler for SIGTERM
    time.sleep(1)

    t_grace = datetime.now()
    minimal_job.cancel()
    t_grace = datetime.now() - t_grace
    minimal_job.wait()

    # Read pid of spawned sleep
    with open(minimal_job.stdout) as fp:
        sleep_pid = int(fp.read())

    assert t_grace.total_seconds() >= 2
    assert minimal_job.state == 'TIMEOUT'

    # Verify that the spawned sleep is killed, too
    assert_process_died(sleep_pid)

