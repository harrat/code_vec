def test_get_free_port():
    port = get_free_port()
    assert isinstance(port, int)
    assert 65536 > port > 0

