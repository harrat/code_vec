@mock.patch("IPython.display.display")
def test_scraps_report_with_scrap_and_notebook_names(mock_display, notebook_collection):
    notebook_collection.scraps_report(scrap_names=["output"], notebook_names=["result1"])
    mock_display.assert_has_calls(
        [
            mock.call(AnyMarkdownWith("### result1")),
            mock.call(AnyMarkdownWith("#### output")),
            mock.call({"text/plain": "'Hello World!'"}, metadata={}, raw=True),
        ]
    )
