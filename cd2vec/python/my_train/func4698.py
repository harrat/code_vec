def test_serverx_async_handler(tcp_client):
    """
    Test serverx object for reading message form client and handle it with coroutine.

    :param tcp_client: function for send message to server
    """
    host, port, client = tcp_client

    async def handler(message, reader, writer):
        logger.info(f"got message: `{message}`")
        logger.debug(f"got reader: `{reader}` and `{writer}`")
        await asyncio.sleep(0.01)
        writer.write(b"ok")

    run_test_serverx(host, port, client, handler)

