def test_redis_subscribers_empty_topics(event_loop, redis_instance):
    import logging

    logger = logging.getLogger("aiotasks")

    class CustomLogger(logging.StreamHandler):
        def __init__(self):
            super(CustomLogger, self).__init__()
            self.content = []

        def emit(self, record):
            self.content.append(record.msg)

    custom = CustomLogger()
    logger.addHandler(custom)

    manager = build_manager(dsn=redis_instance, loop=event_loop)

    @manager.subscribe()
    async def task_test_redis_subscribers_oks(topic, data):
        if topic == "hello" and data == "world":
            globals()["test_redis_subscribers_oks_finished"] = True

    async def run():
        manager.run()

        await manager.publish("hello", "world")

        await manager.wait(timeout=0.2, exit_on_finish=True, wait_timeout=0.2)

    event_loop.run_until_complete(run())
    manager.stop()

    assert len(manager.topics_subscribers) == 0
    assert "Empty topic fount in function 'task_test_redis_subscribers_oks'. Skipping it." in custom.content

