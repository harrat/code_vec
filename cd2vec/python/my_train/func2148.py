def test_list_iter_mapping(self):
        # arrange
        self.stub_request('get', 'events', {'data': [{'resource': 'event'} for _ in range(20)]})
        # act
        for event in self.client.event.list_paging_iter():
            # assert
            self.assertIsInstance(event, Event)
