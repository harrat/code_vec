@staticmethod
    def test_cli_create_snippet_003(snippy):
        """Try to create snippet from CLI.

        Try to create new snippet without defining mandatory content data.
        """

        content = {
            'data': [
                Snippet.REMOVE
            ]
        }
        brief = content['data'][0]['brief']
        groups = content['data'][0]['groups']
        tags = content['data'][0]['tags']
        links = content['data'][0]['links']
        cause = snippy.run(['snippy', 'create', '--brief', brief, '--groups', groups, '--tags', tags, '--links', links, '--no-editor'])
        assert cause == 'NOK: content was not stored because mandatory content field data is empty'
        Content.assert_storage(None)
