@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_update_snippet_002(snippy, edited_remove):
        """Update snippet with ``--digest`` option.

        Update snippet based on very short message digest. This must match to
        a single snippet that must be updated.
        """

        content = {
            'data': [
                Content.deepcopy(Snippet.REMOVE),
                Snippet.FORCED
            ]
        }
        content['data'][0]['data'] = ('docker images', )
        content['data'][0]['digest'] = 'af8c89629dc1a5313fd15c95fa9c1199b2b99874426e0b2532a952f40dcf980d'
        edited_remove.return_value = Content.dump_text(content['data'][0])
        cause = snippy.run(['snippy', 'update', '-d', '54e41', '--format', 'text'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
