def test_list_iter_mapping(self):
        # arrange
        self.stub_request('get', 'charges', {'data': [{'resource': 'charge'} for _ in range(20)]})
        # act
        for charge in self.client.charge.list_paging_iter():
            # assert
            self.assertIsInstance(charge, Charge)
