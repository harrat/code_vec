def test_getnodefieldname():
    """py.test for getnodefieldname"""
    tdata = (
        ("PIPE:ADIABATIC", "pipe1", "Inlet_Node_Name", "", "Inlet_Node_Name"),
        # objtype, objname, endswith, fluid, nodefieldname
        (
            "CHILLER:ELECTRIC",
            "pipe1",
            "Inlet_Node_Name",
            "",
            "Chilled_Water_Inlet_Node_Name",
        ),
        # objtype, objname, endswith, fluid, nodefieldname
        (
            "COIL:COOLING:WATER",
            "pipe1",
            "Inlet_Node_Name",
            "Water",
            "Water_Inlet_Node_Name",
        ),
        # objtype, objname, endswith, fluid, nodefieldname
        (
            "COIL:COOLING:WATER",
            "pipe1",
            "Inlet_Node_Name",
            "Air",
            "Air_Inlet_Node_Name",
        ),
        # objtype, objname, endswith, fluid, nodefieldname
        (
            "COIL:COOLING:WATER",
            "pipe1",
            "Outlet_Node_Name",
            "Air",
            "Air_Outlet_Node_Name",
        ),
        # objtype, objname, endswith, fluid, nodefieldname
    )
    for objtype, objname, endswith, fluid, nodefieldname in tdata:
        fhandle = StringIO("")
        idf = IDF(fhandle)
        idfobject = idf.newidfobject(objtype, Name=objname)
        result = hvacbuilder.getnodefieldname(idfobject, endswith, fluid)
        assert result == nodefieldname

