def test_decompress_file(self):
        path = self.root.make_file()
        gzfile = Path(str(path) + '.gz')
        with gzip.open(gzfile, 'wt') as o:
            o.write('foo')

        path2 = decompress_file(gzfile, keep=True)
        assert path == path2
        assert path.exists()
        assert gzfile.exists()
        with open(path, 'rt') as i:
            assert i.read() == 'foo'

        with open(gzfile, 'rb') as i:
            path2 = decompress_file(i, keep=True)
            assert path == path2
            assert path.exists()
            assert gzfile.exists()
            with open(path, 'rt') as j:
                assert j.read() == 'foo'
