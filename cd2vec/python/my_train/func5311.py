def test_file_creation2(self):
        path = os.path.realpath('./test.pd')

        with PD(path=path) as pd:
            print('pd[test]: ', pd['test'])
            self.assertTrue(pd['test'] == 123)

        os.remove(path)
