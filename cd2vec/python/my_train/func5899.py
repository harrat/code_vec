def test_delete_resource_html(self):
        """Test DELETEing a resource via HTML."""
        response = self.app.delete('/artists/239',
                headers={'Accept': 'text/html'})
        assert response.status_code == 204
        assert response.headers['Content-type'].startswith('text/html')
        response = self.get_response('/artists/239',
                404,
                False,
                headers={'Accept': 'text/html'})
        assert response.headers['Content-type'].startswith('text/html')
