def test_compilation():
    """Test compilation of gettext files."""
    # valid file
    assert PoFile(local_path('fr.po')).compile()[1] == 0

    # invalid file
    assert PoFile(local_path('fr_compile.po')).compile()[1] == 1

