def test_models_binary():
    """Assert that the fit method works with all models for binary."""
    for model in [m for m in MODEL_NAMES if m not in ONLY_REGRESSION]:
        atom = ATOMClassifier(X_bin, y_bin, test_size=0.24, random_state=1)
        atom.run(models=model,
                 metric='auc',
                 n_calls=2,
                 n_random_starts=1,
                 bo_params={'base_estimator': 'rf', 'cv': 1})
        assert not atom.errors  # Assert that the model ran without errors
        assert hasattr(atom, model) and hasattr(atom, model.lower())

