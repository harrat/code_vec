@mock.patch('groc.cli.Groc._get_db_url')
@mock.patch('groc.cli.Groc._get_connection')
def test_add_dir_with_duplicate_purchase(groc_connection, groc_db_url,
                                         connection_function_scope,
                                         create_purchase_csvs,
                                         add_csv_file_with_duplicate,
                                         purchase_csv_dir):

    groc_connection.return_value = connection_function_scope
    _ = Groc()
    dir_path = purchase_csv_dir

    files = purchase_csv_dir.listdir()

    output = (
        f'Importing data from {files[0]}\n'
        f'2 purchase(s) added\n'
        f'Importing data from {files[1]}\n'
        f'2 purchase(s) added\n'
        f'Importing data from {files[2]}'
    )
    exc_str = 'Duplicate purchase detected -- '\
              '(date: 2019-01-03, store: Store Bar, ' \
              'total: 25.00, description: bars)'

    runner = CliRunner()
    result = runner.invoke(
        groc_cli, ['add', '--source', dir_path]
    )
    assert result.exit_code == 1
    assert result.output == f'{output}\n'
    assert result.exception.__str__() == exc_str

