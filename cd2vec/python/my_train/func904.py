def test_should_load_a_missing_rc_when_not_in_strict_mode(self):
        directory = os.path.join(DATA_DIR, 'temp', 'missing')
        if os.path.isdir(directory):
            os.rmdir(directory)
        os.mkdir(directory)
        rc = factory.load(directory, False)
        assert rc is not None
