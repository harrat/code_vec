@pytest.mark.parametrize(
    "name,scrap,encoder,data,metadata",
    [
        (
            "foobarbaz",
            {"foo": "bar", "baz": 1},
            None,
            {
                GLUE_PAYLOAD_FMT.format(encoder="json"): {
                    "name": "foobarbaz",
                    "data": {"foo": "bar", "baz": 1},
                    "encoder": "json",
                    "version": 1,
                }
            },
            {"scrapbook": {"name": "foobarbaz", "data": True, "display": False}},
        ),
        (
            "foobarbaz",
            '{"foo":"bar","baz":1}',
            None,
            {
                GLUE_PAYLOAD_FMT.format(encoder="text"): {
                    "name": "foobarbaz",
                    "data": '{"foo":"bar","baz":1}',
                    "encoder": "text",
                    "version": 1,
                }
            },
            {"scrapbook": {"name": "foobarbaz", "data": True, "display": False}},
        ),
        (
            "foobarbaz",
            '{"foo":"bar","baz":1}',
            "json",
            {
                GLUE_PAYLOAD_FMT.format(encoder="json"): {
                    "name": "foobarbaz",
                    "data": {"foo": "bar", "baz": 1},
                    "encoder": "json",
                    "version": 1,
                }
            },
            {"scrapbook": {"name": "foobarbaz", "data": True, "display": False}},
        ),
        (
            "foobarbaz",
            # Pick something we don't match normally
            collections.OrderedDict({"foo": "bar", "baz": 1}),
            "json",
            {
                GLUE_PAYLOAD_FMT.format(encoder="json"): {
                    "name": "foobarbaz",
                    "data": {"foo": "bar", "baz": 1},
                    "encoder": "json",
                    "version": 1,
                }
            },
            {"scrapbook": {"name": "foobarbaz", "data": True, "display": False}},
        ),
    ],
)
@mock.patch("IPython.display.display")
def test_glue(mock_display, name, scrap, encoder, data, metadata):
    glue(name, scrap, encoder)
    mock_display.assert_called_once_with(data, metadata=metadata, raw=True)

