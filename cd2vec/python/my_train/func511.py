@cli_args(
    [TEST_STRING]
)
def test_output(capsys, argv):
    app = BasicApp()
    app.run()
    stdout, _ = capsys.readouterr()
    assert stdout == TEST_STRING+"\n"

