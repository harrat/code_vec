def test_create_and_cancel_order(self):
        order = ac.order(pair="SWTH_NEO", side="buy",
                         price=0.00001, quantity=10000, private_key=kp,
                         use_native_token=True, order_type="limit")
        ac.cancel_order(order_id=order['id'], private_key=kp)
        testnet_scripthash = 'fea2b883725ef2d194c9060f606cd0a0468a2c59'
        cancelled = False
        for trade in ac.get_orders(address=testnet_scripthash):
            if trade['id'] == order['id'] and\
                    trade['status'] == 'processed' and\
                    trade['makes'][0]['status'] in ['cancelled', 'cancelling']:
                cancelled = True
        self.assertTrue(cancelled)
