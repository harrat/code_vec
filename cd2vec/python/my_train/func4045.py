def test_decrypt_invalid_decryption_key(self):
        """
        Test that the right error is thrown when an invalid decryption key
        is specified with a Decrypt request.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        decryption_key = pie_objects.OpaqueObject(
            b'\x01\x02\x03\x04',
            enums.OpaqueDataType.NONE
        )

        e._data_session.add(decryption_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        unique_identifier = str(decryption_key.unique_identifier)
        cryptographic_parameters = attributes.CryptographicParameters(
            block_cipher_mode=enums.BlockCipherMode.CBC,
            padding_method=enums.PaddingMethod.PKCS5,
            cryptographic_algorithm=enums.CryptographicAlgorithm.BLOWFISH
        )
        data = (
            b'\x37\x36\x35\x34\x33\x32\x31\x20'
            b'\x4E\x6F\x77\x20\x69\x73\x20\x74'
            b'\x68\x65\x20\x74\x69\x6D\x65\x20'
            b'\x66\x6F\x72\x20\x00'
        )
        iv_counter_nonce = b'\xFE\xDC\xBA\x98\x76\x54\x32\x10'

        payload = payloads.DecryptRequestPayload(
            unique_identifier,
            cryptographic_parameters,
            data,
            iv_counter_nonce
        )

        args = (payload, )
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            "The requested decryption key is not a symmetric key. "
            "Only symmetric decryption is currently supported.",
            e._process_decrypt,
            *args
        )
