def test_default_access(self):
        random_data = generate_nonce(50).encode()
        attachment = self.member.create_blob(self.member.member_id, self.file_type, self.file_name, random_data, Blob.DEFAULT)
        with pytest.raises(RequestError) as exec_info:
            self.other_member.get_blob(attachment.blob_id)
        assert exec_info.value.details == "BlobId: {}".format(attachment.blob_id)
        assert exec_info.value.code == StatusCode.NOT_FOUND
