def test_get_combined_credits(empty_person: Person):
    credits = empty_person.get_combined_credits()
    assert credits == empty_person.data["combined_credits"]

