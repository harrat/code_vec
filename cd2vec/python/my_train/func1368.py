@responses.activate
    def test_command_list(self, centreon_con):
        with open(resource_dir / 'test_commands_list.json') as data:
            wsresponses = json.load(data)
        responses.add(responses.POST,
                      'http://api.domain.tld/centreon/api/index.php?action=action&object=centreon_clapi',
                      json=wsresponses, status=200, content_type='application/json')

        _, res = centreon_con.commands.get('OS-Linux-SNMP-Memory')
        assert res.id == "111"
