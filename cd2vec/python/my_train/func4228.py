def test_load_file_creates_parent_dir_if_it_doesnt_exist(
        self, monkeypatch, http_folder
    ):
        mkdir_mock = MagicMock()
        with monkeypatch.context() as ctx:
            ctx.setattr(Path, "mkdir", mkdir_mock)
            http_folder.load_file(file_name)
        mkdir_mock.assert_called_once_with(exist_ok=True, parents=True)
