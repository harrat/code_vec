@pytest.mark.vcr()
def test_agents_list_sort_field_typeerror(api):
    with pytest.raises(TypeError):
        api.agents.list(sort=((1, 'asc'),))
