def test_met_request_should_handle_connection_problem(capsys):
    with requests_mock.Mocker() as m:
        m.get('https://www.met.ie/api/weather/national',
              exc=requests.exceptions.ConnectionError)
