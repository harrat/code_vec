@staticmethod
    @pytest.mark.usefixtures('import-nginx', 'import-nginx-utc')
    def test_cli_import_solution_019(snippy):
        """Import solution based on message digest.

        Try to import defined solution with message digest that cannot be
        found. In this case there is one solution already stored.
        """

        content = {
            'data': [
                Solution.NGINX
            ]
        }
        updates = {
            'data': [
                Content.deepcopy(Solution.NGINX)
            ]
        }
        updates['data'][0]['data'] = content['data'][0]['data'][:4] + ('    # Changed.',) + content['data'][0]['data'][5:]
        updates['data'][0]['description'] = 'Changed.'
        updates['data'][0]['updated'] = Content.KAFKA_TIME
        updates['data'][0]['digest'] = 'ce3f7a0ab75dc74f7bbea68ae323c29b2361965975c0c8d34897551149d29118'
        file_content = Content.get_file_content(Content.TEXT, updates)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '-d', '123456789abcdef0', '-f', 'one-solution.text'])
            assert cause == 'NOK: cannot find content with message digest: 123456789abcdef0'
            Content.assert_storage(content)
            mock_file.assert_not_called()
