def test_binary_file_content_restored_on_delete(self):

        binary_frozen = os.path.join(self.FROZEN_RESOURCES_PATH, self.EXECUTABLE_BINARY_NAME)
        binary_path = os.path.join(self.RESOURCES_PATH, self.EXECUTABLE_BINARY_NAME)

        self._assert_files_equal(binary_path, binary_frozen)

        @guard(binary_path)
        def function_that_removes_the_file():
            os.remove(binary_path)
            p = Path(binary_path)
            self.assertFalse(p.is_file(), f'File was not removed')

        function_that_removes_the_file()

        self._assert_files_equal(binary_path, binary_frozen)
