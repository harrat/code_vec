def test_synchronization_coordinator_with_multiobject_exception():
    mo = MultiObject(range(3))

    executed = []

    class MyException(Exception):
        pass

    def foo(i, _sync=SYNC):
        def execute(caption):
            executed.append((i, caption))

        _sync.wait_for_everyone()
        execute('after wait')

        if i == 2:
            raise MyException

        _sync.wait_for_everyone()
        execute('after wait/abandon')

    with pytest.raises(MultiException) as exc:
        mo.call(foo)
    assert exc.value.count == 1
    assert exc.value.common_type is MyException

    verify_concurrent_order(
        executed,
        {(i, 'after wait') for i in range(3)},
        {(i, 'after wait/abandon') for i in range(2)})
