def test_write_not_existing_path(self):
        output_file_path = gf.absolute_path(self.NOT_EXISTING_FILE, __file__)
        audiofile = self.load(self.AUDIO_FILE_WAVE, rs=True)
        with self.assertRaises(OSError):
            audiofile.write(output_file_path)
