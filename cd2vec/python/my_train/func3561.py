@pytest.mark.filterwarnings("ignore:PkgResourcesDeprecationWarning")
def test_ppo_save() -> None:
    ppo = PPOAgent(config())
    ppo.optimizer.param_groups[0]["lr"] = 1.0
    ppo.clip_eps = 0.2
    ppo.save("ppo-agent.pth")
    ppo.close()
    ppo = PPOAgent(config())
    path = ppo.config.logger.logdir.joinpath("ppo-agent.pth")
    ppo.load(path.as_posix())
    assert ppo.clip_eps == 0.2
    assert ppo.optimizer.param_groups[0]["lr"] == 1.0
    ppo.close()

