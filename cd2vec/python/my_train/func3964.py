@autodoc.describe('POST /foo/bar')
    def test_foo_bar(self):
        """ POST /foo/bar """
        params = {'id': 1, 'message': 'foo'}
        req = self.create_request('http://localhost:5000/foo/bar', 'POST',
                                  params)
        res = self.send(req, params, '../tests/data/post.json')
        self.assertEqual(res.status_code, 200)

        return res
