@pytest.mark.parametrize("not_exporters", ["htm", "ipython", "markup"])
def test_raises(not_exporters, tmp_path: Path) -> None:
    """Make sure a ``ValueError`` is raised if ``nbconv`` gets a bad exporter."""
    nb = make_notebook(tmp_path)
    with pytest.raises(ValueError):
        nbconv(in_file=nb, exporter=not_exporters)
        nbconv(in_file=nb, out_file="out." + not_exporters)
