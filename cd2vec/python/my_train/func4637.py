def test_hidden_state_not_changed_after_evaluation():
    trained_model = load_default_model()
    trained_model.feed_text('Some context which after evaluation has to be restored', extension='java')
    before = take_hidden_state_snapshot(trained_model.model)
    evaluate_on_string(trained_model, 'import')
    after = take_hidden_state_snapshot(trained_model.model)

    for aa,bb in zip(after, before):
        for a, b in zip(aa, bb):
            assert torch.equal(b, a)

