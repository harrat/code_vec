@pytest.mark.vcr()
def test_access_groups_list_limit_typeerror(api):
    with pytest.raises(TypeError):
        api.access_groups.list(limit='nope')
