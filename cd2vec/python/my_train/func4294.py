@patch('stackify.transport.default.DefaultTransport.create_message')
    @patch('stackify.transport.default.DefaultTransport.create_group_message')
    @patch('stackify.transport.default.http.HTTPClient.send_log_group')
    def test_send_group_crash(self, send_log_group, logmsggroup, logmsg):
        '''The listener drops messages after retrying'''
        listener = StackifyListener(queue_=Mock(), max_batch=3, config=self.config)
        listener.transport._transport.identified = True

        send_log_group.side_effect = Exception

        listener.handle(1)
        listener.handle(2)
        listener.handle(3)
        self.assertEqual(len(listener.messages), 0)
        listener.handle(4)
        self.assertEqual(len(listener.messages), 1)
        self.assertEqual(send_log_group.call_count, 1)
