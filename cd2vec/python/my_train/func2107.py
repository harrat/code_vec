def test_1_get_lock_info_none(self):

        info: dict = get_lock_info(TEST_FILE_PATH)

        assert info['user'] is None
        assert info[s.LOCK_PART_SIZE_BYTES_FIELD] is None
        assert info[s.LOCK_TOTAL_SIZE_BYTES_FIELD] is None
