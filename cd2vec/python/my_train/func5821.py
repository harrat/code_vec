def test_repository_active_branch_name_color_is_mute():
    with tempfile.TemporaryDirectory() as tmp_repo:
        empty_repo = git.Repo.init(tmp_repo)
        segment = repository.ActiveBranch(tmp_repo).segment()
