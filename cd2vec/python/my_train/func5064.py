def test_invalid_config_2(clean):
    shutil.copyfile('tests/data/error_config_2.ini', clean)
    with pytest.raises(ValueError, match='Invalid color value: 42'):
        import pdir
