def test_compute_accessibilities():
    for G, distances, betas in network_generator():
        data_dict = mock.mock_data_dict(G)
        landuse_labels = mock.mock_categorical_data(len(data_dict))
        # easy version
        N_easy = networks.Network_Layer_From_nX(G, distances)
        D_easy = layers.Data_Layer_From_Dict(data_dict)
        D_easy.assign_to_network(N_easy, max_dist=500)
        D_easy.compute_accessibilities(landuse_labels, ['c'])
        # custom version
        N_full = networks.Network_Layer_From_nX(G, distances)
        D_full = layers.Data_Layer_From_Dict(data_dict)
        D_full.assign_to_network(N_full, max_dist=500)
        D_full.compute_aggregated(landuse_labels, accessibility_keys=['c'])
        # compare
        for d in distances:
            for wt in ['weighted', 'non_weighted']:
                assert np.allclose(N_easy.metrics['accessibility'][wt]['c'][d],
                                   N_full.metrics['accessibility'][wt]['c'][d], atol=0.001, rtol=0)
