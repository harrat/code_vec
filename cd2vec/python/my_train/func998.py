@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_nfl_qb_returns_requested_player_season_stats(self,
                                                          *args,
                                                          **kwargs):
        # Request the 2017 stats
        player = Player('BreeDr00')
        player = player('2017')

        for attribute, value in self.qb_results_2017.items():
            assert getattr(player, attribute) == value
