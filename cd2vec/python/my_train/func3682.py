def test_pad(self):
        pad = Pad.parse(
            '(pad 1 smd rect (at 0.1 0.1) (size 0.2 0.2) (layers F.Cu))')
        assert pad.name == '1'
        assert pad.type == 'smd'
        assert pad.shape == 'rect'
        assert pad.at == [0.1, 0.1]
        assert pad.size == [0.2, 0.2]
        assert pad.layers == ['F.Cu']
        assert Pad.parse(pad.to_string()) == pad
