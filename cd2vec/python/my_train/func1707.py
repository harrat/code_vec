@given(strategies.data, strategies.dialects,
       strategies.metadatas,
       strategies.min_sizes, strategies.max_sizes)
def test_examples(data: DataObject,
                  dialect: Dialect,
                  metadata: MetaData,
                  min_size: int,
                  max_size: Optional[int]) -> None:
    strategy = factory(dialect=dialect,
                       metadata=metadata,
                       min_size=min_size,
                       max_size=max_size)

    result = data.draw(strategy)

    assert isinstance(result, Table)
    assert min_size <= len(result.columns)
    assert max_size is None or len(result.columns) <= max_size
    assert not result.columns or result.primary_key

