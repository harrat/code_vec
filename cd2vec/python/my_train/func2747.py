def test_modify_attribute_kmip_2_0_with_singlevalued_unset_attr(self):
        """
        Test that a KmipError is raised when attempting to modify an
        unset attribute with KMIP 2.0 parameters.
        """
        e = engine.KmipEngine()
        e._protocol_version = contents.ProtocolVersion(2, 0)
        e._attribute_policy._version = e._protocol_version
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._get_attribute_from_managed_object = mock.Mock(return_value=None)

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._set_attribute_on_managed_object(
            secret,
            ("Sensitive", primitives.Boolean(None))
        )

        args = (
            payloads.ModifyAttributeRequestPayload(
                unique_identifier="1",
                new_attribute=objects.NewAttribute(
                    attribute=primitives.Boolean(
                        True,
                        tag=enums.Tags.SENSITIVE
                    )
                )
            ),
        )
        self.assertRaisesRegex(
            exceptions.KmipError,
            "The 'Sensitive' attribute is not set on the managed "
            "object. It must be set before it can be modified.",
            e._process_modify_attribute,
            *args
        )
