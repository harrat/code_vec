@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_delete_snippet_010(snippy):
        """Delete snippet with data.

        Try to delete snippet with content data that does not exist. In this
        case the content data is truncated.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--content', 'docker rm --volumes $(docker ps --all)'])
        assert cause == 'NOK: cannot find content with content data: docker rm --volumes $(docker p...'
        Content.assert_storage(content)
