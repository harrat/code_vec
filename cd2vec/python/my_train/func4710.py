@given(NON_EMPTY_TEXT_ITERABLES)
    def test_it_generates_regexp_that_can_match_input_strings(self, strings):
        self.check_formatter_output(strings)
