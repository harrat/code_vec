def test_specnorm(self):
        layer_test(
            DynastesDense, kwargs={'units': 12,
                                   'kernel_normalizer': 'spectral',
                                   'use_wscale': True,
                                   'wnorm': True,
                                   'kernel_regularizer': 'orthogonal'}, input_shape=(5, 32, 3))
