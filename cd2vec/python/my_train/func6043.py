@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_update_reference_001(snippy, edited_gitlog):
        """Update reference with ``digest`` option.

        Update reference based on short message digest. Only content links
        are updated. The update is made with editor.
        """

        content = {
            'data': [
                Content.deepcopy(Reference.GITLOG),
                Reference.REGEXP
            ]
        }
        content['data'][0]['links'] = ('https://docs.docker.com', )
        content['data'][0]['digest'] = '1fc34e79a4d2bac51a039b7265da464ad787da41574c3d6651dc6a128d4c7c10'
        edited_gitlog.return_value = Content.dump_text(content['data'][0])
        cause = snippy.run(['snippy', 'update', '--scat', 'reference', '-d', '5c2071094dbfaa33', '--format', 'text'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
