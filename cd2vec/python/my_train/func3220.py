def test_backup_file_exists(tmpdir, binary_file):
    """Test wkr.os.backup_file where the backup file already exists."""
    binary_path = tmpdir.__class__(binary_file)
    # create the backup file and put something in it
    backup_path = binary_path.new(basename=binary_path.basename + '~')
    original_contents = b'abcde'
    backup_path.ensure().write(original_contents)
    # now do the backup
    backup_file(binary_file)
    # assert that the backup_path exists and that it contains the same
    # content as the original file
    assert backup_path.read(mode='rb') != original_contents
    assert backup_path.read(mode='rb') == binary_path.read(mode='rb')

