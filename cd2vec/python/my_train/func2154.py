def test_logged_condition_exception():
    cond = LoggedCondition('test', log_interval=.2)

    should_throw = False

    class TestException(Exception):
        pass

    def waiter():
        if should_throw:
            raise TestException

    with pytest.raises(TestException):
        with concurrent(cond.wait_for, waiter, 'throw'):
            sleep(0.5)
            should_throw = True
