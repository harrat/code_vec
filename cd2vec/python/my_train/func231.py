@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_nhl_goalie_returns_requested_career_stats(self, *args, **kwargs):
        # Request the career stats
        player = Player('howarja02')
        player = player('')

        for attribute, value in self.goalie_results_career.items():
            assert getattr(player, attribute) == value
