def test_get_attributes_from_managed_object(self):
        """
        Test that multiple attributes can be retrieved from a given managed
        object.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        symmetric_key = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b'',
            masks=[enums.CryptographicUsageMask.ENCRYPT,
                   enums.CryptographicUsageMask.DECRYPT]
        )
        symmetric_key.names = ['Name 1', 'Name 2']

        e._data_session.add(symmetric_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        result = e._get_attributes_from_managed_object(
            symmetric_key,
            ['Unique Identifier',
             'Name',
             'Cryptographic Algorithm',
             'Cryptographic Length',
             'Cryptographic Usage Mask',
             'invalid']
        )
        attribute_factory = factory.AttributeFactory()

        self.assertEqual(6, len(result))

        attribute = attribute_factory.create_attribute(
            enums.AttributeType.UNIQUE_IDENTIFIER,
            '1'
        )
        self.assertIn(attribute, result)

        attribute = attribute_factory.create_attribute(
            enums.AttributeType.CRYPTOGRAPHIC_ALGORITHM,
            enums.CryptographicAlgorithm.AES
        )
        self.assertIn(attribute, result)

        attribute = attribute_factory.create_attribute(
            enums.AttributeType.CRYPTOGRAPHIC_LENGTH,
            0
        )
        self.assertIn(attribute, result)

        attribute = attribute_factory.create_attribute(
            enums.AttributeType.CRYPTOGRAPHIC_USAGE_MASK,
            [enums.CryptographicUsageMask.ENCRYPT,
             enums.CryptographicUsageMask.DECRYPT]
        )
        self.assertIn(attribute, result)
