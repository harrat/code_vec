def test_which_path(tmpdir):
    dirname = os.path.abspath(os.path.join(str(tmpdir), 'name with spaces'))
    fn = 'asv_test_exe_1234.exe'
    fn2 = 'asv_test_exe_4321.bat'

    os.makedirs(dirname)
    shutil.copyfile(sys.executable, os.path.join(dirname, fn))
    shutil.copyfile(sys.executable, os.path.join(dirname, fn2))

    old_path = os.environ.get('PATH', '')
    try:
        if WIN:
            os.environ['PATH'] = old_path + os.pathsep + '"' + dirname + '"'
            util.which('asv_test_exe_1234')
            util.which('asv_test_exe_1234.exe')
            util.which('asv_test_exe_4321')
            util.which('asv_test_exe_4321.bat')
