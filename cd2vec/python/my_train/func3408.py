def test_large_inserts(self):
        ''' data structure should be empty at the beginning '''
        kvs_size = self.kvs_size()
        self.assertEqual(kvs_size, 0)
        kvs_count = self.kvs_count()
        self.assertEqual(kvs_count, 0)
        rc_count = self.rc_count()
        self.assertEqual(rc_count, 0)

        ''' verify for content lengths of different orders of magnitude '''
        for chunking_levels in range(1, 8):
            content_length = self.random.randint(
                int(self.S * (self.S / self.R) ** (chunking_levels - 1)), int(self.S * (self.S / self.R) ** (chunking_levels)))

            ''' averaged over 10 insertions, it is unlikely that a content's storage costs are above four times of its expected storage costs '''
            expected_costs = (content_length / self.S) * (self.S + 
                                                          self.digest_size) * 2  # storage for leaf chunks times two
            for _ in range(10):
                content = bytes([self.random.randint(0, 255)
                                 for _ in range(content_length)])
                self.seccs.put_content(content)

                kvs_size += 4 * expected_costs

            self.assertLess(self.kvs_size(), kvs_size)
            kvs_size = self.kvs_size()
