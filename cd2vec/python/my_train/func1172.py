@pytest.mark.googlevision
@pytest.mark.parametrize(
    "image", [empty_path, cv2.imread(empty_path), Image.open(empty_path)]
)
def test_empty_image_google_vision(image, google_vision_empty_image_mock):
    text, conf = gpyocr.google_vision_ocr(image)
    assert text == ""
    assert conf == 0

