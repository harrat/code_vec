def test_output_has_Person_objects(results):
    assert any([isinstance(item, Person) for item in results[0]])

