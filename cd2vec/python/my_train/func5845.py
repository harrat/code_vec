def test_get_head(git_dir):
    git_dir.chdir()
    assert dzonegit.get_head() == "4b825dc642cb6eb9a060e54bf8d69288fbee4904"
    git_dir.join("dummy").write("dummy\n")
    subprocess.call(["git", "add", "dummy"])
    subprocess.call(["git", "commit", "-m", "dummy"])
    assert dzonegit.get_head() != "4b825dc642cb6eb9a060e54bf8d69288fbee4904"

