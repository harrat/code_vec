def test_certwarn_false(self, request, httpbin_secure):
        """Test quiet_urllib=False shows warning from urllib3."""
        url = httpbin_secure.url
        http = Http(url=url, certwarn=False)

        http()
