@pytest.mark.parametrize('error,status', (
        (api.ImmediateHttpResponse(None, HTTPStatus.NOT_MODIFIED, {}), HTTPStatus.NOT_MODIFIED),
        (ValidationError("Error"), 400),
        (ValidationError({}), 400),
        (NotImplementedError, 501),
        (ValueError, 500),
        (api.ImmediateHttpResponse(ValueError, HTTPStatus.NOT_MODIFIED, {}), 500),
    ))
    def test_dispatch__exceptions(self, error, status):
        def callback(request):
            raise error

        target = containers.ApiInterfaceBase()
        operation = Operation(callback)
        actual = target.dispatch(operation, MockRequest())

        assert actual.status == status
