@mock_requests(method=GET, response_file="cards.json")
    @mock_requests(method=POST, response_file="card_unblock_single.json")
    def test_unblock_card_cli_single(self):
        from n26.cli import card_unblock
        card_id = "12345678-1234-abcd-abcd-1234567890ab"
        result = self._run_cli_cmd(card_unblock, ["--card", card_id])
        self.assertEqual(result.output, "Unblocked card: {}\n".format(card_id))
