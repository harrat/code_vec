def test_user_validation_reject(self):
        """Test user-defined validation which on request which should be
        rejected."""
        self.get_response('/styles/1', 403, False)
