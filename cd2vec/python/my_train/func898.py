@project_setup_py_test("hello-pure", ["build"], disable_languages_test=True)
def test_hello_pure_builds(capsys):
    out, _ = capsys.readouterr()
    assert "skipping skbuild (no CMakeLists.txt found)" in out

