def test_lookup_with_extra_fields():
    res = powo.lookup('urn:lsid:ipni.org:names:320035-2', include=['distribution', 'descriptions'])
    assert 'distribution' in res
    assert 'descriptions' in res

def test_filters():
    q = { Name.family: 'Poaceae' }
    f = Filters.accepted
    unfiltered = powo.search(q)
    filtered = powo.search(q, filters = f)
