def test_defined_weeks():
    progress_bar = ProgressBarYum(2000000000, 'file.iso')

    assert 'file.iso   0% [          ] --- KiB/s |   0.0 B              ' == str(progress_bar)

    eta._NOW = lambda: 1411868722.0
    progress_bar.numerator = 1
    assert 'file.iso   0% [          ] --- KiB/s |   1.0 B              ' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 2
    assert 'file.i   0% [       ]   0.5 B/s |   2.0 B  1111111:06:36 ETA' == str(progress_bar)

