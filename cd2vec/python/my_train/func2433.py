def test_refine_video_metadata(mkv):
    scanned_video = scan_video(mkv['test5'])
    refine(scanned_video, episode_refiners=('metadata',), movie_refiners=('metadata',))
    assert type(scanned_video) is Movie
    assert scanned_video.name == mkv['test5']
    assert scanned_video.source is None
    assert scanned_video.release_group is None
    assert scanned_video.resolution is None
    assert scanned_video.video_codec == 'H.264'
    assert scanned_video.audio_codec == 'AAC'
    assert scanned_video.imdb_id is None
    assert scanned_video.hashes == {
        'opensubtitles': '49e2530ea3bd0d18',
        'shooter': '36f3e2c50566ca01f939bf15d8031432;b6132ab62b8f7d4aaabe9d6344b90d90;'
                   'bea6074cef7f1de85794f3941530ba8b;18db05758d5d0d96f246249e4e4b5d79',
        'thesubdb': '64a8b87f12daa4f31895616e6c3fd39e'}
    assert scanned_video.size == 31762747
    assert scanned_video.subtitle_languages == {Language('spa'), Language('deu'), Language('jpn'), Language('und'),
                                                Language('ita'), Language('fra'), Language('hun')}
    assert scanned_video.title == 'test5'
    assert scanned_video.year is None

