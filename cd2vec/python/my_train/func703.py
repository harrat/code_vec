@pytest.mark.parametrize("key", [
    "Do", "mu", "S", "beta_corr", "drizzle_N", "drizzle_lwc", "drizzle_lwf", "v_drizzle",
    "v_air", "Do_error", "drizzle_lwc_error", "drizzle_lwf_error", "S_error",
    "Do_bias", "drizzle_lwc_bias", "drizzle_lwf_bias", "drizzle_N_error",
    "v_drizzle_error", "mu_error", "drizzle_N_bias", "v_drizzle_bias"])
def test_append_data(class_objects, result, key):
    from cloudnetpy.products.drizzle import _append_data
    d_source, d_class, s_width = class_objects
    _append_data(d_source, result)
    assert key in d_source.data.keys()

