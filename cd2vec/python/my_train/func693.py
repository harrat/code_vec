@given(NON_EMPTY_TEXT_ITERABLES)
    def test_accepts_iterable_of_strings(self, expected_strings):
        self._tree.extend(expected_strings)
        assert_strings_can_be_matched(
            self, self._tree.to_regexp(PythonFormatter), expected_strings
        )
