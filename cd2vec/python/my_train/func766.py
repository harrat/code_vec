def test_simple(self):
        layer_test(
            DynastesConv1DTranspose,
            kwargs={'filters': 3, 'kernel_size': 3, 'strides': 2, 'padding': 'same'},
            input_shape=(None, 16, 3),
            expected_output_shape=(None, 32, 3)
        )
