def test_load_path(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        self.assertEqual(audiofile.all_mfcc.shape[0], 13)
        self.assertEqual(audiofile.all_mfcc.shape[1], 1331)
        self.assertAlmostEqual(audiofile.audio_length, TimeValue("53.3"), places=1)     # 53.266
