def test_list_graph_edges(self):
        edges = []
        for edge in self.graph.edges:
            edges.append(edge.name)
        assert str(self.graph.list_graph_edges()) == \
            "['ab', 'x', 'x', 'x', 'x']"
