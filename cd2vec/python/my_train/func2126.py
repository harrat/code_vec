def test_defined_rounded():
    eta._NOW = lambda: 1411868723.5
    progress_bar = ProgressBarWget(1023)

    assert ' 0% [                  ] 0           --.-KiB/s              ' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 1022
    assert '99% [================> ] 1,022       --.-KiB/s              ' == str(progress_bar)

    eta._NOW = lambda: 1411868724.5
    progress_bar.numerator = 1023
    assert '100%[=================>] 1,023       --.-KiB/s   in 1s      ' == str(progress_bar)

