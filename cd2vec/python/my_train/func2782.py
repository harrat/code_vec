def test_write_lc_filter():
    d, e, f = prepare_testset_real()
    hjd = np.linspace(989.34316, 1002.63264, d.shape[1])
    fn = ['B', 'Han']
    r = np.random.uniform(size=d.shape[1])
    fm = np.array([r<0.5, r>=0.5], dtype=bool)
    with TmpDir() as td:
        write_lc_filters(fm, fn, hjd, d, e, prefix=join(td.path, 'lc_'))
        pass
