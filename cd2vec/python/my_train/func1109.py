def test_sync_update_move(mbed, testrepos):
    test1 = testrepos[0]
    popen(['python', mbed, 'import', test1, 'testimport', '-vv'])

    with cd('test1/test2'):
        move('test3', 'testmove')
        popen(['python', mbed, 'sync', '-vv'])
        mkcommit()

    with cd('test1'):
        popen(['python', mbed, 'sync', '-vv'])
        mkcommit()

    with cd('testimport'):
        popen(['python', mbed, 'update', '-vv'])

    assertls(mbed, 'testimport', [
        "[mbed]",
        "testimport",
        "`- test2",
        "   `- testmove",
        "      `- test4",
    ])
