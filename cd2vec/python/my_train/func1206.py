def test_fetch_issue_by_id(talker, mock_fetch):
    expected = MockFetchIssueResponse.fetch_issue_data_by_issue_id()
    meta_data = talker.fetch_issue_data_by_issue_id("1")
    assert meta_data is not None
    assert isinstance(meta_data, GenericMetadata)
    assert meta_data.series == expected.series
    assert meta_data.issue == expected.issue
    assert meta_data.year == expected.year
    assert meta_data.day == expected.day
    assert meta_data.credits[0] == expected.credits[0]

