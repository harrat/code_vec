def test_show_nonexistent_project_shows_error(cli):
    output = cli('project', ['show', '777'])
    assert output == 'Error: Could not find project `777`\n'

