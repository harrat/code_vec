def test_pickle_dumpsloads_dotted():
    orig = udict({'one.two': 'one.two'})
    pickled = pickle.dumps(orig)
    unpickled = pickle.loads(pickled)
    assert items(unpickled) == items(orig)

