@pytest.mark.project
def testProjectNewMinimal(nwTempProj, nwRef, nwTemp):
    projFile = path.join(nwTempProj,"nwProject.nwx")
    refFile  = path.join(nwRef,"proj", "1_nwProject.nwx")
    assert theConf.initConfig(nwRef, nwTemp)
    assert theProject.newProject({"projPath": nwTempProj})
    assert theProject.setProjectPath(nwTempProj)
    assert theProject.saveProject()
    assert theProject.closeProject()
    assert cmpFiles(projFile, refFile, [2, 6, 7, 8])

@pytest.mark.project
def testProjectOpen(nwTempProj):
    projFile = path.join(nwTempProj,"nwProject.nwx")
    assert theProject.openProject(projFile)
