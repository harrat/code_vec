def test_demand_add(self):
        self.model.add_demand('A', 'B', 40, 'dmd_a_b')
        self.model.update_simulation()
        self.assertEqual(self.model.__repr__(),
                         'FlexModel(Interfaces: 40, Nodes: 10, Demands: 15, RSVP_LSPs: 4)')
