def test_dependencies(make_runner, dep_cases, common_exec_ctx):
    runner = make_runner()
    runner.runall(dep_cases)
    assert_dependency_run(runner)

