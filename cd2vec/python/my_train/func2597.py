@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_api_search_solution_field_004(server):
        """Get specific solution field.

        Send GET /solutions/{id}/tags for existing solution.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '269'
        }
        expect_body = {
            'data': {
                'type': 'solution',
                'id': Solution.BEATS_UUID,
                'attributes': {
                    'tags': Storage.ebeats['tags']
                }
            },
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/solutions/21cd5827-b6ef-4067-b5ac-3ceac07dde9f/tags'
            }
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/solutions/4346ba4c79247430/tags',
            headers={'accept': 'application/vnd.api+json'})
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
