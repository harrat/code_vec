def test_read_header(self):
        gp = parser.GamryParser(filename='tests/cv_data.dta')
        blob, count = gp.read_header()
        self.assertEqual(count, 789)
        self.assertEqual(gp.header, blob)
        self.assertEqual(gp.header['DATE'], '3/6/2019')
        self.assertEqual(gp.header['CHECKPSTAT'], 'potentiostat-id')
        self.assertEqual(gp.header['CHECKPOTEN'], 0.5)
        self.assertEqual(gp.header['CHECKQUANT'], 1.2345)
        self.assertEqual(gp.header['CHECKIQUANT'], 5)
        self.assertEqual(gp.header['CHECKSELECTOR'], 0)
        self.assertFalse(gp.header['CHECKTOGGLE'])
        self.assertTrue(isinstance(gp.header['CHECK2PARAM'], dict))
        self.assertTrue(gp.header['CHECK2PARAM']['enable'])
        self.assertEqual(gp.header['CHECK2PARAM']['start'], 300)
        self.assertEqual(gp.header['CHECK2PARAM']['finish'], 0.5)
