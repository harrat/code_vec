def test_s3_trigger(tracer_and_invocation_support, handler, mock_s3_event, mock_context):
    thundra, handler = handler
    tracer, invocation_support = tracer_and_invocation_support
    assert lambda_event_utils.get_lambda_event_type(mock_s3_event,
                                                    mock_context) == lambda_event_utils.LambdaEventType.S3
    try:
        response = handler(mock_s3_event, mock_context)
    except:
        print("Error running handler!")
        raise
    span = tracer.recorder.get_spans()[0]

    invocation_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, InvocationPlugin):
            invocation_plugin = plugin

    assert span.get_tag(constants.SpanTags['TRIGGER_DOMAIN_NAME']) == constants.DomainNames['STORAGE']
    assert span.get_tag(constants.SpanTags['TRIGGER_CLASS_NAME']) == constants.ClassNames['S3']
    assert span.get_tag(constants.SpanTags['TRIGGER_OPERATION_NAMES']) == ['example-bucket']

    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_DOMAIN_NAME']) == constants.DomainNames['STORAGE']
    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_CLASS_NAME']) == constants.ClassNames['S3']
    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_OPERATION_NAMES']) == ['example-bucket']

    assert invocation_plugin.invocation_data['incomingTraceLinks'] == ["EXAMPLE123456789"]

