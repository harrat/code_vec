@tornado.testing.gen_test
    def test_app_with_user_keyonly_for_bad_authentication_type(self):
        self.body_dict.update(username='keyonly', password='foo')
        response = yield self.async_post('/', self.body_dict)
        self.assertEqual(response.code, 200)
        self.assert_status_in('Bad authentication type', json.loads(to_str(response.body))) # noqa
