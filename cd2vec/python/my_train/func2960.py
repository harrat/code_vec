def test_outdated_projects_db(tmpdir):
    # Simulate a projects db version change
    projects.LocalProjectsDb.VERSION = 1
    try:
        p = projects.ProjectsDb(tmpdir.strpath)
        p.update([])
    finally:
        projects.LocalProjectsDb.VERSION = 2

    with pytest.raises(projects.OutdatedProjectsDbException):
        p.get_projects()
