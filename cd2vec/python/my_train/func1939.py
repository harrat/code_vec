def test_signal_weakref_context_manager_delete_during():
    import gc

    result = []

    class Foo:
        @contextmanager
        def on_ctx_test(self, before, after):
            result.append(before)
            yield
            result.append(after)

    foo = Foo()
    register_object(foo)

    with on_ctx_test(before=1, after=2):
        assert result == [1]
    assert result == [1, 2]

    with on_ctx_test(before=3, after=4):
        assert result == [1, 2, 3]
        del foo
        gc.collect()
    # The context manager should keep the signal handler alive
    assert result == [1, 2, 3, 4]

