def test_get_sheets(self):
        sheets = self.spreadsheet.sheets
        assert len(sheets) == 2
        for sheet in sheets:
            assert isinstance(sheet, Sheet)
