def test_logging_std_nested(sphinx_app):
    """Test that nested stdout/stderr uses within a given script work."""
    log_rst = op.join(
        sphinx_app.srcdir, 'auto_examples', 'plot_log.rst')
    with codecs.open(log_rst, 'r', 'utf-8') as fid:
        lines = fid.read()
    assert '.. code-block:: none\n\n    is in the same cell' in lines
    assert '.. code-block:: none\n\n    is not in the same cell' in lines

