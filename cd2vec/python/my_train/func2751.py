@patch("necbaas.Service._do_request")
    def test_execute_rest(self, mock):
        """??? REST API ????????"""
        service = baas.Service(self.get_sample_param())

        service.session_token = "token1"

        query = {"a": 1}

        mock.return_value = {}
        ret = service.execute_rest("GET", "a/b/c", query=query)
        assert ret == {}

        method = mock.call_args[0][0]
        kwargs = mock.call_args[1]
        assert method == "GET"
        assert kwargs["url"] == "http://localhost/api/1/tenant1/a/b/c"
        assert kwargs["params"] == query

        headers = kwargs["headers"]
        assert headers["X-Application-Id"] == "app1"
        assert headers["X-Application-Key"] == "key1"
        assert headers["X-Session-Token"] == "token1"

        # timeout ??????
        assert "timeout" not in kwargs
