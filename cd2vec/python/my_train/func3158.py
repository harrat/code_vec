def test_delete_resource(self):
        """Test DELETEing a resource."""
        response = self.app.delete('/artists/239')
        assert response.status_code == 204
        response = self.get_response('/artists/239', 404, False)
