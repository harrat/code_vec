@staticmethod
    @pytest.mark.usefixtures('default-references', 'export-time')
    def test_cli_export_reference_030(snippy):
        """Export reference template.

        Export reference template by explicitly defining content category
        and the template text format.
        """

        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'reference', '--template', '--format', 'text'])
            assert cause == Cause.ALL_OK
            mock_file.assert_called_once_with('./reference-template.text', mode='w', encoding='utf-8')
            file_handle = mock_file.return_value.__enter__.return_value
            file_handle.write.assert_called_with(Const.NEWLINE.join(Reference.TEMPLATE_TEXT))
