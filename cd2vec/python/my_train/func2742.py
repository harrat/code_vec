def test_initread():
    """Test for IDF.initread() with filename in unicode and as python str.
    """
    # setup
    idf = IDF()
    idf.initreadtxt(idfsnippet)
    idf.saveas("tmp.idf")

    # test fname as unicode
    fname = "tmp.idf"
    assert isinstance(fname, string_types)
    idf = IDF()
    idf.initread(fname)
    assert idf.getobject("BUILDING", "Building")

    # test fname as str
    fname = str("tmp.idf")
    assert isinstance(fname, string_types)
    idf = IDF()
    idf.initread(fname)
    assert idf.getobject("BUILDING", "Building")

    # test that a nonexistent file raises an IOError
    fname = "notarealfilename.notreal"
    idf = IDF()
    try:
        idf.initread(fname)
        assert False  # shouldn't reach here
    except IOError:
        pass

    # teardown
    os.remove("tmp.idf")

