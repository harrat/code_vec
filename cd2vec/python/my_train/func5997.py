def test_n_random_starts_lower_1():
    """Assert than an error is raised when n_random_starts<1."""
    atom = ATOMClassifier(X_bin, y_bin, random_state=1)
    atom.run(['LR', 'LDA'], n_calls=5, n_random_starts=(2, -1))
    assert atom.errors.get('LDA')

