def test_wrongfile_download():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open('hello.txt', 'w') as f:
            f.write('Hello World!')
        thread = threading.Thread(target=runner.invoke, args=[localshare.share, ["hello.txt"]])
        thread.start()
        result = runner.invoke(localshare.download, ['nothello.txt'])
        assert result.exit_code == 1
        assert 'nothello.txt not found in available files.' in str(result.exception)
