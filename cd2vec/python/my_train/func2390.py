def test_pull_player_list_throws_error_when_year_before_1917():
	nhl = NHLScraper()
	with pytest.raises(ValueError):
		nhl._pull_player_list("19161917")
