@staticmethod
    @pytest.mark.usefixtures('yaml', 'default-snippets', 'export-time')
    def test_cli_export_snippet_019(snippy):
        """Export defined snippet with content data.

        Export defined snippet based on content data. File name is defined in
        command line as yaml file.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Snippet.REMOVE
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-c', 'docker rm --volumes $(docker ps --all --quiet)', '-f', 'defined-snippet.yaml'])  # pylint: disable=line-too-long
            assert cause == Cause.ALL_OK
            Content.assert_yaml(yaml, mock_file, 'defined-snippet.yaml', content)
