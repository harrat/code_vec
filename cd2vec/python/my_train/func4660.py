def test_slicetiming_list_outputs(create_files_in_directory):
    filelist, outdir = create_files_in_directory
    st = spm.SliceTiming(in_files=filelist[0])
    assert st._list_outputs()["timecorrected_files"][0][0] == "a"

