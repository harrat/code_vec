def test_write_will_create_path_if_missing():
    """
    Test that the hosts file declared when constructing a Hosts instance will
    be created if it doesn't exist
    """
    now = datetime.datetime.now()
    timestamp = now.strftime('%Y%m%d%H%M%S')
    hosts_path = '/tmp/testwrite.{0}'.format(timestamp)
    hosts = Hosts(path=hosts_path)
    entry = HostsEntry.str_to_hostentry('1.2.3.4 example.com example.org')
    hosts.add(entries=[entry])
    hosts.write()
    hosts2 = Hosts(path=hosts_path)
    os.remove(hosts_path)
    assert hosts2.exists(address='1.2.3.4')

