def test_for_each_cell(blot):
    def test_forward(x, y):
        assert x, y in blot._cells

    blot.for_each_cell(test_forward)

def test_aspect():
    grid_width = 1000
    grid_height = 10
    num_cells = 500
