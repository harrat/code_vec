def test_ds_joint_kde_smoke(ds_NDs):
    with seaborn.axes_style("ticks"):
        fig2 = _do_jointplots(ds_NDs, kde=True)
        return fig2
