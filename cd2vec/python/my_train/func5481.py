def test_AveragedFunction():
    ## averaging a simple function 
    func = lambda x: np.asarray(x).sum()
    avfunc = noisyopt.AveragedFunction(func, N=30)
    av, avse = avfunc([1.0, 1.0])
    assert av == 2.0
    assert avse == 0.0

    # se of function value difference between two points is zero
    # (as function evaluation is not stochastic)
    diffse = avfunc.diffse([1.0, 1.0], [2.0, 1.0])
    assert diffse == 0.0

    ## changing the number of evaluations
    avfunc.N *= 2
    assert avfunc.N == 60

    ## averaging a stochastic function
    func = lambda x: np.asarray(x).sum() + np.random.randn()
    avfunc = noisyopt.AveragedFunction(func, N=30)
    # check that reevaluation gives the same thing due to caching
    av30_1, avse30_1 = avfunc([1.0, 1.0])
    av30_2, avse30_2 = avfunc([1.0, 1.0])
    assert av30_1 == av30_2
    assert avse30_1 == avse30_2
    # check that se decreases if 
    avfunc.N *= 2
    av60, avse60 = avfunc([1.0, 1.0])
    assert av30_1 != av60
    assert avse30_1 > avse60

    # test with floating point N
    noisyopt.AveragedFunction(func, N=30.0, paired=True)

def test_DifferenceFunction():
    difffunc = noisyopt.DifferenceFunction(lambda x: x+1, lambda x: x+2)
    d, dse = difffunc(0)
    assert d == -1.0
    d, dse = difffunc(1)
    assert d == -1.0
    # again with same value to test caching
    d, dse = difffunc(0)
    assert d == -1.0
