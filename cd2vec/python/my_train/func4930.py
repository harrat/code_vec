def test_correct_initial_index_found(self):
        seasons = ['2017', None, '2018']
        mock_season = mock.PropertyMock(return_value=seasons)
        player = Player('verlaju01')
        type(player)._season = mock_season

        result = player._find_initial_index()

        assert player._index == 1
