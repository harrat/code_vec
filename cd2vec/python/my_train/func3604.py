def test_race_condition(fx_stages, fx_feed):
    stage, _ = fx_stages
    with stage:
        stage.feeds['test'] = fx_feed
    result = parallel_map(
        10,
        functools.partial(apply_timestamp, stage, 'test'),
        map(timestamp, range(10))
    )
    for _ in result:
        pass
    with stage:
        updated_at = stage.feeds['test'].entries[0].read.updated_at
    assert updated_at == timestamp(9)

