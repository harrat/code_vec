def test_geweke_default_failed():
    data = np.vstack((np.random.normal(loc=0.0, size=100000),
                      np.random.normal(loc=1.0, size=100000))).T
    consumer = ChainConsumer()
    consumer.add_chain(data, walkers=20, name="c1")
    data2 = data.copy()
    data2[98000:, :] += 0.3
    consumer.add_chain(data2, walkers=20, name="c2")
    assert not consumer.diagnostic.geweke()
