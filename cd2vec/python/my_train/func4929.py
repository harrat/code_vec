def test_implements_not_cls_convention():
    """
    Case: do not implement interface member, which do not follow naming convention, that is class method.
    Expect: class does not implement interface member error message.
    """
    class HumanBasicsInterface:

        @classmethod
        def think(this, about, *args, **kwargs):
            pass

    with pytest.raises(InterfaceMemberHasNotBeenImplementedException) as error:

        @implements(HumanBasicsInterface)
        class HumanWithoutImplementation:
            pass

    assert 'class HumanWithoutImplementation does not implement ' \
           'interface member HumanBasicsInterface.think(this, about, args, kwargs)' == error.value.message
