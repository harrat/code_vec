def test_list_graph_vertices(self):
        vertices = []
        for vertex in self.graph.vertices:
            vertices.append(vertex)
        assert str(self.graph.list_graph_vertices()) == \
            "['b', 'a', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']"
