def test_instance():
    tb = TestBean()
    _register.uncheck(4)
    tb.field1 = 1
    _register.check(4, 1)
    _register.uncheck(1)
    assert tb.field1 == 1
    _register.check(1)
    with pytest.raises(TypeError):
        tb.field2 = True
    alpha = Alpha()
    tb.field2 = alpha
    _register.uncheck(2)
    assert tb.field2 == alpha
    _register.check(2)
    tb.field3 = "test"
    _register.uncheck(3)
    assert tb.field3 == "test"
    _register.check(3)

