def test_static_string(good_data):
    """ Test statically sized string fields """
    string = six.text_type(good_data, encoding='utf8')
    packet1 = StaticPacket(string=string)
    raw1    = packet1.pack()
    packet2 = StaticPacket.from_raw(raw1)
    raw2    = struct.pack('>256s', good_data)
    packet3 = StaticPacket()
    packet3.unpack(raw2)

    assert packet1['string'] == string
    assert packet2['string'] == string
    assert packet3['string'] == string
    assert raw1              == raw2

def test_dynamic_string(good_data):
    """ Test dynamically sized string fields """
    string = six.text_type(good_data, encoding='utf8')
    count = len(good_data)
    packet1 = DynamicPacket(string=string)
    raw1    = packet1.pack()
    packet2 = DynamicPacket.from_raw(raw1)
    raw2    = struct.pack('>H{}s'.format(count), count, good_data)
    packet3 = DynamicPacket()
    packet3.unpack(raw2)
