def test_auto_log_process(auto_logger):
    proc_resumer = None
    auto_logger.setLevel(logging.DEBUG)

    def resumer(i_am_process):
        advance(10)
        i_am_process.resume()

    def proc():
        nonlocal proc_resumer
        local.name = "i-am-process"
        advance(10)
        proc_resumer = add(resumer, Process.current())
        pause()

    sim = Simulator()
    proc_proc = sim.add(proc)
    name_proc_orig = proc_proc.local.name
    sim.run()

    name_proc = proc_proc.local.name
    check_log(
        auto_logger,
        (logging.INFO, 0.0, "", "Simulator", sim.name, "add", dict(fn=proc, args=(), kwargs={})),
        (
            logging.DEBUG, 0.0, "", "Simulator", sim.name, "schedule",
            dict(delay=0.0, fn=proc_proc.switch, args=(), kwargs={}, counter=0)
        ),
        (logging.INFO, 0.0, "", "Simulator", sim.name, "run", dict(duration=inf)),
        (logging.DEBUG, 0.0, "", "Simulator", sim.name, "exec-event", dict(counter=0)),
        (logging.DEBUG, 0.0, name_proc_orig, "Process", name_proc_orig, "rename", dict(new="i-am-process")),
        (logging.INFO, 0.0, name_proc, "Process", name_proc, "advance", dict(delay=10.0)),
        (
            logging.DEBUG, 0.0, name_proc, "Simulator", sim.name, "schedule",
            dict(delay=10.0, fn=proc_proc.switch, args=(), kwargs={}, counter=1)
        ),
        (logging.DEBUG, 10.0, "", "Simulator", sim.name, "exec-event", dict(counter=1)),
        (logging.INFO, 10.0, name_proc, "Simulator", sim.name, "add", dict(fn=resumer, args=(proc_proc,), kwargs={})),
        (
            logging.DEBUG, 10.0, name_proc, "Simulator", sim.name, "schedule",
            dict(delay=0.0, fn=proc_resumer.switch, args=(proc_proc,), kwargs={}, counter=2)
        ),
        (logging.INFO, 10.0, name_proc, "Process", name_proc, "pause", {}),
        (logging.DEBUG, 10.0, "", "Simulator", sim.name, "exec-event", dict(counter=2)),
        (logging.INFO, 10.0, proc_resumer.local.name, "Process", proc_resumer.local.name, "advance", dict(delay=10.0)),
        (
            logging.DEBUG, 10.0, proc_resumer.local.name, "Simulator", sim.name, "schedule",
            dict(delay=10.0, fn=proc_resumer.switch, args=(), kwargs={}, counter=3)
        ),
        (logging.DEBUG, 20.0, "", "Simulator", sim.name, "exec-event", dict(counter=3)),
        (logging.INFO, 20.0, proc_resumer.local.name, "Process", name_proc, "resume", {}),
        (
            logging.DEBUG, 20.0, proc_resumer.local.name, "Simulator", sim.name, "schedule",
            dict(delay=0.0, fn=proc_proc.switch, args=(), kwargs={}, counter=4)
        ),
        (logging.INFO, 20.0, proc_resumer.local.name, "Process", proc_resumer.local.name, "die-finish", {}),
        (logging.DEBUG, 20.0, "", "Simulator", sim.name, "exec-event", dict(counter=4)),
        (logging.INFO, 20.0, name_proc, "Process", name_proc, "die-finish", {}),
        (logging.DEBUG, 20.0, "", "Simulator", sim.name, "out-of-events", {}),
        (logging.INFO, 20.0, "", "Simulator", sim.name, "stop", {})
    )
