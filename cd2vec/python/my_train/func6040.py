def test_check_banner_old_php_ossl(self):
        res = apache_httpd.check_banner(
            "Apache/2.4.6 (FreeBSD) PHP/5.4.23 OpenSSL/0.9.8n",
            "<raw-request-data>",
            "http://adamcaudill.com",
        )

        self.assertEqual(5, len(res))
        self.assertEqual("Apache Server Version Exposed: Apache/2.4.6", res[0].message)
        self.assertIn("Apache Server Outdated:", res[1].message)
        self.assertEqual("PHP Version Exposed: PHP/5.4.23", res[2].message)
        self.assertIn("PHP Outdated:", res[3].message)
        self.assertEqual("OpenSSL Version Exposed: OpenSSL/0.9.8n", res[4].message)
