def test_config():
    config = build_config(None)
    assert config["auth"]["username"].get() == "dremio"
    assert config["auth"]["password"].get() == "dremio123"
    assert config["auth"]["type"].get() == "basic"
    assert config["hostname"].get() == "localhost"
    assert config["port"].get(int) == 9047
    assert config["ssl"].get(bool) is False

