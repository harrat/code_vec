def test_jointplot_defaultlabels(jp_data):
    jg1 = viz.jointplot(x="B", y="C", data=jp_data, one2one=False, color="b")
    assert jg1.ax_joint.get_xlabel() == "B"
    assert jg1.ax_joint.get_ylabel() == "C"
    nptest.assert_array_equal(numpy.round(jg1.ax_joint.get_xlim()), [0, 17])
    nptest.assert_array_equal(numpy.round(jg1.ax_joint.get_ylim()), [0, 23])
    return jg1.fig

