def test_tokenize(self):
        sentence = "?????"
        gold = ["?", "?", "?", "?", "?"]
        self.assertEqual(
                gold,
                self.tokenizer.tokenize(sentence)
                )
