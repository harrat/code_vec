def test_incomplete_log(travis):

    jobs = travis.jobs(state='started')

    log = jobs[0].log

    assert log._body is not None

    assert log.body is not None

