@pytest.mark.vcr()
def test_access_groups_list_sort_direction_unexpectedvalue(api):
    with pytest.raises(UnexpectedValueError):
        api.access_groups.list(sort=(('uuid', 'nope'),))
