def test_invalid_property_name_starting_character_in_instance(self):
        new_object = copy.deepcopy(self.valid_custom_object)
        new_object['9ome_custom_stuff'] = new_object.pop('some_custom_stuff')

        self.assertFalseWithOptions(new_object)
