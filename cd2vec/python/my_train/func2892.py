@staticmethod
    @pytest.mark.usefixtures('import-kafka', 'export-time')
    def test_cli_export_solution_041(snippy):
        """Export defined solution with digest.

        Export text native content when the ``filename`` attribute defines the
        default file as text file. In this case the ``--format`` option defines
        that the exported file must be Markdown file. Command line option must
        always override other configuration.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Solution.KAFKA
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-d', 'ee3f2ab7c63d6965', '--format', 'mkdn'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, 'kubernetes-docker-log-driver-kafka.mkdn', content)
