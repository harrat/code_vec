def test_specnorm(self):
        layer_test(
            DynastesConv2D, kwargs={'filters': 7, 'kernel_size': (3, 3),
                                    'kernel_normalizer': 'spectral',
                                    'kernel_regularizer': 'orthogonal'}, input_shape=(4, 16, 16, 5))
