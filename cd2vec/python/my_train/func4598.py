@pytest.mark.parametrize("key", ["BUILDING", "Building", "building", "BuIlDiNg"])
def test_get_and_set(base_idf, key):
    idf = base_idf
    idf.newidfobject(key, Name="Building")
    buildings = idf.idfobjects["building"]
    assert len(buildings) == 1

