def test_eventbridge_trigger(tracer_and_invocation_support, handler, mock_eventbridge_event, mock_context):
    thundra, handler = handler
    tracer, invocation_support = tracer_and_invocation_support
    assert lambda_event_utils.get_lambda_event_type(mock_eventbridge_event,
                                                    mock_context) == lambda_event_utils.LambdaEventType.EventBridge
    try:
        handler(mock_eventbridge_event, mock_context)
    except:
        print("Error running handler!")
        raise
    span = tracer.recorder.get_spans()[0]

    invocation_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, InvocationPlugin):
            invocation_plugin = plugin

    assert span.get_tag(constants.SpanTags['TRIGGER_DOMAIN_NAME']) == constants.DomainNames['MESSAGING']
    assert span.get_tag(constants.SpanTags['TRIGGER_CLASS_NAME']) == constants.ClassNames['EVENTBRIDGE']
    assert span.get_tag(constants.SpanTags['TRIGGER_OPERATION_NAMES']) == ['EC2 Command Status-change Notification']

    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_DOMAIN_NAME']) == constants.DomainNames['MESSAGING']
    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_CLASS_NAME']) == constants.ClassNames['EVENTBRIDGE']
    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_OPERATION_NAMES']) == ['EC2 Command Status-change Notification']

    assert invocation_plugin.invocation_data['incomingTraceLinks'] == ["51c0891d-0e34-45b1-83d6-95db273d1602"]

