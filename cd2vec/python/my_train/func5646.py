@mock_requests(method=GET, response_file="cards.json")
    @mock_requests(method=POST, response_file="card_block_single.json")
    def test_block_card_cli_all(self):
        from n26.cli import card_block
        card_id_1 = "12345678-1234-abcd-abcd-1234567890ab"
        card_id_2 = "22345678-1234-abcd-abcd-1234567890ab"

        result = self._run_cli_cmd(card_block)
        self.assertEqual(result.output, "Blocked card: {}\nBlocked card: {}\n".format(card_id_1, card_id_2))
