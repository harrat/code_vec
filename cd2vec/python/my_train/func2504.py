def test_video_hook_atari() -> None:
    c = rainy.Config()
    c.eval_hooks.append(lib.hooks.VideoWriterHook(video_name="BreakoutVideo"))
    c.set_net_fn("dqn", net.value.dqn_conv())
    c.set_env(lambda: envs.Atari("Breakout"))
    c.eval_env = envs.Atari("Breakout")
    ag = agents.DQNAgent(c)
    c.initialize_hooks()
    _ = ag.eval_episode()
    ag.close()
    videopath = c.logger.logdir.joinpath("BreakoutVideo-0.avi")
    assert videopath.exists()

