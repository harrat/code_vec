@staticmethod
    def test_help_option_006(capsys, caplog):
        """Test invalid command line option.

        Try to run snippy with invalid command line option.
        """

        output = (
            'usage: snippy [-v, --version] [-h, --help] <operation> [<options>] [-vv] [-q]',
            'snippy: error: unrecognized arguments: -a',
            ''
        )
        snippy = Snippy(['snippy', '-a'])
        snippy.run()
        snippy.release()
        out, err = capsys.readouterr()
        assert out == Const.EMPTY
        assert err == Const.NEWLINE.join(output)
        assert not caplog.records[:]
        Content.delete()
