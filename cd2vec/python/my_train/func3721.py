@pytest.mark.remote
def test_fetch_event_json():
    out = api.fetch_event_json("GW150914")
    meta = out["events"]["GW150914-v3"]
    assert int(meta["GPS"]) == 1126259462
    assert meta["version"] == 3

