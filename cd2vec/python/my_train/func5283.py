def test_load_file_uses_loader_for_file(self, monkeypatch, http_folder):
        http_folder.load_file(file_name)

        loaders[file_name].assert_called_once_with(http_folder.local_dir / file_name)
