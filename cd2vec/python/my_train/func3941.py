def test_pysal_holed_multi_polygon(self):
        # East Porterville CDP, MultiPolygon with Holes
        geodata = self.conn.mapservice.query(layer=36, where="PLACE=21012")
        numpy.testing.assert_allclose(
            geodata.total_bounds,
            numpy.array([-13247907.9566, 4307142.6093, -13238473.741, 4310175.0494]),
        )
