def test_load_with_json_file(self, json_file):
        fadapter = File(json_file.name)
        fadapter.load()

        assert 'ABC' in fadapter.data
        assert fadapter.data['ABC'] == '123'
