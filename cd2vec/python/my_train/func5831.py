def test_xopen_buffer(self):
        buf = BytesIO(b'foo')
        f = xopen(cast(IOBase, buf), 'rb')
        assert b'foo' == f.read(3)
        with self.assertRaises(ValueError):
            xopen(cast(IOBase, buf), 'wb')

        with open_(str) as buf:
            buf.write('foo')
        assert 'foo' == buf.getvalue()

        with open_(bytes) as buf:
            buf.write(b'foo')
        assert b'foo' == buf.getvalue()

        # with compression
        with self.assertRaises(ValueError):
            with open_(bytes, compression=True):
                pass
        with self.assertRaises(ValueError):
            with open_(str, compression='gzip'):
                pass

        with open_(bytes, mode='wt', compression='gzip') as buf:
            buf.write('foo')
        assert b'foo' == gzip.decompress(buf.getvalue())

        # from string/bytes
        with self.assertRaises(ValueError):
            xopen('foo', 'wt', file_type=FileType.BUFFER)
        with self.assertRaises(ValueError):
            xopen('foo', 'rb', file_type=FileType.BUFFER)
        with open_(
                'foo', file_type=FileType.BUFFER, context_wrapper=True) as buf:
            assert 'foo' == buf.read()

        with self.assertRaises(ValueError):
            xopen(b'foo', 'rt', file_type=FileType.BUFFER)
        with open_(
                b'foo', file_type=FileType.BUFFER, context_wrapper=True) as buf:
            assert b'foo' == buf.read()
