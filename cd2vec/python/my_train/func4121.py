def test_hello_wheel():
    expected_content = [
        'hello/_hello%s' % get_ext_suffix(),
        'hello/__init__.py',
        'hello/__main__.py',
        'hello/world.py',
        'helloModule.py',
        'bonjour/__init__.py',
        'bonjour/data/ciel.txt',
        'bonjour/data/soleil.txt',
        'bonjour/data/terre.txt',
        'bonjourModule.py'
    ]

    expected_distribution_name = 'hello-1.2.3'

    @project_setup_py_test("hello-cpp", ["bdist_wheel"])
    def build_wheel():
        whls = glob.glob('dist/*.whl')
        assert len(whls) == 1
        check_wheel_content(whls[0], expected_distribution_name, expected_content)
        os.remove(whls[0])
        assert not os.path.exists(whls[0])

        assert os.path.exists(os.path.join(CMAKE_BUILD_DIR(), "CMakeCache.txt"))
        os.remove(os.path.join(CMAKE_BUILD_DIR(), "CMakeCache.txt"))

    tmp_dir = build_wheel()[0]

    @project_setup_py_test("hello-cpp", ["--skip-cmake", "bdist_wheel"], tmp_dir=tmp_dir)
    def build_wheel_skip_cmake():
        assert not os.path.exists(os.path.join(CMAKE_BUILD_DIR(), "CMakeCache.txt"))
        whls = glob.glob('dist/*.whl')
        assert len(whls) == 1
        check_wheel_content(whls[0], expected_distribution_name, expected_content)

    build_wheel_skip_cmake()

