@pytest.mark.vcr()
def test_workbench_export_asset_uuid_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.export(asset_uuid='something')
