def test_nbexec(tmp_path: Path) -> None:
    """Run ``nbexec`` to execute a temporary notebook file."""
    for cell in nbexec(make_notebook(tmp_path)).cells:
        if cell.cell_type == "code":
            assert cell.execution_count
            for output in cell.outputs:
                assert output

