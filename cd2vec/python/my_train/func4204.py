def test_masterpassword(self):
        password = "foobar"
        config = InRamConfigurationStore()
        keys = InRamEncryptedKeyStore(config=config)
        self.assertFalse(keys.has_masterpassword())
        master = keys._new_masterpassword(password)
        self.assertEqual(
            len(master),
            len("66eaab244153031e8172e6ffed321" "7288515ddb63646bbefa981a654bdf25b9f"),
        )
        with self.assertRaises(Exception):
            keys._new_masterpassword(master)

        keys.lock()

        with self.assertRaises(Exception):
            keys.change_password("foobar")

        keys.unlock(password)
        self.assertEqual(keys.decrypted_master, master)

        new_pass = "new_secret_password"
        keys.change_password(new_pass)
        keys.lock()
        keys.unlock(new_pass)
        self.assertEqual(keys.decrypted_master, master)
