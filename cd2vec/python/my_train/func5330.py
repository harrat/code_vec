def test_clear_yum_repo(command_tester, fake_collector, monkeypatch):
    """Clearing a repo with yum content succeeds."""

    task_instance = FakeClearRepo()

    repo = YumRepository(
        id="some-yumrepo", relative_url="some/publish/url", mutable_urls=["repomd.xml"]
    )

    files = [
        RpmUnit(
            name="bash",
            version="1.23",
            release="1.test8",
            arch="x86_64",
            sha256sum="a" * 64,
            md5sum="b" * 32,
            signing_key="aabbcc",
        ),
        ModulemdUnit(
            name="mymod", stream="s1", version=123, context="a1c2", arch="s390x"
        ),
    ]

    task_instance.pulp_client_controller.insert_repository(repo)
    task_instance.pulp_client_controller.insert_units(repo, files)

    # Let's try setting the cache flush root via env.
    monkeypatch.setenv("FASTPURGE_ROOT_URL", "https://cdn.example2.com/")

    # It should run with expected output.
    command_tester.test(
        task_instance.main,
        [
            "test-clear-repo",
            "--pulp-url",
            "https://pulp.example.com/",
            "--fastpurge-host",
            "fakehost-xxx.example.net",
            "--fastpurge-client-secret",
            "abcdef",
            "--fastpurge-client-token",
            "efg",
            "--fastpurge-access-token",
            "tok",
            "some-yumrepo",
        ],
    )

    # It should record that it removed these push items:
    assert sorted(fake_collector.items, key=lambda pi: pi["filename"]) == [
        {
            "state": "DELETED",
            "origin": "pulp",
            "filename": "bash-1.23-1.test8.x86_64.rpm",
            "checksums": {"sha256": "a" * 64, "md5": "b" * 32},
            "signing_key": "aabbcc",
        },
        {"state": "DELETED", "origin": "pulp", "filename": "mymod:s1:123:a1c2:s390x"},
    ]

    # It should have flushed these URLs
    assert task_instance.fastpurge_client.purged_urls == [
        "https://cdn.example2.com/some/publish/url/repomd.xml"
    ]
