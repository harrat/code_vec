def test_initinletoutlet():
    """py.test for initinletoutlet"""
    tdata = (
        (
            "PIPE:ADIABATIC",
            "apipe",
            None,
            True,
            ["apipe_Inlet_Node_Name"],
            ["apipe_Outlet_Node_Name"],
        ),
        # idfobjectkey, idfobjname, thisnode, force, inlets, outlets
        ("PIPE:ADIABATIC", "apipe", None, False, ["Gumby"], ["apipe_Outlet_Node_Name"]),
        # idfobjectkey, idfobjname, thisnode, force, inlets, outlets
        (
            "Coil:Cooling:Water",
            "acoil",
            "Water_",
            True,
            ["acoil_Water_Inlet_Node_Name", ""],
            ["acoil_Water_Outlet_Node_Name", ""],
        ),
        # idfobjectkey, idfobjname, thisnode, force, inlets, outlets
    )
    fhandle = StringIO("")
    idf = IDF(fhandle)
    for idfobjectkey, idfobjname, thisnode, force, inlets, outlets in tdata:
        idfobject = idf.newidfobject(idfobjectkey, Name=idfobjname)
        inodefields = hvacbuilder.getfieldnamesendswith(idfobject, "Inlet_Node_Name")
        idfobject[inodefields[0]] = "Gumby"
        hvacbuilder.initinletoutlet(idf, idfobject, thisnode, force=force)
        inodefields = hvacbuilder.getfieldnamesendswith(idfobject, "Inlet_Node_Name")
        for nodefield, inlet in zip(inodefields, inlets):
            result = idfobject[nodefield]
            assert result == inlet
        onodefields = hvacbuilder.getfieldnamesendswith(idfobject, "Outlet_Node_Name")
        for nodefield, outlet in zip(onodefields, outlets):
            result = idfobject[nodefield]
            assert result == outlet

