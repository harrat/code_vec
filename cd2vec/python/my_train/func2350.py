def test_chunk(self):
    """test_chunk ensures the chunk method is splitting responses with more
    than 128 purl results into 128-purl chunks"""
    file = Path(__file__).parent / "condalistoutput.txt"
    with open(file, "r") as stdin:
      purls = self.parse.get_deps_stdin(stdin)
      actual_result = self.func.chunk(purls)
    self.assertEqual(len(actual_result), 3)
    self.assertEqual(len(actual_result[0]), 128)
    self.assertEqual(actual_result[0][0],
                     "pkg:conda/_ipyw_jlab_nb_ext_conf@0.1.0")
    self.assertEqual(actual_result[1][0], "pkg:conda/mistune@0.8.4")
    self.assertEqual(actual_result[2][0], "pkg:conda/yaml@0.1.7")
