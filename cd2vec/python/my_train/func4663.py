def test_start_end_ub(self):
        self.grange.start = 'A1'
        self.grange.end = 'D'
        assert self.grange.start == pygsheets.Address('A', True)
        assert self.grange.end == pygsheets.Address('D', True)
        assert self.grange.label == self.worksheet.title + '!' + 'A' + ':' + 'D'
