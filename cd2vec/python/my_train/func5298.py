def test_clear(self):
        self.worksheet.update_value('S10', 100)
        self.worksheet.clear()
        assert self.worksheet.get_all_values(include_tailing_empty=False, include_tailing_empty_rows=False) == [[]]
