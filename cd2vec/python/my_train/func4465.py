def test_mlb_game_info(self):
        fields = {
            'attendance': 26340,
            'date': 'Monday, July 9, 2018',
            'time': '4:05 p.m. ET',
            'venue': 'Oriole Park at Camden Yards',
            'duration': '2:55',
            'time_of_day': 'Night'
        }

        mock_field = """Monday, July 9, 2018
Start Time: 4:05 p.m. ET
Attendance: 26,340
Venue: Oriole Park at Camden Yards
Game Duration: 2:55
Night Game, on grass
"""

        m = MockBoxscoreData(MockField(mock_field))

        self.boxscore._parse_game_date_and_location(m)
        for field, value in fields.items():
            assert getattr(self.boxscore, field) == value
