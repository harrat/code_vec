def test_annotate(annotate_fxt):
    single_annotate, multiple_annotate = annotate_fxt

    s_ldmk = single_annotate.get_ldmk()
    assert isinstance(s_ldmk, np.ndarray)
    assert np.shape(s_ldmk) == (68, 2)

    m_ldmk = multiple_annotate.get_ldmk()
    assert isinstance(m_ldmk, np.ndarray)
    assert np.shape(m_ldmk) == (5, 68, 2)

@pytest.fixture
def metrics_fxt(annotate_fxt):
    single_annotate, multiple_annotate = annotate_fxt
