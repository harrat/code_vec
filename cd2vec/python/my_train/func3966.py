def test_prepare_polarity(self):
        d = chazutsu.datasets.MovieReview.polarity()
        dataset_root, extracted = d.save_and_extract(DATA_ROOT)
        path = d.prepare(dataset_root, extracted)

        pos = 0
        neg = 0

        try:
            with open(path, encoding="utf-8") as f:
                for ln in f:
                    els = ln.strip().split("\t")
                    if len(els) != 2:
                        raise Exception("data file is not constructed by label and text.")
                    if els[0] == "1":
                        pos += 1
                    else:
                        neg += 1
        except Exception as ex:
            d.clear_trush()
            self.fail(ex)
        count = d.get_line_count(path)
        
        d.clear_trush()
        os.remove(path)
        # pos=1000, neg=1000
        self.assertEqual(count, 2000)
        self.assertEqual(pos, 1000)
        self.assertEqual(neg, 1000)
