def test_create(self):
        """
        Test that a Create request can be processed correctly.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        attribute_factory = factory.AttributeFactory()

        # Build Create request
        object_type = enums.ObjectType.SYMMETRIC_KEY
        template_attribute = objects.TemplateAttribute(
            attributes=[
                attribute_factory.create_attribute(
                    enums.AttributeType.NAME,
                    attributes.Name.create(
                        'Test Symmetric Key',
                        enums.NameType.UNINTERPRETED_TEXT_STRING
                    )
                ),
                attribute_factory.create_attribute(
                    enums.AttributeType.CRYPTOGRAPHIC_ALGORITHM,
                    enums.CryptographicAlgorithm.AES
                ),
                attribute_factory.create_attribute(
                    enums.AttributeType.CRYPTOGRAPHIC_LENGTH,
                    256
                ),
                attribute_factory.create_attribute(
                    enums.AttributeType.CRYPTOGRAPHIC_USAGE_MASK,
                    [
                        enums.CryptographicUsageMask.ENCRYPT,
                        enums.CryptographicUsageMask.DECRYPT
                    ]
                ),
                attribute_factory.create_attribute(
                    enums.AttributeType.OPERATION_POLICY_NAME,
                    'test'
                ),
                attribute_factory.create_attribute(
                    enums.AttributeType.APPLICATION_SPECIFIC_INFORMATION,
                    {
                        "application_namespace": "ssl",
                        "application_data": "www.example.com"
                    }
                ),
                attribute_factory.create_attribute(
                    enums.AttributeType.OBJECT_GROUP,
                    "Group1"
                )
            ]
        )
        payload = payloads.CreateRequestPayload(
            object_type,
            template_attribute
        )

        response_payload = e._process_create(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call(
            "Processing operation: Create"
        )

        uid = response_payload.unique_identifier
        self.assertEqual('1', uid)

        # Retrieve the stored object and verify all attributes were set
        # appropriately.
        symmetric_key = e._data_session.query(
            pie_objects.SymmetricKey
        ).filter(
            pie_objects.ManagedObject.unique_identifier == uid
        ).one()
        self.assertEqual(
            enums.KeyFormatType.RAW,
            symmetric_key.key_format_type
        )
        self.assertEqual(1, len(symmetric_key.names))
        self.assertIn('Test Symmetric Key', symmetric_key.names)
        self.assertEqual(256, len(symmetric_key.value) * 8)
        self.assertEqual(
            enums.CryptographicAlgorithm.AES,
            symmetric_key.cryptographic_algorithm
        )
        self.assertEqual(256, symmetric_key.cryptographic_length)
        self.assertEqual(2, len(symmetric_key.cryptographic_usage_masks))
        self.assertIn(
            enums.CryptographicUsageMask.ENCRYPT,
            symmetric_key.cryptographic_usage_masks
        )
        self.assertIn(
            enums.CryptographicUsageMask.DECRYPT,
            symmetric_key.cryptographic_usage_masks
        )
        self.assertEqual('test', symmetric_key.operation_policy_name)
        self.assertIsNotNone(symmetric_key.initial_date)
        self.assertNotEqual(0, symmetric_key.initial_date)
        self.assertEqual(1, len(symmetric_key.app_specific_info))
        self.assertEqual(
            "ssl",
            symmetric_key.app_specific_info[0].application_namespace
        )
        self.assertEqual(
            "www.example.com",
            symmetric_key.app_specific_info[0].application_data
        )
        self.assertEqual(1, len(symmetric_key.object_groups))
        self.assertEqual("Group1", symmetric_key.object_groups[0].object_group)

        self.assertEqual(uid, e._id_placeholder)
