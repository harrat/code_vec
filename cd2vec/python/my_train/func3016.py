@given(dataframes=n_time_series_with_same_index(min_n=5))
    def test_fit_predict_basic_bottom_up_on_different_data(
        self, dataframes, hierarchical_basic_bottom_up_model
    ):
        hierarchical_basic_bottom_up_model.fit(dataframes).predict(dataframes)
