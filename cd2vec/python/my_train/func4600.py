def test_set_synt_wave_path(self):
        aligner = DTWAligner(synt_wave_path=self.AUDIO_FILE)
        self.assertIsNone(aligner.real_wave_mfcc)
        self.assertIsNotNone(aligner.synt_wave_path)
        self.assertIsNone(aligner.real_wave_path)
        self.assertIsNotNone(aligner.synt_wave_mfcc)
