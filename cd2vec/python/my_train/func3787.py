@skipIf(sys.version_info[:2] <= (3, 3), "Incompatible test")
    def test_truncated_gz(self):
        fmt = get_format('.gz')
        for use_system in (True, False):
            with self.subTest(use_system=use_system):
                path = self.root.make_path()
                gzfile = Path(str(path) + ".gz")
                create_truncated_file(gzfile, fmt)
                with self.assertRaises(IOError):
                    fmt.decompress_file(gzfile, use_system=use_system)
