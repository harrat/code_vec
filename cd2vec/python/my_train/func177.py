@staticmethod
    @pytest.mark.usefixtures('json', 'default-references', 'export-time')
    def test_cli_export_reference_006(snippy):
        """Export all references.

        Export defined reference based on message digest. File name is defined
        in command line as json file.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Reference.REGEXP
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-d', 'cb9225a81eab8ced', '-f', 'defined-reference.json'])
            assert cause == Cause.ALL_OK
            Content.assert_json(json, mock_file, 'defined-reference.json', content)
