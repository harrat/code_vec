def test_transaction_signing_single_input(self, tx1, tx1_extids):
        # Override timestamp to match historical transactions
        tx1._timestamp = "1557879692"

        tx1.sign()

        actual_tx1_extids = tx1._ext_ids

        # add = FactoidPrivateKey(key_string="Fs1KWJrpLdfucvmYwN2nWrwepLn8ercpMbzXshd1g8zyhKXLVLWj")
        # a = FactoidPrivateKey(key_string="Fs2jSmXgaysrqiADPmAvvb71NfAa9MqvXvRemozTE8LRc64hLqtf")

        assert tx1_extids == actual_tx1_extids
