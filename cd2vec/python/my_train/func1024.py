def test_same_output_stage_dir(run_reframe, tmp_path):
    output_dir = str(tmp_path / 'foo')
    returncode, *_ = run_reframe(
        more_options=['-o', output_dir, '-s', output_dir]
    )
    assert returncode == 1

    # Retry with --keep-stage-files
    returncode, *_ = run_reframe(
        more_options=['-o', output_dir, '-s', output_dir, '--keep-stage-files']
    )
    assert returncode == 0
    assert os.path.exists(output_dir)

