def test_save_directory_does_not_exist(self):
        tempdir = bcore._get_temp_path()
        agent = BackendAgentTest.DebugAgent()
        with pytest.raises(Exception):
            agent.save(tempdir, [])
