def test_transaction_signing_multiple_input(self, tx2, tx2_extids):
        # Override timestamp to match historical transactions
        tx2._timestamp = "1571166720"

        tx2.sign()

        actual_tx2_extids = tx2._ext_ids

        # Skip comparing timestamps
        assert tx2_extids == actual_tx2_extids
