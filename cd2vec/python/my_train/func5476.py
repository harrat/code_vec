def test_autodiscover_with_pattern(self):
        path = pathlib.Path(PATH)
        autodiscover = AutoDiscover(path=path, pattern='pattern.py')
        module = 'tests.module_to_import.pattern'
        sys.modules.pop(module)

        autodiscover()

        self.assertIn(module, sys.modules)
