@pytest.mark.vcr()
def test_workbench_export_asset_uuid(api):
    assets = api.workbenches.assets()
    fobj = api.workbenches.export(asset_uuid=assets[0]['id'])
    assert isinstance(fobj, BytesIO)

@pytest.mark.vcr()
def test_workbench_vulns_age_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vulns(age='none')
