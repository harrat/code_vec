@pytest.mark.vcr()
def test_workbench_assets_bad_filter(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.assets(('operating_system', 'contains', 'Linux'))
