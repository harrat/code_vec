def test_ind2sub(self):
        rows, cols = ind2sub(self.mat.shape, self.inds)
        self.assertTrue(np.allclose(self.mat[rows, cols], self.mat.ravel()[self.inds]))
