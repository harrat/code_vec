@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_search_snippet_001(snippy, capsys):
        """Search snippets with ``--sall`` option.

        Search snippets from all resource attributes. A ``data`` attribute
        of a single snippet produces a match.
        """

        output = (
            '1. Remove docker image with force @docker [53908d68425c61dc]',
            '',
            '   $ docker rm --force redis',
            '',
            '   # cleanup,container,docker,docker-ce,moby',
            '   > https://docs.docker.com/engine/reference/commandline/rm/',
            '   > https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--sall', 'redis', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
