def test_importanize_lines(self) -> None:
        result = next(
            run_importanize_on_source(
                self.input_text,
                RuntimeConfig(formatter_name="lines", _config=self.config),
            )
        )

        assert result.organized == self.output_lines.read_text()
