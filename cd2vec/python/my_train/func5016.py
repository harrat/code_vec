def test_symlink_win(tmp_path):
    fn = "test/files/rar5-symlink-win.rar"
    with rarfile.RarFile(fn) as rf:
        assert get_props(rf, "content/dir1") == "-D-"
        assert get_props(rf, "content/dir2") == "-D-"
        assert get_props(rf, "content/file.txt") == "F--"
        assert get_props(rf, "links/bad_link") == "--L"
        assert get_props(rf, "links/dir_junction") == "--L"
        assert get_props(rf, "links/dir_link") == "--L"
        assert get_props(rf, "links/file_link") == "--L"
