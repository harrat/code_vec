def test_train():
    np.random.seed(13)
    torch.manual_seed(13)

    model = train(comet=False, device_options=DeviceOptions(fallback_to_cpu=True),
          training_config=LMTrainingConfig(Corpus(os.path.join(project_dir, 'data', 'dev')),
                                           training=Training(schedule=RafaelsTrainingSchedule(max_epochs=1))))
    assert int(model.metrics.bin_entropy) == 9

