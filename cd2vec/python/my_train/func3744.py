def test_unauthorized():
    notifier = Notifier()

    notice = notifier.notify_sync("hello")

    assert notice["error"] == "Project API key is required"

