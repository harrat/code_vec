def test_tsv(self):
        assert [] == list(read_delimited(Path('foobar'), errors=False))

        path = self.root.make_file()
        with open(path, 'wt') as o:
            o.write('a\tb\tc\n')
            o.write('1\t2\t3\n')
            o.write('4\t5\t6\n')

        with self.assertRaises(ValueError):
            list(read_delimited(path, header=False, converters='int'))
        with self.assertRaises(ValueError):
            list(read_delimited(
                path, header=False, converters=int, row_type='dict',
                yield_header=False))

        assert [
            ['a', 'b', 'c'],
            [1, 2, 3],
            [4, 5, 6]
        ] == list(read_delimited(
            path, header=True, converters=int))
        assert [
            ['a', 'b', 'c'],
            (1, 2, 3),
            (4, 5, 6)
        ] == list(read_delimited(
            path, header=True, converters=int, row_type='tuple'))
        assert [
            ['a', 'b', 'c'],
            (1, 2, 3),
            (4, 5, 6)
        ] == list(read_delimited(
            path, header=True, converters=int, row_type=tuple))
        assert [
            dict(a=1, b=2, c=3),
            dict(a=4, b=5, c=6)
        ] == list(read_delimited(
            path, header=True, converters=int, row_type='dict',
            yield_header=False))
