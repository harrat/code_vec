@staticmethod
    @pytest.mark.usefixtures('yaml')
    def test_cli_import_reference_016(snippy):
        """Import references defaults.

        Import reference defaults. All references should be imported from
        predefined file location under tool data folder from yaml format.
        """

        content = {
            'data': [
                Reference.PYTEST,
                Reference.GITLOG,
            ]
        }
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '--scat', 'reference', '--defaults'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            defaults_references = pkg_resources.resource_filename('snippy', 'data/defaults/references.yaml')
            Content.assert_arglist(mock_file, defaults_references, mode='r', encoding='utf-8')
