def test_simple(self):
        t_steps = 12
        bs = 2
        dim = 16
        v_dim = dim * 2
        s = 2

        layer = LocalizedAttentionLayer1D(kernel_size=3,
                                          strides=s,
                                          num_heads=4,
                                          dilation_rate=2)

        q = to_tensor(normal(size=(bs, t_steps // s, dim))
                      .astype(np.float32))
        k = to_tensor(normal(size=(bs, t_steps, dim))
                      .astype(np.float32))
        v = to_tensor(normal(size=(bs, t_steps, v_dim))
                      .astype(np.float32))

        @tf.function
        def test_func(q, k, v):
            r, _ = layer(q, k, v)
            return r

        r = test_func(q, k=k, v=v)

        ex_res_shape = np.zeros((bs, t_steps // s, v_dim))

        self.assertShapeEqual(ex_res_shape, r)

        _test_grads(self, test_func, [q, k, v])
