def test_version_pep386_dev(self):
        runner('git tag -a 1.0.0 -m "test message"')
        touch('testfile2')
        runner('git add testfile2')
        runner('git commit -m "add testfile2"')
        v = relic.release.get_info()
        assert '.dev' in v.pep386
        assert not v.dirty
        assert int(v.post) > 0
