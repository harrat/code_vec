def test_label_samples_real(testfiles, atlas):
    out = samples_.label_samples(first_entry(testfiles, 'annotation'),
                                 atlas['image'])
    assert isinstance(out, pd.DataFrame)
    assert out.index.name == 'sample_id'
    assert out.columns == ['label']

