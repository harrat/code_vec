def test_add_single_comment(self):
        add_comments('image.fits', "single comment")
        comments = str(fits.getval('image.fits', 'COMMENT')).split('\n')
        assert comments[-1] == "single comment"
