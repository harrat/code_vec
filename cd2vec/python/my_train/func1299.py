@patch(
    'confluent_kafka.avro.CachedSchemaRegistryClient.register',
    MagicMock(return_value=1)
)
@patch(
    'confluent_kafka.avro.CachedSchemaRegistryClient.get_latest_schema',
    MagicMock(return_value=(1, message._schema, 1))
)
@patch(
    'confluent_kafka.avro.CachedSchemaRegistryClient.get_by_id',
    MagicMock(return_value=message._schema)
)
def test_avro_producer_produce(avro_producer):
    key = 'a'
    value = message
    topic = 'z'
    avro_producer.produce(key=key, value=value, topic=topic)

    producer_produce_mock.assert_called_once_with(
        topic,
        key,
        avro_producer.value_serializer.serialize(value, topic)
    )
