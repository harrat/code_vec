def test_batch_matching(rootdir):

    stdin = '\n'.join(['asthma',
                       'Iron-metabolism disorder',
                       'Alzheimer'
                       ])

    runner = CliRunner()
    result = runner.invoke(ontoma, args=['-','-'],input=stdin)
    assert result.exit_code == 0
    assert 'http://www.ebi.ac.uk/efo/EFO_0000270' in result.output

