def test_thermostat_type_4_get_core_heating_days(thermostat_type_4):
    core_heating_day_sets = thermostat_type_4.get_core_heating_days(
            method="year_mid_to_mid")
    assert len(core_heating_day_sets) == 5

