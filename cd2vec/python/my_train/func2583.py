def test_exception_to_http_response_with_global_traceback():
    activate_traceback()
    try:
        raise Problem()
    except Problem as e:
        response = e.to_http_response()
        body = json.loads(response['body'])
        assert "Traceback (most recent call last):" in body['traceback']
        del body['traceback']
        assert body == {}
