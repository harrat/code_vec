@pytest.mark.parametrize('deck_id,card_id,question,answers,examples,other',
                         [(1, 1, 'meaning of life', [42, '42'], ['42dot'],
                           {'key1': 'val1', 'key2': 'val2'}),
                          (1, 2, 'nothing else than 0', [0], ['0', 0.0, 0],
                           {'key3': ['val3', 'val']}),
                          (1, 3, 'just third', [3], ['only 3', 3], {'4': 5}),
                          (1, 4, 'only thing you need to know', [8], ['what'],
                           {'hello': ['gbye']}),
                          (1, 5, 'five.', [5], [5, 5.5], {'key': {'k': 'v'}})])
def test_create_card(deck_id: int, card_id: int, question: str,
                     answers: List[str], examples: List[str],
                     other: dict) -> None:
    deck = spacedr.get_deck_by_id(deck_id)
    spacedr.create_card(deck, question, answers, examples, other)

    card = spacedr.get_card_by_id(card_id)

    assert card.question == question
    assert card.answers == answers
    assert card.examples == examples
    assert card.other == other

