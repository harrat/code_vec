@tests.api_call
    def test_102_cli_proxy(self):
        test_args = ['', '-r', str(tests.PROJECT_ID), '-u', self.mock_users_url]
        with self.assertRaises(Exception):
            with patch.dict('os.environ', {'HTTP_PROXY': "https://myproxy.com"}):
                with patch.object(sys, 'argv', test_args):
                    main()
        self.assertFalse(os.path.isfile('output.csv'))
