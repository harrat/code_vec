def test_invalid_objects_property(self):
        observed_data = copy.deepcopy(self.valid_observed_data)
        observed_data['objects'] = [
            {
                "type": "windows-registry-key",
                "id": "windows-registry-key--ff1e0780-358c-5808-a8c7-d0fca4ef6ef4",
                "key": "HKEY_LOCAL_MACHINE\\SYSTEM\\ControlSet001\\Services\\WSALG2"
            }
            ]
        self.assertFalseWithOptions(observed_data)
