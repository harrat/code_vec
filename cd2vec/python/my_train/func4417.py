def test_poll_before_submit(minimal_job):
    prepare_job(minimal_job, 'sleep 3')
    with pytest.raises(JobNotStartedError):
        minimal_job.finished()
