def test_check_vertical_metrics_regressions(cabin_ttFonts):
    from fontbakery.profiles.shared_conditions import family_directory
    from fontbakery.profiles.googlefonts \
        import (com_google_fonts_check_vertical_metrics_regressions as check,
                api_gfonts_ttFont,
                style,
                remote_styles,
                family_metadata)
    from copy import copy

    family_meta = family_metadata(family_directory(cabin_fonts[0]))
    remote = remote_styles(family_meta.name)
    if remote:
        ttFonts = [TTFont(f) for f in cabin_fonts]
        # Cabin test family should match by default
        assert_PASS(check(ttFonts, remote),
                    'with a good family...')

        ttFonts2 = copy(ttFonts)
        for ttfont in ttFonts2:
            ttfont['OS/2'].sTypoAscender = 0
        assert_results_contain(check(ttFonts2, remote),
                               FAIL, 'bad-typo-ascender',
                               'with a family which has an incorrect typoAscender...')

        # TODO:
        #   FAIL, "bad-typo-descender"
        #   FAIL, "bad-hhea-ascender"
        #   FAIL, "bad-hhea-descender"

        remote2 = copy(remote)
        for key, ttfont in remote2.items():
            ttfont["OS/2"].fsSelection = ttfont["OS/2"].fsSelection ^ 0b10000000
        assert_results_contain(check(ttFonts, remote2),
                               FAIL, None, # FIXME: This needs a message keyword!
                               'with a remote family which does not have'
                               ' typo metrics enabled and the fonts being checked'
                               ' don\'t take this fact into consideration...')

        ttFonts3 = copy(ttFonts)
        for ttfont in ttFonts3:
            ttfont["OS/2"].sTypoAscender = 1139
            ttfont["OS/2"].sTypoDescender = -314
            ttfont["hhea"].ascent = 1139
            ttfont["hhea"].descent = -314
        assert_PASS(check(ttFonts3, remote2),
                    'with a remote family which does not have typo metrics'
                    ' enabled but the checked fonts vertical metrics have been'
                    ' set so its typo and hhea metrics match the remote'
                    ' fonts win metrics.')
    #
    #else:
    #  TODO: There should be a warning message here
