def test_export(self):
        wks_1 = self.spreadsheet.sheet1
        wks_1.update_row(1, ['test', 'test', 'test', 'test'])
        self.spreadsheet.add_worksheet('Test2')
        wks_2 = self.spreadsheet.worksheet('title', 'Test2')
        wks_2.update_row(1, ['test', 'test', 'test', 'test'])

        self.spreadsheet.export(filename='test', path=self.output_path)

        self.spreadsheet.export(file_format=ExportType.XLS, filename='test', path=self.output_path)
        self.spreadsheet.export(file_format=ExportType.HTML, filename='test', path=self.output_path)
        self.spreadsheet.export(file_format=ExportType.ODT, filename='test', path=self.output_path)
        self.spreadsheet.export(file_format=ExportType.PDF, filename='test', path=self.output_path)

        assert os.path.exists('{}/test.pdf'.format(self.output_path))
        assert os.path.exists('{}/test.xls'.format(self.output_path))
        assert os.path.exists('{}/test.odt'.format(self.output_path))
        assert os.path.exists('{}/test.zip'.format(self.output_path))

        self.spreadsheet.export(filename='spreadsheet', path=self.output_path)

        assert os.path.exists('{}/spreadsheet0.csv'.format(self.output_path))
        assert os.path.exists('{}/spreadsheet1.csv'.format(self.output_path))

        self.spreadsheet.export(file_format=ExportType.TSV, filename='tsv_spreadsheet', path=self.output_path)

        assert os.path.exists('{}/tsv_spreadsheet0.tsv'.format(self.output_path))
        assert os.path.exists('{}/tsv_spreadsheet1.tsv'.format(self.output_path))

        wks_1.clear()
        self.spreadsheet.del_worksheet(wks_2)
