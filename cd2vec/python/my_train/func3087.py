def test_max_complexity_is_always_an_int(self):
        """Ensure bug #32 does not regress."""
        class _options(object):
            max_complexity = None

        options = _options()
        options.max_complexity = '16'

        self.assertEqual(0, mccabe.McCabeChecker.max_complexity)
        mccabe.McCabeChecker.parse_options(options)
        self.assertEqual(16, mccabe.McCabeChecker.max_complexity)
