def test_full_report(capsys):
    next_line = ['1999-01-01', '00:00:00', '1999-01-01', '00:00:00', '00:00:00']
    following_line = ['1999-01-02', '00:00:01', '1999-01-02', '00:00:02', '00:00:01']

    with open(path, 'r', newline='') as myFile:
        reader = list(csv.reader(myFile))
        reader.pop()
        reader.pop()
        with open(path, 'w', newline='') as myfile:
            wr = csv.writer(myfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
            for i in reader:
                wr.writerow(i)
            wr.writerow(next_line)
            wr.writerow(following_line)

    full_report("test")
    captured = capsys.readouterr()
    assert captured.out == """[+] Generating Full Report
-------------------------------------------------------------------------
start_date       | start_time | end_date         | end_time | total    | 
-------------------------------------------------------------------------
January  1, 1999 | 12:00 AM   | January  1, 1999 | 12:00 AM | 00:00:00 | 
-------------------------------------------------------------------------
January  2, 1999 | 12:00 AM   | January  2, 1999 | 12:00 AM | 00:00:01 | 
