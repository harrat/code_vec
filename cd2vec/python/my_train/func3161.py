@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_function_with_closure_var(fixture, request):
    result = _runcall(fixture, request, function_tests.fn_withvar, 5.0)
    assert result == 8.0

