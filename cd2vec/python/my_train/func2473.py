def test_train(self):
        agents.seed = 0
        for backend in get_backends(PpoAgent):
            ppo = PpoAgent(gym_env_name=_cartpole_name, backend=backend)
            tc = core.PpoTrainContext()
            tc.num_iterations = 10
            tc.num_episodes_per_iteration = 10
            tc.max_steps_per_episode = 200
            tc.num_epochs_per_iteration = 5
            tc.num_iterations_between_eval = 5
            tc.num_episodes_per_eval = 5
            ppo.train([log.Iteration()], train_context=tc, default_plots=False)
            assert max_avg_rewards(tc) >= 50
