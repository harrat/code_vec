@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_search_snippet_013(snippy, capsys):
        """Search snippets with ``sall`` option.

        Try to search snippets with keyword that cannot be found.
        """

        output = 'NOK: cannot find content with given search criteria\n'
        cause = snippy.run(['snippy', 'search', '--sall', 'not-found', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == 'NOK: cannot find content with given search criteria'
        assert out == output
        assert not err
