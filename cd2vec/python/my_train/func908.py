@staticmethod
    def test_airlines():
        fr = flightradar24.Api()
        airlines = fr.get_airlines()
        assert airlines['rows'] is not None
        assert len(airlines['rows']) > 100  # Expect more than 100 airports
        check_tk = 0
        for airline in airlines['rows']:
            if airline['ICAO'] == "THY":
                check_tk = 1
        assert check_tk == 1
