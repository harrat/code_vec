def test_convenience_need_not_initialized():
    foo = Library('foo', '')
    x1 = Resource(foo, 'a.js')
    x2 = Resource(foo, 'b.css')
    y1 = Resource(foo, 'c.js', depends=[x1, x2])

    dummy = get_needed()
    assert not isinstance(dummy, NeededResources)

    # We return a new dummy instance for every get_needed:
    dummy2 = get_needed()
    assert dummy != dummy2

    # A dummy never has resources:
    assert not dummy.has_resources()

    dummy.need(y1)
    with pytest.raises(NotImplementedError):
        dummy.render()
