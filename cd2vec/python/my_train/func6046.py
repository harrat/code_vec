def test_read_zip_file(zip_file):
    """Test that wkr.open can read zip files."""
    for num in range(1, 4):
        with wkr.open('{zip_file}:file{num}.txt'.format(
                zip_file=zip_file, num=num)) as input_file:
            contents = input_file.read()
            assert isinstance(contents, binary_type)
            contents = contents.split(b'\n')
            assert len(contents) == 2
            assert contents[0] == (u'line {}'.format(num)).encode('ascii')
            assert contents[1] == BINARY_DATA
