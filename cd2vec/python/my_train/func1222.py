def test_to_output_script(self):
        self.assertEqual(
            addr.to_output_script(helpers.OP_IF['p2sh']),
            helpers.OP_IF['output_script'])
        self.assertEqual(
            addr.to_output_script(helpers.P2WSH['human']['ins'][0]['addr']),
            helpers.P2WSH['ser']['ins'][0]['pk_script'])
        self.assertEqual(
            addr.to_output_script(helpers.ADDR[0]['p2pkh']),
            helpers.PK['ser'][0]['pkh_output'])
        self.assertEqual(
            addr.to_output_script(helpers.P2WPKH_ADDR['address']),
            helpers.P2WPKH_ADDR['output'])

        with self.assertRaises(ValueError) as context:
            # Junk B58 w valid checksum
            addr.to_output_script('1111111111111111111111111111111111177fdsQ')

        self.assertIn(
            'Cannot parse output script from address.',
            str(context.exception))
