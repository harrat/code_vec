@pytest.mark.parametrize("value", [0, 1, 2])
def test_update_status(value):
    time = np.array([0])
    STATUS_OBJ._update_status(time)
    assert value in STATUS_OBJ.status

