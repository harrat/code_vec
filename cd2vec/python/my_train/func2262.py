def test_host_deletehostgroup(self, host_load_data):
        host = host_load_data
        with open(resource_dir / 'test_host_hostgroup.json') as hg:
            hgs = HostGroup(json.load(hg))

        data = dict()
        data['action'] = 'delhostgroup'
        data['object'] = 'HOST'
        data['values'] = ["mail-uranus-frontend", "centreon-prod"]

        with patch('requests.post') as patched_post:
            host.deletehostgroup(hgs)
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
