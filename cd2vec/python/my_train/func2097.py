@staticmethod
    def test_cli_create_reference_009(snippy):
        """Try to create reference from CLI.

        Try to create new reference by from command line when the content
        category contains invalid and valid content category. Because of a
        failure to define the content category correctly, content creation
        must fail.
        """

        cause = snippy.run(['snippy', 'create', '--scat', 'reference,failure', '--links', 'http://short', '--no-editor'])
        assert cause == "NOK: content categories ('failure', 'reference') are not a subset of ('snippet', 'solution', 'reference')"
        Content.assert_storage(None)
