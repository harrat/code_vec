def test_coldstarts(handler, mock_context, mock_event):
    ConfigProvider.set(config_names.THUNDRA_APPLICATION_STAGE, 'dev')

    thundra, handler = handler

    invocation_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, InvocationPlugin):
            invocation_plugin = plugin

    handler(mock_event, mock_context)
    assert invocation_plugin.invocation_data['coldStart'] is True
    assert invocation_plugin.invocation_data['tags']['aws.lambda.invocation.coldstart'] is True

    handler(mock_event, mock_context)
    assert invocation_plugin.invocation_data['coldStart'] is False
    assert invocation_plugin.invocation_data['tags']['aws.lambda.invocation.coldstart'] is False

