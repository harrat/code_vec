def test_numeric_intermixed(self, request) -> NoReturn:
        # todo: why do we get different results when shuffling the input?
        unsorted = ['e0lm', 'e1lm', 'E2lm', 'e9lm', 'e10lm', 'E12lm', 'e13lm', 'elm', 'e01lm']
        random.shuffle(unsorted)
        test = ['e0lm', 'e1lm', 'e01lm', 'E2lm', 'e9lm', 'e10lm', 'E12lm', 'e13lm', 'elm']
        test_sorted = naturalsort.naturalsort(unsorted)

        try:
            self.cache['test_numeric_intermixed'].add(tuple(test_sorted))
        except KeyError:
            self.cache['test_numeric_intermixed'] = set()
            self.cache['test_numeric_intermixed'].add(tuple(test_sorted))

        assert test == test_sorted

        if len(self.cache['test_numeric_intermixed']) > 1:
            if NFO:
                logger.info(f'Found {len(self.cache["test_numeric_intermixed"])} sorted variants')
