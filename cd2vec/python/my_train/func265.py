def test_pickle_dumpsloads_nested_dotted():
    orig = udict.fromdict({
        'one': {
            'two': 'one->two'
        },
        'one.two': 'one.two'
    })
    unpickled = pickle.loads(pickle.dumps(orig))
    # assert unpickled == orig
    assert items(unpickled) == items(orig)
    assert isinstance(unpickled, udict)
    assert isinstance(unpickled['one'], udict)

