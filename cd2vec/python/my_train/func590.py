def test_authentication_and_collect(cert_and_key, ip_address):
    """Authenticate call and then collect with the returned orderRef UUID."""

    c = bankid.BankIDJSONClient(certificates=cert_and_key, test_server=True)
    assert "appapi2.test.bankid.com.pem" in c.verify_cert
    out = c.authenticate(ip_address, _get_random_personal_number())
    assert isinstance(out, dict)
    # UUID.__init__ performs the UUID compliance assertion.
    order_ref = uuid.UUID(out.get("orderRef"), version=4)
    collect_status = c.collect(out.get("orderRef"))
    assert collect_status.get("status") == "pending"
    assert collect_status.get("hintCode") in ("outstandingTransaction", "noClient")

