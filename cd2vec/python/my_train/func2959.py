def test_edit_status(cli, config, data_dir):
    efg = EntriesFileGenerator(data_dir, '%m_%Y.txt')
    efg.expand(datetime.date(2014, 1, 21)).write(
        "20/01/2014\nalias_1 2 hello world"
    )
    efg.expand(datetime.date(2014, 2, 21)).write(
        "20/02/2014\nalias_1 2 hello world"
    )
    efg.patch_config(config)

    with freeze_time('2014-01-21'):
        stdout = cli('edit')

    assert 'Monday 20 january' in stdout

