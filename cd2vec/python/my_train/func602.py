@tornado.testing.gen_test
    def test_app_with_user_pkey2fa_with_empty_passcode(self):
        url = self.get_url('/')
        privatekey = read_file(make_tests_data_path('user_rsa_key'))
        self.body_dict.update(username='pkey2fa', password='password',
                              privatekey=privatekey, totp='')
        response = yield self.async_post(url, self.body_dict)
        data = json.loads(to_str(response.body))
        self.assert_status_in('Need a verification code', data)
