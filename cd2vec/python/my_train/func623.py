def test_053_get_success_percentage(self):
        success_percentage = get_success_percentage(tests.PROJECT_INFO)
        self.assertEqual(success_percentage, 70)
