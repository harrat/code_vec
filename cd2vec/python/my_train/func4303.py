@mock.patch("gtsrvd.nginx.call")
    def test_create_proxy(self, call):
        with mock.patch("gtsrvd.nginx.NGINX_CONF_ROOT", self.temp_dir):
            conf_path = nginx.proxy_conf_path(self.subdomain)
            nginx.create_proxy("blastedstudios.com", self.subdomain, 8081)
            self.assertTrue(os.path.exists(conf_path))
