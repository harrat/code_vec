def test_memoize_pandas_save(tmpdir):
    """Test that `wkr.pd.memoize_pandas` saves to CSV."""
    for idx, df in enumerate(dataframe_gen()):
        filename = tmpdir.join('data{}.csv'.format(idx))

        @pandas_memoize(filename.strpath)
        def f():
            return df

        assert not filename.exists()
        df2 = f()
        assert df2.equals(df)
        assert filename.exists()
        df3 = pd.read_csv(filename.strpath,
                          encoding='utf-8', index_col=0)
        assert df3.equals(df)

