def test_broken_is_initialized(self, test_cli_stash):
        assert test_cli_stash.is_initialized is True
        test_cli_stash._storage.delete('stored_passphrase')
        assert test_cli_stash.is_initialized is False
