def test_nonstandard_sections_invalid_url():
    standard_sections = []
    for sect_id in NONSTANDARD_SECTION_IDS:
        test_url = f'{SHORT_DICOM_URL_PREFIX}{sect_id}.2.html'
        status_code = requests.get(test_url).status_code
        if status_code != 404:
            standard_sections.append(sect_id)
    sections_str = ', '.join(standard_sections)
    assert not standard_sections, f'Section(s) {sections_str} have at least one valid subsection URL'

