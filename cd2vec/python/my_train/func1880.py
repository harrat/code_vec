def test_aggregated_setitem_method(mocked_aggregated):
    """Test FlaskMultiRedis aggregated __setitem__ method."""

    mocked_aggregated['name'] = 'node0'
    nodes = mocked_aggregated._aggregator._redis_nodes
    assert [x.name for x in nodes] == ['node0', 'node0', 'node0']
    mocked_aggregated._aggregator._redis_nodes = []
    mocked_aggregated['name'] = 'node0'

