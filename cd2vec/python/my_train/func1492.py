def test_valid_registrations(self):
        """
        Check the current registry (provisioned by registered task holder and
        coroutine) holds the right references.
        """
        # A task holder is registered as a tuple made of (klass, coro_fn)
        register('my-task-holder', 'do_it')(MyTaskHolder)
        klass, coro_fn = TaskRegistry.get('my-task-holder')
        self.assertIs(klass, MyTaskHolder)
        self.assertIs(coro_fn, MyTaskHolder.do_it)
        self.assertEqual(MyTaskHolder.TASK_NAME, 'my-task-holder')

        # A coroutine is registered as a tuple made of (None, coro_fn)
        register('my-coro-task')(my_coro_task)
        klass, coro_fn = TaskRegistry.get('my-coro-task')
        self.assertIs(klass, None)
        self.assertIs(coro_fn, my_coro_task)

        # A coroutine can be registered sevral times with distinct names.
        # Note that an extra parameter passed to register will be ignored.
        register('other-coro-task', 'dummy')(my_coro_task)
        klass, coro_fn = TaskRegistry.get('other-coro-task')
        self.assertIs(klass, None)
        self.assertIs(coro_fn, my_coro_task)
