def test_set_okta_username(monkeypatch):
    """Test whether data sent is the same as data returned."""
    from tokendito import helpers, settings

    monkeypatch.setattr('tokendito.helpers.input', lambda _: 'pytest_patched')
    val = helpers.set_okta_username()

    assert val == 'pytest_patched'
    assert settings.okta_username == 'pytest_patched'

