def test_alias_no_inactive_excludes_inactive_aliases(cli, alias_config):
    stdout = cli('alias', ['list', '--no-inactive'])

    assert 'not started project' not in stdout
    assert 'active project' in stdout

