def test_is_valid_date(self):
        """
        Test that object date checking yields the correct results.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        # If the date range isn't fully defined, the value is implicitly valid.
        self.assertTrue(
            e._is_valid_date(
                enums.AttributeType.INITIAL_DATE,
                1563564520,
                None,
                None
            )
        )
        self.assertTrue(
            e._is_valid_date(
                enums.AttributeType.INITIAL_DATE,
                1563564520,
                None,
                1563564521
            )
        )

        # Verify the value is valid for a fully defined, encompassing range.
        self.assertTrue(
            e._is_valid_date(
                enums.AttributeType.INITIAL_DATE,
                1563564520,
                1563564519,
                1563564521
            )
        )

        # Verify the value is valid for a specific date value.
        self.assertTrue(
            e._is_valid_date(
                enums.AttributeType.INITIAL_DATE,
                1563564520,
                1563564520,
                None
            )
        )

        # Verify the value is invalid for a specific date value.
        self.assertFalse(
            e._is_valid_date(
                enums.AttributeType.INITIAL_DATE,
                1563564520,
                1563564519,
                None
            )
        )
        e._logger.debug.assert_any_call(
            "Failed match: "
            "object's initial date (Fri Jul 19 19:28:40 2019) does not match "
            "the specified initial date (Fri Jul 19 19:28:39 2019)."
        )
        e._logger.reset_mock()

        # Verify the value is invalid below a specific date range.
        self.assertFalse(
            e._is_valid_date(
                enums.AttributeType.INITIAL_DATE,
                1563564519,
                1563564520,
                1563564521
            )
        )
        e._logger.debug.assert_any_call(
            "Failed match: "
            "object's initial date (Fri Jul 19 19:28:39 2019) is less than "
            "the starting initial date (Fri Jul 19 19:28:40 2019)."
        )
        e._logger.reset_mock()

        # Verify the value is invalid above a specific date range.
        self.assertFalse(
            e._is_valid_date(
                enums.AttributeType.INITIAL_DATE,
                1563564521,
                1563564519,
                1563564520
            )
        )
        e._logger.debug.assert_any_call(
            "Failed match: "
            "object's initial date (Fri Jul 19 19:28:41 2019) is greater than "
            "the ending initial date (Fri Jul 19 19:28:40 2019)."
        )
