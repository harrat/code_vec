def test_add_and_remove_alias(self):
        alias1 = utils.generate_alias(type=Alias.DOMAIN)
        alias2 = utils.generate_alias(type=Alias.DOMAIN)
        member = self.client.create_member(alias1)

        member.add_alias(alias2)
        received_aliases = utils.repeated_composite_container_to_list(member.get_aliases())
        expected_aliases = [alias1, alias2]
        assert sorted(received_aliases, key=lambda v: v.value) == sorted(expected_aliases, key=lambda v: v.value)

        member.remove_alias(alias1)
        received_aliases = utils.repeated_composite_container_to_list(member.get_aliases())
        expected_aliases = [alias2]
        assert received_aliases == expected_aliases
