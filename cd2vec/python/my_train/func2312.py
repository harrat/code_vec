def test_invalid_format() -> None:
    reportfilepath = "/tmp/reportfile_{}".format(randint(0, 9999))

    with open(reportfilepath, "w") as f:
        f.write("20/100")

    p = Popen(
        ["python", "-m", "typecov", "100", reportfilepath], stdout=PIPE, stderr=PIPE
    )
    stdout, stderr = p.communicate()

    os.remove(reportfilepath)

    assert p.returncode == 3
    assert stdout.decode() == ""
    assert stderr.decode() == "error: {}: Incorrect format of coverage report.\n".format(
        reportfilepath
    )
