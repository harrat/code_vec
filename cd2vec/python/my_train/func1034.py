def test_via_threading():
    numbers = [1, 2, 3, 4, 5]
    expected_result = [1, 4, 9, 16, 25]

    wrapper = TraceAwareWrapper()
    thread = Thread(target=wrapper(calculate_in_parallel), args=(numbers,))
    thread.start()
    thread.join()

    tracer = ThundraTracer.get_instance()
    nodes = tracer.get_spans()
    active_span = None
    for key in nodes:
        if key.operation_name == 'calculate_in_parallel':
            active_span = key

    assert active_span is not None
    args = active_span.get_tag('method.args')
    assert args[0]['name'] == 'arg-0'
    assert args[0]['value'] == numbers
    assert args[0]['type'] == 'list'

    return_value = active_span.get_tag('method.return_value')
    assert return_value['value'] == expected_result
    assert return_value['type'] == 'list'

    error = active_span.get_tag('error')
    assert error is None

