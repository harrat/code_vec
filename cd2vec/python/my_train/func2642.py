def test_others(self):
        self.assertTrue(
            JalaliDateTime.fromtimestamp(time.time() - 10) <= JalaliDateTime.now()
        )
        self.assertEqual(
            JalaliDateTime(1367, 2, 14, 4, 30, 0, 0, pytz.utc).timestamp(), 578723400
        )
        self.assertEqual(
            JalaliDateTime.fromtimestamp(578723400, pytz.utc),
            JalaliDateTime(1367, 2, 14, 4, 30, 0, 0, pytz.utc),
        )
        self.assertEqual(
            JalaliDateTime(1367, 2, 14, 4, 30, 4, 4444).jdate(), JalaliDate(1367, 2, 14)
        )
        self.assertEqual(
            JalaliDateTime(1367, 2, 14, 4, 30, 4, 4444).date(), date(1988, 5, 4)
        )
        self.assertEqual(
            JalaliDateTime(1367, 2, 14, 4, 30, 0, 0)
            .replace(tzinfo=pytz.utc)
            .__repr__(),
            "JalaliDateTime(1367, 2, 14, 4, 30, tzinfo=<UTC>)",
        )
        self.assertEqual(
            JalaliDateTime(1367, 2, 14, 4, 30, 4, 4444).replace(
                year=1395, day=3, minute=59
            ),
            JalaliDateTime(1395, 2, 3, 4, 59, 4, 4444),
        )

        self.assertEqual(JalaliDateTime.now(pytz.utc).tzname(), "UTC")
        self.assertIsNone(JalaliDateTime.today().tzname())
        self.assertIsNone(JalaliDateTime.today().dst())

        self.assertEqual(
            JalaliDateTime(1367, 2, 14, 4, 30, 0, 0).ctime(),
            "Chaharshanbeh 14 Ordibehesht 1367 04:30:00",
        )
        self.assertEqual(
            JalaliDateTime(1396, 7, 27, 21, 48, 0, 0).ctime(),
            "Panjshanbeh 27 Mehr 1396 21:48:00",
        )
        self.assertEqual(
            JalaliDateTime(1367, 2, 14, 4, 30, 0, 0).replace(locale="fa").ctime(),
            "???????? ?? ???????? ???? ??:??:??",
        )
        self.assertEqual(
            JalaliDateTime(1397, 12, 1, 23, 32, 0, 0).replace(locale="fa").ctime(),
            "???????? ?? ????? ???? ??:??:??",
        )

        self.assertEqual(
            JalaliDateTime(1367, 2, 14, 4, 30, 0, 1, pytz.utc).isoformat(),
            "1367-02-14T04:30:00.000001+00:00",
        )
        self.assertEqual(
            JalaliDateTime(1367, 2, 14, 4, 30, 0, 1, pytz.utc).__str__(),
            "1367-02-14 04:30:00.000001+00:00",
        )
