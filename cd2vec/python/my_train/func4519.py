def test_heatcapacity_airgap_construction(self):
        self.idf.initreadtxt(air_gap)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        expected = m.Thickness * m.Specific_Heat * m.Density * 0.001 * 2
        assert almostequal(c.heatcapacity, expected, places=2)
        assert almostequal(c.heatcapacity, 240, places=2)
