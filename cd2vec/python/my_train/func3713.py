def test_export(self):
        self.worksheet.update_row(1, ['test', 'test', 'test'])
        self.worksheet.export(filename='test', path=self.output_path)
        self.worksheet.export(file_format=ExportType.PDF, filename='test', path=self.output_path)
        self.worksheet.export(file_format=ExportType.XLS, filename='test', path=self.output_path)
        self.worksheet.export(file_format=ExportType.ODT, filename='test', path=self.output_path)
        self.worksheet.export(file_format=ExportType.HTML, filename='test', path=self.output_path)
        self.worksheet.export(file_format=ExportType.TSV, filename='test', path=self.output_path)

        assert os.path.exists(self.output_path + '/test.csv')
        assert os.path.exists(self.output_path + '/test.tsv')
        assert os.path.exists(self.output_path + '/test.xls')
        assert os.path.exists(self.output_path + '/test.odt')
        assert os.path.exists(self.output_path + '/test.zip')

        self.spreadsheet.add_worksheet('test2')
        worksheet_2 = self.spreadsheet.worksheet('title', 'test2')
        worksheet_2.update_row(1, ['test', 'test', 'test', 'test', 'test'])
        worksheet_2.export(file_format=ExportType.CSV, filename='test', path=self.output_path)

        assert os.path.exists(self.output_path + '/test.csv')
        with open(self.output_path + '/test.csv', 'r') as file:
            content = file.read()
            assert 'test,test,test,test,test' == content

        self.worksheet.clear()
        self.spreadsheet.del_worksheet(worksheet_2)
