def test_momentary_chdir(tmpdir):
    """Test wkr.os.momentary_chdir."""
    start_dir = os.getcwd()
    with momentary_chdir(tmpdir.strpath):
        new_dir = os.getcwd()
        assert new_dir != start_dir
        assert new_dir == tmpdir.strpath
    assert os.getcwd() == start_dir

