@pytest.mark.parametrize('tablestr, context, outtable,exception', [
		(Raw('whatevertable'), None, 'whatevertable', None),
		('s.t', None, '"s"."t"', None),
		('s.t', "insert", '"s"."t"', None),
		('s.t', "update", '"s"."t"', None),
		('s.t', "delete", '"s"."t"', None),
		('s.t', "selectinto", '"s"."t"', None),
		('s.t(t1)', 'from', '"s"."t" AS "t1"', None),
		('t(t1)', 'from', '"t" AS "t1"', None),
		('t', 'from', '"t"', None),
		('[]t', 'from', None, TableParseError),
		('t', 'unknown', None, TableParseError),
	])
	def testParse(self, tablestr, context, outtable, exception):
		if exception:
			with pytest.raises(exception):
				Table.parse(tablestr, context)
		else:
			assert Table.parse(tablestr, context) == outtable
