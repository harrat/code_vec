def test_delete_attribute_from_managed_object_bad_attribute_index(self):
        """
        Test that an ItemNotFound error is raised when attempting to delete
        an attribute by index that cannot be found on a managed object.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        managed_object = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )
        args = (
            managed_object,
            (
                "Object Group",
                3,
                None
            )
        )
        self.assertRaisesRegex(
            exceptions.ItemNotFound,
            "Could not locate the attribute instance with the specified "
            "index: 3",
            e._delete_attribute_from_managed_object,
            *args
        )
