def test_eq_ne_hash():
    RTestUtil.test_eq_ne_hash(RNotNoneTypeMatcher(), RNotNoneTypeMatcher(), is_equal=True)
    RTestUtil.test_eq_ne_hash(RNotNoneTypeMatcher(), INT | RNotNoneTypeMatcher(), is_equal=False)

