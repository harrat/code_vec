@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_persistent_and_nonpersistent_mixture(fixture, request):
    # References only persisted in objects that request it
    testobj = MyRefObj()
    testobj.o = MyRefObj()
    testobj.o.o = testobj
    testobj.should_persist = MyRefObj()
    testobj._PERSIST_REFERENCES = False
    testobj.o._PERSIST_REFERENCES = False

    engine = request.getfixturevalue(fixture)
    pycall = pyccc.PythonCall(testobj.identity)

    job = engine.launch(PYIMAGE, pycall, interpreter=PYVERSION, persist_references=True)
    print(job.rundata)
    job.wait()
    result = job.result
    assert result is not testobj
    assert result.o is not testobj.o
    assert result.o.o is result
    assert result.should_persist is testobj.should_persist

