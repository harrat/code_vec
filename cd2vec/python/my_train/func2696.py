@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_directory_input(fixture, request):
    engine = request.getfixturevalue(fixture)

    job = engine.launch(image='alpine', command='cat data/a data/b',
                        inputs={'data':
                                    pyccc.LocalDirectoryReference(os.path.join(THISDIR, 'data'))})
    print(job.rundata)
    job.wait()
    assert job.exitcode == 0
    assert job.stdout.strip() == 'a\nb'

