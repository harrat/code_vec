def test_array_size():
    global cu
    cu.arraysize = 2
    cu.execute("delete from tests")
    cu.execute("insert into tests(id, name, text_field) values (15, 'test_array_size', 'A')")
    cu.execute("insert into tests(id, name, text_field) values (16, 'test_array_size', 'B')")
    cu.execute("insert into tests(id, name, text_field) values (17, 'test_array_size', 'C')")
    cu.execute("select text_field from tests")
    res = cu.fetchmany()
    assert len(res) == 2
    res = cu.fetchmany()
    assert len(res) == 1

