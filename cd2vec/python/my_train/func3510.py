@pytest.mark.last
def test_resp_log(client):
    resp = client.get('/resp-log/')

    assert resp.status_code == 200
    assert type(resp.get_json().get('data')) is list

