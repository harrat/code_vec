def test_log_kv():
    tracer = ThundraTracer.get_instance()
    with tracer.start_active_span(operation_name='operation name', finish_on_close=True) as scope:
        span = scope.span
        assert len(span.logs) == 0
