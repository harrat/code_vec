@pytest.mark.vcr()
def test_workbench_asset_vuln_info_uuid_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.asset_vuln_info(1, 1)
