def test_from_import_as(index):
    src = dedent('''
        from clastic import MiddleWare as WebMiddleWare
        ''').strip()
    scope = Scope.from_source(src)
    new_src = update_imports(src, index, *scope.find_unresolved_and_unreferenced_symbols())
    assert src == new_src.strip()

