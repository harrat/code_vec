def test_load_counter_3(tmpdir):
    """Test the wkr.io.load_counter method."""
    # write the counter out to file
    filename = tmpdir.join('counts.tsv').ensure().strpath
    with open(filename, 'wb') as output_file:
        output_file.write(b'2\ta\tb\n')
        output_file.write(b'4\tb\tb\n')
        output_file.write(b'1\tc\tb\n')
        output_file.write(b'3\ta\ta\n')
    # read it back in
    assert wkr.io.load_counter(filename) == Counter(dict([
        (('a', 'b'), 2),
        (('b', 'b'), 4),
        (('c', 'b'), 1),
        (('a', 'a'), 3),
    ]))
