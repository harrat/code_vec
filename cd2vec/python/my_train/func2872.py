@all_modes()
def test_display_name(d, mode):
    kern = install(d, "%s --display-name=AAA MOD1"%mode)
    assert kern['kernel']['display_name'] == 'AAA'

@all_modes(['conda'])
def test_template(d, mode):
    os.environ['JUPYTER_PATH'] = pjoin(d, 'share/jupyter')
    subprocess.call("python -m ipykernel install --name=aaa-ipy --display-name=BBB --prefix=%s"%d, shell=True)
    #os.environ['ENVKERNEL_TESTPATH'] = os.path.join(d, 'share/jupyter/kernels')
    kern = install(d, "%s --kernel-template aaa-ipy MOD1"%mode)
    assert kern['kernel']['display_name'] == 'BBB'
