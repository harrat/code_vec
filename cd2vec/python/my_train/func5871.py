def test_rowcount_executemany():
    cu.execute("delete from tests")
    cu.executemany("insert into tests(id, name) values (?, ?)", [
        (1, 'test_rowcount_executemany'),
        (2, 'test_rowcount_executemany'),
        (3, 'test_rowcount_executemany')
    ])
    assert cu.rowcount == 3

