def test_inventory(self):
        self.xm.session.inventory()
        print('+++')
        for c_name, chassis in self.xm.session.chassis_list.items():
            print(c_name)
            for m_name, module in chassis.modules.items():
                print('\tmodule {}'.format(m_name))
                for p_name, port in module.ports.items():
                    print('\t\tport {}'.format(p_name))
                    for s_name, _ in port.streams.items():
                        print('\t\t\tstream {}'.format(s_name))
        print('+++')

        save_config = path.join(path.dirname(__file__), 'configs', 'save_config.xmc')
        list(self.xm.session.chassis_list.values())[0].save_config(save_config)
