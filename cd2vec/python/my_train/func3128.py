def test_host_addcontactgroupt(self, host_load_data):
        host = host_load_data
        with open(resource_dir / 'test_host_contactgroup.json') as cg:
            cgs = ContactGroup(json.load(cg))

        data = dict()
        data['action'] = 'addcontactgroup'
        data['object'] = 'HOST'
        data['values'] = ["mail-uranus-frontend", "astreinte"]

        with patch('requests.post') as patched_post:
            host.addcontactgroup(cgs)
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
