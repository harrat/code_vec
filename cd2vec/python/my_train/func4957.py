def test_covariant_covariance_calc(self):
        data1 = np.random.multivariate_normal([0, 0], [[1, 0], [0, 1]], size=10000)
        data2 = np.random.multivariate_normal([0, 0], [[2, 1], [1, 2]], size=10000)
        weights = np.concatenate((np.ones(10000), np.zeros(10000)))
        data = np.concatenate((data1, data2))
        c = ChainConsumer()
        c.add_chain(data, weights=weights, parameters=["x", "y"])
        p, cor = c.analysis.get_covariance()
        assert p[0] == "x"
        assert p[1] == "y"
        assert np.isclose(cor[0, 0], 1, atol=4e-2)
        assert np.isclose(cor[1, 1], 1, atol=4e-2)
        assert np.isclose(cor[0, 1], 0, atol=4e-2)
        assert cor.shape == (2, 2)
