def test_get_ct_log_name(self):
        self.assertEqual(
            "Google 'Argon2018' log",
            cert_info.get_ct_log_name(
                "a4501269055a15545e6211ab37bc103f62ae5576a45e4b1714453e1b22106a25"
            ),
        )
