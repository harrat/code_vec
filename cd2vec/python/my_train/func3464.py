@freeze_time('2014-01-21')
def test_today_only_shows_todays_entries(cli, entries_file):
    entries_file.write("""20/01/2014
alias_1 1 Invisible entry

21/01/2014
alias_1 1 Visible entry

22/01/2014
alias_1 1 Invisible entry

23/01/2014
alias_1 1 Invisible entry
""")
    stdout = cli('status', ['--today'])

    assert 'Invisible entry' not in stdout
    assert 'Visible entry' in stdout

