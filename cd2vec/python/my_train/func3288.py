def test_show_durations(capsys, show_fixture):
    conf = show_fixture

    tools.run_asv_with_conf(conf, 'show', '--machine=cheetah', '--durations')
    text, err = capsys.readouterr()
    assert '13dd6571547f8dd87b24c4e29536d33cc4f335c9  6.00s' in text.strip()

    tools.run_asv_with_conf(conf, 'show', '13dd6571', '--machine=cheetah',
                            '--durations')
    text, err = capsys.readouterr()
    expected = textwrap.dedent("""
    Commit: 13dd6571

    Machine    : cheetah
    Environment: py2.7-Cython-numpy1.8

        <setup_cache example:21>  3.00s
        <build>  2.00s
        time_quantity.time_quantity_array_conversion  1.00s

        total duration: 6.00s
    """)
    assert text.strip() == expected.strip()

