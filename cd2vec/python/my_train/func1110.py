def test_external_reference_no_external_id(self):
        attack_pattern = copy.deepcopy(self.valid_attack_pattern)
        ext_refs = attack_pattern['external_references']
        del ext_refs[0]['external_id']
        results = validate_parsed_json(attack_pattern, self.options)
        self.assertEqual(results.is_valid, False)
