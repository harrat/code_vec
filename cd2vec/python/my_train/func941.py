def test_descriptor(self):
        with self.assertRaises(ValueError):
            TempPathDescriptor(path_type='d', contents='foo')
        with self.assertRaises(IOError):
            _ = TempPathDescriptor().absolute_path
        with TempDir(permissions='rwx') as temp:
            f = temp.make_file(name='foo', permissions=None)
            f.unlink()
            assert temp[f].set_permissions('r') is None
        with TempDir(permissions='rwx') as temp:
            f = temp.make_file(name='foo', permissions=None)
            assert Path('foo') in temp
            assert temp[f].exists
            assert Path('foo') == temp[f].relative_path
            assert temp.absolute_path / 'foo' == temp[f].absolute_path
            assert PermissionSet('rwx') == temp[f].permissions
            assert PermissionSet('r') == temp[f].set_permissions('r')
            with self.assertRaises(PermissionError):
                open(f, 'w')
        with TempDir(permissions='rwx') as temp:
            desc = TempPathDescriptor(
                name='foo', path_type='f', parent=temp)
            assert Path('foo') == desc.relative_path
            assert temp.absolute_path / 'foo' == desc.absolute_path
