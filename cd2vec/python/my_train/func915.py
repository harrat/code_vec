def test_report(handler_with_profile, mock_context, mock_event):
    thundra, handler = handler_with_profile

    invocation_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, InvocationPlugin):
            invocation_plugin = plugin

    handler(mock_event, mock_context)

    assert invocation_plugin.invocation_data['startTimestamp'] is not None
    assert invocation_plugin.invocation_data['finishTimestamp'] is not None

    start_time = invocation_plugin.invocation_data['startTimestamp']
    end_time = invocation_plugin.invocation_data['finishTimestamp']

    duration = int(end_time - start_time)

    assert invocation_plugin.invocation_data['duration'] == duration
    assert invocation_plugin.invocation_data['erroneous'] is False
    assert invocation_plugin.invocation_data['errorType'] == ''
    assert invocation_plugin.invocation_data['errorMessage'] == ''

    assert invocation_plugin.invocation_data['functionRegion'] == 'region'

