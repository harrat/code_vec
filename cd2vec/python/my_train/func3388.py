def test_translation():
    en = 'china'
    fr = 'chine'
    translator = Translator.translator(src='en', dest='fr')
    out = translator.translate(en)
    assert out.lower() == fr

