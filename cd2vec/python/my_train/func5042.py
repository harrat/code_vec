def test_submit(make_job, exec_ctx):
    minimal_job = make_job(sched_access=exec_ctx.access)
    prepare_job(minimal_job)
    assert minimal_job.nodelist is None
    minimal_job.submit()
    assert minimal_job.jobid is not None
    minimal_job.wait()

    # Additional scheduler-specific checks
    sched_name = minimal_job.scheduler.registered_name
    if sched_name == 'local':
        assert [socket.gethostname()] == minimal_job.nodelist
        assert 0 == minimal_job.exitcode
    elif sched_name == ('slurm', 'squeue', 'pbs', 'torque'):
        num_tasks_per_node = minimal_job.num_tasks_per_node or 1
        num_nodes = minimal_job.num_tasks // num_tasks_per_node
        assert num_nodes == len(minimal_job.nodelist)
        assert 0 == minimal_job.exitcode
