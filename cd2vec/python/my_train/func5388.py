@pytest.mark.parametrize(
        "date,period,url",
        [
            (None, None, "/stock/AAPL/sentiment/daily"),
            ("20190101", None, "/stock/AAPL/sentiment/daily/20190101"),
            ("20190101", "minute", "/stock/AAPL/sentiment/minute/20190101"),
        ],
    )
    def test_social_url(self, date, period, url):
        obj = SocialSentiment("AAPL", date=date, period_type=period)

        assert obj.url == url
