def test_temp_file_name(tmpdir, tmpdir_suffix):
    """Basic test of wkr.os.temp_file_name."""
    # directory is empty
    assert not tmpdir.listdir()
    with temp_file_name(suffix=tmpdir_suffix,
                        directory=tmpdir.strpath) as newpath:
        assert newpath.endswith(tmpdir_suffix)
        # either the directory is empty and the file is not there, or
        # it is there and the directory is not empty, but the file is
        # empty
        assert ((not tmpdir.listdir() and not os.path.exists(newpath)) or (
            tmpdir.listdir() and
            os.path.exists(newpath) and
            not os.stat(newpath).st_size))
        # we should be able to write to the file
        output_file = open(newpath, 'wb')
        output_file.write(b'abcde')
        output_file.close()
        # now the file should be there
        assert tmpdir.listdir()
        assert os.path.exists(newpath)
        assert os.stat(newpath).st_size > 0
    # directory is empty
    assert not tmpdir.listdir()

