def test_command_line_args_img(sphinx_app):
    generated_examples_dir = op.join(sphinx_app.outdir, 'auto_examples')
    thumb_fname = '../_images/sphx_glr_plot_command_line_args_thumb.png'
    file_fname = op.join(generated_examples_dir, thumb_fname)
    assert op.isfile(file_fname), file_fname

