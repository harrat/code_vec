def test_replacebranch():
    """py.test for replacebranch"""
    tdata = (
        (
            "p_loop",
            ["sb0", ["sb1", "sb2", "sb3"], "sb4"],
            ["db0", ["db1", "db2", "db3"], "db4"],
            "sb0",
            [
                ("Chiller:Electric", "Central_Chiller", "Chilled_Water_"),
                ("PIPE:ADIABATIC", "np1", None),
                ("PIPE:ADIABATIC", "np2", None),
            ],
            "Water",
            [
                "BRANCH",
                "sb0",
                0.0,
                "",
                "CHILLER:ELECTRIC",
                "Central_Chiller",
                "p_loop Supply Inlet",
                "Central_Chiller_np1_node",
                "",
                "PIPE:ADIABATIC",
                "np1",
                "Central_Chiller_np1_node",
                "np1_np2_node",
                "",
                "PIPE:ADIABATIC",
                "np2",
                "np1_np2_node",
                "np2_Outlet_Node_Name",
                "",
            ],
        ),  # loopname, sloop, dloop, branchname, componenttuple, fluid, outbranch
    )
    for (loopname, sloop, dloop, branchname, componenttuple, fluid, outbranch) in tdata:
        fhandle = StringIO("")
        idf = IDF(fhandle)
        loop = hvacbuilder.makeplantloop(idf, loopname, sloop, dloop)
        components_thisnodes = [
            (idf.newidfobject(key, Name=nm), thisnode)
            for key, nm, thisnode in componenttuple
        ]
        branch = idf.getobject("BRANCH", branchname)
        newbr = hvacbuilder.replacebranch(
            idf, loop, branch, components_thisnodes, fluid=fluid
        )
        assert newbr.obj == outbranch

