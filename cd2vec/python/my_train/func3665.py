@staticmethod
    @pytest.mark.usefixtures('edit-solution-template')
    def test_cli_create_solution_003(snippy):
        """Try to create solution from CLI.

        Try to create new solution without any changes to template.
        """

        cause = snippy.run(['snippy', 'create', '--scat', 'solution', '--format', 'text'])
        assert cause == 'NOK: content was not stored because it was matching to an empty template'
        Content.assert_storage(None)
