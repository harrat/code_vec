@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'caller')
    def test_api_delete_snippet_003(server):
        """Try to delete snippet.

        Try to send DELETE /snippets without ``id`` in URI that identifies
        the delete resource.
        """

        content = {
            'data': [
                Storage.remove,
                Storage.forced
            ]
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '368'
        }
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '404',
                'statusString': '404 Not Found',
                'module': 'snippy.testing.testing:123',
                'title': 'cannot delete content without identified resource'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_delete(
            path='/api/snippy/rest/snippets',
            headers={'accept': 'application/vnd.api+json'})
        assert result.status == falcon.HTTP_404
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(content)
