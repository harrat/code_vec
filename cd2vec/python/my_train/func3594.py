@all_modes()
def test_ipykernel(d, mode):
    kern = install(d, "%s --kernel=ipykernel MOD1"%mode)
    assert kern['kernel']['language'] == 'python'
    assert kern['k'][0] == 'python'
    assert kern['k'][1:4] == ['-m', 'ipykernel_launcher', '-f']

@all_modes()
def test_ir(d, mode):
    kern = install(d, "%s --kernel=ir MOD1"%mode)
    assert kern['kernel']['language'] == 'R'
    assert kern['k'][0] == 'R'
    assert kern['k'][1:5] == ['--slave', '-e', 'IRkernel::main()', '--args']
