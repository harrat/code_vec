def test_prepare_no_exclusive(make_job, slurm_only):
    job = make_job(sched_exclusive_access=False)
    prepare_job(job)
    with open(job.script_filename) as fp:
        assert re.search(r'--exclusive', fp.read()) is None
