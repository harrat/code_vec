def test_process_with_files(self):
        inp = self.root.make_file(suffix='.gz')
        with gzip.open(inp, 'wt') as o:
            o.write('foo')
        out = self.root.make_file(suffix='.gz')
        with self.assertRaises(OSError):
            with gzip.open(inp, 'rt') as o, open(out, 'wt') as i:
                with Process('cat', stdin=o, stdout=i) as p:
                    p.wrap_pipes(stdin=dict(mode='wt'))
        with gzip.open(out, 'rt') as i:
            assert 'foo' == i.read()
        with popen(('echo', 'abc\n123'), stdout=PIPE) as p:
            self.assertListEqual(
                [b'abc\n', b'123\n'],
                list(line for line in p))
        with popen(('echo', 'abc\n123'), stdout=PIPE) as p:
            assert b'abc\n' == next(p)
            assert b'123\n' == next(p)
        with popen(('echo', 'abc\n123'), stdout=(PIPE, 'rt')) as p:
            assert 'abc\n' == next(p)
            assert '123\n' == next(p)
