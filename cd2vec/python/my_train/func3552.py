@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_delete_reference_011(snippy):
        """Delete reference with data.

        Try to delete reference with empty content data. Nothing should be
        deleted in this case because there is more than one content left.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--content', ''])
        assert cause == 'NOK: cannot use empty content data for delete operation'
        Content.assert_storage(content)
