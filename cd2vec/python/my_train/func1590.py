@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'yaml', 'import-beats', 'import-kafka-utc')
    def test_cli_import_solution_010(snippy):
        """Import all solution resources.

        Import solutions from yaml file when all but one of the solutions in
        the file is already stored. Because one solution was stored
        successfully, the return cause is OK.

        The UUID is modified to avoid the UUID collision which produces error.
        The test verifies that user modified resource attributes do not stop
        importing multiple resources.
        """

        content = {
            'data': [
                Solution.KAFKA,
                Content.deepcopy(Solution.BEATS)
            ]
        }
        content['data'][1]['uuid'] = Content.UUID1
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '--file', './all-solutions.yaml'])
            assert cause == Cause.ALL_OK
            content['data'][1]['uuid'] = Solution.BEATS_UUID
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './all-solutions.yaml', mode='r', encoding='utf-8')
