def test_loads_each_split(self):
        params = [(WikiText2, 36_718, 3_760, 4_358),
                  (WikiText103, 1_801_350, 3_760, 4_358)]
        for dataset, train_size, dev_size, test_size in params:
            with self.subTest(dataset=dataset, train_size=train_size,
                              dev_size=dev_size, test_size=test_size):
                train = dataset(split='train')
                self.assertEqual(len(train), train_size)
                dev = dataset(split='dev')
                self.assertEqual(len(dev), dev_size)
                test = dataset(split='test')
                self.assertEqual(len(test), test_size)
