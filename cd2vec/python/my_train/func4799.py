def test_items(test_setting, test_key, actual_value):
    for key, val in test_setting.items():
        assert key == test_key
        assert val == actual_value

