@mock.patch("yunomi.core.metrics_registry.time")
    def test_time_calls_decorator(self, time_mock):
        time_mock.return_value = 0.0
        @time_calls
        def test():
            time_mock.return_value += 1.0

        for i in xrange(10):
            test()
        _timer = timer("test_calls")
        snapshot = _timer.get_snapshot()
        self.assertEqual(_timer.get_count(), 10)
        self.assertEqual(_timer.get_max(), 1)
        self.assertEqual(_timer.get_min(), 1)
        self.assertAlmostEqual(_timer.get_std_dev(), 0)
        self.assertAlmostEqual(snapshot.get_75th_percentile(), 1.0)
        self.assertAlmostEqual(snapshot.get_98th_percentile(), 1.0)
        self.assertAlmostEqual(snapshot.get_99th_percentile(), 1.0)
        self.assertAlmostEqual(snapshot.get_999th_percentile(), 1.0)
