def test_extract_to(self):
        poiolib.wikipedia.extract_to("cre", self.tmp_dir)
        self.assertEqual(
            len(glob.glob(os.path.join(self.tmp_dir, "crwiki*.xml.bz2"))), 1
        )
        self.assertTrue(os.path.isfile(os.path.join(self.tmp_dir, "AA", "wiki_00")))
