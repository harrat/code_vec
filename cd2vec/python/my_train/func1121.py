def test_delete_not_exist(self):
        """
        Tests helpers.File.delete for the case that the file does
        not exists.
        """

        filename = "this_file_is_a_ghost"

        expected = False
        actual = path.isfile(filename)

        self.assertEqual(expected, actual)

        File(filename).delete()

        expected = False
        actual = path.isfile(filename)

        self.assertEqual(expected, actual)
