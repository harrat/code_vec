def test_forward_algorithm(self, hidden_markov_model):
        observations = np.random.choice(np.arange(10), size=10)
        hidden_markov_model.forward_algorithm(observations)
        probability = np.sum(hidden_markov_model.alpha[-1, :])
        assert 0 < probability <= 1
