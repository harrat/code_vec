def test_convert_filepaths(monkeypatch):
    folders = [
        ('/home/ubuntu/Programming/code',
         '/Users/Jake/Programming/code'),
        ('/vagrant/myproject',
         '/Users/Jake/Programming/vagrant/myproject'),
    ]
    filepaths = [
        '/home/ubuntu/Programming/code/project1/file.py',
        '/home/ubuntu/privatedir/password.txt',
        '/vagrant/myproject/awesome.py'
    ]
    expected = [
        '/Users/Jake/Programming/code/project1/file.py',
        '/Users/Jake/Programming/vagrant/myproject/awesome.py'
    ]

    converted, invalid = client.convert_filepaths(filepaths, folders)

    assert converted == expected

@pytest.mark.parametrize('data, expected', [
    (['-p', '3800', '--log', 'critial', 'file.py', 'folder/awesome.txt'],
        (50, 3800, ['file.py', 'folder/awesome.txt'])),
    (['file1.py', 'file2.py', 'file3.py'],
        (30, arg_handler.default_port, ['file1.py', 'file2.py', 'file3.py']))
])
def test_parse_client(data, expected):
    # level, port, files
    results = arg_handler.parse_client(data)
    assert results == expected
