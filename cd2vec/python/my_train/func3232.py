@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_update_solution_010(snippy):
        """Update solution with ``--content`` option.

        Try to update solution with empty content data. Nothing must be
        updated in this case because there is more than one content stored.
        """

        content = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        cause = snippy.run(['snippy', 'update', '--scat', 'solution', '-c', ''])
        assert cause == 'NOK: cannot use empty content data for update operation'
        Content.assert_storage(content)
