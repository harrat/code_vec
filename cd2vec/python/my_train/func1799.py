@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'import-kafka', 'import-pytest')
    def test_api_search_groups_002(server):
        """Get unique content based on ``groups`` attribute.

        Send GET /groups to get unique groups only from solution category.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '69'
        }
        expect_body = {
            'data': {
                'type': 'groups',
                'attributes': {
                    'groups': {
                        'docker': 1
                    }
                }
            }
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/groups',
            headers={'accept': 'application/vnd.api+json'},
            query_string='scat=solution')
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
