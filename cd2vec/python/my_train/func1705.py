@staticmethod
    @pytest.mark.usefixtures('yaml', 'default-snippets')
    def test_cli_import_snippet_019(snippy):
        """Import snippet based on digest.

        Try to import snippet based on message digest that matches to two
        other snippets.

        Note! Don't change the test snippets because this case is produced
        with real digests that just happen to have same digit starting both
        of the cases.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '-d', '5', '-f', 'one-snippet.yaml'])
            assert cause == 'NOK: content digest 5 matched 2 times preventing import operation'
            Content.assert_storage(content)
            mock_file.assert_not_called()
            yaml.safe_load.assert_not_called()
