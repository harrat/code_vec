@mock_ssm
def test_it_returns_the_default_value(key, value):
    assert ParamStore.delete(key)
    default_value = 'a-default-value'
    assert ParamStore.get(key, default=default_value).value == default_value

