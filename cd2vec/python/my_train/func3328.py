def test_load_audio_file(self):
        af = AudioFile(gf.absolute_path(self.AUDIO_FILE_WAVE, __file__))
        af.read_samples_from_file()
        audiofile = AudioFileMFCC(audio_file=af)
        self.assertIsNotNone(audiofile.all_mfcc)
        self.assertAlmostEqual(audiofile.audio_length, TimeValue("53.3"), places=1)     # 53.266
