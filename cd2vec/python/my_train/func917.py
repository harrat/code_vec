def test_request_signature(self):
        member1 = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        member2 = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        payload = AccessTokenBuilder.create_with_alias(member2.get_first_alias()).for_all().build()
        token = member1.create_access_token(payload)
        signature = member1.sign_token_request_state(generate_nonce(), token.id, generate_nonce())
        assert signature.signature
