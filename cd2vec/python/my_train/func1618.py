def test_check_path_std(self):
        check_path(STDIN_OR_STDOUT, 'f', 'r')
        check_path(STDIN_OR_STDOUT, 'f', 'w')
        check_path(STDIN, 'f', 'r')
        check_path(STDOUT, 'f', 'w')
        check_path(STDERR, 'f', 'w')
        with self.assertRaises(IOError):
            check_path(STDIN, 'f', 'w')
        with self.assertRaises(IOError):
            check_path(STDOUT, 'f', 'r')
        with self.assertRaises(IOError):
            check_path(STDERR, 'f', 'r')
        with self.assertRaises(IOError):
            check_path(STDOUT, 'd', 'r')
