@pytest.mark.parametrize("distribution_type", ('pure', 'skbuild'))
def test_py_modules_keyword(distribution_type, capsys):

    # -------------------------------------------------------------------------
    #
    # "SOURCE" tree layout for "pure" distribution:
    #
    # ROOT/
    #     setup.py
    #     foo.py
    #     bar.py
    #
    # "SOURCE" tree layout for "skbuild" distribution:
    #
    # ROOT/
    #     setup.py
    #     CMakeLists.txt
    #
    # -------------------------------------------------------------------------
    # "BINARY" distribution layout is identical for both
    #
    # ROOT/
    #     foo.py
    #     bar.py
    #

    tmp_dir = _tmpdir('py_modules_keyword')

    tmp_dir.join('setup.py').write(textwrap.dedent(
        """
        from skbuild import setup
        setup(
            name="test_py_modules_keyword",
            version="1.2.3",
            description="a package testing use of py_modules keyword",
            author='The scikit-build team',
            license="MIT",
            py_modules=['foo', 'bar']
        )
        """
    ))

    if distribution_type == 'skbuild':
        tmp_dir.join('CMakeLists.txt').write(textwrap.dedent(
            """
            cmake_minimum_required(VERSION 3.5.0)
            project(foobar NONE)
            file(WRITE "${CMAKE_BINARY_DIR}/foo.py" "# foo.py")
            file(WRITE "${CMAKE_BINARY_DIR}/bar.py" "# bar.py")
            install(
                FILES
                    "${CMAKE_BINARY_DIR}/foo.py"
                    "${CMAKE_BINARY_DIR}/bar.py"
                DESTINATION "."
                )
            """
        ))

        messages = [
            "copying {}/{}.py -> "
            "{}/setuptools/lib".format(CMAKE_INSTALL_DIR(), module, SKBUILD_DIR())
            for module in ['foo', 'bar']]

    elif distribution_type == 'pure':
        tmp_dir.join('foo.py').write("# foo.py")
        tmp_dir.join('bar.py').write("# bar.py")

        messages = [
            "copying {}.py -> "
            "{}/setuptools/lib".format(module, SKBUILD_DIR())
            for module in ['foo', 'bar']]

    with execute_setup_py(tmp_dir, ['build'], disable_languages_test=True):
        pass

    out, _ = capsys.readouterr()
    for message in messages:
        assert to_platform_path(message) in out

