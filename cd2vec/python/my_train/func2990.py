def test_invalid_connection(self):
        self.assertRaises(
            FilmAffinityConnectionError,
            self.service._load_url, "http://notworking.tz",
            headers={'User-Agent': 'Mozilla/5.0'}, verify=True,
            timeout=1, force_server_response=True)
