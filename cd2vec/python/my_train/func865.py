def test__select_model(self):
        self.re = RoleExtractor(n_role_range=(2, 5), n_bit_range=(2, 5))
        model = self.re._select_model(self.features)
        G, F = model
        expected_n_roles = 2  # model selection selects 2 for the generated data
        n_roles = G.shape[1]
        self.assertEqual(G.shape[1], F.shape[0])
        self.assertEqual(n_roles, expected_n_roles)
