def test_biz_data(self):
        fact = BizFactory('step_function')
        fact.__sfn_client__ = Mock()
        fact.__sfn_arn__ = "arn"

        fact.create('every1', Every(5, Every.MINUTES))
        fact({'detail-type': 'Scheduled Event',
              'resources': ['arn:aws:events:eu-west-1:123456789:rule/step_function-every1']}, {})
        fact.sfn_client.start_execution.assert_called_once_with(input='{}', stateMachineArn='arn')
        fact.create('every2', Every(10, Every.HOURS), data={'key': 'value'})
        fact({'detail-type': 'Scheduled Event',
              'resources': ['arn:aws:events:eu-west-1:123456789:rule/step_function-every2']}, {})
        fact.sfn_client.start_execution.assert_called_with(input='{"key": "value"}', stateMachineArn='arn')

        assert len(fact.trigger_sources) == 2
        assert [t['name'] for t in fact.trigger_sources] == ['step_function-every1', 'step_function-every2']
        assert [t['value'] for t in fact.trigger_sources] == ['rate(5 minutes)', 'rate(10 hours)']
