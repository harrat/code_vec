@pytest.mark.parametrize('path', ['.', None])
def test_opera_driver_manager_with_correct_version_and_token(path):
    driver_path = OperaDriverManager(version="v.2.45", path=path).install()
    assert os.path.exists(driver_path)

