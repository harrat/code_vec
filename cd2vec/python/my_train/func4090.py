@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_delete_reference_009(snippy):
        """Delete reference with data.

        Try to delete reference with content data that does not exist. In this
        case the content data is not truncated.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--content', 'not found content'])
        assert cause == 'NOK: cannot find content with content data: not found content'
        Content.assert_storage(content)
