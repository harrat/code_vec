@mock.patch('epsagon.triggers.http.HTTPTriggerFactory')
@mock.patch('epsagon.runners.django.DjangoRunner.update_response')
@mock.patch('time.time', return_value=1)
def test_after_request(
    _, runner_mock, trigger_mock, test_request, trace_transport
):
    """
    Test the whole  flow - a trace is generated with  the
    right content.
    """
    request_mw = DjangoRequestMiddleware(test_request)

    request_mw.before_request()
    request_mw.after_request({"content": "bla"})

    runner_mock.assert_called_once()
    trigger_mock.factory.assert_called_once()

    assert trace_transport.last_trace.events[
        0].resource['metadata']['Request Data'] == TEST_BODY
    assert trace_transport.last_trace.events[
        0].resource['metadata']['Path'] == TEST_PATH
