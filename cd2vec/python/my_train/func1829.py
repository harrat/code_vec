def test_collect_controllers():
    controllers = app.collect_controllers()
    assert controllers == [{'empty_controller': 'fixtures.lambda_app.functions.empty_controller.handler'}, {'fake_lambda_proxy_controller': 'fixtures.lambda_app.functions.fake_lambda_proxy_controller.handler'}]

