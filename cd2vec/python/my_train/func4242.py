def test_create(self):
        self.stub_request('post', TestCreateResource.RESOURCE_PATH,
                          {'resource': 'checkout', 'foo': 'bar', })
        TestCreateResource._api_client = self.client
        api_obj = TestCreateResource.create()
        self.assert_requested('post', TestCreateResource.RESOURCE_PATH, data={})
        self.assertEqual('bar', api_obj.foo)
        self.assertIsInstance(api_obj, Checkout)
