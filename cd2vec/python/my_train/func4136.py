@staticmethod
    @pytest.mark.usefixtures('snippy')
    def test_cli_export_solution_030(snippy):
        """Export solution defaults.

        Try to export solution defaults when there are no stored solutions.
        Files should not be created and proper NOK cause should be printed
        for end user.
        """

        with mock.patch('snippy.content.migrate.io.open', mock.mock_open(), create=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '--defaults'])
            assert cause == 'NOK: no content found to be exported'
            mock_file.assert_not_called()
