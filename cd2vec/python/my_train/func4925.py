@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_delete_solution_012(snippy):
        """Delete solution with data.

        Try to delete solution with empty content data. Nothing should be
        deleted in this case because there is more than ne content left.
        """

        content = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--scat', 'solution', '--content', ''])
        assert cause == 'NOK: cannot use empty content data for delete operation'
        Content.assert_storage(content)
