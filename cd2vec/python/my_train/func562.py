def test_length(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE, rs=True)
        audiofile.clear_data()
        self.assertAlmostEqual(audiofile.audio_length, TimeValue("53.3"), places=1)     # 53.266
