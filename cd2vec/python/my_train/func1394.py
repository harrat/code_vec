def test_ask_skopt():
    s_opt = SkoptOptimizer()
    n_samples = 9
    s_opt.initialize(['a', 'b', 'c'], a=[0, 1], b=[0, 2], c=[0, 3],
                     popsize=n_samples, rounds=2)

    params = s_opt.ask(n_samples)
    assert_equal(np.shape(params), (n_samples, 3))

    for i in np.arange(0, 3):
        assert all(np.array(params)[:, i] <= i+1), 'Values in params are bigger than required'
        assert all(np.array(params)[:, i] >= 0), 'Values in params are smaller than required'

