def test_dir():
    global dir
    a = aud.Dir(dir)
    a.config_set_extensions(["wav"])

    a.config_set_log_file("mock/test.log")
    assert a.log("TESTING LOG")

    assert sorted(a.get_all()) == ["bloop.wav", "song.wav"]

    assert a.get_single(0) == "bloop.wav"  # doesn't work because its not sorted

    assert a.backup(join(dir, "backup"))
    assert sorted(a.get_all()) == ["bloop.wav", "song.wav"]
    assert sorted(listdir(join(dir, "backup"))) == ["bloop.wav", "song.wav"]

    assert a.copy(join(dir, "copy"))
    assert sorted(a.get_all()) == ["bloop.wav", "song.wav"]
    assert sorted(listdir(join(dir, "copy"))) == ["bloop.wav", "song.wav"]

    assert a.move(join(dir, "move"))
    assert sorted(a.get_all()) == ["bloop.wav", "song.wav"]
    assert sorted(listdir(join(dir, "move"))) == ["bloop.wav", "song.wav"]

    assert a.move(dir)
    assert sorted(a.get_all()) == ["bloop.wav", "song.wav"]
    assert sorted(listdir(dir)) == [
        "abc.txt",
        "backup",
        "bloop.wav",
        "copy",
        "move",
        "song.wav",
        "test.log",
        "test.txt",
    ]

    assert a.zip("mock/test.zip")
    assert isfile("mock/test.zip")
    with zipfile.ZipFile("mock/test.zip") as file:
        assert sorted(file.namelist()) == ["bloop.wav", "song.wav"]
