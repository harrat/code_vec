def test_build_deps_unknown_source_env(loader, exec_ctx):
    checks = loader.load_all()

    # Add some inexistent dependencies
    test0 = find_check('Test0', checks)
    test1 = find_check('Test1_default', checks)
    test1.depends_on('Test0', rfm.DEPEND_EXACT, {'eX': ['e0']})

    # Unknown source is ignored, because it might simply be that the test
    # is not executed for eX
    deps = dependency.build_deps(executors.generate_testcases(checks))
    assert num_deps(deps, 'Test1_default') == 4

