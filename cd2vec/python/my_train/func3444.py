@pytest.mark.parametrize('deck_id,card_id,question,answers,examples,other',
                         [(1, 1, 'nothing else than 0', [0], ['4'], {'n': 5}),
                          (1, 2, 'life meaning', [42, '42'], [2], {'l': 'f'})])
def test_edit_card(deck_id: int, card_id: int, question: str,
                   answers: List[str], examples: List[str],
                   other: dict) -> None:
    card = spacedr.get_card_by_id(card_id)
    spacedr.edit_card(card, deck_id=deck_id, question=question,
                      answers=answers, examples=examples, other=other)

    assert card.deck_id == deck_id
    assert card.question == question
    assert card.answers == answers
    assert card.examples == examples
    assert card.other == other

