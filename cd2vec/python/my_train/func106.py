def test_medida_si_3():
    m = Medida("1+-0.1", "ft� deg lb h �F A mol^-1").SI()

    assert unidades_em_texto(m.unidades_originais) == "m� rad kg s K A mol?�"

def test_medida_sub_1():
    m1 = Medida(1, "m")
    m2 = Medida(2, "m")
    m = m1-m2
    assert m.nominal == -1
    assert m.incerteza == 0
