def test_process_operation(self):
        """
        Test that the right subroutine is called when invoking operations
        supported by the server.
        """
        e = engine.KmipEngine()
        e._logger = mock.MagicMock()

        # TODO (peterhamilton) Alphatize these.
        e._process_create = mock.MagicMock()
        e._process_create_key_pair = mock.MagicMock()
        e._process_delete_attribute = mock.MagicMock()
        e._process_derive_key = mock.MagicMock()
        e._process_register = mock.MagicMock()
        e._process_locate = mock.MagicMock()
        e._process_get = mock.MagicMock()
        e._process_get_attributes = mock.MagicMock()
        e._process_get_attribute_list = mock.MagicMock()
        e._process_activate = mock.MagicMock()
        e._process_revoke = mock.MagicMock()
        e._process_destroy = mock.MagicMock()
        e._process_query = mock.MagicMock()
        e._process_discover_versions = mock.MagicMock()
        e._process_encrypt = mock.MagicMock()
        e._process_decrypt = mock.MagicMock()
        e._process_signature_verify = mock.MagicMock()
        e._process_mac = mock.MagicMock()
        e._process_sign = mock.MagicMock()
        e._process_set_attribute = mock.MagicMock()
        e._process_modify_attribute = mock.MagicMock()

        e._process_operation(enums.Operation.CREATE, None)
        e._process_operation(enums.Operation.CREATE_KEY_PAIR, None)
        e._process_operation(enums.Operation.DELETE_ATTRIBUTE, None)
        e._process_operation(enums.Operation.DERIVE_KEY, None)
        e._process_operation(enums.Operation.REGISTER, None)
        e._process_operation(enums.Operation.LOCATE, None)
        e._process_operation(enums.Operation.GET, None)
        e._process_operation(enums.Operation.GET_ATTRIBUTES, None)
        e._process_operation(enums.Operation.GET_ATTRIBUTE_LIST, None)
        e._process_operation(enums.Operation.ACTIVATE, None)
        e._process_operation(enums.Operation.REVOKE, None)
        e._process_operation(enums.Operation.DESTROY, None)
        e._process_operation(enums.Operation.QUERY, None)
        e._process_operation(enums.Operation.DISCOVER_VERSIONS, None)
        e._process_operation(enums.Operation.ENCRYPT, None)
        e._process_operation(enums.Operation.DECRYPT, None)
        e._process_operation(enums.Operation.SIGN, None)
        e._process_operation(enums.Operation.SIGNATURE_VERIFY, None)
        e._process_operation(enums.Operation.MAC, None)
        e._process_operation(enums.Operation.SET_ATTRIBUTE, None)
        e._process_operation(enums.Operation.MODIFY_ATTRIBUTE, None)

        e._process_create.assert_called_with(None)
        e._process_create_key_pair.assert_called_with(None)
        e._process_delete_attribute.assert_called_with(None)
        e._process_derive_key.assert_called_with(None)
        e._process_register.assert_called_with(None)
        e._process_locate.assert_called_with(None)
        e._process_get.assert_called_with(None)
        e._process_get_attributes.assert_called_with(None)
        e._process_get_attribute_list.assert_called_with(None)
        e._process_activate.assert_called_with(None)
        e._process_revoke.assert_called_with(None)
        e._process_destroy.assert_called_with(None)
        e._process_query.assert_called_with(None)
        e._process_discover_versions.assert_called_with(None)
        e._process_encrypt.assert_called_with(None)
        e._process_decrypt.assert_called_with(None)
        e._process_signature_verify.assert_called_with(None)
        e._process_mac.assert_called_with(None)
        e._process_set_attribute.assert_called_with(None)
        e._process_modify_attribute.assert_called_with(None)
