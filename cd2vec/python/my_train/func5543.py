def test_performance_var_evaluation(dummytest, sanity_file,
                                    perf_file, dummy_gpu_exec_ctx):
    # All performance values must be evaluated, despite the first one
    # failing To test this, we need an extract function that will have a
    # side effect when evaluated, whose result we will check after calling
    # `check_performance()`.
    logfile = 'perf.log'

    @sn.sanity_function
    def extract_perf(patt, tag):
        val = sn.evaluate(
            sn.extractsingle(patt, perf_file, tag, float)
        )
        with open('perf.log', 'a') as fp:
            fp.write('%s=%s' % (tag, val))

        return val

    sanity_file.write_text('result = success\n')
    perf_file.write_text('perf1 = 1.0\n'
                         'perf2 = 1.8\n'
                         'perf3 = 3.3\n')
    dummytest.perf_patterns = {
        'value1': extract_perf(r'perf1 = (?P<v1>\S+)', 'v1'),
        'value2': extract_perf(r'perf2 = (?P<v2>\S+)', 'v2'),
        'value3': extract_perf(r'perf3 = (?P<v3>\S+)', 'v3')
    }
    with pytest.raises(PerformanceError) as cm:
        _run_sanity(dummytest, *dummy_gpu_exec_ctx)

    logfile = os.path.join(dummytest.stagedir, logfile)
    with open(logfile) as fp:
        log_output = fp.read()

    assert 'v1' in log_output
    assert 'v2' in log_output
    assert 'v3' in log_output

