def test_iter_stream(self):
        progress = MockProgress()
        xphyle.configure(progress=True, progress_wrapper=progress)
        with intercept_stdin('foo\nbar\nbaz'):
            with xopen(
                    STDIN, 'rt', context_wrapper=True, compression=False) as o:
                lines = list(o)
                self.assertListEqual(['foo\n', 'bar\n', 'baz\n'], lines)
        assert 3 == progress.count
