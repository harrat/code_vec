@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'json', 'import-remove', 'update-remove-utc')
    def test_cli_import_snippet_011(snippy):
        """Import defined snippet.

        Import defined snippet based on message digest. File name is defined
        from command line as json file which contain one snippet. Content
        brief were updated.
        """

        content = {
            'data': [
                Content.deepcopy(Snippet.REMOVE)
            ]
        }
        content['data'][0]['brief'] = 'Updated brief description'
        content['data'][0]['digest'] = 'f07547e7c692741ac5f142853899383ea0398558ffcce7c033adb8b0e12ffda5'
        file_content = Content.get_file_content(Content.JSON, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            json.load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '-d', '54e41e9b52a02b63', '-f', 'one-snippet.json'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-snippet.json', mode='r', encoding='utf-8')
