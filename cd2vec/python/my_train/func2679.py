def test_observable_object_embedded_property_reference(self):
        observed_data = copy.deepcopy(self.valid_observed_data)
        observed_data['objects']['0']['extensions']['archive-ext']['contains_refs'][0] = '999'
        self.assertFalseWithOptions(observed_data)

        observed_data['objects']['2'] = {
          "type": "directory",
          "path": "C:\\Windows\\System32",
          "contains_refs": ['0']
        }
        observed_data['objects']['0']['extensions']['archive-ext']['contains_refs'][0] = '2'
        self.assertFalseWithOptions(observed_data)
