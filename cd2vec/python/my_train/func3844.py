@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'yaml', 'import-remove', 'update-remove-utc')
    def test_cli_import_snippet_009(snippy):
        """Import defined snippet.

        Import defined snippet based on message digest. File name is defined
        from command line as yaml file which contain one snippet. Content is
        not updated in this case because the same content is imported again.
        """

        content = {
            'data': [
                Snippet.REMOVE
            ]
        }
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '-d', '54e41e9b52a02b63', '-f', 'one-snippet.yml'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-snippet.yml', mode='r', encoding='utf-8')
