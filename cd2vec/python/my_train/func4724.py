def test_rule_with_fix(self):
        cli_args = _create_arg_parser().parse_args(["--fix", "test_rule_with_fix"])
        reload(config)
        config.REPORTER = MemoryReporter
        linter = Linter(cli_args, config)
        lines, _ = linter.lint_lines("test_rule_with_fix", self.TEST_INPUT)

        self.assertEqual(lines, self.FIXED_INPUT)
