def test_check_banner_old_invalid(self):
        res = apache_httpd.check_banner(
            "Apache/1.1.7", "<raw-request-data>", "http://adamcaudill.com"
        )

        self.assertEqual(2, len(res))
        self.assertEqual("Apache Server Version Exposed: Apache/1.1.7", res[0].message)
        self.assertIn("Apache Server Outdated:", res[1].message)
