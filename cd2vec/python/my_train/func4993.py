@mock.patch("IPython.display.display")
def test_scraps_report_with_notebook_names(mock_display, notebook_collection):
    notebook_collection.scraps_report(notebook_names="result1")
    mock_display.assert_has_calls(
        [
            mock.call(AnyMarkdownWith("### result1")),
            mock.call(AnyMarkdownWith("#### output")),
            mock.call({"text/plain": "'Hello World!'"}, metadata={}, raw=True),
            mock.call(AnyMarkdownWith("#### one_only")),
            mock.call({"text/plain": "'Just here!'"}, metadata={}, raw=True),
        ]
    )
