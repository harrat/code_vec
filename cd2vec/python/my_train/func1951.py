def test_all(self):
        data = {
            'post_script': [
                "echo " + self.first_out,
                "echo " + self.second_out,
            ],
            'pre_script': [
                "echo " + self.first_out,
                "echo " + self.second_out,
            ],
            'remote': 'origin',
            'branch': 'master'
        }

        with open(self.filename, 'w') as f:
            json.dump(data, f)

        chain = CommandChain.load_from_config(self.filename)
        chain.commands[0].execute()
        chain.commands[1].execute()
        chain.commands[3].execute()
        chain.commands[4].execute()
        self.assertEqual(len(chain.commands), 5)
        self.assertEqual(chain.commands[2].cmd, 'git pull origin master')
        self.assertIn(self.first_out, chain.commands[0].out)
        self.assertIn(self.second_out, chain.commands[1].out)
        self.assertIn(self.first_out, chain.commands[3].out)
        self.assertIn(self.second_out, chain.commands[4].out)
