def test_fit(self):
        prior = .5
        x, y, x_t, y_t = gen_twonorm_pu(n_p=30, n_u=200, 
                                        prior_u=prior, n_t=100)
        pu_sl = pu_mr.PU_SL(prior, basis='lm')
        pu_sl.fit(x, y)
        y_h = pu_sl.predict(x_t)
        err = bin_clf_err(y_h, y_t, prior)
        self.assertLess(err, .2)
