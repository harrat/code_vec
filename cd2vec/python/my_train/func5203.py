def test_selector_invalid_index(self):
        observed_data = copy.deepcopy(self.valid_observed_data)
        observed_data['granular_markings'][0]['selectors'] = [
            "objects.0.extensions.archive-ext.contains_refs.[5]"
        ]
        self.assertFalseWithOptions(observed_data)
