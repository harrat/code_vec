def test_get_details(empty_person: Person):
    details = empty_person.get_details()
    assert isinstance(details, dict)
    assert empty_person.data == details

