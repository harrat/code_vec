def test_check_readable_file(self):
        readable = self.root.make_file(permissions='r')
        non_readable = self.root.make_file(permissions='w')
        directory = self.root.make_directory()
        check_readable_file(readable)
        with self.assertRaises(IOError):
            check_readable_file(non_readable)
        with self.assertRaises(IOError):
            check_readable_file(Path('foo'))
        with self.assertRaises(IOError):
            check_readable_file(directory)
        assert safe_check_readable_file(readable)
        assert safe_check_readable_file(non_readable) is None
