def test_network_traffic_required_fields(self):
        net_traffic = copy.deepcopy(self.valid_net_traffic)
        del net_traffic['src_ref']
        self.assertFalseWithOptions(net_traffic)

        net_traffic = copy.deepcopy(self.valid_net_traffic)
        del net_traffic['protocols']
        self.assertFalseWithOptions(net_traffic)
