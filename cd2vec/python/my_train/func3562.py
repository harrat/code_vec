def test_flex_alloc_nodes_zero(make_flexible_job):
    job = make_flexible_job(0, sched_access=['--constraint=f1'])
    with pytest.raises(JobError):
        prepare_job(job)
