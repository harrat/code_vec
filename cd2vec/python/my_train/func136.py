def test_get_url_file_name(self):
        with TempDir() as temp:
            path = abspath(temp.make_file(name='foo.txt'))
            url = open_url(path.as_uri())
            assert get_url_file_name(url) == str(path)
        # TODO: need to find a reliable compressed file URL with a
        # Content-Disposition, or figure out how to mock one up
