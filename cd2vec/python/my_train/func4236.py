def test_read_config_from_custom_location(clean):
    os.environ['PDIR2_CONFIG_FILE'] = os.path.join(os.path.expanduser('~'), '.myconfig')
    shutil.copyfile('tests/data/config_1.ini', os.environ['PDIR2_CONFIG_FILE'])
    from pdir.format import doc_color, category_color, attribute_color, comma

    assert doc_color == COLORS['white']
    assert category_color == COLORS['bright yellow']
    assert comma == '\033[1;32m, \033[0m'
    assert attribute_color == COLORS['cyan']

