def test_single_view(dispatcher, view):
    j = Jackfruit(dispatcher, view)

    assert len(j.STATE) == 1
    assert j.STATE[view.get_name()] == view

    assert len(dispatcher.HANDLERS) == 2
    for h in dispatcher.HANDLERS:
        if isinstance(h, CallbackQueryHandler):
            assert h.callback == j._dispatch
        elif isinstance(h, MessageHandler):
            assert h.callback == j._dispatch
        else:
            assert False
