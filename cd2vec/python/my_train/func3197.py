def test_heatcapacity_2_layer_construction(self):
        self.idf.initreadtxt(double_layer)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        expected = m.Thickness * m.Specific_Heat * m.Density * 0.001 * 2
        assert c.heatcapacity == expected
        assert c.heatcapacity == 240
