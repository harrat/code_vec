def test_check_ssl_redirect_path(self):
        self.assertEqual(
            "https://mail.google.com/",
            network.check_ssl_redirect("http://mail.google.com/"),
        )
