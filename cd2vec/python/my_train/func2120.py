def test_ingredient_fractions_serialization(valid_ingredient_fractions_predictor_data):
    """"Ensure that a serialized IngredientsFractionsPredictor looks sane."""
    predictor = IngredientFractionsPredictor.build(valid_ingredient_fractions_predictor_data)
    serialized = predictor.dump()
    serialized["id"] = valid_ingredient_fractions_predictor_data['id']
    assert serialized == valid_serialization_output(valid_ingredient_fractions_predictor_data)

