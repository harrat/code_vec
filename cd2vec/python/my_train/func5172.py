def test_events(self):
        global before_save_called
        global before_delete_called

        class ChildModel(Model):
            name = 'PRODUTO'
            schema = {
                'id': StringField(required=True)
            }

        stream = PanamahStream('auth', 'secret', '123')
        before_save_called = False
        before_delete_called = False

        class TestEvents:
            def before_save(self, model, prevent_save):
                global before_save_called
                before_save_called = True
                prevent_save()

            def before_delete(self, model, prevent_delete):
                global before_delete_called
                before_delete_called = True
                prevent_delete()
                
        test_events = TestEvents()

        stream.on('before_save', test_events.before_save)
        stream.on('before_delete', test_events.before_delete)

        with mock.patch.object(stream.processor, 'save') as processor_save_method:
            processor_save_method.return_value = True
            model = ChildModel(id='1')
