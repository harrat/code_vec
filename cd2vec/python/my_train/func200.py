def tests_patch___new__():
    """Test _patch___new__"""
    # Check if patched
    assert BUILD_EXT_NEW is not build_ext.__new__

    # Check build_ext instantiation
    from distutils.dist import Distribution

    build_ext(Distribution())
    assert GET_EXT_FULLNAME is not build_ext.get_ext_fullname
    assert GET_EXT_FILENAME is not build_ext.get_ext_filename
    assert BUILD_EXTENSION is not build_ext.build_extension
    assert GET_OUTPUTS is not build_ext.get_outputs

