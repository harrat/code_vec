@responses.activate
def test_refresh(client):
    """Test refresh updates the access_token, expires_in, and token_expiration.

    :param Client client: Client instance with test data.
    """
    responses.add(responses.POST,
                  '%s/oauth/token' % settings.API_BASE_URL,
                  body='{"access_token": "tail", "refresh_token": "wagging", "expires_in": 3600}',
                  status=200,
                  content_type='application/json'
                  )
    old_token_expiration = client.auth.token_expiration
    client.refresh_authorization()
    assert client.auth.access_token == 'tail'
    assert client.auth.token_expiration > old_token_expiration

