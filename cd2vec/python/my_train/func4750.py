@pytest.mark.vcr()
def test_scan_export_bytesio(api):
    from io import BytesIO
    from tenable.reports.nessusv2 import NessusReportv2
    fobj = api.scans.export(SCAN_ID_WITH_RESULTS)
    assert isinstance(fobj, BytesIO)

    counter = 0
    for i in NessusReportv2(fobj):
        counter += 1
        if counter > 10:
            break

@pytest.mark.vcr()
def test_scan_export_file_object(api):
    from tenable.reports.nessusv2 import NessusReportv2
    fn = '{}.nessus'.format(uuid.uuid4())
    with open(fn, 'wb') as fobj:
        api.scans.export(SCAN_ID_WITH_RESULTS, fobj=fobj)
