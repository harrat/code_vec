def test_revoke_not_allowed_by_policy(self):
        """
        Test that an unallowed request is handled correctly by Revoke.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=False)
        e._logger = mock.MagicMock()
        e._client_identity = 'test'

        obj_a = pie_objects.OpaqueObject(b'', enums.OpaqueDataType.NONE)
        obj_a._owner = 'admin'

        e._data_session.add(obj_a)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        id_a = str(obj_a.unique_identifier)

        reason_unspecified = objects.RevocationReason(
            code=enums.RevocationReasonCode.UNSPECIFIED)
        date = primitives.DateTime(
            tag=enums.Tags.COMPROMISE_OCCURRENCE_DATE, value=6)

        payload = payloads.RevokeRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(id_a),
            revocation_reason=reason_unspecified,
            compromise_occurrence_date=date)

        args = [payload]
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            "Could not locate object: {0}".format(id_a),
            e._process_revoke,
            *args
        )
