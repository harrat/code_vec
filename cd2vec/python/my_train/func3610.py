@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_search_snippet_036(snippy, capsys):
        """Search snippets with ``filter`` option.

        Try to search snippets defining filter but not any search criteria.
        In this case the filter cannot be applied because no search criteria
        is applied.
        """

        output = 'NOK: please define keyword, uuid, digest or content data as search criteria\n'
        cause = snippy.run(['snippy', 'search', '--filter', '.*(\\$\\s.*)'])
        out, err = capsys.readouterr()
        assert cause == 'NOK: please define keyword, uuid, digest or content data as search criteria'
        assert out == output
        assert not err
