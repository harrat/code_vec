@pytest.mark.core
def testConfigSetWinSize(nwTemp,nwRef):
    tmpConf = path.join(nwTemp,"novelwriter.conf")
    refConf = path.join(nwRef, "novelwriter.conf")
    assert theConf.setWinSize(1105, 655)
    assert not theConf.confChanged
    assert theConf.setWinSize(70,70)
    assert theConf.confChanged
    assert theConf.setWinSize(1100, 650)
    assert theConf.saveConfig()
    assert cmpFiles(tmpConf, refConf, [2])
    assert not theConf.confChanged

@pytest.mark.core
def testConfigSetTreeColWidths(nwTemp,nwRef):
    tmpConf = path.join(nwTemp,"novelwriter.conf")
    refConf = path.join(nwRef, "novelwriter.conf")
    assert theConf.setTreeColWidths([0, 0, 0])
    assert theConf.confChanged
    assert theConf.setTreeColWidths([120, 30, 50])
    assert theConf.setProjColWidths([140, 55, 140])
    assert theConf.saveConfig()
    assert cmpFiles(tmpConf, refConf, [2])
    assert not theConf.confChanged
