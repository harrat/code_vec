def test_run(monkeypatch, tmpdir):
    zmq_instance = mock.ZMQContextInstance()
    monkeypatch.setattr(zmq.Context, "instance", zmq_instance)
    monkeypatch.setattr(zmq, "Poller", mock.ZMQPoller)

    from reactobus.db import DB, Message

    dbname = tmpdir.join("testing.sqlite3")
    db_url = "sqlite:///%s" % dbname
    db = DB({"url": db_url}, "inproc://test_run")
    with pytest.raises(IndexError):
        db.run()
    sub = zmq_instance.socks[zmq.SUB]
    assert len(sub.recv) == 0
    assert sub.connected is True
    assert sub.opts == {zmq.SUBSCRIBE: b""}

    # Test that wrong message will not make the process crash
    sub.recv = [[]]
    with pytest.raises(IndexError):
        db.run()
    assert len(sub.recv) == 0

    # Check that the db is empty
    session = db.sessions()
    assert session.query(Message).count() == 0

    # Test that wrong message will not make the process crash
    sub.recv = [
        [
            "org.reactobus.1",
            str(uuid.uuid1()),
            datetime.datetime.utcnow().isoformat(),
            "lavaserver",
            json.dumps({}),
        ],
        [
            "org.reactobus.2",
            str(uuid.uuid1()),
            datetime.datetime.utcnow().isoformat(),
            "lavaserver",
            json.dumps({}),
        ],
        [
            "org.reactobus.3",
            str(uuid.uuid1()),
            datetime.datetime.utcnow().isoformat(),
            "lavaserver",
            json.dumps({}),
        ],
        [
            "org.reactobus.4",
            str(uuid.uuid1()),
            "2016/01/01",
            "lavaserver",
            json.dumps({}),
        ],
        [
            "org.reactobus.5",
            str(uuid.uuid1()),
            datetime.datetime.utcnow().isoformat(),
            "lavaserver",
            json.dumps({}),
        ],
    ]
    with pytest.raises(IndexError):
        db.run()
    # Force the databse flush
    db.save_to_db()
    assert len(sub.recv) == 0

    # Check that the db is empty
    session = db.sessions()
    assert session.query(Message).count() == 4
    assert session.query(Message).get(1).topic == "org.reactobus.1"
    assert session.query(Message).get(2).topic == "org.reactobus.2"
    assert session.query(Message).get(3).topic == "org.reactobus.3"
    assert session.query(Message).get(4).topic == "org.reactobus.5"

