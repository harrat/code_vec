def test_serialization(valid_data, valid_serialization_output):
    """Ensure a serialized DesignWorkflow looks sane."""
    workflow: DesignWorkflow = Workflow.build(valid_data)
    serialized = workflow.dump()
    serialized['id'] = valid_data['id']
    assert serialized == valid_serialization_output

