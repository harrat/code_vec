def test_aggregator_strategy(aggregated):
    """Test that a constructor with aggregate strategy will initialize
    the connection."""
    assert aggregated._redis_client is not None
    assert hasattr(aggregated._redis_client, 'connection_pool')
    assert hasattr(aggregated._app, 'extensions')
    assert 'redis' in aggregated._app.extensions
    assert aggregated._app.extensions['redis'] == aggregated

