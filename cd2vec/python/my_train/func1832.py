def test_load_plugin():
    os.chdir(dirname(__file__))

    parser, subparsers = commands.make_argparser()
    args = parser.parse_args(['custom'])

    assert hasattr(args, 'func')

    args.func(args)

    for env in util.iter_subclasses(environment.Environment):
        print(env.__name__)
        if env.__name__ == 'MyEnvironment':
            break
    else:
        assert False, "Custom plugin not loaded"
