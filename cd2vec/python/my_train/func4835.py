def test_file_meta_dataset_implicit_vr_strict(self, enforce_valid_values):
        """Test reading a file meta dataset that is implicit VR"""

        bytestream = (
            b"\x02\x00\x10\x00\x12\x00\x00\x00"
            b"\x31\x2e\x32\x2e\x38\x34\x30\x2e"
            b"\x31\x30\x30\x30\x38\x2e\x31\x2e"
            b"\x32\x00"
        )
        fp = BytesIO(bytestream)
        with pytest.raises(InvalidDicomError,
                           match="Expected explicit VR, "
                                 "but found implicit VR"):
            dcmread(fp, force=True)
