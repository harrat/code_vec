def test_aggregated_delitem_method(mocked_aggregated):
    """Test FlaskMultiRedis aggregated __delitem__ method."""

    del(mocked_aggregated['name'])
    for node in mocked_aggregated._aggregator._redis_nodes:
        assert not hasattr(node, 'name')
    mocked_aggregated._aggregator._redis_nodes = []
    del(mocked_aggregated['name'])

