def test_no_id(self):
        subject = 'No pound sign.'
        result = self._call_function_under_test(subject)
        self.assertIsNone(result)
