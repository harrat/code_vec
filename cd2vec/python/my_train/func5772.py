def test_cannot_write_zip_file(zip_file):
    """Test that wkr.open cannot write to zip files."""
    modes = ['w', 'a', 'wb', 'ab']
    for mode in modes:
        with pytest.raises(ValueError):
            _ = wkr.open('{}:file1.txt'.format(zip_file), mode)  # noqa f841
