def test_make_p2wsh_output(self):
        helper_witness = helpers.P2WSH['human']['witnesses'][0]
        self.assertEqual(
            tb.make_p2wsh_output(
                value=helpers.P2WSH['human']['outs'][3]['value'],
                output_script=helper_witness['wit_script']),
            helpers.P2WSH['ser']['outs'][3]['output'])
