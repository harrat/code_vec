def test_crew_attr(person: Person):
    assert isinstance(person.crew, list)
    item, credit = person.crew[0]
    assert isinstance(item, (Show, Movie))
    assert isinstance(credit, Credit)

