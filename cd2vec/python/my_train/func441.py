def test_h5delete_file(self):
        h5create_file(main_path, 'test_deleting')
        h5delete_file('{}/test_deleting.h5'.format(test_path))

        file_check = os.path.isfile('{}/test_deleting.h5'.format(test_path))
        self.assertFalse(file_check)
