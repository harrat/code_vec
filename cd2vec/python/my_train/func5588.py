def test_set_env_without_config(clean):
    os.environ['PDIR2_CONFIG_FILE'] = 'aaa'
    with pytest.raises(OSError, match='Config file not exist: aaa'):
        import pdir
