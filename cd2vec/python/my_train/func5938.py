def test_background(self):
        """Test that runs code in the background.

        ASCII art depicting timeline shown below:
          0        1        2        3        4 seconds|
        --+--------+--------+--------+--------+--------+
          ^        ^        ^    ^       ^    ^        |Spawned
        "start"    |      "end"  |       |    |        |process
                   |             |Windows|    |        |-------
              "is_alive"         |Kills  | "is_dead"   |Test
                                 |Process|             |Checks
        """
        fileback = 'test_run_base_back.py'
        launch = Launcher(fileback, 'URL')
        launch.run(True)
        time.sleep(1)
        assert launch.process_is_alive
        launch.process_join(timeout=5)
        # Really takes up to 5 seconds for windows to kill process
        assert not launch.process_is_alive
        assert launch.process_exitcode == 0
