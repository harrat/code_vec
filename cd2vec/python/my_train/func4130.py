def test_dups(self):
        self._extract_structure()
        empty = os.path.join(data_dr, "empty")
        something = os.path.join(data_dr, "full")
        reusables.touch(empty)
        with open(something, "w") as f:
            f.write("stuff in here")
        try:
            dups = list(reusables.dup_finder(empty, data_dr))
            assert len(dups) == 1, dups
            dups2 = list(reusables.dup_finder(something, data_dr))
            assert len(dups2) == 1, dups
        finally:
            os.unlink(something)
            os.unlink(empty)
