def test_package_exists():
    """Check whether the package is installed."""
    proc = run_process([sys.executable, '-m', 'pip', 'show', 'tokendito'])
    assert not proc['stderr']
    assert proc['exit_status'] == 0

