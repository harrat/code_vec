def test_get_cleanup_list_without_target_finds_files(pyinstaller_files):
    assert cork.get_cleanup_list(target=None) == [
        "build",
        SOURCE_SPECFILE,
        "corkfile",
    ]
