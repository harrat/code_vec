def test_context_commands():
    Breathe.add_commands(
        AppContext("notepad"),
        {"test [<num>]": lambda num: DoNothing().execute()},
        [Choice("num", {"four": "4", "five": "5", "six": "6"})],
        {"num": ""},
    )
    with pytest.raises(MimicFailure):
        engine.mimic(["test", "three", "test", "four"])
    engine.mimic(["test", "three", "test", "four"], executable="notepad")

