@pytest.mark.parametrize(
    "name,obj,data_output,display_output,encoder,data_metadata,display_metadata,display",
    [
        (
            "foobarbaz",
            "foo,bar,baz",
            {
                GLUE_PAYLOAD_FMT.format(encoder="text"): {
                    "name": "foobarbaz",
                    "data": "foo,bar,baz",
                    "encoder": "text",
                    "version": 1,
                }
            },
            {"text/plain": "'foo,bar,baz'"},
            None,  # Save data as default
            {"scrapbook": {"name": "foobarbaz", "data": True, "display": False}},
            {"scrapbook": {"name": "foobarbaz", "data": False, "display": True}},
            True,  # Indicate display should also be available
        ),
        (
            "foobarbaz",
            ["foo", "bar", "baz"],
            {
                GLUE_PAYLOAD_FMT.format(encoder="json"): {
                    "name": "foobarbaz",
                    "data": ["foo", "bar", "baz"],
                    "encoder": "json",
                    "version": 1,
                }
            },
            {"text/plain": "['foo', 'bar', 'baz']"},
            "json",  # Save data as json
            {"scrapbook": {"name": "foobarbaz", "data": True, "display": False}},
            {"scrapbook": {"name": "foobarbaz", "data": False, "display": True}},
            True,  # Indicate display should also be available
        ),
    ],
)
@mock.patch("IPython.display.display")
def test_glue_plus_display(
    mock_display,
    name,
    obj,
    data_output,
    display_output,
    encoder,
    data_metadata,
    display_metadata,
    display,
):
    glue(name, obj, encoder, display)
    mock_display.assert_has_calls(
        [
            mock.call(data_output, metadata=data_metadata, raw=True),
            mock.call(display_output, metadata=display_metadata, raw=True),
        ]
    )
