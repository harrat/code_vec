def test_bitcoin_data_export_adjmatrix(self):
        expected_hash = 'e82afed32d1ef7772e371bddc810b1c1'
        node_data = [('1DqeUNa3wqJRamTEUMTiUXHAQynQuLh426', {'name': 'Near Genesis'})]
        edge_data = bitcoin_network(node_data)
        graph_data = bitcoin_graph(node_data, edge_data)
        bitcoin_data_export(graph_data, 'adjacencymatrix', 'test')
        md5_hash = hashlib.md5()
        with open('test.csv', "rb") as f:
            for chunk in iter(lambda: f.read(4098), b""):
                md5_hash.update(chunk)
        self.assertEqual(md5_hash.hexdigest(), expected_hash, f"Should be {expected_hash}")
