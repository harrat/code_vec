def test_global_env_type_option(self):
        self.assertIs(
            self.get_opts('test').global_env_type,
            None
        )
        self.assertEqual(
            self.get_opts(
                'test', '--global-env-type', 'abc'
            ).global_env_type,
            'ABC'
        )
