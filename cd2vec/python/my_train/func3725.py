def test_aws_related_tags(handler_with_profile, mock_context, mock_event, monkeypatch):
    ConfigProvider.set(config_names.THUNDRA_APPLICATION_STAGE, 'dev')
    monkeypatch.setitem(os.environ, "_X_AMZN_TRACE_ID", "Root=1-5759e988-bd862e3fe1be46a994272793;Parent=53995c3f42cd8ad8;Sampled=1")
    thundra, handler = handler_with_profile

    invocation_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, InvocationPlugin):
            invocation_plugin = plugin

    try:
        handler(mock_event, mock_context)
    except Exception:
        pass

    assert invocation_plugin.invocation_data['tags']['aws.lambda.arn'] == 'arn:aws:lambda:us-west-2:123456789123:function:test'
    assert invocation_plugin.invocation_data['tags']['aws.account_no'] == '123456789123'
    assert invocation_plugin.invocation_data['tags']['aws.lambda.memory_limit'] == 128
    assert invocation_plugin.invocation_data['tags']['aws.lambda.log_group_name'] == 'log_group_name'
    assert invocation_plugin.invocation_data['tags']['aws.lambda.log_stream_name'] == 'log_stream_name[]id'
    assert invocation_plugin.invocation_data['tags']['aws.lambda.invocation.request_id'] == 'aws_request_id'
    assert invocation_plugin.invocation_data['tags']['aws.xray.trace.id'] == '1-5759e988-bd862e3fe1be46a994272793'
    assert invocation_plugin.invocation_data['tags']['aws.xray.segment.id'] == '53995c3f42cd8ad8'

