@mock.patch('os.makedirs')
    def test_fails_to_make_directory(self, f: Callable):
        f.side_effect = OSError()
        with self.assertRaises(OSError):
            download.cached_download('https://example.com')
