def test_stop(self):
        """
        Test that the PolicyDirectoryMonitor processes stop calls correctly.
        """
        m = monitor.PolicyDirectoryMonitor(
            self.tmp_dir,
            multiprocessing.Manager().dict()
        )

        self.assertFalse(m.halt_trigger.is_set())

        m.stop()

        self.assertTrue(m.halt_trigger.is_set())
