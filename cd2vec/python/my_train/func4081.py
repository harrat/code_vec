@pytest.mark.skipif(not is_connected(), reason="Need an internet connection")
def test_validate_imports_alias():
    assert validate_imports(["dateutil"], ["python-dateutil"])
    assert not validate_imports(["dateutil"], ["___"])

