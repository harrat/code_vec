@mark.django_db
def test_revision_is_visible_in_list(django_user_model):
    alpha = Dragonfly.objects.create(name="alpha", age=47)

    request = RequestFactory().get("/", {})
    request.user = user_with_perms(django_user_model, ["testapp.view_dragonfly"])

    with VersionedDragonflyViewSet().create_revision(request):
        set_comment("number one")
        alpha.save()

    with VersionedDragonflyViewSet().create_revision(request):
        set_comment("number two")
        alpha.save()

    with VersionedDragonflyViewSet().create_revision(request):
        set_comment("number three")
        alpha.save()

    assert Version.objects.get_for_object_reference(Dragonfly, alpha.pk).count() == 3

    view = VersionedDragonflyViewSet()._get_view(
        VersionedDragonflyViewSet().components["version_list"]
    )

    response = view(request, pk=alpha.pk)
    response_content = response.rendered_content

    assert "number one" in response_content
    assert "number two" in response_content
    assert "number three" in response_content

