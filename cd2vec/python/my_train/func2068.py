def test_announcing_no_tasks(self, mocker):
        class Task:
            app = MyrApp()

        mocker.patch.object(Task.app, 'send_task')

        announce(Task)
        Task.app.send_task.assert_called_with(
            ENV.get('MYR_ANNOUNCE_TASK'),
            args=[{}],
            queue=ENV.get('MYR_ANNOUNCE_QUEUE'))
