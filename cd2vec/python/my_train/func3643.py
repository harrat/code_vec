@staticmethod
    @pytest.mark.usefixtures('snippy', 'edit-beats')
    def test_cli_create_solution_006(snippy):
        """Create solution from editor.

        Create new solution by defining all values from editor.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.BEATS)
            ]
        }
        content['data'][0]['uuid'] = Content.UUID_EDIT
        cause = snippy.run(['snippy', 'create', '--scat', 'solution', '--editor', '--format', 'text'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
