def test_get_root(self):
        """Test simple HTTP GET"""
        response = self.get_response('/artists/meta', 200)
        assert 'Name' in json.loads(response.get_data(as_text=True))['Artist']
