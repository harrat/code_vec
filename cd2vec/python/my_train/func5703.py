@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'import-netcat', 'caller')
    def test_api_delete_snippet_002(server):
        """Try to delete snippet.

        Try to send DELETE /snippet{id} with a ``id`` in URI that does not
        exist.
        """

        content = {
            'data': [
                Storage.remove,
                Storage.forced,
                Storage.netcat,
            ]
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '370'
        }
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '404',
                'statusString': '404 Not Found',
                'module': 'snippy.testing.testing:123',
                'title': 'cannot find content with content identity: beefbeef'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_delete(
            path='/api/snippy/rest/snippets/beefbeef',
            headers={'accept': 'application/json'})
        assert result.status == falcon.HTTP_404
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(content)
