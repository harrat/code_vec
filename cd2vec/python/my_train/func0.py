def test_main_results():
    """Test the results of the real arguments"""
    # Due to complexities testing with arguments to get full coverage
    # run the script externally with full arguments
    os.popen('python3 -m pip install -e .')
    os.popen(
        'python3 Examples/WSO.py -url cn1234.awtest.com -username citests -password hunter2 -tenantcode shibboleet'
    ).read()

    filename = "uem.json"

    assert AUTH.check_file_exists(filename) is True
    assert AUTH.verify_config(filename, 'authorization',
                              AUTH.encode("citests", "hunter2")) is True
    assert AUTH.verify_config(filename, 'url', "cn1234.awtest.com") is True
    assert AUTH.verify_config(filename, 'aw-tenant-code', "shibboleet") is True

