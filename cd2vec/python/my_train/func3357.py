def test_session_locator_does_not_get_deleted_from_cache(self, basic_configurator, monkey_patcher):
        monkey_patcher()

        container = basic_configurator.get_container()
        dependency = basic_configurator.get_dependency_wrapper()

        d = dependency(lifetime=DependencyLifetimeEnum.INSTANCE)
        d2 = dependency(lifetime=DependencyLifetimeEnum.SESSION)

        @d
        def a():
            return type("A", (), {})()

        @d2
        def c():
            return type("C", (), {})()

        class B:
            def __init__(self, a, c):
                pass

        b = container.wire_dependencies(B)
        del b
        gc.collect()

        time.sleep(5)
        assert not d.services
        assert d2.services
