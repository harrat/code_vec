def test_fan_maxcfm():
    """py.test for fan_maxcfm in idf"""
    idf = IDF(StringIO(vavfan))
    thefans = idf.idfobjects["Fan:VariableVolume".upper()]
    thefan = thefans[0]
    cfm = thefan.f_fan_maxcfm
    assert almostequal(cfm, 12000, places=5)
    # test autosize
    thefan.Maximum_Flow_Rate = "autosize"
    watts = thefan.f_fanpower_watts
    assert watts == "autosize"

