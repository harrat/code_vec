def test_show_config_param(run_reframe):
    # Just make sure that this option does not make the frontend crash
    returncode, stdout, stderr = run_reframe(
        more_options=['--show-config=systems'],
        system='testsys'
    )
    assert 'Traceback' not in stdout
    assert 'Traceback' not in stderr
    assert returncode == 0

