@staticmethod
    @pytest.mark.usefixtures('default-references', 'import-netcat')
    def test_cli_search_reference_008(snippy, capsys):
        """Search references with ``uuid`` option.

        Try to search references by defining short uuid that matches to
        multiple contents in different categories. Since uuid must be
        unique and it must match to only one content, this must not
        find any content.
        """

        output = (
            'NOK: cannot find content with given search criteria',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--scat', 'reference', '--uuid', '1', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == 'NOK: cannot find content with given search criteria'
        assert out == Const.NEWLINE.join(output)
        assert not err
