def test_get_refund(self):
        payment_request = self.client.create_payment(
            payee_payment_reference='0123456789',
            callback_url='https://example.com/api/swishcb/paymentrequests',
            amount=100,
            currency='SEK',
            message='Kingston USB Flash Drive 8 GB'
        )
        time.sleep(5)
        payment = self.client.get_payment(payment_request.id)
        refund_request = self.client.create_refund(
            original_payment_reference=payment.payment_reference,
            amount=100,
            currency='SEK',
            callback_url='https://example.com/api/swishcb/refunds',
            payer_payment_reference='0123456789',
            message='Refund for Kingston USB Flash Drive 8 GB'
        )
        refund = self.client.get_refund(refund_request.id)
        self.assertEqual(refund.original_payment_reference, payment.payment_reference)
        self.assertEqual(refund.callback_url, 'https://example.com/api/swishcb/refunds')
        self.assertEqual(refund.amount, 100)
        self.assertEqual(refund.currency, 'SEK')
        self.assertEqual(refund.message, 'Refund for Kingston USB Flash Drive 8 GB')
