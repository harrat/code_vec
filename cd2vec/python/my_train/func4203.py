def test_it_should_not_fail_when_try_load_non_exist_default_file():
    del os.environ["ENV_YAML_FILE"]
    del os.environ["ENV_FILE"]

    env = EnvYAML()

    assert isinstance(env, EnvYAML)

