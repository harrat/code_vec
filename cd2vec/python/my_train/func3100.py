def test_tee_fileoutput(self):
        file1 = self.root.make_file(suffix='.gz')
        file2 = self.root.make_file()
        with self.assertRaises(ValueError):
            textoutput((file1, file2), access='z')
        with textoutput((file1, file2)) as o:
            o.writelines(('foo', 'bar', 'baz'))
        with gzip.open(file1, 'rt') as i:
            assert 'foo\nbar\nbaz\n' == i.read()
        with open(file2, 'rt') as i:
            assert 'foo\nbar\nbaz\n' == i.read()
