@mock.patch('cherry.classifyer.Classify._classify')
    @mock.patch('cherry.classifyer.Classify._load_cache')
    def test_init(self, mock_load, mock_classify):
        mock_load.return_value = ('foo', 'bar')
        cherry.classifyer.Classify(model='random', text=['random text'])
        mock_load.assert_called_once_with('random')
        mock_classify.assert_called_once_with(['random text'])
