def test_away_team_wins(self):
        fake_away_goals = PropertyMock(return_value=4)
        fake_home_goals = PropertyMock(return_value=3)
        type(self.boxscore)._away_goals = fake_away_goals
        type(self.boxscore)._home_goals = fake_home_goals

        assert self.boxscore.winner == AWAY
