@mock.patch('subprocess.check_output', mock.MagicMock(autospec=True, return_value=''))
    @mock.patch('skipper.runner.run', autospec=True, return_value=0)
    def test_run_without_build_container_tag(self, skipper_runner_run_mock):
        global_params = self.global_params[:-2]
        command = ['ls', '-l']
        run_params = command
        self._invoke_cli(
            global_params=global_params,
            subcmd='run',
            subcmd_params=run_params
        )
        expected_commands = [
            mock.call(['build', '--network=host', '-t', 'build-container-image', '-f',
                       'Dockerfile.build-container-image', '.']),
            mock.call(command, fqdn_image='build-container-image', environment=[],
                      interactive=False, name=None, net='host', volumes=None, workdir=None, workspace=None,
                      use_cache=False),
        ]
        skipper_runner_run_mock.assert_has_calls(expected_commands)
