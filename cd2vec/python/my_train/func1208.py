def test_gen_links_reqs(self, dcard, client):
        reqs = dcard.posts.gen_links_reqs([_post_id])
        result = list(client.imap(reqs))[0]
        assert isinstance(result.json(), dict)
