def test_list_option(self):
        self.assertIs(self.get_opts('test').opt_list, None)
        self.assertListEqual(
            self.get_opts(
                'test', '--opt-list', '1', 'b', '3'
            ).opt_list,
            ['1', 'b', '3']
        )
