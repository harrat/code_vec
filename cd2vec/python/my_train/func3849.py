def test_priorities():

    l = []

    @on_test.register(priority=PRIORITIES.LAST)
    def last():
        l.append(3)

    @on_test.register(priority=PRIORITIES.FIRST)
    def first():
        l.append(1)

    @on_test.register()
    def mid():
        l.append(2)

    on_test()

    assert l == [1, 2, 3]

