def test_set_tail_bad2(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        with self.assertRaises(ValueError):
            audiofile.set_head_middle_tail(tail_length=TimeValue("1000.000"))
