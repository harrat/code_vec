def test_sort_exception(query):
    """
    Test exception in sort method.
    """

    curkey = query.sort_key
    sortval = 'kgsdkfkfd'  # random sort parameter
    with pytest.raises(KeyError):
        query.sort(sort_attr=sortval)

    sort_key = 1.2  # non-string sort key
    with pytest.raises(ValueError):
        query.sort_key = sort_key

    # reset sort key
    query.sort_key = curkey

