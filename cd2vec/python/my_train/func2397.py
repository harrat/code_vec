def test_guard_if_function_throws_exception_text_file(self):
        @guard(TestFileGuardDecorator.TEST_TEXT_FILE_PATH)
        def function_that_changes_the_file():
            lines_to_write = ['of course\n', 'I would\n']
            self._assert_file_content_equals(TestFileGuardDecorator.TEST_FILE_CONTENTS)

            with open(TestFileGuardDecorator.TEST_TEXT_FILE_PATH, 'w') as file:
                file.writelines(lines_to_write)

            self._assert_file_content_equals(lines_to_write)
            raise Exception('Something happened #Windows10')

        with self.assertRaises(Exception):
            function_that_changes_the_file()
        self._assert_file_content_equals(TestFileGuardDecorator.TEST_FILE_CONTENTS)
