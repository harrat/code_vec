def test_prescript_only(self):
        data = {
            'pre_script': [
                "echo " + self.first_out,
                "echo " + self.second_out,
            ],
            'post_script': [],
            'remote': 'origin',
            'branch': 'master'
        }

        with open(self.filename, 'w') as f:
            json.dump(data, f)

        chain = CommandChain.load_from_config(self.filename)
        chain.commands[0].execute()
        chain.commands[1].execute()
        self.assertEqual(len(chain.commands), 3)
        self.assertEqual(chain.commands[2].cmd, 'git pull origin master')
        self.assertIn(self.first_out, chain.commands[0].out)
        self.assertIn(self.second_out, chain.commands[1].out)
