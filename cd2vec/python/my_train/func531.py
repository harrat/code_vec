def test_edit_utf8_file(cli, config, entries_file):
    """
    Editing a file that contains accents should not crash.
    """
    # I wish I could just `entries_file.write()` without encoding anything but... Python 2
    entries_file.write_binary(
        "20/01/2014\nalias_1 2 pr�paration du caf� pour l'�v�nement".encode('utf-8')
    )

    cli('edit')

