def test_nbexec_cli_out(tmp_path: Path) -> None:
    """Run nbexec to execute a temporary notebook with a custom filename."""
    runner = CliRunner()
    with runner.isolated_filesystem():
        nb = make_notebook(tmp_path)
        runner.invoke(nbexec_cli.nbexec_cli, [nb, "-o", "exe.ipynb"])
        cells = nbformat.read("exe.ipynb", as_version=4).cells
        for cell in cells:
            if cell.cell_type == "code":
                assert cell.execution_count
                for output in cell.outputs:
                    assert output
