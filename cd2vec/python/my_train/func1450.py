def test_prepare_without_smt(fake_job, slurm_only):
    fake_job.use_smt = False
    prepare_job(fake_job)
    with open(fake_job.script_filename) as fp:
        assert re.search(r'--hint=nomultithread', fp.read()) is not None
