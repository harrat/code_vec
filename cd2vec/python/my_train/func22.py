def test_predicates(capsys):
    context = new_context()
    context = shell.parse(context, input_file)
    context = shell.predicates(context, '')
    (out, err) = capsys.readouterr()
    # Skip first line (result of parse) and last line (blank)
    predicates = out.split('\n')[1:-1]
    assert len(predicates) == 6
    for predicate in predicates[:-1]:
        assert (predicate.startswith('http://example.org/social/') or
                predicate == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type')
