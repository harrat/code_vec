def test_dropping_seq(self):

        async def read():
            self.iterator_timestamp = int(time.time())
            self.consumer = self.consumer_mock.get_consumer(
                stream_name='test-stream', checkpoint_table='test-table', host_key=self.host_key,
                shard_iterator_type='AT_TIMESTAMP', iterator_timestamp=self.iterator_timestamp
            )

            async for shard_reader in self.consumer.get_shard_readers():
                print('Got shard reader: {}'.format(shard_reader))
                try:
                    async for record in shard_reader.get_records():
                        self.test_data.append(record[0]['Data'])
                except ShardClosedException:
                    print('Got ShardClosedException')
                    # We should get second shard reader after first one gets closed
                    # However we signal mocked method to stop returning shards after that
                    self.consumer_mock.shard_closed = True

        async def stop_test():
            await asyncio.sleep(1.2)
            self.consumer.stop()
            self.assertEqual('AT_TIMESTAMP', self.consumer_mock.iterator_kwargs.get('ShardIteratorType'))
            self.assertEqual(self.iterator_timestamp, self.consumer_mock.iterator_kwargs.get('Timestamp'))
            remove_cmd_found = False
            for cmd in self.consumer_mock.dynamodb_mock.commands:
                if cmd['cmd'] != 'update_item':
                    continue
                if cmd['kwargs']['UpdateExpression'] == 'remove superseq':
                    remove_cmd_found = True
                    break
            self.assertTrue(remove_cmd_found)

        async def test():
            await asyncio.gather(
                asyncio.ensure_future(stop_test()),
                asyncio.ensure_future(read())
            )

        self.event_loop.run_until_complete(test())
