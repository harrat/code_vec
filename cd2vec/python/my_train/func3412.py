@pytest.mark.remote
def test_datasets_detector():
    datasets = catalog.datasets("GWTC-1-confident", detector="V1")
    assert "GW150914_R1" not in datasets
    assert "GW170817_R1" in datasets

