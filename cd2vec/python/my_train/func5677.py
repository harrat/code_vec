@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_nhl_skater_returns_requested_career_stats(self, *args, **kwargs):
        # Request the career stats
        player = Player('zettehe01')
        player = player('')

        for attribute, value in self.skater_results_career.items():
            assert getattr(player, attribute) == value
