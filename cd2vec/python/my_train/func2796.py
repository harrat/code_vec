def test_attributes_transmission_from_aggregated_node(aggregated):
    """Test that attributes from aggregated nodes are
    available directly from FlaskMultiRedis object."""

    node = aggregated._aggregator._redis_nodes[0]
    assert aggregated.connection_pool is node.connection_pool

