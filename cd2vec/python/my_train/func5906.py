def test_agent(self):
        self.agent.getEconomyData(self.economy)
        self.agent.solve()
        self.assertAlmostEqual(self.agent.solution[0].cFunc[0](10., self.economy.MSS),
                               1.23867751)
