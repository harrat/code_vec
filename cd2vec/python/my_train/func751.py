def test_computeLRF2(self):
        # randomise the labels and compute the distance
        t3 = randomLabels(self.t1)
        t4 = mutateLabeledTree(t3, 5)
        res = computeLRF(t3,t4)
        self.assertEqual(res,5)
