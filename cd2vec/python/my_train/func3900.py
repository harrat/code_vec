@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_delete_reference_013(snippy):
        """Try to delete reference with search.

        Try to delete reference based on search keyword so that the category
        is left out. In this case the search keyword matches but the default
        category is snippet and content is not deleted.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--sall', 'chris'])
        assert cause == 'NOK: cannot find content with given search criteria'
        Content.assert_storage(content)
