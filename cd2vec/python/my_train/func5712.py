@pytest.mark.parametrize('collection', [1000, ], indirect=True)
    def test_pagination_max_page_size(self, app_fixture):
        client = app_fixture.client
        # default: page_size > 100 => 422
        # custom: page_size > 10 => 422
        response = client.get('/test/', query_string={'page_size': 101})
        assert response.status_code == 422
        response = client.get('/test/', query_string={'page_size': 11})
        if app_fixture.custom_params is False:
            assert response.status_code == 200
        else:
            assert response.status_code == 422
