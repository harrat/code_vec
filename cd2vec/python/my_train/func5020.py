def test_typed_cli_argument_initialization_with_no_value():
    parsed_value = _TestArgument.TYPED_ARGUMENT.from_args(argparse.Namespace(typed_argument=None))
    assert parsed_value is None

