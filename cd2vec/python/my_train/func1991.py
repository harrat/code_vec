def test_get_link_header_for_resource(self):
        """Does GETing a resource return the 'Link' header field?"""
        response = self.get_response('/tracks/1', 200)
        assert 'Link' in response.headers
        assert '</MediaType/1>; rel="related"' in response.headers['Link'] 
        assert self.get_response('/Album/1', 200).data is not None
