def test_get_with_filter(self):
        """Test simple HTTP GET"""
        response = self.get_response('/artists', 200, params={'Name': 'AC/DC'})
        assert len(json.loads(response.get_data(as_text=True))[u'resources']) == 1
