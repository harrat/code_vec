@pytest.mark.parametrize("msg", ["1", "123", "hola"])
def test_clientx(tcp_server, msg):
    """
    Test tcp clientx coroutine.

    :param tcp_server: tuple with host and port of testing server
    :param msg: message to send
    """
    host, port = tcp_server
    loop = asyncio.get_event_loop()

    assert loop.run_until_complete(clientx(host, port, msg.encode(), 32, time_out=5)).decode() == f"received:'{msg}'"
    assert (
        loop.run_until_complete(clientx(host, port, b"control message", 32, time_out=5))
        == b"received:'control message'"
    )
