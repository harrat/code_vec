@autodoc.describe('GET /')
def test_index():
    app = TestApp(create_app)
    res = app.get('/')
    assert res.status_code == 200

    return res

