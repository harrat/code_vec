def test_invalid_entry_names(self):
        ''' Invalid entry names. '''
        with self.assertRaisesRegexp(ValueError,
                                     r'\[barchart\] .*entry names.*'):
            barchart.draw(self.axes, _data(), entry_names=['x', 'y', 'z'])
