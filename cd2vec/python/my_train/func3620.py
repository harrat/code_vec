def test_alias_add(cli, alias_config):
    cli('alias', ['add', '-b', 'test', 'bar', '43/1'])

    with open(alias_config.path, 'r') as f:
        config_lines = f.readlines()

    assert 'bar = 43/1\n' in config_lines

