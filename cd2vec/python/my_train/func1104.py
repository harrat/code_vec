def test_kbd_interrupt_in_wait_with_limited_concurrency(
        async_runner, make_cases, make_async_exec_ctx):
    # The general idea for this test is to allow enough time for all the
    # four checks to be submitted and at the same time we need the
    # KeyboardInterruptCheck to finish first (the corresponding wait should
    # trigger the failure), so as to make the framework kill the remaining
    # three.
    ctx = make_async_exec_ctx(2)
    next(ctx)

    runner, _ = async_runner
    with pytest.raises(KeyboardInterrupt):
        runner.runall(make_cases([
            KeyboardInterruptCheck(), SleepCheck(10),
            SleepCheck(10), SleepCheck(10)
        ]))
        # FIXME: Dump everything in case Github #1369 appears
        print(util.repr(runner))
        print(runner.stats.failure_report())
        print(util.repr(rt.runtime().site_config))

    assert_interrupted_run(runner)

