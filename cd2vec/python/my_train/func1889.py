@responses.activate
    @pytest.mark.parametrize("medias", [[1234567890, 9876543210]])
    @patch("time.sleep", return_value=None)
    def test_like_medias(self, patched_time_sleep, medias):
        self.bot._following = [1]

        for media in medias:
            TEST_PHOTO_ITEM["id"] = media
            responses.add(
                responses.GET,
                "{api_url}media/{media_id}/info/".format(
                    api_url=API_URL, media_id=media
                ),
                json={
                    "auto_load_more_enabled": True,
                    "num_results": 1,
                    "status": "ok",
                    "more_available": False,
                    "items": [TEST_PHOTO_ITEM],
                },
                status=200,
            )

            results = 2
            response_data = {
                "caption": TEST_CAPTION_ITEM,
                "caption_is_edited": False,
                "comment_count": 4,
                "comment_likes_enabled": True,
                "comments": [TEST_COMMENT_ITEM for _ in range(results)],
                "has_more_comments": False,
                "has_more_headload_comments": False,
                "media_header_display": "none",
                "preview_comments": [],
                "status": "ok",
            }
            responses.add(
                responses.GET,
                "{api_url}media/{media_id}/comments/?".format(
                    api_url=API_URL, media_id=TEST_PHOTO_ITEM["id"]
                ),
                json=response_data,
                status=200,
            )

            response_data = {"status": "ok", "user": TEST_USERNAME_INFO_ITEM}
            responses.add(
                responses.GET,
                "{api_url}users/{user_id}/info/".format(
                    api_url=API_URL, user_id=TEST_PHOTO_ITEM["user"]["pk"]
                ),
                status=200,
                json=response_data,
            )

            response_data = {"status": "ok", "user": TEST_USERNAME_INFO_ITEM}
            responses.add(
                responses.GET,
                "{api_url}users/{user_id}/info/".format(
                    api_url=API_URL, user_id=TEST_PHOTO_ITEM["user"]["pk"]
                ),
                status=200,
                json=response_data,
            )

            responses.add(
                responses.POST,
                "{api_url}media/{media_id}/like/".format(
                    api_url=API_URL, media_id=TEST_PHOTO_ITEM["id"]
                ),
                status=200,
                json={"status": "ok"},
            )

        broken_items = self.bot.like_medias(medias)
        assert [] == broken_items
