def test_performance_report(run_reframe):
    returncode, stdout, stderr = run_reframe(
        checkpath=['unittests/resources/checks/frontend_checks.py'],
        more_options=['-t', 'PerformanceFailureCheck', '--performance-report']
    )
    assert r'PERFORMANCE REPORT' in stdout
    assert r'perf: 10 Gflop/s' in stdout

