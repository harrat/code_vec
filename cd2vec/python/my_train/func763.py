def test_get_document_listing(self, connected_adapter):
        """Test listing of documents stored on Ceph."""
        assert list(connected_adapter.get_document_listing()) == []

        document1, document1_id = {'foo': 'bar'}, '666'
        document2, document2_id = {'foo': 'baz'}, '42'

        connected_adapter.store_document(document1, document1_id)
        connected_adapter.store_document(document2, document2_id)
        document_listing = list(connected_adapter.get_document_listing())

        assert len(document_listing) == 2
        assert document1_id in document_listing
        assert document2_id in document_listing
