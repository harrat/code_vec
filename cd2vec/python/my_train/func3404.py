def test_fileoutput_stdout(self):
        path = self.root.make_file()
        sys.argv = [self.system_args, path]
        with textoutput() as o:
            o.writelines(('foo', 'bar', 'baz'))
        with open(path, 'rt') as i:
            assert 'foo\nbar\nbaz\n' == i.read()
        sys.argv = []
        with intercept_stdout(True) as outbuf:
            with byteoutput() as o:
                o.writelines((b'foo', b'bar', b'baz'))
            assert b'foo\nbar\nbaz\n' == outbuf.getvalue()
