def test_fake_update_cache(path):
    assert not os.path.isfile(path)

    ua = UserAgent(path=path, cache=False, use_cache_server=False)

    assert not os.path.isfile(path)

    with pytest.raises(AssertionError):
        ua.update(cache='y')

    ua.update(cache=True)

    assert os.path.isfile(path)

    _probe(ua)

