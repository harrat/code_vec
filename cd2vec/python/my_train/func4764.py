def test_busca_avatar_integracao():
    usuario = 'renzon'
    url_esperada = "https://avatars3.githubusercontent.com/u/3457115?v=4"

    url_obtida = githubapi.buscar_avatar(usuario)

    assert url_esperada == url_obtida

