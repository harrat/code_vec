def test_import(pkgname_from_dirs):
    """
    This test demonstrates the use of a _fixture_. It's not super useful in this case, but in general if you have
    tests that depend on some mutable state that you want to reinitialize each time, or some object that is immutable
    but expensive to instantiate, or a connection to a database, file, or other I/O, this is a good way to do it.
    The argument name 'pkgname_from_dirs' here tells pytest to call the above function with the same name, and pass
    that value in for the argument's value. With the scope='module' specification up there we're telling pytest it only
    needs to evaluate that fixture once for the duration of this test suite."""
    print(pkgname_from_dirs)
    __import__(pkgname_from_dirs)

