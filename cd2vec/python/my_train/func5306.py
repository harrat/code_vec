def test_set_rewrite_rules(self):
        self.client.set_rewrite_rules([
            (r'https://stackoverflow.com(.*)', r'https://github.com\1'),
        ])
        self._make_request('https://stackoverflow.com')

        last_request = self.client.get_last_request()

        self.assertEqual('https://github.com/', last_request['url'])
        self.assertEqual('github.com', last_request['headers']['Host'])
