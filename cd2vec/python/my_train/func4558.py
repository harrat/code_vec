def test_firehose_trigger(tracer_and_invocation_support, handler, mock_firehose_event, mock_context):
    thundra, handler = handler
    tracer, invocation_support = tracer_and_invocation_support
    assert lambda_event_utils.get_lambda_event_type(mock_firehose_event,
                                                    mock_context) == lambda_event_utils.LambdaEventType.Firehose
    try:
        response = handler(mock_firehose_event, mock_context)
    except:
        print("Error running handler!")
        raise
    span = tracer.recorder.get_spans()[0]

    invocation_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, InvocationPlugin):
            invocation_plugin = plugin

    assert span.get_tag(constants.SpanTags['TRIGGER_DOMAIN_NAME']) == constants.DomainNames['STREAM']
    assert span.get_tag(constants.SpanTags['TRIGGER_CLASS_NAME']) == constants.ClassNames['FIREHOSE']
    assert span.get_tag(constants.SpanTags['TRIGGER_OPERATION_NAMES']) == ['exampleStream']

    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_DOMAIN_NAME']) == constants.DomainNames['STREAM']
    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_CLASS_NAME']) == constants.ClassNames['FIREHOSE']
    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_OPERATION_NAMES']) == ['exampleStream']

    links = [
        "eu-west-2:exampleStream:1495072948:75c5afa1146857f64e92e6bb6e561ded",
        "eu-west-2:exampleStream:1495072949:75c5afa1146857f64e92e6bb6e561ded",
        "eu-west-2:exampleStream:1495072950:75c5afa1146857f64e92e6bb6e561ded",
    ]
    assert sorted(invocation_plugin.invocation_data['incomingTraceLinks']) == sorted(links)

