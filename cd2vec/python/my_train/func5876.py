def test_n_predictions():
    predictions = execute_pipeline("distilbert",
        "Since when does the Excellence Program of BNP Paribas exist?", 5
    )

    assert len(predictions) == 5

    assert predictions[0] == (
        "January 2016",
        "BNP Paribas? commitment to universities and schools",
    )
