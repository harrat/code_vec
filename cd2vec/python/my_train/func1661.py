def test_get_maker():
    M = get_maker('docs')
    assert M is not None
    assert isinstance(M(gettempdir(), True, True), DocMaker)

    M = get_maker('invalid')
    assert M is None

