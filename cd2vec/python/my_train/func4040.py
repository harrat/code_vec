@patch_ensure_connection_contextual()
def test_connect_failed_missing_db(unknown_db):
    """An unknown database must not trigger retrying, it should just fail.

    Behavior validation is done by intercepting the logging messages
    """
    io = StringIO()
    LOGGER.addHandler(StreamHandler(io))
    with pytest.raises(OperationalError):
        backend = load_backend(unknown_db["ENGINE"])
        conn = backend.DatabaseWrapper(unknown_db, "unknown_db")
        conn.ensure_connection()

    # test that retrying has NOT taken place (there's no point)
    assert "trial 0" not in io.getvalue()

