def test_nginx_check_banner_outdated(self):
        res = nginx.check_banner("nginx/1.0.0", "head_data", "http://example.com")

        self.assertTrue(any("Nginx Outdated" in r.message for r in res))
