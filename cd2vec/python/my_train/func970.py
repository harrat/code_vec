@staticmethod
    @pytest.mark.usefixtures('yaml', 'default-solutions', 'export-time')
    def test_cli_export_solution_022(snippy):
        """Export solution with search keyword.

        Export defined solution based on search keyword. File name is defined
        in solution metadata but not by command line -f|--file option. Content
        filename fields is used because the search result is a single content.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Content.deepcopy(Solution.BEATS)
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '--sall', 'beats'])
            assert cause == Cause.ALL_OK
            Content.assert_text(mock_file, 'howto-debug-elastic-beats.txt', content)
