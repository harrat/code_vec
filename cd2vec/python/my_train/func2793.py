def test_subscribe_to_public_channels(self, mocker):
        header_mock = mocker.patch(
            'bitmex_websocket._bitmex_websocket.BitMEXWebsocket.header')
        header_mock.return_value = []

        subscribe_mock: MagicMock = mocker.patch(
            'bitmex_websocket._bitmex_websocket.BitMEXWebsocket.subscribe')

        channels = [
            Channels.connected,
            SecureChannels.margin,
            SecureInstrumentChannels.order
        ]

        instrument = Instrument(channels=channels, should_auth=True)

        instrument.subscribe_channels()

        assert channels == instrument.channels

        subscribe_mock.assert_has_calls([mock.call('margin:XBTUSD'),
                                         mock.call('order:XBTUSD')])
