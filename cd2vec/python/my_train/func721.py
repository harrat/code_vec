def test_get_filepath_with_config_root(self):
        """Test _get_filepath method with TEST_CONFIG_ROOT env var set."""
        gbr_config_root = '/var/lib/gbr/'
        os.environ['TEST_CONFIG_ROOT'] = gbr_config_root
        conf = TESTConfig()
        with mock.patch('yamlconf.config.os.path.exists') as mock_exists:
            # TEST_CONFIG_ROOT env var set, no filename, not found in basepath
            mock_exists.side_effect = [False, True]
            expected = os.path.join(gbr_config_root, 'config.yaml')
            result = conf._get_filepath()
            self.assertEqual(expected, result)
