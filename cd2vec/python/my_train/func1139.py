def test_test_store_blob(self, connected_adapter):
        """Test storing binary objects onto Ceph."""
        blob = b'foo'
        key = 'some-key'
        connected_adapter.store_blob(blob, key)
        assert connected_adapter.retrieve_blob(key) == blob
