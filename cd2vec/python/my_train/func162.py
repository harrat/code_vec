def test_basic_site_remove_existing(basic_config):
    # create extra files
    # this file does not exist in path_contents, and so
    # should be removed
    os.mkdir(basic_config['deploy_path'])
    with open(join(basic_config['deploy_path'], 'rm_this'), 'w') as f:
        f.write('blablabla')

    strange_case(basic_config)

    path_contents = {
        '001_2012_01_16_file.html': True,
        'index.html': True,
        'blogs': {
            'index.html': True,
            '2012_01_01_post1.html': True,
            '2012_01_02_post2.html': True,
        },
    }
    check_path_contents(basic_config['deploy_path'], path_contents)

