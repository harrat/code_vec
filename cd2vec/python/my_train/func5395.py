@pytest.mark.skipif(HARDWARE, reason='not possible on actual hardware')
@patch('piripherals.util.GPIO', create=1)
def test_mpr121_irq(GPIO, bus):
    mpr = MPR121(bus=bus, handlers=1, irq=4, setup=1)

    GPIO.setmode.assert_called_with(GPIO.BCM)
    GPIO.setup.assert_called_with(4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect.assert_called_with(4, GPIO.FALLING, ANY)
    irq = GPIO.add_event_detect.call_args[0][2]

    handlers = []
    dev = bus.device(ADDR)
    for i in range(13):
        handlers.append(Mock())
        mpr.on_touch(i, handlers[i])

    for i in range(13):
        dev.write_word(0, 1 << i)
        irq()
        sleep(0.01)
        handlers[i].assert_called_once_with(True, i)
        dev.write_word(0, 0)
        irq()
        sleep(0.01)
        handlers[i].assert_called_with(False, i)

