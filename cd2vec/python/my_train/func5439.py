@staticmethod
    def test_help_option_002(capsys, caplog):
        """Test printing help from console.

        Print help with short option.
        """

        snippy = Snippy(['snippy', '-h'])
        snippy.run()
        snippy.release()
        out, err = capsys.readouterr()
        assert out == Const.NEWLINE.join(TestCliOptions.HELP)
        assert not err
        assert not caplog.records[:]
        Content.delete()
