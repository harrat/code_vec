def testOpenRead(tmpdir, zipPkg):

    # without a zip
    tmpDirPath = str(tmpdir.realpath())
    aaaDir = tmpdir.mkdir("aaa")
    aaaDir.join("a1").write('111')

    pkgPath = pypkg.PkgPath(joinpath(str(aaaDir), 'a1'))
    data = pkgPath.read()
    assert isinstance(data, pyutils.binarytype)
    assert data == b'111'

    data = pkgPath.readText()
    assert isinstance(data, pyutils.texttype)
    assert data == '111'

    # with a zip
    path = joinpath(MODULE_ZIP_PATH, 'pkg2', 'pkg3', 'module2.py')
    pkgPath = pypkg.PkgPath(path, zipPkg)
    data = pkgPath.read()
    assert isinstance(data, pyutils.binarytype)
    assert b'something' in data

    data = pkgPath.readText()
    assert isinstance(data, pyutils.texttype)
    assert 'something' in data
