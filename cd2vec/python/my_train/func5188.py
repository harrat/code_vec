def test_preprocess_args_topics_and_partitions(self, mock_kafka_client):
        args = mock.Mock(
            groupid="some_group",
            partitions=[0, 1, 2],
            topics=["topic1", "topic2"],
        )

        with self.mock_get_topics(), mock.patch.object(
            sys,
            "exit",
            autospec=True,
        ) as mock_exit:
            OffsetManagerBase.preprocess_args(
                args.groupid,
                args.topic,
                args.partitions,
                mock.Mock(),
                mock_kafka_client,
                topics=args.topics,
            )
            assert mock_exit.called
