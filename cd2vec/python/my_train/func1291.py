@override_settings(DJOSER=dict(settings.DJOSER, **{"HIDE_USERS": False}))
    def test_fail_403_without_permission(self):
        other_user = create_user(
            **{
                "username": "paul",
                "password": "verysecret",
                "email": "paul@beatles.com",
            }
        )
        data = {"email": "ringo@beatles.com"}
        url = reverse("user-detail", kwargs={User._meta.pk.name: other_user.pk})

        login_user(self.client, self.user)
        response1 = self.client.put(url, data=data)
        self.assert_status_equal(response1, status.HTTP_403_FORBIDDEN)
        response2 = self.client.get(self.url)
        self.assert_status_equal(response2, status.HTTP_200_OK)
