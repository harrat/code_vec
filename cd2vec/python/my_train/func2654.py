def test_show_inexistent_object(cli):
    output = cli('show', ['aoeuidhtns'])
    assert output == "Your search string aoeuidhtns is nothing.\n"

