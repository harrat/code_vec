def test_jointplot_one2one_zerominFalse(jp_data):
    jg1 = viz.jointplot(x="A", y="C", data=jp_data, one2one=True, zeromin=False)
    nptest.assert_array_equal(numpy.round(jg1.ax_joint.get_xlim()), [-7, 23])
    nptest.assert_array_equal(numpy.round(jg1.ax_joint.get_ylim()), [-7, 23])
    return jg1.fig

