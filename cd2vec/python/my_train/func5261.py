def test_fake_user_agent_path(path):
    assert not os.path.isfile(path)

    ua = UserAgent(path=path, cache=True, use_cache_server=False)

    assert path == ua.path

    assert os.path.isfile(path)

