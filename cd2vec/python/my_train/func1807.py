def test_positional():
    "make sure we didn't break existing functionality of argparse"
    p = configparse.Parser(prog=NAME)
    p.add_argument("positional")
    assert p.parse_args(["first arg"]).positional == 'first arg'
    with pytest.raises(SystemExit):
        p.parse_args([])
