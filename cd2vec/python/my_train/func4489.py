def test_set_audio_file_path_absolute(self):
        task = Task()
        task.audio_file_path_absolute = gf.absolute_path("res/container/job/assets/p001.mp3", __file__)
        self.assertIsNotNone(task.audio_file)
        self.assertEqual(task.audio_file.file_size, 426735)
        self.assertAlmostEqual(task.audio_file.audio_length, TimeValue("53.3"), places=1)
