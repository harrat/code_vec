def test_generate_should_generate_dockerfiles(testassets_path,
                                              partial_path,
                                              generated_path,
                                              expected_generated_dockerfilepaths,
                                              expected_generated_base_dockerfile_dir,
                                              expected_generated_builder_dockerfile_dir,
                                              expected_tf_bazel_builder_base_dockerfile_count,
                                              expected_tf_bazel_builder_dockerfile_count):
    rm_rf(os.path.join(testassets_path, generated_path))
    invoke_generate(testassets_path, partial_path, generated_path)

    for each_filepath in expected_generated_dockerfilepaths:
        assert os.path.exists(each_filepath), "Check [{}] existance.".format(each_filepath)

