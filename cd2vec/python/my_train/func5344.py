@freeze_time('2014-02-20')
def test_status_previous_file(cli, config, data_dir):
    config.set('taxi', 'nb_previous_files', '1')
    efg = EntriesFileGenerator(data_dir, '%m_%Y.tks')
    efg.expand(datetime.date(2014, 1, 1)).write(
        "01/01/2014\nalias_1 1 january"
    )
    efg.expand(datetime.date(2014, 2, 1)).write(
        "01/02/2014\nalias_1 1 february"
    )
    efg.patch_config(config)

    stdout = cli('status')

    assert 'january' in stdout
    assert 'february' in stdout

