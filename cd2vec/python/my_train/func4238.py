def test_backoff(monkeypatch: Any, proc: ContinuousSSH, caplog: Any) -> None:

    sleep_times = []

    def mock_sleep(duration):
        sleep_times.append(duration)

    times: Deque[float] = collections.deque([])

    def mock_monotonic():
        now = times.popleft()
        print(f"Time: {now}")
        return now

    monkeypatch.setattr(time, "sleep", mock_sleep)
    monkeypatch.setattr(time, "monotonic", mock_monotonic)

    # Test backoff when process appeared to take 0 time.
    times.extend([0.1, 0.1])
    with proc._backoff():
        pass

    last_wait = sleep_times.pop()
    assert last_wait == pytest.approx(0.1)
    assert proc._backoff_time == pytest.approx(0.2)
    assert not times

    times.extend([0.0, 0.2])
    with proc._backoff():
        pass

    assert not sleep_times
    assert proc._backoff_time == pytest.approx(0.1)

