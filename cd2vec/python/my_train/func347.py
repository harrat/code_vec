def test_redis_delay_task_decorator_oks(event_loop, redis_instance):

    manager = build_manager(dsn=redis_instance, loop=event_loop)

    globals()["test_redis_delay_task_decorator_oks_finished"] = False

    @manager.task()
    async def task_test_redis_delay_task_decorator_oks(num):
        globals()["test_redis_delay_task_decorator_oks_finished"] = True

    async def run():
        manager.run()

        await task_test_redis_delay_task_decorator_oks.delay(1)

        await manager.wait(timeout=0.2, exit_on_finish=True, wait_timeout=0.1)

    event_loop.run_until_complete(run())
    manager.stop()

    assert globals()["test_redis_delay_task_decorator_oks_finished"] is True

    del globals()["test_redis_delay_task_decorator_oks_finished"]

