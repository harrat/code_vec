def test_run_d40_rastrigin():
    print("test_run_d40:")
    dim = 40
    mean = np.ones([dim, 1]) * 3
    sigma = 2.0
    lamb = 1130 # note that lamb (sample size) should be even number
    allowable_evals = (148 + 8.8*3) * 1e3 # 2 sigma
    iteration_number = int(allowable_evals / lamb) + 1

    cr = CRFMNES(dim, rastrigin, mean, sigma, lamb)
    x_best, f_best = cr.optimize(iteration_number)
    print("f_best:{}".format(f_best))
    assert f_best < 1e-12

