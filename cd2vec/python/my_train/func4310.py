def test_flex_alloc_enough_nodes_constraint_partition(make_flexible_job):
    job = make_flexible_job('all')
    job.options = ['-C f1&f2', '--partition=p1,p2']
    job.num_tasks = -4
    prepare_job(job)
    assert job.num_tasks == 4

