def test_parse_udf_subdir(tmpdir):
    indir = tmpdir.mkdir('udfsubdir')
    outfile = str(indir)+'.iso'
    indir.mkdir('dir1').mkdir('subdir1')
    subprocess.call(['genisoimage', '-v', '-v', '-no-pad', '-iso-level', '1',
                     '-udf', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_udf_subdir)

def test_parse_udf_subdir_odd(tmpdir):
    indir = tmpdir.mkdir('udfsubdir')
    outfile = str(indir)+'.iso'
    indir.mkdir('dir1').mkdir('subdi1')
    subprocess.call(['genisoimage', '-v', '-v', '-no-pad', '-iso-level', '1',
                     '-udf', '-o', str(outfile), str(indir)])
