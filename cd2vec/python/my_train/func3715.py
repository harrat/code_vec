@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_solution_031(snippy):
        """Import all solution resources.

        Import all solutions from Markdown formatted file.
        """

        content = {
            'data': [
                Solution.KAFKA,
                Solution.BEATS
            ]
        }
        file_content = Content.get_file_content(Content.MKDN, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '-f', './all-solutions.md'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './all-solutions.md', mode='r', encoding='utf-8')
