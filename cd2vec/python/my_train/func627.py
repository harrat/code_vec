def it_skips_uninstallation_when_no_git_dir(
    tmp_path: Path, capsys: CaptureFixture
) -> None:
    whippet.uninstall_hooks(tmp_path)
    captured = capsys.readouterr()
    hooks_dir = tmp_path / ".git" / "hooks"

    assert "skipping hooks uninstallation" in captured.out
    assert not hooks_dir.exists()

