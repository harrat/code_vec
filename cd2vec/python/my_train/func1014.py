def test_init_app(app):
    """Test that a constructor without app instance will not initialize the
    connection.

    After FlaskMultiRedis.init_app(app) is called, the connection will be
    initialized."""
    redis = FlaskMultiRedis()
    assert redis._app is None
    assert len(redis._redis_nodes) == 0
    redis.init_app(app)
    assert redis._app is app
    assert len(redis._redis_nodes) == 1
    assert hasattr(redis._redis_client, 'connection_pool')

