def test_init_and_destroy():
    aloop = AsyncLoop()
    assert isinstance(aloop._event_loop, asyncio.AbstractEventLoop)
    assert not aloop.is_alive()

    aloop.start()
    assert aloop.is_alive()
    assert aloop._event_loop.is_running()

    aloop.stop()
    time.sleep(.01)
    assert not aloop._event_loop.is_running()
    assert not aloop.is_alive()

