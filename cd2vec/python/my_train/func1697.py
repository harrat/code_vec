def test_toJSON_normal(self):
		expect = OrderedDict([(u'@context', model.factory.context_uri), 
			(u'@id', u'http://lod.example.org/museum/Person/1'), (u'@type', u'crm:E21_Person'),
			('rdfs:label', 'Test Person')])
		model.factory.full_names = True
		p = model.Person("1")
		p._label = "Test Person"
		outj = model.factory.toJSON(p)
		self.assertEqual(expect, outj)
		# reset
		model.factory.full_names = False
