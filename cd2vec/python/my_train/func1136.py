def test_if_exception_is_handled(handler_with_exception, mock_context, mock_event):
    thundra, handler = handler_with_exception
    with pytest.raises(Exception) as exinfo:
        handler(mock_event, mock_context)

    assert 'error' in thundra.plugin_context

