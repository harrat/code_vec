@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_pulling_team_directly(self, *args, **kwargs):
        schedule = MockSchedule(None, None)

        flexmock(Team) \
            .should_receive('schedule') \
            .and_return(schedule)

        kansas = Team('KAN')

        for attribute, value in self.results.items():
            assert getattr(kansas, attribute) == value
