@staticmethod
    @pytest.mark.usefixtures('yaml', 'default-snippets', 'default-snippets-utc')
    def test_cli_import_snippet_016(snippy):
        """Import snippet defaults.

        Try to import snippet defaults again. The second import should fail
        with an error because the content already exist. The error text must
        be the same for all content categories.

        Because of random order dictionary in the code, the reported digest
        can vary when there are multiple failures to import each content.

        Because there is unique constraint violation for ``data`` and ``uuid``
        attributes and PostgreSQL and Sqlite throw the error from different
        attributes, both attributes must be checked.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '--defaults'])
            assert cause in ('NOK: content data already exist with digest 53908d68425c61dc',
                             'NOK: content uuid already exist with digest 53908d68425c61dc',
                             'NOK: content data already exist with digest 54e41e9b52a02b63',
                             'NOK: content uuid already exist with digest 54e41e9b52a02b63')
            Content.assert_storage(content)
            defaults_snippets = pkg_resources.resource_filename('snippy', 'data/defaults/snippets.yaml')
            Content.assert_arglist(mock_file, defaults_snippets, mode='r', encoding='utf-8')
