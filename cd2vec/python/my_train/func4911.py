@pytest.mark.parametrize(
    "file, result",
    [
        ("env.py", "ARG1 = 'VAL1'\nARG3 = 3\nARG2 = 'VAL2'"),
        (".env", "ARG1=VAL1\nARG3=3\nARG2=VAL2"),
    ],
)
def test_cofig_writing_with_dump(file, result, create_conf_file, monkeypatch):
    monkeypatch.setenv("ARG1", "VAL1")
    monkeypatch.setenv("VARS_DUMP", "True")
    from dynamic_conf import _main

    conf_file = create_conf_file(_file_name=repr(file), ARG1=1, ARG3=3)

    _main([conf_file, "ARG2=VAL2"])

    env_file = os.path.join(os.path.dirname(conf_file), file)
    assert os.path.exists(env_file)
    with open(env_file) as f:
        content = f.read()
        assert content == result
