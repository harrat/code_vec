def test_get_node_happy(monkeypatch):
    monkeypatch.setattr('processor.connector.snapshot_azure.http_get_request', mock_http_get_request_happy)
    from processor.connector.snapshot_azure import get_node
    data = {
        'type': 'Microsoft.Network/virtualNetworks',
        'snapshotId': '1',
        'path': "/resourceGroups/mno-nonprod-shared-cet-eastus2-networkWatcher/providers/"
                "Microsoft.Compute/availabilitySets/mno-nonprod-shared-cet-eastus2-tab-as03"

    }
    ret = get_node(None, None, None, data, 'abc', 'azureStructure')
    assert True == isinstance(ret, dict)
    ret = get_node('abcd', 'devtest', 'xyz', data, 'abc', 'azureStructure')
    assert True == isinstance(ret, dict)
    assert {'a': 'b'} == ret['json']

