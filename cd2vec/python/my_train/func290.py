def test_get_expanded_resource(self):
        """Does GETing a resource return the 'Link' header field?"""
        response = self.get_response('/tracks/1', 200, params={'expand': 1})
        assert 'album' in json.loads(response.get_data(as_text=True))
