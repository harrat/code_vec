def test_generate_file_tree(self):
        generator = Generator()
        generator.generate_project("Gatsby")

        self.assertTrue(os.path.isdir("gatsby/project/Gatsby/"))
        self.assertTrue(os.path.isfile("gatsby/settings.yml"))
        self.assertTrue(os.path.isfile("gatsby/.gitignore"))

        rmtree("gatsby")
