def test_retrieve_missing(tmpdir):
    source_root = tmpdir.mkdir("source")
    repo_root = tmpdir.mkdir("repo")

    # Create new source files
    (source_root.join("simple_2019-03-01.txt")).open("w").close()
    (source_root.join("simple_2019-03-02.txt")).open("w").close()
    (source_root.join("simple_2019-03-03.txt")).open("w").close()
    (source_root.join("simple_2019-03-04.txt")).open("w").close()

    p = Profile("foo", FilesystemDriver, dict(root=str(source_root)))
    r = Repository(
        "foo",
        period="1 days",
        start=dt.datetime(2019, 3, 1),
        profile=p,
        targets=dict(default="empty_{time:%Y-%m-%d}.dat"),
        configuration=dict(patterns=dict(default="simple_{time:%Y-%m-%d}.txt")),
    )
    ref_time = dt.datetime(2019, 3, 5)

    redis_conn = FakeStrictRedis()
    runner.retrieve_missing(
        repo_root, [r], redis_conn=redis_conn, is_async=False, ref_time=ref_time
    )
    assert len(list(glob.glob(str(repo_root.join("foo/*.dat"))))) == 4

