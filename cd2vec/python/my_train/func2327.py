def test_not_trusted_by_mozilla_but_trusted_by_microsoft(self):
        # Given a server to scan that has a certificate chain valid for the Microsoft but not the Mozilla trust stores
        server_location = ServerNetworkLocationViaDirectConnection.with_ip_address_lookup(
            "webmail.russia.nasa.gov", 443
        )
        server_info = ServerConnectivityTester().perform(server_location)

        # When running the scan, it succeeds
        plugin_result = CertificateInfoImplementation.scan_server(server_info)

        # And the chain was correctly identified as valid with the Microsoft store
        found_microsoft_store = False
        for validation_result in plugin_result.certificate_deployments[0].path_validation_results:
            if validation_result.trust_store.name == "Windows":
                found_microsoft_store = True
                assert validation_result.was_validation_successful
                break
        assert found_microsoft_store
