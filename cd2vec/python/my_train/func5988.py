def test_get_file_contents(git_dir):
    git_dir.chdir()
    assert dzonegit.get_file_contents("dummy") == b"dummy\n"
    with pytest.raises(subprocess.CalledProcessError):
        dzonegit.get_file_contents('nonexistent')
