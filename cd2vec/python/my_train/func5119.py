def test_very_hres_chi2(frame_setup_very_hres):
    frame = copy.deepcopy(frame_setup_very_hres)
    frame.add_noise(10, noise_type='chi2')
    
    assert abs(np.mean(frame.get_data()) / 10 - 1) < 0.01
    assert abs(np.std(frame.get_data()) / (10 * (2 / (4 * 2))**0.5) - 1) < 0.01
