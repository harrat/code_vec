def test_messages():
    user, password = "u1", "p1"
    resp = {"message_list": [{"a": "b"}, {"c": "d"}]}
    client = LathermailClient(user, password)

    last_request = [None]
    last_url = [None]

    @httmock.all_requests
    def messages_mock(url, request):
        assert url.path == "/api/0/messages/"
        assert request.headers["X-Mail-Password"] == password
        assert request.headers["X-Mail-Inbox"] == user
        last_url[0] = url
        last_request[0] = request
        return json_response(resp, 204 if request.method == "DELETE" else 200)


    with httmock.HTTMock(messages_mock):
        assert client.get_messages() == resp["message_list"]
        assert not last_url[0].query
        client.get_messages(recipients_address="tst@tst.tt")
        assert "tst.tt" in last_url[0].query
        assert "recipients.address" in last_url[0].query
        assert last_request[0].method == "GET"
