@pytest.mark.parametrize(
    "logline,expected_action",
    [
        ("Entering interactive session", Action.CONNECTED),
        ("debug1: Reading configuration", Action.CONTINUE),
        ("Host example.com not responding", Action.DISCONNECTED),
    ],
)
def test_ssh_log_line(proc: ContinuousSSH, logline: str, expected_action: Action, caplog: Any) -> None:

    logger = logging.getLogger(__name__).getChild("ssh")
    assert proc._handle_ssh_line(logline, logger) == expected_action

    for record in caplog.records:
        assert "debug1" not in record.message

