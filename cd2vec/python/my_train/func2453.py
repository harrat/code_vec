def test_http_non_dataset_uri(tmp_dtool_server):  # NOQA
    import dtool_http
    with pytest.raises(dtool_http.storagebroker.HTTPError):
        DataSet.from_uri(tmp_dtool_server + "not-here")
