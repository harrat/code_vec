def test_preset_document_response(self, bot, bot_chat_id, generate_new_update, payload):
        response = _PresetDocumentResponse(self.DOCUMENT_URL)
        update = generate_new_update(chat_id=bot_chat_id)

        message = response.respond(bot, update, payload)
        assert message
        assert message.document
