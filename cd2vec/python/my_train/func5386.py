def test_concurrency_unlimited(async_runner, make_cases, make_async_exec_ctx):
    num_checks = 3

    # Trigger evaluation of the execution context
    ctx = make_async_exec_ctx(num_checks)
    next(ctx)

    runner, monitor = async_runner
    runner.runall(make_cases([SleepCheck(.5) for i in range(num_checks)]))

    # Ensure that all tests were run and without failures.
    assert num_checks == runner.stats.num_cases()
    assert_runall(runner)
    assert 0 == len(runner.stats.failures())

    # Ensure that maximum concurrency was reached as fast as possible
    assert num_checks == max(monitor.num_tasks)
    assert num_checks == monitor.num_tasks[num_checks]
    begin_stamps, end_stamps = _read_timestamps(monitor.tasks)

    # Warn if not all tests were run in parallel; the corresponding strict
    # check would be:
    #
    #     assert begin_stamps[-1] <= end_stamps[0]
    #
    if begin_stamps[-1] > end_stamps[0]:
        pytest.skip('the system seems too much loaded.')

