def test_provenance(tmpdir):
    metawf = pe.Workflow(name="meta")
    metawf.base_dir = tmpdir.strpath
    metawf.add_nodes([create_wf("wf%d" % i) for i in range(1)])
    eg = metawf.run(plugin="Linear")
    prov_base = tmpdir.join("workflow_provenance_test").strpath
    psg = write_workflow_prov(eg, prov_base, format="all")
    assert len(psg.bundles) == 2
    assert len(psg.get_records()) == 7

