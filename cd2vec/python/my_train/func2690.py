def test_posted_uri(self):
        """Make sure 'uri' in the links returned in response actually points to
        new resource created during POST."""
        post_response = self.post_response()
        as_json = json.loads(post_response.get_data(as_text=True))
        assert as_json == {
                'ArtistId': 276,
                'Name': 'Jeff Knupp',
                'self': '/artists/276',
                'links': [{'rel': 'self', 'uri': '/artists/276'}]
                }
        uri = as_json['self']
        self.app.get(uri)
        assert as_json[u'Name'] == u'Jeff Knupp'
