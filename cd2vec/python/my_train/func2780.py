def test_post(self):
        """Test simple HTTP POST"""
        response = self.post_response()
        assert json.loads(response.get_data(as_text=True)) == {
            'ArtistId': 276,
            'Name': 'Jeff Knupp',
            'self': '/artists/276',
            'links': [{'rel': 'self', 'uri': '/artists/276'}]}
