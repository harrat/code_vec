def test_get_metas_with_time_bound_param(self, dcard, boundary_date):
        forum = dcard.forums(_forum_name)
        assert len(forum.get_metas()) == 30
        assert len(forum.get_metas(timebound='')) == 30
        assert len(forum.get_metas(timebound=boundary_date.isoformat())) >= 0
