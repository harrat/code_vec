def test_check_data_file():

    filename = 'sample_data_1.npy'

    check_data_file(filename, TEST_FOLDER)
    assert os.path.isfile(os.path.join(TEST_FOLDER, filename))

def test_fetch_ndsp_data():
