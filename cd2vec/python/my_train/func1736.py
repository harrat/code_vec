@can_only_run_on_linux_64
    def test_early_data_disabled(self):
        # Given a server to scan that does NOT support early data because it it is disabled
        with ModernOpenSslServer(max_early_data=None) as server:
            server_location = ServerNetworkLocationViaDirectConnection(
                hostname=server.hostname, ip_address=server.ip_address, port=server.port
            )
            server_info = ServerConnectivityTester().perform(server_location)

            # When testing for early data support, it succeeds
            result: EarlyDataScanResult = EarlyDataImplementation.scan_server(server_info)

            # And the right result is returned
        assert not result.supports_early_data
