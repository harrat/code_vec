def test_read_UV_explicit_little(self):
        """Check reading element with VR of UV encoded as explicit"""
        ds = dcmread(self.fp_ex, force=True)
        elem = ds["UVElementMinimum"]
        assert "UV" == elem.VR
        assert 0xFFFE0003 == elem.tag
        assert 0 == elem.value

        new = DataElement(0xFFFE0003, "UV", 0)
        assert elem == new

        elem = ds["UVElementMaximum"]
        assert "UV" == elem.VR
        assert 0xFFFE0004 == elem.tag
        assert 2 ** 64 - 1 == elem.value

        new = DataElement(0xFFFE0004, "UV", 2 ** 64 - 1)
        assert elem == new
