@skip_on_exception(RemoteDataError)
    def test_yahoo(self):
        # Asserts that yahoo is minimally working
        start = datetime(2010, 1, 1)
        end = datetime(2013, 1, 25)

        assert round(web.DataReader("F", "yahoo", start, end)["Close"][-1], 2) == 13.68
