def test_uuid1_future():
    """
    Test that we can go back in time after setting a future date.
    Normally UUID1 would disallow this, since it keeps track of
    the _last_timestamp, but we override that now.
    """
    future_target = datetime.datetime(2056, 2, 6, 14, 3, 21)
    with freeze_time(future_target):
        assert time_from_uuid(uuid.uuid1()) == future_target

    past_target = datetime.datetime(1978, 7, 6, 23, 6, 31)
    with freeze_time(past_target):
        assert time_from_uuid(uuid.uuid1()) == past_target
