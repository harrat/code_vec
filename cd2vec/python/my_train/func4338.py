def test_output_has_Movie_objects(results):
    assert any([isinstance(item, Movie) for item in results[0]])

