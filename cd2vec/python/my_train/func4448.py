def test_get_version_for_type(monkeypatch):
    from processor.connector.snapshot_azure import get_version_for_type
    assert None == get_version_for_type({})
    assert '2019-09-01' == get_version_for_type({'type': 'Microsoft.Network/virtualNetworks'})

    monkeypatch.setattr('processor.connector.snapshot_azure.json_source', mock_db_json_source)
    monkeypatch.setattr('processor.connector.snapshot_azure.config_value', mock_config_value)
    monkeypatch.setattr('processor.connector.snapshot_azure.get_documents', mock_api_version_get_document)
    assert '2017-07-01' == get_version_for_type({'type': 'Microsoft.RecoveryServices/locations/backupStatus'})

