def test_open_vocab_format_space(self):
        malware = copy.deepcopy(self.valid_malware)
        malware['malware_types'] += "ransom ware"
        self.assertFalseWithOptions(malware,
                                    disabled='malware-types')
