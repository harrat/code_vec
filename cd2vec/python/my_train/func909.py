def test_show_cast_attr(person: Person):
    assert isinstance(person.show_cast, list)
    show, credit = person.show_cast[0]
    assert isinstance(show, Show)
    assert isinstance(credit, Credit)

