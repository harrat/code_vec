def test_connectcomponents():
    """py.test for connectcomponents"""
    fhandle = StringIO("")
    idf = IDF(fhandle)

    tdata = (
        (
            [
                (idf.newidfobject("PIPE:ADIABATIC", Name="pipe1"), None),
                (idf.newidfobject("PIPE:ADIABATIC", Name="pipe2"), None),
            ],
            ["pipe1_Inlet_Node_Name", ["pipe2_Inlet_Node_Name", "pipe1_pipe2_node"]],
            [["pipe1_Outlet_Node_Name", "pipe1_pipe2_node"], "pipe2_Outlet_Node_Name"],
            "",
        ),
        # components_thisnodes, inlets, outlets, fluid
        (
            [
                (idf.newidfobject("Coil:Cooling:Water", Name="pipe1"), "Water_"),
                (idf.newidfobject("Coil:Cooling:Water", Name="pipe2"), "Water_"),
            ],
            [
                "pipe1_Water_Inlet_Node_Name",
                "",
                "pipe2_Water_Inlet_Node_Name",
                ["", "pipe1_pipe2_node"],
            ],
            [
                ["pipe1_Water_Outlet_Node_Name", "pipe1_pipe2_node"],
                "",
                "pipe2_Water_Outlet_Node_Name",
                "",
            ],
            "Air",
        ),
        # components_thisnodes, inlets, outlets, fluid
        (
            [
                (idf.newidfobject("PIPE:ADIABATIC", Name="pipe1"), None),
                (idf.newidfobject("Coil:Cooling:Water", Name="pipe2"), "Water_"),
            ],
            [
                "pipe1_Inlet_Node_Name",
                "pipe2_Water_Inlet_Node_Name",
                ["pipe2_Air_Inlet_Node_Name", "pipe1_pipe2_node"],
            ],
            [
                ["pipe1_Outlet_Node_Name", "pipe1_pipe2_node"],
                "pipe2_Water_Outlet_Node_Name",
                "",
            ],
            "Air",
        ),
        # components_thisnodes, inlets, outlets, fluid
    )
    for components_thisnodes, inlets, outlets, fluid in tdata:
        # init the nodes in the new components
        for component, thisnode in components_thisnodes:
            hvacbuilder.initinletoutlet(idf, component, thisnode)
        hvacbuilder.connectcomponents(idf, components_thisnodes, fluid)
        inresult = []
        for component, thisnode in components_thisnodes:
            fldnames = hvacbuilder.getfieldnamesendswith(component, "Inlet_Node_Name")
            for name in fldnames:
                inresult.append(component[name])
        assert inresult == inresult
        outresult = []
        for component, thisnode in components_thisnodes:
            fldnames = hvacbuilder.getfieldnamesendswith(component, "Outlet_Node_Name")
            for name in fldnames:
                outresult.append(component[name])
        assert outresult == outlets

