def test_write_lines(self):
        linesep_len = len(os.linesep)
        path = self.root.make_file()
        assert 3 == write_lines(['foo'], path, linesep=None)
        assert list(read_lines(path)) == ['foo']
        path = self.root.make_file()
        self.assertEquals(
            9 + (2*linesep_len),
            write_lines(('foo', 'bar', 'baz'), path, linesep=None))
        self.assertEqual(
            list(read_lines(path)),
            ['foo', 'bar', 'baz'])
        path = self.root.make_file()
        self.assertEquals(
            11, write_lines(('foo', 'bar', 'baz'), path, linesep='|'))
        assert list(read_lines(path)) == ['foo|bar|baz']
        path = self.root.make_file(permissions='r')
        assert -1 == write_lines(['foo'], path, errors=False)
