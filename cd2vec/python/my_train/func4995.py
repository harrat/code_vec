def test_drop_mismatch_samples_real(testfiles):
    annotation = flatten_dict(testfiles, 'annotation').values()
    ontology = flatten_dict(testfiles, 'ontology').values()
    for an, on in zip(annotation, ontology):
        samples_.drop_mismatch_samples(an, on)

