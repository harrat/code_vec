@mock.patch('requests.session', side_effect=mock.MagicMock)
    def test_resource_not_found_error(self, session_mock):
        mock.MagicMock.ok = PropertyMock(return_value=False)
        mock.MagicMock.status_code = PropertyMock(return_value=404)
        client = TestApiClient.mock_client()
        with self.assertRaises(ResourceNotFoundError):
            client._request('get', 'foo')
