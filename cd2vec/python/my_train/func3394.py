def test_load_jsonlines():
    """Test loading JSONlines file.
    Loads files from test_save_fg.
    """

    res_file_name = 'test_fooofgroup_res'

    for data in load_jsonlines(res_file_name, TEST_DATA_PATH):
        assert data

def test_load_file_contents():
    """Check that loaded files contain the contents they should.
    Note that is this test fails, it likely stems from an issue from saving.
    """
