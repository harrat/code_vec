def test_outside_repo(self, cli_runner, tmpdir):
        with chdir(tmpdir.strpath):
            result = cli_runner.invoke(cli.uninstall)
            assert "Not a git repository (or any of the parent directories)" in result.output
            assert result.exception
            assert result.exit_code == 1
