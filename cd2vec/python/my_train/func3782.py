def test_clear_querystring_overrides(self):
        self.client.set_querystring_overrides('foo=baz')
        self.client.clear_querystring_overrides()
        self._make_request('https://www.stackoverflow.com')

        last_request = self.client.get_last_request()

        query = urlsplit(last_request['url'])[3]
        self.assertEqual('', query)
