def test_with_metas_through__call__(self, dcard, metas):
        posts = dcard.posts(metas)
        assert posts.ids
        assert posts.metas
        assert not posts.only_id
