def test_it_loads_books(client, app):
    query = """
    {
        books{
            id
            title
            author {
                id
                firstName
            }
            readers {
                id
            }
        }
    }
    """
    with app.app_context():
        author = Author.create(first_name="test1", last_name="test2")
        book = Book.create(title="test", isbn="r123213", author=author)
        book2 = Book.create(title="test2", isbn="r12321312", author=author)
