def test_priorities_async():

    l = []
    import time

    @on_test.register(asynchronous=True, priority=PRIORITIES.FIRST)
    def first():
        time.sleep(.05)
        l.append(1)

    @on_test.register(asynchronous=True, priority=PRIORITIES.LAST)
    def last():
        l.append(2)

    on_test()

    assert l == [1, 2]

