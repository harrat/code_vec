def test_posters_attr(person: Person):
    assert isinstance(person.profiles, list)
    assert all(isinstance(item, Image) for item in person.profiles)
    assert person.profiles[0]._type == "profile"

