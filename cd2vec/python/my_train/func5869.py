def test_init_with_include_arg(self, bids_layout):
        root = join(DIRNAME, 'data', '7t_trt')
        config = join(DIRNAME, 'specs', 'test.json')
        layout = Layout([(root, config)], regex_search=True, include='sub-\d*')
        target = join(root, "dataset_description.json")
        assert target in bids_layout.files
        assert target not in layout.files
        assert join(root, "sub-01", "sub-01_sessions.tsv") in layout.files
        with pytest.raises(ValueError):
            layout = Layout([(root, config)], include='sub-\d*', exclude="meh")
