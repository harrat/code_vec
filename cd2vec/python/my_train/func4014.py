def test_tsv_dict_dups(self):
        path = self.root.make_file()
        with open(path, 'wt') as o:
            o.write('id\ta\tb\tc\n')
            o.write('row1\t1\t2\t3\n')
            o.write('row1\t4\t5\t6\n')

        with self.assertRaises(Exception):
            read_delimited_as_dict(
                path, key='id', header=True, converters=(str, int, int, int))
