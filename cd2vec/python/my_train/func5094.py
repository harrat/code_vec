def test_rfc3339_timezone_wrong_directive(self):
        self.formatter = rlog.RFC3339Formatter(
            fmt='[%(asctime)s] %(levelname)s: %(check_name)s: %(message)s',
            datefmt='%FT%T:z')
        self.handler.setFormatter(self.formatter)
        self.logger_without_check.info('foo')
        assert self.found_in_logfile(':z')
