def test_update_item(json_file):
    mod_item = {'price': 777.0,
                'name': "Test Product via update_item"
                }
    results = atomic.update_item(str(json_file), mod_item, "1144d69e-joya-33e8-bdfd-680688cce955")
    results_get = atomic.get_item(
        str(json_file), "1144d69e-joya-33e8-bdfd-680688cce955")
    assert results
    assert results_get[0]["name"] == 'Test Product via update_item'
    assert results_get[0]["price"] == 777.0

