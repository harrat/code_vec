@project_setup_py_test("hello-cpp", ["develop"])
def test_hello_develop():
    for expected_file in [
        # These files are the "regular" source files
        'setup.py',
        'CMakeLists.txt',
        'bonjour/__init__.py',
        'bonjourModule.py',
        'hello/__init__.py',
        'hello/__main__.py',
        'hello/_hello.cxx',
        'hello/CMakeLists.txt',
        # These files are "generated" by CMake and
        # are copied from CMAKE_INSTALL_DIR
        'hello/_hello%s' % get_ext_suffix(),
        'hello/world.py',
        'helloModule.py'
    ]:
        assert os.path.exists(expected_file)

