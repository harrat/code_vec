def test_train(self):
        model_config = core.ModelConfig(_lineworld_name)
        tc = core.PpoTrainContext()
        ppo_agent = tfagents.TfPpoAgent(model_config=model_config)
        ppo_agent.train(train_context=tc, callbacks=[duration.Fast(), log.Iteration()])
