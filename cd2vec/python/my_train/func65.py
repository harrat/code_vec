def test_check_arg():
    __check_arg('abc', is_ok=True)
    __check_arg(123, is_ok=True)
    __check_arg(True, is_ok=True)
    __check_arg(123.456, is_ok=True)
    __check_arg([123], is_ok=True)
    __check_arg(None, is_ok=False)
    __check_arg([None], is_ok=True)

