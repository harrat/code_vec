@pytest.mark.vcr()
def test_scan_export_was(api):
    api.scans.export(SCAN_ID_WITH_RESULTS, scan_type='web-app')

@pytest.mark.vcr()
def test_scan_export_bytesio(api):
    from io import BytesIO
    from tenable.reports.nessusv2 import NessusReportv2
    fobj = api.scans.export(SCAN_ID_WITH_RESULTS)
    assert isinstance(fobj, BytesIO)
