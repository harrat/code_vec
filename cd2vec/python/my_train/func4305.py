def test_check_www_redirect_valid(self):
        self.assertEqual(
            "https://adamcaudill.com/",
            network.check_www_redirect("https://www.adamcaudill.com/"),
        )
