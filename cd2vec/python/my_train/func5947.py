@pytest.mark.parametrize('items, prefix, expected', [
        ([[1], [2]], 'pre', 'pre193457943119183791520927887192579877848'),
        ([[1], [2]], b'pre', 'pre193457943119183791520927887192579877848'),
    ])
    def test_combine_hashes_lists(self, items, prefix, expected):
        result = combine_hashes_lists(items, prefix)
        assert expected == result
