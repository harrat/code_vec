@given(
        df=giotto_time_series(min_length=3, max_length=500),
        cycle=st.one_of(
            st.sampled_from(["year", "quarter", "month", "week"]),
            st.from_regex(r"[1-9][DWMQY]", fullmatch=True),
        ),
        freq=st.one_of(st.from_regex(r"[1-9]?[DWMQ]", fullmatch=True), st.none()),
        agg=st.sampled_from(["mean", "sum", "last"]),
    )
    @settings(deadline=None)
    def test_seasonal_split_shape_named(self, df, cycle, freq, agg):
        split = seasonal_split(df, cycle=cycle, freq=freq, agg=agg)
        if freq is None:
            freq = df.index.freqstr
        assert split.stack().shape == df.resample(freq).agg(agg).dropna().shape
