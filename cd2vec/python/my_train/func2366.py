@autodoc.describe('POST /')
def test_post(setup):
    res = setup.post_json('/', params={'id': 1, 'message': 'foo'})
    assert res.status_code == 200

    return res

