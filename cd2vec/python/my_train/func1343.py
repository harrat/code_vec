def test_defined_hour():
    progress_bar = ProgressBarBytes(2000)

    assert '  0% (0.00/1.95 KiB) [               ] eta --:-- /' == str(progress_bar)

    eta._NOW = lambda: 1411868722.0
    progress_bar.numerator = 1
    assert '  0% (0.00/1.95 KiB) [               ] eta --:-- -' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 2
    assert '  0% (0.00/1.95 KiB) [             ] eta 1:06:36 \\' == str(progress_bar)

