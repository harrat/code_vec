def test_driver_manager():
    cls = _env.FindClass('java.sql.DriverManager')
    mid1 = _env.GetStaticMethodID(
        cls,
        'getConnection',
        '(Ljava/lang/String;)Ljava/sql/Connection;'
    )
    s = _env.NewStringUTF('jdbc:sqlite::memory:')
    args = py2jdbc.jni.jvalue.__mul__(1)()
    setattr(args[0], 'l', s)
    conn = _env.CallStaticObjectMethodA(cls, mid1, args)
    _env.DeleteLocalRef(s)
    _env.DeleteLocalRef(conn)
    _env.DeleteLocalRef(cls)

