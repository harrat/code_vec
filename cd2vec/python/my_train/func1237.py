@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_delete_reference_008(snippy):
        """Delete reference with data.

        Delete reference based on content data.
        """

        content = {
            'data': [
                Reference.REGEXP
            ]
        }
        Content.assert_storage_size(2)
        cause = snippy.run(['snippy', 'delete', '--content', 'https://chris.beams.io/posts/git-commit/'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
