@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'import-netcat')
    def test_api_delete_snippet_001(server):
        """Delete snippet with digest.

        Send DELETE /snippets/{id} to delete one snippet. The ``id`` in URI
        that matches to one resource that is deleted.
        """

        content = {
            'data': [
                Storage.remove,
                Storage.forced
            ]
        }
        expect_headers = {}
        result = testing.TestClient(server.server.api).simulate_delete(
            path='/api/snippy/rest/snippets/f3fd167c64b6f97e',
            headers={'accept': 'application/json'})
        assert result.status == falcon.HTTP_204
        assert result.headers == expect_headers
        assert not result.text
        Content.assert_storage(content)
