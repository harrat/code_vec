def test_WHOIS_uk():
    sleep(.1)
    whois = WHOIS("wow.uk")
    assert str(whois.creation_date()) == "2014-06-11"
    assert whois.registrar() == "Planet Hippo Internet Ltd t/a EUKHOST [Tag = PLANETHIPPO]"

