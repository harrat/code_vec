@pytest.mark.integration
def test_chart():
    """test cartographer.chart"""

    helpers.setup(with_data=True)

    out_dir = helpers.TEST_PATH
    title = "test"
    map_layer_names = "test_layer"
    marker_file_names = "test_marker"

    list(
        map(
            lambda r: os.makedirs(os.path.join(out_dir, map_layer_names, str(r))),
            range(2),
        )
    )

    c.chart(out_dir, title, [map_layer_names], [marker_file_names])

    expected_html = os.path.join(out_dir, "test_index.html")
    actual_html = os.path.join(out_dir, "index.html")

    files_match = filecmp.cmp(expected_html, actual_html)

    helpers.tear_down()

    assert files_match

