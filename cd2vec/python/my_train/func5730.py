def test_wellhead():
    # test well9
    well9_tvd, well9_northing, well9_easting, _ = minimum_curvature(well9_true_md_m, well9_true_inc, well9_true_azi)
    tvd, mN, mE = loc_to_wellhead(
        well9_tvd,
        well9_northing,
        well9_easting,
        well9_true_surface_northing,
        well9_true_surface_easting
        )
    np.testing.assert_equal(tvd, well9_tvd)
    np.testing.assert_allclose(mN, well9_true_northing, atol=1)
    np.testing.assert_allclose(mE, well9_true_easting, atol=1)
    # test well10
    well10_tvd, well10_northing, well10_easting, _ = minimum_curvature(well10_true_md_m, well10_true_inc, well10_true_azi)
    tvd, mN, mE = loc_to_wellhead(
        well10_tvd,
        well10_northing,
        well10_easting,
        well10_true_surface_northing,
        well10_true_surface_easting
        )
    np.testing.assert_equal(tvd, well10_tvd)
    np.testing.assert_allclose(mN, well10_true_northing, atol=1)
    np.testing.assert_allclose(mE, well10_true_easting, atol=1)

def test_zero():
    # test well9
    _, well9_northing, well9_easting, _ = minimum_curvature(well9_true_md_m, well9_true_inc, well9_true_azi)
    tvd, mN, mE = loc_to_zero(
        well9_true_tvd_m,
        well9_true_northing,
        well9_true_easting,
        well9_true_surface_northing,
        well9_true_surface_easting
        )
    np.testing.assert_equal(tvd, well9_true_tvd_m)
    np.testing.assert_allclose(mN, well9_northing, atol=1)
    np.testing.assert_allclose(mE, well9_easting, atol=1)
    # test well10
    _, well10_northing, well10_easting, _ = minimum_curvature(well10_true_md_m, well10_true_inc, well10_true_azi)
    tvd, mN, mE = loc_to_zero(
        well10_true_tvd_m,
        well10_true_northing,
        well10_true_easting,
        well10_true_surface_northing,
        well10_true_surface_easting
        )
    np.testing.assert_equal(tvd, well10_true_tvd_m)
    np.testing.assert_allclose(mN, well10_northing, atol=1)
    np.testing.assert_allclose(mE, well10_easting, atol=1)
