@staticmethod
    @pytest.mark.usefixtures('default-references', 'import-pytest', 'caller')
    def test_api_delete_reference_002(server):
        """Try to delete reference.

        Try to send DELETE /reference{id} with ``id`` in URI does not exist.
        """

        storage = {
            'data': [
                Storage.pytest,
                Storage.gitlog,
                Storage.regexp
            ]
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '370'
        }
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '404',
                'statusString': '404 Not Found',
                'module': 'snippy.testing.testing:123',
                'title': 'cannot find content with content identity: beefbeef'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_delete(
            path='/api/snippy/rest/references/beefbeef',
            headers={'accept': 'application/json'})
        assert result.status == falcon.HTTP_404
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
