@pytest.mark.parametrize('kernels', list_to_test)
@pytest.mark.parametrize('kernel_type', list_type)
def test_force_en(kernels, kernel_type):
    """Check that the analytical force/en kernel matches finite difference of
    energy kernel."""

    cutoffs = np.ones(3)*1.2
    delta = 1e-5
    tol = 1e-4
    cell = 1e7 * np.eye(3)

    # set hyperparameters
    d1 = 1

    np.random.seed(10)
    env1 = generate_mb_envs(cutoffs, cell, delta, d1, kern_type=kernel_type)
    env2 = generate_mb_envs(cutoffs, cell, delta, d1, kern_type=kernel_type)

    hyps = generate_hm(kernels)

    _, _, en_kernel, force_en_kernel, _, _, _ = \
        str_to_kernel_set(kernels, kernel_type)
    print(force_en_kernel.__name__)

    nterm = 0
    for term in ['2', '3', 'many']:
        if (term in kernels):
            nterm += 1

    kern_finite_diff = 0
    if ('many' in kernels):
        _, _, enm_kernel, _, _, _, _ = \
            str_to_kernel_set(['many'], kernel_type)
        mhyps = hyps[(nterm-1)*2:]
        calc = 0
        nat = len(env1[0])
        for i in range(nat):
            calc += enm_kernel(env1[2][i], env2[0][0], mhyps, cutoffs)
            calc -= enm_kernel(env1[1][i], env2[0][0], mhyps, cutoffs)
        mb_diff = calc / (2 * delta)
        kern_finite_diff += mb_diff

    if ('2' in kernels):
        ntwobody = 1
        _, _, en2_kernel, _, _, _, _ = str_to_kernel_set('2', kernel_type)
        calc1 = en2_kernel(env1[2][0], env2[0][0], hyps[0:ntwobody * 2], cutoffs)
        calc2 = en2_kernel(env1[1][0], env2[0][0], hyps[0:ntwobody * 2], cutoffs)
        diff2b = 4 * (calc1 - calc2) / 2.0 / 2.0 / delta

        kern_finite_diff += diff2b
    else:
        ntwobody = 0

    if ('3' in kernels):
        _, _, en3_kernel, _, _, _, _ = str_to_kernel_set('3', kernel_type)
        calc1 = en3_kernel(env1[2][0], env2[0][0], hyps[ntwobody * 2:], cutoffs)
        calc2 = en3_kernel(env1[1][0], env2[0][0], hyps[ntwobody * 2:], cutoffs)
        diff3b = 9 * (calc1 - calc2) / 2.0 / 3.0 / delta

        kern_finite_diff += diff3b

    kern_analytical = \
        force_en_kernel(env1[0][0], env2[0][0], d1, hyps, cutoffs)

    print("\nforce_en", kernels, kern_finite_diff, kern_analytical)

    assert (isclose(kern_finite_diff, kern_analytical, rtol=tol))

