def test_wait_until_vanished_passes_correct_function_to_wait_until(monkeypatch, mock_po):
    monkeypatch.setattr(mock_po.__class__, '_locator_value', 'locator')
    mock_po.is_existing_called = False
    def is_existing(log=False):
        mock_po.is_existing_called = True
        return False
    mock_po.is_existing = is_existing
    mock_po.wait_until_vanished()
    assert mock_po.is_existing_called == True

