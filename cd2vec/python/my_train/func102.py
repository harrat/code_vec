def test_get_json(self):
        """Test explicitly getting the JSON version of a resource."""
        response = self.get_response('/artists',
                200,
                headers={'Accept': 'application/json'})
        assert len(json.loads(response.get_data(as_text=True))[u'resources']) == 275
