def test_single_episode(self):
        agent = agents.PpoAgent("CartPole-v0")
        count=log._CallbackCounts()
        agent.train([duration._SingleEpisode(), log._Callbacks(), count])
        assert count.train_iteration_begin_count == 1
