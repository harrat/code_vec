def test_deploy_by_date(self):
        """
        deploy_by_date
        """
        cronpi.run_by_date("ls", isOverwrite=True).on("2020-10-20 7:30")
        self.assertEqual(get_job_list()[0], "30 7 20 10 * ls")

        cronpi.run_by_date("ls", isOverwrite=True).on("2020-10-20 7:30am")
        self.assertEqual(get_job_list()[0], "30 7 20 10 * ls")

        cronpi.run_by_date("ls", isOverwrite=True).on("2020-10-20 7:30pm")
        self.assertEqual(get_job_list()[0], "30 19 20 10 * ls")

        cronpi.run_by_date("ls", isOverwrite=True).on("2020-10-20 17:30")
        self.assertEqual(get_job_list()[0], "30 17 20 10 * ls")
