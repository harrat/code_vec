def test_tukio_task_attrs_from_coro(self):
        """
        Check `TukioTask` instances always have `.holder` and `.uid` attributes
        when `tukio_factory` is set.
        """
        self.loop.set_task_factory(tukio_factory)

        # Create a task from a simple coroutine
        task = asyncio.ensure_future(my_coro_task(None))
        self.assertTrue(hasattr(task, 'holder'))
        self.assertTrue(hasattr(task, 'uid'))
        self.assertIsNone(task.holder)
        self.assertIsInstance(task.uid, str)
        # uuid4() always returns a 36-chars long ID
        self.assertEqual(len(task.uid), 36)

        # Create a task from a task holder (inherited from `TaskHolder`)
        task = asyncio.ensure_future(self.holder.do_it('foo'))
        self.assertTrue(hasattr(task, 'holder'))
        self.assertTrue(hasattr(task, 'uid'))
        self.assertIs(task.holder, self.holder)
        self.assertIsInstance(task.uid, str)
        # uuid4() always returns a 36-chars long ID
        self.assertEqual(len(task.uid), 36)
        self.assertEqual(task.uid, task.holder.uid)

        # Create a task from a basic task holder
        task = asyncio.ensure_future(self.basic.mycoro())
        self.assertTrue(hasattr(task, 'holder'))
        self.assertTrue(hasattr(task, 'uid'))
        self.assertIs(task.holder, self.basic)
        self.assertIsInstance(task.uid, str)
        # uuid4() always returns a 36-chars long ID
        self.assertEqual(len(task.uid), 36)
