def test_write_dict(self):
        path = self.root.make_file()
        write_dict(OrderedDict([('foo', 1), ('bar', 2)]), path, linesep=None)
        assert list(read_lines(path)) == ['foo=1', 'bar=2']
