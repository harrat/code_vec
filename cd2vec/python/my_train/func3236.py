def test_new_task(self):
        """
        Can create new asyncio tasks from the task template
        """
        self.loop.set_task_factory(tukio_factory)

        # Create a task from a registered task holder
        task_tmpl = TaskTemplate('my-task-holder')
        task = task_tmpl.new_task('dummy-data', loop=self.loop)
        self.assertIsInstance(task, TukioTask)
        self.assertEqual(task.inputs, 'dummy-data')

        # Also works with a registered coroutine
        task_tmpl = TaskTemplate('dummy-coro')
        task = task_tmpl.new_task(data='junk-data', loop=self.loop)
        self.assertIsInstance(task, TukioTask)
        self.assertEqual(task.inputs, 'junk-data')
