@pytest.mark.run(order=5)
def test_can_uninstall():
    subprocess.run('jupyter nbextension uninstall --py jupytemplate --sys-prefix', shell=True).check_returncode()

