def test_no_token_raises_exception(self):
        with self.assertRaises(IdentixOneException):
            Client(token=None, version=1)
