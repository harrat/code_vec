def test_version_date(self):
        # Only check if date is populated with "something"
        runner('git tag -a 1.0.0 -m "test message"')
        v = relic.release.get_info()
        assert isinstance(v.date, str)
        assert v.date
