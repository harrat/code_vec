def test_derive_key_invalid_base_key(self):
        """
        Test that the right error is thrown when an object not suitable for
        key derivation is provided as the base key with a DeriveKey request.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        invalid_key = pie_objects.OpaqueObject(
            b'\x01\x02\x04\x08\x10\x20\x40\x80',
            enums.OpaqueDataType.NONE
        )
        e._data_session.add(invalid_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        payload = payloads.DeriveKeyRequestPayload(
            object_type=enums.ObjectType.SECRET_DATA,
            unique_identifiers=[str(invalid_key.unique_identifier)]
        )

        args = (payload, )
        self.assertRaisesRegex(
            exceptions.InvalidField,
            "Object 1 is not a suitable type for key derivation. Please "
            "specify a key or secret data.",
            e._process_derive_key,
            *args
        )
