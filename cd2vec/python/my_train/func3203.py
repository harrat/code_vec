def functional_test_symlink_created(self):
        git_file, git_dir = get_clean_dir()
        cwd = git_dir.name
        source = git_file.name
        target = source + ".foo"
        try:
            pwstore.symlink(cwd, source, target)
            assert os.path.islink(target)
        finally:
            git_dir.cleanup()
