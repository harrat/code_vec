def test_reverse(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        self.assertFalse(audiofile.is_reversed)
        all_mfcc_pre = audiofile.all_mfcc
        audiofile.reverse()
        all_mfcc_post = audiofile.all_mfcc
        self.assertTrue(audiofile.is_reversed)
        self.assertTrue((all_mfcc_pre == all_mfcc_post[:, ::-1]).all())
        audiofile.reverse()
        all_mfcc_post = audiofile.all_mfcc
        self.assertFalse(audiofile.is_reversed)
        self.assertTrue((all_mfcc_pre == all_mfcc_post).all())
