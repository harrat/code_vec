def test_compress_fileobj(self):
        path = self.root.make_file()
        with open(path, 'wt') as o:
            o.write('foo')

        f = open(path, 'rb')
        try:
            gzfile = compress_file(f, compression='gz')
            assert gzfile == Path(str(path) + '.gz')
            assert path.exists()
            assert gzfile.exists()
            with gzip.open(gzfile, 'rt') as i:
                assert i.read() == 'foo'
        finally:
            f.close()

        gzpath = Path(str(path) + '.gz')
        gzfile = gzip.open(gzpath, 'w')
        try:
            assert gzpath == compress_file(path, gzfile, compression=True)
        finally:
            gzfile.close()
        assert path.exists()
        assert gzpath.exists()
        with gzip.open(gzpath, 'rt') as i:
            assert i.read() == 'foo'
