def test_resolve_exe(self):
        exe = self.root.make_file(suffix=".exe")
        exe_name = exe.name
        path = EXECUTABLE_CACHE.resolve_exe([exe_name])
        assert path is None
        EXECUTABLE_CACHE.cache.clear()
        EXECUTABLE_CACHE.add_search_path(exe.parent)
        path = EXECUTABLE_CACHE.resolve_exe([exe_name])
        assert path is not None
        assert exe == path[0]
