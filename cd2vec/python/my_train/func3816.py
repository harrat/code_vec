@mock.patch('six.moves.urllib.request.urlopen', side_effect=request_mock)
  def test_query_no_api_key(self, urlopen):
    del os.environ[utils._ENV_VAR_API_KEY]
    # Issue a dummy SPARQL query that tells the mock to not expect a key
    self.assertEqual(dc.query(_SPARQL_NO_KEY), [])
