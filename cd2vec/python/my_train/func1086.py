def test_pysal_multi_polygon(self):
        # East Rancho Dominguez CDP, MultiPolygon
        geodata = self.conn.mapservice.query(layer=36, where="PLACE=21034")
        numpy.testing.assert_allclose(
            geodata.total_bounds,
            numpy.array([-13158648.204, 4012917.1367, -13156433.948, 4016243.6976]),
        )
