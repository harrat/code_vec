def test_aggregated_getitem_method(mocked_aggregated):
    """Test FlaskMultiRedis aggregated __getitem__ method."""

    assert mocked_aggregated['pattern'] == 'node2'
    mocked_aggregated._aggregator._redis_nodes = []
    assert mocked_aggregated['pattern'] is None

