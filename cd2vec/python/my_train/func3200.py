def test_cell_any():
    filt = FilterCellAny()
    assert filt._type == 'CELL'
    assert filt.label == 'CELL, Always True'
    assert filt._obs == []
    assert filt('whatever')  # always True

