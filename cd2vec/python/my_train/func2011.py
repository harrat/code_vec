def test_checkfile(self, gpx_path, dummy_files):
        tmp_dir = dummy_files['dir']

        assert cmdline.checkfile(gpx_path)
        assert cmdline.checkfile(tmp_dir)
        assert cmdline.checkfile('nonexistent-file') is False
