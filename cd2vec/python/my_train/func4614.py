def test_get_dists(tmpvenv):
    dist_names = [dist.project_name for dist in tmpvenv.get_distributions()]
    assert all(pkg in dist_names for pkg in ['setuptools', 'pip', 'wheel']), dist_names

