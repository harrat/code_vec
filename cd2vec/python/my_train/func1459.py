def test_get_requests_multiple(self):
        self._make_request('https://github.com/')
        self._make_request('https://www.wikipedia.org/')

        requests = self.client.get_requests()

        self.assertEqual(2, len(requests))
