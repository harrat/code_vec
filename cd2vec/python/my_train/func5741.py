def test_flight(self):
        self.fp.add_flight(a="BRU", b="BCN")
        self.assertEqual(len(self.fp.steps), 1)
        self.assertAlmostEqual(self.fp.emissions, 116, delta=5)
