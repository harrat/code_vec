def test_momentary_chdir_fail(tmpdir):
    """Test wkr.os.momentary_chdir."""
    start_dir = os.getcwd()
    path = tmpdir.join('newdir')
    assert not path.exists()
    with pytest.raises(OSError):
        with momentary_chdir(path.strpath):
            # this is never executed
            assert False
    assert os.getcwd() == start_dir

