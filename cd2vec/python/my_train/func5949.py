def test_flex_alloc_maintenance_nodes(make_flexible_job):
    job = make_flexible_job('maint')
    job.options = ['--partition=p4']
    prepare_job(job)
    assert job.num_tasks == 4

