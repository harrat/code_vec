def test_derive_key_non_derivable_base_key(self):
        """
        Test that the right error is thrown when an object suitable for
        key derivation but not marked as such is provided as the base key
        with a DeriveKey request.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        base_key = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            128,
            (
                b'\x00\x01\x02\x03\x04\x05\x06\x07'
                b'\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F'
            ),
            [enums.CryptographicUsageMask.ENCRYPT]
        )
        e._data_session.add(base_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        payload = payloads.DeriveKeyRequestPayload(
            object_type=enums.ObjectType.SECRET_DATA,
            unique_identifiers=[str(base_key.unique_identifier)]
        )

        args = (payload, )
        self.assertRaisesRegex(
            exceptions.InvalidField,
            "The DeriveKey bit must be set in the cryptographic usage mask "
            "for object 1 for it to be used in key derivation.",
            e._process_derive_key,
            *args
        )
