def test_set_real_wave_path(self):
        aligner = DTWAligner(real_wave_path=self.AUDIO_FILE)
        self.assertIsNotNone(aligner.real_wave_mfcc)
        self.assertIsNone(aligner.synt_wave_mfcc)
        self.assertIsNotNone(aligner.real_wave_path)
        self.assertIsNone(aligner.synt_wave_path)
