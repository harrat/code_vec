def test_repository_filter_branches_by_newer_than(mock_repository, mock_branch_pool):
    all_branches = set(mock_branch_pool.values())
    result = mock_repository.filter_branches(all_branches, newer_than=1)
    assert result == [mock_branch_pool['newer_than']]

