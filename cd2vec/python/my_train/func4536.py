def test_flex_alloc_exclude_nodes_cmd(make_flexible_job):
    job = make_flexible_job('all',
                            sched_access=['--constraint=f1'],
                            sched_exclude_nodelist='nid00001')
    prepare_job(job)
    assert job.num_tasks == 8

