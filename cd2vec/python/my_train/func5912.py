@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_delete_reference_006(snippy):
        """Delete reference with uuid.

        Try to delete content with uuid that does not match to any content.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        cause = snippy.run(['snippy', 'delete', '-u', '1234567'])
        assert cause == 'NOK: cannot find content with content uuid: 1234567'
        Content.assert_storage(content)
