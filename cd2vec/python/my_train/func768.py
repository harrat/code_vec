def test_toString(self):
		expect = u'{"@context":"'+model.factory.context_uri+'","id":"http://lod.example.org/museum/InformationObject/collection","type":"InformationObject","_label":"Test Object"}'
		outs = model.factory.toString(self.collection)
		self.assertEqual(expect, outs)
