@unittest.skipIf(platform.system() == "Windows" or platform.system() == "windows", "test not supported on windows")
    def test_load_default_config(self):
        try:
            os.remove(self.config_file_path)
            del os.environ["SECURENATIVE_API_KEY"]
            del os.environ["SECURENATIVE_API_URL"]
            del os.environ["SECURENATIVE_INTERVAL"]
            del os.environ["SECURENATIVE_MAX_EVENTS"]
            del os.environ["SECURENATIVE_TIMEOUT"]
            del os.environ["SECURENATIVE_AUTO_SEND"]
            del os.environ["SECURENATIVE_DISABLE"]
            del os.environ["SECURENATIVE_LOG_LEVEL"]
            del os.environ["SECURENATIVE_FAILOVER_STRATEGY"]
        except FileNotFoundError:
            pass
        except KeyError:
            pass

        options = ConfigurationManager.load_config(None)

        self.assertIsNotNone(options)
        self.assertIsNone(options.api_key)
        self.assertEqual(options.api_url, "https://api.securenative.com/collector/api/v1")
        self.assertEqual(str(options.interval), "1000")
        self.assertEqual(options.timeout, "1500")
        self.assertEqual(str(options.max_events), "1000")
        self.assertEqual(str(options.auto_send), "True")
        self.assertEqual(str(options.disable), "False")
        self.assertEqual(options.log_level, "CRITICAL")
        self.assertEqual(options.fail_over_strategy, FailOverStrategy.FAIL_OPEN.value)
