@staticmethod
    def test_cli_import_snippet_022(snippy):
        """Import all snippet resources.

        Try to import snippet from file which file format is not supported.
        In this case supported file extension is part of the filename when
        the file is in unknown format.
        """

        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            cause = snippy.run(['snippy', 'import', '-f', '.text.bar'])
            assert cause == 'NOK: cannot identify file format for file: .text.bar'
            Content.assert_storage(None)
            mock_file.assert_not_called()
