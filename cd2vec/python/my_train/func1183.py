@mock.patch('time.sleep')
def test_delay_amount(mocked_time):
    try:
        tracer = ThundraTracer.get_instance()
        with tracer.start_active_span(operation_name='foo', finish_on_close=True) as scope:
            span = scope.span
            delay = 370
            lsl = LatencyInjectorSpanListener(delay=delay)
