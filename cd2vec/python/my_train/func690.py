@staticmethod
    @pytest.mark.usefixtures('default-references', 'export-time')
    def test_cli_export_reference_009(snippy):
        """Export all references.

        Export defined reference based on uuid.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Reference.REGEXP
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-u', '32cd5827-b6ef-4067-b5ac-3ceac07dde9f'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './references.mkdn', content)
