def test_nonexisting(self):
        '''
        Test that non-existing license cannot be found
        '''
        with pytest.raises(KeyError):
            licenraptor.find('This is not an existing SPDX identifier')
