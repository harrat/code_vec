def test_trace_with_default_configs(trace):
    tracer = ThundraTracer.get_instance()
    nodes = tracer.get_spans()
    count = 0
    for key in nodes:
        if key.operation_name == 'func':
            count += 1

    assert count == 0

    traceable, func = trace
    func(arg='test')

    active_span = None
    nodes = tracer.get_spans()
    for key in nodes:
        if key.operation_name == 'func':
            count += 1
            active_span = key

    args = active_span.get_tag('method.args')
    assert args is None

    return_value = active_span.get_tag('method.return_value')
    assert return_value is None

    error = active_span.get_tag('error')
    assert error is None

    assert count == 1
    assert traceable.trace_args is False
    assert traceable.trace_return_value is False
    assert traceable.trace_error is True

