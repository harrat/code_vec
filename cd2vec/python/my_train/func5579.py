@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'import-content-utc')
    def test_cli_import_solution_027(snippy):
        """Import solutions from text template.

        Import solution template that does not have any changes to file header
        located at the top of content data. This tests a scenario where user
        does not bother to do any changes to header which has the solution
        metadata. Because the content was changed the import operation must
        work.
        """

        template = Const.NEWLINE.join(Solution.TEMPLATE)
        template = template.replace('## Description', '## Description changed')
        content = {
            'data': [
                Content.dump_dict(template)
            ]
        }
        content['data'][0]['uuid'] = Content.UUID2
        file_content = Content.get_file_content(Content.TEXT, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '-f', './solution-template.txt'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './solution-template.txt', mode='r', encoding='utf-8')
