def test_main():
    expected = ["Flask", "Jinja2", "MarkupSafe", "Werkzeug", "itsdangerous"]

    install_dist('Flask')
    for name in expected:
        assert has_dist(name)

    pip_autoremove.main(['-y', 'Flask'])
    for name in expected:
        assert not has_dist(name)

