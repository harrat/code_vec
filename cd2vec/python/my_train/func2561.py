def test_great(self):
        r = ruler.FileSizeRule("> 1m")
        self.assertFalse(r.match(self.f1))
        self.assertTrue(r.match(self.f2))

        r = ruler.FileSizeRule("> 1024k")
        self.assertFalse(r.match(self.f1))
        self.assertTrue(r.match(self.f2))

        r = ruler.FileSizeRule(">0.0009765625g")
        self.assertFalse(r.match(self.f1))
        self.assertTrue(r.match(self.f2))
