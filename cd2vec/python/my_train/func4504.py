def test_disable_telnet(psr):
    last_update_timestamp = 0
    runtime = 0
    cwd = os.getcwd()
    os.chdir(cst.DEMO_PROJECT_PATH)
    try:
        version = '1.5.1' if (cst.ON_WINDOWS or on_fedora) else '1.6.0'
        cmd = 'pip install scrapy==%s' % version
        cst.sub_process(cmd, block=True)
        for name in ['enable_telnet', 'disable_telnet']:
            enable_telnet = name == 'enable_telnet'
            parser = psr(execute_main=False, enable_telnet=enable_telnet)
            # Test MyTelnet.verify_log_file_path()
            if enable_telnet:
                for _name in ['6023', '6024']:
                    _log_file = os.path.join(cst.DEMO_PROJECT_LOG_FOLDER_PATH, '%s.log' % _name)
                    cst.write_text(_log_file, TELNET_151_PORT_16023.replace(':16023', ':%s' % _name))
