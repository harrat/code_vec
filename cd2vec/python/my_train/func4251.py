def test_name_user_inheritance():
    class MyBaseTest(rfm.RegressionTest):
        def __init__(self, a, b):
            self.a = a
            self.b = b

    class MyTest(MyBaseTest):
        def __init__(self):
            super().__init__(1, 2)

    test = MyTest()
    assert 'test_name_user_inheritance.<locals>.MyTest' == test.name

