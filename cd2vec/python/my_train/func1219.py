def test_lappeenranta(self):
        now = datetime.now(tz=tzutc())

        f = FMI(place='Lappeenranta')
        for point in f.observations():
            assert point.time < now
            assert isinstance(point.temperature, float)
