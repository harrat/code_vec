def test_get_on_the_air():
    shows = isle.show.get_on_the_air()
    assert inspect.isgenerator(shows)
    show = next(shows)
    assert isinstance(show, Show)

