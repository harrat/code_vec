def test_dataset_io():
    """Ensure that I/O functions works correctly."""
    with TemporaryDirectory(dir='.') as temp_dir:
        filename = Path(temp_dir) / 'test.nc'
