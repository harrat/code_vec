def test_reservation(queue):
    j1 = queue.submit_job("default", "abc123", print, "Hallo")
    assert j1 is not None
    j2 = queue.submit_job("default", "abc123", print, "Hallo")
    assert j2 is None
    assert queue.job_status("abc123") == "queued"

