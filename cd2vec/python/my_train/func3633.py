@pytest.mark.remote
def test_find_datasets_type():
    runsets = datasets.find_datasets(type='run')
    assert 'O1' in runsets
    run_regex = re.compile(r'\A([OS]\d+|BKGW\d{6})(_\d+KHZ)?(_[RV]\d+)?\Z')
    for dset in runsets:
        assert run_regex.match(dset)

    assert datasets.find_datasets(type='badtype') == []

