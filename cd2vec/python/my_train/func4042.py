@freeze_time('2017-06-21')
def test_used_flag_includes_inactive_by_default(cli, entries_file, alias_config):
    entries_file.write("""20.06.2017
    inactive1 1 Play ping-pong
    """)

    stdout = cli('alias', ['list', '--used'])

    assert 'inactive1' in stdout

