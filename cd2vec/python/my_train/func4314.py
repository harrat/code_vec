def test_postsresult_from_post_with_metas(self, dcard, metas):
        posts = dcard.posts(metas[0]).get(comments=False, links=False).result()
        assert len(posts) == 1
        assert posts[0] is not None
