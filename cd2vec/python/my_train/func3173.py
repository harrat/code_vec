def test_contains():
	header = ljson.base.generic.Header(header_descriptor)
	table = ljson.base.mem.Table(header, data)
	
	from io import StringIO
	fio = StringIO()
	table.save(fio)
	fio.seek(0)
	table = ljson.base.disk.Table.from_file(fio)

	assert {"lname": "griffin"} in table
	assert not {"lname": "griffindor"} in table

def test_delete(tmpdir):
	import copy
	header = ljson.base.generic.Header(header_descriptor)
	table = ljson.base.mem.Table(header, copy.copy(data))
	import os
