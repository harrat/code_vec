def test_host_settemplate(self, host_load_data):
        host = host_load_data
        templates = list()
        with open(resource_dir / 'test_host_template.json') as htlp:
            tmp = json.load(htlp)
            for tlp in tmp:
                print(tlp)
                templates.append(HostTemplate(tlp))

        data = dict()
        data['action'] = 'settemplate'
        data['object'] = 'HOST'
        data['values'] = [
            "mail-uranus-frontend",
            "OS-Linux-SNMP-custom|OS-Linux-SNMP-Disk-/"]

        with patch('requests.post') as patched_post:
            host.settemplate(templates)
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
