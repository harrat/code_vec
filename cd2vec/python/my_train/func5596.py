@responses.activate
def test_cli_with_errors_only(runner, valid_urls):
    reset_globals()
    urls = (
        ('http://www.test1.com', 400),
        ('http://www.test2.com', 500),
        ('http://www.test3.com', 103),
    )
    for url, code in urls:
        responses.add(responses.HEAD, url, status=code)

    with runner.isolated_filesystem():
        with open('errors.md', 'w') as f:
            f.write(valid_urls)
