def test_register_without_create(self):  # noqa: D102
        test_run = invoke_runner(
            plugin_register.main,
            ['--name', 'plug', '--path', '%CONFIG_DIR%'])
        assert test_run.result.exit_code == 1
        assert 'Could not find a python module' in test_run.result.output
