def test_lca_two(tmpdir):
    # setup saga and get init hash
    os.chdir(tmpdir)
    run_cmd("saga init")
    # make a branch a and b
    run_cmd("saga branch a")
    run_cmd("saga branch b")
    # make one commit on a
    run_cmd("saga checkout a")
    random_file("filea")
    run_cmd("saga add filea")
    a_blurb = run_cmd("saga commit -m \"ack\"")
    a_commit = blurb_to_hash(str(tmpdir), a_blurb)
    # make one commit on b
    run_cmd("saga checkout b")
    random_file("fileb")
    run_cmd("saga add fileb")
    b_blurb = run_cmd("saga commit -m \"ack\"")
    b_commit = blurb_to_hash(str(tmpdir), b_blurb)
    # merge from b to a
    b_merge_blurb = run_cmd("saga merge a")
    b_merge_hash = blurb_to_hash(str(tmpdir), b_merge_blurb)
    # merge from a to b
    run_cmd("saga checkout a")
    # TODO: we cannot do this currently, as we have no ability to 
    # merge in remote branches -- or merge in by commit hash
    # (which we should have...)
    return
    a_merge_blurb = run_cmd("saga merge b")
    a_merge_hash = blurb_to_hash(str(tmpdir), a_merge_blurb)
    # check that we get two common ancestors
    commit_graph = CommitGraph(os.path.join(tmpdir, ".saga", "commits"))
    lcas = commit_graph.least_common_ancestors(b_merge_hash, a_merge_hash)
    assert len(lcas) == 2
    assert a_commit in lcas and b_commit in lcas

