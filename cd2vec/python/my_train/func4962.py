def test_empty_archived_log(travis):

    job = travis.job(81891594)

    assert job.build.id == 81891579

    assert job.log.job_id == 81891594



    log = job.log

    assert log._body is None

    assert log.body == ''

    assert log._body == ''

