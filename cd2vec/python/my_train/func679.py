def test_pypi_500(mocked_responses, tmpdir):
    wheeldir = str(tmpdir)
    mocked_responses.add(responses.GET, INDEX_URL + "/numpy/", status=500)
    repo = PyPIRepository(INDEX_URL, wheeldir)

    with pytest.raises(requests.HTTPError):
        repo.get_candidates(pkg_resources.Requirement.parse("numpy"))
