def test_host_setparent(self, host_load_data):
        host = host_load_data
        with open(resource_dir / 'test_host_parent.json') as parent:
            parents = Host(json.load(parent))

        data = dict()
        data['action'] = 'setparent'
        data['object'] = 'HOST'
        data['values'] = ["mail-uranus-frontend", "mail-neptune-frontend"]

        with patch('requests.post') as patched_post:
            host.setparent(parents)
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
def test_host_setparent(self, host_load_data):
        host = host_load_data
        with open(resource_dir / 'test_host_parent.json') as parent:
            parents = Host(json.load(parent))

        data = dict()
        data['action'] = 'delparent'
        data['object'] = 'HOST'
        data['values'] = ["mail-uranus-frontend", "mail-neptune-frontend"]

        with patch('requests.post') as patched_post:
            host.deleteparent(parents)
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
def test_host_setparent(self, host_load_data):
        host = host_load_data
        with open(resource_dir / 'test_host_hostgroup.json') as hg:
            hgs = HostGroup(json.load(hg))

        data = dict()
        data['action'] = 'sethostgroup'
        data['object'] = 'HOST'
        data['values'] = ["mail-uranus-frontend", "centreon-prod"]

        with patch('requests.post') as patched_post:
            host.sethostgroup(hgs)
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
