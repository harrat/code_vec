def test_deploy_daily(self):
        """
        deploy_daily
        """        
        cronpi.run_every_day("ls", isOverwrite=True).on("7:30")
        self.assertEqual(get_job_list()[0], "30 7 * * * ls")

        cronpi.run_every_day("ls", isOverwrite=True).on("7:30pm")
        self.assertEqual(get_job_list()[0], "30 19 * * * ls")

        cronpi.run_every_day("ls", isOverwrite=True).on("17:30")
        self.assertEqual(get_job_list()[0], "30 17 * * * ls")
