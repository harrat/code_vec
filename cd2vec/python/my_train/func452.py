def test_jointplot_zerominFalse(jp_data):
    jg1 = viz.jointplot(x="A", y="C", data=jp_data, zeromin=False, one2one=False)
    nptest.assert_array_equal(numpy.round(jg1.ax_joint.get_xlim()), [-4, 4])
    nptest.assert_array_equal(numpy.round(jg1.ax_joint.get_ylim()), [-7, 23])
    return jg1.fig

