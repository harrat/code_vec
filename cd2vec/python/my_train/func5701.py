def test_rowcount_execute():
    cu.execute("delete from tests")
    cu.execute("insert into tests(id, name) values (1, 'test_rowcount_execute')")
    cu.execute("insert into tests(id, name) values (2, 'test_rowcount_execute')")
    cu.execute("update tests set name='foo'")
    assert cu.rowcount == 2

