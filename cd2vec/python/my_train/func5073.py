def test_defaults_to_user_cache_dir_for_storage_location(self):
        repo = AnnotationRepository()
        assert (
            repo.http_folder.local_dir
            == Path.home() / ".cache" / "epic_kitchens" / "v1.5.0"
        )
