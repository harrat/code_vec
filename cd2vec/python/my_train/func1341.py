def test_tag_post_ids():
    m = """\
---
title: Blog post {}
author: Famous author
published: 2015-01-{}
tags: tag1, tag2
public: yes
chronological: yes
kind: writing
summary: This is a summary
---
"""
    assert len(DB.posts.all()) == 20
    with open(os.path.join(CONFIG['content_root'], 'e.md'), 'w') as f:
        f.write(m.format(25, 25))
    with open(os.path.join(CONFIG['content_root'], 'f.md'), 'w') as f:
        f.write(m.format(27, 27))

    e1 = Entry(os.path.join(CONFIG['content_root'], 'e.md'))
    e1.tags

    e2 = Entry(os.path.join(CONFIG['content_root'], 'f.md'))
    e2.tags
    assert len(DB.posts.all()) == 22
    #assert e1.tags[0].posts == e2.tags[0].posts
    e1.render()
    [t.render() for t in e1.tags]

    assert len(DB.posts.all()) == 22

