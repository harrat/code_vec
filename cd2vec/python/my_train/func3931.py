def test_host_setparam(self, host_load_data):
        host = host_load_data

        data = dict()
        data['action'] = 'setparam'
        data['object'] = 'HOST'
        data['values'] = ["mail-uranus-frontend", "notes", "tested"]

        with patch('requests.post') as patched_post:
            host.setparam("notes", "tested")
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
