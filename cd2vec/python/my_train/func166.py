def test_metrics_v2_api(client, mocker,
                        mock_puppetdb_environments,
                        mock_puppetdb_default_nodes):
    # starting with v6.9.1 they changed the metric API to v2
    # and a totally different format
    query_data = {
        'version': [{'version': '6.9.1'}],
        'metrics-list': [
            {
                'validate': {
                    'data': {
                        'value': {
                            'java.lang': {
                                'type=Memory': {}
                            },
                            'puppetlabs.puppetdb.population': {
                                'name=num-nodes': {}
                            },
                        }
                    },
                    'checks': {
                    }
                }
            }
        ]
    }
    dbquery = MockDbQuery(query_data)
    mocker.patch.object(app.puppetdb, '_query', side_effect=dbquery.get)
    rv = client.get('/metrics')

    soup = BeautifulSoup(rv.data, 'html.parser')
    assert soup.title.contents[0] == 'Puppetboard'
    ul_list = soup.find_all('ul', attrs={'class': 'ui list searchable'})
    assert len(ul_list) == 1
    vals = ul_list[0].find_all('a')

    assert len(vals) == 2
    assert vals[0].string == 'java.lang:type=Memory'
    assert vals[1].string == 'puppetlabs.puppetdb.population:name=num-nodes'

    assert rv.status_code == 200

