def test_writer_writes_to_filename(self):
        fname = '/tmp/testobj{}.obj'.format(uuid4())
        self.writer.dump(fname)
        self.assertTrue(os.path.exists(fname))
