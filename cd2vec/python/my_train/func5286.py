def test_text(self):
        inputs = [verif.input.get_input("verif/tests/files/file_sigdig.txt")]
        data = verif.data.Data(inputs=inputs)
        metric = verif.metric.Obs()

        output = verif.output.Standard(metric)
        output.text(data)

        output.axis = verif.axis.Time()
        output.text(data)
