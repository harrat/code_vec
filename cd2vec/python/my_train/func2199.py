def testDirs(tmpdir, zipPkg):

    # 'dirs' without a zip
    tmpDirPath = str(tmpdir.realpath())
    tmpdir.mkdir("aaa")
    tmpdir.mkdir('bbb')
    pkgPath = pypkg.PkgPath(tmpDirPath)
    assert sorted(['aaa', 'bbb']) == sorted(pkgPath.dirs())
    pkgPath = pypkg.PkgPath(joinpath(tmpDirPath, 'notexisting'))
    assert list(pkgPath.dirs()) == []

    # 'exists', 'isfile', 'isdir' without a zip
    assert pypkg.PkgPath(joinpath(tmpDirPath, 'aaa')).exists()
    assert pypkg.PkgPath(joinpath(tmpDirPath, 'aaa')).isdir()
    assert not pypkg.PkgPath(joinpath(tmpDirPath, 'aaa')).isfile()
    assert not pypkg.PkgPath(joinpath(tmpDirPath, 'notexisting')).exists()
    assert not pypkg.PkgPath(joinpath(tmpDirPath, 'notexisting')).isdir()
    assert not pypkg.PkgPath(joinpath(tmpDirPath, 'notexisting')).isfile()

    # 'dirs' with a zip
    pkgPath = pypkg.PkgPath(MODULE_ZIP_PATH, zipPkg)
    assert sorted(pkgPath.dirs()) == sorted(['pkg1', 'pkg2'])
    pkgPath = pypkg.PkgPath(joinpath(MODULE_ZIP_PATH, 'pkg2'), zipPkg)
    assert list(pkgPath.dirs()) == ['pkg3']
    pkgPath = pypkg.PkgPath(joinpath(MODULE_ZIP_PATH, 'notexisting'), zipPkg)
    assert list(pkgPath.dirs()) == []

    # 'exists', 'isfile', 'isdir' with a zip
    path = joinpath(MODULE_ZIP_PATH, 'pkg1')
    assert pypkg.PkgPath(path, zipPkg).exists()
    assert pypkg.PkgPath(path, zipPkg).isdir()
    assert not pypkg.PkgPath(path, zipPkg).isfile()
    path = joinpath(MODULE_ZIP_PATH, 'notexisting')
    assert not pypkg.PkgPath(path, zipPkg).exists()
    assert not pypkg.PkgPath(path, zipPkg).isdir()
    assert not pypkg.PkgPath(path, zipPkg).isfile()

def testFiles(tmpdir, zipPkg):
