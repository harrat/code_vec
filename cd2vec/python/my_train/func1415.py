def test_verbosity_with_check(run_reframe):
    returncode, stdout, stderr = run_reframe(
        more_options=['-vvvvv'],
        system='testsys',
        checkpath=['unittests/resources/checks/hellocheck.py']
    )
    assert '' != stdout
    assert '--- Logging error ---' not in stdout
    assert 'Traceback' not in stdout
    assert 'Traceback' not in stderr
    assert 0 == returncode

