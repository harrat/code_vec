def test_dev(capsys, basic_conf):
    tmpdir, local, conf = basic_conf

    # Test Dev runs (with full benchmark suite)
    ret = tools.run_asv_with_conf(conf, 'dev', '--quick', '-e',
                                  _machine_file=join(tmpdir, 'asv-machine.json'))
    assert ret is None
    text, err = capsys.readouterr()

    # time_with_warnings failure case
    assert re.search("File.*time_exception.*RuntimeError", text, re.S)
    assert re.search(r"time_secondary.track_value\s+42.0", text)

    # Check that it did not clone or install
    assert "Cloning" not in text
    assert "Installing" not in text

