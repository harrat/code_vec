def test_middle_begin_good2(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        audiofile.middle_begin = 10
        self.assertEqual(audiofile.all_length, 1331)
        self.assertEqual(audiofile.head_length, 10)
        self.assertEqual(audiofile.middle_length, 1321)
        self.assertEqual(audiofile.tail_length, 0)
