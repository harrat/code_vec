def test_keyed_cached(cache_obj):
    for _ in range(10):
        cache_obj.test3(8, 0)

    cache_obj.test4()  # Shouldn't be cached

    assert len(c) == 1

    key = list(c.keys())[0]
    assert key == 'asdf'

    c.clear()
    assert len(c) == 0

    # Make sure the cached function is properly wrapped
    assert cache_obj.test3.__doc__ == 'running test3'

