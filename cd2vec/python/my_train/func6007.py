def test_put_fail_validation(self):
        """Test HTTP PUTing a resource that fails user-defined validation."""
        response = self.app.put('/tracks/999',
                content_type='application/json',
                data=json.dumps(
                    {'Name': 'Some New Album',
                      'GenreId': 1,
                      'AlbumId': 1,
                      'MediaTypeId': 1,
                      'Milliseconds': 343719,
                      'TrackId': 999,
                      'UnitPrice': 0.99,}))
        assert response.status_code == 403
