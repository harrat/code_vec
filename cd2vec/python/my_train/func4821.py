@pytest.mark.vcr()
def test_workbench_vuln_outputs_filter_type_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vuln_outputs(19506, filter_type=123)
