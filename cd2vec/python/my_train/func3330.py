def test_get_attribute_index_from_managed_object(self):
        """
        Test that an attribute's index can be retrieved from a given managed
        object.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        symmetric_key = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b'',
            masks=[enums.CryptographicUsageMask.ENCRYPT,
                   enums.CryptographicUsageMask.DECRYPT]
        )
        certificate = pie_objects.X509Certificate(
            b''
        )

        e._data_session.add(symmetric_key)
        e._data_session.add(certificate)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._set_attribute_on_managed_object(
            symmetric_key,
            (
                "Application Specific Information",
                [
                    attributes.ApplicationSpecificInformation(
                        application_namespace="Example Namespace",
                        application_data="Example Data"
                    )
                ]
            )
        )
        e._set_attribute_on_managed_object(
            symmetric_key,
            (
                "Name",
                [
                    attributes.Name(
                        name_value=attributes.Name.NameValue("Name 1")
                    ),
                    attributes.Name(
                        name_value=attributes.Name.NameValue("Name 2")
                    )
                ]
            )
        )
        e._set_attribute_on_managed_object(
            symmetric_key,
            (
                "Object Group",
                [
                    primitives.TextString(
                        "Example Group",
                        tag=enums.Tags.OBJECT_GROUP
                    )
                ]
            )
        )

        # Test getting the index for an ApplicationSpecificInfo attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Application Specific Information",
            attributes.ApplicationSpecificInformation(
                application_namespace="Example Namespace",
                application_data="Example Data"
            )
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Application Specific Information",
            attributes.ApplicationSpecificInformation(
                application_namespace="Wrong Namespace",
                application_data="Wrong Data"
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a CertificateType attribute
        index = e._get_attribute_index_from_managed_object(
            certificate,
            "Certificate Type",
            primitives.Enumeration(
                enums.CertificateType,
                enums.CertificateType.X_509,
                tag=enums.Tags.CERTIFICATE_TYPE
            )
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            certificate,
            "Certificate Type",
            primitives.Enumeration(
                enums.CertificateType,
                enums.CertificateType.PGP,
                tag=enums.Tags.CERTIFICATE_TYPE
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a CryptographicAlgorithm attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Cryptographic Algorithm",
            primitives.Enumeration(
                enums.CryptographicAlgorithm,
                enums.CryptographicAlgorithm.AES,
                tag=enums.Tags.CRYPTOGRAPHIC_ALGORITHM
            )
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Cryptographic Algorithm",
            primitives.Enumeration(
                enums.CryptographicAlgorithm,
                enums.CryptographicAlgorithm.RSA,
                tag=enums.Tags.CRYPTOGRAPHIC_ALGORITHM
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a CryptographicLength attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Cryptographic Length",
            primitives.Integer(
                0,
                tag=enums.Tags.CRYPTOGRAPHIC_LENGTH
            )
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Cryptographic Length",
            primitives.Integer(
                128,
                tag=enums.Tags.CRYPTOGRAPHIC_LENGTH
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a CryptographicUsageMasks attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Cryptographic Usage Mask",
            primitives.Integer(
                12,
                tag=enums.Tags.CRYPTOGRAPHIC_USAGE_MASK
            )
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Cryptographic Usage Mask",
            primitives.Integer(
                0,
                tag=enums.Tags.CRYPTOGRAPHIC_USAGE_MASK
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a InitialDate attribute
        date = e._get_attribute_from_managed_object(
            symmetric_key,
            "Initial Date"
        )
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Initial Date",
            primitives.DateTime(
                date,
                tag=enums.Tags.INITIAL_DATE
            )
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Initial Date",
            primitives.DateTime(
                9999,
                tag=enums.Tags.INITIAL_DATE
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a Name attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Name",
            attributes.Name(
                name_value=attributes.Name.NameValue("Name 2")
            )
        )
        self.assertEqual(2, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Name",
            attributes.Name(
                name_value=attributes.Name.NameValue("Name 3")
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a ObjectGroup attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Object Group",
            primitives.TextString(
                "Example Group",
                tag=enums.Tags.OBJECT_GROUP
            )
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Object Group",
            primitives.TextString(
                "Invalid Group",
                tag=enums.Tags.OBJECT_GROUP
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a ObjectType attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Object Type",
            primitives.Enumeration(
                enums.ObjectType,
                enums.ObjectType.SYMMETRIC_KEY,
                tag=enums.Tags.OBJECT_TYPE
            )
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Object Type",
            primitives.Enumeration(
                enums.ObjectType,
                enums.ObjectType.CERTIFICATE,
                tag=enums.Tags.OBJECT_TYPE
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a OperationPolicyName attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Operation Policy Name",
            primitives.TextString(
                "default",
                tag=enums.Tags.OPERATION_POLICY_NAME
            )
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Operation Policy Name",
            primitives.TextString(
                "invalid",
                tag=enums.Tags.OPERATION_POLICY_NAME
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a Sensitive attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Sensitive",
            primitives.Boolean(
                False,
                tag=enums.Tags.SENSITIVE
            )
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Sensitive",
            primitives.Boolean(
                True,
                tag=enums.Tags.SENSITIVE
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a State attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "State",
            primitives.Enumeration(
                enums.State,
                enums.State.PRE_ACTIVE,
                tag=enums.Tags.STATE
            )
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "State",
            primitives.Enumeration(
                enums.State,
                enums.State.ACTIVE,
                tag=enums.Tags.STATE
            )
        )
        self.assertIsNone(index)

        # Test getting the index for a UniqueIdentifier attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Unique Identifier",
            primitives.TextString(value="1", tag=enums.Tags.UNIQUE_IDENTIFIER)
        )
        self.assertEqual(0, index)
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Unique Identifier",
            primitives.TextString(value="9", tag=enums.Tags.UNIQUE_IDENTIFIER)
        )
        self.assertIsNone(index)

        # Test getting the index for an unsupported attribute
        index = e._get_attribute_index_from_managed_object(
            symmetric_key,
            "Archive Date",
            None
        )
        self.assertIsNone(index)
