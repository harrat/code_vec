def test_fetch_ic(self):
        """
        Fetch two classes that are parent-child
        and test that the child has a higher IC
        than the parent
        :return:
        """
        classes = ['HP:0000739', 'HP:0000740']
        ic_dict = self.owlsim2_api.get_profile_ic(classes)
        assert isinstance(ic_dict['HP:0000740'], float)
        assert ic_dict['HP:0000740'] > ic_dict['HP:0000739']
