@pytest.mark.parametrize("data", [{"up_to": 30}, {"page": 1}])
def test_newly_released(data):
    data["userpass"] = get_userpass()
    rv = client.post("/new", data=json.dumps(data))
    rsp = json.loads(rv.data.decode("utf-8"))
    assert len(rsp) > 0

