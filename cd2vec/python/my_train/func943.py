def test_tabular():
    s = AvroTableSource(urlpath=path)
    disc = s.discover()
    assert list(disc['dtype']) == columns
    assert s.container == 'dataframe'
    out = s.read()
    assert out.equals(expected)

    s = AvroTableSource(urlpath=[path, path2])
    out = s.read().reset_index(drop=True)
    assert out.equals(pd.concat([expected, expected], ignore_index=True))

    s = AvroTableSource(urlpath=pathstar)
    out = s.read().reset_index(drop=True)
    assert out.equals(pd.concat([expected, expected], ignore_index=True))

