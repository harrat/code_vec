def test_items(region):
    region['foo'] = 'bar'
    assert region['foo'] == 'bar'
    assert region._region_cache.conn.hget(region.name, 'foo') is not None
    del region['foo']
    assert pytest.raises(KeyError, lambda: region['foo'])

