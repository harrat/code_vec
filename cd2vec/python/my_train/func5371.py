def test_heatcapacity_nomass_construction(self):
        self.idf.initreadtxt(no_mass)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        expected = m.Thickness * m.Specific_Heat * m.Density * 0.001 * 2
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            assert almostequal(c.heatcapacity, expected, places=2)
            assert almostequal(c.heatcapacity, 240, places=2)
            assert issubclass(w[-1].category, UserWarning)
