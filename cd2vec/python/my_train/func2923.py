def test_subregions(region_cache):
    r = region_cache.region('abc.xyz')
    assert '{region_cache._root_name}.abc'.format(region_cache=region_cache) in region_cache._regions
    assert '{region_cache._root_name}.abc.xyz'.format(region_cache=region_cache)  in region_cache._regions
    assert 'abc.xyz' not in region_cache._regions
    assert 'xyz' not in region_cache._regions

    r1 = region_cache.region('xml', timeout=60)
    assert r1._timeout == 60
    r2 = r1.region('json')
    assert r2._timeout == 60

