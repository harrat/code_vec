@pytest.mark.vcr()
def test_workbench_asset_vuln_output_plugin_id_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.asset_vuln_output(str(uuid.uuid4()), 'something here')
