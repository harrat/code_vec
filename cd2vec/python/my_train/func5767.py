def test_function_wrapper_sanity(trace_transport, ):
    retval = 'success'

    @epsagon.python_wrapper(name='test-func')
    def wrapped_function(event, context):
        return retval

    assert wrapped_function('a', 'b') == 'success'

    assert len(trace_transport.last_trace.events) == 1

    event = trace_transport.last_trace.events[0]
    assert event.resource['type'] == 'python_function'
    assert event.resource['name'] == 'test-func'
    assert event.resource['metadata']['python.function.return_value'] == retval
    assert event.error_code == 0

