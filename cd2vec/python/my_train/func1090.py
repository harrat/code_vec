def test_set_real_wave_mfcc(self):
        af = AudioFileMFCC(self.AUDIO_FILE)
        aligner = DTWAligner(real_wave_mfcc=af)
        self.assertIsNotNone(aligner.real_wave_mfcc)
        self.assertIsNone(aligner.synt_wave_mfcc)
        self.assertIsNone(aligner.real_wave_path)
        self.assertIsNone(aligner.synt_wave_path)
