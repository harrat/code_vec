def test_get_config(self):
        with test_utils.lauch(
            self.base_config,
            get_process=True,
            options=["--get-config", "pidfile"]
        ) as p:
            self.assertEqual(p.wait(), 0)
            self.assertEqual(p.stdout.read(), self.base_config["pidfile"].encode())
        with test_utils.lauch(
            self.base_config,
            get_process=True,
            options=["--get-config", "sqlite_config.database"]
        ) as p:
            self.assertEqual(p.wait(), 0)
            self.assertEqual(
                p.stdout.read(),
                self.base_config["sqlite_config"]["database"].encode()
            )
        with test_utils.lauch(
            self.base_config,
            get_process=True,
            options=["--get-config", "foo"]
        ) as p:
            self.assertEqual(p.wait(), 1)
        with test_utils.lauch(
            None,
            get_process=True,
            options=["--get-config", "pidfile"]
        ) as p:
            self.assertEqual(p.wait(), 0)
            self.assertEqual(
                p.stdout.read(),
                b'/var/run/policyd-rate-limit/policyd-rate-limit.pid'
            )
