def test_import_mda(self):

        save_dir = '{}/'.format(test_path)
        save_name = 'test_deleting5'
        mda_path = '{}/test_mda'.format(test_path)
        file = '{}/test_deleting5.h5'.format(test_path)

        import_mda(mda_path, save_dir, save_name)

        first_path = h5path_exists(file, 'mda')
        second_path = h5path_exists(file, 'mda/0291')
        third_path = h5path_exists(file, 'mda/0291/D01')

        self.assertTrue(first_path)
        self.assertTrue(second_path)
        self.assertTrue(third_path)

        h5delete_file(file)
