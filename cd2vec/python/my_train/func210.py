def test_main_options(self):
        """ Test main program with output options """
        fname = os.path.join(testpath, 'test_main-save.json')
        items = main(datetime='2020-01-01', save=fname, printcal=True, printmd=[],
                     collections=['sentinel-s2-l2a'], query=['eo:cloud_cover=0', 'data_coverage>80'])
        self.assertEqual(len(items), 212)
        self.assertTrue(os.path.exists(fname))
        os.remove(fname)
        self.assertFalse(os.path.exists(fname))
