def test_masked_length_no_explicit_run_vad(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        self.assertNotEqual(audiofile.masked_length, 0)
