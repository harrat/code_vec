@pytest.mark.vcr()
def test_agents_list_filter_value_typeerror(api):
    with pytest.raises(TypeError):
        api.agents.list(('distro', 'match', 1))
