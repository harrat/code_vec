def test_assoc_query():
    """
    reconstitution test
    """
    print("Making ont factory")
    factory = OntologyFactory()
    # default method is sparql
    print("Creating ont")
    ont = factory.create('pato')
    print("Creating assoc set")
    aset = AssociationSet(ontology=ont,
                        association_map={
                            'a': [],
                            'b': [EUPLOID],
                            'c': [Y_SHAPED],
                            'd': [EUPLOID, Y_SHAPED],
                        })

    rs = aset.query([],[])
    assert len(rs) == 4
    
    rs = aset.query([EUPLOID],[])
    assert len(rs) == 2
    assert 'b' in rs
    assert 'd' in rs

    rs = aset.query([EUPLOID, Y_SHAPED],[])
    assert len(rs) == 1
    assert 'd' in rs

    rs = aset.query([PLOIDY, SHAPE],[])
    assert len(rs) == 1
    assert 'd' in rs
    
    rs = aset.query([],[PLOIDY, SHAPE])
    assert len(rs) == 1
    assert 'a' in rs

    rs = aset.query([PLOIDY], [SHAPE])
    assert len(rs) == 1
    assert 'b' in rs

    rs = aset.query([EUPLOID], [Y_SHAPED])
    assert len(rs) == 1
    assert 'b' in rs

    rs = aset.query([EUPLOID], [PLOIDY])
    assert len(rs) == 0

    rs = aset.query([PLOIDY], [EUPLOID])
    assert len(rs) == 0

    rs = aset.query([QUALITY], [PLOIDY])
    assert len(rs) == 1
    assert 'c' in rs    

    rs = aset.query([SHAPE], [QUALITY])
    assert len(rs) == 0

    rs = aset.query([QUALITY], [QUALITY])
    assert len(rs) == 0

    for s1 in aset.subjects:
        for s2 in aset.subjects:
            sim = aset.jaccard_similarity(s1,s2)
            print("{} vs {} = {}".format(s1,s2,sim))
            if s1 == 'a' or s2 == 'a':
                assert sim == 0.0
            elif s1 == s2:
                assert sim == 1.0
            else:
                assert sim == aset.jaccard_similarity(s2,s1)

    terms1 = [QUALITY,PLOIDY,SHAPE]
    terms2 = [QUALITY,EUPLOID,Y_SHAPED]
    ilist = aset.query_intersections(terms1, terms2)
    print(str(ilist))

def test_enrichment():
    """
    enrichment
    """
    factory = OntologyFactory()
    ont = factory.create('pato')
