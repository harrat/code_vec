def test_compress_too_small_pdf(self):
        with tempfile.TemporaryDirectory(dir=self.trash_can.name) as tmpoutdir:
            mock_status_callback = Mock(return_value=None)
            pdf_file = create_temporary_files_with_suffixes(self.trash_can.name, files_per_suffix=1)[0]
            pdf_file.close()
            output_path = os.path.join(tmpoutdir, os.path.basename(pdf_file.name))
            pdfebc.core.compress_pdf(pdf_file.name, output_path,
                                     pdfebc.cli.GHOSTSCRIPT_BINARY_DEFAULT, mock_status_callback)
            expected_not_compressing_message = pdfebc.core.NOT_COMPRESSING.format(
                pdf_file.name, 0,
                pdfebc.core.FILE_SIZE_LOWER_LIMIT)
            expected_done_message = pdfebc.core.FILE_DONE.format(output_path)
            mock_status_callback.assert_any_call(expected_not_compressing_message)
            mock_status_callback.assert_any_call(expected_done_message)
