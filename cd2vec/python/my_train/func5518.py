def test_mkdir_p_single(tmpdir):
    """Test mkdir_p on creating a single directory."""
    path = tmpdir.join('newdir')
    assert not path.exists()
    mkdir_p(path.strpath)
    assert path.exists()

