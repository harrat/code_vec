def test_load_fooofgroup():

    file_name = 'test_fooofgroup_all'
    tfg = load_fooofgroup(file_name, TEST_DATA_PATH)

    assert isinstance(tfg, FOOOFGroup)

    # Check that all elements get loaded
    assert len(tfg.group_results) > 0
    for setting in OBJ_DESC['settings']:
        assert getattr(tfg, setting) is not None
    assert tfg.power_spectra is not None
    for meta_dat in OBJ_DESC['meta_data']:
        assert getattr(tfg, meta_dat) is not None

