def test_get_attribute(self):
        """Test simple HTTP GET"""
        response = self.get_response('/artists/1/Name', 200)
        assert json.loads(response.get_data(as_text=True))[u'Name'] == 'AC/DC'
