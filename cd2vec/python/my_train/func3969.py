@pytest.mark.vcr()
def test_workbench_vuln_info_filter_type_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vuln_info(19506, filter_type=123)
