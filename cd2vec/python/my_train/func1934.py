def test_get_doc():
    ConfigProvider.set(config_names.THUNDRA_TRACE_INTEGRATIONS_ELASTICSEARCH_PATH_DEPTH, '3')
    try:
        es = Elasticsearch(['one_host', 'another_host'], max_retries=0)
        es.get(index='test-index', doc_type='tweet', id=1)
    except ElasticsearchException as e:
        pass
    finally:
        tracer = ThundraTracer.get_instance()
        span = tracer.get_spans()[0]
        tracer.clear()
