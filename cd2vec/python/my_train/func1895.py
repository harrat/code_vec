@pytest.mark.remote
def test_find_datasets():
    sets = datasets.find_datasets()
    for dset in ('S6', 'O1', 'GW150914-v1', 'GW170817-v3'):
        assert dset in sets
    assert 'tenyear' not in sets
    assert 'history' not in sets

