@freeze_time('2014-01-20')
def test_status_ignored_not_mapped(cli, entries_file):
    entries_file.write("""20/01/2014
? unmapped 0800-0900 Play ping-pong
""")

    stdout = cli('status')
    assert line_in(
        "unmapped (ignored, inexistent alias) 1.00  Play ping-pong",
        stdout
    )
