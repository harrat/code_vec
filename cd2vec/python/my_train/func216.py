@timeout(5)
def test_inference_async_provide_id():
    rw = RunwayModel()

    @rw.command('test_command', inputs={ 'input': number }, outputs = { 'output': text })
    def test_command(model, inputs):
        time.sleep(0.5)
        yield 'hello world'

    ws = None
    proc = None

    try:
        os.environ['RW_NO_SERVE'] = '0'
        proc = Process(target=rw.run)
        proc.start()
