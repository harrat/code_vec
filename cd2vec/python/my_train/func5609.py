def test_concurrent_threads_landmask_generation():
  delete_mask()

  def f(i):
    print("launching instance:", i)
    l = Landmask(__concurreny_delay__ = 2., __no_retry__ = True)
    print("instance", i, "done")

  from concurrent.futures import ThreadPoolExecutor, wait
  with ThreadPoolExecutor(max_workers = 4) as exe:
    futures = [ exe.submit(f, i) for i in range(5) ]
    wait(futures)
