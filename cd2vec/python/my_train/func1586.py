@pytest.mark.optional
@pytest.mark.plotting
def test_plot_dag():

    class TestPostprocess(AbstractNode):
        @staticmethod
        def necessary_config(node_config):
            return set(['key1', 'key2'])
        def run(self, data_object):
            return data_object
    NodeFactory().register("TestPostprocess", TestPostprocess)

    class Testpipeline(AbstractNode):
        #def __init__(self, configuration, instance_name):
        #    pass
        @staticmethod
        def necessary_config(node_config):
            return set([])
        def run(self, data_object):
            return data_object
    NodeFactory().register("Testpipeline", Testpipeline)
    class TestCleanup(AbstractNode):
        @staticmethod
        def necessary_config(node_config):
            return set([])
        def run(self, data_object):
            return data_object
    NodeFactory().register("TestCleanup", TestCleanup)

    config = {
        "implementation_config":{
            "reader_config": {
                "csv_reader": {
                    "class": "CsvReader",
                    "filename": "some/path/to/file",
                    "destinations": ["pipeline1"]
                }
            },
            "pipeline_config": {
                "pipeline1": {
                    "class": "Testpipeline",
                    "destinations": ["decision_tree_model"]
                }
            },
            "model_config": {
                "decision_tree_model": {
                    "class": "SklearnClassifierModel",
                    "model_parameters": {},
                    "sklearn_classifier_name": "tree.DecisionTreeClassifier",
                    "grid_search_scoring": "roc_auc",
                    "cv_folds": 3,
                    "mode": "predict",
                    "destinations": ["nodename"]
                }
            },
            "postprocess_config": {
                "nodename": {
                    "class": "TestPostprocess",
                    "key1":"val1",
                    "key2":"val2",
                    "destinations": ["write_output"]
                }
            },
            "writer_config": {
                "write_output": {
                    "class": "CsvWriter",
                    "key": "read_data",
                    "dir": "cache",
                    "filename": "some/path/to/file.csv",
                    "destinations": ["donothingsuccess"]
                }
            },
            "cleanup_config": {
                "donothingsuccess": {
                    "class": "TestCleanup",
                }
            }
        }
    }

    cfilename = "test/test_dag_plotting.json"
    with open(cfilename, 'w') as f:
        jstyleson.dump(config, f)
    config = Configuration(config_location=cfilename)
    dag = config.dag
    dag.create_dag()

    filename = "test/test_dag_plotting.png"
    if os.path.exists(filename):
        os.remove(filename)

    dag.plot_dag(filename, traverser=ConfigLayerTraverser(config))

    assert os.path.exists(filename)

    if os.path.exists(cfilename):
        os.remove(cfilename)

    if os.path.exists(filename):
        os.remove(filename)

def test_upstream_keys1():
    config = {
        "implementation_config":{
            "reader_config":{
                "csv_reader": {
                    "class": "CsvReader",
                    "filename": "test/minimal.csv",
                    "destinations": ['csv_writer']
                }
            },
            "writer_config": {
                "csv_writer": {
                    "class": "CsvWriter",
                    "key":"test_data",
                    "dir": "cache",
                    "filename": "unittest_similar_recipes.csv"
                }
            }
        }
    }
    dag = ConfigurationDag(config['implementation_config'])
    dag.create_dag()
    ukeys = dag.upstream_keys('csv_writer')
    assert list(ukeys) == ['csv_reader']
