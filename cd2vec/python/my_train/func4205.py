def test_extract(self):
        r = chazutsu.datasets.Text8().download(directory=DATA_ROOT, test_size=10)
        self.assertTrue(r.train_file_path)
        self.assertTrue(r.test_file_path)
