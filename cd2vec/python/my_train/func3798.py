def test_github_repo_description():
    environment = Environment(extensions=['jinja2_github.GitHubRepoDescriptionExtension'])

    template = environment.from_string(
        "{% github_repo_description 'jcfr/jinja2-github' %}"
    )

    assert template.render().startswith("jinja2 extensions for rendering Github project properties")

