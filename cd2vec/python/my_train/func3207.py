@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_update_solution_008(snippy, edited_beats):
        """Update solution with ``--content`` option.

        Update solution based on content data.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.BEATS),
                Solution.NGINX
            ]
        }
        content['data'][0]['data'] = tuple([line.replace('## Description', '## updated desc') for line in content['data'][0]['data']])
        content['data'][0]['description'] = ''
        content['data'][0]['digest'] = '23312e20cb961d46b3fb0ac5a63dacfbb16f13a220b48250019977940e9720f3'
        edited_beats.return_value = Content.dump_text(content['data'][0])
        data = Const.NEWLINE.join(Solution.BEATS['data'])
        cause = snippy.run(['snippy', 'update', '--scat', 'solution', '-c', data, '--format', 'text', '--editor'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
