def test_format_header(self, mock_parser):
        format_kernel_header('image.fits', mock_parser, PIXSCALE)
        assert round(fits.getval('image.fits', 'CD1_1') * 3600, 1) == PIXSCALE
        assert round(fits.getval('image.fits', 'CD1_2') * 3600, 1) == 0.0
        assert round(fits.getval('image.fits', 'CD2_1') * 3600, 1) == 0.0
        assert round(fits.getval('image.fits', 'CD2_2') * 3600, 1) == PIXSCALE
