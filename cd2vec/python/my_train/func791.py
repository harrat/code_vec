@staticmethod
    @pytest.mark.usefixtures('import-kafka-mkdn', 'export-time')
    def test_cli_export_solution_038(snippy):
        """Export defined solution with digest.

        Export Markdown native solution. The solution data must not be
        surrounded with additional Markdown code blocks that are added for
        text content. The exported file name must be based on content
        metadata because the file name is not defined from command line.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Solution.KAFKA_MKDN
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-d', 'c54c8a896b94ea35'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, 'kubernetes-docker-log-driver-kafka.mkdn', content)
