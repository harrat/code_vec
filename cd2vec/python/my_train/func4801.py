def test_terminal(self):

        print("Testing utils.run_command")
        from watchme.utils import run_command
        from watchme.utils import which

        result = run_command("echo Las Papas Fritas")
        self.assertEqual(result["message"], "Las Papas Fritas\n")
        self.assertEqual(result["return_code"], 0)

        print("Testing utils.which")
        result = which("echo")
        self.assertEqual(result, "/bin/echo")
