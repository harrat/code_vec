def test_external_compatibility(self):
        """
            Test the compatibility of the current version of `EasyJWT` with externally created tokens.

            Expected Result: The verification succeeds without errors. All data is restored.
        """

        # Get the token. The version is not needed.
        token, _ = self.get_token_and_version_from_file('external.jwt')
        message = 'External Token'

        # Try to verify the token.
        with self.assertNotRaises(EasyJWTError, message):
            token_object = ExternalCompatibilityToken.verify(token, KEY, ISSUER, AUDIENCE)

        # Verify the claim set.
        self.assertIsNotNone(token_object, message)
        self.verify_claim_set(token_object, message)
