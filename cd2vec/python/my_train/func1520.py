@freeze_time("2018-01-01")
def test_profile_track_charge() -> None:
    """Test the profile_track_charge method."""
    m = MixpanelTrack(
        settings={
            "mixpanel.profile_properties": "pyramid_mixpanel.tests.test_track.FooProfileProperties"
        },
        distinct_id="foo",
    )

    m.profile_track_charge(100)
    assert len(m.api._consumer.mocked_messages) == 1
    assert m.api._consumer.mocked_messages[0].endpoint == "people"
    assert m.api._consumer.mocked_messages[0].msg == {
        "$token": "testing",
        "$time": 1514764800000,
        "$distinct_id": "foo",
        "$append": {"$transactions": {"$amount": 100}},
    }

    m.profile_track_charge(222, props={FooProfileProperties.foo: "Bar"})
    assert len(m.api._consumer.mocked_messages) == 2
    assert m.api._consumer.mocked_messages[1].endpoint == "people"
    assert m.api._consumer.mocked_messages[1].msg == {
        "$token": "testing",
        "$time": 1514764800000,
        "$distinct_id": "foo",
        "$append": {"$transactions": {"Foo": "Bar", "$amount": 222}},
    }
