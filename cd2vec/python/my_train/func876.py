@pytest.mark.usefixtures('concurrency_enabled_and_disabled')
def test_multiexception_api():
    with pytest.raises(MultiException) as exc:
        MultiObject([0, 5]).call(lambda i: 10 // i)

    failed, sucsessful = exc.value.futures

    assert failed.done()
    with pytest.raises(ZeroDivisionError):
        failed.result()
    assert isinstance(failed.exception(), ZeroDivisionError)

    assert sucsessful.done()
    assert sucsessful.result() == 2
    assert sucsessful.exception() is None

