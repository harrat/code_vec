@patch('citrine.resources.file_link.boto3_client')
@patch('citrine.resources.file_link.open')
@patch('citrine.resources.file_link.os.stat')
@patch('citrine.resources.file_link.os.path.isfile')
def test_upload(mock_isfile, mock_stat, mock_open, mock_boto3_client, collection, session):
    """Test signaling that an upload has completed and the creation of a FileLink object."""
    StatStub = namedtuple('StatStub', ['st_size'])

    mock_isfile.return_value = True
    mock_stat.return_value = StatStub(st_size=22300)
    mock_open.return_value.__enter__.return_value = 'Random file contents'
    mock_boto3_client.return_value = FakeS3Client({'VersionId': '3'})

    # It would be good to test these, but the values assigned are not accessible
    dest_names = {
        'foo.txt': 'text/plain',
        'foo.TXT': 'text/plain',  # Capitalization in extension is fine
        'foo.bar': 'application/octet-stream'  # No match == generic binary
    }
    file_id = '12345'
    version = '13'

    # This is the dictionary structure we expect from the upload completion request
    file_info_response = {
        'file_info': {
            'file_id': file_id,
            'version': version
        }
    }
    uploads_response = {
        's3_region': 'us-east-1',
        's3_bucket': 'temp-bucket',
        'temporary_credentials': {
            'access_key_id': '1234',
            'secret_access_key': 'abbb8777',
            'session_token': 'hefheuhuhhu83772333',
        },
        'uploads': [
            {
                's3_key': '66377378',
                'upload_id': '111',
            }
        ]
    }

    for dest_name in dest_names:
        session.set_responses(uploads_response, file_info_response)
        file_link = collection.upload(dest_name)

        url = 'projects/{}/datasets/{}/files/{}/versions/{}'\
            .format(collection.project_id, collection.dataset_id, file_id, version)
        assert file_link.dump() == FileLink(dest_name, url=url).dump()

    assert session.num_calls == 2 * len(dest_names)

