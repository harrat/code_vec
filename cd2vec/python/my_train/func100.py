def test_get_conll2000(self):
        raw = get_conll2000()
        self.assertIn('train', raw)
        self.assertEqual(len(raw['train']), 8_937)
        self.assertIn('test', raw)
        self.assertEqual(len(raw['test']), 2_013)
