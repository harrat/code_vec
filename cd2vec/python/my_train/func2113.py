def test_describe_process(self):
        self.create_default_experiment()
        cluster = Cluster.new('tmux', server_name=_TEST_SERVER)

        with self.assertRaises(ValueError):
            cluster.describe_process('bad_exp', 'hello')
            cluster.describe_process('exp', 'bad_process')
            cluster.describe_process('exp', None)
            cluster.describe_process('exp', None, process_group_name='group')
            cluster.describe_process('exp', '')
            cluster.describe_process('exp', '', process_group_name='group')
            cluster.describe_process('exp', 'hello')
            cluster.describe_process('exp', 'bad_process',
                                      process_group_name='group')
            cluster.describe_process('exp', 'alone',
                                      process_group_name='group')
        process_dict = cluster.describe_process('exp', 'hello',
                                                process_group_name='group')
        self.assertDictEqual(process_dict, { 'status': 'live' })
        process_dict = cluster.describe_process('exp', 'alone')
        self.assertDictEqual(process_dict, { 'status': 'live' })
