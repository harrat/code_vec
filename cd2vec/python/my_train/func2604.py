def test_bot_ai(self, bot: BotAI):
        bot._game_info.map_ramps, bot._game_info.vision_blockers = bot._game_info._find_ramps_and_vision_blockers()
        assert bot.main_base_ramp  # Test if any ramp was found

        # Clear cache for expansion locations, recalculate and time it
        if hasattr(bot, "_cache_expansion_locations"):
            delattr(bot, "_cache_expansion_locations")
        t0 = time.perf_counter()
        bot._find_expansion_locations()
        t1 = time.perf_counter()
        print(f"Time to calculate expansion locations: {t1-t0} s")

        # TODO: Cache all expansion positions for a map and check if it is the same
        # BelShirVestigeLE has only 10 bases - perhaps it should be removed since it was a WOL / HOTS map
        assert len(bot.expansion_locations_list) >= 10, f"Too few expansions found: {len(bot.expansion_locations_list)}"
        # Honorgrounds LE has 24 bases
        assert (
            len(bot.expansion_locations_list) <= 24
        ), f"Too many expansions found: {len(bot.expansion_locations_list)}"
        # On N player maps, it is expected that there are N*X bases because of symmetry, at least for maps designed for 1vs1
        assert (
            len(bot.expansion_locations_list) % (len(bot.enemy_start_locations) + 1) == 0
        ), f"{bot.expansion_locations_list}"
        # Test if bot start location is in expansion locations
        assert bot.townhalls.random.position in set(
            bot.expansion_locations_list
        ), f'This error might occur if you are running the tests locally using command "pytest test/", possibly because you are using an outdated cache.py version, but it should not occur when using docker and pipenv.\n{bot.townhalls.random.position}, {bot.expansion_locations_list}'
        # Test if enemy start locations are in expansion locations
        for location in bot.enemy_start_locations:
            assert location in set(bot.expansion_locations_list), f"{location}, {bot.expansion_locations_list}"
        # Each expansion is supposed to have at least one geysir and 6-12 minerals
        for expansion, resource_positions in bot.expansion_locations_dict.items():
            assert isinstance(expansion, Point2)
            assert isinstance(resource_positions, Units)
            if resource_positions:
                assert isinstance(resource_positions[0], Unit)
            # Neon violet has bases with just 6 resources. I think that was the back corner base with 4 minerals and 2 vespene
            # Odyssey has bases with 10 mineral patches and 2 geysirs
            assert (
                6 <= len(resource_positions) <= 12
            ), f"{len(resource_positions)} resource fields in one base on map {bot.game_info.map_name}"

        assert bot.owned_expansions == {bot.townhalls.first.position: bot.townhalls.first}
