@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'default-references-utc')
    def test_cli_import_reference_006(snippy):
        """Import all reference resources.

        Import all references from txt file without specifying the reference
        category. File name and format are extracted from command line
        ``--file`` option. File extension is '*.txt' in this case.
        """

        content = {
            'data': [
                Content.deepcopy(Reference.GITLOG),
                Content.deepcopy(Reference.REGEXP)
            ]
        }
        content['data'][0]['uuid'] = Content.UUID1
        content['data'][1]['uuid'] = Content.UUID2
        file_content = Content.get_file_content(Content.TEXT, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '-f', './all-references.txt'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './all-references.txt', mode='r', encoding='utf-8')
