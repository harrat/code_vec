def test_create_update_delete_email_forwarding(self):
        result = api.create_email_forwarding(emailBox=sample2.emailBox, emailTo=sample2.emailTo)
        email_forwarding = result.email_forwarding
        self.assertEqual(email_forwarding, sample2)

        result = api.update_email_forwarding(emailBox=sample3.emailBox,
                                                    emailTo=sample3.emailTo)
        email_forwarding = result.email_forwarding
        self.assertEqual(email_forwarding, sample3)

        result = api.delete_email_forwarding(emailBox=sample3.emailBox)
        self.assertEqual(result.status_code, 200)
