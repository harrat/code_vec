def test_met_request_should_return_dict():
    session = requests.Session()
    adapter = requests_mock.Adapter()
    session.mount('mock', adapter)

    json_data = '{"name": "Bob"}'

    with requests_mock.mock() as m:
        m.get('https://www.met.ie/api/weather/national', text=json_data)
        result = metcli.met_request('weather/national')
