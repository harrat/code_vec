def test_normal(self):
        r1 = ruler.AndRule([
            {
                "name": "nihao"
            },
            {
                "file_size": "> 1024b"
            }
        ])

        r2 = ruler.AndRule([
            {
                "name": "nihao"
            },
            {
                "file_size": "<= 2047b"
            }
        ])

        self.assertTrue(r1.match("/tmp/nihao"))
        self.assertFalse(r2.match("/tmp/nihao"))
