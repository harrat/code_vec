def test_references_placed_in_markdown_file(self):
        md_file = MdUtils(file_name="Test_file", title="")

        text = "mdutils library"
        reference_tag = "mdutils library"
        link = "https://github.com/didix21/mdutils"

        expected_value = "\n\n\n[mdutils library0][mdutils library0]\n" \
                         "[mdutils library1][mdutils library1]\n" \
                         "[mdutils library2][mdutils library2]\n" \
                         "[mdutils library3][mdutils library3]\n" \
                         "\n\n\n" \
                         "[mdutils library0]: https://github.com/didix21/mdutils0\n" \
                         "[mdutils library1]: https://github.com/didix21/mdutils1\n" \
                         "[mdutils library2]: https://github.com/didix21/mdutils2\n" \
                         "[mdutils library3]: https://github.com/didix21/mdutils3\n"

        for i in range(4):
            md_file.write(md_file.new_reference_link(
                link=link + str(i),
                text=text + str(i),
                reference_tag=reference_tag + str(i)))
            md_file.write('\n')

        md_file.create_md_file()

        created_data = MarkDownFile.read_file('Test_file.md')

        self.assertEqual(expected_value, created_data)
