@mock.patch.object(opener, 'has_command', return_value=False)
def test_open_with_associated_app_linux_without_xdg(mocked_command):
    if not sys.platform.startswith('linux'):
        return

    # test _find_mime()
    with mock.patch('os.path.splitext') as mocked_splitext:
        opener._get_associated_application_linux('anyfile')
        mocked_command.assert_called_with('xdg-mime')
        mocked_splitext.assert_any_call('anyfile')

    # test _find_desktop
    with mock.patch('os.path.exists', return_value=False) as mocked_exists:
        with mock.patch('subprocess.check_output') as mocked_subprocess:
            opener._get_associated_application_linux('dummy.txt')
            mocked_subprocess.assert_not_called()
        assert mocked_exists.mock_any_call('/usr/share/applications/mimeinfo.cache')
