def test_increment_epoch(self):
        os.environ["RELEASE_TYPE"] = "epoch"
        v1 = VersionUtils.increment(self.v1)
        v2 = VersionUtils.increment(self.v2)
        v3 = VersionUtils.increment(self.v3)
        v4 = VersionUtils.increment(self.v4)
        v5 = VersionUtils.increment(self.v5)
        v6 = VersionUtils.increment(self.v6)
        self.assertEqual(v1, "2!1.0.0")
        self.assertEqual(v2, "1!1.0.0")
        self.assertEqual(v3, "1!1.0.0")
        self.assertEqual(v4, "1!1.0.0")
        self.assertEqual(v5, "1!1.0.0")
        self.assertEqual(v6, "1!1.0.0")
