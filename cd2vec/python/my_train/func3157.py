def test_conceptual_parts(self):
		r = model.Right()
		r2 = model.Right()
		self.assertRaises(model.DataError, r.__setattr__, 'part', r2)
		r.c_part = r2
		self.assertTrue(r2 in r.c_part)

		vocab.conceptual_only_parts()
		r3 = model.Right()
		r4 = model.Right()
		r3.part = r4
		self.assertTrue(r4 in r3.c_part)
		self.assertTrue("part" in model.factory.toJSON(r3))
		self.assertTrue(r4 in r3.part)
