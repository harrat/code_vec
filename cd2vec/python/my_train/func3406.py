def test_read_dict(self):
        path = self.root.make_file()
        with open(path, 'wt') as o:
            o.write("# This is a comment\n")
            o.write("foo=1\n")
            o.write("bar=2\n")
        d = read_dict(path, convert=int, ordered=True)
        assert len(d) == 2
        assert d['foo'] == 1
        assert d['bar'] == 2
        assert list(d.items()) == [('foo', 1), ('bar', 2)]
