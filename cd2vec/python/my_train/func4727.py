def test_sub2ind(self):
        ind = sub2ind(self.mat.shape, self.rows, self.cols)
        self.assertTrue(np.allclose(self.mat[self.rows, self.cols], self.mat.ravel()[ind]))
