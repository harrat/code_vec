def test_ivrs_parser_ivrs_full(self):
        # finally the big daddy... load it up!
        IvrsParserTest.ivhd_10h.addDTEEntry(IvrsParserTest.dte_01h)
        IvrsParserTest.ivhd_10h.addDTEEntry(IvrsParserTest.dte_02h)
        IvrsParserTest.ivhd_10h.addDTEEntry(IvrsParserTest.dte_03h)
        IvrsParserTest.ivhd_10h.addDTEEntry(IvrsParserTest.dte_00h)

        IvrsParserTest.ivhd_11h.addDTEEntry(IvrsParserTest.dte_42h)
        IvrsParserTest.ivhd_11h.addDTEEntry(IvrsParserTest.dte_43h)
        IvrsParserTest.ivhd_11h.addDTEEntry(IvrsParserTest.dte_46h)
        IvrsParserTest.ivhd_11h.addDTEEntry(IvrsParserTest.dte_47h)
        IvrsParserTest.ivhd_11h.addDTEEntry(IvrsParserTest.dte_48h)

        IvrsParserTest.ivhd_40h.addDTEEntry(IvrsParserTest.dte_f0h_0)
        IvrsParserTest.ivhd_40h.addDTEEntry(IvrsParserTest.dte_f0h_1)
        IvrsParserTest.ivhd_40h.addDTEEntry(IvrsParserTest.dte_f0h_2)

        IvrsParserTest.ivrs.addIVHDEntry(IvrsParserTest.ivhd_10h)
        IvrsParserTest.ivrs.addIVMDEntry(IvrsParserTest.ivmd_20h)
        IvrsParserTest.ivrs.addIVHDEntry(IvrsParserTest.ivhd_11h)
        IvrsParserTest.ivrs.addIVMDEntry(IvrsParserTest.ivmd_21h)
        IvrsParserTest.ivrs.addIVHDEntry(IvrsParserTest.ivhd_40h)
        IvrsParserTest.ivrs.addIVMDEntry(IvrsParserTest.ivmd_22h)

        IvrsParserTest.ivrs.DumpInfo()
        IvrsParserTest.ivrs.ToXmlElementTree()

        ivrs_byte = IvrsParserTest.ivrs.Encode()
        ivrs2 = IVRS_TABLE(ivrs_byte)
        self.assertEqual(ivrs2.Encode(), ivrs_byte)
