@pytest.mark.parametrize('key, has_parq, columns', [
    ('microarray', True, None),
    ('pacall', True, None),
    ('annotation', False, annot_cols),
    ('ontology', False, ont_cols),
    ('probes', False, probe_cols),
])
def test_readfiles(testfiles, key, has_parq, columns):
    for d, fn in flatten_dict(testfiles, key).items():
        func = getattr(io, 'read_{}'.format(key))

        # check file (CSV + parquet) exist
        assert op.exists(fn)
        if has_parq and io.use_parq:
            assert op.exists(fn.rpartition('.csv')[0] + '.parq')

        # check loading from filepath
        data = func(fn, parquet=True) if has_parq else func(fn)
        assert isinstance(data, pd.DataFrame)

        # check loading from dataframe (should return same object)
        data2 = func(data)
        assert id(data) == id(data2)

        # check that copy parameter works as expected
        data3 = func(data, copy=True)
        assert isinstance(data3, pd.DataFrame)
        assert id(data) != id(data3)

        # confirm columns are as expected
        if columns is not None:
            assert np.all(columns == data.columns)

        # confirm errors
        with pytest.raises(TypeError):
            func(1)

        with pytest.raises(TypeError):
            func([1, 2, 3])

        with pytest.raises(FileNotFoundError):
            func('notafile')
