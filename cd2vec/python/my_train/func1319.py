@pytest.mark.sandbox_disable
def test__get_env_dir(fake_project):
    assert npmenv._get_env_dir(fake_project['proj_dir']) == fake_project['env_dir']

