def test_should_change_separators(self):
        """ Should change separators. """
        res = self.client.get('/')
        autodoc.separators = '*'
        autodoc.parse('GET /', res)
        var = {
            'response_content_type': 'application/json',
            'response_body': '{\n  "response": "index"\n}',
            'describe': 'GET /',
            'request': 'GET /',
            'params': '',
            'status_code': 200,
            'target_url': 'http://localhost:80',
            'describe_separators': '*****'
        }
        for k, v in iteritems(autodoc.vars[0]):
            self.assertEqual(v, var[k])

        autodoc.clear()
