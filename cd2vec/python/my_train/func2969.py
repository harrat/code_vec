@staticmethod
    @pytest.mark.usefixtures('default-references', 'export-time')
    def test_cli_export_reference_015(snippy):
        """Export defined reference with search keyword.

        Try to export reference based on search keyword that cannot befound.
        """

        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--sall', 'notfound', '-f', 'defined-reference.yaml', '--scat', 'reference'])
            assert cause == 'NOK: cannot find content with given search criteria'
            mock_file.assert_not_called()
