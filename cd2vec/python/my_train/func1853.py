@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_api_search_reference_008(server):
        """Search reference with digets.

        Send GET /api/snippy/rest/references/{id} to get explicit reference
        based on digest. In this case the reference is found.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '768'
        }
        expect_body = {
            'meta': {
                'count': 1,
                'limit': 20,
                'offset': 0,
                'total': 1
            },
            'data': {
                'type': 'reference',
                'id': Reference.GITLOG_UUID,
                'attributes': Storage.gitlog
            },
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/references/' + Reference.GITLOG_UUID
            }
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/references/5c20',
            headers={'accept': 'application/json'})
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
