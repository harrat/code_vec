def test_translator_false_input(client):
    try:
        translator(app=None)
    except Exception as e:
        assert type(e) == AttributeError
    try:
        translator(app=app, cache=200)
    except Exception as e:
        assert type(e) == AttributeError
    remove(eng.file_name)

