def test_delete_not_supported(self):
        """Test DELETEing a resource for an endpoint that doesn't support it."""
        response = self.app.delete('/playlists/1')
        assert response.status_code == 403
