@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_output_dump(fixture, request, tmpdir):
    from pathlib import Path
    import shutil

    engine = request.getfixturevalue(fixture)
    dirpath = Path(str(tmpdir))
    subdir = dirpath / 'test'
    expected_exception = OSError if PY2 else FileExistsError
    expected_files = set('a.txt b c d.txt e.gif dirA/A dirA/B dirB/C'.split())

    job = engine.launch('alpine',
                        'mkdir dirA dirB && touch a.txt b c d.txt e.gif dirA/A dirA/B dirB/C')
    print(job.rundata)
    job.wait()
    job.dump_all_outputs(str(dirpath), exist_ok=True, update_references=False)
    _verify_output_dump(dirpath, expected_files, job, outputs_updated=False)

    # test that exception raised if directory exists and exist_ok is False
    subdir.mkdir()
    with pytest.raises(expected_exception):
        job.dump_all_outputs(str(subdir), exist_ok=False)

    # test that directory created if it doesn't exist
    shutil.rmtree(str(subdir))
    job.dump_all_outputs(str(subdir), exist_ok=False, update_references=True)
    assert subdir.is_dir()
    _verify_output_dump(subdir, expected_files, job, outputs_updated=True)

