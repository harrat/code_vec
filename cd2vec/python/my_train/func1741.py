def test_gaode_reverse():
    """ Expected result :
        http://restapi.amap.com/v3/geocode/regeo?output=xml&location=116.310003,39.991957&key=<???key>&radius=1000&extensions=all
    """
    g = geocoder.gaode(place, method='reverse', key='0716e5809437f14e3dd0793a5c6d2b13')
    assert g.ok
    assert g.country == u'??'
    assert g.state == u'???'
    assert g.city == u'???'
    assert g.street == u'UBP??'

