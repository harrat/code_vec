@pytest.mark.parametrize(
        "registered", [MyListener().call_on_event, lambda event: print(event.data)]
    )
    def test_it_allows_to_register(self, registered, capsys):
        py_event_dispatcher = EventDispatcher()
        py_event_dispatcher.register("foo.bar", registered)
        py_event_dispatcher.dispatch(Event("foo.bar", {"a": "b"}))

        captured = capsys.readouterr()

        assert captured.out == "{'a': 'b'}\n"
