def test_provider_decorated(self):
        inj = Injector(use_decorators=True)
        assert inj.get_instance("value") == 3
        assert isinstance(inj.get_instance(Thing), Thing)
