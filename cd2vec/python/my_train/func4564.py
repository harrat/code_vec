def test_reset(run, tmpenvdir, monkeypatch):
    "Resetting an env var with an empty file"
    monkeypatch.setattr(os, "execvpe", functools.partial(mocked_execvpe, monkeypatch))
    tmpenvdir.join("RESET").write("")
    os.environ["RESET"] = "test3"
    with py.test.raises(Response):
        run("envdir", str(tmpenvdir), "ls")
    assert os.environ["DEFAULT"] == "test"
    with py.test.raises(KeyError):
        assert os.environ["RESET"] == "test3"
