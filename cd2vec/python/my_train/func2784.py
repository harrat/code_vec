def test_process_read(self):
        with Process(('echo', 'foo'), stdout=PIPE) as p:
            assert b'foo\n' == p.read()
        with open_('|echo foo', 'rt') as p:
            assert 'foo\n' == p.read()
