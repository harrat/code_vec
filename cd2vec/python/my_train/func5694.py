def test_set_okta_password(monkeypatch):
    """Test whether data sent is the same as data returned."""
    from tokendito import helpers, settings
    import getpass

    monkeypatch.setattr(getpass, 'getpass', lambda: 'pytest_patched')
    val = helpers.set_okta_password()

    assert val == 'pytest_patched'
    assert settings.okta_password == 'pytest_patched'

