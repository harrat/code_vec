def test_log_debug():
    logger_name = 'test_name'
    TestMagicLogger = MagicLogger(logger_name=logger_name, file_name='test_output.txt', host=None)
    test_message = 'test_message'
    TestMagicLogger.debug(test_message)

    with open('test_output.txt', 'r') as f:
        log_split = f.read().split(' - ')
        assert log_split[3].rstrip() == test_message
        assert log_split[2] == 'DEBUG'
        assert log_split[1] == logger_name

    os.remove('test_output.txt')

def test_log_info():
    logger_name = 'test_name'
    TestMagicLogger = MagicLogger(logger_name=logger_name, file_name='test_output.txt', host=None)
    test_message = 'test_message'
    TestMagicLogger.info(test_message)
