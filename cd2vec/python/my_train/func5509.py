def test_error_on_option_without_section():
    with pytest.warns(config.UnusedOptionWarning) as w:
        parse('''
            acme-server = https://acme.example.org/directory
            [account]
            [mgmt]
            ''')
    assert 'acme-server' in str(w[-1].message)
    assert 'https://acme.example.org/directory' in str(w[-1].message)

