def test_execution_modes(run_reframe):
    returncode, stdout, stderr = run_reframe(
        checkpath=[],
        environs=[],
        local=False,
        mode='unittest'
    )
    assert 'Traceback' not in stdout
    assert 'Traceback' not in stderr
    assert 'FAILED' not in stdout
    assert 'PASSED' in stdout
    assert 'Ran 1 test case' in stdout

