def test_safe_checks(self):
        path = self.root.make_file(permissions='r')
        assert safe_check_path(path, 'f', 'r')
        assert not safe_check_path(path, 'd', 'r')
        assert not safe_check_path(path, 'f', 'w')
