def test_mona_files(self):
        self.maxDiff = None

        dirpath = tempfile.mkdtemp()
        #dirpath = os.path.join(os.path.dirname(__file__), 'original_results')
        db_pth = os.path.join(dirpath, 'test_msp_mona.db')

        create_db(file_pth=db_pth)

        libdata = LibraryData(msp_pth=os.path.join(os.path.dirname(__file__), 'msp_files', 'mona', 'MoNA-export-Pathogen_Box-small.msp'),
                              db_pth=db_pth,
                              db_type='sqlite',

                              schema='mona',
                              source='pathogen',
                              mslevel=None,
                              chunk=2)

        libdata = LibraryData(msp_pth=os.path.join(os.path.dirname(__file__), 'msp_files', 'mona', 'MoNA-export-MetaboBASE-small.msp'),

                              db_pth=db_pth,
                              db_type='sqlite',

                              schema='mona',
                              source='massbank',
                              mslevel=None,
                              chunk=2)

        libdata = LibraryData(msp_pth=os.path.join(os.path.dirname(__file__), 'msp_files', 'mona', 'MoNA-export-MassBank-small.msp'),
                              db_pth=db_pth,
                              db_type='sqlite',

                              schema='mona',
                              source='metabobase',
                              mslevel=None,
                              chunk=2)

        db_new = libdata.get_db_dict()

        # get original database info
        conn = sqlite3.connect(os.path.join(os.path.dirname(__file__), 'original_results', 'test_msp_mona.db'))
        cursor = conn.cursor()

        db_original = db_dict(cursor)

        self.compare_db_d(db_new, db_original)
