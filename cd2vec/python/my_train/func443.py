@pytest.mark.parametrize('limit', [2, 20])
def test_get_proxy_with_limit(get_proxy, regex_proxy, limit):
    prox_list = get_proxy(limit=limit)
    assert len(prox_list) == limit

    for prox_dict in prox_list:
        assert 'http', 'https' in prox_dict.keys()

        assert regex_proxy(prox_dict)

