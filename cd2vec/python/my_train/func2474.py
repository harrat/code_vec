@pytest.mark.perf
def test_fastq():
    def generate_fastq(seqlen=100):
        num_records = randint(100000, 500000)
        qualspace = list(chr(i + 33) for i in range(60))

        def rand_seq():
            return "".join(choices(['A', 'C', 'G', 'T'], k=seqlen))

        def rand_qual():
            return "".join(choices(qualspace, k=seqlen))

        return "\n".join(
            "\n".join((
                "read{}".format(i),
                rand_seq(),
                '+',
                rand_qual()))
            for i in range(num_records))
    return perftest('fastq', generate_fastq)

