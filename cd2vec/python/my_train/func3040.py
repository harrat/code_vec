@unittest.skipIf(platform.system() == "Windows" or platform.system() == "windows", "test not supported on windows")
    def test_ignore_unknown_config_in_properties_file(self):
        try:
            os.remove(self.config_file_path)
            del os.environ["SECURENATIVE_API_KEY"]
            del os.environ["SECURENATIVE_API_URL"]
            del os.environ["SECURENATIVE_INTERVAL"]
            del os.environ["SECURENATIVE_MAX_EVENTS"]
            del os.environ["SECURENATIVE_TIMEOUT"]
            del os.environ["SECURENATIVE_AUTO_SEND"]
            del os.environ["SECURENATIVE_DISABLE"]
            del os.environ["SECURENATIVE_LOG_LEVEL"]
            del os.environ["SECURENATIVE_FAILOVER_STRATEGY"]
        except FileNotFoundError:
            pass
        except KeyError:
            pass

        config = {
            "SECURENATIVE_TIMEOUT": "1500",
            "SECURENATIVE_UNKNOWN_KEY": "SOME_UNKNOWN_KEY"
        }

        self.create_ini_file(config)
        options = ConfigurationManager.load_config(None)

        self.assertIsNotNone(options)
        self.assertEqual(options.timeout, "1500")
