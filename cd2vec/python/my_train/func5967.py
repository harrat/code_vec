def test_flex_alloc_nodes_positive(make_flexible_job):
    job = make_flexible_job(12, sched_access=['--constraint=f1'])
    prepare_job(job)
    assert job.num_tasks == 48

