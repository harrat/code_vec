@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_roster_class_pulls_all_player_stats(self, *args, **kwargs):
        flexmock(utils) \
            .should_receive('_find_year_for_season') \
            .and_return('2018')
        roster = Roster('DET')

        assert len(roster.players) == 2

        for player in roster.players:
            assert player.name in ['Jimmy Howard', 'Henrik Zetterberg']
