def test_redis_delay_task_decorator_no_port_gotten(event_loop, redis_instance):

    _redis_instance, _ = redis_instance.rsplit(":", maxsplit=1)

    manager = build_manager(dsn=_redis_instance, loop=event_loop)

    globals()["test_redis_delay_task_decorator_no_port_gotten_finished"] = False

    @manager.task()
    async def task_test_redis_delay_task_decorator_oks_no_port_gotten(num):
        globals()["test_redis_delay_task_decorator_no_port_gotten_finished"] = True

    async def run():
        manager.run()

        await task_test_redis_delay_task_decorator_oks_no_port_gotten.delay(1)

        await manager.wait(timeout=0.2, exit_on_finish=True, wait_timeout=0.1)

    event_loop.run_until_complete(run())
    manager.stop()

    assert globals()["test_redis_delay_task_decorator_no_port_gotten_finished"] is True

    del globals()["test_redis_delay_task_decorator_no_port_gotten_finished"]

