def test_get_snil(self):
        raw = get_snli()
        self.assertIn('train', raw)
        self.assertEqual(len(raw['train']), 550_152)
        self.assertIn('dev', raw)
        self.assertEqual(len(raw['dev']), 10_000)
        self.assertIn('test', raw)
        self.assertEqual(len(raw['test']), 10_000)
