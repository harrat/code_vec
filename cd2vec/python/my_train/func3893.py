def test_public_access(self):
        random_data = generate_nonce(50).encode()
        attachment = self.member.create_blob(self.member.member_id, self.file_type, self.file_name, random_data, Blob.PUBLIC)
        out_blob = self.other_member.get_blob(attachment.blob_id)
        assert out_blob.payload.owner_id == self.member.member_id
