def test_partial_autodetect():
    text = 'amazing world.:lt:smart world.:ja:??????'
    expect = collections.OrderedDict(
        [('en', 'amazing world'),
         ('lt', 'smart world'),
         ('ja', '??????')]
    )

    result = split(text)

    assert(
        result == expect
    )
