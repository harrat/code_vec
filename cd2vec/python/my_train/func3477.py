def test_get_penn_treebank_twice(self):
        get_penn_treebank()
        with mock.patch('lineflow.datasets.penn_treebank.pickle', autospect=True) as mock_pickle:
            get_penn_treebank()
        mock_pickle.dump.assert_not_called()
        mock_pickle.load.assert_called_once()
