@pytest.mark.vcr()
def test_access_groups_create_success(api, agroup):
    assert isinstance(agroup, dict)
    check(agroup, 'created_at', 'datetime')
    check(agroup, 'updated_at', 'datetime')
    check(agroup, 'id', 'uuid')
    check(agroup, 'name', str)
    check(agroup, 'all_assets', bool)
    check(agroup, 'all_users', bool)
    check(agroup, 'rules', list)
    for rule in agroup['rules']:
        check(rule, 'type', str)
        check(rule, 'operator', str)
        check(rule, 'terms', list)
    check(agroup, 'principals', list)
    for principal in agroup['principals']:
        check(principal, 'type', str)
        check(principal, 'principal_id', 'uuid')
        check(principal, 'principal_name', str)
    check(agroup, 'created_by_uuid', 'uuid')
    check(agroup, 'updated_by_uuid', 'uuid')
    check(agroup, 'created_by_name', str)
    check(agroup, 'updated_by_name', str)
    check(agroup, 'processing_percent_complete', int)

@pytest.mark.vcr()
def test_access_groups_delete_success(api, agroup):
    api.access_groups.delete(agroup['id'])
