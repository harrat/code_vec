def test_settings_restored(self):
        """Ensure that settings are restored after test_settings_before."""
        import django
        from django.conf import settings

        assert TestLiveServer._test_settings_before_run is True
        assert (
            "%s.%s" % (settings.__class__.__module__, settings.__class__.__name__)
            == "django.conf.Settings"
        )
        if django.VERSION >= (1, 11):
            assert settings.ALLOWED_HOSTS == ["testserver"]
        else:
            assert settings.ALLOWED_HOSTS == ["*"]
