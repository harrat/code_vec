@staticmethod
    def test_help_option_003(capsys, caplog):
        """Test printing help from console.

        Generate help text by giving only the tool name.
        """

        snippy = Snippy(['snippy'])
        snippy.run()
        snippy.release()
        out, err = capsys.readouterr()
        assert out == Const.NEWLINE.join(TestCliOptions.HELP)
        assert not err
        assert not caplog.records[:]
        Content.delete()
