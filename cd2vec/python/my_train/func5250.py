def test_ecg_process(self):
        ecg_processed = nk.ecg_process(self.ecg, rsp=None, sampling_rate=1000, quality_model=None)
        self.assertAlmostEqual(ecg_processed["df"]["Heart_Rate"].mean(), 60.0, places=1)
