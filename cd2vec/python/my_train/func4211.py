def test_gpi_solver_known_solution_problem_2(self):
        problem = skp.ProcrustesProblem((4, 4, 2, 2), problemnumber=2)
        mysolver = skp.GPISolver(verbose=0)
        result = mysolver.solve(problem)
        assert_allclose(result.solution, problem.Xsol, atol=1e-2)
        #assert_allclose(result.fun, 1e-6, atol=1e-2)
