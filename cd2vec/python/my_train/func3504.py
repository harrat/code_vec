def test_pheno_assocs():
    payload = search_associations(subject=TWIST_ZFIN,
                                  object_category='phenotype'
    )
    assocs = payload['associations']
    assert len(assocs) > 0
    for a in assocs:
        print(str(a))
    assocs = [a for a in assocs if a['subject']['id'] == TWIST_ZFIN]
    assert len(assocs) > 0

def test_pheno_assocs_compact():
    assocs = search_associations_compact(subject=TWIST_ZFIN,
                                         rows=1000,
                                         object_category='phenotype'
    )
    assert len(assocs) == 1
    a = assocs[0]
    assert a['subject'] == TWIST_ZFIN
    assert 'ZP:0007631' in a['objects']
