def test_mutil_sent_trans():
    en = 'hello. china.'
    translator = Translator.translator(src='en', dest='fr')
    rep = translator.translate('hello. china.', multi=True)
    assert rep[0][0][0].lower() == 'bonjour. '
    assert rep[0][1][0].lower() == 'chine.'

