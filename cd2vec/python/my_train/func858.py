def test_contains(self):
        root = self.manifold.select('')
        self.assertIn(root, self.manifold.graphs[0])
        self.assertNotIn(root, self.manifold.graphs[1])

        clusters = self.manifold.find_clusters(root.medoid, root.radius, depth=5)
        for c in clusters:
            self.assertIn(c, self.manifold.graphs[5])
        return
