def test_read_verbose(fname=fname):
    """test son.load"""
    metadata, data = son.load(fname, verbose=True)

    assert metadata == m
    assert data == [d1, d2]

