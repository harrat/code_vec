def test_env_type_option(self):
        self.assertIs(
            self.get_opts('test').env_type,
            None
        )
        self.assertEqual(
            self.get_opts(
                'test', '--env-type', 'ABC'
            ).env_type,
            'abc'
        )
