@staticmethod
    @pytest.mark.usefixtures('import-forced', 'caller')
    def test_api_create_snippet_015(server):
        """Update snippet with POST that maps to PATCH.

        Send POST /snippets with the ``X-HTTP-Method-Override`` header to
        update a resource. All resource attributes are tried to be updated.
        This must generate HTTP error because it is not possible to update
        for example the ``uuid`` attribute by client.
        """

        storage = {
            'data': [
                Storage.forced
            ]
        }
        request_body = {
            'data': {
                'type': 'snippet',
                'attributes': {
                    'categeory': 'solution',
                    'data': 'data row1\ndata row2',
                    'brief': 'brief description',
                    'description': 'long description',
                    'name': 'runme',
                    'groups': 'solution',
                    'tags': 'tag1,tag2',
                    'links': 'link1\nlink2',
                    'source': 'http://testing/snippets.html',
                    'versions': 'version==1.1',
                    'languages': 'python',
                    'filename': 'filename.txt',
                    'created': 'invalid time',
                    'updated': 'invalid time',
                    'uuid': Snippet.EXITED_UUID,
                    'digest': 'invalid digest'
                }
            }
        }
        expect_headers_p3 = {'content-type': 'application/vnd.api+json; charset=UTF-8', 'content-length': '1830'}
        expect_headers_p2 = {'content-type': 'application/vnd.api+json; charset=UTF-8', 'content-length': '1925'}
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '400',
                'statusString': '400 Bad Request',
                'module': 'snippy.testing.testing:123',
                'title': 'json media validation failed'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_post(
            path='/api/snippy/rest/snippets/53908d68425c61dc',
            headers={'accept': 'application/vnd.api+json', 'X-HTTP-Method-Override': 'PATCH'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_400
        assert result.headers in (expect_headers_p2, expect_headers_p3)
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
