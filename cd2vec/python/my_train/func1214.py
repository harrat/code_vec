def test_verify_config():
    """Verify written config"""
    # Verify config written earlier
    assert AUTH.verify_config("test.json", KEY, VALUE) is True

    # Verify config mismatch
    assert AUTH.verify_config("test.json", KEY, "Not value") is False

    # Verify non json data
    assert AUTH.verify_config("bad_file.json", KEY, VALUE) is False

