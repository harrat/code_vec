def test_current_status(capsys):
    current_status("test")
    captured = capsys.readouterr()
    assert captured.out == "[+] Status report for 'test':  You have not started work on this project yet.\n"

