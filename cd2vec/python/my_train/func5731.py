def test_prompt_symbol_style_is_primary_color_when_last_command_succeed():
    assert prompt_symbol.style(last_command_status=constants.SUCCESS) == colors.style('primary')

