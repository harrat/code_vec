def test_for_overwrite(self):
        """
        deploy_by_date

        Note: Donot forget to crontab -r
        """
        cron_job = (
            "* * * * * ls",
            "30 * * * * ls",
            "30 7 * * * ls",
        )
        cronpi.run_custom(cron_job[0], isOverwrite=True)
        cronpi.run_custom("30 * * * * ls")
        cronpi.run_every_day("ls").on("7:30am")

        cron_result = get_job_list()
        for itm in cron_result:
            self.assertTrue(itm in cron_job)

        for itm in cron_job:
            self.assertTrue(itm in cron_result)
