def test_register_multiple():
    container.register_many(Example, [Example1, Example2])
    instance = container.get(List[Example])

    assert type(instance) is list
    assert len(instance) == 2
    assert type(instance[0]) is Example1
    assert type(instance[1]) is Example2

