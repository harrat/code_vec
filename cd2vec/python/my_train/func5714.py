@mock.patch('google.cloud.pubsub_v1.PublisherClient')
    def test_publish(self, mock_client):
        pub = Publisher()
        Publisher._topic_path = mock.MagicMock(return_value=TOPIC_NAME)
        pub.publish(TOPIC_NAME, MOCK_DATA)

        expected = [mock.call.publish(TOPIC_NAME, data=MOCK_DATA)]

        pub._topic_path.assert_called_once()
        self.assertEquals(pub._Publisher__publisher.mock_calls, expected)
