@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_search_reference_006(snippy, capsys):
        """Search references with ``uuid`` option.

        Try to search reference by explicitly defining content uuid that
        cannot be found.
        """

        output = 'NOK: cannot find content with given search criteria\n'
        cause = snippy.run(['snippy', 'search', '--scat', 'reference', '--uuid', '1234567', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == 'NOK: cannot find content with given search criteria'
        assert out == output
        assert not err
