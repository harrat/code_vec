def test_nba_player_returns_requested_player_season_stats(self):
        # Request the 2017-18 stats
        player = self.player('2017-18')

        for attribute, value in self.results_2018.items():
            assert getattr(player, attribute) == value
