def test_tsv_dict(self):
        path = self.root.make_file()
        with open(path, 'wt') as o:
            o.write('id\ta\tb\tc\n')
            o.write('row1\t1\t2\t3\n')
            o.write('row2\t4\t5\t6\n')

        with self.assertRaises(ValueError):
            read_delimited_as_dict(path, key='id', header=False)
        with self.assertRaises(ValueError):
            read_delimited_as_dict(path, key=None, header=False)

        assert dict(
                row1=['row1', 1, 2, 3],
                row2=['row2', 4, 5, 6]
        ) == read_delimited_as_dict(
            path, key=0, header=True, converters=(str, int, int, int))
        assert dict(
                row1=['row1', 1, 2, 3],
                row2=['row2', 4, 5, 6]
        ) == read_delimited_as_dict(
            path, key='id', header=True, converters=(str, int, int, int))

        with open(path, 'wt') as o:
            o.write('a\tb\tc\n')
            o.write('1\t2\t3\n')
            o.write('4\t5\t6\n')

        assert dict(
            row1=[1, 2, 3],
            row4=[4, 5, 6]
        ) == read_delimited_as_dict(
            path, key=lambda row: 'row{}'.format(row[0]),
            header=True, converters=int)
