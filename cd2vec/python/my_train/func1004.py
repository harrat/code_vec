def test_embeddings_with_spacy(self):
        with self.assertRaises(ValueError):
            load_wv_with_spacy("wiki.da.small.swv")

        embeddings = load_wv_with_spacy("wiki.da.wv")

        sentence = embeddings('jeg gik ned af en gade')
        for token in sentence:
            self.assertTrue(token.has_vector)
