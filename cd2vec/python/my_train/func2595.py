def test_no_empty_lines_in_preamble(minimal_job):
    for line in minimal_job.scheduler.emit_preamble(minimal_job):
        assert line != ''

