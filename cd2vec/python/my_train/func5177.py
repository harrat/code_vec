def test_unique_and_count(self, bids_layout):
        result = bids_layout.unique('subject')
        assert len(result) == 10
        assert '03' in result
        assert bids_layout.count('run') == 2
        assert bids_layout.count('run', files=True) > 2
