def test_kms():
    try:
        kms = boto3.client('kms', region_name='us-west-2')
        kms.update_key_description(
            KeyId='1234abcd-12ab-34cd-56ef-1234567890ab',
            Description='foo'
        )
        raise Exception("Shouldn't reach here")
    except botocore_errors:
        pass
