def test_should_use_real_time():
    frozen = datetime.datetime(2015, 3, 5)
    expected_frozen = 1425513600.0
    # TODO: local time seems to leak the local timezone, so this test fails in CI
    # expected_frozen_local = (2015, 3, 5, 1, 0, 0, 3, 64, -1)
    expected_frozen_gmt = (2015, 3, 5, 0, 0, 0, 3, 64, -1)
    expected_clock = 0

    from freezegun import api
    api.call_stack_inspection_limit = 100  # just to increase coverage

    timestamp_to_convert = 1579602312
    time_tuple = time.gmtime(timestamp_to_convert)

    with freeze_time(frozen):
        assert time.time() == expected_frozen
        # assert time.localtime() == expected_frozen_local
        assert time.gmtime() == expected_frozen_gmt
        if HAS_CLOCK:
            assert time.clock() == expected_clock
        if HAS_TIME_NS:
            assert time.time_ns() == expected_frozen * 1e9

        assert calendar.timegm(time.gmtime()) == expected_frozen
        assert calendar.timegm(time_tuple) == timestamp_to_convert

    with freeze_time(frozen, ignore=['_pytest', 'nose']):
        assert time.time() != expected_frozen
        # assert time.localtime() != expected_frozen_local
        assert time.gmtime() != expected_frozen_gmt
        if HAS_CLOCK:
            assert time.clock() != expected_clock
        if HAS_TIME_NS:
            assert time.time_ns() != expected_frozen * 1e9
