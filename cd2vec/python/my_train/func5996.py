def test_subscribe_instrument_on_message(self, mocker):
        socket = BitMEXWebsocket()
        message = {
            "success": "true",
            "subscribe": "instrument:XBTH17",
            "request": {
                "op": "subscribe",
                "args": ["instrument:XBTH17"]
            }
        }
        subscribe_handler = mocker.stub()

        @socket.on('subscribe')
        def handler(message):
            subscribe_handler(message)

        socket.on_message(json.dumps(message))

        subscribe_handler.assert_called_once_with(message)
