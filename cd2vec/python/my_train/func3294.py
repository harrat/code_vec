def test_posted_location(self):
        """Make sure 'Location' header returned in response actually points to
        new resource created during POST."""
        post_response = self.post_response()
        location = post_response.headers['Location']
        self.get_response(location, 200)
