def test_length_exact(self):
        audiofile = self.load(self.AUDIO_FILE_EXACT, rs=True)
        audiofile.clear_data()
        self.assertAlmostEqual(audiofile.audio_length, TimeValue("5.600"), places=3)     # 5.600
