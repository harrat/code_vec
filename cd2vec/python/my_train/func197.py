def test_invalid_ipv4(self):
        values = [
            "foo",
            "999.999.999.999",
            "192.168.0.1/",
            "192.168.0.1/33",
            "192.168.0.1/a",
            "192.168.0.1/01",
        ]
        ipv4 = copy.deepcopy(self.valid_ipv4)
        for value in values:
            ipv4['value'] = value
            self.assertFalseWithOptions(ipv4)
