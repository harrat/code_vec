def test_observable_str(all_params):
    """This test checks codestring obtained using __str__

    Randomly instantiate a given observable <obs>
    Check that second instance made from str(obs) is identical.
    """
    for kwargs in all_params:
        obs = Observable(**kwargs)
        nobs = Observable(from_string=str(obs))
        for attr in obs._attr_names:
            if attr == 'time_window' and not obs.local_fit:
                # check only if local_fit is True,
                # since when it's false, time_window is not printed in str
                continue
            assert hasattr(nobs, attr)
            assert getattr(nobs, attr) == getattr(obs, attr)

