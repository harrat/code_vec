@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_api_search_reference_011(server):
        """Search reference without search parameters.

        Send GET /references without defining search parameters.
        In this case only one reference must be returned because the
        limit is set to one. Also the sorting based on brief field
        causes the last reference to be returned.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '724'
        }
        expect_body = {
            'meta': {
                'count': 1,
                'limit': 1,
                'offset': 0,
                'total': 2
            },
            'data': [{
                'type': 'reference',
                'id': Reference.REGEXP_UUID,
                'attributes': Reference.REGEXP
            }]
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/references',
            headers={'accept': 'application/json'},
            query_string='limit=1&sort=-brief')
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
