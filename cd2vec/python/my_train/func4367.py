def test_invalidate_on(region):
    import blinker
    s = blinker.signal('named_signal')
    t = blinker.signal('other_signal')

    region['key'] = 'value'
    region.invalidate_on(s, t)

    s.send('nothing',in_='particular')
    assert 'key' not in region
    assert region._region_cache.conn.hget(region.name, 'key') is None

    region['key'] = 'value'

    t.send('nothing', in_='particular')
    assert 'key' not in region
    assert region._region_cache.conn.hget(region.name, 'key') is None

