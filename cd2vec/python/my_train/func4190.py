def test_get_link(self):
        my_image_loader = ImgLoader()

        dl_link = my_image_loader.get_image_direct_link('https://imgur.com/a/jpwwwq6')
        self.assertEqual('https://i.imgur.com/W0Cu3zE.png', dl_link)
