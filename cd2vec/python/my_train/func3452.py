def test_getShocks(self):
        self.agent.initializeSim()
        self.agent.simBirth(np.array([True,False]))
        self.agent.simOnePeriod()
        self.agent.simBirth(np.array([False,True]))

        self.agent.getShocks()

        self.assertEqual(self.agent.PermShkNow[0],
                         1.0427376294215103)
        self.assertEqual(self.agent.PermShkNow[1],
                         0.9278094171517413)
        self.assertEqual(self.agent.TranShkNow[0],
                         0.881761797501595)
