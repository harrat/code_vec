def test_09_get_migration_table_schema():
    """Verify that we get a non-Falsey return from get_migration_table_schema."""
    schema = pydbvolve.get_migration_table_schema()
    assert(schema is not None)
    assert(isinstance(schema, str))
    assert(len(schema) > 0)
# End test_09_get_migration_table_name
