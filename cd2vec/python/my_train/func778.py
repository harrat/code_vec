def test_download_file():
    url_zip = "https://raw.githubusercontent.com/nltk/nltk_data/gh-pages/packages/corpora/stopwords.zip"
    url_tar = "https://bit.ly/30JYkQF"
    filename = "razdel.tar.gz"
    dirpath = "/tmp/ruts_download"
    assert download_file(url_zip, dirpath=dirpath) == "/tmp/ruts_download/stopwords.zip"
    assert download_file(url_tar, filename=filename, dirpath=dirpath) == "/tmp/ruts_download/razdel.tar.gz"
    assert download_file(url_zip, dirpath=dirpath) == None

def test_download_file_runtime_error():
    url = "https://raw.githubusercontent.com/nltk/nltk_data/gh-pages/packages/corpora/stopword.zip"
    dirpath = "/tmp/ruts_download"
    with pytest.raises(RuntimeError):
        download_file(url, dirpath=dirpath, force=True)
