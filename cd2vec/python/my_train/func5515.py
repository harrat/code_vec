def test_get_top_rated():
    shows = isle.show.get_top_rated()
    assert inspect.isgenerator(shows)
    show = next(shows)
    assert isinstance(show, Show)

