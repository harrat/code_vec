def test_clean(self):
        self.base_config["report_to"] = "foo@example.com"
        with test_utils.lauch(self.base_config, options=["--clean"], get_process=True) as p:
            self.assertEqual(p.wait(), 0)
        self.base_config["report_only_if_needed"] = False
        self.base_config["smtp_server"] = "localhost"
        with test_utils.lauch(self.base_config, options=["--clean"], get_process=True) as p:
            self.assertEqual(p.wait(), 8)
