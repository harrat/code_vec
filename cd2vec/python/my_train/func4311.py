def test_deploy_like_crontab(self):
        """
        deploy_by_date
        """
        cronpi.run_custom("* * * * * ls", isOverwrite=True)
        self.assertEqual(get_job_list()[0], "* * * * * ls")
