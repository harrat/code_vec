def test_dynamodb_put_item():
    ConfigProvider.set(config_names.THUNDRA_TRACE_INTEGRATIONS_AWS_DYNAMODB_TRACEINJECTION_ENABLE, 'true')

    try:
        item = {
            'id': {'S': "3"},
            'text': {'S': "test2"}
        }
