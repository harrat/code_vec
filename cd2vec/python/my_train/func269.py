def test_get_name_actor_deceased(self):
        name_id = 'nm0000122'
        scraper = PyMDbScraper()
        name = scraper.get_name(name_id, include_known_for_titles=True)

        # Correct values
        display_name = 'Charles Chaplin'
        known_for_titles = ['tt0032553', 'tt0044837', 'tt0027977', 'tt0039631']
        birth_date = datetime(1889, 4, 16)
        birth_city = 'Walworth, London, England, UK'
        death_date = datetime(1977, 12, 25)
        death_city = 'Vevey, Vaud, Switzerland'
        death_cause = 'stroke'
        birth_name = 'Charles Spencer Chaplin'
        nicknames = ['Charlie', 'Charlot', 'The Little Tramp']
        height = 1.63

        self.assertEqual(name.name_id, name_id)
        self.assertEqual(name.display_name, display_name)
        self.assertEqual(sorted(name.known_for_titles), sorted(known_for_titles))
        self.assertEqual(name.birth_date, birth_date)
        self.assertEqual(name.birth_city, birth_city)
        self.assertEqual(name.death_date, death_date)
        self.assertEqual(name.death_city, death_city)
        self.assertEqual(name.death_cause, death_cause)
        self.assertEqual(name.birth_name, birth_name)
        self.assertEqual(sorted(name.nicknames), sorted(nicknames))
        self.assertEqual(name.height, height)
