@given(deviation_survey())
def test_resample_onto_unchanged_md(survey):
    md, inc, azi = survey

    dev = deviation(md, inc, azi)
    pos = dev.minimum_curvature()
    resampled = pos.resample(depths = md)

    np.testing.assert_allclose(pos.depth, resampled.depth)
    np.testing.assert_allclose(pos.northing, resampled.northing)
    np.testing.assert_allclose(pos.easting, resampled.easting)

def test_copy():
    original = position_log(np.array([]), np.array([1,2,3,4]), [4,3,2,1], [1,1,1,1])
    copy = original.copy()
    original.depth += 10
    np.testing.assert_equal([1,2,3,4], copy.depth)
