@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_delete_snippet_006(snippy):
        """Delete snippet with dgiest.

        Try to delete snippet with empty message digest. Nothing should be
        deleted in this case because there is more than one content stored.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        cause = snippy.run(['snippy', 'delete', '-d', ''])
        assert cause == 'NOK: cannot use empty message digest for delete operation'
        Content.assert_storage(content)
