@pytest.mark.vcr()
def test_workbench_asset_vuln_info_plugin_id_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.asset_vuln_info(str(uuid.uuid4()), 'something here')
