def test_static_routes(self, test_client):
        """
        Tests if the static path is handled both by default and
        if the path is past to the static_folder kwarg
        """
        rv = test_client.get("/static/images/Group.jpg")
        assert "200" in str(rv.status)

        rv = test_client.get("/")
        assert "200" in str(rv.status)
