def test_is_adult_attr(person: Person):
    assert isinstance(person.is_adult, bool)

