def test_resolve_with_parent(self):
        self.root.make_directory(name='foo')
        path = self.root.make_file(parent=self.root[Path('foo')])
        name = path.name
        parent = path.parent
        assert path == resolve_path(Path(name), parent)
