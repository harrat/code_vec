def test_granular_marking_invalid_marking_ref_type(self):
        marking_definition = copy.deepcopy(self.valid_marking_definition)
        marking_definition['granular_markings'] = [{
            "marking_ref": "indicator--3478bf48-9af2-4afa-9fc5-7075f6af04af",
            "selectors": ["created"]
        }]
        self.assertFalseWithOptions(marking_definition)
