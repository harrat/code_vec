def test_similar_contents(self):
        ''' data structure should be empty at the beginning '''
        kvs_size = self.kvs_size()
        self.assertEqual(kvs_size, 0)
        kvs_count = self.kvs_count()
        self.assertEqual(kvs_count, 0)
        rc_count = self.rc_count()
        self.assertEqual(rc_count, 0)

        ''' verify for content lengths of different orders of magnitude '''
        for chunking_levels in range(1, 8):
            content_length = self.random.randint(
                int(self.S * (self.S / self.R) ** (chunking_levels - 1)), int(self.S * (self.S / self.R) ** (chunking_levels)))

            ''' choose random offset '''
            offset = self.random.randint(0, content_length)

            ''' choose random d '''
            d = self.random.randint(
                1, content_length // 10 + 1)  # should be at least an order of magnitude less than the length

            increase_size = 0
            expected_increase = 0

            ''' average over 10 runs '''
            for _ in range(10):
                content = bytes([self.random.randint(0, 255)
                                 for _ in range(content_length)])
                self.seccs.put_content(content)
                kvs_size = self.kvs_size()

                content = b''.join([content[:offset], bytes(
                    [self.random.randint(0, 255) for _ in range(d)]), content[offset + d:]])
                self.seccs.put_content(content)

                increase_size += self.kvs_size() - kvs_size

                ''' calculate expected increase '''
                expected_increase += 4 * \
                    ((math.ceil(math.log(content_length / self.S) / 
                                math.log(self.S / self.R)) + 1) * (self.S + self.digest_size) + d)

            self.assertLess(increase_size, expected_increase)
            kvs_size = self.kvs_size()
