def test_with_multiple_nets(self):
        nc = NetClass('default')
        nc.nets += ['GND', 'VO']
        nc_str = nc.to_string()
        assert NetClass.parse(nc.to_string()) == nc
