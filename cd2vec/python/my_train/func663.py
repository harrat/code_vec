def test_resolve_new_numpy(mocked_responses, tmpdir, read_contents, mocker):
    wheeldir = str(tmpdir)
    mocked_responses.add(
        responses.GET,
        INDEX_URL + "/numpy/",
        body=read_contents("numpy.html"),
        status=200,
    )

    repo = PyPIRepository(INDEX_URL, wheeldir)
    candidates = repo.get_candidates(pkg_resources.Requirement.parse("numpy"))
    for candidate in candidates:
        if "1.16.3" in candidate.link[1]:
            mocked_responses.add(
                responses.GET,
                candidate.link[1],
                body=read_contents("numpy.whl-contents"),
                status=200,
            )

    mock_extract = mocker.MagicMock()
    mock_extract.return_value.name = "numpy"

    mocker.patch("req_compile.repos.pypi.extract_metadata", mock_extract)
    candidate, cached = repo.get_candidate(pkg_resources.Requirement.parse("numpy"))
    assert candidate is not None
    assert not cached

    listing = tmpdir.listdir()
    assert len(listing) == 1
    assert "1.16.3" in str(listing[0])
    assert ".whl" in str(listing[0])

    # Query the index, and download
    assert len(mocked_responses.calls) == 2

