def test_get_object_attribute(self):
        """Test simple HTTP GET"""
        response = self.get_response('/tracks/347', 200)
        response = self.get_response('/tracks/347/Genre', 200)
        assert json.loads(response.get_data(as_text=True))[u'Name'] == 'Rock'
