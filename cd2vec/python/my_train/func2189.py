def test_run_without_git_no_config(self, cli_runner, project):
        project.remove(".therapist.yml")
        project.write("pass.py")

        with chdir(project.path):
            result = cli_runner.invoke(cli.run)
            assert "No Therapist configuration file was found." in result.output
            assert result.exception
            assert result.exit_code == 1
