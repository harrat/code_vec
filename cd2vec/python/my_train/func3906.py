def test_current_shows_current_entry(cli, config, data_dir):
    efg = EntriesFileGenerator(data_dir, '%m_%Y.txt')
    efg.expand(datetime.date(2014, 1, 21)).write(
        "20/01/2014\nalias_1 1000-? hello world"
    )
    efg.patch_config(config)

    with freeze_time('2014-01-20 10:45:00'):
        stdout = cli('current')

    assert stdout.strip() == "alias_1 00h45m"

