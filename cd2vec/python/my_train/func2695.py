def test_execute_illegal_sql():
    global cu
    with pytest.raises(py2jdbc.OperationalError):
        cu.execute("select asdf")
