def test_get_schedule():
    """Tests to see we get something when scraping. We want it to return a dictionary"""
    assert isinstance(json_schedule.get_schedule("2018-03-28", "2018-07-01"), dict)
    assert isinstance(json_schedule.get_schedule("2017-09-01", "2018-09-15"), dict)

