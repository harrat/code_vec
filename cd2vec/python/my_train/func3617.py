@pytest.mark.googlevision
@pytest.mark.parametrize(
    "image", [image_path, cv2.imread(image_path), Image.open(image_path)]
)
def test_google_vision_ocr(image, google_vision_ocr_mock):
    text, conf = gpyocr.google_vision_ocr(image, langs=["en"])
    assert text.count("\n") == 11  # text of 12 lines
    assert 1 <= conf <= 100

