def test_custom_property_name_short(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['mp'] = "abc123"
        results = validate_parsed_json(indicator, self.options)
        self.assertEqual(results.is_valid, False)
