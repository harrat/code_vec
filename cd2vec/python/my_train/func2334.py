@pytest.mark.vcr()
def test_boundary(shed):
    """check a few random properties of the returned geojson dict"""
    result = shed.boundary
    geojson_out = geojson.loads(json.dumps(result))
    assert geojson_out.is_valid

