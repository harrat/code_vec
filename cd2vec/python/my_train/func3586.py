def test_new_template(self):
        """
        Valid new task template operations
        """
        # The simplest case: only a name is required
        # Note: task name is not checked!
        name = 'dummy'
        task_tmpl = TaskTemplate(name)
        self.assertEqual(task_tmpl.name, name)
        self.assertEqual(task_tmpl.config, {})
        self.assertEqual(task_tmpl.topics, [])
        # Even if no ID is provided, it must be generated
        self.assertIsInstance(task_tmpl.uid, str)
        self.assertEqual(len(task_tmpl.uid), 36)
