def test_extract_to_txt(self):
        output_file = os.path.join(self.tmp_dir, "cre.txt")
        poiolib.wikipedia.extract_to_txt("cre", output_file)
        self.assertTrue(os.path.isfile(output_file))
        self.assertNotEqual(os.path.getsize(output_file), 0)
