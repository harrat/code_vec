def test_invalid():
    Breathe.add_commands(
        AppContext("code.exe"),
        {
            "test that <nonexistent_extra>": DoNothing(),
            1: DoNothing(),
        },
    )
    assert len(Breathe.contexts) == 1
    assert len(Breathe.context_commands) == 1

