def test_create_and_get_addresses(self):
        member = self.client.create_member(utils.generate_alias())
        addresses = {utils.generate_nonce(): utils.generate_address() for _ in range(3)}
        for name, address in addresses.items():
            member.add_address(name, address)
        actual = {address_record.name: address_record.address for address_record in member.get_addresses()}
        assert addresses == actual
