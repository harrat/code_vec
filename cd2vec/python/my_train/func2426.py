@pytest.mark.skipif(sys.version_info < (3, 6),
                    reason="The labeling of nodes will vary for Python 2")
@pytest.mark.mpl_image_compare(baseline_dir='baseline_images',
                               filename='16_markers.png',
                               savefig_kwargs={'dpi': 150})
def test_markers(example_district):
    """Tests marking a node and an edge
    """
    heating_network_1 = example_district.create_subgraphs(
        network_type='heating',
        all_buildings=False,
    )['default']

    edgelist = list(heating_network_1.edges())
    edgelist_tuples_sorted = []
    for edge in edgelist:
        edgelist_tuples_sorted.append(sorted(edge))

    edgelist_sorted = sorted(edgelist_tuples_sorted)

    for edge in edgelist_sorted:
        edge_to_mark = [edge[0], edge[1]]
        break

    nodelist = list(heating_network_1.nodes())
    nodelist_sorted = sorted(nodelist)

    node_to_mark = nodelist_sorted[2]

    scaling_factor = 10

    vis = ug.Visuals(heating_network_1)
    fig = vis.show_network(
        show_plot=False,
        scaling_factor=scaling_factor,
        node_markers=[node_to_mark],
        edge_markers=[edge_to_mark],
        labels='name',
        label_size=30
    )
    return fig

