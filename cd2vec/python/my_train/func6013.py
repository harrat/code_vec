def test_fetch_data_wdc_format_hour_data_from_wdc(tmpdir):  # pylint: disable=invalid-name
    """
    make sure that our `fetch_data` wrapper function does the same as
    instantiating the classes, making the request,
    and saving the unpacked zip files sequentually
    """
    cadence = 'hour'
    stations = ['NGK', 'LER']
    start_date = date(2015, 4, 1)
    end_date = date(2015, 4, 30)
    service = 'WDC'
    expected_filenames = ['ngk2015.wdc', 'ler2015.wdc']
    configpath = os.path.join(DATAPATH, 'wdc_minute_data_wdcoutput.ini')
    # TODO: cf fetch_data vs oracle files
    # oraclefile = os.path.join(ORACLEPATH, 'ngk2015.wdc')

    # ensure we have somewhere to put the data
    manualdir = os.path.join(str(tmpdir), 'manual')
    funcdir = os.path.join(os.path.dirname(manualdir), 'via__fetch_data')
    for dir_ in (manualdir, funcdir):
        os.mkdir(dir_)
    manualfile = []
    funcfile = []
    for filename_ in expected_filenames:
        manualfile.append(os.path.join(manualdir, filename_))
        funcfile.append(os.path.join(funcdir, filename_))

    # 'manual' way
    for station_ in stations:
        config = cws.ParsedConfigFile(configpath, service)
        form_data = cws.FormData(config)
        form_data.set_datasets(start_date, end_date, station_, cadence,
                               service)
        req_wdc = cws.DataRequest()
        req_wdc.read_attributes(config)
        req_wdc.set_form_data(form_data.as_dict())
        resp_wdc = rq.post(
            req_wdc.url, data=req_wdc.form_data, headers=req_wdc.headers
        )

        cws.check_response(resp_wdc.status_code, resp_wdc.content)

        with zipfile.ZipFile(BytesIO(resp_wdc.content)) as fzip:
            fzip.extractall(manualdir)

    for filepath_ in funcfile:
        assert not os.path.isfile(filepath_)
    # with wrapper function
    cws.fetch_data(
        start_date=start_date, end_date=end_date,
        station_list=stations, cadence=cadence,
        service=service, saveroot=funcdir, configpath=configpath
    )
    for filepath_ in funcfile:
        assert os.path.isfile(filepath_)
    [assert_all_lines_same(file1_, file2_) for file1_, file2_ in
        zip(funcfile, manualfile)]
