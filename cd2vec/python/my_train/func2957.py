def test_biography_attr(person: Person):
    biography = person.biography
    assert isinstance(biography, dict)
    assert "default" in biography
    assert all(len(key) == 2 for key in filter(str.isupper, biography))
    assert all(biography[key] for key in filter(str.isupper, biography))

