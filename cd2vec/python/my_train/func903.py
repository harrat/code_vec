def test_simulator_gc_processes_hanging(log_destroy):
    sim = set_up_simulator_with_destructor(log_destroy)
    sim.run(15.0)
    assert len(list(sim.events())) > 0
    assert log_destroy == ["A finish"]
    sim = None
    gc.collect(0)
    assert log_destroy == ["A finish", "B EXIT", "B finish", "D EXIT", "D finish", "C EXIT", "C finish", "sim"]

