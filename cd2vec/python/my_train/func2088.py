def test_baggage_item():
    tracer = ThundraTracer.get_instance()
    with tracer.start_active_span(operation_name='operation name', finish_on_close=True) as scope:
        span = scope.span
        assert bool(span.context.baggage) == False
