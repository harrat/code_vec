@staticmethod
    @pytest.mark.usefixtures('default-references', 'export-time')
    def test_cli_export_reference_007(snippy):
        """Export defined reference with digest.

        Export defined reference based on message digest. Filename and format
        are defined with command line option ``--file``.

        Because text template does not have UUID, the UUID mock allocates a new
        UUID for the exported comparison. Because of this the imported resource
        UUID cannot be compared to exported text.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Content.deepcopy(Reference.REGEXP)
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-d', 'cb9225a81eab8ced', '-f', 'defined-reference.txt'])
            assert cause == Cause.ALL_OK
            Content.assert_text(mock_file, 'defined-reference.txt', content)
