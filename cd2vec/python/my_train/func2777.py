def test_instruments_endpoints():
    ## Testing getting one instrument
    market = cryptowatch.instruments.get("btcusd")
    # Test instrument object structure
    assert hasattr(market, "instrument")
    assert hasattr(market.instrument, "id")
    assert hasattr(market.instrument, "base")
    assert hasattr(market.instrument, "quote")
    assert hasattr(market.instrument, "symbol")
    assert hasattr(market.instrument, "route")
    assert hasattr(market.instrument, "markets")
    assert type(market.instrument.base) == cryptowatch.resources.assets.AssetResource
    assert type(market.instrument.quote) == cryptowatch.resources.assets.AssetResource
    assert type(market.instrument.markets) == type(list())
    # test market instrument values
    assert market.instrument.id == 9
    assert market.instrument.symbol == "btcusd"
    assert market.instrument.base.symbol == "btc"
    assert market.instrument.quote.symbol == "usd"
    assert market.instrument.route.startswith("https")
    # Testing listing all instruments
    instruments = cryptowatch.instruments.list()
    assert hasattr(instruments, "instruments")
    assert type(instruments.instruments) == type(list())
    assert (
        type(instruments.instruments[0])
        == cryptowatch.resources.instruments.InstrumentResource
    )
    # This should raise an APIResourceNotFoundError Exception
    with pytest.raises(cryptowatch.errors.APIResourceNotFoundError):
        cryptowatch.instruments.get("instrumentthatdoesntexists")
