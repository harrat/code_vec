def test_calc_beta_z_ratio(class_objects):
    d_source, d_class, s_width = class_objects
    obj = DrizzleSolving(d_source, d_class, s_width)
    obj.data.beta = np.array([[1, 1, 2], [1, 1, 3]])
    obj.data.z = np.array([[2, 2, 1], [1, 1, 1]])
    compare = 2 / np.pi * obj.data.beta / obj.data.z
    testing.assert_array_almost_equal(obj._calc_beta_z_ratio(), compare)

