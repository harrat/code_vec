def test_poll(make_job, exec_ctx):
    minimal_job = make_job(sched_access=exec_ctx.access)
    prepare_job(minimal_job, 'sleep 2')
    minimal_job.submit()
    assert not minimal_job.finished()
    minimal_job.wait()

