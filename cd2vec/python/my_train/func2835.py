def test_sys_modules(self, caplog, monkeypatch):
        """Test sys.modules interactions."""
        caplog.set_level(logging.DEBUG, 'runway.SafeHaven')
        monkeypatch.setattr(SafeHaven, 'reset_all', MagicMock())

        # pylint: disable=unnecessary-comprehension
        orig_val = {k: v for k, v in sys.modules.items()}
        expected_logs = ['entering a safe haven...',
                         'resetting sys.modules...']

        with SafeHaven() as obj:
            from .fixtures import mock_hooks  # noqa pylint: disable=E,W,C
            assert sys.modules != orig_val
            obj.reset_sys_modules()
        assert sys.modules == orig_val
        assert caplog.messages[:2] == expected_logs
        assert caplog.messages[-1] == 'leaving the safe haven...'
