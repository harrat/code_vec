def test_arg_params(self):
        tech = TechMS()

        uri_params = {'_0': 1, '_1': 2}
        data = {'get': '/params/{_0}/{_1}', 'uri_params': uri_params}
        call = TechState.get_call_data(None, data)
        res = tech(call, {})
        assert res['statusCode'] == 200
        assert res['body'] == "get 1 and 2"
