def test_list_search(self):
        search = sfpl.Search('red', _type='list')

        lists = search.getResults()

        for list_ in next(lists):
            self.assertTrue('red' in list_.title.lower())
