@pytest.mark.unit
    def test_split_data_throws_error_for_existing_folder(self):
        url_example_data = "https://osf.io/vrdpe/?action=download"  # URL of example data hosted on OSF
        file_data = "SEM_dataset.zip"

        if not download_data(url_example_data)==0:
            print('ERROR: Data was not succesfully downloaded and unzipped - please check your link and filename and try again.')
        else:
            print('Data downloaded and unzipped succesfully.')
        
        assert self.data_split_path.is_dir()
        with pytest.raises(IOError):
            split_data(self.downloaded_data, self.data_split_path, seed=2019, split = [0.8, 0.2])
