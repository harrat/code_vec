def test_mlb_second_game_double_header_info(self):
        fields = {
            'attendance': 26340,
            'date': 'Monday, July 9, 2018',
            'attendance': 26340,
            'venue': 'Oriole Park at Camden Yards',
            'duration': '3:13',
            'time_of_day': 'Night'
        }

        mock_field = """Monday, July 9, 2018
Attendance: 26,340
Venue: Oriole Park at Camden Yards
Game Duration: 3:13
Night Game, on grass
Second game of doubleheader
"""

        m = MockBoxscoreData(MockField(mock_field))

        self.boxscore._parse_game_date_and_location(m)
        for field, value in fields.items():
            assert getattr(self.boxscore, field) == value
