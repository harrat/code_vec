@staticmethod
    @pytest.mark.usefixtures('devel_no_tests')
    def test_help_option_010(capsys, caplog):
        """Print test documentation when testing package does not exist.

        Try to print tool test case reference documentation when tests are
        not packaged with the release.
        """

        snippy = Snippy(['snippy', '--help', 'tests'])
        cause = snippy.run()
        snippy.release()
        out, err = capsys.readouterr()
        assert cause == 'NOK: test cases are not packaged with release No module named \'tests\''
        assert out == 'NOK: test cases are not packaged with release No module named \'tests\'' + Const.NEWLINE
        assert not err
        assert not caplog.records[:]
        snippy.release()
        Content.delete()
