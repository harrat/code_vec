@mock.patch("IPython.display.display")
def test_scraps_report_with_data_no_headers(mock_display, notebook_collection):
    notebook_collection.scraps_report(headers=None, include_data=True)
    mock_display.assert_has_calls(
        [
            mock.call({"text/plain": "'Hello World!'"}, metadata={}, raw=True),
            mock.call({"text/plain": "'Just here!'"}, metadata={}, raw=True),
            mock.call("one: 1"),
            mock.call("number: 1"),
            mock.call("list: [1, 2, 3]"),
            mock.call("dict: {'a': 1, 'b': 2}" if six.PY3 else "dict: {u'a': 1, u'b': 2}"),
            mock.call({"text/plain": "'Hello World 2!'"}, metadata={}, raw=True),
            mock.call({"text/plain": "'Just here!'"}, metadata={}, raw=True),
            mock.call("two: 2"),
            mock.call("number: 2"),
            mock.call("list: [4, 5, 6]"),
            mock.call("dict: {'a': 3, 'b': 4}" if six.PY3 else "dict: {u'a': 3, u'b': 4}"),
        ]
    )
