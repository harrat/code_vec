def test_pass_in_dataframe3(self):
        data = np.random.uniform(-4, 6, size=(1000000, 1))
        weight = norm.pdf(data)
        df = pd.DataFrame(data, columns=["a"])
        df["weight"] = weight
        c = ChainConsumer()
        c.add_chain(df)
        summary1 = c.analysis.get_summary()
        assert np.isclose(summary1["a"][0], -1, atol=0.03)
        assert np.isclose(summary1["a"][1], 0, atol=0.05)
        assert np.isclose(summary1["a"][2], 1, atol=0.03)
