def test_train_missingArgs(self):
        agent = BackendAgentTest.DebugAgent()
        with self.assertRaises(AssertionError):
            agent.train(train_context=None, callbacks=[])
        with self.assertRaises(AssertionError):
            agent.train(train_context=self.tc, callbacks=None)
