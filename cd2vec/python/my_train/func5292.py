@pytest.mark.parametrize('opts', [
    ({'atlas_info': True}),
    ({'exact': False}),
    ({'reannotated': False}),
    ({'atlas_info': True, 'exact': False}),
])
def test_extra_get_expression_data(testfiles, atlas, opts):
    if opts.get('atlas_info'):
        opts['atlas_info'] = atlas['info']

    out = allen.get_expression_data(atlas['image'], donors=['12876', '15496'],
                                    **opts)
    assert out.index.name == 'label'
    assert out.columns.name == 'gene_symbol'

