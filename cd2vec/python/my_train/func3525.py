def test_processing(selft, rnp):
        prev_time = time.time()
        rnp.new_task(simple_task).run(every=0.1, times=10).result()
        time_elapsed = time.time() - prev_time
        is_ok = (time_elapsed < 1) and (time_elapsed > 0.88)
        assert is_ok is True
