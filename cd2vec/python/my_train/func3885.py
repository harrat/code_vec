def test_compress_path(self):
        b = (True, False) if gz_path else (False,)
        for use_system in b:
            with self.subTest(use_system=use_system):
                path = self.root.make_file()
                with open(path, 'wt') as o:
                    o.write('foo')
                fmt = get_format('.gz')
                dest = fmt.compress_file(path, use_system=use_system)
                gzfile = Path(str(path) + '.gz')
                assert dest == gzfile
                self.assertTrue(os.path.exists(path))
                self.assertTrue(os.path.exists(gzfile))
                with gzip.open(gzfile, 'rt') as i:
                    assert i.read() == 'foo'
