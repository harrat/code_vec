@pytest.mark.remote
def test_get_urls_version():
    """Regression test against version not being respected in get_urls
    """
    urls = locate.get_urls('L1', 1187008877, 1187008887, version=2)
    assert Path(urls[0]).name == "L-L1_LOSC_CLN_4_V1-1187007040-2048.hdf5"

