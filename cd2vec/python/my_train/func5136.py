def test_ignored_routes(self, test_client):
        rv = test_client.get("/ignore")
        assert "200" in str(rv.status)
