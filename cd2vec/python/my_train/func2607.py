def test_update_profile(self):
        in_new_profile = Profile(display_name_first='Wang', display_name_last='Lao')
        back_profile = self.member.set_profile(in_new_profile)
        out_profile = self.member.get_profile(self.member.member_id)
        assert in_new_profile == back_profile == out_profile
