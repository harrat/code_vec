def test_load_file_absolute(self):
        checks = self.loader.load_from_file(
            os.path.abspath('unittests/resources/checks/emptycheck.py'))
        assert 1 == len(checks)
        assert checks[0].name == 'EmptyTest'
