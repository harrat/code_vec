@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'import-nginx-utc')
    def test_cli_import_solution_024(snippy):
        """Import solution.

        Import new solution from text file. In this case the file extension
        is '*.text'.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.NGINX)
            ]
        }
        content['data'][0]['uuid'] = Content.UUID1
        file_content = Content.get_file_content(Content.TEXT, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '-f', 'one-solution.text'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-solution.text', mode='r', encoding='utf-8')
