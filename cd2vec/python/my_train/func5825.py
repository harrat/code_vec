@mock.patch("IPython.display.display")
def test_reglue_rename(mock_display, notebook_result):
    notebook_result.reglue("output", "new_output")
    mock_display.assert_called_once_with(
        {"text/plain": "'Hello World!'"},
        metadata={"scrapbook": {"name": "new_output", "data": False, "display": True}},
        raw=True,
    )
