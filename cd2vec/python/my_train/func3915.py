def test_seed(self):
        oldseed = agents.seed
        agents.seed = 123
        random_agent = RandomAgent(_line_world_name)
        assert random_agent._model_config.seed == 123
        agents.seed = oldseed
