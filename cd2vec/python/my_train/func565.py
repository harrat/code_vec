def test_add_profile_ok(self, capsys, monkeypatch):
        arg_parser = parser.get_arguments_parser()
        arguments = arg_parser.parse_args(["add", "test_add_profile_ok"])

        fake_input = io.StringIO("\n".join(["test_add_profile_ok"] * 3))
        monkeypatch.setattr("sys.stdin", fake_input)
        executor.execute_command(arguments)

        out, err = capsys.readouterr()
        assert not err
        assert msg.BUILD_USER_INPUT in out
        assert msg.BUILD_MAIL_INPUT in out
        assert msg.BUILD_SKEY_INPUT in out

        success = msg.INFO_ADD_SUCCESS.format("test_add_profile_ok")
        assert success in out

        arguments = arg_parser.parse_args(["del", "test_add_profile_ok"])
        executor.execute_command(arguments)
        out, err = capsys.readouterr()

        delmsg = msg.INFO_DEL_SUCCESS.format("test_add_profile_ok")
        assert delmsg in out
        assert not err
