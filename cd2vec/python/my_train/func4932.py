def test_methods_transmission_from_redis(loadbalanced):
    """Test that methods from redis are available
    direcly from FlaskMultiRedis object (loadbalancing)."""

    assert isinstance(loadbalanced.keys.__self__, StrictRedis)

