def test_hybrid_modify_in_place_onefile(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('modifyinplaceonefile')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'foo'), 'wb') as outfp:
        outfp.write(b'f\n')
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-o', str(outfile), str(indir)])

    # Now open up the ISO with pycdlib and modify it.
    iso = pycdlib.PyCdlib()
    iso.open(str(outfile), 'r+b')
    foostr = b'foo\n'
    iso.modify_file_in_place(BytesIO(foostr), len(foostr), '/FOO.;1')
    iso.close()

    # Now re-open it and check things out.
    open_and_check(outfile, check_onefile)

def test_hybrid_joliet_modify_in_place_onefile(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('jolietmodifyinplaceonefile')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'foo'), 'wb') as outfp:
        outfp.write(b'f\n')
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-J', '-o', str(outfile), str(indir)])
