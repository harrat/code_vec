def test_pfpickle_generation():
    '''
    This tests generation of period-finding result pickles for the LCs.

    '''

    # remove the test-basedir
    shutil.rmtree('./test-basedir', ignore_errors=True)

    # make a basedir in the current directory
    cli.prepare_basedir('./test-basedir',
                        interactive=False)

    # make a new collection and its directories
    cli.new_collection_directories(
        './test-basedir',
        'test-collection'
    )

    basedir = './test-basedir'
    collection = 'test-collection'
    collection_subdir = os.path.join(basedir, collection)
    lightcurves_subdir = os.path.join(basedir, collection, 'lightcurves')

    with tempfile.TemporaryDirectory() as tempdir:

        # download light curves into a temporary directory
        get_lightcurves(tempdir)

        # copy over the lcformat-description.json, object-db.csv, and
        # lcreadermodule.py to collection_subdir
        dl_lcformat_json = os.path.join(tempdir,
                                        'lcc-server-demo',
                                        'lcformat-description.json')
        dl_readermodule = os.path.join(tempdir,
                                       'lcc-server-demo',
                                       'lcreadermodule.py')
        dl_objectdb = os.path.join(tempdir,
                                   'lcc-server-demo',
                                   'object-db.csv')

        shutil.copy(dl_lcformat_json, collection_subdir)
        shutil.copy(dl_readermodule, collection_subdir)
        shutil.copy(dl_objectdb, collection_subdir)

        # copy over the light curves to the lightcurves_subdir
        os.rmdir(lightcurves_subdir)
        dl_lcdir = os.path.join(tempdir, 'lcc-server-demo', 'lightcurves')
        shutil.copytree(dl_lcdir, lightcurves_subdir)

    # check if we have an lcformat-description.json, object-db.csv, and
    # lcreadermodule.py
    lcformat_json = os.path.join(collection_subdir,'lcformat-description.json')
    objectdb_csv = os.path.join(collection_subdir,'object-db.csv')
    lcreadermodule = os.path.join(collection_subdir,'lcreadermodule.py')

    assert os.path.exists(lcformat_json)
    assert os.path.exists(objectdb_csv)
    assert os.path.exists(lcreadermodule)

    # check if we have all 100 original LCs
    lcfiles = sorted(glob.glob(os.path.join(lightcurves_subdir,'*.csv')))
    assert len(lcfiles) == 100
    assert os.path.basename(lcfiles[0]) == 'HAT-215-0001809-lc.csv'
    assert os.path.basename(lcfiles[-1]) == 'HAT-265-0037533-lc.csv'

    # register the LC format
    from astrobase.lcproc import register_lcformat
    from lccserver.backend.abcat import get_lcformat_description
    lcform = get_lcformat_description(
        os.path.join(collection_subdir,'lcformat-description.json')
    )
    register_lcformat(
        lcform['parsed_formatinfo']['formatkey'],
        lcform['parsed_formatinfo']['fileglob'],
        ['rjd'],
        ['aep_000'],
        ['aie_000'],
        lcform['parsed_formatinfo']['readermodule'],
        lcform['parsed_formatinfo']['readerfunc'],
        readerfunc_kwargs=lcform['parsed_formatinfo']['readerfunc_kwargs'],
        normfunc_module=lcform['parsed_formatinfo']['normmodule'],
        normfunc=lcform['parsed_formatinfo']['normfunc'],
        normfunc_kwargs=lcform['parsed_formatinfo']['normfunc_kwargs'],
        magsarefluxes=lcform['magsarefluxes'],
        overwrite_existing=True
    )

    # generate period-finding pickles for the first 5 LCs
    from astrobase.lcproc.periodsearch import parallel_pf

    parallel_pf(
        lcfiles[:5],
        os.path.join(collection_subdir,
                     'periodfinding'),
        lcformat=lcform['formatkey'],
        pfmethods=('gls','pdm'),
        pfkwargs=({},{}),
        getblssnr=False
    )

    # check if all period-finding pickles exist
    pfpickle_subdir = os.path.join(collection_subdir,
                                   'periodfinding')

    pfpickles = sorted(glob.glob(os.path.join(pfpickle_subdir,
                                              'periodfinding*.pkl')))
    assert len(pfpickles) == 5
    assert (
        os.path.basename(pfpickles[0]) == 'periodfinding-HAT-215-0001809.pkl'
    )
    assert (
        os.path.basename(pfpickles[-1]) == 'periodfinding-HAT-215-0010422.pkl'
    )

    # remove the test-basedir
    shutil.rmtree('./test-basedir', ignore_errors=True)

