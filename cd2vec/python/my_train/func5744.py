def test_xmpp_but_server_rejected_opportunistic_tls(self):
        # Given an XMPP server
        hostname = "jabber.org"
        server_location = ServerNetworkLocationViaDirectConnection.with_ip_address_lookup(hostname=hostname, port=5222)
        network_configuration = ServerNetworkConfiguration(
            # But we provide a wrong XMPP setting
            xmpp_to_hostname="lol.lol",
            tls_server_name_indication=hostname,
            tls_opportunistic_encryption=ProtocolWithOpportunisticTlsEnum.XMPP,
        )

        # When testing connectivity, it fails with the right error
        with pytest.raises(ServerRejectedOpportunisticTlsNegotiation):
            ServerConnectivityTester().perform(server_location, network_configuration)
