def test_set_param_overrides(self):
        self.client.set_param_overrides({'foo': 'baz'})

        self._make_request('https://httpbin.org/?foo=bar&spam=eggs')

        last_request = self.client.get_last_request()

        params = {k: v[0] for k, v in parse_qs(urlsplit(last_request['url']).query).items()}
        self.assertEqual({
            'foo': 'baz',
            'spam': 'eggs'
        }, params)
