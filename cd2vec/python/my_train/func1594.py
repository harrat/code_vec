def test_pad_with_drill(self):
        pad = Pad.parse('(pad 1 smd rect (at 0.1 0.1) (size 0.2 0.2) '
                        '(layers F.Cu) (drill 0.8))')
        assert pad.drill.size == 0.8
        assert Pad.parse(pad.to_string()) == pad
