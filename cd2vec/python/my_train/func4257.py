def test__mirror_ontology_real(testfiles):
    annotation = flatten_dict(testfiles, 'annotation').values()
    ontology = flatten_dict(testfiles, 'ontology').values()
    orig = [363, 470]
    for a, o, l in zip(annotation, ontology, orig):
        annot = samples_._mirror_ontology(a, o)
        assert len(annot) == l

