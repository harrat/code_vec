def test_simple_signal_object_identifier_attribute():

    class Foo():
        def on_test_identifier(self):
            1 / 0
        __init__ = register_object

    f1 = Foo()
    f1.obj = 'obj1'

    on_test_identifier(obj='xxx')

    with pytest.raises(ZeroDivisionError):
        on_test_identifier(obj='obj1')

    unregister_object(f1)

