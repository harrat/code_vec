def test_already_running(self):
        with test_utils.lauch(self.base_config, no_coverage=True, get_process=True) as p1:
            pid = p1.pid
            with test_utils.lauch(self.base_config, get_process=True) as p2:
                self.assertEqual(p2.wait(), 3)
        with open(self.base_config["pidfile"], 'w') as f:
            f.write("%s" % pid)
        try:
            with test_utils.lauch(self.base_config, get_process=True) as p:
                pass
            self.assertEqual(p.wait(), 0)
            with open(self.base_config["pidfile"], 'w') as f:
                f.write("foo")
            with test_utils.lauch(self.base_config, get_process=True) as p:
                pass
            self.assertEqual(p.wait(), 0)
            with open(self.base_config["pidfile"], 'w') as f:
                f.write("")
            os.chmod(self.base_config["pidfile"], 0)
            with test_utils.lauch(self.base_config, get_process=True) as p:
                self.assertEqual(p.wait(), 6)
        finally:
            try:
                os.remove(self.base_config["pidfile"])
            except OSError:
                pass
