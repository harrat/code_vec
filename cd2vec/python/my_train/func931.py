@pytest.mark.parametrize('entity_class', CLASSES)
def test_specific_entity(mocker, repo, entity_class):
    """ This test will run for each entity in the repo, when each time it will randomly generate dependencies
        in the repo and verify that the expected dependencies has been updated in the pack metadata correctly.

        Given
        - Content repository and content entity
        When
        - Running find_dependencies
        Then
        - Update packs dependencies in pack metadata
    """
    number_of_packs = 20
    repo.setup_content_repo(number_of_packs)
    repo.setup_one_pack('CommonTypes')

    methods_pool: list = \
        [(method_name, entity_class) for method_name in list(entity_class.__dict__.keys())
         if '_' != method_name[0]]

    dependencies = run_random_methods(repo, 0, methods_pool, len(methods_pool))

    with ChangeCWD(repo.path):
        # Circle froze on 3.7 dut to high usage of processing power.
        # pool = Pool(processes=cpu_count() * 2) is the line that in charge of the multiprocessing initiation,
        # so changing `cpu_count` return value to 1 still gives you multiprocessing but with only 2 processors,
        # and not the maximum amount.
        import demisto_sdk.commands.common.update_id_set as uis
        mocker.patch.object(uis, 'cpu_count', return_value=1)
        PackDependencies.find_dependencies('pack_0', silent_mode=True)

    dependencies_from_pack_metadata = repo.packs[0].pack_metadata.read_json_as_dict().get('dependencies').keys()

    if 'pack_0' in dependencies:
        dependencies.remove('pack_0')

    assert IsEqualFunctions.is_lists_equal(list(dependencies), list(dependencies_from_pack_metadata))

