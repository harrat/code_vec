def test_13_pre_config():
    res = None
    
    try:
        res = pydbvolve.get_db_credentials(None)
    except Exception as e:
        assert(e.__class__.__name__ == 'NotImplementedError')
    
    assert(res is None)
    
    try:
        res = pydbvolve.get_db_connection(None, None)
    except Exception as f:
        assert(f.__class__.__name__ == 'NotImplementedError')
    
    assert(res is None)
# End test_13_pre_config
