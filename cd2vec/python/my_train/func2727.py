def test_hosts_delete_with_obj(self, centreon_con, host_load_data):
        host = host_load_data
        data = dict()
        data['action'] = 'del'
        data['object'] = 'HOST'
        data['values'] = 'mail-uranus-frontend'

        with patch('requests.post') as patched_post:
            centreon_con.hosts.delete(host, post_refresh=False)
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
