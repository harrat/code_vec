def test_get_attributes(self):
        """
        Test that a GetAttributes request can be processed correctly.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        payload = payloads.GetAttributesRequestPayload(
            unique_identifier='1',
            attribute_names=['Object Type', 'Cryptographic Algorithm']
        )

        response_payload = e._process_get_attributes(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call(
            "Processing operation: GetAttributes"
        )
        self.assertEqual(
            '1',
            response_payload.unique_identifier
        )
        self.assertEqual(
            2,
            len(response_payload.attributes)
        )

        attribute_factory = factory.AttributeFactory()

        attribute = attribute_factory.create_attribute(
            enums.AttributeType.OBJECT_TYPE,
            enums.ObjectType.SYMMETRIC_KEY
        )
        self.assertIn(attribute, response_payload.attributes)

        attribute = attribute_factory.create_attribute(
            enums.AttributeType.CRYPTOGRAPHIC_ALGORITHM,
            enums.CryptographicAlgorithm.AES
        )
        self.assertIn(attribute, response_payload.attributes)
