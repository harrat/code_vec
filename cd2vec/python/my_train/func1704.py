def test_discover_benchmarks(benchmarks_fixture):
    conf, repo, envs, commit_hash = benchmarks_fixture

    b = benchmarks.Benchmarks.discover(conf, repo, envs, [commit_hash],
                                       regex='secondary')
    assert len(b) == 6

    old_branches = conf.branches
    conf.branches = ["master", "some-missing-branch"]  # missing branches ignored
    b = benchmarks.Benchmarks.discover(conf, repo, envs, [commit_hash],
                                       regex='example')
    conf.branches = old_branches
    assert len(b) == 36

    b = benchmarks.Benchmarks.discover(conf, repo, envs, [commit_hash],
                              regex='time_example_benchmark_1')
    assert len(b) == 2

    b = benchmarks.Benchmarks.discover(conf, repo, envs, [commit_hash],
                              regex=['time_example_benchmark_1',
                                     'some regexp that does not match anything'])
    assert len(b) == 2

    b = benchmarks.Benchmarks.discover(conf, repo, envs, [commit_hash], regex='custom')
    assert sorted(b.keys()) == ['custom.time_function', 'custom.track_method',
                                'named.track_custom_pretty_name']
    assert 'pretty_name' not in b['custom.track_method']
    assert b['custom.time_function']['pretty_name'] == 'My Custom Function'
    assert b['named.track_custom_pretty_name']['pretty_name'] == 'this.is/the.answer'

    # benchmark param selection with regex
    b = benchmarks.Benchmarks.discover(conf, repo, envs, [commit_hash],
                                       regex=r'track_param_selection\(.*, 3\)')
    assert list(b.keys()) == ['params_examples.track_param_selection']
    assert b._benchmark_selection['params_examples.track_param_selection'] == [0, 2]
    b = benchmarks.Benchmarks.discover(conf, repo, envs, [commit_hash],
                                       regex=r'track_param_selection\(1, ')
    assert list(b.keys()) == ['params_examples.track_param_selection']
    assert b._benchmark_selection['params_examples.track_param_selection'] == [0, 1]
    b = benchmarks.Benchmarks.discover(conf, repo, envs, [commit_hash],
                                       regex='track_param_selection')
    assert list(b.keys()) == ['params_examples.track_param_selection']
    assert b._benchmark_selection['params_examples.track_param_selection'] == [0, 1, 2, 3]

    b = benchmarks.Benchmarks.discover(conf, repo, envs, [commit_hash])
    assert len(b) == 50

    assert 'named.OtherSuite.track_some_func' in b

    params = b['params_examples.FunctionParamSuite.time_func']['params']
    assert len(params) == 1
    assert len(params[0]) == 2
    assert params[0][0] == '<function track_param>'
    # repr is a bit different on py2 vs py3 here
    assert params[0][1] in ['<function FunctionParamSuite.<lambda>>', '<function <lambda>>']

    # Raw timing benchmarks
    assert b['timeraw_examples.TimerawSuite.timeraw_count']['repeat'] == 7
    assert b['timeraw_examples.TimerawSuite.timeraw_count']['number'] == 3
    assert b['timeraw_examples.TimerawSuite.timeraw_setup']['number'] == 1

