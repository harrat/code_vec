@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_api_search_solution_001(server):
        """Search solution with GET.

        Send GET /solutions and search keywords from all attributes. The
        search query matches to two solutions and both of them are returned.
        The search is sorted based on one attribute. The search result limit
        defined in the search query is not exceeded.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '4847'
        }
        expect_body = {
            'meta': {
                'count': 2,
                'limit': 20,
                'offset': 0,
                'total': 2
            },
            'data': [{
                'type': 'solution',
                'id': Solution.BEATS_UUID,
                'attributes': Storage.ebeats
            }, {
                'type': 'solution',
                'id': Solution.NGINX_UUID,
                'attributes': Storage.dnginx
            }]
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/solutions',
            headers={'accept': 'application/vnd.api+json; charset=UTF-8'},
            query_string='sall=nginx%2CElastic&limit=20&sort=brief')
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
