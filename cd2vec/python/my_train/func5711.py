def test_flex_alloc_valid_partition_cmd(make_flexible_job):
    job = make_flexible_job('all', sched_partition='p2')
    prepare_job(job)
    assert job.num_tasks == 8

