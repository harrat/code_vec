def test_kinesis_trigger(tracer_and_invocation_support, handler, mock_kinesis_event, mock_context):
    thundra, handler = handler
    tracer, invocation_support = tracer_and_invocation_support
    try:
        response = handler(mock_kinesis_event, mock_context)
    except:
        print("Error running handler!")
        raise
    span = tracer.recorder.get_spans()[0]

    invocation_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, InvocationPlugin):
            invocation_plugin = plugin

    assert lambda_event_utils.get_lambda_event_type(mock_kinesis_event,
                                                    mock_context) == lambda_event_utils.LambdaEventType.Kinesis

    assert span.get_tag(constants.SpanTags['TRIGGER_DOMAIN_NAME']) == constants.DomainNames['STREAM']
    assert span.get_tag(constants.SpanTags['TRIGGER_CLASS_NAME']) == constants.ClassNames['KINESIS']
    assert span.get_tag(constants.SpanTags['TRIGGER_OPERATION_NAMES']) == ['example_stream']

    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_DOMAIN_NAME']) == constants.DomainNames['STREAM']
    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_CLASS_NAME']) == constants.ClassNames['KINESIS']
    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_OPERATION_NAMES']) == ['example_stream']

    links = ["eu-west-2:example_stream:shardId-000000000000:49545115243490985018280067714973144582180062593244200961"]
    assert invocation_plugin.invocation_data['incomingTraceLinks'] == links

