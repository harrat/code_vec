def test_class_decorated(self):
        inj = Injector(use_decorators=True)
        otherthing = inj.get_instance(OtherThing)
        assert otherthing.value == 3
        assert isinstance(otherthing.thing, Thing)
        assert isinstance(otherthing, OtherThing)
