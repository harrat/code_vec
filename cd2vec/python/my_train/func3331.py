@pytest.mark.vcr()
def test_workbench_export_asset_uuid_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.export(asset_uuid=123)
