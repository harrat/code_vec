def test_drill_oval(self):
        drill = Drill.parse('(drill oval 0.6 0.8)')
        assert drill.size == [0.6, 0.8]
        assert Drill.parse(drill.to_string()) == drill
