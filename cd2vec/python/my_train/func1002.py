def test_use_previous_end_time_as_start_time(cli, entries_file):
    entries = """20/01/2014
alias_1 09:00-10:00 foobar
"""
    expected = """20/01/2014
alias_1 09:00-10:00 foobar
alias_1 10:00-? ?
"""

    entries_file.write(entries)
    with freeze_time('2014-01-20'):
        cli('start', ['alias_1'])
    assert entries_file.read() == expected

