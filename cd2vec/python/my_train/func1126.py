def test_search_sort(self):
        """ Perform search with sort """
        with open(os.path.join(self.path, 'aoi1.geojson')) as f:
            aoi = json.load(f)
        search = Search.search(datetime='2020-06-07', intersects=aoi['geometry'], sortby=['-properties.datetime'])
        items = search.items()
        assert(len(items) == 12)
