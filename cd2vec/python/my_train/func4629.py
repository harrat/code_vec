def test_restore_or_delete_policy_restore(self):
        """
        Test that the PolicyDirectoryMonitor can correctly restore policy data
        upon a policy file change.
        """
        m = monitor.PolicyDirectoryMonitor(
            self.tmp_dir,
            multiprocessing.Manager().dict()
        )
        m.logger = mock.MagicMock(logging.Logger)

        m.policy_cache = {
            "policy_A": [
                (
                    1480043060.870089,
                    os.path.join(self.tmp_dir, "policy_1.json"),
                    {'{"policy_1"}'}
                ),
                (
                    1480043062.02171,
                    os.path.join(self.tmp_dir, "policy_2.json"),
                    {'{"policy_2"}'}
                ),
                (
                    1480043063.453713,
                    os.path.join(self.tmp_dir, "policy_3.json"),
                    {'{"policy_3"}'}
                )
            ]
        }
        m.policy_store["policy_A"] = {'{"policy_4"}'}
        m.policy_map["policy_A"] = os.path.join(self.tmp_dir, "policy_4.json")

        m.restore_or_delete_policy("policy_A")

        m.logger.info.assert_not_called()
        self.assertEqual(
            [
                (
                    1480043060.870089,
                    os.path.join(self.tmp_dir, "policy_1.json"),
                    {'{"policy_1"}'}
                ),
                (
                    1480043062.02171,
                    os.path.join(self.tmp_dir, "policy_2.json"),
                    {'{"policy_2"}'}
                )
            ],
            m.policy_cache.get("policy_A", [])
        )
        self.assertEqual(
            {'{"policy_3"}'},
            m.policy_store.get("policy_A", {})
        )
        self.assertEqual(
            os.path.join(self.tmp_dir, "policy_3.json"),
            m.policy_map.get("policy_A", None)
        )
