def test_callbacks_class():
    """Assert that custom callbacks works as intended for a class."""
    atom = ATOMClassifier(X_bin, y_bin, random_state=1)
    atom.run('LR', n_calls=5, bo_params={'callbacks': TimerCallback()})
    assert not atom.errors

