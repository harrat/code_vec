def test_invalid_orderref_raises_error(cert_and_key):
    c = bankid.BankIDJSONClient(certificates=cert_and_key, test_server=True)
    with pytest.raises(bankid.exceptions.InvalidParametersError):
        collect_status = c.collect("invalid-uuid")
