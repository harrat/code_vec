def test_small_retrievals(self):
        ''' verify contents up to the average chunk size that are stored in a single chunk '''
        for content_length in range(0, self.S + 1):
            content = bytes([self.random.randint(0, 255)
                             for _ in range(content_length)])
            k = self.seccs.put_content(content)

            m = self.seccs.get_content(k)
            self.assertEqual(m, content)
