def test_implements_no_implementation_static_method():
    """
    Case: do not implement interface member that is static method.
    Expect: class does not implement interface member error message.
    """
    class HumanBasicsInterface:

        @staticmethod
        def eat(food, *args, allergy=None, **kwargs):
            pass

    with pytest.raises(InterfaceMemberHasNotBeenImplementedException) as error:

        @implements(HumanBasicsInterface)
        class HumanWithoutImplementation:
            pass

    assert 'class HumanWithoutImplementation does not implement ' \
           'interface member HumanBasicsInterface.eat(food, args, allergy, kwargs)' == error.value.message
