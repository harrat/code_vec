def test_catalogue_shape(query):
    """
    Test the catalogue for shape consistency
    """

    length = query.catalogue_len
    shape = query.catalogue_shape
    rows = query.catalogue_nrows
    cols = query.catalogue_ncols
    colnames = query.columns

    assert length == rows and length == shape[0]
    assert cols == len(colnames) and cols == shape[1]

