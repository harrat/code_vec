def test_custom_handler_levels(self):
        self.handler.setLevel('verbose')
        self.handler.setLevel(rlog.VERBOSE)

        self.logger_with_check.debug('foo')
        self.logger_with_check.verbose('bar')

        assert not self.found_in_logfile('foo')
        assert self.found_in_logfile('bar')
