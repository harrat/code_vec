def test_get_referenced_object(self):
        """py.test for get_referenced_object"""
        idf = IDF()
        idf.initnew("test.idf")
        idf.newidfobject("VERSION")  # does not have a field "Name"

        # construction material
        construction = idf.newidfobject("CONSTRUCTION", Name="construction")
        construction.Outside_Layer = "TestMaterial"

        expected = idf.newidfobject("MATERIAL", Name="TestMaterial")

        fetched = idf.getobject("MATERIAL", "TestMaterial")
        assert fetched == expected

        material = construction.get_referenced_object("Outside_Layer")
        assert material == expected

        # window material
        glazing_group = idf.newidfobject(
            "WINDOWMATERIAL:GLAZINGGROUP:THERMOCHROMIC", Name="glazing_group"
        )
        glazing_group.Window_Material_Glazing_Name_1 = "TestWindowMaterial"

        expected = idf.newidfobject(
            "WINDOWMATERIAL:GLAZING", Name="TestWindowMaterial"
        )  # has several \references

        fetched = idf.getobject("WINDOWMATERIAL:GLAZING", "TestWindowMaterial")
        assert fetched == expected

        material = glazing_group.get_referenced_object("Window_Material_Glazing_Name_1")
        assert material == expected
