@pytest.mark.unit
def test_make_tile_mpl():
    """Test convert.make_tile_mpl"""
    helpers.setup()

    out_dir = helpers.TEST_PATH
    test_arr = np.arange(100 * 100).reshape([100, 100])
    test_job = (0, 0, 0, slice(0, 100), slice(0, 100))
    vmin = test_arr.min()
    vmax = test_arr.max()

    os.makedirs(os.path.join(out_dir, "0/0/"))

    convert.make_tile_mpl(vmin, vmax, out_dir, test_arr, test_job)

    actual_img = np.array(Image.open(os.path.join(out_dir, "0/0/0.png")))

    expected_img = test_arr // 256

    np.array_equal(expected_img, actual_img)

    helpers.tear_down()

