def test_load_config_from_env(self, config):
        config.register_config(name='test1', config_type=str, default='test1', env_var='TEST1')
        config.load_config_from_env()
        assert config.test1 == 'test1'
        os.environ['TEST1'] = 'test2'
        config.load_config_from_env()
        assert config.test1 == 'test2'
        del os.environ['TEST1']
        config.set_config('test1', 'test1')
        config.load_config_from_env()
        assert config.test1 == 'test1'
