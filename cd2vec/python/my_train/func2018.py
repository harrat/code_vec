def test_alias_search_mapping_partial(cli, alias_config):
    output = cli('alias', ['list', '--no-inactive', 'active'])
    lines = output.splitlines()

    assert lines == [
        "[test] active1 -> 43/1 (active project, activity 1)",
        "[test] active2 -> 43/2 (active project, activity 2)",
        "[test] p2_active -> 44/1 (2nd active project, activity 1)",
    ]
