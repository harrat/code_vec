@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'import-kafka', 'import-pytest')
    def test_api_search_tags_001(server):
        """Get unique content based on ``tags`` attribute.

        Send GET /tags to get unique groups from all content categories.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '245'
        }
        expect_body = {
            'data': {
                'type': 'tags',
                'attributes': {
                    'tags': {
                        'docker': 3,
                        'moby': 3,
                        'cleanup': 2,
                        'container': 2,
                        'docker-ce': 2,
                        'driver': 1,
                        'kafka': 1,
                        'kubernetes': 1,
                        'logging': 1,
                        'logs2kafka': 1,
                        'plugin': 1,
                        'docs': 1,
                        'pytest': 1,
                        'python': 1
                    }
                }
            }
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/tags',
            headers={'accept': 'application/vnd.api+json'})
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
