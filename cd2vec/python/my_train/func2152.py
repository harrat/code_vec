def test_have_not_name(self):
        assert 2 == self.count_checks(filters.have_not_name('check1'))
        assert 1 == self.count_checks(filters.have_not_name('check1|check3'))
        assert 0 == self.count_checks(filters.have_not_name(
            'check1|check2|check3'))
        assert 3 == self.count_checks(filters.have_not_name('Check1'))
        assert 2 == self.count_checks(filters.have_not_name('(?i)Check1'))
