def test_run_main(capsys, atlas, datadir):
    outputfile = os.path.join(datadir, 'abagen_expression.csv')

    # check basic usage
    run.main([
        '--data-dir', datadir,
        '--donors', '12876', '15496',
        '--output-file', outputfile,
        atlas['image']
    ])
    assert os.path.exists(outputfile)

    # check that save donors/counts outputs desired files
    run.main([
        '--data-dir', datadir,
        '--donors', '12876', '15496',
        '--output-file', outputfile,
        '--save-donors', '--save-counts',
        atlas['image']
    ])
    for rep in ['_counts.csv', '_12876.csv', '_15496.csv']:
        assert os.path.exists(outputfile.replace('.csv', rep))

    # check stdout (BLARGH)
    run.main([
        '--data-dir', datadir,
        '--donors', '12876', '15496',
        '--output-file', outputfile,
        '--stdout',
        atlas['image']
    ])
    stdout = capsys.readouterr().out.replace('\r\n', '\n').replace('\r', '\n')
    with open(outputfile, 'r') as src:
        data = src.read().replace('\r\n', '\n').replace('\r', '\n')
        assert stdout == data
