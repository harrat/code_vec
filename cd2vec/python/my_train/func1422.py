def test_compute_acm_real_path(self):
        aligner = DTWAligner(real_wave_path=self.AUDIO_FILE)
        with self.assertRaises(DTWAlignerNotInitialized):
            aligner.compute_accumulated_cost_matrix()
