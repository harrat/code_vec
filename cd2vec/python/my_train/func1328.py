def test_prepare(fake_job):
    sched_name = fake_job.scheduler.registered_name
    if sched_name == 'pbs':
        fake_job.options += ['mem=100GB', 'cpu_type=haswell']
    elif sched_name == 'torque':
        fake_job.options += ['-l mem=100GB', 'haswell']

    prepare_job(fake_job)
    with open(fake_job.script_filename) as fp:
        found_directives = set(re.findall(r'^\#\w+ .*', fp.read(),
                                          re.MULTILINE))

    expected_directives = globals()[f'_expected_{sched_name}_directives']
    assert_job_script_sanity(fake_job)
    assert expected_directives(fake_job) == found_directives

