@redirect_io
    def test_help_missing_params(self):
        """ Ensure the appropriate error is thrown when a required argument is missing. """

        # work around an argparse behavior where output is not printed and SystemExit
        # is not raised on Python 2.7.9
        if sys.version_info < (2, 7, 10):
            try:
                self.cli_ctx.invoke('n1 -a 1 --arg 2'.split())
            except SystemExit:
                pass
        else:
            with self.assertRaises(SystemExit):
                self.cli_ctx.invoke('n1 -a 1 --arg 2'.split())
