@pytest.mark.parametrize(
    "url_to_check",
    [
        "https://pypi.org/numpy-1.16.3-cp37-cp37m-win_amd64.whl#sha256=HASH",
        "https://pypi.org/numpy-1.16.3-cp37-cp37m-manylinux1_x86_64.whl#sha256=HASH",
        "https://pypi.org/numpy-1.16.3.zip#sha256=HASH",
    ],
)
def test_python_requires_wheel_tags(
    mocked_responses, tmpdir, mock_py_version, read_contents, url_to_check
):
    mock_py_version("3.7.12")

    wheeldir = str(tmpdir)
    mocked_responses.add(
        responses.GET,
        INDEX_URL + "/numpy/",
        body=read_contents("numpy.html"),
        status=200,
    )

    repo = PyPIRepository(INDEX_URL, wheeldir)
    candidate = [
        candidate
        for candidate in repo.get_candidates(pkg_resources.Requirement.parse("numpy"))
        if candidate.link[1] == url_to_check
    ][0]
    if candidate.py_version:
        assert candidate.py_version.check_compatibility()

