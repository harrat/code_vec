@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'import-remove', 'import-gitlog', 'export-time')
    def test_cli_export_solution_031(snippy):
        """Export all solutions.

        Export all content by defining the content category to ``all``. This
        must export solutions, snippets and references.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Snippet.REMOVE,
                Solution.BEATS,
                Solution.NGINX,
                Reference.GITLOG
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'all'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './content.mkdn', content)
