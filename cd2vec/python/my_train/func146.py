def test_h5get_image_destination(self):
        file = main_path +'test_data.h5'
        scan_numbers = [178, 178]
        dataset_name = 'test_178'
        test_fs = SXDMFrameset(file, dataset_name, scan_numbers=scan_numbers)

        row = 1
        column = 9

        create_imagearray(test_fs, True)

        pix = grab_pix(array=test_fs.image_array, row=row, column=column, int_convert=True)
        destination = h5get_image_destination(self=test_fs, pixel=pix)
        destination2 = h5get_image_destination_v2(self=test_fs, pixel=pix)

        equality = destination == ['images/0178/165137', 'images/0178/165144']
        equality2 = destination2 == [('0178', '165137'), ('0178', '165144')]

        self.assertTrue(equality)
        self.assertTrue(equality2)
