def test_RFECV_strategy_before_pipeline_classification():
    """Assert that the RFECV strategy works before a fitted pipeline."""
    fs = FeatureSelector('RFECV', solver='LGB_class', n_features=None)
    X = fs.fit_transform(X_bin, y_bin)
    assert X.shape[1] == 20

