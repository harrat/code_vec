def test_getrefnames():
    """py.test for getrefnames"""
    tdata = (
        (
            "ZONE",
            [
                "ZoneNames",
                "OutFaceEnvNames",
                "ZoneAndZoneListNames",
                "AirflowNetworkNodeAndZoneNames",
            ],
        ),  # objkey, therefs
        (
            "FluidProperties:Name".upper(),
            ["FluidNames", "FluidAndGlycolNames"],
        ),  # objkey, therefs
        ("Building".upper(), []),  # objkey, therefs
    )
    for objkey, therefs in tdata:
        fhandle = StringIO("")
        idf = IDF(fhandle)
        result = modeleditor.getrefnames(idf, objkey)
        assert result == therefs

