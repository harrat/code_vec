def test_olink():

    # create the olink object
    obj = {**ASSAY_CORE}
    obj["panel"] = "panel v1"

    # create the artifact object
    npx = ARTIFACT_OBJ.copy()
    npx["data_format"] = "NPX"
    npx["samples"] = ["CTTTPPPS1.00", "CTTTPPPS2.00", "CTTTPPPS3.00"]
    npx["number_of_samples"] = 3
    csv = ARTIFACT_OBJ.copy()
    csv["data_format"] = "CSV"
    record = OLINK_RECORD.copy()
    record["files"]["assay_npx"] = npx
    record["files"]["assay_raw_ct"] = csv

    # add a demo record.
    obj["records"] = [record]
    obj["study"] = {"npx_manager_version": "whatever", "study_npx": npx}

    # create validator assert schemas are valid.
    validator = _fetch_validator("olink")
    validator.validate(obj)

    # assert negative behaviors
    del obj["records"][0]["number_of_samples"]
    with pytest.raises(jsonschema.ValidationError):
        validator.validate(obj)
