def test_02_check_migration_table_bad_structure():
    """Verify that an aberrant table structure can be detected"""
    config = pydbvolve.initialize(TEST_CONFIG_FILE, 'info', 'r1.1.10', True, False)
    _create_bad_migration_table(config)
    exc = None
    try:
        res = pydbvolve.check_migration_table(config)
    except Exception as e:
        exc = e
    
    assert(isinstance(exc, pydbvolve.MigrationTableOutOfSync))
# End test_02_check_migration_table_bad_structure
