def test_processing(self, rnp):
        prev_time = time.time()
        rnp.new_task(sleepy_task).run(every=0.1, times=2).result()
        time_elapsed = time.time() - prev_time
        is_ok = (time_elapsed < 0.51) and (time_elapsed > 0.2)
        assert is_ok is True
