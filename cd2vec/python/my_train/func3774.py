@pytest.mark.parametrize('version, result', [
    ('9.8.7', True),
    ('1.2.3', False),
])
def test_check_latest_version(conf, version, result):
    response_obj = requests.Response()
    response_obj.status_code = 200
    response_obj.headers['Content-Type'] = 'application/json'
    response_obj._content = json.dumps({'releases': {version: ''}}).encode()

    with mock.patch('andriller.config.requests.get', return_value=response_obj):
        conf.check_latest_version()
        assert conf.update_available == result
