def test_pad_with_multiple_layers(self):
        pad = Pad.parse('(pad 1 smd rect (at 0.1 0.1) (size 0.2 0.2) '
                        '(layers F.Cu B.Cu))')
        assert pad.layers == ['F.Cu', 'B.Cu']
        assert Pad.parse(pad.to_string()) == pad
