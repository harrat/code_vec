def test_activate(self):
        """
        Test that an Activate request can be processed correctly.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        managed_object = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )
        e._data_session.add(managed_object)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        self.assertEqual(enums.State.PRE_ACTIVE, managed_object.state)

        object_id = str(managed_object.unique_identifier)

        # Test by specifying the ID of the object to activate.
        payload = payloads.ActivateRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(object_id)
        )

        response_payload = e._process_activate(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call(
            "Processing operation: Activate"
        )
        self.assertEqual(
            str(object_id),
            response_payload.unique_identifier.value
        )

        symmetric_key = e._data_session.query(
            pie_objects.SymmetricKey
        ).filter(
            pie_objects.ManagedObject.unique_identifier == object_id
        ).one()

        self.assertEqual(enums.State.ACTIVE, symmetric_key.state)

        args = (payload,)
        regex = "The object state is not pre-active and cannot be activated."
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            regex,
            e._process_activate,
            *args
        )

        # Test that the ID placeholder can also be used to specify activation.
        e._id_placeholder = str(object_id)
        payload = payloads.ActivateRequestPayload()
        args = (payload,)
        regex = "The object state is not pre-active and cannot be activated."
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            regex,
            e._process_activate,
            *args
        )
