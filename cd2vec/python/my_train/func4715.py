@staticmethod
    @pytest.mark.usefixtures('default-references', 'export-time')
    def test_cli_export_reference_031(snippy):
        """Export reference template.

        Export reference template by explicitly defining content category
        and the template Markdown format.
        """

        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'reference', '--template', '--format', 'mkdn'])
            assert cause == Cause.ALL_OK
            mock_file.assert_called_once_with('./reference-template.mkdn', mode='w', encoding='utf-8')
            file_handle = mock_file.return_value.__enter__.return_value
            file_handle.write.assert_called_with(Const.NEWLINE.join(Reference.TEMPLATE_MKDN))
