@pytest.mark.parametrize("model", ["bert", "distilbert"])
def test_predict(model):
    assert execute_pipeline(model,
        "Since when does the Excellence Program of BNP Paribas exist?"
    ) == ("January 2016", "BNP Paribas? commitment to universities and schools")
