def test_insert_into_cache_does_not_duplicate(self):
    """test_insert_into_cache_does_not_duplicate ensures that maybe_insert_into_cache
    does not insert results into cache if they exist in cache"""
    file = Path(__file__).parent / "ossindexresponse.txt"
    with open(file, "r") as stdin:
      response = json.loads(stdin.read(), cls=ResultsDecoder)
      self.func.maybe_insert_into_cache(response)
      (cached, num_cached) = self.func.maybe_insert_into_cache(response)
    self.assertEqual(num_cached, 0)
    self.assertEqual(cached, False)
