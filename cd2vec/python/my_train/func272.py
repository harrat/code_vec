def test_runall_skip_performance_check(make_runner, make_cases,
                                       common_exec_ctx):
    runner = make_runner()
    runner.policy.skip_performance_check = True
    runner.runall(make_cases())
    stats = runner.stats
    assert 8 == stats.num_cases()
    assert_runall(runner)
    assert 4 == len(stats.failures())
    assert 2 == num_failures_stage(runner, 'setup')
    assert 1 == num_failures_stage(runner, 'sanity')
    assert 0 == num_failures_stage(runner, 'performance')
    assert 1 == num_failures_stage(runner, 'cleanup')

