@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'import-kafka')
    def test_api_search_solution_005(server):
        """Search solution with GET.

        Send GET /solutions and search keywords from all attributes. The
        search query matches to three solutions but limit defined in search
        query results only two of them sorted by the created attribute in
        descending order and then based on brief attribute also in descending
        order.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '4846'
        }
        expect_body = {
            'meta': {
                'count': 2,
                'limit': 2,
                'offset': 0,
                'total': 3
            },
            'data': [{
                'type': 'solution',
                'id': Solution.NGINX_UUID,
                'attributes': Storage.dnginx
            }, {
                'type': 'solution',
                'id': Solution.BEATS_UUID,
                'attributes': Storage.ebeats
            }]
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/solutions',
            headers={'accept': 'application/json'},
            query_string='sall=docker,beats%2Cnmap&limit=2&sort=-created,-brief')
        assert result.headers == expect_headers
        assert result.status == falcon.HTTP_200
        Content.assert_restapi(result.json, expect_body)
