def test_querying(self, bids_layout):

        # With regex_search = True (as set in Layout())
        result = bids_layout.get(subject=1, run=1, session=1,
                                 extensions='nii.gz')
        assert len(result) == 8
        result = bids_layout.get(subject='01', run=1, session=1,
                                 type='phasediff', extensions='.json')
        assert len(result) == 1
        assert 'phasediff.json' in result[0].filename
        assert hasattr(result[0], 'run')
        assert result[0].run == 1

        # With exact matching...
        result = bids_layout.get(subject='1', run=1, session=1,
                                 extensions='nii.gz', regex_search=False)
        assert len(result) == 0

        result = bids_layout.get(target='subject', return_type='id')
        assert len(result) == 10
        assert '03' in result
        result = bids_layout.get(target='subject', return_type='dir')

        if hasattr(bids_layout, '_hdfs_client'):
            assert bids_layout._hdfs_client.list(bids_layout.root)
        else:
            assert os.path.exists(join(bids_layout.root, result[0]))
            assert os.path.isdir(join(bids_layout.root, result[0]))

        result = bids_layout.get(target='subject', type='phasediff',
                                 return_type='file')

        if hasattr(bids_layout, '_hdfs_client'):
            assert all([bids_layout._hdfs_client.content(f) for f in result])
        else:
            assert all([os.path.exists(join(bids_layout.root, f))
                        for f in result])
