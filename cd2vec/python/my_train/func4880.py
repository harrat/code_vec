def test_validate_profile_off(self):
		model.factory.validate_profile = False
		ia = model.IdentifierAssignment()
		# If it's not turned off this should raise
		model.factory.validate_profile = True
		self.assertRaises(model.ProfileError, model.IdentifierAssignment)		
		p1 = model.Person()
		self.assertRaises(model.ProfileError, p1.__setattr__, 'documented_in', "foo")
