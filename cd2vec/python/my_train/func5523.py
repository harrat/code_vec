def test_kwargs_params(self):
        tech = TechMS()

        data = {'get': '/params'}
        call = TechState.get_call_data(None, data)
        res = tech(call, {})
        assert res['statusCode'] == 200
        assert res['body'] == "get 1 and 2"

        query_params = {'value': [3], 'other': [4]}
        data = {'get': '/params', 'query_params': query_params}
        call = TechState.get_call_data(None, data)
        res = tech(call, {})
        assert res['statusCode'] == 200
        assert res['body'] == "get 3 and 4"

        query_params = {'value': [5], 'other': [6]}
        data = {'post': '/params', 'query_params': query_params}
        call = TechState.get_call_data(None, data)
        res = tech(call, {})
        assert res['statusCode'] == 200
        assert res['body'] == "get 5 and 6"

        body = {'value': 7, 'other': 8}
        data = {'post': '/params', 'body': body}
        call = TechState.get_call_data(None, data)
        res = tech(call, {})
        assert res['statusCode'] == 200
        assert res['body'] == "get 7 and 8"
