def test_ctx_with_identifier():
    class Foo():
        @contextmanager
        def on_ctx_test_identifier(self, before, after):
            self.result.append(before)
            yield
            self.result.append(after)

    f1 = Foo()
    f1.obj = 'obj1'
    f1.result = []
    f2 = Foo()
    f2.obj = 'obj2'
    f2.result = []

    register_object(f1)
    register_object(f2)

    with on_ctx_test_identifier(before=1, after=2, obj='obj1'):
        assert f1.result == [1]
        assert f2.result == []
    assert f1.result == [1, 2]
    assert f2.result == []

    with on_ctx_test_identifier(before=3, after=4, obj='obj2'):
        assert f1.result == [1, 2]
        assert f2.result == [3]
    assert f1.result == [1, 2]
    assert f2.result == [3, 4]

