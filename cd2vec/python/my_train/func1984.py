@pytest.mark.parametrize('field, oprt, value, out', [
		(Field('f'), '__add__', 1, '"f"+1'),
		(Field('f'), '__sub__', 1, '"f"-1'),
		(Field('f'), '__mul__', 1, '"f"*1'),
		(Field('f'), '__div__', 1, '"f"/1'),
		(Field('f'), '__mod__', 1, '"f"%1'),
	])
	def testOprt(self, field, oprt, value, out):
		assert str(getattr(field, oprt)(value)) == out
