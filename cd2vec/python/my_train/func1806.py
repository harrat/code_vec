def test_checkpath_recursion(run_reframe):
    _, stdout, _ = run_reframe(action='list', checkpath=[])
    num_checks_default = re.search(r'Found (\d+) check', stdout).group(1)

    _, stdout, _ = run_reframe(action='list',
                               checkpath=['checks/'],
                               more_options=['-R'])
    num_checks_in_checkdir = re.search(r'Found (\d+) check', stdout).group(1)
    assert num_checks_in_checkdir == num_checks_default

    _, stdout, _ = run_reframe(action='list',
                               checkpath=['checks/'],
                               more_options=[])
    num_checks_in_checkdir = re.search(r'Found (\d+) check', stdout).group(1)
    assert num_checks_in_checkdir == '0'

