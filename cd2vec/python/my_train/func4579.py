def test_execute_arg_string():
    global cu
    cu.execute("insert into tests(id, name, text_field) values (?, ?, ?)",
               (4, 'test_execute_arg_string', 'Hugo'))
    cu.execute("select text_field from tests where id = ?", (4,))
    row = cu.fetchone()
    assert row[0] == 'Hugo'

