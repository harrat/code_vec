@freeze_time('2014-01-20')
def test_status_doesnt_show_pushed_entries(cli, config, entries_file):
    config.set_dict({
        'test_aliases': {'alias_1': '123/456', 'alias_2': '456/789'}
    })
    entries_file.write("""20/01/2014
= alias_1 0800-0900 Play ping-pong
alias_2 1200-1300 Play ping-pong
""")

    stdout = cli('status')

    assert 'alias_1' not in stdout

