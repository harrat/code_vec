def test_mkdir_p_exists(tmpdir):
    """Test mkdir_p on creating a directory that already exists."""
    path = tmpdir.join('newdir').ensure(dir=True)
    assert path.exists()
    assert path.isdir()
    mkdir_p(path.strpath)  # should not throw an exception

