def test_recovery(self):
        alias = utils.generate_alias(type=Alias.DOMAIN)
        member = self.client.create_member(alias)
        member.use_default_recovery_rule()
        verification_id = self.client.begin_recovery(alias)
        recovered = self.client.complete_recovery_with_default_rule(member.member_id, verification_id, 'code')
        assert member.member_id == recovered.member_id
        assert len(recovered.get_keys()) == 3
        assert len(recovered.get_aliases()) == 0
        assert not self.client.is_alias_exists(alias)

        recovered.verify_alias(verification_id, 'code')
        assert self.client.is_alias_exists(alias)
        received_aliases = utils.repeated_composite_container_to_list(recovered.get_aliases())
        expected_aliases = [alias]
        assert received_aliases == expected_aliases
