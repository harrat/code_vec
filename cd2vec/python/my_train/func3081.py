def test_label_fractions_serialization(valid_label_fractions_predictor_data):
    """Ensure that a serialized LabelFractionPredictor looks sane."""
    predictor = LabelFractionsPredictor.build(valid_label_fractions_predictor_data)
    serialized = predictor.dump()
    serialized['id'] = valid_label_fractions_predictor_data['id']
    assert serialized == valid_serialization_output(valid_label_fractions_predictor_data)

