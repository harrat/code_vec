def test_host_delmacro(self, host_load_data):
        host = host_load_data
        with open(resource_dir / 'test_host_macro.json') as m:
            macro = HostMacro(json.load(m))
        data = dict()
        data['action'] = 'delmacro'
        data['object'] = 'HOST'
        data['values'] = [host.name, 'NRPEPORT']

        with patch('requests.post') as patched_post:
            host.deletemacro(macro)
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
