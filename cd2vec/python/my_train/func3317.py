def test_nbless_one_cell(tmp_path: Path) -> None:
    """Run ``nbless`` to create and execute three notebook files."""
    for tempfile in make_files(tmp_path):
        assert nbless([tempfile]).cells[0].source == Path(tempfile).read_text()

