def test_Safe_classmethod_fail():
    result_log4 = Safely.safely(math.log)(-1)
    assert np.isnan(result_log4['result']), 'Must result is True'
    assert result_log4['error'] is not None, 'Must result is True'

