def test_monkeypatch_no_terminal(testdir):
    """Don't crash without 'terminal' plugin.
    """
    testdir.makepyfile(
        """
        def test_foo(mocker):
            stub = mocker.stub()
            stub(1, greet='hello')
            stub.assert_called_once_with(1, greet='hey')
        """
    )
    result = runpytest_subprocess(testdir, "-p", "no:terminal", "-s")
    assert result.ret == 1
    assert result.stdout.lines == []

