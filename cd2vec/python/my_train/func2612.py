def test_write_cdt_with_proba(self, caplog, tmp_dir):
        res = wrapper.Output(target_root_name)
        res.extract_results()
        res.aggregate_input_data()
        res.write_cdt(with_proba=True)
        assert os.path.isfile(res.root_out_name + "_withproba.cdt")
        ref_file = target_root_path + "_out_withproba.cdt"
        assert filecmp.cmp(ref_file,
                           res.root_out_name + "_withproba.cdt",
                           shallow=False)
