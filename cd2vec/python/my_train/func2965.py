def test_getiddgroupdict():
    """py.test for IDF.getiddgroupdict()"""
    data = (({None: ["Lead Input", "Simulation Data"]},),)  # gdict,
    for (gdict,) in data:
        fhandle = StringIO("")
        idf = IDF(fhandle)
        result = idf.getiddgroupdict()
        assert result[None] == gdict[None]

