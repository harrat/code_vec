def test_get_url(self):
    """test_get_url ensures url is being retrieved correctly"""
    self.assertEqual(self.func.get_url(), "http://blahblah")
