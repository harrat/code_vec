def test_set_multiples_scopes(self):
        self.client.set_scopes(('.*stackoverflow.*', '.*github.*'))

        self._make_request('https://stackoverflow.com')
        last_request = self.client.get_last_request()
        self.assertEqual('https://stackoverflow.com/', last_request['url'])
        self.assertEqual('stackoverflow.com', last_request['headers']['Host'])

        self._make_request('https://github.com')
        last_request = self.client.get_last_request()
        self.assertEqual('https://github.com/', last_request['url'])
        self.assertEqual('github.com', last_request['headers']['Host'])

        self._make_request('https://google.com')
        last_request = self.client.get_last_request()
        self.assertNotEqual('https://google.com/', last_request['url'])
        self.assertNotEqual('google.com', last_request['headers']['Host'])
