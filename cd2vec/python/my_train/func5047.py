def test_bar():
    bar = Bar()

    assert '[          ]' == bar.bar(12, 0)
    assert '[          ]' == bar.bar(12, 9)
    assert '[#         ]' == bar.bar(12, 10)
    assert '[#         ]' == bar.bar(12, 15)
    assert '[##        ]' == bar.bar(12, 20)
    assert '[#####     ]' == bar.bar(12, 50)
    assert '[######### ]' == bar.bar(12, 99)
    assert '[##########]' == bar.bar(12, 100)

