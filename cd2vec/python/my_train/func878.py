def test_text_with_thickness(self):
        text = Text.parse('(fp_text user text (at 0.0 0.0) (layer F.SilkS) '
                          '(effects (font (size 0.1 0.1) (thickness 0.2))))')
        assert text.size == [0.1, 0.1]
        assert text.thickness == 0.2
        assert Text.parse(text.to_string()) == text
