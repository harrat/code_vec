def test_custom_parser_implicit(self):
        from serpextract.serpextract import _get_search_engines, _engines
        self.assertInvalidSERP(self.custom_serp_url)
        add_custom_parser(u'search.piccshare.com', self.custom_parser)
        self.assertValidSERP(self.custom_serp_url,
                             self.custom_parser.engine_name,
                             u'test')
        del _engines[u'search.piccshare.com']
