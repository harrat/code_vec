@given(lists(tuples(*strat)))
def test_sort_property(e):
    strings = gen_test_data(e)
    backup = strings[:]
    random.shuffle(strings)
    assert backup == sort(strings)

