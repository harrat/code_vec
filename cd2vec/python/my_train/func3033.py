def test_curl(tempname):
    n = 3
    curl(path=tempname, url=make_url(2012), max_chunk=n)
    assert Path(tempname).stat().st_size == n * 1024

