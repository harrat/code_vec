@staticmethod
    @pytest.mark.usefixtures('import-beats', 'update-beats-utc')
    def test_api_create_solution_004(server):
        """Update solution with POST that maps to PATCH.

        Send POST /solutions/{id} to update existing solution with the
        ``X-HTTP-Method-Override`` header that overrides the operation as
        PATCH.

        The UUID must not be changed when the resource is updated because it
        is immutable resource identity used in resource URI.
        """

        storage = {
            'data': [
                Content.deepcopy(Solution.BEATS)
            ]
        }
        storage['data'][0]['data'] = Solution.NGINX['data']
        storage['data'][0]['uuid'] = Solution.BEATS_UUID
        storage['data'][0]['digest'] = '038441c764ac6c4386fcb5a3bdf9b0521e4c0c0535ff5b445d022a83b7419c9a'
        request_body = {
            'data': {
                'type': 'solution',
                'attributes': {
                    'data': storage['data'][0]['data'],
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '2847'
        }
        expect_body = {
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/solutions/' + Solution.BEATS_UUID
            },
            'data': {
                'type': 'solution',
                'id': storage['data'][0]['uuid'],
                'attributes': storage['data'][0]
            }
        }
        result = testing.TestClient(server.server.api).simulate_post(
            path='/api/snippy/rest/solutions/4346ba4c79247430',
            headers={'accept': 'application/vnd.api+json; charset=UTF-8', 'X-HTTP-Method-Override': 'PATCH'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
