@staticmethod
    @pytest.mark.usefixtures('snippy', 'create-kafka-mkdn-utc')
    def test_cli_create_solution_007(snippy, editor_data):
        """Create solution from editor.

        Create a new solution by using the prefilled default Markdown template
        in editor. The template presented in editor is manually defined in this
        test case on purpose. This tries to verity that the testing framework
        does not hide problems compared to situation where the template would
        be generated automatically by the testing framework.

        When content is created, the timestamp is allocated once for created
        and updated timestamps. The timestamp must not be updated from what
        is presented in the editor.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.KAFKA_MKDN)
            ]
        }
        template = (
            '# Add brief title for content @groups',
            '',
            '> Add a description that defines the content in one chapter.',
            '',
            '> ',
            '',
            '## Description',
            '',
            '## References',
            '',
            '## Commands',
            '',
            '## Configurations',
            '',
            '## Solutions',
            '',
            '## Whiteboard',
            '',
            '## Meta',
            '',
            '> category  : solution  ',
            'created   : 2019-01-04T10:54:49.265512+00:00  ',
            'digest    : 5facdc16dc81851c2f65b112a0921eb2f2db206c7756714efb45ba0026471f11  ',
            'filename  : example-content.md  ',
            'languages : example-language  ',
            'name      : example content handle  ',
            'source    : https://www.example.com/source.md  ',
            'tags      : example,tags  ',
            'updated   : 2019-01-04T10:54:49.265512+00:00  ',
            'uuid      : a1cd5827-b6ef-4067-b5ac-3ceac07dde9f  ',
            'versions  : example=3.9.0,python>=3  ',
            ''
        )
        edited = (
            '# Testing docker log drivers @docker',
            '',
            '> Investigate docker log drivers and the logs2kafka log plugin',
            '',
            '>',
            '',
            '## Description',
            '',
            'Investigate docker log drivers.',
            '',
            '## Solutions',
            '',
            '## Whiteboard',
            '',
            '## Meta',
            '',
            '> category  : solution  ',
            'created   : 2019-01-04T10:54:49.265512+00:00  ',
            'digest    : 18473ec207798670c302fb711a40df6555e8973e26481e4cd6b2ed205f5e633c  ',
            'filename  : kubernetes-docker-log-driver-kafka.mkdn  ',
            'languages :  ',
            'name      :  ',
            'source    :  ',
            'tags      : docker,driver,kafka,kubernetes,logging,logs2kafka,moby,plugin  ',
            'updated   : 2019-01-04T10:54:49.265512+00:00  ',
            'uuid      : a1cd5827-b6ef-4067-b5ac-3ceac07dde9f  ',
            'versions  :  ',
            '')
        content['data'][0]['data'] = (
            '## Description',
            '',
            'Investigate docker log drivers.',
            '',
            '## Solutions',
            '',
            '## Whiteboard',
            ''
        )
        content['data'][0]['description'] = 'Investigate docker log drivers and the logs2kafka log plugin'
        content['data'][0]['links'] = ()
        content['data'][0]['updated'] = '2019-01-04T10:54:49.265512+00:00'
        content['data'][0]['uuid'] = Content.UUID1
        content['data'][0]['digest'] = '7941851522a23d3651f223b6d69441f77649ccb7ae1e72c6709890f2caf6401a'
        editor_data.return_value = '\n'.join(edited)
        cause = snippy.run(['snippy', 'create', '--scat', 'solution'])
        editor_data.assert_called_with('\n'.join(template))
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
