@staticmethod
    @pytest.mark.usefixtures('import-beats', 'caller')
    def test_api_update_solution_004(server):
        """Try to update solution with malformed request.

        Try to send PUT /solutions/{id} to update resource with malformed
        JSON request.
        """

        storage = {
            'data': [
                Storage.ebeats
            ]
        }
        request_body = {
            'data': Const.NEWLINE.join(Request.dnginx['data']),
            'brief': Request.dnginx['brief'],
            'groups': Request.dnginx['groups'],
            'tags': Const.DELIMITER_TAGS.join(Request.dnginx['tags']),
            'links': Const.DELIMITER_LINKS.join(Request.dnginx['links'])
        }
        expect_headers_p3 = {'content-type': 'application/vnd.api+json; charset=UTF-8', 'content-length': '5090'}
        expect_headers_p2 = {'content-type': 'application/vnd.api+json; charset=UTF-8', 'content-length': '4975'}
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '400',
                'statusString': '400 Bad Request',
                'module': 'snippy.testing.testing:123',
                'title': 'json media validation failed'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_put(
            path='/api/snippy/rest/solutions/4346ba4c79247430',
            headers={'accept': 'application/json'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_400
        assert result.headers in (expect_headers_p2, expect_headers_p3)
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
