def test_source_relationship(self):
        relationship = copy.deepcopy(self.valid_relationship)
        relationship['source_ref'] = "relationship--31b940d4-6f7f-459a-80ea-9c1f17b5891b"
        results = validate_parsed_json(relationship, self.options)
        self.assertEqual(results.is_valid, False)
        self.assertEqual(len(results.errors), 1)
