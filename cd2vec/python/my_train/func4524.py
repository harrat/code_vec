def test_edit_with_unsupported_date_format(cli, config, data_dir):
    config.set('taxi', 'file', os.path.join(data_dir, '/tmp/%Y_%f.txt'))
    with pytest.raises(click.ClickException):
        cli('edit')
