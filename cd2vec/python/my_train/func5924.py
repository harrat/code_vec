@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_update_reference_007(snippy, edited_gitlog):
        """Update reference with ``uuid`` option.

        Update reference based on uuid. The content must be updated so that
        only links get updated.
        """

        content = {
            'data': [
                Content.deepcopy(Reference.GITLOG),
                Reference.REGEXP
            ]
        }
        content['data'][0]['links'] = ('https://docs.docker.com', )
        content['data'][0]['digest'] = '1fc34e79a4d2bac51a039b7265da464ad787da41574c3d6651dc6a128d4c7c10'
        edited_gitlog.return_value = Content.dump_text(content['data'][0])
        cause = snippy.run(['snippy', 'update', '--scat', 'reference', '-u', '31cd5827-b6ef-4067-b5ac-3ceac07dde9f', '--format', 'text'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
