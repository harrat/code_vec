def test_host_setmacro(self, host_load_data):
        host = host_load_data
        data = dict()
        data['action'] = 'setmacro'
        data['object'] = 'HOST'
        data['values'] = [host.name, 'MACRO_TEST', 'VALUE_TEST', '0', 'DESC']

        with patch('requests.post') as patched_post:
            host.setmacro('MACRO_TEST', 'VALUE_TEST', '0', 'DESC')
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
