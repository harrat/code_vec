def test_ls_star(capsys, interp):
    interp.do_ls('*')
    out, err = capsys.readouterr()
    assert out == """\
 Group2/
    field1
Group1/
    Subgroup1/ field1
./
    \n"""
