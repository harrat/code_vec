def test_build_config(self):

        #: :type port: xenavalkyrie.xena_port.XenaPort
        port = self.xm.session.reserve_ports([self.port1], force=False, reset=True)[self.port1]

        assert(XenaStream.next_tpld_id == 0)
        assert(len(port.streams) == 0)
        assert(port.get_attribute('ps_indices') == '')

        stream = port.add_stream('first stream')
        assert(stream.get_attribute('ps_comment') == 'first stream')
        assert(stream.get_attribute('ps_tpldid') == '0')
        assert(XenaStream.next_tpld_id == 1)
        assert(len(port.streams) == 1)

        stream = port.add_stream(tpld_id=7)
        assert(stream.get_attribute('ps_tpldid') == '7')
        assert(XenaStream.next_tpld_id == 8)
        assert(len(port.streams) == 2)

        if self.api == ApiType.rest:
            return

        match = port.add_match()
        # Order matters
        match.set_attributes(pm_protocol='ETHERNET VLAN')
        match.set_attributes(pm_position=14)
        match.set_attributes(pm_match='0x0FFF000000000000 0x0064000000000000')
        assert(len(port.matches) == 1)

        filter = port.add_filter(comment='New Filter')
        filter.set_attributes(pf_condition='0 0 0 0 1 0')
        filter.set_state(XenaFilterState.on)
        assert filter.get_attribute('pf_comment') == 'New Filter'
        assert(len(port.filters) == 1)

        length = port.add_length()
        assert(len(port.lengthes) == 1)

        port.remove_length(0)
        assert(len(port.lengthes) == 0)
        port.remove_filter(0)
        assert(len(port.filters) == 0)
        port.remove_match(0)
        assert(len(port.matches) == 0)

        port.remove_stream(0)
        assert(len(port.streams) == 1)
        assert(port.streams.get(1))
        assert(port.get_attribute('ps_indices').split()[0] == '1')

        port.save_config(path.join(path.dirname(__file__), 'configs', 'save_config.xpc'))
