def test_get_requests_single(self):
        self._make_request('https://www.python.org/')

        requests = self.client.get_requests()

        self.assertEqual(len(requests), 1)
        request = requests[0]
        self.assertEqual('GET', request['method'])
        self.assertEqual('https://www.python.org/', request['url'])
        self.assertEqual('identity', request['headers']['Accept-Encoding'])
        self.assertEqual(200, request['response']['status_code'])
        self.assertEqual( 'text/html; charset=utf-8', request['response']['headers']['Content-Type'])
