def test_defined_rounded():
    progress_bar = ProgressBarBits(1999)

    assert '  0% (0.00/2.00 kb) [                ] eta --:-- /' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 1998
    assert ' 99% (1.99/2.00 kb) [############### ] eta --:-- -' == str(progress_bar)

    eta._NOW = lambda: 1411868724.5
    progress_bar.numerator = 1999
    assert '100% (2.00/2.00 kb) [################] eta --:-- \\' == str(progress_bar)
    assert '100% (2.00/2.00 kb) [################] eta --:-- |' == str(progress_bar)

