def test_spelling_id_multilpe_pwl():
    """
    Test spelling on source messages (English) of gettext files
    using multiple personal word lists.
    """
    po_check = PoCheck()
    pwl_files = [
        local_path('pwl1.txt'),
        local_path('pwl2.txt'),
    ]
    po_check.set_spelling_options('id', None, pwl_files)
    result = po_check.check_files([local_path('fr_spelling_id.po')])

    # be sure we have 1 file in result
    assert len(result) == 1

    # the file has 2 spelling errors: "Thsi" and "errro"
    errors = result[0][1]
    assert len(errors) == 2
    for i, word in enumerate(('Thsi', 'errro')):
        assert errors[i].idmsg == 'spelling-id'
        assert isinstance(errors[i].message, list)
        assert len(errors[i].message) == 1
        assert errors[i].message[0] == word

