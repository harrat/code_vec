def test_job_parameter_set(all_parameters):
    """Map Parameter objects to their parameter names in job-creation functions"""
    mapped = JobParameterSet(all_parameters).as_kwargs()
    assert mapped["anon_name"] == "patientName0"

    # sending an unknown parameter will raise an exception
    class UnknownIdentifier(SourceIdentifier):
        pass

    with pytest.raises(ParameterMappingException):
        JobParameterSet(
            all_parameters + [SourceIdentifierParameter(str(UnknownIdentifier(None)))]
        ).as_kwargs()

    class UnknownParameter(Parameter):
        pass

    with pytest.raises(ParameterMappingException):
        JobParameterSet(all_parameters + [UnknownParameter()]).as_kwargs()
