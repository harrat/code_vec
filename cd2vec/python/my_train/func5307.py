def test_progress(self):
        progress = MockProgress()
        xphyle.configure(progress=True, progress_wrapper=progress)
        path = self.root.make_file()
        with open(path, 'wt') as o:
            for i in range(100):
                o.write(random_text())
        compress_file(
            path, compression='gz', use_system=False)
        assert 100 == progress.count
