def test_commands_parse(capsys):
	commands = Commands()
	commands._inherit = False
	with pytest.raises(SystemExit):
		commands._parse()
	assert 'help' in capsys.readouterr().err

	with pytest.raises(SystemExit):
		commands._parse(['help'])
	assert 'help' in capsys.readouterr().err

	with pytest.raises(SystemExit):
		commands._parse(['1'])
	assert 'No command given.' in capsys.readouterr().err

	commands.cmd1            = 'First command'
	commands.cmd1._usage     = '{prog} options'
	commands.cmd2            = commands.cmd1
	commands.cmd2.a.required = True
	commands._.a.required    = True
	with pytest.raises(SystemExit):
		commands._parse(['cmd2'])
	assert "Option '-a' is required." in capsys.readouterr().err

	with pytest.raises(SystemExit):
		commands._parse(['-a', 'cmd2', '0'])
	err = capsys.readouterr().err
	assert "Option '-a' is required." in err
	assert "program cmd1|cmd2" in err

	command, opts, gopts = commands._parse(['-a', '2', 'cmd1', '-a', '1'])
	assert command == 'cmd1'
	assert opts == {'h': False, 'help': False, 'H': False, 'a': 1}
	assert gopts == {'h': False, 'help': False, 'H': False, 'a': 2}

def test_commands_parse_inherit():
	commands = Commands()
	commands._._prefix = '-'
	commands._.a = 1
	commands.list = 'List commands'
	with pytest.raises(ValueError): # inconsistent prefix
		commands._parse()
	commands._._prefix = 'auto'
	commands.list.a = True
	with pytest.raises(ParamNameError): # can't share option
		commands._parse()
	del commands.list._params['a']
