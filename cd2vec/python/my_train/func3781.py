@pytest.mark.unit
def test_build_conditional_css():
    """test cartographer.build_conditional_css"""

    helpers.setup()

    actual_css = c.build_conditional_css(helpers.TEST_PATH)

    expected_css = "\n".join(
        [
            "   <link rel='stylesheet' href='https://unpkg.com/leaflet-search@2.9.8/dist/leaflet-search.src.css'/>",
            "   <link rel='stylesheet' href='css/MarkerCluster.Default.css'/>",
            "   <link rel='stylesheet' href='css/MarkerCluster.css'/>",
        ]
    )

    helpers.tear_down()

    assert expected_css == actual_css

