def test_getting_iaga_format_minute_data_from_wdc(tmpdir):  # pylint: disable=invalid-name
    """
    smoke test WRT 'known good' data
    - download a bunch of Eskdalemuir minutely averages
    - in IAGA2002 format
    - extract them to a temp dir
    - compare each downloaded files against known-good examples
    """
    cadence = 'minute'
    station = 'ESK'
    service = 'WDC'
    year = 2015
    start_date = date(year, 1, 15)
    end_date = date(year, 12, 1)
    configpath = os.path.join(DATAPATH, 'wdc_minute_data_iaga2002output.ini')
    file_pattern = station.lower() + str(year) + '*dmin.min'

    tmppath = str(tmpdir)  # pytest 'magic' for a temp folder
    # truth file names and full paths
    oraclenames, oraclefiles = zip(*[
            (os.path.basename(file_), file_) for file_ in
            glob.glob(os.path.join(ORACLEPATH, file_pattern))
            ])

    config = cws.ParsedConfigFile(configpath, service)
    form_data = cws.FormData(config)
    with pytest.raises(ValueError):
        form_data.as_dict()
    form_data.set_datasets(start_date, end_date, station, cadence, service)
    req_iaga = cws.DataRequest()
    req_iaga.read_attributes(config)
    assert req_iaga.can_send is False
    assert req_iaga.form_data == {}
    req_iaga.set_form_data(form_data.as_dict())
    assert req_iaga.can_send is True

    resp_iaga = rq.post(
        req_iaga.url, data=req_iaga.form_data, headers=req_iaga.headers
    )
    with zipfile.ZipFile(BytesIO(resp_iaga.content)) as fzip:
        fzip.extractall(tmppath)

    # check lists of file names are same only (content comparison broken
    # by line endings)
    _, _, errs = filecmp.cmpfiles(tmppath, ORACLEPATH,
                                  oraclenames, shallow=False)
    assert errs == [], (
        "could not compare {} to expected, ".format(errs) +
        "perhaps we didn't download them"
    )

    # full paths to dowloaded files
    gotfiles = [os.path.join(tmppath, file_) for file_ in oraclenames]
    # custom file content comparison function due to line-ending vagueries
    [assert_all_lines_same(file1_, file2_) for file1_, file2_ in
        zip(gotfiles, oraclefiles)]
