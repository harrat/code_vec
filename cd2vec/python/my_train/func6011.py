def test_output(self):
        for i in range(len(helpers.P2WSH['human']['outs'])):
            self.assertEqual(
                simple.output(
                    value=helpers.P2WSH['human']['outs'][i]['value'],
                    address=helpers.P2WSH['human']['outs'][i]['addr']),
                helpers.P2WSH['ser']['outs'][i]['output'])
