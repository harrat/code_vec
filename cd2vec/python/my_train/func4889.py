def testMkSymlink(tmpdir, monkeypatch):

    destdir = str(tmpdir.realpath())
    testfile = joinpath(destdir, 'testfile')
    with open(testfile, 'w+') as file:
        file.write("trash")

    if PLATFORM == 'windows':
        _mksymlink = getattr(os, "symlink", None)
        if callable(_mksymlink):
            try:
                _mksymlink(testfile, joinpath(destdir, 'a'))
            except OSError:
                # On windows we have no implemention or rights to make symlink
                return

    symlink = joinpath(destdir, 'symlink')
    utils.mksymlink(testfile, symlink)
    assert os.path.islink(symlink)

    with pytest.raises(OSError):
        utils.mksymlink(testfile, symlink, force = False)

    utils.mksymlink(testfile, symlink, force = True)
    assert os.path.islink(symlink)

    monkeypatch.setattr(os, "symlink", None)
    with pytest.raises(NotImplementedError):
        utils.mksymlink(testfile, symlink)
