def test_undefined_animated_resize():
    bar = BarUndefinedAnimated()

    assert '[?       ]' == bar.bar(10)
    assert '[ ?      ]' == bar.bar(10)
    assert '[  ?     ]' == bar.bar(10)
    assert '[   ?    ]' == bar.bar(10)
    assert '[    ?   ]' == bar.bar(10)
    assert '[     ?  ]' == bar.bar(10)
    assert '[ ? ]' == bar.bar(5)
    assert '[?       ]' == bar.bar(10)
    assert '[ ?      ]' == bar.bar(10)

