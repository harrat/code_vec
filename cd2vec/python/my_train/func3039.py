def test_set_profile(self):
        in_profile = Profile(display_name_first='Ming', display_name_last='Xiao')
        back_profile = self.member.set_profile(in_profile)
        out_profile = self.member.get_profile(self.member.member_id)
        assert in_profile == back_profile == out_profile
