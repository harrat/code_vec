def test_no_ignore_check_conflicts(run_reframe):
    returncode, *_ = run_reframe(
        checkpath=['unittests/resources/checks'],
        more_options=['-R'],
        ignore_check_conflicts=False,
        action='list'
    )
    assert returncode != 0

