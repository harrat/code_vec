@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_search_reference_001(snippy, capsys):
        """Search references with ``sall`` option.

        Search references from all fields. The match is made from one reference
        content data.
        """

        output = (
            '1. Python regular expression @python [cb9225a81eab8ced]',
            '',
            '   > https://www.cheatography.com/davechild/cheat-sheets/regular-expressions/',
            '   > https://pythex.org/',
            '   # howto,online,python,regexp',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--scat', 'reference', '--sall', 'regexp', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
