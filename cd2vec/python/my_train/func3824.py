@pytest.mark.vcr()
def test_workbench_export_chapters_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.export(format='html', chapters=['something'])
