def test_alt_text_image(sphinx_app):
    """Test alt text for matplotlib images in html and rst"""
    out_dir = sphinx_app.outdir
    src_dir = sphinx_app.srcdir
    # alt text is fig titles, rst
    example_rst = op.join(src_dir, 'auto_examples', 'plot_matplotlib_alt.rst')
    with codecs.open(example_rst, 'r', 'utf-8') as fid:
        rst = fid.read()
    # suptitle and axes titles
    assert ':alt: This is a sup title, subplot 1, subplot 2' in rst
    # multiple titles
    assert ':alt: Left Title, Center Title, Right Title' in rst

    # no fig title - alt text is file name, rst
    example_rst = op.join(src_dir, 'auto_examples',
                          'plot_numpy_matplotlib.rst')
    with codecs.open(example_rst, 'r', 'utf-8') as fid:
        rst = fid.read()
    assert ':alt: plot numpy matplotlib' in rst
    # html
    example_html = op.join(out_dir, 'auto_examples',
                           'plot_numpy_matplotlib.html')
    with codecs.open(example_html, 'r', 'utf-8') as fid:
        html = fid.read()
    assert 'alt="plot numpy matplotlib"' in html

