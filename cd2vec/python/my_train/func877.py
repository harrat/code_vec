def test_new_task_bad_holder(self):
        """
        Trying to create a new task from an invalid task holder may raise
        various exceptions. Ensure those exceptions are raised by `new_task`.
        """
        # Cannot create a task with `__init__` has an  invalid signature
        with self.assertRaisesRegex(TypeError, 'positional argument'):
            new_task('task-bad-inputs', config={'hello': 'world'})

        # Cannot create a task when `__init__` raises an exception
        with self.assertRaises(MyDummyError):
            new_task('task-init-exc')
