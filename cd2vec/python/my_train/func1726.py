def test_all_defaults(self, block_format_env):
        base = _IEXBase()

        assert base.output_format == "json"
        assert base.pause == 0.5
