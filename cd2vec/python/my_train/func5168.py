def test_is_allowed_policy_not_found(self):
        """
        Test that an access check using a non-existent policy is handled
        correctly.
        """
        e = engine.KmipEngine()
        e.get_relevant_policy_section = mock.Mock(return_value=None)

        result = e.is_allowed(
            'test_policy',
            'test_user',
            'test_group',
            'test_user',
            enums.ObjectType.SYMMETRIC_KEY,
            enums.Operation.GET
        )
        self.assertFalse(result)
