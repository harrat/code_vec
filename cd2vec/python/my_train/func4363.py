def test_default_plots_True_plotcallback(self):
        agent = agents.PpoAgent("CartPole-v0")
        p = plot.Loss()
        r = plot.Rewards()
        c = agent._add_plot_callbacks([r], True, [p])
        assert p in c
        assert r in c
