def test_basic_search_with_bicyle():
    from_date = "{} 08:00".format(_TOMORROW)
    to_date = "{} 12:00".format(_TOMORROW)
    departure_station = "Toulouse Matabiau"
    arrival_station = "Narbonne"

    results = trainline.search(
        departure_station=departure_station,
        arrival_station=arrival_station,
        from_date=from_date,
        to_date=to_date,
        bicycle_with_or_without_reservation=True)
    print()
    print("Search trips for {} to {}, between {} and {}".format(
        departure_station, arrival_station, from_date, to_date))
    print("{} results".format(len(results)))
    assert len(results) > 0

    display_trips(results)

