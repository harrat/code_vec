def test_object_marking_ref_invalid_type(self):
        marking_definition = copy.deepcopy(self.valid_marking_definition)
        marking_definition['object_marking_refs'] = ["indicator--44098fce-860f-48ae-8e50-ebd3cc5e41da"]
        self.assertFalseWithOptions(marking_definition)
