def test_embeddings_with_flair(self):
        from flair.data import Sentence

        embs = load_context_embeddings_with_flair()

        sentence1 = Sentence('Han fik bank')
        sentence2 = Sentence('Han fik en ny bank')

        embs.embed(sentence1)
        embs.embed(sentence2)

        # Check length of context embeddings
        self.assertEqual(len(sentence1[2].embedding), 2364)
        self.assertEqual(len(sentence2[4].embedding), 2364)
