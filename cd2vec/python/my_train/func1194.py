def test_regression_atom_feed(generate_result_dir):
    conf, repo, commits = generate_result_dir(5 * [1] + 5 * [10] + 5 * [15])
    tools.run_asv_with_conf(conf, "publish")

    commits = list(reversed(repo.get_branch_commits(None)))

    tree = etree.parse(join(conf.html_dir, "regressions.xml"))
    root = tree.getroot()

    assert root.tag == '{http://www.w3.org/2005/Atom}feed'
    entries = root.findall('{http://www.w3.org/2005/Atom}entry')

    # Check entry titles
    assert len(entries) == 2
    title = entries[0].find('{http://www.w3.org/2005/Atom}title')
    assert title.text == '900.00% time_func'
    title = entries[1].find('{http://www.w3.org/2005/Atom}title')
    assert title.text == '50.00% time_func'

    # Check there's a link of some sort to the website in the content
    content = entries[0].find('{http://www.w3.org/2005/Atom}content')
    assert ('<a href="index.html#time_func?commits=' + commits[5]) in content.text
    content = entries[1].find('{http://www.w3.org/2005/Atom}content')
    assert ('<a href="index.html#time_func?commits=' + commits[10]) in content.text

    # Smoke check ids
    id_1 = entries[0].find('{http://www.w3.org/2005/Atom}id')
    id_2 = entries[1].find('{http://www.w3.org/2005/Atom}id')
    assert id_1.text != id_2.text

