def test_rowcount_executemany2():
    global cx
    with cx.cursor() as c:
        c.execute("delete from tests")
        c.executemany("insert into tests(id, name) values (?, ?)", [
            (1, 'test_rowcount_executemany2'),
            (2, 'test_rowcount_executemany2'),
            (3, 'test_rowcount_executemany2'),
            (4, 'test_rowcount_executemany2')
        ])
        assert c.rowcount == 4
