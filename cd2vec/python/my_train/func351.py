def test_today_report_err_one(capsys):
    today_report("test")
    captured = capsys.readouterr()
    assert captured.out == "[+] Generating daily report for test...\n[-] You are in the middle of tracking, end this session with `devtracker stop` before generation a report.\n"

def test_no_dir_error(capsys):
    today_report("no_dir")
    captured = capsys.readouterr()
    assert captured.out == "[-] 'devtracker report' was entered.  You must run 'devtracker start' to create project tracking file.\n"
