def test_scan_drms_folder(
    drm_model_tax_coupon_1, drm_model_sat_coupon_1, drm_model_no_match_1
):
    """
    Unit: tests that all the drms on the given folder get properly scanned and
          translated from yml to dictionary (valid ones only).
    """
    DRM_SCANNER_TEST_YML_FOLDER = "./tests/data/drms_scanner/"

    # method invocation
    all_drms = scan_drms_folder(DRM_SCANNER_TEST_YML_FOLDER)

    expected_drm_1 = parse_yml(DRM_SCANNER_TEST_YML_FOLDER + "drm_1.yml")
    expected_drm_2 = parse_yml(DRM_SCANNER_TEST_YML_FOLDER + "drm_2.yml")

    expected_drms = [expected_drm_1, expected_drm_2]

    assert all_drms == expected_drms

