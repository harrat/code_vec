def test_generate_empty_template(pbmc_schema_path, pbmc_template, tmpdir):
    """Check that generate_empty_template generates the correct template."""
    # Generate the xlsx file with the convenience method
    target_path = tmpdir.join("pbmc_target.xlsx")
    generate_empty_template(pbmc_schema_path, target_path)

    # Generate the xlsx file from the known-good Template instance
    # for comparison
    cmp_path = tmpdir.join("pbmc_truth.xlsx")
    pbmc_template.to_excel(cmp_path)

    # Check that the files have the same contents
    with open(target_path, "rb") as generated, open(cmp_path, "rb") as comparison:
        assert generated.read() == comparison.read()
