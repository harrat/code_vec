@all_modes()
def test_ir(d, mode):
    kern = install(d, "%s --kernel=ir MOD1"%mode)
    assert kern['kernel']['language'] == 'R'
    assert kern['k'][0] == 'R'
    assert kern['k'][1:5] == ['--slave', '-e', 'IRkernel::main()', '--args']

@all_modes()
def test_imatlab(d, mode):
    kern = install(d, "%s --kernel=imatlab MOD1"%mode)
    assert kern['kernel']['language'] == 'matlab'
    assert kern['k'][0].endswith('python')
    assert kern['k'][1:4] == ['-m', 'imatlab', '-f']
