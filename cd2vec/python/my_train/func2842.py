def test_3():
    """ Test case 3. """
    case_data = CaseData('3.json')
    run(case_data, ping_seconds=constants.DEFAULT_PING_SECONDS)
    # We must clear server caches to allow to re-create a Server with same test case but different server attributes.
    Server.__cache__.clear()

def test_3_ping_1s():
    """ Test case 3 with small ping (1 second). """
    case_data = CaseData('3.json')
    run(case_data, ping_seconds=1)
    # We must clear server caches to allow to re-create a Server with same test case but different server attributes.
    Server.__cache__.clear()
