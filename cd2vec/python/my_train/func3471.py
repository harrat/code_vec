@all_modes()
def test_imatlab(d, mode):
    kern = install(d, "%s --kernel=imatlab MOD1"%mode)
    assert kern['kernel']['language'] == 'matlab'
    assert kern['k'][0].endswith('python')
    assert kern['k'][1:4] == ['-m', 'imatlab', '-f']

