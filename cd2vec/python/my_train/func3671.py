@pytest.mark.parametrize(
        "package_version, package_name, metadata",
        [("delegator.py==0.1.1", "delegator.py", _DELEGATOR_PY_METADATA), ("click==7.0", "click", _CLICK_METADATA)],
    )
    def test_get_package_metadata(self, venv, package_version, package_name, metadata):
        """Test getting package metadata."""
        venv.install(package_version)
        discovered_metadata = get_package_metadata(venv.python, package_name)
        # We don't care about files present now, just check we have some files in the output.
        assert "files" in discovered_metadata
        discovered_metadata.pop("files")
        assert discovered_metadata == metadata
