def test_convenience_clear_not_initialized():
    # This test is put near the top of this module, or at least before
    # the very first time ``init_needed()`` is called.
    dummy = get_needed()
    with pytest.raises(NotImplementedError):
        dummy.clear()
    with pytest.raises(NotImplementedError):
        clear_needed()

    # Initialize a needed resources object.
    needed = init_needed()
    assert get_needed() == needed
    assert thread_local_needed_data.__dict__[NEEDED] == needed

    # Clear it.
    del_needed()

    # It is gone, really.
    with pytest.raises(KeyError):
        thread_local_needed_data.__dict__[NEEDED]

    # Clearing it again is OK.
    del_needed()

    # get_needed still work, dummy-style.
    dummy2 = get_needed()
    assert dummy2 != needed
    with pytest.raises(NotImplementedError):
        dummy.clear()
    with pytest.raises(NotImplementedError):
        clear_needed()
