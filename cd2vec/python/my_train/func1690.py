def test_check_access_file(self):
        path = self.root.make_file(permissions='rwx')
        check_access(path, 'r')
        check_access(path, 'w')
        check_access(path, 'x')
