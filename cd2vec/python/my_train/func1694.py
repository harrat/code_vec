def testFiles(tmpdir, zipPkg):

    # 'files' without a zip
    tmpDirPath = str(tmpdir.realpath())
    aaaDir = tmpdir.mkdir("aaa")
    aaaDir.join("a1").write('111')
    aaaDir.join("a2").write('111')
    pkgPath = pypkg.PkgPath(str(aaaDir))
    assert sorted(['a1', 'a2']) == sorted(pkgPath.files())
    pkgPath = pypkg.PkgPath(joinpath(tmpDirPath, 'notexisting'))
    assert list(pkgPath.files()) == []

    # 'exists', 'isfile', 'isdir' without a zip
    assert pypkg.PkgPath(joinpath(str(aaaDir), 'a1')).exists()
    assert pypkg.PkgPath(joinpath(str(aaaDir), 'a1')).isfile()
    assert not pypkg.PkgPath(joinpath(str(aaaDir), 'a1')).isdir()
    assert not pypkg.PkgPath(joinpath(str(aaaDir), 'notexisting')).exists()
    assert not pypkg.PkgPath(joinpath(str(aaaDir), 'notexisting')).isfile()
    assert not pypkg.PkgPath(joinpath(str(aaaDir), 'notexisting')).isdir()

    # 'files' with a zip
    pkgPath = pypkg.PkgPath(MODULE_ZIP_PATH, zipPkg)
    assert list(pkgPath.files()) == ['module.py']
    pkgPath = pypkg.PkgPath(joinpath(MODULE_ZIP_PATH, 'pkg1'), zipPkg)
    assert sorted(pkgPath.files()) == sorted(['module1.py', 'module2.py'])
    pkgPath = pypkg.PkgPath(joinpath(MODULE_ZIP_PATH, 'notexisting'), zipPkg)
    assert list(pkgPath.files()) == []

    # 'exists', 'isfile', 'isdir' with a zip
    path = joinpath(MODULE_ZIP_PATH, 'pkg1', 'module1.py')
    assert pypkg.PkgPath(path, zipPkg).exists()
    assert pypkg.PkgPath(path, zipPkg).isfile()
    assert not pypkg.PkgPath(path, zipPkg).isdir()
    path = joinpath(MODULE_ZIP_PATH, 'pkg1', 'notexisting')
    assert not pypkg.PkgPath(path, zipPkg).exists()
    assert not pypkg.PkgPath(path, zipPkg).isdir()
    assert not pypkg.PkgPath(path, zipPkg).isfile()

def testOpenRead(tmpdir, zipPkg):
