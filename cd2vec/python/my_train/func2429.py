@pytest.mark.vcr()
def test_workbench_vulns_severity_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vulns(severity=['low'])
