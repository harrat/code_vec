def test_model_find_one_returns_model_instance(mocker, response2):
    mocker.patch.object(Campaign.client, 'get')
    Campaign.client.get.return_value = response2
    assert isinstance(Campaign.find_one(), Campaign)

