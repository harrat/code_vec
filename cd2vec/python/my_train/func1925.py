def test_get_post_bundles(self, dcard, metas):
        ids = [m['id'] for m in metas]
        posts1 = dcard.posts(metas).get(comments=False, links=False).result()
        posts2 = dcard.posts(ids).get(comments=False, links=False).result()
        titles = [post['title'] for post in posts1]
        assert len(posts1) == len(posts2)
        assert all(titles)
