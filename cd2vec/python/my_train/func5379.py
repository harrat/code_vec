def test_multiple_hooks(local_exec_ctx):
    @fixtures.custom_prefix('unittests/resources/checks')
    class MyTest(HelloTest):
        def __init__(self):
            super().__init__()
            self.name = type(self).__name__
            self.executable = os.path.join('.', self.name)
            self.var = 0

        @rfm.run_after('setup')
        def x(self):
            self.var += 1

        @rfm.run_after('setup')
        def y(self):
            self.var += 1

        @rfm.run_after('setup')
        def z(self):
            self.var += 1

    test = MyTest()
    _run(test, *local_exec_ctx)
    assert test.var == 3

