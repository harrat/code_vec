@mock.patch("IPython.display.display")
def test_reglue_display(mock_display, notebook_result):
    notebook_result.reglue("output")
    mock_display.assert_called_once_with(
        {"text/plain": "'Hello World!'"},
        metadata={"scrapbook": {"name": "output", "data": False, "display": True}},
        raw=True,
    )
