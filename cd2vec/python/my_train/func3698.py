@pytest.mark.vcr()
def test_watershed(shed):
    """Verify that the JSON response contains expected keys."""
    keys = shed.data.keys()
    assert 'workspaceID' in keys
    assert 'featurecollection' in keys
    assert 'parameters' in keys
    assert 'messages' in keys
    assert '04150305' in str(shed)
    assert str(shed.lat) in str(shed)
    assert str(shed.lon) in str(shed)

