def test_copy_file_to_file(self) -> None:
        src_path: Path = random_local_path(root=self.src_tmp_dir.name)[0]
        dst_path: Path = random_local_path(root=self.dst_tmp_dir.name)[0]
        content: bytes = randstr().encode()

        self.fs.file_write(src_path, content)

        self.sut.copy_file_to_file(CopyFile(self.fs, src_path), CopyFile(self.fs, dst_path))

        assert self.fs.file_read(dst_path) == content
