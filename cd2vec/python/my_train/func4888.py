def test_attributes_transmission_if_aggregated_has_no_host(aggregated):
    """Test that attributes transmission return None if Aggregator
    has an empty node list."""
    aggregated._aggregator._redis_nodes = []
    assert aggregated._redis_client is None

