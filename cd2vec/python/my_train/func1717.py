def test_query_1_1(self):
        """
        Test that a Query request can be processed correctly, for KMIP 1.1.
        """
        e = engine.KmipEngine()

        e._logger = mock.MagicMock()
        e._protocol_version = contents.ProtocolVersion(1, 1)

        payload = payloads.QueryRequestPayload(
            query_functions=[
                enums.QueryFunction.QUERY_OPERATIONS,
                enums.QueryFunction.QUERY_OBJECTS,
                enums.QueryFunction.QUERY_SERVER_INFORMATION,
                enums.QueryFunction.QUERY_APPLICATION_NAMESPACES,
                enums.QueryFunction.QUERY_EXTENSION_LIST,
                enums.QueryFunction.QUERY_EXTENSION_MAP
            ]
        )

        result = e._process_query(payload)

        e._logger.info.assert_called_once_with("Processing operation: Query")
        self.assertIsInstance(result, payloads.QueryResponsePayload)
        self.assertIsInstance(result.operations, list)
        self.assertEqual(13, len(result.operations))
        self.assertEqual(
            enums.Operation.CREATE,
            result.operations[0]
        )
        self.assertEqual(
            enums.Operation.CREATE_KEY_PAIR,
            result.operations[1]
        )
        self.assertEqual(
            enums.Operation.REGISTER,
            result.operations[2]
        )
        self.assertEqual(
            enums.Operation.DERIVE_KEY,
            result.operations[3]
        )
        self.assertEqual(
            enums.Operation.LOCATE,
            result.operations[4]
        )
        self.assertEqual(
            enums.Operation.GET,
            result.operations[5]
        )
        self.assertEqual(
            enums.Operation.GET_ATTRIBUTES,
            result.operations[6]
        )
        self.assertEqual(
            enums.Operation.GET_ATTRIBUTE_LIST,
            result.operations[7]
        )
        self.assertEqual(
            enums.Operation.ACTIVATE,
            result.operations[8]
        )
        self.assertEqual(
            enums.Operation.REVOKE,
            result.operations[9]
        )
        self.assertEqual(
            enums.Operation.DESTROY,
            result.operations[10]
        )
        self.assertEqual(
            enums.Operation.QUERY,
            result.operations[11]
        )
        self.assertEqual(
            enums.Operation.DISCOVER_VERSIONS,
            result.operations[12]
        )
        self.assertIsNone(result.object_types)
        self.assertIsNotNone(result.vendor_identification)
        self.assertEqual(
            "PyKMIP {0} Software Server".format(kmip.__version__),
            result.vendor_identification
        )
        self.assertIsNone(result.server_information)
        self.assertIsNone(result.application_namespaces)
        self.assertIsNone(result.extension_information)
