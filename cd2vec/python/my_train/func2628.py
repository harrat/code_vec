@mock.patch(
    "kafka_utils.kafka_corruption_check."
    "main.get_partition_leaders",
    return_value={"t0-0": 1, "t0-1": 2},
)
def test_filter_leader_files(mock_get_partition):
    filtered = main.filter_leader_files(None, [(1, "host1", ["a/kafka-logs/t0-0/0123.log",
                                                             "a/kafka-logs/t2-0/0123.log",
                                                             "a/kafka-logs/t0-1/0123.log"]),
                                               (2, "host2", ["a/kafka-logs/t0-0/0123.log",
                                                             "a/kafka-logs/t0-1/0123.log"])])
    assert filtered == [(1, 'host1', ['a/kafka-logs/t0-0/0123.log',
                                      'a/kafka-logs/t2-0/0123.log']),
                        (2, 'host2', ['a/kafka-logs/t0-1/0123.log'])]
