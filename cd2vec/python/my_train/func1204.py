def test_get_purls_from_cache_with_non_valid_object(self):
    """test_get_purls_from_cache_with_non_valid_object ensures calls to
    get_purls_and_results_from_cache with improper objects returns None for
    new_purls and results"""
    (new_purls, results) = self.func.get_purls_and_results_from_cache(
        "bad data")
    self.assertEqual(new_purls, None)
    self.assertEqual(results, None)
