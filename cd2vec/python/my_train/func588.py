def test_read_OL_implicit_little(self):
        """Check creation of OL DataElement from byte data works correctly."""
        ds = dcmread(self.fp, force=True)
        ref_elem = ds.get(0x00720075)
        elem = DataElement(
            0x00720075,
            "OL",
            b"\x00\x01\x02\x03\x04\x05\x06\x07" b"\x01\x01\x02\x03",
        )
        assert ref_elem == elem
