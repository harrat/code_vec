def test_execute_arg_string_with_zero_byte():
    global cu
    cu.execute("insert into tests(id, name, text_field) values (?, ?, ?)",
               (5, 'test_execute_arg_string_with_zero_byte', six.u("Hu\u0000go")))
    cu.execute("select text_field from tests where id = ?", (5,))
    row = cu.fetchone()
    assert row[0] == six.u("Hu\u0000go")

