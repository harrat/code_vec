def test_check_ssl_redirect_valid(self):
        self.assertEqual(
            "https://adamcaudill.com/",
            network.check_ssl_redirect("http://adamcaudill.com/"),
        )
