def test_start_time():
    with open(path, 'r') as myfile:
        reader = csv.reader(myfile)
        next(reader)
        start_info = next(reader)

    assert start_info == [new_start_date, new_start_time]

def test_end_time_total():
    time.sleep(5)
    end_time("test")
    new_end_date = str(datetime.today().strftime("%Y-%m-%d"))
    new_end_time = str(datetime.today().strftime("%H:%M:%S"))
    FMT = "%H:%M:%S"
    new_total = str(datetime.strptime(new_end_time, FMT) - datetime.strptime(new_start_time, FMT))
    with open(path, 'r') as myfile:
        reader = csv.reader(myfile)
        next(reader)
        full_entry = next(reader)
