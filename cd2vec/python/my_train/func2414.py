@pytest.mark.parametrize(
    "attr",
    [
        "geomean_conf_interval",
        "logmean_conf_interval",
        "mean_conf_interval",
        "median_conf_interval",
        "shapiro",
        "shapiro_log",
        "lilliefors",
        "lilliefors_log",
        "color",
    ],
)
def test_location_stats_arrays(location, attr):
    expected = {
        "color": {
            True: [0.32157, 0.45271, 0.66667],
            False: [0.32157, 0.45271, 0.66667],
        },
        "geomean_conf_interval": {True: [6.55572, 9.79677], False: [7.25255, 10.34346]},
        "logmean_conf_interval": {True: [1.88631, 2.27656], False: [1.97075, 2.34456]},
        "mean_conf_interval": {True: [7.74564, 11.49393], False: [8.52743, 11.97627]},
        "median_conf_interval": {True: [5.66000, 8.71000], False: [6.65000, 9.850000]},
        "shapiro": {True: [0.886889, 0.001789], False: [0.896744, 0.003236]},
        "shapiro_log": {True: [0.972679, 0.520949], False: [0.964298, 0.306435]},
        "lilliefors": {True: [0.185180, 0.003756], False: [0.160353, 0.023078]},
        "lilliefors_log": {True: [0.091855, 0.64099], False: [0.08148, 0.80351]},
    }
    nptest.assert_array_almost_equal(
        getattr(location, attr), expected[attr][location.useros], decimal=5
    )
