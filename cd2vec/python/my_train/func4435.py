@pytest.mark.parametrize("pwl_module",
                         [SlopedPWL, MonoSlopedPWL, MonoPointPWL])
@pytest.mark.parametrize("num_channels", [1, 3])
@pytest.mark.parametrize("num_breakpoints", [1, 7])
def test_pwl_default_init_response(pwl_module, num_channels, num_breakpoints):
    module = pwl_module(
        num_channels=num_channels, num_breakpoints=num_breakpoints)
    x = get_x(num_channels)
    y = module(x)
    # Should initialize to y = x by default.
    expected_y = x
    assert torch.max(torch.abs(y - expected_y)) < TOLERANCE

