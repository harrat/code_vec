def test_recover_failures(self):
        processor = BatchProcessor(
            'auth', 'secret', '12345', batch_max_length=1
        )

        holding = PanamahHolding(id='1234', descricao='teste')
        batch = Batch()
        batch_filename = batch.filename
        batch.append(Update.from_model(holding))
        batch.save(directory=ACCUMULATED_PATH)

        loaded_batch = Batch(filename='%s/%s' %
                             (ACCUMULATED_PATH, batch_filename))
        mock_response = {
            'falhas': {
                'total': 1,
                'itens': loaded_batch.operations
            }
        }
        processor.recover_from_failures(loaded_batch, mock_response)
        self.assertTrue(bool(next((file for file in os.listdir(
            ACCUMULATED_PATH) if file.startswith('0_')), False)))
