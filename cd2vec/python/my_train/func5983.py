def test_also_known_as_attr(person: Person):
    assert isinstance(person.also_known_as, list)
    assert all(isinstance(item, str) for item in person.also_known_as)

