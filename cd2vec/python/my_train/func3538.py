@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_passing_files_between_jobs(fixture, request):
    engine = request.getfixturevalue(fixture)

    job1 = engine.launch(image='alpine', command='echo hello > world')
    print('job1:', job1.rundata)
    job1.wait()
    assert job1.exitcode == 0

    job2 = engine.launch(image='alpine', command='cat helloworld',
                         inputs={'helloworld': job1.get_output('world')})
    print('job2:', job2.rundata)
    job2.wait()
    assert job2.exitcode == 0
    assert job2.stdout.strip() == 'hello'

