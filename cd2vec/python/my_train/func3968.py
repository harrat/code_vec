@pytest.mark.vcr()
def test_workbench_vuln_assets(api):
    assets = api.workbenches.vuln_assets()
    assert isinstance(assets, list)
    a = assets[0]
    check(a, 'agent_name', list)
    check(a, 'fqdn', list)
    check(a, 'id', 'uuid')
    check(a, 'ipv4', list)
    check(a, 'ipv6', list)
    check(a, 'last_seen', 'datetime')
    check(a, 'netbios_name', list)
    check(a, 'severities', list)
    for i in a['severities']:
        check(i, 'count', int)
        check(i, 'level', int)
        check(i, 'name', str)
    check(a, 'total', int)

@pytest.mark.vcr()
def test_workbench_export_asset_uuid_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.export(asset_uuid=123)
