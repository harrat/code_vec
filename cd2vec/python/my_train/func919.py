def test_make_p2wsh_output_script(self):
        self.assertEqual(
            tb.make_p2wsh_output_script(
                helpers.P2WSH['human']['witnesses'][0]['wit_script']),
            helpers.P2WSH['ser']['ins'][0]['pk_script'])
