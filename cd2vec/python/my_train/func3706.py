@autodoc.describe('GET /')
    def test_get(self):
        """ GET / """
        res = self.client.get('/')
        self.assertEqual(res.status_code, 200)

        return res
