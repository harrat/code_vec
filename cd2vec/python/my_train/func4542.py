def test_train_emptyArgs(self):
        agent = BackendAgentTest.DebugAgent()
        agent.train(train_context=self.tc, callbacks=[])
        assert self.tc.training_done
        assert self.tc.iterations_done_in_training == 1
        assert self.tc.episodes_done_in_iteration == 1
        assert self.tc.episodes_done_in_training == 1
