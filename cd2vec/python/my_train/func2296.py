def test_constructor(loadbalanced):
    """Test that a constructor with app instance will initialize the
    connection."""
    assert loadbalanced._redis_client is not None
    assert hasattr(loadbalanced._redis_client, 'connection_pool')
    assert hasattr(loadbalanced._app, 'extensions')
    assert 'redis' in loadbalanced._app.extensions
    assert loadbalanced._app.extensions['redis'] is loadbalanced

