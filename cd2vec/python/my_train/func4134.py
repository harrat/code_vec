def test_downloading_image(self):
        with open('tests/testdata/imgload.png', 'rb') as img_file:
            sample_img_bytes = img_file.read()
        my_image_loader = ImgLoader()

        dl_img_bytes = my_image_loader.get_image_as_bytes('https://imgur.com/a/jpwwwq6')
        self.assertEqual(sample_img_bytes, dl_img_bytes)
