def test_get_metas_with_num_param(self, dcard):
        forum = dcard.forums(_forum_name)
        assert len(forum.get_metas()) == 30
        assert len(forum.get_metas(num=0)) == 0
        assert len(forum.get_metas(num=90)) == 90
        assert len(forum.get_metas(num=33)) == 33
        assert len(forum.get_metas(num=87)) == 87
