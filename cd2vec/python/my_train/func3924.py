@mock.patch('requests.head', side_effect=mock_request)
    @mock.patch('requests.get', side_effect=mock_pyquery)
    def test_mlb_invalid_default_year_reverts_to_previous_year(self,
                                                               *args,
                                                               **kwargs):
        flexmock(utils) \
            .should_receive('_find_year_for_season') \
            .and_return(2018)

        roster = Roster('HOU')

        assert len(roster.players) == 3

        for player in roster.players:
            assert player.name in [u'Jos� Altuve', 'Justin Verlander',
                                   'Charlie Morton']
