def test_formatted_output(caplog, capsys):
    log.set_default_format()
    log.debug("debug")
    assert m(caplog) == "debug"
    out, err = capsys.readouterr()
    assert out.endswith("debug\n")
    assert "DEBUG [tests.test_logx.test_formatted_output:" in out
    assert not err

