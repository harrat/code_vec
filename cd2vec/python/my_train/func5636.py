def test_read_SV_implicit_little(self):
        """Check reading element with VR of SV encoded as implicit"""
        ds = dcmread(self.fp, force=True)
        elem = ds["SVElementMinimum"]
        assert "SV" == elem.VR
        assert 0xFFFE0001 == elem.tag
        assert -(2 ** 63) == elem.value

        new = DataElement(0xFFFE0001, "SV", -(2 ** 63))
        assert elem == new

        elem = ds["SVElementMaximum"]
        assert "SV" == elem.VR
        assert 0xFFFE0002 == elem.tag
        assert 2 ** 63 - 1 == elem.value

        new = DataElement(0xFFFE0002, "SV", 2 ** 63 - 1)
        assert elem == new
