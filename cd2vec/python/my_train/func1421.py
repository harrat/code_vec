def test_match_with_hpo(ontclient):
    assert ontclient.find_term('Jaundice') == 'http://purl.obolibrary.org/obo/HP_0000952'

def test_exists_in_zooma_but_not_included_in_ot(ontclient):
    #Zooma finds a mapping, but the _is_included could fail
    assert ontclient.find_term('Failure to thrive') == 'http://purl.obolibrary.org/obo/HP_0001508'
