@redirect_io
    def test_help_extra_params(self):
        """ Ensure appropriate error is thrown when an extra argument is used. """

        # work around an argparse behavior where output is not printed and SystemExit
        # is not raised on Python 2.7.9
        if sys.version_info < (2, 7, 10):
            try:
                self.cli_ctx.invoke('n1 -a 1 -b c -c extra'.split())
            except SystemExit:
                pass
        else:
            with self.assertRaises(SystemExit):
                self.cli_ctx.invoke('n1 -a 1 -b c -c extra'.split())

        actual = io.getvalue()
        expected = 'unrecognized arguments: -c extra'
        self.assertIn(expected, actual)
