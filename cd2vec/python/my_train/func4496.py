@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_job_status(fixture, request):
    engine = request.getfixturevalue(fixture)
    job = engine.launch('alpine', 'sleep 3', submit=False)
    assert job.status.lower() == 'unsubmitted'
    job.submit()
    print(job.rundata)
    assert job.status.lower() in ('queued', 'running', 'downloading')
    assert not job.stopped
    job.wait()
    assert job.status.lower() == 'finished'
    assert job.stopped

