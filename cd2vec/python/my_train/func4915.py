def test_valid_capec_id(self):
        attack_pattern = copy.deepcopy(self.valid_attack_pattern)
        ext_refs = attack_pattern['external_references']
        ext_refs[0]['external_id'] = "CAPEC-abc"
        results = validate_parsed_json(attack_pattern, self.options)
        self.assertEqual(results.is_valid, False)
