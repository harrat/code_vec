def test_blackout_sections(self):
        print('test blackout sections...')
        operator = CutoutOperator(self.volume_path,
                                  mip=self.mip,
                                  blackout_sections=True)

        offset = (4, 64, 64)
        shape = (28, 320, 320)
        output_bbox = Bbox.from_delta(offset, shape)
        chunk = operator(output_bbox)

        img = np.copy(self.img)
        for z in self.blackout_section_ids:
            img[z, :, :] = 0

        img = img[4:-4, 64:-64, 64:-64]
        self.assertTrue(img == chunk)
        shutil.rmtree('/tmp/test')
