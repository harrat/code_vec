@pytest.mark.vcr()
def test_scan_export_history_id_typeerror(api):
    with pytest.raises(TypeError):
        api.scans.export(1, history_id='nope')
