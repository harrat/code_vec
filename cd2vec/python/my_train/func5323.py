@mock.patch('thundra.opentracing.recorder.ThundraRecorder')
def test_start_span(mock_recorder, span):
    tracer = ThundraTracer.get_instance()
    start_time = time.time()
    with tracer.start_span(operation_name='test', child_of=span, start_time=start_time) as active_span:
        assert active_span.operation_name == 'test'
        assert active_span.start_time == start_time
        assert active_span.context.parent_span_id == span.context.span_id
        assert active_span.context.trace_id == span.context.trace_id
