def test_mkdir():

    dir_name = "foo"
    path = os.path.join(TEST_DIR, dir_name)

    if (os.path.exists(path)):
        raise ValueError("Directory %s already exists!" % path)

    fs.mkdir(path)

    assert os.path.exists(path) is True

def test_mkdir_recursive():
