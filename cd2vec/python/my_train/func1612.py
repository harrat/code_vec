def test_mock_queue_put_get():
    assert mock_queue.qsize() == 0

    task = MockTask()
    task.uri = "my_uri"
    mock_queue.put(task)
    assert mock_queue.qsize() == 1

    returned_task = mock_queue.get()
    assert isinstance(returned_task, MockTask) is True
    assert returned_task.uid == task.uid
    assert returned_task.unique_hash() == task.unique_hash()
    assert returned_task.uri == "my_uri"
    assert mock_queue.qsize() == 0

