def test_get_references(query):
    """
    Test getting the references (without wanting ADS urls).
    """

    query.get_references()

    # test parsing a reference
    ref = query.parse_ref('ksm+06')

    assert isinstance(ref, string_types)
    assert "2006, Science, 314, 97" in ref

    # test parsing two references
    ref = query.parse_ref(['ksm+06', 'abb+18'])

    assert len(ref) == 2
    assert "2006, Science, 314, 97" in ref[0]
    assert "2018, ApJS, 235, 37" in ref[1]

    # test parsing two references (with the second one being gibberish)
    ref = query.parse_ref(['ksm+06', 'wlihlacljkblf'])

    assert len(ref) == 2
    assert "2006, Science, 314, 97" in ref[0]
    assert ref[1] is None

