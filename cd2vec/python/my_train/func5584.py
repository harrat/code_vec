@pytest.mark.parametrize('fname, saved_as, written_as', [
    ('alexa_test', 'csv', None),
    ('csv_test', 'csv', None),
    ('txt_test', 'txt', None),
    ('file_override_test', 'txt', 'csv')
])
def test_saving_utterances(global_um, tmpdir, fname, saved_as, written_as):
    """
    Test the saving methods of UtterMore
    """
    written_as = written_as or saved_as
    test_dir = path.join(path.dirname(path.realpath(__file__)), 'test_files')

    if fname == 'alexa_test':
        global_um.save_for_alexa(tmpdir, fname)
    else:
        global_um.save_utterances(tmpdir, fname, saved_as, written_as=written_as)

    file_name = fname + '.' + saved_as
    assert filecmp.cmp(path.join(test_dir, file_name),
                       tmpdir.join(file_name))
