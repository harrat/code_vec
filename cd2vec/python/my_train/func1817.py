def test_prefill_entries_add_to_bottom(cli, data_dir, config):
    efg = EntriesFileGenerator(data_dir, '%m_%Y.txt')
    efg.expand(datetime.date(2014, 1, 21)).write(
        """20/01/2014
alias_1 2 hello world

21/01/2014
alias_1 1 foo bar""")
    efg.expand(datetime.date(2014, 2, 21)).write(
        "20/02/2014\nalias_1 2 hello world"
    )
    efg.patch_config(config)

    with freeze_time('2014-02-21'):
        cli('edit')

    lines = efg.expand(datetime.date(2014, 2, 21)).readlines()

    assert '20/02/2014\n' == lines[0]
    assert '21/02/2014\n' == lines[3]

