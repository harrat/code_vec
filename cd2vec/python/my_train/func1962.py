def test_migrate_files(self):
        '''tests that migrate_files migrates the correct number of files from the Example bootstrap template directory
         to the Test_application directory. This is done by walking through the Test_application directory with os.walk
         and counting the number of files with a particular set of extensions (see extensions list) and comparing this
         number to the number of files gotten from walking through a "gold_standard" respective Test_application_test_folder.
        '''
        migrate_dict = self.test.detect_static_files()
        self.test.migrate_files(migrate_dict)
        extensions = ['.js', '.css', '.jpg', '.png', 'gif', '.ico', '.otf',
                      '.eot', '.svg', '.ttf', '.woff', '.woff2']
        test_dir = self.flaskerized_app_dir
        test_file_list = []
        gold_standard_file_list = []
        for dir in [test_dir, self.gold_standard_dir]:
            for path, subdir, files in os.walk(dir):
                for name in files:
                    for extension in extensions:
                        if name.endswith(extension):
                            if dir == test_dir:
                                test_file_list.append(name)
                            if dir == self.gold_standard_dir:
                                gold_standard_file_list.append(name)
        self.assertEqual(len(test_file_list), len(gold_standard_file_list))
