def test_open_(self):
        path = self.root.make_file(contents='foo')
        with self.assertRaises(ValueError):
            with open_(path, wrap_fileobj=False):
                pass
        with open_(path, compression=False) as fh:
            assert fh.read() == 'foo'
        with open_(path, compression=False) as fh:
            assert next(fh) == 'foo'
        with open(path) as fh:
            with open_(fh, compression=False, context_wrapper=True) as fh2:
                self.assertTrue(isinstance(fh2, FileLikeWrapper))
                assert fh2.read() == 'foo'
        with open(path) as fh3:
            with open_(fh, wrap_fileobj=False, context_wrapper=True):
                self.assertFalse(isinstance(fh3, FileLikeWrapper))
