def test_convert_activity_xml_1():

    cove_temp_folder = tempfile.mkdtemp(
        prefix="lib-cove-iati-tests-", dir=tempfile.gettempdir()
    )

    activity_xml_filename = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "fixtures",
        "converters",
        "convert_activity_1.xml",
    )

    lib_cove_config = LibCoveConfig()
    output = convert_json(
        cove_temp_folder,
        "",
        activity_xml_filename,
        lib_cove_config,
        flatten=True,
        xml=True,
        root_list_path="iati-activity",
    )

    assert output["converted_url"] == "/flattened"
    assert len(output["conversion_warning_messages"]) == 0
    assert output["conversion"] == "flatten"

    conversion_warning_messages_name = os.path.join(
        cove_temp_folder, "conversion_warning_messages.json"
    )
    assert os.path.isfile(conversion_warning_messages_name)
    with open(conversion_warning_messages_name) as fp:
        conversion_warning_messages_data = json.load(fp)
    assert conversion_warning_messages_data == []

    assert os.path.isfile(os.path.join(cove_temp_folder, "flattened.xlsx"))
    assert os.path.isfile(
        os.path.join(cove_temp_folder, "flattened", "iati-activity.csv")
    )

    with open(
        os.path.join(cove_temp_folder, "flattened", "iati-activity.csv"), "r"
    ) as csvfile:
        csvreader = csv.reader(csvfile)
