def test_check_logger(self):
        self.logger_with_check.info('foo')
        self.logger_with_check.verbose('bar')

        assert os.path.exists(self.logfile)
        assert self.found_in_logfile('info')
        assert self.found_in_logfile('verbose')
        assert self.found_in_logfile('_FakeCheck')
