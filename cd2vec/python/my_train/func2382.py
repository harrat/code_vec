def test_screen_rain(class_objects, result):
    from cloudnetpy.products.drizzle import _screen_rain
    d_source, d_class, s_width = class_objects
    result = _screen_rain(result, d_class)
    compare = True
    for key in result.keys():
        if not utils.isscalar(result[key]):
            if not np.any(result[key][-1]) == np.any(np.array([0, 0, 0])):
                compare = False
    assert compare is True

