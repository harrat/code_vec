def test__write_log(init_ExpStock_with_args):
    e = init_ExpStock_with_args.__next__()
    filename = 'git_head.txt'
    filepath = os.path.join(e.log_dirname, filename)

    e._write_log(filename, b'testtesttest', 'wb')
    assert os.path.isfile(filepath)
    with open(filepath) as f:
        data = f.read()
    assert data == 'testtesttest'

def test__write_logs(init_ExpStock_with_args):
    e = init_ExpStock_with_args.__next__()
    filename = 'params.txt'
    filepath = os.path.join(e.log_dirname, filename)
