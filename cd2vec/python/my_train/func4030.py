def test_run_docker(d):
    def test_exec(_file, argv):
        assert argv[0:5] == ['docker', 'run', '--rm', '-i', '--user']
        for port in range(10000, 10005):
            assert '--expose=%d'%port in argv
            assert is_sublist(argv, ['-p', '%d:%d'%(port, port)])
        assert '--some-arg=AAA' in argv
        assert 'IMAGE' in argv
        i = argv.index('-f')
        assert json.loads(open(argv[i+1]).read())['ip'] == '0.0.0.0'
        connection_file = pjoin(d, 'connection.json')
        assert is_sublist(argv, ["--mount", "type=bind,source=%s,destination=%s,ro=false"%(connection_file, connection_file)])
    kern = install(d, "docker --some-arg=AAA IMAGE")
    run(d, kern, test_exec)

    # Test the --pwd option, and also that it does not go when it is NOT present
    pwd_mount = ["--mount", "type=bind,source=%s,destination=%s,ro=false"%(os.getcwd(), os.getcwd())]
    def test_exec(_file, argv):
        assert is_sublist(argv, pwd_mount)
    kern = install(d, "docker --some-arg=AAA --pwd IMAGE")
    run(d, kern, test_exec)
    # Test above, but reversed
    def test_exec(_file, argv):
        assert not is_sublist(argv, pwd_mount)
    kern = install(d, "docker --some-arg=AAA IMAGE")
    run(d, kern, test_exec)
    # Custom workdir
    pwd_mount = ["--mount", "type=bind,source=%s,destination=%s,ro=false"%(os.getcwd(), '/WORKDIR')]
    def test_exec(_file, argv):
        assert is_sublist(argv, pwd_mount)
    kern = install(d, "docker --some-arg=AAA --workdir=/WORKDIR IMAGE")
    run(d, kern, test_exec)

