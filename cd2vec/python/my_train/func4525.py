def test_load_with_yaml_file(self, yaml_file):
        fadapter = File(yaml_file.name)
        fadapter.load()
        assert 'ABC' in fadapter.data
        assert fadapter.data['ABC'] == '123'
