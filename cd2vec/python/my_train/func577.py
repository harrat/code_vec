def test_parse_unicode_name_two_byte(tmpdir):
    indir = tmpdir.mkdir('unicode')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'f?o'), 'wb') as outfp:
        outfp.write(b'foo\n')

    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_unicode_name_two_byte)

def test_parse_unicode_name_two_byte(tmpdir):
    indir = tmpdir.mkdir('unicode')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'f?o'), 'wb') as outfp:
        outfp.write(b'foo\n')
def test_parse_unicode_name_two_byte(tmpdir):
    indir = tmpdir.mkdir('unicode')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'f?o'), 'wb') as outfp:
        outfp.write(b'foo\n')

    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_unicode_name_two_byte)

def test_parse_unicode_name_two_byte_isolevel4(tmpdir):
    indir = tmpdir.mkdir('unicodeisolevel4')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'f?o'), 'wb') as outfp:
        outfp.write(b'foo\n')
