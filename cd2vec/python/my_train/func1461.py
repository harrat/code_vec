def test_forward_reverse_equivalence(self):
        transform = CompositeTransform()
        transform.rotate(np.array([1., 2., 3.]))
        transform.translate(np.array([3., 2., 1.]))
        transform.scale(10.)
        transform.rotate(np.array([7., 13., 5.]))

        forward = transform.matrix_for()
        reverse = transform.matrix_for(reverse=True)
        np.testing.assert_allclose(reverse, np.linalg.inv(forward))

        forward = transform.matrix_for(from_range=(0, 2))
        reverse = transform.matrix_for(from_range=(0, 2), reverse=True)
        np.testing.assert_allclose(reverse, np.linalg.inv(forward))
