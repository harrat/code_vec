def test_database_error():
    assert issubclass(py2jdbc.DatabaseError, py2jdbc.Error), \
        "DatabaseError is not a subclass of Error"
