def test_reconnect_backoff(region, region_cache):
    region['key1'] = 0
    region['key2'] = 1
    region_cache._reconnect_backoff = 5  # 5 second backoff before trying to reconnect
    region_cache.invalidate_connections()
    assert region_cache.is_disconnected()
    with pytest.raises(KeyError):
        region['key1']
    assert region_cache._w_conn is None
    assert region_cache._r_conn is None

