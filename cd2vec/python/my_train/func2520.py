def test_miniconda_install(tmp):
    c = CondaCreator(conda_root=tmp)
    assert tmp in c.conda_bin
    assert os.path.exists(c.conda_bin)

