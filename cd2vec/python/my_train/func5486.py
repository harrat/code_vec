@pytest.mark.parametrize(
    'backend', ['cython', 'opencl',
                pytest.param('cuda', marks=pytest.mark.xfail)])
def test_sort_by_keys_with_output(backend):
    check_import(backend)

    # Given
    nparr1 = np.random.randint(0, 100, 16, dtype=np.int32)
    nparr2 = np.random.randint(0, 100, 16, dtype=np.int32)
    dev_array1, dev_array2 = array.wrap(nparr1, nparr2, backend=backend)
    out_arrays = [
        array.zeros_like(dev_array1),
        array.zeros_like(dev_array2)]

    # When
    array.sort_by_keys([dev_array1, dev_array2],
                       out_list=out_arrays, use_radix_sort=False)

    # Then
    order = np.argsort(nparr1)
    act_result1 = np.take(nparr1, order)
    act_result2 = np.take(nparr2, order)
    assert np.all(out_arrays[0].get() == act_result1)
    assert np.all(out_arrays[1].get() == act_result2)

