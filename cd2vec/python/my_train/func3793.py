def test_parse(self):
        self.assertEqual(addr.parse(helpers.OP_IF['p2sh']),
                         b'\x05' + helpers.OP_IF['script_hash'])
        self.assertEqual(addr.parse(helpers.MSIG_2_2['p2sh']),
                         b'\x05' + helpers.MSIG_2_2['script_hash'])
        self.assertEqual(addr.parse(
            helpers.P2WSH['human']['ins'][0]['addr']),
            b'\x00\x20' + helpers.P2WSH['ser']['ins'][0]['pk_script'][2:])
        self.assertEqual(addr.parse(helpers.P2WPKH_ADDR['address']),
                         b'\x00\x14' + helpers.P2WPKH_ADDR['pkh'])
        self.assertEqual(addr.parse(helpers.ADDR[0]['p2pkh']),
                         b'\x00' + helpers.PK['ser'][0]['pkh'])

        with self.assertRaises(ValueError) as context:
            addr.parse('This is not a valid address.')

        self.assertIn('Unsupported address format. Got: ',
                      str(context.exception))
