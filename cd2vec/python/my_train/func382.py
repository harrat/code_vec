def test_getidfkeyswithnodes():
    """py.test for getidfkeyswithnodes"""
    nodekeys = idf_helpers.getidfkeyswithnodes()
    # print(len(nodekeys))
    assert "PLANTLOOP" in nodekeys
    assert "ZONE" not in nodekeys

