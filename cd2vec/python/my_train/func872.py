def test_observable_object_custom_properties_strict(self):
        observed_data = copy.deepcopy(self.valid_observed_data)
        observed_data['objects']['0']['x_x_foo'] = "bar"
        self.assertFalseWithOptions(observed_data, strict_properties=True)
