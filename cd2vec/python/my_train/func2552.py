def test_get_expression_data_errors(testfiles, atlas):
    # invalid probe_selection method
    with pytest.raises(ValueError):
        allen.get_expression_data(atlas['image'], donors=['12876', '15496'],
                                  probe_selection='nonsense')

    # cannot use diff_stability with only one donor
    with pytest.raises(ValueError):
        allen.get_expression_data(atlas['image'], donors=['12876'],
                                  probe_selection='diff_stability')
