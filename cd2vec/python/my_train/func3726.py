def testfanpower_watts():
    """py.test for fanpower_watts in idf"""
    idf = IDF(StringIO(vavfan))
    thefans = idf.idfobjects["Fan:VariableVolume".upper()]
    thefan = thefans[0]
    watts = thefan.f_fanpower_watts
    assert almostequal(watts, 1791.9664027495671)
    # test autosize
    thefan.Maximum_Flow_Rate = "autosize"
    watts = thefan.f_fanpower_watts
    assert watts == "autosize"

