def test_read_samples_from_file(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE, rs=True)
        self.assertIsNotNone(audiofile.audio_samples)
        audiofile.clear_data()
