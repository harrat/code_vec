def test_get_cnn_dailymail(self):
        raw = get_cnn_dailymail()
        # train
        self.assertIn('train', raw)
        self.assertEqual(len(raw['train']), 2)
        for x in raw['train']:
            self.assertEqual(len(x), 287_227)
        # dev
        self.assertIn('dev', raw)
        self.assertEqual(len(raw['dev']), 2)
        for x in raw['dev']:
            self.assertEqual(len(x), 13_368)
        # test
        self.assertIn('test', raw)
        self.assertEqual(len(raw['test']), 2)
        for x in raw['test']:
            self.assertEqual(len(x), 11_490)
