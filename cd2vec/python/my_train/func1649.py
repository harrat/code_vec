def test_spelling_bad_dict():
    """Test spelling with a bad dict option."""
    po_check = PoCheck()
    po_check.set_spelling_options('str', 'xxx', None)
    assert not po_check.extra_checkers

