def test_get_product_trades(self, client):
        r = list(islice(client.get_product_trades('BTC-USD'), 200))
        assert type(r) is list
        assert 'trade_id' in r[0]
