def test_scale_airborne_survey():
    """
    Test if synthetic airborne survey returns the expected survey after scaled
    """
    region = (-10.1, 9.7, -20.3, -10.5)  # a random region to scale the survey
    survey = airborne_survey(region=region)
    assert set(survey.columns) == set(["longitude", "latitude", "height"])
    assert survey.longitude.size == 5673
    npt.assert_allclose(survey.longitude.min(), region[0])
    npt.assert_allclose(survey.longitude.max(), region[1])
    npt.assert_allclose(survey.latitude.min(), region[2])
    npt.assert_allclose(survey.latitude.max(), region[3])
    npt.assert_allclose(survey.height.min(), 359.0)
    npt.assert_allclose(survey.height.max(), 1255.0)

