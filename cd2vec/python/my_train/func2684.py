def test_makedirs_to(self):
        # Make a directory tree
        d = tempfile.mkdtemp(prefix='tmp_bupper_test_')
        utils.makedirs_to(join(d, 'some/other/thing'))
        assert exists(join(d, 'some/other'))
        assert not exists(join(d, 'some/other/thing'))
        os.removedirs(join(d, 'some/other'))
