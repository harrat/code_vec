def test_get_external_ids(empty_person: Person):
    external_ids = empty_person.get_external_ids()
    assert external_ids == empty_person.data["external_ids"]

