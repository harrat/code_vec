def test_rolling_fileoutput(self):
        path = str(self.root.make_file())
        with RollingFileOutput(
                path + '{index}.txt', char_mode=TextMode, linesep=os.linesep,
                lines_per_file=3) as out:
            for i in range(6):
                out.write(str(i))
        with open(path + '0.txt', 'rt') as infile:
            assert '0\n1\n2\n' == infile.read()
        with open(path + '1.txt', 'rt') as infile:
            assert '3\n4\n5\n' == infile.read()
