@patch('multiprocessing.pool.Pool.apply_async')
    def test_launch_tests(self, mock_apply, *args):
        self.provider.tests = fixture('tests.yaml', get_yaml=True)
        self.provider.action()
        self.assertEqual(3, mock_apply.call_count)
