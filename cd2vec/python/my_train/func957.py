def test_zonearea_zonevolume():
    """py.test for zonearea and zonevolume"""
    idftxt = """Zone, 473222, 0.0, 0.0, 0.0, 0.0, , 1;
        BuildingSurface:Detailed, F7289B, Floor, Exterior Floor, 473222,
        Ground, ,
        NoSun, NoWind, , 4, 2.23, 2.56, 0.0, 2.23, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        2.56, 0.0;  BuildingSurface:Detailed, F3659B, Wall, Exterior Wall,
        473222, Outdoors, , SunExposed, WindExposed, , 4, 2.23, 2.56, 1.49,
        2.23, 2.56, 0.0, 0.0, 2.56, 0.0, 0.0, 2.56, 1.49;
        BuildingSurface:Detailed, 46C6C9, Wall, Exterior Wall, 473222,
        Outdoors, , SunExposed, WindExposed, , 4, 2.23, 0.0, 1.49, 2.23,
        0.0, 0.0, 2.23, 1.02548139464, 0.0, 2.23, 1.02548139464, 1.49;
        BuildingSurface:Detailed, 4287DD, Wall, Exterior Wall, 473222,
        Outdoors, , SunExposed, WindExposed, , 4, 0.0, 2.56, 1.49, 0.0,
        2.56, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.49;
        BuildingSurface:Detailed, 570C2E, Wall, Exterior Wall, 473222,
        Outdoors, , SunExposed, WindExposed, , 4, 0.0, 0.0, 1.49, 0.0, 0.0,
        0.0, 2.23, 0.0, 0.0, 2.23, 0.0, 1.49;  BuildingSurface:Detailed,
        BAEA99, Roof, Exterior Roof, 473222, Outdoors, , SunExposed,
        WindExposed, , 4, 0.0, 2.56, 1.49, 0.0, 0.0, 1.49, 2.23, 0.0, 1.49,
        2.23, 2.56, 1.49;  BuildingSurface:Detailed, C879FE, Floor,
        Exterior Floor, 473222, Ground, , NoSun, NoWind, , 4, 3.22,
        2.52548139464, 0.0, 3.22, 1.02548139464, 0.0, 2.23,
        1.02548139464, 0.0, 2.23, 2.52548139464, 0.0;
        BuildingSurface:Detailed, 25B601, Wall, Exterior Wall, 473222,
        Outdoors, , SunExposed, WindExposed, , 4, 2.23,
        1.02548139464, 1.49, 2.23, 1.02548139464, 0.0, 2.23, 2.52548139464,
        0.0, 2.23, 2.52548139464, 1.49;  BuildingSurface:Detailed, F5EADC,
        Wall, Exterior Wall, 473222, Outdoors, , SunExposed, WindExposed, ,
        4, 2.23, 1.02548139464, 1.49, 2.23, 1.02548139464, 0.0, 3.22,
        1.02548139464, 0.0, 3.22, 1.02548139464, 1.49;
        BuildingSurface:Detailed, D0AABE, Wall, Exterior Wall, 473222,
        Outdoors, , SunExposed, WindExposed, , 4, 3.22, 1.02548139464,
        1.49, 3.22, 1.02548139464, 0.0, 3.22, 2.52548139464, 0.0, 3.22,
        2.52548139464, 1.49;  BuildingSurface:Detailed, B0EA02, Wall,
        Exterior Wall, 473222, Outdoors, , SunExposed, WindExposed, ,
        4, 3.22, 2.52548139464, 1.49, 3.22, 2.52548139464, 0.0, 2.23,
        2.52548139464, 0.0, 2.23, 2.52548139464, 1.49;
        BuildingSurface:Detailed, E6DF3B, Roof, Exterior Roof, 473222,
        Outdoors, , SunExposed, WindExposed, , 4, 2.23, 2.52548139464, 1.49,
        2.23, 1.02548139464, 1.49, 3.22, 1.02548139464, 1.49, 3.22,
        2.52548139464, 1.49;  BuildingSurface:Detailed, 4F8681, Wall,
        Exterior Wall, 473222, Outdoors, , SunExposed, WindExposed, , 4,
        2.23, 2.52548139464, 1.49, 2.23, 2.52548139464, 0.0, 2.23, 2.56,
        0.0, 2.23, 2.56, 1.49;  """
    idf = IDF(StringIO(idftxt))
    result = modeleditor.zonearea(idf, "473222")
    assert almostequal(result, 7.1938)
    result = modeleditor.zonearea_floor(idf, "473222")
    assert almostequal(result, 7.1938)
    result = modeleditor.zonearea_roofceiling(idf, "473222")
    assert almostequal(result, 7.1938)
    result = modeleditor.zone_floor2roofheight(idf, "473222")
    assert almostequal(result, 1.49)
    result = modeleditor.zoneheight(idf, "473222")
    assert almostequal(result, 1.49)
    result = modeleditor.zone_floor2roofheight(idf, "473222")
    assert almostequal(result, 1.49)
    result = modeleditor.zonevolume(idf, "473222")
    assert almostequal(result, 10.718762)
    # remove floor
    zone = idf.getobject("ZONE", "473222")
    surfs = idf.idfobjects["BuildingSurface:Detailed".upper()]
    zone_surfs = [s for s in surfs if s.Zone_Name == zone.Name]
    floors = [s for s in zone_surfs if s.Surface_Type.upper() == "FLOOR"]
    for floor in floors:
        idf.removeidfobject(floor)
    result = modeleditor.zonearea_floor(idf, "473222")
    assert almostequal(result, 0)
    result = modeleditor.zonearea_roofceiling(idf, "473222")
    assert almostequal(result, 7.1938)
    result = modeleditor.zonearea(idf, "473222")
    assert almostequal(result, 7.1938)
    result = modeleditor.zoneheight(idf, "473222")
    assert almostequal(result, 1.49)
    result = modeleditor.zonevolume(idf, "473222")
    assert almostequal(result, 10.718762)
    # reload idf and remove roof/ceiling
    idf = IDF(StringIO(idftxt))
    zone = idf.getobject("ZONE", "473222")
    surfs = idf.idfobjects["BuildingSurface:Detailed".upper()]
    zone_surfs = [s for s in surfs if s.Zone_Name == zone.Name]
    roofs = [s for s in zone_surfs if s.Surface_Type.upper() == "ROOF"]
    ceilings = [s for s in zone_surfs if s.Surface_Type.upper() == "CEILING"]
    topsurfaces = roofs + ceilings
    for surf in topsurfaces:
        idf.removeidfobject(surf)
    result = modeleditor.zonearea_roofceiling(idf, "473222")
    assert almostequal(result, 0)
    result = modeleditor.zonearea(idf, "473222")
    assert almostequal(result, 7.1938)
    result = modeleditor.zoneheight(idf, "473222")
    assert almostequal(result, 1.49)
    result = modeleditor.zonevolume(idf, "473222")
    assert almostequal(result, 10.718762)

