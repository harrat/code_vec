def test_makepipecomponent():
    """py.test for makepipecomponent"""
    tdata = (
        (
            "apipe",
            ["PIPE:ADIABATIC", "apipe", "apipe_inlet", "apipe_outlet"],
        ),  # pname, pipe_obj
        (
            "bpipe",
            ["PIPE:ADIABATIC", "bpipe", "bpipe_inlet", "bpipe_outlet"],
        ),  # pname, pipe_obj
    )
    for pname, pipe_obj in tdata:
        fhandle = StringIO("")
        idf = IDF(fhandle)
        result = hvacbuilder.makepipecomponent(idf, pname)
        assert result.obj == pipe_obj

