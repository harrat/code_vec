def test_template_config(git_dir):
    template = r"""{
  "header": "# Managed by dzonegit on $datetime, do not edit.\n",
  "footer": "# This is the end",
  "item": " - zone: \"$zonename\"\n   file: \"$zonefile\"\n   $zonevar\n",
  "defaultvar": "template: default",
  "zonevars": {
    "example.com": "template: signed",
    "*": "template: dummy"
  }
}"""
    output = dzonegit.template_config(str(git_dir), template)
    assert output.startswith("# Managed by dzonegit")
    assert " - zone: \"dummy\"\n   file: \"" in output
    assert "   template: dummy" in output
    assert output.endswith("# This is the end")
    output = dzonegit.template_config(
        str(git_dir),
        template,
        whitelist=set("a"),
    )
    assert " - zone: \"dummy\"\n   file: \"" not in output
    output = dzonegit.template_config(
        str(git_dir),
        template,
        blacklist=set("*"),
    )
    assert " - zone: \"dummy\"\n   file: \"" not in output
    output = dzonegit.template_config(str(git_dir), "{}")
    assert len(output) == 0

