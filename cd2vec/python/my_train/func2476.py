@pytest.mark.vcr()
def test_agentgroups_add_mult_agents_to_group(api, agentgroup):
    agents = api.agents.list()
    task = api.agent_groups.add_agent(agentgroup['id'],
        agents.next()['id'],
        agents.next()['id']
    )
    assert isinstance(task, dict)
    check(task, 'container_uuid', str)
    check(task, 'status', str)
    check(task, 'task_id', str)

@pytest.mark.vcr()
def test_agentgroups_configure_group_id_typeerror(api):
    with pytest.raises(TypeError):
        api.agent_groups.configure('nope', 1)
