@helpers.seed
def test_lilliefors_log(dc):
    known_csv = """\
        station,Inflow,Inflow,Outflow,Outflow,Reference,Reference
        result,log-lilliefors,pvalue,log-lilliefors,pvalue,log-lilliefors,pvalue
        param,,,,,,
        A,0.08548109,0.95458004,0.15443943,0.19715747,0.20141389,0.03268737
        B,0.16162839,0.10505016,0.12447902,0.49697902,0.15934334,0.22969362
        C,0.16957278,0.07248915,0.12388174,0.44379732,0.11746642,0.48915671
        D,0.06885549,0.99,0.06067356,0.99,0.13401954,0.41967483
        E,0.13506577,0.47186822,0.14552341,0.47797919,0.09164876,0.92860794
        F,0.14420794,0.30694533,0.08463267,0.92741885,0.08586933,0.9800294
    """
    check_stat(known_csv, dc.lilliefors_log)

