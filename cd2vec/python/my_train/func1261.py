def test_implements_not_self_convention_in_getter():
    """
    Case: do not implement interface member, which do not follow naming convention, that is getter.
    Expect: class does not implement interface member error message.
    """
    class HumanNameInterface:

        @property
        def name(this):
            return

    with pytest.raises(InterfaceMemberHasNotBeenImplementedException) as error:

        @implements(HumanNameInterface)
        class HumanWithoutImplementation:
            pass

    assert 'class HumanWithoutImplementation does not implement ' \
           'interface member HumanNameInterface.name(this)' == error.value.message
