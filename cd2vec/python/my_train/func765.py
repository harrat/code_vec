def test_fit(self):
        dim = torch.randint(2, 4, (1,), dtype=torch.int32).tolist()[0]
        shape = torch.randint(2, 5, (dim,), dtype=torch.int32).tolist()
        normal_shape = shape[-1]
        net = LayerNormalization(normal_shape)
        optimizer = torch.optim.Adam(net.parameters())
        criterion = nn.MSELoss()
        dataset = SimpleDataset(shape)
        for epoch in range(20):
            running_loss = 0.0
            for i in range(len(dataset)):
                x, y = dataset[i]
                optimizer.zero_grad()
                y_hat = net(x)
                loss = criterion(y_hat, y)
                loss.backward()
                optimizer.step()
                running_loss += loss.item()
            print('Epoch: %2d  Loss: %3.4f' % (epoch + 1, running_loss / len(dataset)))
        for i, (x, y) in enumerate(dataset):
            if i > 10:
                break
            y_hat = net(x)
            self.assertTrue(y.allclose(y_hat, rtol=0.0, atol=1e-3), (i, y, y_hat))
