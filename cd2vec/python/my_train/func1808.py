def test_function_wrapper_function_exception(trace_transport):
    @epsagon.python_wrapper()
    def wrapped_function(event, context):
        raise TypeError('test')

    with pytest.raises(TypeError):
        wrapped_function('a', 'b')

    assert len(trace_transport.last_trace.events) == 1

    event = trace_transport.last_trace.events[0]
    assert event.exception['type'] == 'TypeError'
    assert event.resource['metadata']['python.function.return_value'] is None
    assert not trace_transport.last_trace.exceptions

