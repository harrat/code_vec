@given(NON_EMPTY_TEXT_ITERABLES)
    def test_generates_correct_regexp_from_iterable_of_strings(self, expected_strings):
        assert_strings_can_be_matched(
            self, iterable_to_regexp(expected_strings, PythonFormatter), expected_strings
        )
