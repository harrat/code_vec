@staticmethod
    @pytest.mark.usefixtures('default-references', 'import-pytest')
    def test_api_search_reference_paginate_001(server):
        """Search reference with GET.

        Send GET /reference so that pagination is applied with limit zero.
        This is a special case that returns the metadata but the data list
        is empty. This query uses sall parameter with regexp . (dot) which
        matches to all references. The non-zero offset does not affect to the
        total count of query result and it is just returned in the meta as it
        was provided.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '71'
        }
        expect_body = {
            'meta': {
                'count': 0,
                'limit': 0,
                'offset': 4,
                'total': 3
            },
            'data': [],
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/references',
            headers={'accept': 'application/json'},
            query_string='sall=.&offset=4&limit=0&sort=brief')
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
