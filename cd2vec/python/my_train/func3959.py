def test_thermostat_type_1_get_resistance_heat_utilization_bins_rhu1(thermostat_type_1,
        core_heating_day_set_type_1_entire, metrics_type_1_data):

    start = 0
    stop = 60
    step = 5
    temperature_bins = list(t for t in range(start, stop+step, step))
    rhu_runtime = thermostat_type_1.get_resistance_heat_utilization_runtime(
            core_heating_day_set_type_1_entire)
    rhu = thermostat_type_1.get_resistance_heat_utilization_bins(
            rhu_runtime,
            temperature_bins,
            core_heating_day_set_type_1_entire)

    assert len(rhu) == 12

    for item in rhu.itertuples():
        bin_name = thermostat_type_1._format_rhu('rhu1', item.Index.left, item.Index.right, duty_cycle=None)
        bin_value = item.rhu
        assert_allclose(bin_value, metrics_type_1_data[1][bin_name], rtol=1e-3)

