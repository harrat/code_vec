@pytest.mark.vcr()
def test_access_groups_delete_success(api, agroup):
    api.access_groups.delete(agroup['id'])

@pytest.mark.vcr()
def test_access_group_edit_id_typeerror(api):
    with pytest.raises(TypeError):
        api.access_groups.edit(1)
