def test_get_html_attribute(self):
        """Test getting HTML version of a resource rather than JSON."""
        response = self.get_response('/artists/1/Name',
                200,
                headers={'Accept': 'text/html'})
        assert self.is_html_response(response)
