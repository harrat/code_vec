def test_copyidfintoidf():
    """py.test for copyidfintoidf"""
    tonames = ["a", "b", "c"]
    fromnames = ["d", "e"]
    allnames = ["a", "b", "c", "d", "e"]
    toidf = IDF(StringIO(""))
    toidf.newidfobject("building", Name="a")
    toidf.newidfobject("building", Name="b")
    toidf.newidfobject("Site:Location", Name="c")
    result = idf_helpers.getidfobjectlist(toidf)
    assert [res.Name for res in result] == tonames
    fromidf = IDF(StringIO(""))
    fromidf.newidfobject("ScheduleTypeLimits", Name="d")
    fromidf.newidfobject("ScheduleTypeLimits", Name="e")
    result = idf_helpers.getidfobjectlist(fromidf)
    assert [res.Name for res in result] == fromnames
    idf_helpers.copyidfintoidf(toidf, fromidf)
    result = idf_helpers.getidfobjectlist(toidf)
    assert [res.Name for res in result] == allnames

