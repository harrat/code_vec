def test_granular_marking_circular_ref(self):
        marking_definition = copy.deepcopy(self.valid_marking_definition)
        marking_definition['granular_markings'] = [{
            "marking_ref": "marking-definition--34098fce-860f-48ae-8e50-ebd3cc5e41da",
            "selectors": ["created"]
        }]
        self.assertFalseWithOptions(marking_definition)
