def test_search(self):
        search_result = api.search(keyword='cthesky', timeout=5000)

        results = search_result.results
        if results:
            self.assertTrue(all([_.domainName for _ in results]))
            self.assertTrue(all([_.sld for _ in results]))
            self.assertTrue(all([_.tld for _ in results]))
            self.assertIn('cthesky', [_.sld for _ in results])
