def test_set_attribute_on_managed_object_by_index(self):
        """
        Test that an attribute can be modified on a managed object given its
        name, index, and new value.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        symmetric_key = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b'',
            masks=[enums.CryptographicUsageMask.ENCRYPT,
                   enums.CryptographicUsageMask.DECRYPT]
        )

        e._data_session.add(symmetric_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._set_attribute_on_managed_object(
            symmetric_key,
            (
                "Application Specific Information",
                [
                    attributes.ApplicationSpecificInformation(
                        application_namespace="Example Namespace 1",
                        application_data="Example Data 1"
                    ),
                    attributes.ApplicationSpecificInformation(
                        application_namespace="Example Namespace 2",
                        application_data="Example Data 2"
                    )
                ]
            )
        )
        e._set_attribute_on_managed_object(
            symmetric_key,
            (
                "Name",
                [
                    attributes.Name(
                        name_value=attributes.Name.NameValue("Name 1")
                    ),
                    attributes.Name(
                        name_value=attributes.Name.NameValue("Name 2")
                    )
                ]
            )
        )
        e._set_attribute_on_managed_object(
            symmetric_key,
            (
                "Object Group",
                [
                    primitives.TextString(
                        "Example Group 1",
                        tag=enums.Tags.OBJECT_GROUP
                    ),
                    primitives.TextString(
                        "Example Group 2",
                        tag=enums.Tags.OBJECT_GROUP
                    )
                ]
            )
        )

        # Test setting an ApplicationSpecificInformation attribute by index
        a = e._get_attribute_from_managed_object(
            symmetric_key,
            "Application Specific Information"
        )
        self.assertEqual(2, len(a))
        self.assertEqual(
            "Example Namespace 1",
            a[0].get("application_namespace")
        )
        self.assertEqual("Example Data 1", a[0].get("application_data"))
        self.assertEqual(
            "Example Namespace 2",
            a[1].get("application_namespace")
        )
        self.assertEqual("Example Data 2", a[1].get("application_data"))
        e._set_attribute_on_managed_object_by_index(
            symmetric_key,
            "Application Specific Information",
            attributes.ApplicationSpecificInformation(
                application_namespace="Example Namespace 3",
                application_data="Example Data 3"
            ),
            1
        )
        a = e._get_attribute_from_managed_object(
            symmetric_key,
            "Application Specific Information"
        )
        self.assertEqual(2, len(a))
        self.assertEqual(
            "Example Namespace 1",
            a[0].get("application_namespace")
        )
        self.assertEqual("Example Data 1", a[0].get("application_data"))
        self.assertEqual(
            "Example Namespace 3",
            a[1].get("application_namespace")
        )
        self.assertEqual("Example Data 3", a[1].get("application_data"))

        # Test setting a Name attribute by index
        a = e._get_attribute_from_managed_object(symmetric_key, "Name")
        self.assertEqual(3, len(a))
        self.assertEqual("Symmetric Key", a[0].name_value.value)
        self.assertEqual("Name 1", a[1].name_value.value)
        self.assertEqual("Name 2", a[2].name_value.value)
        e._set_attribute_on_managed_object_by_index(
            symmetric_key,
            "Name",
            attributes.Name(
                name_value=attributes.Name.NameValue("Name 3")
            ),
            1
        )
        a = e._get_attribute_from_managed_object(symmetric_key, "Name")
        self.assertEqual(3, len(a))
        self.assertEqual("Symmetric Key", a[0].name_value.value)
        self.assertEqual("Name 3", a[1].name_value.value)
        self.assertEqual("Name 2", a[2].name_value.value)

        # Test setting an ObjectGroup attribute by index
        a = e._get_attribute_from_managed_object(symmetric_key, "Object Group")
        self.assertEqual(2, len(a))
        self.assertEqual("Example Group 1", a[0])
        self.assertEqual("Example Group 2", a[1])
        e._set_attribute_on_managed_object_by_index(
            symmetric_key,
            "Object Group",
            primitives.TextString(
                "Example Group 3",
                tag=enums.Tags.OBJECT_GROUP
            ),
            1
        )
        a = e._get_attribute_from_managed_object(symmetric_key, "Object Group")
        self.assertEqual(2, len(a))
        self.assertEqual("Example Group 1", a[0])
        self.assertEqual("Example Group 3", a[1])
