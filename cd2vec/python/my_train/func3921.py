@pytest.mark.assumption
def test_github_api():
    assert GitHubRenderer(raw=True).render('') == ''
    assert GitHubRenderer(user_content=True, raw=True).render('') == ''

