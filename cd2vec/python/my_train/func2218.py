def test_build_core_object_unsupported_type(self):
        """
        Test that an InvalidField error is generated when building
        kmip.core objects that are unsupported.
        """
        e = engine.KmipEngine()
        e._logger = mock.MagicMock()

        args = (None, )
        regex = "Cannot build an unsupported object type."
        six.assertRaisesRegex(
            self,
            exceptions.InvalidField,
            regex,
            e._build_core_object,
            *args
        )

        class DummyObject:
            def __init__(self):
                self._object_type = enums.ObjectType.TEMPLATE

        args = (DummyObject(), )
        regex = "The Template object type is not supported."
        six.assertRaisesRegex(
            self,
            exceptions.InvalidField,
            regex,
            e._build_core_object,
            *args
        )
