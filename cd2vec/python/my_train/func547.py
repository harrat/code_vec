def test_not_implemented(self):
        '''
        Send a series of formats and ensure they all raise exceptions.
        '''
        for format in ['xlsx', 'textbundle', 'html', 'xml']:
            with self.login(username=self.user1.username):
                print("Testing format {}".format(format))
                self.url_kwargs['format'] = format
                self.get(self.view_string, **self.url_kwargs)
                self.response_404()
