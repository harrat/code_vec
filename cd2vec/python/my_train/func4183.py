def test_ncycle_fileoutput(self):
        file1 = self.root.make_file(suffix='.gz')
        file2 = self.root.make_file()
        with textoutput(
                (file1, file2), lines_per_file=2,
                file_output_type=NCycleFileOutput) as o:
            o.writelines(('foo', 'bar', 'baz', 'blorf', 'bing'))
        with gzip.open(file1, 'rt') as i:
            assert 'foo\nbar\nbing\n' == i.read()
        with open(file2, 'rt') as i:
            assert 'baz\nblorf\n' == i.read()
