def test_known_for_department_attr(person: Person):
    assert isinstance(person.known_for_department, str)
    assert person.data["known_for_department"] == person.known_for_department

