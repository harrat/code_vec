def test_incorrect_cve_format_number(self):
        vulnerability = copy.deepcopy(self.valid_vulnerability)
        ext_refs = vulnerability['external_references']
        ext_refs[0]['external_id'] = "CVE-20161234"
        self.assertFalseWithOptions(vulnerability)
