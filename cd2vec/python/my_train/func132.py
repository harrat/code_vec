@pytest.mark.vcr()
def test_workbench_vulns_filter_type_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vulns(filter_type=123)
