@given(strategies.text(ascii_letters, min_size=2, max_size=10))
def test_app_invalid_options(opt):
    """An invalid option is given."""
    if opt not in ('debug', 'option'):
        with pytest.raises(ArgumentError):
            with DemoApp.from_cmdline(['arg_1', f'--{opt}']) as app:
                pass
