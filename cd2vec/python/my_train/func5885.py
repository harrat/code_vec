def test_image_file_content_restored_on_delete(self):

        image_frozen = os.path.join(self.FROZEN_RESOURCES_PATH, self.IMAGE_NAME)
        image_path = os.path.join(self.RESOURCES_PATH, self.IMAGE_NAME)

        self._assert_files_equal(image_path, image_frozen)

        @guard(image_path)
        def function_that_removes_the_file():
            os.remove(image_path)
            p = Path(image_path)
            self.assertFalse(p.is_file(), f'File was not removed')

        function_that_removes_the_file()

        self._assert_files_equal(image_path, image_frozen)
