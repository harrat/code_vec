def test_timeout_with_context(region_with_timeout):
    with region_with_timeout as r:
        r['key1'] = 0
        r['key2'] = 1

    assert region_with_timeout._region_cache.conn.hget(region_with_timeout.name, 'key1') is not None
    assert region_with_timeout._region_cache.conn.hget(region_with_timeout.name, 'key2') is not None

    assert region_with_timeout._region_cache.conn.ttl(region_with_timeout.name) > 0

    assert 'key1' in region_with_timeout
    assert 'key2' in region_with_timeout

    import time
    time.sleep(1)

    assert region_with_timeout._region_cache.conn.ttl(region_with_timeout.name) > 0

    assert region_with_timeout._region_cache.conn.hget(region_with_timeout.name, 'key1') is not None
    assert region_with_timeout._region_cache.conn.hget(region_with_timeout.name, 'key2') is not None

    assert 'key1' in region_with_timeout
    assert 'key2' in region_with_timeout

    time.sleep(1.5)

    assert region_with_timeout._region_cache.conn.ttl(region_with_timeout.name) == -2

    assert 'key1' not in region_with_timeout
    assert 'key2' not in region_with_timeout

