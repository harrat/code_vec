def test_web_client_data(monkeypatch):
    monkeypatch.setattr('processor.helper.httpapi.restapi_azure.get_from_currentdata',
                        mock_get_from_currentdata)
    monkeypatch.setattr(os, 'getenv', mock_empty_getenv)
    monkeypatch.setattr('processor.helper.httpapi.restapi_azure.input', mock_input)
    from processor.helper.httpapi.restapi_azure import get_client_secret, get_web_client_data
    assert 'clientSecret' == get_client_secret()
    client_id, client_secret, sub_name, sub_id, tenant_id = \
        get_web_client_data('azure', 'azureConnector.json', '<spn-name>')
    assert client_id is not None
    assert client_secret == '<client_secret>'
    assert sub_id is not None
    assert sub_name is not None
    assert tenant_id is not None

