def test_ioc():
    with patch('sys.stdout', new=StringIO()) as fake_out:
        tb = ThreatButt()
        tb.clown_strike_ioc('127.0.0.1')
        assert len(fake_out.getvalue())
