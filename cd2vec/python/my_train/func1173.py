def testWriteZenMakeMetaFile(tmpdir):
    buildconffile = tmpdir.join("buildconf.py")
    buildconffile.write("buildconf")
    dir1 = tmpdir.mkdir("dir1")
    buildconffile2 = dir1.join("buildconf.yml")
    buildconffile2.write("buildconf")
    zmmetafile = tmpdir.join("zmmetafile")

    fakeConfPaths = AutoDict()
    fakeConfPaths.buildconffile = str(buildconffile)
    fakeConfPaths.zmmetafile = str(zmmetafile)

    fakeConfManager = AutoDict()
    fakeConfManager.root.confPaths = fakeConfPaths
    fakeConfManager.configs = [
        AutoDict(path = str(buildconffile)),
        AutoDict(path = str(buildconffile2)),
    ]

    monitFiles = [x.path for x in fakeConfManager.configs]

    assert not isfile(fakeConfPaths.zmmetafile)
    attrs = {'var': 1, 'd': 'zxc'}
    assist.writeZenMakeMetaFile(fakeConfPaths.zmmetafile, monitFiles, attrs)
    assert isfile(fakeConfPaths.zmmetafile)

    dbfile = db.PyDBFile(fakeConfPaths.zmmetafile, extension = '')
    cfgenv = dbfile.load(asConfigSet = True)
    assert 'rundir' in cfgenv
    assert 'topdir' in cfgenv
    assert 'outdir' in cfgenv
    assert 'monitfiles' in cfgenv
    assert cfgenv.monitfiles == [ str(buildconffile), str(buildconffile2) ]
    assert 'monithash' in cfgenv
    assert 'attrs' in cfgenv
    assert cfgenv.attrs == attrs

    _hash = utils.hashFiles(cfgenv.monitfiles)
    assert cfgenv.monithash == _hash

    assert 'toolenvs' in cfgenv
    tvars = ToolchainVars
    envVarNames = tvars.allSysFlagVars() + tvars.allSysVarsToSetToolchain()
    for name in envVarNames:
        assert name in cfgenv.toolenvs

def testMakeTasksCachePath():
    buildtype, zmcachedir = ('somename', 'somedir')
    path = assist.makeTasksCachePath(zmcachedir, buildtype)
    assert path == joinpath(zmcachedir, "%s.tasks" % buildtype)
