@mark.django_db
def test_update(client_with_perms):
    client = client_with_perms("testapp.change_dragonfly")
    alpha = Dragonfly.objects.create(name="alpha", age=47)
    response = client.get(DragonflyViewSet().links["update"].reverse(alpha))
    assert b"alpha" in response.content
    assert "form" in response.context
    assert response.context["form"]["name"].value() == "alpha"

