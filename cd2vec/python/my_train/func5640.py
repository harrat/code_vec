def test_make_p2wpkh_output(self):
        self.assertEqual(
            tb.make_p2wpkh_output(
                value=helpers.P2PKH1['human']['outs'][0]['value'],
                pubkey=helpers.PK['ser'][0]['pk']),
            helpers.PK['ser'][0]['pk_p2wpkh_output'])
