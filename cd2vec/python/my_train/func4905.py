def test_delete_attribute_with_missing_current_attribute_reference(self):
        """
        Test that an InvalidMessage error is raised when attempting to delete
        an attribute without specifying a current attribute or an attribute
        reference (under KMIP 2.0+).
        """
        e = engine.KmipEngine()
        e._protocol_version = contents.ProtocolVersion(2, 0)
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        args = (
            payloads.DeleteAttributeRequestPayload(unique_identifier="1"),
        )

        self.assertRaisesRegex(
            exceptions.InvalidMessage,
            "The DeleteAttribute request must specify the current attribute "
            "or an attribute reference.",
            e._process_delete_attribute,
            *args
        )
