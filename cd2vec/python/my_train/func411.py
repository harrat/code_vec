def test_scale_ground_survey():
    """
    Test if synthetic ground survey returns the expected survey after scaled
    """
    region = (-10.1, 9.7, -20.3, -10.5)  # a random region to scale the survey
    survey = ground_survey(region=region)
    assert set(survey.columns) == set(["longitude", "latitude", "height"])
    assert survey.longitude.size == 963
    npt.assert_allclose(survey.longitude.min(), region[0])
    npt.assert_allclose(survey.longitude.max(), region[1])
    npt.assert_allclose(survey.latitude.min(), region[2])
    npt.assert_allclose(survey.latitude.max(), region[3])
    npt.assert_allclose(survey.height.min(), 0.0)
    npt.assert_allclose(survey.height.max(), 2052.2)

