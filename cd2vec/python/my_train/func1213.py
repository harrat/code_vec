def test_morph():
    """
    Check values for a randomly chosen galaxy.
    """
    curdir = os.path.dirname(__file__)
    hdulist = fits.open('%s/data_slice.fits' % (curdir))
    image = hdulist[0].data
    segmap = hdulist[1].data
    mask = np.bool8(hdulist[2].data)
    gain = 1.0
    source_morphs = statmorph.source_morphology(image, segmap, mask=mask, gain=gain)
    morph = source_morphs[0]

    #for key in correct_values:
    #    print("'%s':%.14f," % (key, morph[key]))

    # Check results
    assert morph['flag'] == 0
    assert morph['flag_sersic'] == 0
    for key in correct_values:
        value = morph[key]
        value0 = correct_values[key]
        relative_error = np.abs((value - value0) / value0)
        assert relative_error < 1e-6

def runall():
    """
    Run all tests.
    """
    start = time.time()
    print('Running statmorph tests...')
    test_morph()
    print('Time: %g s.' % (time.time() - start))
    print('All tests finished successfully.')
