def test_write_atomic(tmpdir, binary_file):
    """Test wkr.os.write_atomic."""
    binary_path = tmpdir.__class__(binary_file)
    assert tmpdir.exists()
    assert binary_path.exists()
    assert binary_path.size() > 0
    original_contents = binary_path.read(mode='rb')
    # backup file
    backup_path = binary_path.new(basename=binary_path.basename + '~')
    assert not backup_path.exists()
    # now do the atomic write
    new_contents = b'abcde'
    assert new_contents != original_contents
    write_atomic([new_contents], binary_file)
    # assert that the backup_path exists and that it contains the same
    # content as the original file
    assert backup_path.exists()
    assert backup_path.read(mode='rb') == original_contents
    # assert that the original file exists, with different contents
    assert binary_path.exists()
    assert binary_path.read(mode='rb') == new_contents

