def test_call_audit_results_prints_output(self):
    """ test_call_audit_results_prints_output ensures that when called with
    a valid result, audit_results returns the number of vulnerabilities found """
    filename = Path(__file__).parent / "ossindexresponse.txt"
    with open(filename, "r") as stdin:
      response = json.loads(
          stdin.read(),
          cls=ResultsDecoder)
    self.assertEqual(self.func.audit_results(response),
                     self.expected_results())
