def test_1_leaves():
    update(record=t_1)
    assert tree.rootHash == hash(t_1)

def test_2_leaves():
    update(record=t_2)
    assert tree.rootHash == hash(
        hash(t_1),
        hash(t_2)
    )
