def test_multiple_deletion(self):
        ''' verify (insert a, insert a, delete a, delete a) vs. () '''

        ''' record data structure state '''
        kvs = dict(self.kvs)
        ref_kvs = dict(self.ref_kvs)

        ''' insert and delete contents of lengths of different orders of magnitude '''
        for chunking_levels in range(1, 8):
            content_length = self.random.randint(
                int(self.S * (self.S / self.R) ** (chunking_levels - 1)), int(self.S * (self.S / self.R) ** (chunking_levels)))

            content = bytes([self.random.randint(0, 255)
                             for _ in range(content_length)])

            ''' insert and delete immediately afterwards '''
            k = self.seccs.put_content(content)
            k2 = self.seccs.put_content(content)

            self.assertEqual(k, k2)

            self.seccs.delete_content(k)
            self.seccs.delete_content(k2)

            self.assertEqual(self.kvs, kvs)
            self.assertEqual(self.ref_kvs, ref_kvs)
