def test_mlb_first_game_double_header_info(self):
        fields = {
            'attendance': None,
            'date': 'Monday, July 9, 2018',
            'time': '4:05 p.m. ET',
            'venue': 'Oriole Park at Camden Yards',
            'duration': '2:55',
            'time_of_day': 'Night'
        }

        mock_field = """Monday, July 9, 2018
Start Time: 4:05 p.m. ET
Venue: Oriole Park at Camden Yards
Game Duration: 2:55
Night Game, on grass
First game of doubleheader
"""

        m = MockBoxscoreData(MockField(mock_field))

        self.boxscore._parse_game_date_and_location(m)
        for field, value in fields.items():
            assert getattr(self.boxscore, field) == value
