def test_derive_key(self):
        """
        Test that a DeriveKey request can be processed correctly.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        base_key = pie_objects.SymmetricKey(
            algorithm=enums.CryptographicAlgorithm.HMAC_SHA256,
            length=176,
            value=(
                b'\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b'
                b'\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b'
                b'\x0b\x0b\x0b\x0b\x0b\x0b'
            ),
            masks=[enums.CryptographicUsageMask.DERIVE_KEY]
        )
        e._data_session.add(base_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        attribute_factory = factory.AttributeFactory()

        # Derive a SymmetricKey object.
        payload = payloads.DeriveKeyRequestPayload(
            object_type=enums.ObjectType.SYMMETRIC_KEY,
            unique_identifiers=[str(base_key.unique_identifier)],
            derivation_method=enums.DerivationMethod.HMAC,
            derivation_parameters=attributes.DerivationParameters(
                cryptographic_parameters=attributes.CryptographicParameters(
                    hashing_algorithm=enums.HashingAlgorithm.SHA_256
                ),
                derivation_data=(
                    b'\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf7'
                    b'\xf8\xf9'
                ),
                salt=(
                    b'\x00\x01\x02\x03\x04\x05\x06\x07'
                    b'\x08\x09\x0a\x0b\x0c'
                )
            ),
            template_attribute=objects.TemplateAttribute(
                attributes=[
                    attribute_factory.create_attribute(
                        enums.AttributeType.CRYPTOGRAPHIC_LENGTH,
                        336
                    ),
                    attribute_factory.create_attribute(
                        enums.AttributeType.CRYPTOGRAPHIC_ALGORITHM,
                        enums.CryptographicAlgorithm.AES
                    )
                ]
            )
        )

        response_payload = e._process_derive_key(payload)

        e._logger.info.assert_any_call("Processing operation: DeriveKey")
        e._logger.info.assert_any_call(
            "Object 1 will be used as the keying material for the derivation "
            "process."
        )
        e._logger.info.assert_any_call("Created a SymmetricKey with ID: 2")

        self.assertEqual("2", response_payload.unique_identifier)

        managed_object = e._data_session.query(
            pie_objects.SymmetricKey
        ).filter(
            pie_objects.SymmetricKey.unique_identifier == 2
        ).one()

        self.assertEqual(
            (
                b'\x3c\xb2\x5f\x25\xfa\xac\xd5\x7a'
                b'\x90\x43\x4f\x64\xd0\x36\x2f\x2a'
                b'\x2d\x2d\x0a\x90\xcf\x1a\x5a\x4c'
                b'\x5d\xb0\x2d\x56\xec\xc4\xc5\xbf'
                b'\x34\x00\x72\x08\xd5\xb8\x87\x18'
                b'\x58\x65'
            ),
            managed_object.value
        )
        self.assertEqual(
            enums.CryptographicAlgorithm.AES,
            managed_object.cryptographic_algorithm
        )
        self.assertEqual(
            336,
            managed_object.cryptographic_length
        )
        self.assertIsNotNone(managed_object.initial_date)

        e._logger.reset_mock()

        base_key = pie_objects.SymmetricKey(
            algorithm=enums.CryptographicAlgorithm.BLOWFISH,
            length=128,
            value=(
                b'\x01\x23\x45\x67\x89\xAB\xCD\xEF'
                b'\xF0\xE1\xD2\xC3\xB4\xA5\x96\x87'
            ),
            masks=[enums.CryptographicUsageMask.DERIVE_KEY]
        )
        e._data_session.add(base_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        # Derive a SecretData object.
        payload = payloads.DeriveKeyRequestPayload(
            object_type=enums.ObjectType.SECRET_DATA,
            unique_identifiers=[str(base_key.unique_identifier)],
            derivation_method=enums.DerivationMethod.ENCRYPT,
            derivation_parameters=attributes.DerivationParameters(
                cryptographic_parameters=attributes.CryptographicParameters(
                    block_cipher_mode=enums.BlockCipherMode.CBC,
                    padding_method=enums.PaddingMethod.PKCS5,
                    hashing_algorithm=enums.HashingAlgorithm.SHA_256,
                    cryptographic_algorithm=(
                        enums.CryptographicAlgorithm.BLOWFISH
                    )
                ),
                initialization_vector=b'\xFE\xDC\xBA\x98\x76\x54\x32\x10',
                derivation_data=(
                    b'\x37\x36\x35\x34\x33\x32\x31\x20'
                    b'\x4E\x6F\x77\x20\x69\x73\x20\x74'
                    b'\x68\x65\x20\x74\x69\x6D\x65\x20'
                    b'\x66\x6F\x72\x20\x00'
                ),
            ),
            template_attribute=objects.TemplateAttribute(
                attributes=[
                    attribute_factory.create_attribute(
                        enums.AttributeType.CRYPTOGRAPHIC_LENGTH,
                        256
                    )
                ]
            )
        )

        response_payload = e._process_derive_key(payload)

        e._logger.info.assert_any_call("Processing operation: DeriveKey")
        e._logger.info.assert_any_call(
            "Object 3 will be used as the keying material for the derivation "
            "process."
        )
        e._logger.info.assert_any_call("Created a SecretData with ID: 4")

        self.assertEqual("4", response_payload.unique_identifier)

        managed_object = e._data_session.query(
            pie_objects.SecretData
        ).filter(
            pie_objects.SecretData.unique_identifier == 4
        ).one()

        self.assertEqual(
            (
                b'\x6B\x77\xB4\xD6\x30\x06\xDE\xE6'
                b'\x05\xB1\x56\xE2\x74\x03\x97\x93'
                b'\x58\xDE\xB9\xE7\x15\x46\x16\xD9'
                b'\x74\x9D\xEC\xBE\xC0\x5D\x26\x4B'
            ),
            managed_object.value
        )
        self.assertEqual(enums.SecretDataType.SEED, managed_object.data_type)
        self.assertIsNotNone(managed_object.initial_date)
