def test_locate_with_initial_date(self):
        """
        Test the Locate operation when 'Initial Date' attributes are given.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        key = (
            b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        )

        obj_a = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            128,
            key,
            name='name1'
        )
        obj_a.initial_date = int(time.time())
        obj_a_time_str = time.strftime(
            "%a %b %-2d %H:%M:%S %Y",
            time.gmtime(obj_a.initial_date)
        )

        time.sleep(2)
        mid_time = int(time.time())
        mid_time_str = time.strftime(
            "%a %b %-2d %H:%M:%S %Y",
            time.gmtime(mid_time)
        )
        time.sleep(2)

        obj_b = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.DES,
            128,
            key,
            name='name2'
        )
        obj_b.initial_date = int(time.time())
        obj_b_time_str = time.strftime(
            "%a %b %-2d %H:%M:%S %Y",
            time.gmtime(obj_b.initial_date)
        )

        time.sleep(2)
        end_time = int(time.time())

        e._data_session.add(obj_a)
        e._data_session.add(obj_b)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        id_a = str(obj_a.unique_identifier)
        id_b = str(obj_b.unique_identifier)

        attribute_factory = factory.AttributeFactory()

        # Locate the object with a specific timestamp
        attrs = [
            attribute_factory.create_attribute(
                enums.AttributeType.INITIAL_DATE,
                obj_a.initial_date
            )
        ]
        payload = payloads.LocateRequestPayload(attributes=attrs)
        e._logger.reset_mock()
        response_payload = e._process_locate(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call("Processing operation: Locate")
        e._logger.debug.assert_any_call(
            "Failed match: object's initial date ({}) does not match "
            "the specified initial date ({}).".format(
                obj_b_time_str,
                obj_a_time_str
            )
        )
        e._logger.debug.assert_any_call(
            "Locate filter matched object: {}".format(id_a)
        )
        self.assertEqual(len(response_payload.unique_identifiers), 1)
        self.assertIn(id_a, response_payload.unique_identifiers)

        # Locate an object with a timestamp range
        attrs = [
            attribute_factory.create_attribute(
                enums.AttributeType.INITIAL_DATE,
                mid_time
            ),
            attribute_factory.create_attribute(
                enums.AttributeType.INITIAL_DATE,
                end_time
            )
        ]
        payload = payloads.LocateRequestPayload(attributes=attrs)
        e._logger.reset_mock()
        response_payload = e._process_locate(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call("Processing operation: Locate")
        e._logger.debug.assert_any_call(
            "Failed match: object's initial date ({}) is less than "
            "the starting initial date ({}).".format(
                obj_a_time_str,
                mid_time_str
            )
        )
        e._logger.debug.assert_any_call(
            "Locate filter matched object: {}".format(id_b)
        )
        self.assertEqual(len(response_payload.unique_identifiers), 1)
        self.assertIn(id_b, response_payload.unique_identifiers)
