def test_set_item_fail(json_file):
    new_item = {'uuid': "1144d69e-joya-33e8-bdfd-680688cce955",
                'price': 333.0,
                'name': "Test Product via set_item"
                }
    results = atomic.set_item(str(json_file), new_item)
    assert not results

