@pytest.mark.remote
def test_fetch_json():
    url = 'https://www.gw-openscience.org/archive/1126257414/1126261510/json/'
    out = api.fetch_json(url)
    assert isinstance(out, dict)
    assert len(out['events']) == 3
    assert sorted(out['events']['GW150914-v1']['detectors']) == ['H1', 'L1']
    assert set(out['runs'].keys()).issubset(
        {'tenyear', 'O1', 'O1_16KHZ', 'history'},
    )

    # check errors (use legit URL that isn't JSON)
    url2 = os.path.dirname(os.path.dirname(url))
    with pytest.raises(ValueError) as exc:
        api.fetch_json(url2)
    assert str(exc.value).startswith(
        "Failed to parse LOSC JSON from {!r}: ".format(url2))
