def test_update_app_validation(self, mock_sp, client: AppsModule, shared):
        app = shared['app']  # type: AppModel

        with pytest.raises(ValidationError):
            client.update_app(app.id, domains="website.com")  # invalid parameter type

        mock_sp.return_value = AppMock('update_app')
        response = client.update_app(app.id, domains=['www.myshop.com', 'myshop.com'], runtime='php7.1')

        assert response.id == app.id
        assert response.runtime == 'php7.2'
