def test_new_task_unknown(self):
        """
        Cannot create a new task from an unknown name. It must raise a KeyError
        exception (just like `TaskRegistry.get`).
        """
        with self.assertRaises(UnknownTaskName):
            new_task('dummy')
