def test_writer_writes_to_file(self):
        fname = '/tmp/testobj{}.obj'.format(uuid4())
        with open(fname, 'w') as f:
            self.writer.dump(f)
        self.assertTrue(os.path.exists(fname))
