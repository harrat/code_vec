def test_clear_rewrite_rules(self):
        self.client.set_rewrite_rules([
            (r'https://stackoverflow.com(.*)', r'https://www.github.com\1'),
        ])
        self.client.clear_rewrite_rules()

        self._make_request('https://www.stackoverflow.com/')

        last_request = self.client.get_last_request()

        self.assertEqual('https://stackoverflow.com/', last_request['url'])
        self.assertEqual('stackoverflow.com', last_request['headers']['Host'])
