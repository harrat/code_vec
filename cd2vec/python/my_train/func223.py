def test_guard_change_two_text_files_multiple_arguments(self):
        @guard(TestFileGuardDecoratorMultipleFiles.TEST_TEXT_FILE_1_PATH, TestFileGuardDecoratorMultipleFiles.TEST_TEXT_FILE_2_PATH)
        def function_that_changes_the_file():
            lines_to_write = ['of course\n', 'I would\n']

            with open(self.TEST_TEXT_FILE_1_PATH, 'w') as file:
                file.writelines(lines_to_write)

            with open(self.TEST_TEXT_FILE_2_PATH, 'w') as file:
                file.writelines(lines_to_write)

            self._assert_file_content_equals(self.TEST_TEXT_FILE_1_PATH, lines_to_write)
            self._assert_file_content_equals(self.TEST_TEXT_FILE_2_PATH, lines_to_write)


        self._assert_file_content_equals(self.TEST_TEXT_FILE_1_PATH, self.TEST_FILE_1_CONTENTS)
        self._assert_file_content_equals(self.TEST_TEXT_FILE_2_PATH, self.TEST_FILE_2_CONTENTS)

        function_that_changes_the_file()


        self._assert_file_content_equals(self.TEST_TEXT_FILE_2_PATH, self.TEST_FILE_2_CONTENTS)
        self._assert_file_content_equals(self.TEST_TEXT_FILE_1_PATH, self.TEST_FILE_1_CONTENTS)
