def test_make_pkh_output_script(self):
        self.assertEqual(
            tb.make_pkh_output_script(helpers.PK['ser'][0]['pk']),
            helpers.PK['ser'][0]['pkh_output'])
        self.assertEqual(
            tb.make_pkh_output_script(
                helpers.PK['ser'][0]['pk'],
                witness=True),
            helpers.PK['ser'][0]['pkh_p2wpkh_output'])

        riemann.select_network('bitcoin_cash_main')
        with self.assertRaises(ValueError) as context:
            tb.make_pkh_output_script(helpers.PK['ser'][0]['pk'], witness=True)

        self.assertIn(
            'Network bitcoin_cash_main does not support witness scripts.',
            str(context.exception))

        with self.assertRaises(ValueError) as context:
            tb.make_pkh_output_script('hello world')
        self.assertIn(
            'Unknown pubkey format. Expected bytes. Got: ',
            str(context.exception))
