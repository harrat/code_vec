def test_to_idna_multiple_urls(self):
        """
        Runs and tests Converter.
        """

        domains_to_test = [
            "http://b?llogram.com",
            "https://bittr�?.com/path;parameters?query#fragment",
            "ftp://cryptopi?.com",
            "git://coinb?se.com",
            "://coinb?se.com/hello_world",
        ]

        expected = [
            "http://xn--bllogram-g80d.com",
            "https://xn--bittr-fsa6124c.com/path;parameters?query#fragment",
            "ftp://xn--cryptopi-ux0d.com",
            "git://xn--coinbse-30c.com",
            "://xn--coinbse-30c.com/hello_world",
        ]
        actual = Converter(domains_to_test).get_converted()

        self.assertEqual(expected, actual)
