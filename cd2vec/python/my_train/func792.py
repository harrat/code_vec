def test_supports_environ(hellotest, generic_system):
    hellotest.valid_prog_environs = ['*']
    assert hellotest.supports_environ('foo1')
    assert hellotest.supports_environ('foo-env')
    assert hellotest.supports_environ('*')

