def test_run_vad(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        audiofile.run_vad()
        self.assertIsNotNone(audiofile.masked_mfcc)
        self.assertIsNotNone(audiofile.masked_map)
        self.assertNotEqual(audiofile.masked_length, 0)
        self.assertIsNotNone(audiofile.masked_middle_mfcc)
        self.assertNotEqual(audiofile.masked_middle_length, 0)
        self.assertIsNotNone(audiofile.masked_middle_map)
