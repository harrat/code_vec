def test_unsupported_patch_resource(self):
        """Test PATCHing a resource for an endpoint that doesn't support it."""
        response = self.app.patch('/styles/26',
                content_type='application/json',
                data=json.dumps({u'Name': u'Hip-Hop'}))
        assert response.status_code == 403
