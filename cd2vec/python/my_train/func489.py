def test_scale(self):
        filepath = self.random_profile_file()
        scale = 2.25
        p1 = Profile(filepath)
        p2 = Profile(filepath)
        p2 = Profile.scale(p2, scale)
        for i in range(len(p1.top.coordinates)):
            assert p2.top.coordinates[i] == p1.top.coordinates[i] * scale
        for i in range(len(p1.bottom.coordinates)):
            assert p2.bottom.coordinates[i] == p1.bottom.coordinates[i] * scale
        p3 = p1*scale
        assert p2 == p3
