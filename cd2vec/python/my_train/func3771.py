def test_does_not_raise_exception_when_subscribing_secure_channel(
            self,
            mocker):
        header_mock = mocker.patch(
            'bitmex_websocket._bitmex_websocket.BitMEXWebsocket.header')
        header_mock.return_value = []

        channels = [
            Channels.connected, SecureChannels.margin]

        instrument = Instrument(channels=channels, should_auth=True)
        instrument.emit('open')

        assert channels == instrument.channels
