def test_list_range(self):
        ontology = self.client.ontology("aero")
        terms = ontology.terms({'size': self.client.page_size})

        slice_terms = terms[23:253]
        term_3 = terms[252]

        self.assertEqual(230, len(slice_terms))
        self.assertGreaterEqual(454, len(terms))
        current = 23
        for term in slice_terms:
            self.assertEqual(term.accession, terms[current].accession)
            current += 1
        self.assertEqual(slice_terms[len(slice_terms) - 1], term_3)
        with self.assertRaises(KeyError):
            error_slice = terms[1:550]
        with self.assertRaises(IndexError):
            current = slice_terms[12555]
        with self.assertRaises(TypeError):
            current = slice_terms['12512']
