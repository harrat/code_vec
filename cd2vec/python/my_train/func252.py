def test_flex_alloc_sched_access_constraint_partition(make_flexible_job):
    job = make_flexible_job(
        'all', sched_access=['--constraint=f1', '--partition=p2']
    )
    prepare_job(job)
    assert job.num_tasks == 4

