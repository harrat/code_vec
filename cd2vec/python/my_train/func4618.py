@pytest.mark.vcr()
def test_workbench_asset_vuln_output_filter_type_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.asset_vuln_output(str(uuid.uuid4()), 19506, filter_type='NOT')
