def test_only_public_channels(self):
        instrument = Instrument(channels=[Channels.connected])

        assert [Channels.connected] == instrument.channels
