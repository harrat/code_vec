def test_check_www_redirect_www(self):
        self.assertEqual(
            "https://www.apple.com/", network.check_www_redirect("https://apple.com/")
        )
