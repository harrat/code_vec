def test_invalid_hatchs(self):
        ''' Invalid hatchs. '''
        with self.assertRaisesRegexp(ValueError, r'\[barchart\] .*hatchs.*'):
            barchart.draw(self.axes, _data(), hatchs=['/', '//', 'xx'])
