@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_callback(fixture, request):
    def _callback(job):
        return job.get_output('out.txt').read().strip()

    engine = request.getfixturevalue(fixture)
    job = engine.launch(image=PYIMAGE,
                        command='echo hello world > out.txt',
                        when_finished=_callback)
    print(job.rundata)
    job.wait()

    assert job.result == 'hello world'

