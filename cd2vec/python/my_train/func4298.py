def test(self):
        # Given a server that supports session resumption with both TLS tickets and session IDs
        server_location = ServerNetworkLocationViaDirectConnection.with_ip_address_lookup("www.facebook.com", 443)
        server_info = ServerConnectivityTester().perform(server_location)

        # When testing for resumption, it succeeds
        result: SessionResumptionSupportScanResult = SessionResumptionSupportImplementation.scan_server(server_info)

        # And it confirms that both session IDs and TLS tickets are supported
        assert result.attempted_session_id_resumptions_count
        assert result.successful_session_id_resumptions_count
        assert result.is_session_id_resumption_supported

        assert result.tls_ticket_resumption_result
        assert result.is_tls_ticket_resumption_supported

        # And a CLI output can be generated
        assert SessionResumptionSupportImplementation.cli_connector_cls.result_to_console_output(result)
