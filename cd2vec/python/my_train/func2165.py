def test_enable_force_ssl(self, mock_sp, client: AppsModule, shared):
        app = shared['app']  # type: AppModel

        with pytest.raises(ValidationError):
            client.set_force_ssl(app.id)

            client.set_force_ssl(app.id, force="yes")  # invalid parameter type

        mock_sp.return_value = AppMock('set_force_ssl')
        response = client.set_force_ssl(app.id, force=True)

        assert response.key == app.ssl.key
        assert response.force is True
