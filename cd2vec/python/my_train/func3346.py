@pytest.mark.vcr()
def test_workbench_asset_vulns_filter_type_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.asset_vulns(str(uuid.uuid4()), filter_type=123)
