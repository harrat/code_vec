def test_find(self):
        level1 = self.root.make_directory()
        level2 = self.root.make_directory(prefix='foo', parent=level1)
        paths = self.root.make_empty_files(3, prefix='bar', parent=level2)

        # recursive
        x = find(level1, 'foo.*', 'd', recursive=True)
        assert 1 == len(x)
        assert level2 == x[0]
        y = find(level1, 'bar.*', 'f', recursive=True)
        assert 3 == len(y)
        assert sorted(paths) == sorted(y)

        # non-recursive
        x = find(level1, 'foo.*', 'd', recursive=False)
        assert 1 == len(x)
        assert level2 == x[0]
        y = find(level1, 'bar.*', 'f', recursive=False)
        assert 0 == len(y)

        # absolute match
        x = find(
            level1, os.path.join(str(level1), 'foo.*', 'bar.*'), 'f',
            recursive=True)
        assert 3 == len(x)
        assert sorted(paths) == sorted(x)

        # fifo
        path = self.root.make_fifo(prefix='baz', parent=level1)
        x = find(level1, 'baz.*', '|')
        assert 1 == len(x)
        assert path == x[0]
