def test_misformed(self):
        with pytest.raises(InvalidIdentifierError) as exc_info:
            api.lookup( api.TYPE_APPLE_SERIAL, 'XXBADSERIALXX', )
