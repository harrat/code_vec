def test_post_no_json_data(self):
        """Test POSTing a resource with no JSON data."""
        response = self.app.post('/artists',
                content_type='application/json',
                data=dict())
        assert response.status_code == 400
