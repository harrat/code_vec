def test_sgd_circles():
    samples = 50
    a = np.random.uniform(0, 2 * np.pi, samples * 2)
    r = np.append(np.random.uniform(0, 10, samples),
                  np.random.uniform(20, 30, samples))
    x = np.matrix([np.multiply(r, np.sin(a)), np.multiply(r, np.cos(a))]).T
    y = np.append(np.ones(samples), np.zeros(samples))
    x, y = Optimizer.shuffle(x, y)
    limit = int(len(x) * 0.8)

    nn = NN(2)
    nn.add_layer(Layer(4, Rectifier))
    nn.add_layer(Layer(1, Sigmoid))
    sgd = SGD(MSE())
    sgd.seed = 42
    sgd.train(nn, x[:limit], y[:limit], 0.1, 5, 100, metrics=[accuracy])
    assert accuracy(nn.predict_batch(x[limit:]), y[limit:]) > 0.7

