def test_TMVN_sampling_non_truncated():
    """
    Test if the sampling method returns appropriate samples for a non-truncated
    univariate and multivariate normal distribution.

    """

    # univariate case
    ref_mean = 0.5
    ref_std = 0.25
    ref_var = ref_std**2.

    sampling_function = lambda sample_size: tmvn_rvs(ref_mean, ref_var,
                                                     size=sample_size)
    assert assert_normal_distribution(sampling_function, ref_mean, ref_var)

    # bivariate case, various correlation coefficients
    rho_list = [-1., -0.5, 0., 0.5, 1.]
    for rho in rho_list:
        ref_mean = [0.5, 1.5]
        ref_std = [0.25, 1.0]
        ref_rho = np.asarray([[1.0, rho],[rho, 1.0]])
        ref_COV = np.outer(ref_std, ref_std) * ref_rho

        sampling_function = lambda sample_size: tmvn_rvs(ref_mean, ref_COV,
                                                         size=sample_size)

        assert assert_normal_distribution(sampling_function, ref_mean, ref_COV)

    # multi-dimensional case
    dims = 5
    ref_mean = np.arange(dims, dtype=np.float64)
    ref_std = np.arange(1,dims+1, dtype=np.float64)
    ref_rho = np.ones((dims,dims))*0.3
    ref_COV = np.outer(ref_std,ref_std) * ref_rho
    np.fill_diagonal(ref_COV,ref_std**2.)

    sampling_function = lambda sample_size: tmvn_rvs(ref_mean, ref_COV,
                                                     size=sample_size)

    assert assert_normal_distribution(sampling_function, ref_mean, ref_COV)

def test_TMVN_sampling_truncated_wide_limits():
    """
    Test if the sampling method returns appropriate samples for a truncated
    univariate and multivariate normal distribution when the truncation limits
    are sufficiently wide to consider the result a normal distribution.
