def test_flex_alloc_valid_partition_opt(make_flexible_job):
    job = make_flexible_job('all')
    job.options = ['-p p2']
    prepare_job(job)
    assert job.num_tasks == 8

