def test_sdf_derivatives():
    verbose = False
    circles = []
    for center, radius in sample_circles(nb_circles=10):
        circles.append(Circle(center, radius))
    for c in circles:
        signed_distance_field = SignedDistance2DMap(c)
        assert check_jacobian_against_finite_difference(
            signed_distance_field, verbose)
        assert check_hessian_against_finite_difference(
            signed_distance_field)
