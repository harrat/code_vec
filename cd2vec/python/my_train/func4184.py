def test_delete_attribute_from_managed_object(self):
        """
        Test that various attributes can be deleted correctly from a given
        managed object.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        attribute_factory = factory.AttributeFactory()

        name_1 = attribute_factory.create_attribute(
            enums.AttributeType.NAME,
            attributes.Name.create(
                "Name 1",
                enums.NameType.UNINTERPRETED_TEXT_STRING
            )
        )
        name_2 = attribute_factory.create_attribute(
            enums.AttributeType.NAME,
            attributes.Name.create(
                "Name 2",
                enums.NameType.UNINTERPRETED_TEXT_STRING
            )
        )
        app_specific_info_1 = attribute_factory.create_attribute(
            enums.AttributeType.APPLICATION_SPECIFIC_INFORMATION,
            {
                "application_namespace": "Namespace 1",
                "application_data": "Data 1"
            }
        )
        app_specific_info_2 = attribute_factory.create_attribute(
            enums.AttributeType.APPLICATION_SPECIFIC_INFORMATION,
            {
                "application_namespace": "Namespace 2",
                "application_data": "Data 2"
            }
        )
        object_group_1 = attribute_factory.create_attribute(
            enums.AttributeType.OBJECT_GROUP,
            "Object Group 1"
        )
        object_group_2 = attribute_factory.create_attribute(
            enums.AttributeType.OBJECT_GROUP,
            "Object Group 2"
        )
        managed_object = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )
        managed_object.names.clear()

        self.assertEqual(0, len(managed_object.names))
        self.assertEqual(0, len(managed_object.app_specific_info))
        self.assertEqual(0, len(managed_object.object_groups))

        e._set_attribute_on_managed_object(
            managed_object,
            (
                "Name",
                [
                    name_1.attribute_value,
                    name_2.attribute_value
                ]
            )
        )
        e._set_attribute_on_managed_object(
            managed_object,
            (
                "Application Specific Information",
                [
                    app_specific_info_1.attribute_value,
                    app_specific_info_2.attribute_value
                ]
            )
        )
        e._set_attribute_on_managed_object(
            managed_object,
            (
                "Object Group",
                [
                    object_group_1.attribute_value,
                    object_group_2.attribute_value
                ]
            )
        )

        self.assertEqual(2, len(managed_object.names))
        self.assertEqual(2, len(managed_object.app_specific_info))
        self.assertEqual(2, len(managed_object.object_groups))

        e._delete_attribute_from_managed_object(
            managed_object,
            (
                "Application Specific Information",
                0,
                None
            )
        )

        self.assertEqual(1, len(managed_object.app_specific_info))

        e._delete_attribute_from_managed_object(
            managed_object,
            (
                "Application Specific Information",
                0,
                app_specific_info_2.attribute_value
            )
        )

        self.assertEqual(0, len(managed_object.app_specific_info))

        e._delete_attribute_from_managed_object(
            managed_object,
            (
                "Name",
                None,
                primitives.TextString(value="Name 2", tag=enums.Tags.NAME)
            )
        )

        self.assertEqual(1, len(managed_object.names))
        self.assertEqual("Name 1", managed_object.names[0])

        e._delete_attribute_from_managed_object(
            managed_object,
            (
                "Object Group",
                None,
                None
            )
        )

        self.assertEqual(0, len(managed_object.object_groups))

        e._set_attribute_on_managed_object(
            managed_object,
            (
                "Object Group",
                [object_group_1.attribute_value]
            )
        )

        self.assertEqual(1, len(managed_object.object_groups))

        e._delete_attribute_from_managed_object(
            managed_object,
            (
                "Object Group",
                None,
                primitives.TextString(
                    value="Object Group 1",
                    tag=enums.Tags.OBJECT_GROUP
                )
            )
        )

        self.assertEqual(0, len(managed_object.object_groups))
