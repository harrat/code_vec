@staticmethod
    @pytest.mark.usefixtures('import-kafka', 'update-three-kafka-utc')
    def test_cli_update_solution_015(snippy, editor_data):
        """Update text native solution with editor.

        Update existing text formatted solution first in text format, then
        in Markdown format and then again in text format without making any
        changes. The content must not change when updated between different
        formats.

        Each update must generate different timestamp in content ``updated``
        attribute.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.KAFKA)
            ]
        }
        content['data'][0]['brief'] = 'Testing docker log drivers again'
        content['data'][0]['digest'] = '1072f9a0ddb2ab15a7f6cca0acd9f7e48903faa576fb19eca4e0ec98dc20c041'
        template = Content.dump_text(content['data'][0])
        editor_data.return_value = template
        cause = snippy.run(['snippy', 'update', '-d', 'ee3f2ab7c63d6965', '--format', 'text', '--brief', 'Testing docker log drivers again'])
        editor_data.assert_called_with(template)
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)

        content['data'][0]['brief'] = 'Testing docker log drivers again in mkdn'
        content['data'][0]['digest'] = '2887f455e73ad3a6040df7299e69548748db5eb208b9c7eb4717aa2527af4778'
        template = Content.dump_mkdn(content['data'][0])
        editor_data.return_value = template
        content['data'][0]['updated'] = '2017-11-20T06:16:27.000001+00:00'
        cause = snippy.run(['snippy', 'update', '-d', '1072f9a0ddb2ab15', '--format', 'mkdn', '--brief', 'Testing docker log drivers again in mkdn'])  # pylint: disable=line-too-long
        editor_data.assert_called_with(template)
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)

        content['data'][0]['brief'] = 'Testing docker log drivers again'
        content['data'][0]['digest'] = '1072f9a0ddb2ab15a7f6cca0acd9f7e48903faa576fb19eca4e0ec98dc20c041'
        template = Content.dump_text(content['data'][0])
        editor_data.return_value = template
        content['data'][0]['updated'] = '2017-12-20T06:16:27.000001+00:00'
        cause = snippy.run(['snippy', 'update', '-d', '2887f455e73ad3a6', '--format', 'text', '--brief', 'Testing docker log drivers again'])
        editor_data.assert_called_with(template)
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
