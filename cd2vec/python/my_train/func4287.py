@mark.django_db
def test_list_sort(client_with_perms):
    client = client_with_perms("testapp.view_dragonfly")
    Dragonfly.objects.create(name="alpha", age=12)
    Dragonfly.objects.create(name="omega", age=99)

    response = client.get(DragonflyViewSet().links["list"].reverse() + "?o=-name")
    assert response.content.index(b"alpha") > response.content.index(b"omega")

    response = client.get(DragonflyViewSet().links["list"].reverse() + "?o=name")
    assert response.content.index(b"alpha") < response.content.index(b"omega")

