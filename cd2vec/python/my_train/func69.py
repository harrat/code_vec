def test_unsupported_collection_method(self):
        """Test POSTing a collection for an endpoint that doesn't support it."""
        response = self.app.post('/styles',
                content_type='application/json',
                data=json.dumps({u'Name': u'Jeff Knupp'}))
        assert response.status_code == 403
