def test_cached(region):
    called = [0]

    @region.cached
    def foobar(k, x=None):
        called[0] += 1
        return k

    foobar(1)
    assert called[0] == 1
    foobar(1)
    assert called[0] == 1

