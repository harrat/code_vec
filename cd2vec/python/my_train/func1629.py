def test_orbElem2vec_Nbody(self):
        #generate n bodies at random phases
        n = int(1e4)
        a = np.random.rand(n)*100
        e = np.random.rand(n)
        O = np.random.rand(n)*2*np.pi
        I = np.random.rand(n)*np.pi
        w = np.random.rand(n)*2*np.pi
        E = np.random.rand(n)*2*np.pi
        mus = np.random.rand(n)

        #convert back and forth to vectors
        rs,vs,ABs = orbElem2vec(E,mus,orbElem=(a,e,O,I,w),returnAB=True)
        a1,e1,E1,O1,I1,w1,P1,tau1 = vec2orbElem(rs,vs,mus)

        tol = np.spacing(np.max(np.abs(np.linalg.norm(rs,axis=0))))*100000

        #check that you got back what you put in
        self.assertTrue(np.max(np.abs(a - a1))<tol)
        self.assertTrue(np.max(np.abs(e - e1))<tol)
        self.assertTrue(np.max(np.abs(O - O1))<tol)
        self.assertTrue(np.max(np.abs(w - w1))<tol)
        self.assertTrue(np.max(np.abs(I - I1))<tol)
        self.assertTrue(np.max(np.abs(E - E1))<tol)

        #convert back and forth again using A,B
        rs,vs = orbElem2vec(E,mus,AB=ABs)
        a1,e1,E1,O1,I1,w1,P1,tau1 = vec2orbElem(rs,vs,mus)

        self.assertTrue(np.max(np.abs(a - a1))<tol)
        self.assertTrue(np.max(np.abs(e - e1))<tol)
        self.assertTrue(np.max(np.abs(O - O1))<tol)
        self.assertTrue(np.max(np.abs(w - w1))<tol)
        self.assertTrue(np.max(np.abs(I - I1))<tol)
        self.assertTrue(np.max(np.abs(E - E1))<tol)
