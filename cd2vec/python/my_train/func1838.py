def test_progress_delmited(self):
        progress = MockProgress()
        xphyle.configure(progress=True, progress_wrapper=progress)
        path = self.root.make_file()
        with open(path, 'wt') as o:
            for i in range(100):
                o.write('row\t{}\n'.format(i))
        rows = list(read_delimited(path))
        assert 100 == len(rows)
        assert 100 == progress.count
