def test_retries_good_check(make_runner, make_cases, common_exec_ctx):
    runner = make_runner(max_retries=2)
    runner.runall(make_cases([HelloTest()]))

    # Ensure that the test passed without retries.
    assert 1 == runner.stats.num_cases()
    assert_runall(runner)
    assert 0 == rt.runtime().current_run
    assert 0 == len(runner.stats.failures())

