def test_Commandline_environ(monkeypatch, tmpdir):
    from nipype import config

    config.set_default_config()

    tmpdir.chdir()
    monkeypatch.setitem(os.environ, "DISPLAY", ":1")
    # Test environment
    ci3 = nib.CommandLine(command="echo")
    res = ci3.run()
    assert res.runtime.environ["DISPLAY"] == ":1"

    # Test display_variable option
    monkeypatch.delitem(os.environ, "DISPLAY", raising=False)
    config.set("execution", "display_variable", ":3")
    res = ci3.run()
    assert "DISPLAY" not in ci3.inputs.environ
    assert "DISPLAY" not in res.runtime.environ

    # If the interface has _redirect_x then yes, it should be set
    ci3._redirect_x = True
    res = ci3.run()
    assert res.runtime.environ["DISPLAY"] == ":3"

    # Test overwrite
    monkeypatch.setitem(os.environ, "DISPLAY", ":1")
    ci3.inputs.environ = {"DISPLAY": ":2"}
    res = ci3.run()
    assert res.runtime.environ["DISPLAY"] == ":2"

