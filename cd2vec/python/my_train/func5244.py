def test_latex_compiler_works(latex_compiler, app, minimal_tex):
    response = latex_compiler.compile('test', minimal_tex)
    assert 'application/pdf' in response.headers['Content-Type']
    assert response.response.file.raw.read().startswith(b'%PDF')

