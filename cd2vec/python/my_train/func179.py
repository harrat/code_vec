@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_api_search_solution_012(server):
        """Search solution with digets.

        Send GET /api/snippy/rest/solutions/{id} to get explicit solution
        based on digest. In this case the solution is found.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '2279'
        }
        expect_body = {
            'meta': {
                'count': 1,
                'limit': 20,
                'offset': 0,
                'total': 1
            },
            'data': {
                'type': 'solution',
                'id': Solution.BEATS_UUID,
                'attributes': Storage.ebeats
            },
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/solutions/' + Solution.BEATS_UUID
            }
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/solutions/4346ba4c79247430',
            headers={'accept': 'application/json'})
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
