def test_verbosity(run_reframe):
    returncode, stdout, stderr = run_reframe(
        more_options=['-vvvvv'],
        system='testsys',
        action='list'
    )
    assert stdout != ''
    assert 'Traceback' not in stdout
    assert 'Traceback' not in stderr
    assert returncode == 0

