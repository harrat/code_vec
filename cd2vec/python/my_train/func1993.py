@staticmethod
    @pytest.mark.usefixtures('yaml', 'default-solutions', 'export-time')
    def test_cli_export_solution_011(snippy):
        """Export defined solution with digest.

        Export defined solution based on message digest to yaml file without
        specifying the content category explicitly.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Solution.BEATS
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-d', '4346ba4c79247430', '-f', './defined-solution.yml'])
            assert cause == Cause.ALL_OK
            Content.assert_yaml(yaml, mock_file, './defined-solution.yml', content)
