def test_app_config_install_patch(unknown_host):
    """Tests whether the Django app config properly installs the retrier and
    retries connection failures."""

    try:
        patch = DBConnectionRetrierConfig.ready(None)
