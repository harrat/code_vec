def test_show_inactive_project_shows_project(cli, projects_db):
    lines = cli('project', ['show', '42']).splitlines()
    assert lines[0] == 'Id: 42'

