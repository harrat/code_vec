def test(self):
        t = task.Task(self.task_name, self.task)
        self.assertTrue(type(t.match) == ruler.OrRule)
        self.assertTrue(t.match.match("/tmp/b.txt"))
        self.assertTrue(t.match.match("b.txt"))
        self.assertEqual(t.collect(os.path.dirname(self.target)), [])

        self.assertTrue(utils.compare_struct_tree_and_dirs(
            self.dst_struct, self.target))
