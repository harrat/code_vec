@pytest.mark.http
def test_get_images_real_http():
    """
    Get_address should return the property images (real HTTP request)
    """
    booking = Booking()
    booking.url = PROPERTY_URL
    booking.get_parser()
    images = booking.get_images()
    assert PROPERTY_IMAGE in images[0]

