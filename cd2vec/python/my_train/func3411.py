def test_summary_power(self):
        tolerance = 4e-2
        consumer = ChainConsumer()
        data = np.random.normal(loc=0, scale=np.sqrt(2), size=1000000)
        consumer.add_chain(data, power=2.0)
        summary = consumer.analysis.get_summary()
        actual = np.array(list(summary.values())[0])
        expected = np.array([-1.0, 0.0, 1.0])
        diff = np.abs(expected - actual)
        assert np.all(diff < tolerance)
