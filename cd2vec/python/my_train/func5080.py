@staticmethod
    @pytest.mark.usefixtures('import-kafka-mkdn', 'update-beats-utc')
    def test_cli_update_solution_012(snippy, editor_data):
        """Update solution with editor.

        Update existing Markdown native solution. Editor must show existing
        Markdown native content as is. Updated content must be identified as
        Markdown native content. Editor must be used by default when the
        ``--editor`` option is not used.

        In this case the links must be empty when stored. The edited content
        did not have any links in the content data so they must be updated
        to stored content.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.KAFKA_MKDN)
            ]
        }
        edited = (
            '# Testing docker log drivers @docker',
            '',
            '> Investigate docker log drivers and the logs2kafka log plugin',
            '',
            '> [1] https://github.com/MickayG/moby-kafka-logdriver  ',
            '[2] https://github.com/garo/logs2kafka  ',
            '[3] https://groups.google.com/forum/#!topic/kubernetes-users/iLDsG85exRQ',
            '',
            '## Description',
            '',
            'Investigate docker log drivers.',
            '',
            '## Solutions',
            '',
            '## Whiteboard',
            '',
            '## Meta',
            '',
            '> category  : solution  ',
            'created   : 2019-01-04T10:54:49.265512+00:00  ',
            'digest    : c54c8a896b94ea35edf6c798879957419d26268bd835328d74b19a6e9ce2324d  ',
            'filename  : kubernetes-docker-log-driver-kafka.mkdn  ',
            'languages :  ',
            'name      :  ',
            'source    :  ',
            'tags      : docker,driver,kafka,kubernetes,logging,logs2kafka,moby,plugin  ',
            'updated   : 2019-01-05T10:54:49.265512+00:00  ',
            'uuid      : 24cd5827-b6ef-4067-b5ac-3ceac07dde9f  ',
            'versions  :  ',
            '')
        updates = content
        updates['data'][0]['data'] = (
            '## Description',
            '',
            'Investigate docker log drivers.',
            '',
            '## Solutions',
            '',
            '## Whiteboard',
            ''
        )
        updates['data'][0]['description'] = 'Investigate docker log drivers and the logs2kafka log plugin'
        updates['data'][0]['links'] = ()
        updates['data'][0]['updated'] = Content.BEATS_TIME
        updates['data'][0]['uuid'] = Solution.KAFKA_MKDN_UUID
        updates['data'][0]['digest'] = '7941851522a23d3651f223b6d69441f77649ccb7ae1e72c6709890f2caf6401a'
        editor_data.return_value = '\n'.join(edited)
        cause = snippy.run(['snippy', 'update', '-d', 'c54c8a896b94ea35', '--scat', 'solution'])
        editor_data.assert_called_with(Content.dump_mkdn(Solution.KAFKA_MKDN))
        assert cause == Cause.ALL_OK
        Content.assert_storage(updates)
