def test_if_error_is_added_to_report(handler_with_exception, mock_context, mock_event):
    ConfigProvider.set(config_names.THUNDRA_APPLICATION_STAGE, 'dev')
    thundra, handler = handler_with_exception

    invocation_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, InvocationPlugin):
            invocation_plugin = plugin

    try:
        handler(mock_event, mock_context)
    except Exception:
        pass

    assert invocation_plugin.invocation_data['erroneous'] is True
    assert invocation_plugin.invocation_data['errorType'] == 'Exception'
    assert invocation_plugin.invocation_data['errorMessage'] == 'hello'

