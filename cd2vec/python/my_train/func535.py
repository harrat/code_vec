@staticmethod
    @pytest.mark.usefixtures('edit-reference-template')
    def test_cli_create_reference_003(snippy):
        """Try to create reference from CLI.

        Try to create new reference without any changes to the reference
        template.
        """

        cause = snippy.run(['snippy', 'create', '--editor', '--format', 'text'])
        assert cause == 'NOK: content was not stored because it was matching to an empty template'
        Content.assert_storage(None)
