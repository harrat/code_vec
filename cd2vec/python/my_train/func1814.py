def test_load_counter(tmpdir):
    """Test the wkr.io.load_counter method."""
    for size in [0, 1, 10, 100, 1000]:
        # produce a Counter dictionary with random counts
        counter = Counter()
        for key in range(size):
            key = 'key{}'.format(key)
            count = random.randint(0, 10000)
            counter[key] = count
        assert len(counter) == size
        # write the counter out to file
        filename = tmpdir.join('counts.tsv').ensure().strpath
        with open(filename, 'wb') as output_file:
            for (key, count) in counter.items():
                output_file.write(
                    u'{}\t{}\n'.format(count, key).encode('utf-8'))
        # read it back in
        assert wkr.io.load_counter(filename) == counter

