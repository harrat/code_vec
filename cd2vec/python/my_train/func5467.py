@patch('filestack.uploads.intelligent_ingestion.requests.put')
@patch('filestack.uploads.intelligent_ingestion.requests.post')
def test_upload_part_success(post_mock, put_mock):
    post_mock.side_effect = [
        DummyHttpResponse(json_dict={'url': 'http://upload.url', 'headers': {'upload': 'headers'}}),
        DummyHttpResponse()
    ]

    put_mock.return_value = DummyHttpResponse()

    part = {'seek_point': 0, 'num': 1}
    upload_part(
        'Aaaaapikey', 'file.txt', 'tests/data/doom.mp4', 1234, 's3', defaultdict(lambda: 'fs-upload.com'), part
    )
    assert post_mock.call_args_list == [
        call(
            'https://fs-upload.com/multipart/upload',
            json={
                'apikey': 'Aaaaapikey', 'uri': 'fs-upload.com', 'region': 'fs-upload.com',
                'upload_id': 'fs-upload.com', 'store': {'location': 's3'},
                'part': 1, 'size': 5415034, 'md5': 'IuNjhgPo2wbzGFo6f7WhUA==', 'offset': 0, 'fii': True
            },
        ),
        call(
            'https://fs-upload.com/multipart/commit',
            json={
                'apikey': 'Aaaaapikey', 'uri': 'fs-upload.com', 'region': 'fs-upload.com',
                'upload_id': 'fs-upload.com', 'store': {'location': 's3'}, 'part': 1, 'size': 1234
            },
        )
    ]
    put_mock.assert_called_once_with(
        'http://upload.url',
        data=ANY,
        headers={'upload': 'headers'}
    )
