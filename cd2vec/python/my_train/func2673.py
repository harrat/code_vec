@mock.patch("IPython.display.display")
def test_scraps_report_no_headers(mock_display, notebook_collection):
    notebook_collection.scraps_report(headers=None)
    mock_display.assert_has_calls(
        [
            mock.call({"text/plain": "'Hello World!'"}, metadata={}, raw=True),
            mock.call({"text/plain": "'Just here!'"}, metadata={}, raw=True),
            mock.call({"text/plain": "'Hello World 2!'"}, metadata={}, raw=True),
            mock.call({"text/plain": "'Just here!'"}, metadata={}, raw=True),
        ]
    )
