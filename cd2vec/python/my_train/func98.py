def test_anyset_init3_add(self):
        items = [1, 2, {1}, 4, 4, {1}]
        result = AnySet()
        for item in items:
            result.add(item)
        expected = ({1, 2, 4}, {6008617170096987129613061240357171817: {1}})
        assert expected == result
