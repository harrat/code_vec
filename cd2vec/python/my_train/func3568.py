@mock.patch('kmip.services.server.auth.get_certificate_from_connection')
    @mock.patch('kmip.core.messages.messages.RequestMessage')
    def test_handle_message_loop(self, request_mock, cert_mock):
        """
        Test that the correct logging and error handling occurs during the
        message handling loop.
        """
        data = utils.BytearrayStream()

        # Build a response and use it as a dummy processing result.
        batch_item = messages.ResponseBatchItem(
            result_status=contents.ResultStatus(
                enums.ResultStatus.SUCCESS
            ),
            result_reason=contents.ResultReason(
                enums.ResultReason.OBJECT_ARCHIVED
            ),
            result_message=contents.ResultMessage("Test message.")
        )
        batch_items = [batch_item]
        header = messages.ResponseHeader(
            protocol_version=contents.ProtocolVersion(1, 0),
            time_stamp=contents.TimeStamp(int(time.time())),
            batch_count=contents.BatchCount(len(batch_items))
        )
        message = messages.ResponseMessage(
            response_header=header,
            batch_items=batch_items
        )

        cert_mock.return_value = 'test_certificate'
        kmip_engine = engine.KmipEngine()
        kmip_engine._logger = mock.MagicMock()
        kmip_session = session.KmipSession(
            kmip_engine,
            None,
            None,
            name='name',
            enable_tls_client_auth=False
        )
        kmip_session._engine = mock.MagicMock()
        kmip_session.authenticate = mock.MagicMock()
        kmip_session.authenticate.return_value = (
            'test',
            ['group A', 'group B']
        )
        kmip_session._engine.process_request = mock.MagicMock(
            return_value=(
                message,
                kmip_session._max_response_size,
                contents.ProtocolVersion(1, 2)
            )
        )
        kmip_session._logger = mock.MagicMock()
        kmip_session._connection = mock.MagicMock()
        kmip_session._connection.shared_ciphers = mock.MagicMock(
            return_value=[
                ('AES128-SHA256', 'TLSv1/SSLv3', 128),
                ('AES256-SHA256', 'TLSv1/SSLv3', 256)
            ]
        )
        kmip_session._connection.cipher = mock.MagicMock(
            return_value=('AES128-SHA256', 'TLSv1/SSLv3', 128)
        )
        kmip_session._receive_request = mock.MagicMock(return_value=data)
        kmip_session._send_response = mock.MagicMock()
        kmip_session.authenticate = mock.MagicMock(
            return_value=("John Doe", ["Group A"])
        )

        kmip_session._handle_message_loop()

        kmip_session._receive_request.assert_called_once_with()
        kmip_session._logger.info.assert_any_call(
            "Session client identity: John Doe"
        )
        kmip_session._logger.debug.assert_any_call(
            "Possible session ciphers: 2"
        )
        kmip_session._logger.debug.assert_any_call(
            ('AES128-SHA256', 'TLSv1/SSLv3', 128)
        )
        kmip_session._logger.debug.assert_any_call(
            ('AES256-SHA256', 'TLSv1/SSLv3', 256)
        )
        kmip_session._logger.debug.assert_any_call(
            "Session cipher selected: {0}".format(
                ('AES128-SHA256', 'TLSv1/SSLv3', 128)
            )
        )
        kmip_session._logger.warning.assert_not_called()
        kmip_session._logger.exception.assert_not_called()
        self.assertTrue(kmip_session._send_response.called)
