def test_modified_before_created(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['modified'] = "2001-04-06T20:03:48Z"
        results = validate_parsed_json(indicator, self.options)
        self.assertEqual(results.is_valid, False)
