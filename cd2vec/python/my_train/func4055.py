def test_get_show_credits(empty_person: Person):
    credits = empty_person.get_show_credits()
    assert credits == empty_person.data["tv_credits"]

