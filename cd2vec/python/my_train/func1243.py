def test_config():
    global dir
    a = aud.Dir(dir)

    print("SETTING EXTENSIONS")
    assert a.config_set_extensions(["txt"])
    assert a.config_get_extensions() == [".txt"]

    print("SETTING denylist")
    assert a.config_set_denylist(["test.txt"])
    assert a.config_get_denylist() == ["test.txt"]
    assert sorted(a.get_all()) == ["abc.txt"]
    assert a.config_set_allowlist(regex="test.txt")
    assert sorted(a.get_all()) == ["abc.txt", "test.txt"]
    assert a.config_set_allowlist([])
    assert a.config_set_denylist(regex="test.txt")
    assert sorted(a.get_all()) == ["abc.txt"]

    print("SETTING allowlist")
    assert a.config_set_allowlist(["test.txt"])
    assert a.config_get_allowlist() == ["test.txt"]
    assert sorted(a.get_all()) == ["abc.txt", "test.txt"]

