def test_tags():
    entries = [
        Entry.entry_from_db(os.path.join(CONFIG['content_root'],
                                         e.get('filename')), e.doc_id)
        for e in DB.posts.all()]
    tags = DB.tags.all()  # noqa

    t = entries[0].tags

    assert len(t) == 4
    assert t[0].name == 'buf'

    new_tag = Tag('buggg')
    new_tag.posts = [100, 100]
    with pytest.raises(ValueError):
        new_tag.posts = "This should not work"
    with pytest.raises(ValueError):
        new_tag.posts = 1  # This should not either

    new_tag.posts = [100]
    with pytest.raises(ValueError):
        list(new_tag.entries)
