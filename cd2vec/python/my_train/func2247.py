def test_include_samwise_template(include_template, include_data, namespace):
    template_path, obj, metadata = handler.load(include_template, namespace)
    assert obj
    yaml_string = yaml_dumps(obj['Resources']['MyStateMachine']['Properties']['DefinitionString'])
    assert yaml_string == f"!Sub |\n{include_data}\n"

