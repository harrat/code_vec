def test_when_app_stage_exists(handler_with_profile, mock_context, mock_event):
    thundra, handler = handler_with_profile

    invocation_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, InvocationPlugin):
            invocation_plugin = plugin

    handler(mock_event, mock_context)

    assert invocation_plugin.invocation_data['applicationStage'] == 'dev'

