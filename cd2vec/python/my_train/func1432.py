def test_get_wmt14_twice(self):
        get_wmt14()
        with mock.patch('lineflow.datasets.wmt14.pickle', autospec=True) as \
                mock_pickle:
            get_wmt14()
        mock_pickle.dump.assert_not_called()
        self.assertEqual(mock_pickle.load.call_count, 1)
