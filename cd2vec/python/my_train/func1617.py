def test_all(self):
        config['BACKOFF_MAX_TRIES'] = 2
        config['BACKOFF_MAX_VALUE'] = 2
        with self.assertRaises(requests.RequestException):
            bad_http_code()
        with self.assertRaises(requests.RequestException):
            bad_login()

        assert good_http_code() == "200 OK"

        with self.assertRaises(json.JSONDecodeError):
            bad_json()
