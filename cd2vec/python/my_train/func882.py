@patch('virga.providers.abstract.AbstractProvider.outcome')
    def test_assertion_call_outcome(self, mock_outcome):
        self.provider.assertion("AnyKey=='any-value'", 'Context', {}, 'resource-id')
        mock_outcome.assert_called_once_with(False)
