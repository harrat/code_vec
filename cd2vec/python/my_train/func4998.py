def test_custom_default_config(app_custom_default):
    """Test that we can pass a custom default configuration."""

    redis = FlaskMultiRedis(app_custom_default)
    assert redis.connection_pool.connection_kwargs['port'] == 16379
    assert redis.connection_pool.connection_kwargs['db'] == 9
    assert redis.connection_pool.connection_kwargs['password'] == 'password'
    assert redis.connection_pool.connection_kwargs['socket_timeout'] == 2

