def test_natsort(self, bids_layout):
        result = bids_layout.get(target='subject', return_type='id')
        assert result[:5] == list(map("%02d".__mod__, range(1, 6)))
