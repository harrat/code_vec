def test_set_synt_wave_mfcc(self):
        af = AudioFileMFCC(self.AUDIO_FILE)
        aligner = DTWAligner(synt_wave_mfcc=af)
        self.assertIsNone(aligner.real_wave_mfcc)
        self.assertIsNotNone(aligner.synt_wave_mfcc)
        self.assertIsNone(aligner.real_wave_path)
        self.assertIsNone(aligner.synt_wave_path)
