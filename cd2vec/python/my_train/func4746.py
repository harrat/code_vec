def test_large_retrievals(self):
        ''' verify for content lengths of different orders of magnitude '''
        for chunking_levels in range(1, 8):
            content_length = self.random.randint(
                int(self.S * (self.S / self.R) ** (chunking_levels - 1)), int(self.S * (self.S / self.R) ** (chunking_levels)))

            content = bytes([self.random.randint(0, 255)
                             for _ in range(content_length)])
            k = self.seccs.put_content(content)

            m = self.seccs.get_content(k)
            self.assertEqual(m, content)
