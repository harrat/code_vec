def test_strict_performance_check(make_runner, make_cases, common_exec_ctx):
    runner = make_runner()
    runner.policy.strict_check = True
    runner.runall(make_cases())
    stats = runner.stats
    assert 8 == stats.num_cases()
    assert_runall(runner)
    assert 6 == len(stats.failures())
    assert 2 == num_failures_stage(runner, 'setup')
    assert 1 == num_failures_stage(runner, 'sanity')
    assert 2 == num_failures_stage(runner, 'performance')
    assert 1 == num_failures_stage(runner, 'cleanup')

