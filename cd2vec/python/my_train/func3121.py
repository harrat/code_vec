@responses.activate
    def test_users(self):
        # Should returned cached data from above
        helix = twitch.Helix(client_id='id', bearer_token='token', use_cache=True)

        for user, display_name in zip(helix.users([24250859, 'sodapoppin']), ['Zarlach', 'sodapoppin']):
            self.assertEqual(user.display_name, display_name)
