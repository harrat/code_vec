def test_version_without_tag(self):
        v = relic.release.get_info()
        assert v is not None
        assert isinstance(v, relic.git.GitVersion)
        assert '0.0.0.dev' in v.pep386
        assert '+' in v.pep386
        assert int(v.post) == 1
