@pytest.mark.remote
def test_get_event_urls_segment():
    urls = locate.get_event_urls(
        "GW150914-v1",
        start=1126257415,
        end=1126257425,
    )
    assert len(urls) == 2
    for url in urls:  # check that these are the 4096-second files
        assert "-4096." in url

