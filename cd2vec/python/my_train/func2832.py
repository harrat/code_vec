@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'update-gitlog-utc')
    def test_cli_import_reference_019(snippy):
        """Try to import reference which uuid collides.

        The uuid must be unique and this causes a database integrity error.
        """

        content = {
            'data': [
                Reference.GITLOG
            ]
        }
        file_content = Content.get_file_content(Content.MKDN, content)
        with mock.patch('snippy.content.migrate.io.open', file_content, create=True) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'reference'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            assert mock_file.call_args == mock.call('./references.mkdn', mode='r', encoding='utf-8')

        content_uuid = {
            'data': [
                Content.deepcopy(Reference.REGEXP)
            ]
        }
        content_uuid['data'][0]['uuid'] = content['data'][0]['uuid']
        file_content = Content.get_file_content(Content.MKDN, content_uuid)
        with mock.patch('snippy.content.migrate.io.open', file_content, create=True) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'reference'])
            assert cause == 'NOK: content uuid already exist with digest 5c2071094dbfaa33'
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './references.mkdn', mode='r', encoding='utf-8')
