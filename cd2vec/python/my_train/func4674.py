@log_info
    def test_04_list_checkers(self):
        """Lists all registered checkers."""
        res = list_checkers()
        self.assertTrue(len(res) > 0)
        return res
