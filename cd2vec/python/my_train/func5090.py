def test_params():
    with tmp_module("transformA", make_versionable("", {}, "")) as ta:

        tak1 = ta.TestT(ala="kota")
        tak2 = ta.TestT(basia="kota")
        tak3 = ta.TestT(ala="psa")
        tak4 = ta.TestT(ala="kota")
