def test_get_wmt14(self):
        raw = get_wmt14()
        # train
        self.assertIn('train', raw)
        self.assertEqual(len(raw['train']), 2)
        for x in raw['train']:
            self.assertEqual(len(x), 4_500_966)
        # dev
        self.assertIn('dev', raw)
        self.assertEqual(len(raw['dev']), 2)
        for x in raw['dev']:
            self.assertEqual(len(x), 3_000)
        # test
        self.assertIn('test', raw)
        self.assertEqual(len(raw['test']), 2)
        for x in raw['test']:
            self.assertEqual(len(x), 3_003)
