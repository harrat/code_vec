def test_missing_required(self):
        relationship = copy.deepcopy(self.valid_relationship)
        del relationship['relationship_type']
        self.assertFalseWithOptions(relationship)
