def test_that_enter_exit_works():
    with vault_dev.server() as s:
        p = s.process
        assert s.is_running()
    assert p.poll()

