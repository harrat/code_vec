@helpers.seed
def test_lilliefors(dc):
    known_csv = """\
        station,Inflow,Inflow,Outflow,Outflow,Reference,Reference
        result,lilliefors,pvalue,lilliefors,pvalue,lilliefors,pvalue
        param,,,,,,
        A,0.308131,1.4e-05,0.340594,0.0,0.364453,0.0
        B,0.36764,0.0,0.420343,0.0,0.417165,0.0
        C,0.166799,0.082737,0.324733,0.0,0.161753,0.090455
        D,0.273012,6.7e-05,0.240311,0.000665,0.296919,3.7e-05
        E,0.341398,3e-06,0.239314,0.014862,0.233773,0.005474
        F,0.419545,0.0,0.331315,0.0,0.284249,0.000741
    """
    check_stat(known_csv, dc.lilliefors)

