@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_delete_snippet_002(snippy):
        """Delete snippet with digest.

        Delete snippet with very short version of digest that matches to one
        snippet.
        """

        content = {
            'data': [
                Snippet.FORCED
            ]
        }
        Content.assert_storage_size(2)
        cause = snippy.run(['snippy', 'delete', '-d', '54e41'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
