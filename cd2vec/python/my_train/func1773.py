def test_get_upcoming():
    movies = isle.movie.get_upcoming()
    assert inspect.isgenerator(movies)
    movie = next(movies)
    assert isinstance(movie, Movie)

