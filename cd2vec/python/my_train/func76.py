@pytest.mark.vcr()
def test_workbench_vuln_outputs_plugin_id_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vuln_outputs('something')
