def test_already_in_progress_raises_error_2(cert_and_key, ip_address):
    c = bankid.BankIDJSONClient(certificates=cert_and_key, test_server=True)
    pn = _get_random_personal_number()
    out = c.sign(ip_address, "Text to sign", pn)
    with pytest.raises(bankid.exceptions.AlreadyInProgressError):
        out2 = c.sign(ip_address, "Text to sign", pn)
