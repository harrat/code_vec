@override_settings(
        IDEMPOTENCY_KEY={"STORAGE": {"CLASS": "tests.tests.test_middleware.MyStorage"}}
    )
    def test_middleware_custom_storage(self, client):
        """
        In this test to prove the new custom storage class is being used by creating
        one that does not to store any information.
        Therefore a 409 conflict should never occur and the key will never exist.
        """
        voucher_data = {"id": 1, "name": "myvoucher0", "internal_name": "myvoucher0"}

        response = client.post(
            self.urls["create"],
            voucher_data,
            secure=True,
            HTTP_IDEMPOTENCY_KEY=self.the_key,
        )
        assert status.HTTP_201_CREATED == response.status_code

        response2 = client.post(
            self.urls["create"],
            voucher_data,
            secure=True,
            HTTP_IDEMPOTENCY_KEY=self.the_key,
        )
        assert response2.status_code == status.HTTP_201_CREATED
        request = response2.wsgi_request
        assert request.idempotency_key_exists is False
        assert request.idempotency_key_response is None
        assert request.idempotency_key_exempt is False
        assert request.idempotency_key_manual is False
        assert request.idempotency_key_done is True
        assert (
            request.idempotency_key_encoded_key
            == "1f4bc93e1a614e35350605b01133d77c1d4b15dd515e40fc6599ca3ff137143d"
        )
