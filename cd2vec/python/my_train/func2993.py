def test_too_many_ids(self):
        subject = '#1234 then #5678 too many.'
        result = self._call_function_under_test(subject)
        self.assertIsNone(result)
