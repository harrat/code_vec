def test_get_full_credits_tv_series(self):
        title_id = 'tt0149460'
        scraper = PyMDbScraper()

        # Correct values
        correct_credits = {
            'nm0547772': CreditScrape(
                'nm0547772',
                title_id,
                DIRECTOR,
                None,
                8,
                2010,
                2013
            ),
            'nm0592546': CreditScrape(
                'nm0592546',
                title_id,
                WRITER,
                None,
                26,
                2001,
                2003
            ),
            'nm0224007': CreditScrape(
                'nm0224007',
                title_id,
                ACTOR,
                'Bender / ...',
                124,
                1999,
                2013
            )
        }
        credit_types_count = 20

        credit_types = set()
        for credit in scraper.get_full_credits(title_id):
            if credit.job_title not in credit_types:
                credit_types.add(credit.job_title)

            self.assertEqual(credit.title_id, title_id)
            self.assertIsNotNone(credit.episode_count)
            self.assertIsNotNone(credit.episode_year_start)
            self.assertIsNotNone(credit.name_id)
            self.assertIsNotNone(credit.job_title)

            if credit.name_id in correct_credits:
                correct_credit = correct_credits[credit.name_id]
                self.assertEqual(credit.name_id, correct_credit.name_id)
                self.assertEqual(credit.job_title, correct_credit.job_title)
                self.assertEqual(credit.credit, correct_credit.credit)
                self.assertEqual(credit.episode_count, correct_credit.episode_count)
                self.assertEqual(credit.episode_year_start, correct_credit.episode_year_start)
                self.assertEqual(credit.episode_year_end, correct_credit.episode_year_end)
        self.assertEqual(credit_types_count, len(credit_types))
