def test_get_images(empty_person: Person):
    images = empty_person.get_images()
    assert images == empty_person.data["images"]

