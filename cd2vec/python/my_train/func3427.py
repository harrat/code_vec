def test_defined():
    eta._NOW = lambda: 1411868721.5
    progress_bar = ProgressBarYum(2000, 'file.iso')

    assert 'file.iso   0% [          ] --- KiB/s |   0.0 B              ' == str(progress_bar)
    assert 'file.iso   0% [          ] --- KiB/s |   0.0 B              ' == str(progress_bar)
    assert 'file.iso   0% [          ] --- KiB/s |   0.0 B              ' == str(progress_bar)

    eta._NOW = lambda: 1411868722.0
    progress_bar.numerator = 102
    assert 'file.iso   5% [-         ] --- KiB/s |   102 B              ' == str(progress_bar)

    eta._NOW = lambda: 1411868722.5
    progress_bar.numerator = 281
    assert 'file.iso  14% [=         ]   358 B/s |   281 B  00:00:05 ETA' == str(progress_bar)

    eta._NOW = lambda: 1411868723.0
    progress_bar.numerator = 593
    assert 'file.iso  29% [==-       ]   491 B/s |   593 B  00:00:03 ETA' == str(progress_bar)

    eta._NOW = lambda: 1411868723.5
    progress_bar.numerator = 1925
    assert 'file.iso  96% [=========-] 1.1 KiB/s | 1.9 KiB  00:00:01 ETA' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 1999
    assert 'file.iso  99% [=========-] 1.1 KiB/s | 2.0 KiB  00:00:01 ETA' == str(progress_bar)

    eta._NOW = lambda: 1411868724.5
    progress_bar.numerator = 2000
    assert 'file.iso                             | 2.0 KiB  00:00:03    ' == str(progress_bar)

