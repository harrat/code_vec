def test_empty_experiment(self):
        cluster = Cluster.new('tmux', server_name=_TEST_SERVER)
        exp = cluster.new_experiment('empty_exp')
        cluster.launch(exp)
        # Confirm the launch of experiment on tmux side.
        self.assertListEqual([s.name for s in self.server.sessions],
                             ['empty_exp'])

        # Check windows
        sess = self.server.sessions[0]
        self.assertCountEqual([tmux.cluster._DEFAULT_WINDOW],
                              [w.name for w in sess.windows])
