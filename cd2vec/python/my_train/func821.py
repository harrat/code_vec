def test_get_time(self, client):
        r = client.get_time()
        assert type(r) is dict
        assert 'iso' in r
