def test_dev_strict(basic_conf):
    tmpdir, local, conf = basic_conf
    ret = tools.run_asv_with_conf(conf, 'dev', '--strict', '--quick',
                                  '--bench=TimeSecondary',
                                  _machine_file=join(tmpdir, 'asv-machine.json'))
    assert ret == 2

