def test_list_check_with_empty_prgenvs(run_reframe):
    returncode, stdout, _ = run_reframe(
        checkpath=['unittests/resources/checks/frontend_checks.py'],
        action='list',
        environs=['foo'],
        more_options=['-n', 'NoPrgEnvCheck']
    )
    assert 'Found 0 check(s)' in stdout
    assert returncode == 0

