@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_persist_references_flag(fixture, request):
    testobj = MyRefObj()
    testobj.o = MyRefObj()
    testobj.o.o = testobj

    engine = request.getfixturevalue(fixture)
    pycall = pyccc.PythonCall(testobj.identity)

    # With the right flag, references ARE now persisted
    job = engine.launch(PYIMAGE, pycall, interpreter=PYVERSION, persist_references=True)
    print(job.rundata)
    job.wait()
    result = job.result
    assert result is testobj
    assert result.o is testobj.o
    assert result.o.o is result

