@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'export-time')
    def test_cli_export_solution_001(snippy):
        """Export all solutions.

        Export all solutions into file. File name or format are not defined
        in command line which must result tool default file and format.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './solutions.mkdn', content)
