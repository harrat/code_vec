@staticmethod
    @pytest.mark.usefixtures('import-gitlog', 'update-regexp-utc')
    def test_api_create_reference_004(server):
        """Update Reference resource with POST that maps to PUT.

        Send POST /references/{id} to update existing resource with the
        ``X-HTTP-Method-Override`` header that maps the operation as PUT.

        In this case the resource ``created`` attribute must remain in the
        initial value and the ``updated`` attribute must be set to reflect
        the update time.

        The ``uuid`` attribute must not be changed from it's initial value.
        """

        storage = {
            'data': [
                Storage.regexp
            ]
        }
        storage['data'][0]['created'] = Content.GITLOG_TIME
        storage['data'][0]['updated'] = Content.REGEXP_TIME
        storage['data'][0]['uuid'] = Reference.GITLOG_UUID
        request_body = {
            'data': {
                'type': 'reference',
                'attributes': Request.regexp
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '776'
        }
        expect_body = {
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/references/' + Reference.GITLOG_UUID
            },
            'data': {
                'type': 'reference',
                'id': Reference.GITLOG_UUID,
                'attributes': storage['data'][0]
            }
        }
        result = testing.TestClient(server.server.api).simulate_post(
            path='/api/snippy/rest/references/5c2071094dbfaa33',
            headers={'accept': 'application/vnd.api+json; charset=UTF-8', 'X-HTTP-Method-Override': 'PUT'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
