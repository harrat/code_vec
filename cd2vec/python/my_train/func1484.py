def test_small_inserts(self):
        ''' data structure should be empty at the beginning '''
        kvs_size = self.kvs_size()
        self.assertEqual(kvs_size, 0)
        kvs_count = self.kvs_count()
        self.assertEqual(kvs_count, 0)
        rc_count = self.rc_count()
        self.assertEqual(rc_count, 0)

        ''' verify that contents up to the average chunk size are stored in a single chunk '''
        for content_length in range(0, self.S + 1):
            content = bytes([self.random.randint(0, 255)
                             for _ in range(content_length)])
            self.seccs.put_content(content)

            kvs_size += content_length + self.digest_size
            kvs_count += 1
            rc_count += 1

            self.assertEqual(self.kvs_size(), kvs_size)
            self.assertEqual(self.kvs_count(), kvs_count)
            self.assertEqual(self.rc_count(), rc_count)

        ''' verify that contents beyond the average chunk size are stored in more than one chunk '''
        content = b''.join(bytes([self.random.randint(0, 255)])
                           for _ in range(self.S + 1))
        self.seccs.put_content(content)

        kvs_size += content_length + self.digest_size
        kvs_count += 1
        rc_count += 1

        self.assertGreater(self.kvs_size(), kvs_size)
        self.assertGreater(self.kvs_count(), kvs_count)
        self.assertGreater(self.rc_count(), rc_count)
