def test_home_to_point_field_goals_calc(self):
        fake_none = PropertyMock(return_value=None)
        fake_int = PropertyMock(return_vzalue=5)

        type(self.boxscore)._home_field_goals = fake_none
        type(self.boxscore)._home_three_point_field_goals = fake_none

        assert self.boxscore.home_two_point_field_goals is None

        type(self.boxscore)._home_three_point_field_goals = fake_int
        assert self.boxscore.home_two_point_field_goals is None

        type(self.boxscore)._home_field_goals = fake_int
        type(self.boxscore)._home_three_point_field_goals = fake_none

        assert self.boxscore.home_two_point_field_goals is None

        type(self.boxscore)._home_field_goals = fake_int
        type(self.boxscore)._home_three_point_field_goals = fake_int

        assert isinstance(self.boxscore.home_two_point_field_goals, int)
