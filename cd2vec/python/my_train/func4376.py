def test_environ_setup(hellotest, local_exec_ctx):
    # Use test environment for the regression check
    hellotest.variables = {'_FOO_': '1', '_BAR_': '2'}
    hellotest.setup(*local_exec_ctx)
    for k in hellotest.variables.keys():
        assert k not in os.environ

