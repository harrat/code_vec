def test_get_net(self):
    # TODO Add more tests

    # 1st Case - Not Pretrained with custom classes
    net = nets.utils.get_net(nets.VGG, pretrained=False, kwargs_net={'nc':10})
    self.assertEqual(net.classifier[-1].out_features, 10, 'Nope!')

    # 2nd Case - Pretrained with custom classes
    net = nets.utils.get_net(nets.VGG, pretrained=True, fname='vgg11', 
                             pretrain_url=urls.vgg11, kwargs_net={'nc':10, 
                             'norm': False}, inn=25088, attr='classifier')
    self.assertEqual(net.classifier.out_features, 10, 'Nope!')
