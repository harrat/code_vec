def test_middle_begin_bad2(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        with self.assertRaises(ValueError):
            audiofile.middle_begin = 10000
