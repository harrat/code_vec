@mock.patch("IPython.display.display")
def test_missing_reglue_no_error(mock_display, notebook_result):
    notebook_result.reglue("foo", raise_on_missing=False)
    mock_display.assert_called_once_with("No scrap found with name 'foo' in this notebook")

