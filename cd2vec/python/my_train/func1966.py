def test_synchronization_coordinator_timeout():
    mo = MultiObject(range(3))

    def foo(i, _sync=SYNC):
        sleep(i / 10)
        _sync.wait_for_everyone(timeout=0.1)

    with pytest.raises(MultiException) as exc:
        mo.call(foo)
    assert exc.value.count == len(mo)
    assert exc.value.common_type is threading.BrokenBarrierError

