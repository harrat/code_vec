@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_search_snippet_028(snippy, capsys):
        """Search snippets with ``digest`` option.

        Search snippet by explicitly defining long message digest.
        """

        output = (
            '1. Remove docker image with force @docker [53908d68425c61dc]',
            '',
            '   $ docker rm --force redis',
            '',
            '   # cleanup,container,docker,docker-ce,moby',
            '   > https://docs.docker.com/engine/reference/commandline/rm/',
            '   > https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--digest', '53908d68425c61dc310c9ce49d530bd858c5be197990491ca20dbe888e6deac5', '--no-ansi'])  # pylint: disable=line-too-long
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
