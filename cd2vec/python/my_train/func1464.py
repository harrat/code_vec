def test_ls_simple(capsys, interp):
    interp.do_ls('')
    out, err = capsys.readouterr()
    assert out == " Group2/ Group1/\n"

