@pytest.mark.parametrize("generator, expected_make_program", [
    ("NMake Makefiles", "nmake"),
    ("Unix Makefiles", "make")
])
def test_generator(generator, expected_make_program):

    generator_platform = {
        "NMake Makefiles": ["windows"],
        "Unix Makefiles": ["darwin", "linux"]
    }
    assert generator in generator_platform

    this_platform = platform.system().lower()
    if this_platform not in generator_platform[generator]:
        pytest.skip("%s generator is available only on %s" % (
            generator, this_platform.title()))

    @project_setup_py_test("hello-cpp", ["build"])
    def run_build():
        pass

    with push_env(CMAKE_GENERATOR=generator):
        tmp_dir = run_build()[0]
        cmakecache = tmp_dir.join(CMAKE_BUILD_DIR()).join("CMakeCache.txt")
        assert cmakecache.exists()
        variables = get_cmakecache_variables(str(cmakecache))
        make_program = (variables["CMAKE_MAKE_PROGRAM"][1]
                        if "CMAKE_MAKE_PROGRAM" in variables else "")
        assert make_program.endswith(expected_make_program) or \
            make_program.endswith("%s.exe" % expected_make_program)
