def test_serialize(tmpdir):
    expected_xml = """<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
>
</rdf:RDF>
"""
    p = tmpdir.mkdir("tests").join("sample_out.xml")
    context = new_context()
    context = shell.serialize(context, str(p) + ' xml')
    written = p.read()
    assert written == expected_xml

