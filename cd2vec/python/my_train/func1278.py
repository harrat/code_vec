def test_successful_http_call_with_query_params():
    try:
        url = "https://jsonplaceholder.typicode.com/users/1?test=test"
        parsed_url = urlparse(url)
        path = parsed_url.path
        normalized_path = "/users"
        query = parsed_url.query
        host = parsed_url.netloc
