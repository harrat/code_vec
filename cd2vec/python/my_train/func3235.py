def test_get_subtitles():
    yt_resource = get_yt_resource(subtitles_video)
    info = yt_resource.get_resource_subtitles()
    assert len(info['subtitles']) == 3  # brittle; can change if subs get added
    assert 'ru' in info['subtitles']
    assert 'en' in info['subtitles']
    assert 'zh-CN' in info['subtitles']

def test_non_youtube_url_error():
    url = 'https://vimeo.com/238190750'
    with pytest.raises(utils.VideoURLFormatError):
        youtube.YouTubeResource(url)
