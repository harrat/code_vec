def test_prompt_print_layout(capfd):
    class CommandArguments(object):
        last_command_status = constants.SUCCESS
        json = False

    prompt.prompt(args=CommandArguments)
    stdout, stderr = capfd.readouterr()
    
    assert '?' in stdout
