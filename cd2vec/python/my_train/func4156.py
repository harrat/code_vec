def test_default_search(self):
        spec = FileSpec(
            StrPathVar('id', pattern='[A-Z0-9_]+', invalid=('XYZ999',)),
            StrPathVar('ext', pattern='[^\.]+', valid=('txt', 'exe')),
            template='{id}.{ext}')
        with self.assertRaises(ValueError):
            spec.find()

        level1 = self.root.make_directory(name='ABC123')
        level2 = self.root.make_directory(parent=level1, name='AAA')
        base = level1.parent

        spec = DirSpec(
            StrPathVar('subdir', pattern='[A-Z0-9_]+', invalid=('XYZ999',)),
            StrPathVar('leaf', pattern='[^_]+', valid=('AAA', 'BBB')),
            template=os.path.join(base, '{subdir}', '{leaf}'))

        all_paths = spec.find(recursive=True)
        assert 1 == len(all_paths)
        assert \
            path_inst(level2, dict(subdir='ABC123', leaf='AAA')) == \
            all_paths[0]
