@will_generate('basic_site')
def test_basic_site_remove_existing(config):
    # create extra files
    # this file does not exist in path_contents, and so
    # should be removed
    os.mkdir(config['deploy_path'])
    with open(join(config['deploy_path'], 'rm_this'), 'w') as f:
        f.write('blablabla')

    strange_case(config)

    path_contents = {
        '001_2012_01_16_file.html': True,
        'index.html': True,
        'blogs': {
            'index.html': True,
            '2012_01_01_post1.html': True,
            '2012_01_02_post2.html': True,
        },
    }
    check_path_contents(config['deploy_path'], path_contents)

