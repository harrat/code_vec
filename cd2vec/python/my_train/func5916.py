def test_build_files_with_unicode(tmpdir):
    assert len(tmpdir.listdir()) == 0
    payload = {
        "status": "ok",
        "errors": [],
        "config": [
            {
                "file": "nginx.conf",
                "status": "ok",
                "errors": [],
                "parsed": [
                    {
                        "directive": "user",
                        "line": 1,
                        "args": [u"??"],
                    }
                ]
            }
        ]
    }
    crossplane.builder.build_files(payload, dirname=tmpdir.strpath)
    built_files = tmpdir.listdir()
    assert len(built_files) == 1
    assert built_files[0].strpath == os.path.join(tmpdir.strpath, 'nginx.conf')
    assert built_files[0].read_text('utf-8') == u'user ??;\n'

