@given(data_frames([column('txt_1', elements=sampled_from(lst_text)), column('str_1', dtype=np.unicode_), column('int_1', dtype=np.int_), column('flt_1', dtype=float), column('dt_1', elements=datetimes(min_value=pd.Timestamp.min, max_value=pd.Timestamp.max))], index=indexes(dtype=np.int_, min_size=1)), data_frames([column('txt_1', elements=sampled_from(lst_text)), column('str_1', dtype=np.unicode_), column('int_1', dtype=np.int_), column('flt_1', dtype=float), column('dt_1', elements=datetimes(min_value=pd.Timestamp.min, max_value=pd.Timestamp.max))], index=indexes(dtype=np.int_, min_size=1)))
def test_differ(df_t, df_u):
    df_result = differ(df_t, df_u, left_on='txt_1', right_on='txt_1', fields_l=['str_1','flt_1'], fields_r=['str_1','flt_1'])
    assert isinstance(df_result, pd.DataFrame)
    assert 'compid' in df_result.columns
    assert 'found' in df_result.columns

