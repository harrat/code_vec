def test_get_wikitext(self):
        params = [('wikitext-2', 36_718, 3_760, 4_358),
                  ('wikitext-103', 1_801_350, 3_760, 4_358)]
        for name, train_size, dev_size, test_size in params:
            with self.subTest(name=name, train_size=train_size,
                              dev_size=dev_size, test_size=test_size):
                raw = get_wikitext(name)
                # train
                self.assertIn('train', raw)
                self.assertEqual(len(raw['train']), train_size)
                # dev
                self.assertIn('dev', raw)
                self.assertEqual(len(raw['dev']), dev_size)
                # test
                self.assertIn('test', raw)
                self.assertEqual(len(raw['test']), test_size)
