def test__load___ok():
    db = 'testdb'
    col = 'testcol'
    objs = [
        {
            'name': 'boe',
            'year': 2019
        }
    ]
    index = [('name', ASCENDING), ('year', ASCENDING)]

    broker.save(objs, db, col, index, unique=True)
    assert broker.get_number_of_documents(db, col) == 1

    data = broker.load(db, col, {'year': 2019})
    assert objs == data

