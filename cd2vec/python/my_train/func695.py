def test_guard_if_function_not_changes_text_file(self):

        @guard(TestFileGuardDecorator.TEST_TEXT_FILE_PATH)
        def function_that_changes_the_file():
            pass

        function_that_changes_the_file()
        self._assert_file_content_equals(TestFileGuardDecorator.TEST_FILE_CONTENTS)
