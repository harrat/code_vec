def test_makeductcomponent():
    """py.test for makeductcomponent"""
    tdata = (
        ("aduct", ["DUCT", "aduct", "aduct_inlet", "aduct_outlet"]),  # dname, duct_obj
    )
    for dname, duct_obj in tdata:
        fhandle = StringIO("")
        idf = IDF(fhandle)
        result = hvacbuilder.makeductcomponent(idf, dname)
        assert result.obj == duct_obj

