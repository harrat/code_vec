def testSingleArg(self):
        singleHeader = sc().header(TEXTSINGLE, symbol="/", disableColour=True)
        self.assertMultiLineEqual(HEADEROUTPUT_EXPECTED, singleHeader)
