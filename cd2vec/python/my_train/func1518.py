def test_backup_file(tmpdir, binary_file):
    """Test wkr.os.backup_file."""
    binary_path = tmpdir.__class__(binary_file)
    assert tmpdir.exists()
    assert binary_path.exists()
    assert binary_path.size()
    original_contents = binary_path.read(mode='rb')
    # backup file
    backup_path = binary_path.new(basename=binary_path.basename + '~')
    assert not backup_path.exists()
    # now do the backup
    backup_file(binary_file)
    # assert that the backup_path exists and that it contains the same
    # content as the original file
    assert binary_path.exists()
    assert binary_path.read(mode='rb') == original_contents
    assert backup_path.exists()
    assert backup_path.read(mode='rb') == original_contents

