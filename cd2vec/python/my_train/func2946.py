def test_rvalue_infraredtransparent_construction(self):
        self.idf.initreadtxt(infrared_transparent)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        expected = (
            INSIDE_FILM_R
            + m.Thickness / m.Conductivity
            + m.Thickness / m.Conductivity
            + OUTSIDE_FILM_R
        )
        assert almostequal(c.rvalue, expected, places=2)
        assert almostequal(c.rvalue, 0.55, places=2)
