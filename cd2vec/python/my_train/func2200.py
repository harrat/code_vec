def test_attributes():
    """Creation, access and properties"""

    id_list = np.array(ds.samplet_ids)

    # ensuring strings can't be added to float attributes
    ds.add_attr('age', id_list[0], 43)
    for mismatched_type in ['43', 2 + 3j]:
        with raises(TypeError):
            ds.add_attr('age', id_list[2], mismatched_type)

    # ensuring floats can't be added to string attributes
    ds.add_attr('gender', id_list[3], 'female')
    for mismatched_type in [43, 2 + 3j]:
        with raises(TypeError):
            ds.add_attr('gender', id_list[4], mismatched_type)

    # adding to multiple samplets at a time
    # this should work
    ds.add_attr('gender',
                id_list[:3],
                ('female', 'male', 'male'))
    # but not this:
    with raises(ValueError):
        ds.add_attr('gender',
                    id_list[:3],
                    ('female', 'male',))

    # dataset attributes
    try:
        ds.add_dataset_attr('version', 2.0)
        # ds.add_dataset_attr('params', ['foo', 'bar', 20, 12, '/work/path'])
        # arbitrary values are causing problems with np.not_equal checks
        #   using simple values for now
        ds.add_dataset_attr('params', ['foo', 'bar', '/work/path'])
    except:
        raise AttributeError('Unable to add dataset attributes')

    # retrieval
    random_ids = id_list[random.sample(range(50), 5)]
    values_set = np.random.rand(5)
    ds.add_attr('random_float', random_ids, values_set)
    retrieved = ds.get_attr('random_float', random_ids)
    if not all(np.isclose(retrieved, values_set)):
        raise ValueError('Retrieved attribute values do not match the originals!')

    # ensuring ds.get_subset() returns subset for each attribute
    for index, id_ in enumerate(id_list):
        ds.add_attr('sid', id_, id_)
        ds.add_attr('index', id_, index)

    id_subset = id_list[:3]
    sub = ds.get_subset(id_subset)
    if set(sub.attr['sid']) != set(id_subset) or \
            set(sub.attr['index']) != set(id_subset):
        raise ValueError('attrs are not being propagated during .get_subset()')

    with raises(KeyError):
        ds.get_attr('non_existing_attr')

    with raises(KeyError):
        # existing but not all of them are set
        ds.get_attr('random_float', ds.samplet_ids)

    with warns(UserWarning):
        ds.del_attr('non_existing_attr')

    try:
        ds.del_attr('random_float')
    except:
        raise AttributeError('Attr deletion failed!')
