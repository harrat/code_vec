@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_roster_class_pulls_all_player_stats(self, *args, **kwargs):
        flexmock(utils) \
            .should_receive('_find_year_for_season') \
            .and_return('2018')
        roster = Roster('NOR')

        assert len(roster.players) == 5

        for player in roster.players:
            assert player.name in ['Drew Brees', 'Demario Davis',
                                   'Tommylee Lewis', 'Wil Lutz',
                                   'Thomas Morstead']
