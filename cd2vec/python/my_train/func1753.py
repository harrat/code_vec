def test_imagelistloaded_roizip(self):
        image_paths = [
            os.path.join(self.resources_dir, self.images_dir, img)
            for img in self.image_names
        ]
        images = [datahandler.image2array(pth) for pth in image_paths]
        roi_path = os.path.join(self.resources_dir, self.roi_zip_path)
        exp = core.Experiment(images, roi_path, self.output_dir)
        exp.separate()
        actual = exp.result
        self.assert_equal(len(actual), 1)
        self.assert_equal(len(actual[0]), 1)
        self.assert_allclose(actual[0][0], self.expected_00)
