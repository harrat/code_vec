def test_invalid_regex(self):
        # We need to explicitly call `evaluate` to make sure the exception
        # is triggered in all cases
        with pytest.raises(ReframeError):
            self.count_checks(filters.have_name('*foo')).evaluate()

        with pytest.raises(ReframeError):
            self.count_checks(filters.have_not_name('*foo')).evaluate()

        with pytest.raises(ReframeError):
            self.count_checks(filters.have_tag('*foo')).evaluate()

        with pytest.raises(ReframeError):
            self.count_checks(filters.have_prgenv('*foo')).evaluate()
