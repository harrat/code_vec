def test_handler_init_without_chat():
    with mock.patch('requests.post') as patch:
        response = requests.Response()
        response.status_code = 200
        response._content = json.dumps({'ok': False}).encode()
        patch.return_value = response
