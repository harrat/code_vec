def test_masked_with_head_tail(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        audiofile.run_vad()
        self.assertIsNotNone(audiofile.masked_middle_mfcc)
        self.assertNotEqual(audiofile.masked_middle_length, 0)
        self.assertIsNotNone(audiofile.masked_middle_map)
        pre = audiofile.masked_middle_length
        audiofile.set_head_middle_tail(head_length=TimeValue("0.440"), tail_length=TimeValue("1.200"))
        self.assertEqual(pre, audiofile.masked_middle_length)
        audiofile.set_head_middle_tail(head_length=TimeValue("0.480"), tail_length=TimeValue("1.240"))
        self.assertNotEqual(pre, audiofile.masked_middle_length)
        pre = audiofile.masked_middle_length
        audiofile.set_head_middle_tail(head_length=TimeValue("10.000"), tail_length=TimeValue("10.000"))
        self.assertNotEqual(pre, audiofile.masked_middle_length)
