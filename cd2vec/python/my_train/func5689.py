def test_expiration(cache):
    assert cache._get_expiration({}) == 0
    assert(cache._get_expiration({
        'expires': (datetime.utcnow() + timedelta(seconds=1)).strftime('%a, %d %b %Y %H:%M:%S GMT')
    }) == 1)
    assert(cache._get_expiration({
        'expires': (datetime.utcnow() + timedelta(seconds=100)).strftime('%a, %d %b %Y %H:%M:%S GMT')
    }) == 100)
    assert(cache._get_expiration({
        'expires': (datetime.utcnow() + timedelta(seconds=1234576890)).strftime('%a, %d %b %Y %H:%M:%S GMT')
    }) == 1234576890)
