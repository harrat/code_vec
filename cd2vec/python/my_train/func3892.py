@mock.patch('thundra.integrations.botocore.BaseIntegration.actual_call')
def test_firehose(mock_actual_call, mock_firehose_response):
    mock_actual_call.return_value = mock_firehose_response
    region = 'us-west-2'
    timestamp = 1553695200
    md5 = "098f6bcd4621d373cade4e832627b4f6"
    links = [
        region + ":" + "test-stream" + ":" + str(timestamp) + ":" + md5,
        region + ":" + "test-stream" + ":" + str(timestamp + 1) + ":" + md5,
        region + ":" + "test-stream" + ":" + str(timestamp + 2) + ":" + md5
    ]
    try:
        firehose = boto3.client('firehose', region_name=region)
        firehose.put_record(
            DeliveryStreamName='test-stream',
            Record={
                'Data': 'test'
            }
        )
    except botocore_errors:
        pass
    finally:
        tracer = ThundraTracer.get_instance()
        span = tracer.get_spans()[0]
        assert span.class_name == 'AWS-Firehose'
        assert span.domain_name == 'Stream'
        assert span.get_tag('operation.type') == 'WRITE'
        assert span.get_tag('aws.firehose.stream.name') == 'test-stream'
        assert span.get_tag('aws.request.name') == 'PutRecord'
        assert sorted(span.get_tag(constants.SpanTags['TRACE_LINKS'])) == sorted(links)
        tracer.clear()
