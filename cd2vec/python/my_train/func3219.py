@staticmethod
    @pytest.mark.usefixtures('devel_file_list', 'devel_file_data')
    def test_help_option_008(capsys, caplog):
        """Test printing test documentation from console.

        Print test cases. The ``--no-ansi`` option must work when set before
        the ``--help`` option. There are two files out of three where tests
        are read.
        """

        output = (
            'test case reference list:',
            '',
            '   $ snippy import --filter .*(\\$\\s.*)',
            '   # Import all snippets. File name is not defined in commmand line.',
            '   # This should result tool internal default file name',
            '   # ./snippets.yaml being used by default.',
            '',
            '   $ snippy import --filter .*(\\$\\s.*)',
            '   # Import all snippets. File name is not defined in commmand line.',
            '   # This should result tool internal default file name',
            '   # ./snippets.yaml being used by default.',
            '',
            ''
        )
        snippy = Snippy(['snippy', '--no-ansi', '--help', 'tests'])
        snippy.run()
        snippy.release()
        out, err = capsys.readouterr()
        assert out == Const.NEWLINE.join(output)
        assert not err
        assert not caplog.records[:]
        Content.delete()
