def test_custom_prefix(app):
    """Test that config prefixes enable distinct connections."""
    app.config['DBA_NODES'] = [{'host': 'localhost', 'db': 1}]
    app.config['DBB_NODES'] = [{'host': 'localhost', 'db': 2}]
    redis_a = FlaskMultiRedis(app, config_prefix='DBA')
    redis_b = FlaskMultiRedis(app, config_prefix='DBB')
    assert redis_a.connection_pool.connection_kwargs['db'] == 1
    assert redis_b.connection_pool.connection_kwargs['db'] == 2

