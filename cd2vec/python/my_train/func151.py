@given(st.floats(allow_nan=False, allow_infinity=False),
           st.floats(allow_nan=False, allow_infinity=False),
          st.floats(min_value=0.0001, max_value=1))
    def test_eq(self, low, high, res):
        """This also tests Set.array().
        This test can massively slow down hypothesis with even 
        reasonably large/small values.
        """
        assume(low < high)
        # to avoid MemoryError and runs that take forever..
        assume(high - low <= 10)
        D1 = Domain("1", low, high, res=res)
        D1.s1 = fun.bounded_linear(low, high)
        D2 = Domain("2", low, high, res=res)
        D2.s2 = Set(fun.bounded_linear(low, high))
        assert(D1.s1 == D2.s2)
