def test_all(self):
        cases = ["sina", "qq"]
        for src in cases:
            q = easyquotation.use(src)
            data = q.market_snapshot()
            for k in data.keys():
                self.assertRegex(k, r"\d{6}")
