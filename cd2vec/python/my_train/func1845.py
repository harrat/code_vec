def test_radex_single_run():
    """Ensure that the output of RADEX single run is correct."""
    ds = ndradex.run('co.dat', '1-0', 100, **COMMONS)
    assert np.isclose(ds['I'], 1.36)
    assert np.isclose(ds['F'], 2.684e-8)

