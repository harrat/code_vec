@staticmethod
    @pytest.mark.usefixtures('create-remove-utc')
    def test_cli_create_snippet_012(snippy, editor_data):
        """Create snippet with editor.

        Create a new snippet and define tags and brief already from command
        line interface. Other fields must have the default template content
        that is normally presented for the user when content is created with
        editor.

        The groups, links and description default values are not changed from
        default examples. These values must result tool internal defaults when
        content is stored.

        User fills the content data from editor so the content is stored.

        Editor must be used by default.
        """

        content = {
            'data': [{
                'category': 'snippet',
                'data': ('docker rm --volumes $(docker ps --all --quiet)', ),
                'brief': 'Brief from cli',
                'description': '',
                'name': '',
                'groups': ('default', ),
                'tags': ('cli', 'from', 'tags'),
                'links': (),
                'source': '',
                'versions': (),
                'languages': (),
                'filename': '',
                'created': '2017-10-14T19:56:31.000001+00:00',
                'updated': '2017-10-14T19:56:31.000001+00:00',
                'uuid': 'a1cd5827-b6ef-4067-b5ac-3ceac07dde9f',
                'digest': 'a020eb12a278e4426169360af1e124fb989747fd8a9192c293c938cea05798fa'
            }]
        }
        template = (
            '# Brief from cli @groups',
            '',
            '> Add a description that defines the content in one chapter.',
            '',
            '> [1] https://www.example.com/add-links-here.html',
            '',
            '`$ Markdown commands are defined between backtics and prefixed by a dollar sign`',
            '',
            '## Meta',
            '',
            '> category  : snippet  ',
            'created   : 2017-10-14T19:56:31.000001+00:00  ',
            'digest    : 023dc3bf754064bc5c3692cf535a769f402b187e14ebfb7e64273f6caf03ec6e  ',
            'filename  : example-content.md  ',
            'languages : example-language  ',
            'name      : example content handle  ',
            'source    : https://www.example.com/source.md  ',
            'tags      : cli,from,tags  ',
            'updated   : 2017-10-14T19:56:31.000001+00:00  ',
            'uuid      : a1cd5827-b6ef-4067-b5ac-3ceac07dde9f  ',
            'versions  : example=3.9.0,python>=3  ',
            ''
        )
        edited = (
            '# Brief from cli @groups',
            '',
            '> Add a description that defines the content in one chapter.',
            '',
            '> [1] https://www.example.com/add-links-here.html',
            '',
            '`$ docker rm --volumes $(docker ps --all --quiet)`',
            '',
            '## Meta',
            '',
            '> category : snippet  ',
            'created  : 2017-10-14T19:56:31.000001+00:00  ',
            'digest   : fdbf285d091a8c46cf491da675ecfeda38f7796ef034124b357f49737963cd19  ',
            'filename :  ',
            'language :  ',
            'name     :  ',
            'source   :  ',
            'tags     : cli,from,tags  ',
            'updated  : 2017-10-14T19:56:31.000001+00:00  ',
            'uuid     : a1cd5827-b6ef-4067-b5ac-3ceac07dde9f  ',
            'versions :  ',
            ''
        )
        editor_data.return_value = Const.NEWLINE.join(edited)
        cause = snippy.run(['snippy', 'create', '-t', 'tags,from,cli', '-b', 'Brief from cli'])
        assert cause == Cause.ALL_OK
        editor_data.assert_called_with(Const.NEWLINE.join(template))
        Content.assert_storage(content)
