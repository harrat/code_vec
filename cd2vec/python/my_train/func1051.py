def test_show_crew_attr(person: Person):
    assert isinstance(person.show_crew, list)
    show, credit = person.show_crew[0]
    assert isinstance(show, Show)
    assert isinstance(credit, Credit)

