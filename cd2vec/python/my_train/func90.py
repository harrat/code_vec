def test_module(self):
        module_string = '(module name (layer F.Cu) %s)'
        module = Module.parse(module_string % '')
        assert module.name == 'name'
        assert module.layer == 'F.Cu'
        assert Module.parse(module.to_string()) == module

        pads = ''
        for i in range(2):
            pads += Pad(str(i + 1), drill=Drill(0.8)).to_string()

        module = Module.parse(module_string % pads)
        assert module.pads[0].name == '1'
        assert module.pads[0].drill.size == 0.8
        assert module.pads[1].name == '2'
        assert module.pads[1].drill.size == 0.8
        assert Module.parse(module.to_string()) == module
