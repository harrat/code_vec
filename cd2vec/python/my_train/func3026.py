def test_base_parse_filters_json(api, fitem, fset):
    assert {'filters': [{
        'filter': 'distro',
        'quality': 'match',
        'value': 'win',
    }]} == api.agents._parse_filters(fitem, fset, rtype='json')
