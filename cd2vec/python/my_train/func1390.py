def test_no_ftol(self, optimizer):
        """Test complete run"""
        optm, params = optimizer
        opt = optm(**params)
        opt.optimize(objective_function, iters=iterations, **kwargs)
        assert len(opt.cost_history) == iterations
