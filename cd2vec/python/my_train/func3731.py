def test_a_encrypt(self):
       with open(self.file_name,'w') as test_file:
          test_file.write(self.contents)
       encrypt_file(self.file_name, self.password, self.salt)
       with open(self.file_name) as test_file:
          self.assertNotEqual(self.contents, test_file.read())
