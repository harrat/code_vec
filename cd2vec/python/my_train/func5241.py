def test_toggle():
    pidom = PiDom()
    device_name = 'test'
    pidom.synchronize(device_name)
    assert pidom.state(device_name) is False
    pidom.toggle(device_name)
    assert pidom.state(device_name) is True
    pidom.toggle(device_name)
    assert pidom.state(device_name) is False

