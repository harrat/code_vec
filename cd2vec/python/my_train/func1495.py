def test_execute_param_list():
    global cu
    cu.execute("insert into tests(id, name) values (8, 'test_execute_param_list')")
    cu.execute("select name from tests where name=?", ['test_execute_param_list'])
    row = cu.fetchone()
    assert row[0] == 'test_execute_param_list'

