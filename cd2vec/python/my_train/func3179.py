def test_index_file(tmpdir):
    tmpdir.join('foo.txt').write('foo')
    tmpdir.mkdir('index')
    with tmpdir.as_cwd():
        commands.index('foo.txt')
        assert os.path.samefile(
            'index/2c/26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae.txt',
            'foo.txt')
