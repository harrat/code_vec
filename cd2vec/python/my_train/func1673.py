def test_dir(self):
        temp = TempDir()
        foo = temp.make_directory(name='foo')
        assert foo == temp.absolute_path / 'foo'
        bar = temp.make_directory(name='bar', parent=foo)
        assert bar == temp.absolute_path / 'foo' / 'bar'
        assert (temp.absolute_path / 'foo' / 'bar').exists()
        temp.close()
        assert not temp.absolute_path.exists()
        # make sure trying to close again doesn't raise error
        temp.close()
