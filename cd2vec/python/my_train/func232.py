@pytest.mark.vcr()
def test_agents_list_filter_name_typeerror(api):
    with pytest.raises(TypeError):
        api.agents.list((1, 'match', 'win'))
