def test_historical_daily():
    assert iex_stats.historical_daily().empty == False
    assert iex_stats.historical_daily(date='201704').empty == False
    assert len(iex_stats.historical_daily(last=20).index) == 20
    # Test last out of range.
    with raises(ValueError):
        iex_stats.historical_daily(date="201704", last=0)
    with raises(ValueError):
        iex_stats.historical_daily(last=0)
    with raises(ValueError):
        iex_stats.historical_daily(date="not_a_date")
