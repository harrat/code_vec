def test_init_config():
    runcfg = framework_currentdata()
    rundir = os.path.dirname(runcfg)
    # if os.path.exists(rundir):
    #    shutil.rmtree(rundir)
    # assert False == os.path.exists(rundir)
    assert True == os.path.exists(runcfg)
    init_currentdata()
    assert True == os.path.exists(rundir)
    assert True == os.path.exists(runcfg)
    os.remove(runcfg)
    assert True == os.path.exists(rundir)
    assert False == os.path.exists(runcfg)
    init_currentdata()
    assert True == os.path.exists(rundir)
    assert True == os.path.exists(runcfg)

