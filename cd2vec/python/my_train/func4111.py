def test_middle_map(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        audiofile.middle_begin = 100
        audiofile.middle_end = 400
        self.assertEqual(len(audiofile.middle_map), 300)
