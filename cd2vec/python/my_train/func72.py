def test_fetch_rnaseq():
    f1 = fetchers.fetch_rnaseq(donors=['9861'])
    f2 = fetchers.fetch_rnaseq(donors='9861')
    f3 = fetchers.fetch_rnaseq(donors='H0351.2001')
    f4 = fetchers.fetch_rnaseq(donors=None)

    assert f1 == f2 == f3 == f4
    assert len(f1) == 1
    for k in ['genes', 'ontology', 'counts', 'tpm', 'annotation']:
        assert k in f1['9861']

    with pytest.raises(ValueError):
        fetchers.fetch_rnaseq(donors='notadonor')

    with pytest.raises(ValueError):
        fetchers.fetch_rnaseq(donors=['9861', 'notadonor'])
