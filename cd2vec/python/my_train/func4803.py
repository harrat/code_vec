@pytest.mark.parametrize('name', stops)
def test_find_stop(name):
    """Find known stops and check that they exist."""
    # Find stop by name
    stopByName = pyogt.find_stop(name)
    assert stopByName.get_name() == name
    assert stopByName.get_id() != -1

    # Find the same stop again, this time by id
    stopById = pyogt.find_stop_by_id(stopByName.get_id())
    assert stopById.get_name() == stopByName.get_name()
    assert stopById.get_id() == stopByName.get_id()

