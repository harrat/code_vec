@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_nfl_player_with_no_career_stats_handled_properly(self,
                                                              *args,
                                                              **kwargs):
        player = Player('HatfDo00')

        assert player.name == 'Dominique Hatfield'
