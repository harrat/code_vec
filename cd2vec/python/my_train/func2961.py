def test_forecasts_daily():
    fd = w1.forecasts_daily()

    assert len(fd) >= 7
    assert 'temp_min' in fd[0]
    assert 'temp_max' in fd[0]
    assert 'short_text' in fd[0]

def test_forecasts_3hourly():
    f3 = w1.forecasts_3hourly()
