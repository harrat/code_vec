def test_afx_2():
    global dir
    a = aud.Dir(dir)
    a.config_set_extensions(["wav"])

    assert a.afx_normalize(passes=2)
    assert a.afx_invert_stereo_phase("both")
    assert a.afx_hpf(80)
    assert a.afx_lpf(12000)

