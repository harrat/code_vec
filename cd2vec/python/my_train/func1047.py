def test_resourcecfg_delete(self, centreon_con):
        data = {}
        data['action'] = 'del'
        data['object'] = 'RESOURCECFG'
        data['values'] = '42'

        with patch('requests.post') as patched_post:
            centreon_con.resourcecfgs.delete('42', post_refresh=False)
            patched_post.assert_called_with(self.clapi_url, headers=self.headers, data=json.dumps(data), verify=True)
