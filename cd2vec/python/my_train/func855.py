@pytest.mark.vcr()
def test_access_groups_list_filter_operator_typeerror(api):
    with pytest.raises(TypeError):
        api.access_groups.list(('name', 1, 'win'))
