def test_put_invalid_foreign_key(self):
        """Test HTTP PUTing a resource with a field that refers to a
        non-existent resource (i.e. violate a foreign key constraint)."""
        response = self.app.put('/tracks/998',
                content_type='application/json',
                data=json.dumps(
                    {'Name': 'Some New Album',
                      'Milliseconds': 343719,
                      'TrackId': 998,
                      'UnitPrice': 0.99,}))
        assert response.status_code == 422
