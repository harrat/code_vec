def test_binary_existences():
    """Ensure that builtin RADEX binaries exist."""
    assert (ndradex.RADEX_BINPATH/'radex-uni').exists()
    assert (ndradex.RADEX_BINPATH/'radex-lvg').exists()
    assert (ndradex.RADEX_BINPATH/'radex-slab').exists()

