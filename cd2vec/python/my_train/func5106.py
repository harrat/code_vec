def test_get_detailed_grid_data():
    w = WattTime('user', 'password')
    data = w.get_detailed_grid_data(starttime='2019-01-01', endtime='2019-01-10', ba='CAISO_ZP26')
    market = data[0].get('market')

    if not market:
        raise('Error: market not returned, check free tier for ba')

    assert(market == 'RTM')
