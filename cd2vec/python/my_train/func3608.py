def test_calc_EFL():
    # "voltage traces" that are constant at -70*mV, -60mV, -50mV, -40mV for
    # 50ms each.
    dt = 1*ms
    voltage = np.ones((2, 200))*np.repeat([-70, -60, -50, -40], 50)*mV
    # Note that calcEFL takes times in ms
    inp_times = [[99, 150], [49, 150]]
    results = calc_eFEL(voltage, inp_times, ['voltage_base'], dt=dt)
    assert len(results) == 2
    assert all(res.keys() == {'voltage_base'} for res in results)
    assert_almost_equal(results[0]['voltage_base'], float(-60*mV))
    assert_almost_equal(results[1]['voltage_base'], float(-70*mV))

