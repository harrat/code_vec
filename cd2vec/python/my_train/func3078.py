def test_movie_crew_attr(person: Person):
    assert isinstance(person.movie_crew, list)
    movie, credit = person.movie_crew[0]
    assert isinstance(movie, Movie)
    assert isinstance(credit, Credit)

