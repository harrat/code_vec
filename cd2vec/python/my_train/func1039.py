@autodoc.describe('GET /')
    def test_get(self):
        """ GET / """
        req = self.create_request('http://localhost:5000/', 'GET')
        res = self.send(req, '', '../tests/data/get.json')
        self.assertEqual(res.status_code, 200)

        return res
