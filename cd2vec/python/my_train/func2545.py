def test_set_param_overrides_post(self):
        self.client.set_param_overrides({'foo': 'baz'})

        self._make_request(
            'https://httpbin.org/post',
            method='POST',
            data=b'foo=baz&spam=eggs'
        )

        last_request = self.client.get_last_request()
        body = self.client.get_request_body(last_request['id'])

        qs = parse_qs(body.decode('utf-8'))
        self.assertEqual(2, len(qs))
        self.assertEqual('baz', qs['foo'][0])
        self.assertEqual('eggs', qs['spam'][0])
