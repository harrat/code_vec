def test_stop_without_activity_started(cli, entries_file):
    entries = """20/01/2014
alias_1 10:00-10:30 Play ping-pong
"""

    entries_file.write(entries)
    with freeze_time('2014-01-20 10:15:00'):
        output = cli('stop', ['Play ping-pong'])

    assert output == "Error: You don't have any activity in progress for today\n"

