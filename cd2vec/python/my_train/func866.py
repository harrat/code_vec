def test_custom_parser_explicit(self):
        self.assertInvalidSERP(self.custom_serp_url)
        self.assertValidSERP(self.custom_serp_url,
                             self.custom_parser.engine_name,
                             u'test',
                             parser=self.custom_parser)
