def test_wait_log_predicate(get_log):
    def pred():
        raise PredicateNotSatisfied('bad attempt')

    with pytest.raises(TimeoutException):
        wait(pred=pred, timeout=.5, sleep=.1, message=False, log_interval=0.2)
    durations = re.findall('Still waiting after (.*?): bad attempt', get_log())
    rounded_durations = [round(Duration(d), 2) for d in durations]
    assert rounded_durations == [0.2, 0.4], 'expected logs at 200ms and 400ms, got %s' % (durations,)

