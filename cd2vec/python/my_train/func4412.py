@pytest.mark.parametrize("code", ["ABP-231", "ABP-123", "SSNI-351"])
def test_search_by_code(code):
    rv = client.post("/search_by_code", data=json.dumps({"code": code, "userpass": get_userpass()}))
    rsp = json.loads(rv.data.decode("utf-8"))
    assert len(rsp["videos"]) == 1
    assert requests.get(rsp["videos"][0]["video_url"], proxies=proxy).status_code == 200

