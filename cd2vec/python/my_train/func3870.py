def test_cycle_fileoutput(self):
        file1 = self.root.make_file(suffix='.gz')
        file2 = self.root.make_file()
        with textoutput((file1, file2), file_output_type=CycleFileOutput) as o:
            o.writelines(('foo', 'bar', 'baz'))
        with gzip.open(file1, 'rt') as i:
            assert 'foo\nbaz\n' == i.read()
        with open(file2, 'rt') as i:
            assert 'bar\n' == i.read()
