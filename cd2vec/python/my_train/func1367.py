def test_extract_multiple_parameters_from_jwt_hits_cache(self):
        dictionary = {
            "a": TEST_JWT
        }

        initial_cache_info = decode_jwt.cache_info()

        @extract([
            Parameter("a[jwt]/sub", "event", var_name="sub"),
            Parameter("a[jwt]/aud", "event", var_name="aud")
        ])
        def handler(event, sub=None, aud=None):  # noqa: pylint - unused-argument
            return {}

        handler(dictionary)

        self.assertEqual(decode_jwt.cache_info().hits, initial_cache_info.hits + 1)
        self.assertEqual(decode_jwt.cache_info().misses, initial_cache_info.misses + 1)
        self.assertEqual(decode_jwt.cache_info().currsize, initial_cache_info.currsize + 1)
