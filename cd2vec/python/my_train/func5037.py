def test_load_file_relative(self):
        checks = self.loader.load_from_file(
            'unittests/resources/checks/emptycheck.py')
        assert 1 == len(checks)
        assert checks[0].name == 'EmptyTest'
