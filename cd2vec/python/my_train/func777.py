@staticmethod
    @pytest.mark.usefixtures('yaml', 'default-references', 'export-time')
    def test_cli_export_reference_016(snippy):
        """Export defined reference with content data.

        Export defined reference based on content data. File name is defined in
        command line as yaml file.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Reference.GITLOG
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-c', 'https://chris.beams.io/posts/git-commit/', '-f', 'defined-reference.yaml', '--scat', 'reference'])  # pylint: disable=line-too-long
            assert cause == Cause.ALL_OK
            Content.assert_yaml(yaml, mock_file, 'defined-reference.yaml', content)
