def test_with_ids_through__call__(self, dcard, metas):
        ids = [m['id'] for m in metas]
        posts = dcard.posts(ids)
        assert posts.ids
        assert not posts.metas
        assert posts.only_id
