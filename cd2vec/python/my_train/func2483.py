def test_parse_alternating_subdir(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('alternating')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'bb'), 'wb') as outfp:
        outfp.write(b'bb\n')
    cc = indir.mkdir('cc')
    aa = indir.mkdir('aa')
    with open(os.path.join(str(indir), 'dd'), 'wb') as outfp:
        outfp.write(b'dd\n')
    with open(os.path.join(str(cc), 'sub2'), 'wb') as outfp:
        outfp.write(b'sub2\n')
    with open(os.path.join(str(aa), 'sub1'), 'wb') as outfp:
        outfp.write(b'sub1\n')
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_alternating_subdir)

def test_parse_rr_verylongname(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('rrverylongname')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'a'*RR_MAX_FILENAME_LENGTH), 'wb') as outfp:
        outfp.write(b'aa\n')
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-rational-rock', '-o', str(outfile), str(indir)])
