def test_abspath_rel(self):
        cwd = os.getcwd()
        assert abspath(Path('foo')) == Path(cwd) / 'foo'
