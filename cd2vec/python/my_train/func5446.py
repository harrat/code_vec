def test_demand_add(self):
        self.model.add_demand('A', 'B', 40, 'dmd_a_b')
        self.model.update_simulation()
        self.assertEqual(self.model.__repr__(), 'PerformanceModel(Interfaces: 18, Nodes: 7, Demands: 5, RSVP_LSPs: 3)')
