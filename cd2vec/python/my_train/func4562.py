def test_slab2_file_name_only():
    "Fetch only the filename of the files"
    for zone in ZONES:
        fnames = fetch_slab2(zone, load=False)
        fnames = [os.path.basename(f) for f in fnames]
        expected_fnames = set(
            "{}_slab2_{}.grd".format(ZONES[zone]["fname_indicator"], dataset)
            for dataset in DATASETS
        )
        assert len(fnames) == 5
        assert set(fnames) == expected_fnames

