def test_serialize_bad_file(capsys, tmpdir):
    p = tmpdir.mkdir("tests").join("/dir/doesnt/exist/sample_out.xml")
    context = new_context()
    context = shell.serialize(context, str(p) + ' xml')
    (out, err) = capsys.readouterr()
    assert out.index('Error, unexpected problem writing file.') == 0

