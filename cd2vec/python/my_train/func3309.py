@rt.switch_runtime(fixtures.TEST_CONFIG_FILE, 'testsys')
    def test_partition(self):
        p = fixtures.partition_by_name('gpu')
        assert 2 == self.count_checks(filters.have_partition([p]))
        p = fixtures.partition_by_name('login')
        assert 0 == self.count_checks(filters.have_partition([p]))
