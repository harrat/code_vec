def test_basic_search():
    res = powo.search('Poa Annua')
    assert res.size() >= 2
    assert next(res)['fqId'] == 'urn:lsid:ipni.org:names:320035-2'

def test_advanced_name_search():
    query = { Name.genus: 'Poa', Name.species: 'annua', Name.author: 'L.' }
    res = powo.search(query)
    assert res.size() == 1
    assert next(res)['fqId'] == 'urn:lsid:ipni.org:names:320035-2'
