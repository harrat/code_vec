def test_count_lines(tmpdir, random_lines):
    """Test the wkr.count_lines method."""
    for num_lines in range(len(random_lines) + 1):
        filename = tmpdir.join('text.txt')
        assert not filename.exists()
        with open(filename.strpath, 'wb') as output_file:
            output_file.write(
                (u'\n'.join(random_lines[:num_lines]) + u'\n').encode('utf-8'))
        assert filename.exists()
        assert wkr.io.count_lines(filename.strpath) == max(num_lines, 1)
        filename.remove()
        assert not filename.exists()

