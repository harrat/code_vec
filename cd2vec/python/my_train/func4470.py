def test_ignore_warnings_in_stop():
    """Make sure that modules that was loaded after start() does not trigger
    warnings in stop()"""
    freezer = freeze_time(datetime.datetime(2016, 10, 27, 9, 56))
    freezer.start()

    with assert_module_with_emitted_warning():
        with assert_no_warnings():
            freezer.stop()
