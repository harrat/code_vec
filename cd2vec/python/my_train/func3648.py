def test_write_calculations_to_csv(self):
        path = os.path.join(os.path.dirname(__file__), '.', 'data', 'Test Meta File.txt')
        out_path = os.path.join(os.path.dirname(__file__), '.', 'data', 'test_output.txt')

        def avg_with_units(lst):
            num = np.size(lst)
            acc = 0
            for i in lst:
                acc = i + acc

            return acc / num

        output = write_calculations_to_csv(avg_with_units, 1, 28, path,
                                           ["Average Conc (mg/L)"], out_path,
                                           extension=".xls")

        self.assertSequenceEqual(["1", "2"], output['ID'].tolist())
        self.assertSequenceEqual(
        [5.445427082723495, 5.459751965314751],
        output['Average Conc (mg/L)'].tolist())
