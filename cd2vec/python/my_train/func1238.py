def test_efo_direct_match(ontclient):
    assert ontclient.find_term('Dementias') == 'http://www.ebi.ac.uk/efo/EFO_0004718'

def test_otzooma_mappings_whitespace(ontclient):
    assert ontclient.find_term('Prostate cancer') == 'http://purl.obolibrary.org/obo/MONDO_0008315'
