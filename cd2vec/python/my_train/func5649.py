def test_msg_type_15(self):
        msg = NMEAMessage(b"!AIVDM,1,1,,A,?5OP=l00052HD00,2*5B").decode()
        assert msg['type'] == 15
        assert msg['repeat'] == 0
        assert msg['mmsi'] == 368578000
        assert msg['offset1_1'] == 0

        msg = NMEAMessage(b"!AIVDM,1,1,,B,?h3Ovn1GP<K0<P@59a0,2*04").decode()
        assert msg['type'] == 15
        assert msg['repeat'] == 3
        assert msg['mmsi'] == 3669720
        assert msg['mmsi1'] == 367014320
        assert msg['type1_1'] == 3

        assert msg['mmsi2'] == 0
        assert msg['type1_2'] == 5
        assert msg['offset1_2'] == 617
