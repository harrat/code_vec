def test_save_load(self):
        model_config = core.ModelConfig(_lineworld_name)
        tc = core.PpoTrainContext()
        ppo_agent = tfagents.TfPpoAgent(model_config=model_config)
        ppo_agent.train(train_context=tc, callbacks=[duration._SingleIteration(), log.Iteration()])
        tempdir = bcore._get_temp_path()
        bcore._mkdir(tempdir)
        ppo_agent.save(tempdir, [])
        ppo_agent = tfagents.TfPpoAgent(model_config=model_config)
        ppo_agent.load(tempdir, [])
        pc = core.PlayContext()
        pc.max_steps_per_episode = 10
        pc.num_episodes = 1
        ppo_agent.play(play_context=pc, callbacks=[])
        bcore._rmpath(tempdir)
