def test_ge(self):
        r = ruler.FileSizeRule(">= 1.5m")
        self.assertFalse(r.match(self.f1))
        self.assertTrue(r.match(self.f2))
        self.assertTrue(r.match(self.f3))
