def test_sequence_auto_loosen(table):
    category = Category.create(name='Python')
    author = Author.create(name='Pythoner')
    for title in ['Step1', 'Step2', 'Step3']:
        Course.create(title=category.name + title,
            category_id=category.id, author_id=author.id)

    for round_ in range(50):
        c = Course.select().order_by(-Course.sequence).first()
        c.change_sequence(new_sequence=2)
        ac = Course.select().order_by(+Course.sequence)[1]
        assert ac.id == c.id

