def test_cloudwatch_logs_trigger(tracer_and_invocation_support, handler, mock_cloudwatch_logs_event, mock_context):
    thundra, handler = handler
    tracer, invocation_support = tracer_and_invocation_support
    try:
        response = handler(mock_cloudwatch_logs_event, mock_context)
    except:
        print("Error running handler!")
        raise
    span = tracer.recorder.get_spans()[0]

    assert lambda_event_utils.get_lambda_event_type(mock_cloudwatch_logs_event,
                                                    mock_context) == lambda_event_utils.LambdaEventType.CloudWatchLogs

    compressed_data = base64.b64decode(mock_cloudwatch_logs_event['awslogs']['data'])
    decompressed_data = json.loads(str(GzipFile(fileobj=BytesIO(compressed_data)).read(), 'utf-8'))

    assert span.get_tag(constants.SpanTags['TRIGGER_DOMAIN_NAME']) == 'Log'
    assert span.get_tag(constants.SpanTags['TRIGGER_CLASS_NAME']) == 'AWS-CloudWatch-Log'
    assert span.get_tag(constants.SpanTags['TRIGGER_OPERATION_NAMES']) == [decompressed_data['logGroup']]

    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_DOMAIN_NAME']) == 'Log'
    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_CLASS_NAME']) == 'AWS-CloudWatch-Log'
    assert invocation_support.get_agent_tag(constants.SpanTags['TRIGGER_OPERATION_NAMES']) == [decompressed_data['logGroup']]

