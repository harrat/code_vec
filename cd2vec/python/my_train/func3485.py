def test_dynamodb_put_item_resource():
    ConfigProvider.set(config_names.THUNDRA_TRACE_INTEGRATIONS_AWS_DYNAMODB_TRACEINJECTION_ENABLE, 'true')

    try:
        item = {
            'id': '3',
            'text': 'test'
        }
