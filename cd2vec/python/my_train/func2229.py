@responses.activate
    def test_hosts_not_exist(self, centreon_con):
        with open(resource_dir / 'test_hosts_list.json') as data:
            wsresponses = json.load(data)
        responses.add(
            responses.POST,
            self.clapi_url,
            json=wsresponses, status=200, content_type='application/json')
        state, res = centreon_con.hosts.get('empty')
        assert state == False
        assert res == None
