def test_cancel(make_job, exec_ctx):
    minimal_job = make_job(sched_access=exec_ctx.access)
    prepare_job(minimal_job, 'sleep 30')
    t_job = datetime.now()
    minimal_job.submit()
    minimal_job.cancel()
    minimal_job.wait()
    t_job = datetime.now() - t_job
    assert minimal_job.finished()
    assert t_job.total_seconds() < 30

    # Additional scheduler-specific checks
    sched_name = minimal_job.scheduler.registered_name
    if sched_name in ('slurm', 'squeue'):
        assert minimal_job.state == 'CANCELLED'

