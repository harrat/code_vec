def test_experiment_iter(fake_exp):
    meths = ['iter_containers']
    for meth in meths:
        method = getattr(fake_exp, meth)
        assert callable(method)

