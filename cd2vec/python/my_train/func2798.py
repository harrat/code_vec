def testZipAppCmd(self, tmpdir):
        cmdLine = ['zipapp']
        self.cwd = str(tmpdir.realpath())
        exitcode = runZm(self, cmdLine)[0]
        assert exitcode == 0
        zipAppPath = joinpath(self.cwd, zipapp.ZIPAPP_NAME)
        assert isfile(zipAppPath)
        assert iszip(zipAppPath)
