@staticmethod
    @pytest.mark.usefixtures('exists_true', 'access_true')
    def test_export_shell_completion_001(snippy):
        """Export bash completion script.

        Export Bash completion script. In this case the ``--file`` option is
        not defined and a default filename is used.
        """

        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            cause = snippy.run(['snippy', 'export', '--complete', 'bash'])
            assert cause == Cause.ALL_OK
            Content.assert_arglist(mock_file, './snippy.bash-completion', mode='w', encoding='utf-8')
            file_handle = mock_file.return_value.__enter__.return_value
            file_handle.write.assert_called_with(Content.COMPLETE_BASH)
