def test_multiple_experiments(self):
        self.create_default_experiment()

        # Launch a second experiment.
        cluster = Cluster.new('tmux', server_name=_TEST_SERVER)
        exp2 = cluster.new_experiment('exp2')
        cluster.launch(exp2)

        # Confirm the launch of experiment on tmux side.
        self.assertListEqual([s.name for s in self.server.sessions],
                             ['exp', 'exp2'])

        # Check windows
        sess = self.server.sessions[0]
        self.assertCountEqual(
                [tmux.cluster._DEFAULT_WINDOW, 'group:hello', 'alone'],
                [w.name for w in sess.windows])
        sess = self.server.sessions[1]
        self.assertCountEqual([tmux.cluster._DEFAULT_WINDOW],
                              [w.name for w in sess.windows])
