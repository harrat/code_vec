@staticmethod
    @pytest.mark.usefixtures('create-remove-utc')
    def test_cli_create_snippet_008(snippy, capsys):
        """Create snippet from CLI.

        Create new snippet with three groups. The groups must be sorted when
        they are printed.
        """

        content = {
            'data': [
                Content.deepcopy(Snippet.REMOVE)
            ]
        }
        content['data'][0]['groups'] = ('docker', 'dockerfile', 'moby')
        content['data'][0]['uuid'] = Content.UUID1
        content['data'][0]['digest'] = '03dc5d1629b256271a6f2bf16abdc8f5d6f4f94f6deef9e79288792e41e32fe7'
        data = Const.DELIMITER_DATA.join(content['data'][0]['data'])
        brief = content['data'][0]['brief']
        groups = content['data'][0]['groups']
        tags = Const.DELIMITER_TAGS.join(content['data'][0]['tags'])
        links = Const.DELIMITER_LINKS.join(content['data'][0]['links'])
        cause = snippy.run(['snippy', 'create', '--content', data, '--brief', brief, '--groups', groups, '--tags', tags, '--links', links])  # pylint: disable=line-too-long
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)

        output = (
            '1. Remove all docker containers with volumes @docker,dockerfile,moby [03dc5d1629b25627]',
            '',
            '   $ docker rm --volumes $(docker ps --all --quiet)',
            '',
            '   # cleanup,container,docker,docker-ce,moby',
            '   > https://docs.docker.com/engine/reference/commandline/rm/',
            '',
            'OK',
            ''
        )
        out, err = capsys.readouterr()
        cause = snippy.run(['snippy', 'search', '--sall', 'dockerfile', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
