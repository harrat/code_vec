def runTest(self):
        import docs.examples.ex23 as ex
        self.assertAlmostEqual(max(ex.lmbda_list), ex.turning_point,
                               delta=5e-5)
