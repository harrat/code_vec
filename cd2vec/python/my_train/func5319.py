def test_export():
    global dir
    a = aud.Dir(dir)

    a.config_set_extensions(["wav"])

    assert a.export_for("amuse", "mock/amuse")
    assert sorted(listdir(join(dir, "amuse"))) == [
        "bloop.wav",
        "joined.wav",
        "song.wav",
    ]
