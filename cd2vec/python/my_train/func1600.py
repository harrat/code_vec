def test_annual_change_make_forecast_and_plug(self, annual_stockrow_stmts_cat: FinancialStatements):
        stmts = annual_stockrow_stmts_cat.copy()
        stmts.config.update('total_debt', ['forecast_config', 'make_forecast'], True)
        stmts.config.update('st_debt', ['forecast_config', 'make_forecast'], False)
        stmts.config.update('def_tax_lt', ['forecast_config', 'method'], 'manual')
        stmts.config.update('def_tax_lt', ['forecast_config', 'manual_forecasts'], {'levels': [], 'growth': [4, 5]})
        try:
            super().test_annual(stmts)
        except BalanceSheetNotBalancedException:
            pass
        else:
            assert False
        stmts.config.update("total_debt", ["forecast_config", "plug"], True)
        stmts.config.update("lt_debt", ["forecast_config", "plug"], False)
        super().test_annual(stmts, data=FCST_STOCKROW_CAT_A_PLUG_MAKE_FORECAST_INDEX_DATA_DICT)
