@pytest.mark.skipif(
    "TRAVIS" in os.environ and os.environ["TRAVIS"] == "true",
    reason="doesn't work with Travis",
)
def test_dict_is_song():
    if ydl_utils.dict_is_song(ydl_utils.get_ydl_dict(SEARCH_TERM_IS_ALBUM, 1)):
        raise AssertionError()

    if ydl_utils.dict_is_song(
        ydl_utils.get_ydl_dict(SEARCH_TERM_IS_TOO_LONG, 1)
    ):
        raise AssertionError()

