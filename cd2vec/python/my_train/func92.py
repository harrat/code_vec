def test_save(self):
        self._store.save(self._person)
        dbm_connection = dbm.open(self._tempfile, "r")
        self.assertEqual(
            {"name": "Foobar", "uid": "foobar"},
            json.loads(dbm_connection["Person:foobar"].decode("utf8")))
