def test_insert_filter_full(cuckoo_filter):
    # We can artificially make a filter full by inserting the same item
    # (2 * bucket size) times, so another insert of the same item should fail.
    fake_value = 'fake_value'
    for _ in range(2 * cuckoo_filter.bucket_size):
        cuckoo_filter.insert(fake_value)
    with pytest.raises(CuckooFilterFullException):
        cuckoo_filter.insert(fake_value)
