def test_get_popular():
    shows = isle.people.get_popular()
    assert inspect.isgenerator(shows)
    person = next(shows)
    assert isinstance(person, Person)

