def test_fileinput_defaults(self):
        path = self.root.make_file()
        with open(path, 'wt') as o:
            o.write('foo\nbar\n')
        sys.argv = [self.system_args[0], path]
        self.assertEqual(
            ['foo\n', 'bar\n'],
            list(textinput()))
        sys.argv = []
        with intercept_stdin('foo\n'):
            lines = list(textinput([STDIN]))
            assert 1 == len(lines)
            assert 'foo\n' == lines[0]
        with intercept_stdin(b'foo\nbar\n', is_bytes=True):
            assert [b'foo\n', b'bar\n'] == list(byteinput())
