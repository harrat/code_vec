def test_errorneous_http_call():
    try:
        url = 'http://adummyurlthatnotexists.xyz/'
        parsed_url = urlparse(url)
        path = parsed_url.path
        query = parsed_url.query
        host = parsed_url.netloc
