@pytest.mark.parametrize("node", (leaf_2, leaf_4, node_34))
def test_is_right_parent(node):
    """
    Tests a node's property of being a right parent
    (excluding the possibility of being left parent)
    """
    assert node.is_right_parent() and not node.is_left_parent()

