@pytest.mark.remote
def test_find_datasets_detector():
    v1sets = datasets.find_datasets('V1')
    assert 'GW170817-v3' in v1sets
    assert 'GW150914-v1' not in v1sets

    assert datasets.find_datasets('X1', type="run") == []

