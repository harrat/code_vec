def test_derive_key_alternate_derivation_data(self):
        """
        Test that a DeriveKey request can be processed correctly by
        specifying multiple base objects and no derivation data.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        base_key = pie_objects.SymmetricKey(
            algorithm=enums.CryptographicAlgorithm.HMAC_SHA256,
            length=176,
            value=(
                b'\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b'
                b'\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b'
                b'\x0b\x0b\x0b\x0b\x0b\x0b'
            ),
            masks=[enums.CryptographicUsageMask.DERIVE_KEY]
        )
        e._data_session.add(base_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        base_data = pie_objects.SecretData(
            value=(
                b'\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf7'
                b'\xf8\xf9'
            ),
            data_type=enums.SecretDataType.SEED,
            masks=[enums.CryptographicUsageMask.DERIVE_KEY]
        )
        e._data_session.add(base_data)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        attribute_factory = factory.AttributeFactory()

        payload = payloads.DeriveKeyRequestPayload(
            object_type=enums.ObjectType.SYMMETRIC_KEY,
            unique_identifiers=[
                str(base_key.unique_identifier),
                str(base_data.unique_identifier)
            ],
            derivation_method=enums.DerivationMethod.HMAC,
            derivation_parameters=attributes.DerivationParameters(
                cryptographic_parameters=attributes.CryptographicParameters(
                    hashing_algorithm=enums.HashingAlgorithm.SHA_256
                ),
                salt=(
                    b'\x00\x01\x02\x03\x04\x05\x06\x07'
                    b'\x08\x09\x0a\x0b\x0c'
                )
            ),
            template_attribute=objects.TemplateAttribute(
                attributes=[
                    attribute_factory.create_attribute(
                        enums.AttributeType.CRYPTOGRAPHIC_LENGTH,
                        336
                    ),
                    attribute_factory.create_attribute(
                        enums.AttributeType.CRYPTOGRAPHIC_ALGORITHM,
                        enums.CryptographicAlgorithm.AES
                    )
                ]
            )
        )

        response_payload = e._process_derive_key(payload)

        e._logger.info.assert_any_call("Processing operation: DeriveKey")
        e._logger.info.assert_any_call(
            "2 derivation objects specified with the DeriveKey request."
        )
        e._logger.info.assert_any_call(
            "Object 1 will be used as the keying material for the derivation "
            "process."
        )
        e._logger.info.assert_any_call(
            "Object 2 will be used as the derivation data for the derivation "
            "process."
        )
        e._logger.info.assert_any_call("Created a SymmetricKey with ID: 3")

        self.assertEqual("3", response_payload.unique_identifier)

        managed_object = e._data_session.query(
            pie_objects.SymmetricKey
        ).filter(
            pie_objects.SymmetricKey.unique_identifier == 3
        ).one()

        self.assertEqual(
            (
                b'\x3c\xb2\x5f\x25\xfa\xac\xd5\x7a'
                b'\x90\x43\x4f\x64\xd0\x36\x2f\x2a'
                b'\x2d\x2d\x0a\x90\xcf\x1a\x5a\x4c'
                b'\x5d\xb0\x2d\x56\xec\xc4\xc5\xbf'
                b'\x34\x00\x72\x08\xd5\xb8\x87\x18'
                b'\x58\x65'
            ),
            managed_object.value
        )
        self.assertEqual(
            enums.CryptographicAlgorithm.AES,
            managed_object.cryptographic_algorithm
        )
        self.assertEqual(
            336,
            managed_object.cryptographic_length
        )
        self.assertIsNotNone(managed_object.initial_date)
