def test_calculate_distance_profile_constant_sequence_and_query(self):
        n = 100
        m = np.random.randint(10, n // 2)
        t = np.full(n, np.random.rand())
        q = np.full(m, np.random.rand())
        qt = utils.sliding_dot_product(q, t)
        rolling_mean, rolling_std = utils.rolling_avg_sd(t, m)
        dp = utils.calculate_distance_profile(qt, m, np.mean(q), np.std(q), rolling_mean, rolling_std)
        assert np.allclose(dp, np.full(n - m + 1, 0)), "calculate_distance_profile_constant_sequence_and_query: " \
                                        "distance of constant query to constant sequence is ero by definition."
