def test_conflicted_checks(self):
        self.loader_with_path._ignore_conflicts = False
        with pytest.raises(NameConflictError):
            self.loader_with_path.load_all()
