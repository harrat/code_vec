def test_posivite(api, api_assert):
    r = api.default_uniform()

    data = r['samples']

    assert all([0 < d < 1 for d in data])

