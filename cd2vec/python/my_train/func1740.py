def test_get():
    from configaro import ConfigPropertyNotFoundError, get, init
    init('tests.config')
    expected = {
        'name': 'locals',
        'log': {
            'file': 'some-file.txt',
            'level': 'DEBUG'
        },
        'monitoring': {
            'haproxy': {
                'disabled': True
            },
            'nginx': {
                'disabled': True
            }
        }
    }
    config = get()
    assert config.log.level == 'DEBUG'
    assert config == munch.munchify(expected)
    log = get('log')
    assert log.level == 'DEBUG'
    log = munch.unmunchify(log)
    assert log == expected['log']
    assert get('name') == 'locals'
    assert get('log.level') == 'DEBUG'
    assert get('monitoring.haproxy.disabled') is True
    assert get('monitoring.nginx.disabled') is True
    with pytest.raises(ConfigPropertyNotFoundError):
        assert get('monitoring.nginx.disable') is True
    assert get('monitoring.nginx.disable', default=None) is None

