def test_dynamic_params(self, test_client):
        rv = test_client.put("/api/v1/apples/sub/1")
        assert "200" in str(rv.status)

        rv = test_client.get("/api/v1/apples/sub/")
        assert "404" in str(rv.status)

        rv = test_client.get("/api/v1/apples/sub/hello")
        assert "404" in str(rv.status)
