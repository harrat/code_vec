@pytest.mark.parametrize('fname', [
    os.path.join(data_path, 'tarfile.tar.gz'),
    os.path.join(data_path, 'tarfile.tar.xz'),
    os.path.join(data_path, 'zipfile.zip'),
    os.path.join(data_path, 'dir'),
    tarfile.open(os.path.join(data_path, 'tarfile.tar.gz')),
    zipfile.ZipFile(os.path.join(data_path, 'zipfile.zip')),
    open(os.path.join(data_path, 'zipfile.zip'), 'rb'),
    ])
def test_arlib_read(fname):
    if sys.version_info[0] >= 3:
        with arlib.open(fname, 'r') as ar:
            assert ar.member_names == ['a.txt', 'b.txt']
            with ar.open_member('a.txt', 'r') as f:
                assert f.read() == 'a'
            with ar.open_member('b.txt', 'r') as f:
                assert f.read() == 'b'
