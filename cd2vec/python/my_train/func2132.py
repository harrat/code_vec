def test_signal_weakref_with_identifier():
    import gc

    class Foo:
        def on_test_identifier(self, a, b):
            a / b

    foo = Foo()
    foo.obj = 'obj'
    register_object(foo)

    with pytest.raises(ZeroDivisionError):
        on_test_identifier(a=5, b=0, obj='obj')
    on_test_identifier(a=5, b=0, obj='noobj')

    del foo
    gc.collect()

    on_test_identifier(a=5, b=0, obj='obj')
    on_test_identifier(a=5, b=0, obj='noobj')

