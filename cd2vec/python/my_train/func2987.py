def test_slow_connection(self):
        with test_utils.lauch(self.base_config) as cfg:
            with test_utils.sock(cfg["SOCKET"]) as s:
                msg = test_utils.postfix_request(sasl_username="test")
                i = 0
                s.send(msg[i:10])
                i += 10
                # run another request before the previous one is ended
                data = test_utils.send_policyd_request(cfg["SOCKET"], sasl_username="test")
                self.assertEqual(data.strip(), b"action=dunno")
                s.send(msg[i:1])
                i += 1
                data = test_utils.send_policyd_request(cfg["SOCKET"], sasl_username="test")
                self.assertEqual(data.strip(), b"action=dunno")
                s.send(msg[i:])
                datal = []
                datal.append(s.recv(2))
                # run another request before the previous one is ended
                data = test_utils.send_policyd_request(cfg["SOCKET"], sasl_username="test")
                self.assertEqual(data.strip(), b"action=dunno")
                datal.append(s.recv(1024))
                data = b"".join(datal)
                self.assertEqual(data.strip(), b"action=dunno")
