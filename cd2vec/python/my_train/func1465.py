def test_async_import():
    gqlmod.enable_gql_import()
    import queries_async  # noqa
    prov = MockGithubProvider()
    with _mock_provider('github-async', prov):
        queries_async.Login()
        assert prov.last_vars['__previews'] == set()
