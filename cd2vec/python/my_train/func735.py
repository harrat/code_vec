def test_utils_update(path):
    utils.update(path, use_cache_server=False)

    mtime = os.path.getmtime(path)

    _load = utils.load

    with mock.patch(
        'fake_useragent.utils.load',
        side_effect=_load,
    ) as mocked:
        utils.update(path, use_cache_server=False)

        mocked.assert_called()

    assert os.path.getmtime(path) != mtime

