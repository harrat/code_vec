def test_hosts_add(self, centreon_con):
        values = [
            "new_host.tld",
            "new_host",
            "127.0.0.7",
            "OS-Linux-SNMP-custom|OS-Linux-SNMP-disk",
            "Central",
            "hg"
        ]
        data = dict()
        data['action'] = 'add'
        data['object'] = 'HOST'
        data['values'] = values

        with patch('requests.post') as patched_post:
            centreon_con.hosts.add(
                "new_host.tld",
                "new_host",
                "127.0.0.7",
                "Central",
                ["OS-Linux-SNMP-custom", "OS-Linux-SNMP-disk"],
                ["hg"],
                post_refresh=False
            )
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
