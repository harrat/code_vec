def test_less(self):
        r1 = ruler.DirSizeRule("< 8888b")
        r2 = ruler.DirSizeRule("< 8889b")

        self.assertFalse(r1.match(self.f2))
        self.assertTrue(r2.match(self.f2))
