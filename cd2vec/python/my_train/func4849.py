def test_cant_parse(config, mock_env):
    mock_env({'ATTRIBUTE_INT': 'string'})
    with pytest.raises(AttributeError):
        print(config.ATTRIBUTE_INT)
