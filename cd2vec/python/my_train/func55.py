@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'default-snippets-utc')
    def test_cli_import_snippet_005(snippy):
        """Import all snippet resources.

        Import all snippets from text file. File name and format are extracted
        from command line ``--file`` option. File extension is '*.text' in this
        case.
        """

        content = {
            'data': [
                Content.deepcopy(Snippet.REMOVE),
                Content.deepcopy(Snippet.FORCED)
            ]
        }
        content['data'][0]['uuid'] = Content.UUID1
        content['data'][1]['uuid'] = Content.UUID2
        file_content = Content.get_file_content(Content.TEXT, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '-f', './all-snippets.text'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './all-snippets.text', mode='r', encoding='utf-8')
