def test_list_config(self, config):
        config.register_config(name='test1', config_type=str, default='test1')
        config.register_config(name='test2', config_type=str, default='test2')
        config.register_config(name='test3', config_type=str, default='test3')
        assert set(config.list_config()) == {'test1', 'test2', 'test3'}
        config.unregister_config(name='test1')
        config.unregister_config(name='test3')
        assert config.list_config() == ['test2']
