@pytest.mark.vcr()
def test_workbench_vulns_severity_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.vulns(severity='something else')
