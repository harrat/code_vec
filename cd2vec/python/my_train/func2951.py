def test_update(git_dir):
    git_dir.chdir()
    os.environ.update({"GIT_DIR": str(git_dir.join(".git"))})
    with pytest.raises(SystemExit):
        dzonegit.update(["update", "refs/heads/slave", "0", "0"])
    dzonegit.update([
        "update", "refs/heads/master",
        "0"*40, dzonegit.get_head(),
    ])
