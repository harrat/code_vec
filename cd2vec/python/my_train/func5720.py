def test_line(self):
        line = Line.parse('(fp_line (start 0 0) (end 1 1) (layer layer))')
        assert line.start == [0.0, 0.0]
        assert line.end == [1.0, 1.0]
        assert line.layer == 'layer'
        assert Line.parse(line.to_string()) == line
