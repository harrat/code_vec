def test_get_etag_no_match(self):
        """Does GETing a resource return the 'Link' header field?"""
        response = self.get_response('/tracks/1', 200)
        assert 'ETag' in response.headers
        cached_response = self.get_response('/tracks/1', 412, headers={'If-Match': 'foo'}, has_data=False)
