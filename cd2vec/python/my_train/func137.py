def test_rvalue_2_layer_construction(self):
        self.idf.initreadtxt(double_layer)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        expected = (
            INSIDE_FILM_R
            + m.Thickness / m.Conductivity
            + m.Thickness / m.Conductivity
            + OUTSIDE_FILM_R
        )
        assert c.rvalue == expected
        assert c.rvalue == 0.55
