@pytest.mark.parametrize(('value', 'expected_serialization'), [
    # TODO the trailing ... for non-container values is kinda weird
    (None, "null\n...\n"),
    ('???????', "???????\n...\n"),
    (b'bytes', "!!binary |\n  Ynl0ZXM=\n"),
    (True, "true\n...\n"),
    (133, "133\n...\n"),
    # long, for python 2
    (2**133, "10889035741470030830827987437816582766592\n...\n"),
    (3.52, "3.52\n...\n"),
    ([1, 2, 'three'], "- 1\n- 2\n- three\n"),
    ({'x': 7, 'y': 8, 'z': 9}, "x: 7\ny: 8\nz: 9\n"),
    # TODO this should use ? notation
    (set("qvx"), "!!set\nq: null\nv: null\nx: null\n"),
    (datetime.date(2015, 10, 21), "2015-10-21\n...\n"),
    (datetime.datetime(2015, 10, 21, 4, 29), "2015-10-21 04:29:00\n...\n"),
    # TODO case with timezone...  unfortunately can't preserve the whole thing
    (collections.OrderedDict([('a', 1), ('b', 2), ('c', 3)]), "!!omap\n- a: 1\n- b: 2\n- c: 3\n"),
])
def test_basic_roundtrip(value, expected_serialization):
    camel = Camel()
    dumped = camel.dump(value)
    assert dumped == expected_serialization
    assert camel.load(dumped) == value

