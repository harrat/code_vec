def test_before_middleware(self, monkeypatch, TestMockEntity, mock_token):
        app = Flask(__name__)
        @app.route("/test", methods=["GET"])
        def test_one():
            return "/test"
        # Manually set the primary key
        entity = TestMockEntity(id=1, user_name="joe")

        ctx = app.test_request_context("/test")
        ctx.push()

        assert entity.user_name == "joe"
        assert entity.id == 1

        self.ext.entity_models = [TestMockEntity]
        entity = Entity(self.ext)
        routing = Routing(app, self.ext, entity)

        with ctx:
            # token from args
            monkeypatch.setattr("flask.request.args", MockArgs(mock_token))
            routing.before_middleware()
            assert ctx.g.test_entities == [(1, 'joe')]

        with ctx:
            # token from headers
            monkeypatch.setattr("flask.request.args", MockArgs())
            monkeypatch.setattr("flask.request.headers", MockArgs(mock_token, True))
            routing.before_middleware()
            assert ctx.g.test_entities == [(1, 'joe')]
