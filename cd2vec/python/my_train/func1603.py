def test_get_msr_paraphrase(self):
        raw = get_msr_paraphrase()
        params = [('train', 3_962), ('test', 1_650)]
        for key, size in params:
            with self.subTest(key=key, size=size):
                self.assertIn(key, raw)
                self.assertEqual(len(raw[key]), size)
                self.assertEqual(len(MsrParaphrase(split=key)), size)
