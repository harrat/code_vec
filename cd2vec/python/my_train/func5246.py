def test_reserved_property_severity(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['severity'] = "Something"
        results = validate_parsed_json(indicator, self.options)
        self.assertEqual(results.is_valid, False)
