def test_load_with_pysettings_file_ignores_lowercased_locales(self, pysettings_file):
        fadapter = File(pysettings_file.name)
        fadapter.load()

        assert "should_be_ignored" not in fadapter.data
