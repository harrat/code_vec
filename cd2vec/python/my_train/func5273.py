def test_duplicate_sheet_type(self):
        # Important! This test needs to be executed after test_create_sheets, as it clones that sheet #
        duplicated_sheet = self.spreadsheet.duplicate_sheet(new_sheet_name="cloned_sheet", sheet_name="test_sheet")
        assert isinstance(duplicated_sheet, Sheet)
        assert duplicated_sheet.name == 'cloned_sheet'
