def test_defined_rounded():
    eta._NOW = lambda: 1411868723.5
    progress_bar = ProgressBarYum(1023, 'long_file_name.iso')

    assert 'long_fil   0% [          ] --- KiB/s |   0.0 B              ' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 1022
    assert 'long_fil  99% [=========-] --- KiB/s | 1.0 KiB              ' == str(progress_bar)

    eta._NOW = lambda: 1411868724.5
    progress_bar.numerator = 1023
    assert 'long_file_name.iso                   | 1.0 KiB  00:00:01    ' == str(progress_bar)

