def test_fileinput(self):
        file1 = self.root.make_file(suffix='.gz')
        with gzip.open(file1, 'wt') as o:
            o.write('foo\nbar\n')
        with textinput(file1) as i:
            lines = list(i)
            self.assertListEqual(['foo\n', 'bar\n'], lines)
        file2 = self.root.make_file(suffix='.gz')
        with gzip.open(file2, 'wt') as o:
            o.write('baz\n')
        with textinput((file1, file2)) as i:
            lines = list(i)
            self.assertListEqual(['foo\n', 'bar\n', 'baz\n'], lines)
        with textinput([('key1', file1), ('key2', file2)]) as i:
            assert i.filekey is None
            assert i.filename is None
            assert i.lineno == 0
            assert i.filelineno == 0
