@staticmethod
    @pytest.mark.usefixtures('json', 'default-solutions', 'export-time')
    def test_cli_export_solution_024(snippy):
        """Export solution with search keyword.

        Export defined solution based on search keyword. File name is defined
        in solution metadata and in command line -f|--file option. This should
        result the file name and json format defined by the command line
        option.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Solution.BEATS
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '--sall', 'beats', '-f', './defined-solution.json'])
            assert cause == Cause.ALL_OK
            Content.assert_json(json, mock_file, './defined-solution.json', content)
