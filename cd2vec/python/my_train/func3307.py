def test_aggregator_keys_method(mocked_aggregated):
    """Test aggregator keys method."""

    assert mocked_aggregated.keys('pattern') == ['node3', 'pattern']

