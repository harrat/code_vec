def test_bedmap2_file_name_only():
    "Only fetch the file name."
    names = fetch_bedmap2(datasets=["bed"], load=False)
    assert len(names) == 1
    assert names[0].endswith("bedmap2_bed.tif")
    names = fetch_bedmap2(datasets=["thickness", "surface", "geoid"], load=False)
    assert len(names) == 3
    assert names[0].endswith("bedmap2_thickness.tif")
    assert names[1].endswith("bedmap2_surface.tif")
    assert names[2].endswith("gl04c_geiod_to_WGS84.tif")

