def test_insert_simple(db, basic_module, auto_inputs_module):
    auto_inputs_module.insert_simple(db, "username without type defined before!")
    assert basic_module.count_all(db) == 1

