@pytest.mark.parametrize("fn", [
    "test/files/rar3-readonly-unix.rar",
    "test/files/rar3-readonly-win.rar",
    "test/files/rar5-readonly-unix.rar",
    "test/files/rar5-readonly-win.rar",
])
def test_readonly(fn, tmp_path):
    with rarfile.RarFile(fn) as rf:
        assert get_props(rf, "ro_dir") == "-D-"
        assert get_props(rf, "ro_dir/ro_file.txt") == "F--"

        rf.extractall(tmp_path)

    assert os.access(tmp_path / "ro_dir/ro_file.txt", os.R_OK)
    assert not os.access(tmp_path / "ro_dir/ro_file.txt", os.W_OK)

    if sys.platform != "win32":
        assert os.access(tmp_path / "ro_dir", os.R_OK)
        assert not os.access(tmp_path / "ro_dir", os.W_OK)

