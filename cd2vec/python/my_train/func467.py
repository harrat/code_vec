def test_invalid_pattern(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['pattern'] = """[file:hashes."SHA-256" = 'aec070645fe53ee3b3763059376134f058cc337247c978add178b6ccdfb0019f']"""
        self.assertFalseWithOptions(indicator)
