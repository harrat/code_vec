def test_get_access_token(self):
        member1 = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        address = member1.add_address(generate_nonce(), utils.generate_address())
        payload = AccessTokenBuilder.create_with_alias(member1.get_first_alias()).for_address(address.id).build()
        access_token = member1.create_access_token(payload)
        result = member1.get_token(access_token.id)
        assert access_token == result
