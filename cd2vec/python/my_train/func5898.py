def test_programming_error():
    assert issubclass(py2jdbc.IntegrityError, py2jdbc.DatabaseError), \
        "IntegrityError is not a subclass of DatabaseError"
