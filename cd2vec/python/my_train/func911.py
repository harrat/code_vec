def test_drill_offset(self):
        drill = Drill.parse('(drill 0.8 (offset 0.1 0.2))')
        assert drill.size == 0.8 and drill.offset == [0.1, 0.2]
        assert Drill.parse(drill.to_string()) == drill
