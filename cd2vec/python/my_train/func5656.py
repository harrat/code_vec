def test_add_inactive_project(cli, data_dir):
    project = Project(1, 'test project', Project.STATUS_FINISHED)
    project.activities = [Activity(2, 'test activity')]
    p = ProjectsDb(str(data_dir))
    p.update([project])

    output = cli('project', ['alias', 'test project'], input='test_alias')

    assert "No active project matches your search string" in output

