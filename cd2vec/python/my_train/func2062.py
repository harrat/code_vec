@pytest.mark.vcr()
def test_workbench_assets_filter_type_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.assets(filter_type='NOT')
