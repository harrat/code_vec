def test_223_report_generated(self):
        generate_report(tests.PROJECT_INFO_ENHANCED, 'output.csv')
        with open('output.csv', 'r') as f:
            result = list(csv.reader(f))
            self.assertNotEqual(result[0], result[1])
            with self.assertRaises(IndexError):
                print("report created so there's only two rows: {} should raise en error".format(result[2]))
        os.remove('output.csv')
