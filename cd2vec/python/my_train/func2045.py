@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'yaml', 'import-gitlog', 'update-pytest-utc')
    def test_cli_import_reference_011(snippy):
        """Import reference based on message digest.

        Import defined reference based on message digest without specifying
        the content category explicitly. One line in the reference data was
        updated. The updated content is timestamped with regexp content time.
        """

        content = {
            'data': [
                Content.deepcopy(Reference.GITLOG)
            ]
        }
        content['data'][0]['links'] = ('https://updated-link.html',)
        content['data'][0]['updated'] = Content.PYTEST_TIME
        content['data'][0]['digest'] = 'fafd46eca7ca239bcbff8f1ba3e8cf806cadfbc9e267cdf6ccd3e23e356f9f8d'
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '-d', '5c2071094dbfaa33', '-f', 'one-reference.yaml'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-reference.yaml', mode='r', encoding='utf-8')
