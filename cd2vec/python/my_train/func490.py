@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'export-time')
    def test_cli_export_solution_037(snippy):
        """Export all solutions.

        Export all snippets in Markdown format.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '-f', './all-solutions.md'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './all-solutions.md', content)
