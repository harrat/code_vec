@pytest.mark.parametrize('test_number', range(10))
def test_dependencies(mocker, repo, test_number):
    """ This test will run 10 times, when each time it will randomly generate dependencies in the repo and verify that
        the expected dependencies has been updated in the pack metadata correctly.

        Given
        - Content repository
        When
        - Running find_dependencies
        Then
        - Update packs dependencies in pack metadata
    """
    number_of_packs = 10
    repo.setup_content_repo(number_of_packs)
    repo.setup_one_pack('CommonTypes')

    pack_to_verify = random.choice(range(number_of_packs))

    number_of_methods_to_choose = random.choice(range(1, len(METHODS_POOL)))
    dependencies = run_random_methods(repo, pack_to_verify, METHODS_POOL.copy(), number_of_methods_to_choose)

    with ChangeCWD(repo.path):
        # Circle froze on 3.7 dut to high usage of processing power.
        # pool = Pool(processes=cpu_count() * 2) is the line that in charge of the multiprocessing initiation,
        # so changing `cpu_count` return value to 1 still gives you multiprocessing but with only 2 processors,
        # and not the maximum amount.
        import demisto_sdk.commands.common.update_id_set as uis
        mocker.patch.object(uis, 'cpu_count', return_value=1)
        PackDependencies.find_dependencies(f'pack_{pack_to_verify}', silent_mode=True)

    dependencies_from_pack_metadata = repo.packs[pack_to_verify].pack_metadata.read_json_as_dict().get(
        'dependencies').keys()

    if f'pack_{pack_to_verify}' in dependencies:
        dependencies.remove(f'pack_{pack_to_verify}')

    assert IsEqualFunctions.is_lists_equal(list(dependencies), list(dependencies_from_pack_metadata))

