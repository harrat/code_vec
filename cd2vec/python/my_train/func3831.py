@pytest.mark.parametrize("actress", ["????", "?????", "????", "???", "Nao Jinguuji", "Eimi Fukada"])
def test_search_by_actress(actress):
    rv = client.post(
        "/search_by_actress",
        data=json.dumps({"actress": actress, "with_profile": "true", "userpass": get_userpass()})
    )
    rsp = json.loads(rv.data.decode("utf-8"))
    assert len(rsp["history_names"]) > 0
    assert len(rsp["videos"]) > 0

