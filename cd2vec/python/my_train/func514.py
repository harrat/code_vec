def test_data_region_ground_survey():
    """
    Test if ground survey is changed against a different data_region
    """
    data_region = (10, 30, -30, -12)  # a bigger data_region than the default one
    region = (-10.1, 9.7, -20.3, -10.5)  # a random region to scale the survey
    survey = ground_survey(region=region, data_region=data_region)
    assert survey.longitude.size > 963
    npt.assert_allclose(survey.longitude.min(), region[0])
    npt.assert_allclose(survey.longitude.max(), region[1])
    npt.assert_allclose(survey.latitude.min(), region[2])
    npt.assert_allclose(survey.latitude.max(), region[3])
    assert survey.height.min() <= 0.0
    assert survey.height.max() >= 2052.2

