@staticmethod
    def test_quiet_option_001(snippy, capsys, caplog):
        """Test supressing all output from tool.

        Disable all logging and output to terminal. Only the printed content
        is displayed on the screen.
        """

        snippy = Snippy(['snippy', 'search', '--sall', '.', '-q'])
        cause = snippy.run()
        snippy.release()
        out, err = capsys.readouterr()
        assert cause == 'NOK: cannot find content with given search criteria'
        assert not out
        assert not err
        assert not caplog.records[:]
