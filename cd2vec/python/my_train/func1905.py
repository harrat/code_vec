def test_process_error(self):
        p = popen(('exit', '2'), shell=True)
        with self.assertRaises(IOError):
            p.close1(raise_on_error=True)
        self.assertFalse(p.returncode == 0)
