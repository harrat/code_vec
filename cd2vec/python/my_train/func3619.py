def test_read_json(json_file):
    with open(str(json_file)) as f:
        products_data = json.load(f)
    for item in products_data:
        assert item["uuid"]
        assert item["name"]
        assert item["price"]

