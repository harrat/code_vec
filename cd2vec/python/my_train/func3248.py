def test_get_db(self, mock_sp, client: DbsModule, shared):
        db = shared['db']
        mock_sp.return_value = DbMock('get_db')

        response = client.get_db(db.id)

        assert response.id == db.id
        assert response.appid == db.appid
        assert response.serverid == db.serverid
