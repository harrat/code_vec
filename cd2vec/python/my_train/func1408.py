@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_update_snippet_008(snippy):
        """Update snippet with ``--digest`` option.

        Try to update snippet with one digit digest that matches two snippets.

        NOTE! Don't not change the test snippets because this case is produced
        with real digests that just happen to have same digit starting both of
        the cases.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        cause = snippy.run(['snippy', 'update', '-d', '5', '--format', 'text'])
        assert cause == 'NOK: content digest 5 matched 2 times preventing update operation'
        Content.assert_storage(content)
