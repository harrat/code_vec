def test_sign_invalid_signing_key(self):
        """
        Test that the right error is thrown when an invalid signing key
        is specified with a Sign request.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        signing_key = pie_objects.OpaqueObject(
            b'\x01\x02\x03\x04',
            enums.OpaqueDataType.NONE
        )

        e._data_session.add(signing_key)
        e._data_session.commit()
        e.data_session = e._data_store_session_factory()

        unique_identifier = str(signing_key.unique_identifier)
        payload = payloads.SignRequestPayload(
            unique_identifier=unique_identifier,
            cryptographic_parameters=attributes.CryptographicParameters(
                padding_method=enums.PaddingMethod.PSS,
                digital_signature_algorithm=enums.DigitalSignatureAlgorithm.
                SHA1_WITH_RSA_ENCRYPTION
            ),
            data=(
                b'\x01\x02\x03\x04\x05\x06\x07\x08'
                b'\x09\x10\x11\x12\x13\x14\x15\x16'
            )
        )

        args = (payload, )
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            "The requested signing key is not a private key. "
            "A private key must be specified.",
            e._process_sign,
            *args
        )
