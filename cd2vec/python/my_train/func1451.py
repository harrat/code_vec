def test_specnorm(self):
        layer_test(
            TimeDelayLayer1D, kwargs={'filters': 4,
                                      'kernel_normalizer': 'spectral',
                                      'kernel_regularizer': 'orthogonal'}, input_shape=(5, 32, 3))
