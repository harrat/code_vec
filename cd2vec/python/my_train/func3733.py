def test_nuid_are_very_unique(self):
        nuid = NUID()
        entries = [nuid.next().decode() for i in range(1000000)]
        counted_entries = Counter(entries)
        repeated = [
            entry for entry, count in counted_entries.items() if count > 1
        ]
        self.assertEqual(len(repeated), 0)
