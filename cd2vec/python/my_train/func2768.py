def test_annual_no_balance(self, annual_stockrow_stmts_cat: FinancialStatements):
        super().test_annual(
            annual_stockrow_stmts_cat, data=FCST_STOCKROW_CAT_NO_BALANCE_A_INDEX_DATA_DICT, balance=False
        )
