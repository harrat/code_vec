@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_hello_world(fixture, request):
    engine = request.getfixturevalue(fixture)
    job = engine.launch('alpine', 'echo hello world')
    print(job.rundata)
    job.wait()
    assert job.stdout.strip() == 'hello world'

