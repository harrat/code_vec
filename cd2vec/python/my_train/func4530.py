@project_setup_py_test("hello-pure", ["sdist"], disable_languages_test=True)
def test_hello_pure_sdist():
    sdists_tar = glob.glob('dist/*.tar.gz')
    sdists_zip = glob.glob('dist/*.zip')
    assert sdists_tar or sdists_zip

    expected_content = [
        'hello-pure-1.2.3/hello/__init__.py',
        'hello-pure-1.2.3/setup.py',
    ]

    sdist_archive = 'dist/hello-pure-1.2.3.zip'
    if sdists_tar:
        sdist_archive = 'dist/hello-pure-1.2.3.tar.gz'

    check_sdist_content(sdist_archive, 'hello-pure-1.2.3', expected_content)

