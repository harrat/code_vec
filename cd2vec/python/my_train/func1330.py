def test_iterate_results(self, connected_adapter):
        """Test iterating over stored documents on Ceph."""
        document1, key1 = {'thoth': 'document'}, 'key-1'
        document2, key2 = {'just': 'dict'}, 'key-2'

        for document_id, document in connected_adapter.iterate_results():
            if document_id == key1:
                assert document == document1
            elif document_id == key2:
                assert document == document2
            else:
                assert False, "The retrieved document was not previously stored."
