def test_get_web_user_info_failure(self):
        """Test get user info failure"""

        access_token, openid = 'access_token', 'openid'
        result = WechatAPI.get_web_user_info(access_token, openid)
        self.assertIn('errmsg', result)
        self.assertIn('requrl', result)
