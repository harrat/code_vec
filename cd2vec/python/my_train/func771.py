def test_get_last_request(self):
        self._make_request('https://python.org')
        self._make_request('https://www.bbc.co.uk/')

        last_request = self.client.get_last_request()

        self.assertEqual('https://www.bbc.co.uk/', last_request['url'])
