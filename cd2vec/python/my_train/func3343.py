def test_scan_video_broken(mkv, tmpdir, monkeypatch):
    broken_path = 'test1.mkv'
    with io.open(mkv['test1'], 'rb') as original:
        with tmpdir.join(broken_path).open('wb') as broken:
            broken.write(original.read(512))
    monkeypatch.chdir(str(tmpdir))
    scanned_video = scan_video(broken_path)
    assert type(scanned_video) is Movie
    assert scanned_video.name == str(broken_path)
    assert scanned_video.source is None
    assert scanned_video.release_group is None
    assert scanned_video.resolution is None
    assert scanned_video.video_codec is None
    assert scanned_video.audio_codec is None
    assert scanned_video.imdb_id is None
    assert scanned_video.hashes == {}
    assert scanned_video.size == 512
    assert scanned_video.subtitle_languages == set()
    assert scanned_video.title == 'test1'
    assert scanned_video.year is None

