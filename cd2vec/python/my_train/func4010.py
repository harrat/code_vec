def test_loading():
    with open(file_path, "w") as f:
        f.write("""
from breathe import Breathe
from ..testutils import DoNothing

Breathe.add_commands(
    None,
    {
        "apple": DoNothing(),
    }
)
"""
        )
    engine.mimic("rebuild everything test")
    engine.mimic("apple")

def test_reloading():
    with open(file_path, "w") as f:
        f.write("""
from breathe import Breathe
from ..testutils import DoNothing
