@pytest.mark.parametrize('start,end,granularity',
                             [(current_time - relativedelta(months=1),
                               current_time, 21600)])
    def test_get_historic_rates(self, client, start, end, granularity):
        r = client.get_product_historic_rates('BTC-USD', start=start, end=end, granularity=granularity)
        assert type(r) is list
        for ticker in r:
            assert( all( [type(x) in (int, float) for x in ticker ] ) )
