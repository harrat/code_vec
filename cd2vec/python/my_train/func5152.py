@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'export-time')
    def test_cli_export_snippet_022(snippy):
        """Export snippet template.

        Export snippet template. This should result file name and format based
        on tool internal settings.
        """

        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--template'])
            assert cause == Cause.ALL_OK
            mock_file.assert_called_once_with('./snippet-template.mkdn', mode='w', encoding='utf-8')
            file_handle = mock_file.return_value.__enter__.return_value
            file_handle.write.assert_called_with(Const.NEWLINE.join(Snippet.TEMPLATE_MKDN))
