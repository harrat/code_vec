@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_delete_solution_008(snippy):
        """Delete solution with digest.

        Try to delete solution with short version of digest that does not
        match to any existing message digest.
        """

        content = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--scat', 'solution', '-d', '123456'])
        assert cause == 'NOK: cannot find content with message digest: 123456'
        Content.assert_storage(content)
