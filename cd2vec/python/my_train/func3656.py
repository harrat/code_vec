@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'import-kafka')
    def test_api_delete_solution_001(server):
        """Delete solution with digest.

        Send DELETE /solutions/{id} to delete one resource. The ``id`` in
        URI matches to one resource that is deleted.
        """

        content = {
            'data': [
                Storage.ebeats,
                Storage.dnginx
            ]
        }
        expect_headers = {}
        result = testing.TestClient(server.server.api).simulate_delete(
            path='/api/snippy/rest/solutions/ee3f2ab7c63d696',
            headers={'accept': 'application/json'})
        assert result.headers == expect_headers
        assert result.status == falcon.HTTP_204
        assert not result.text
        Content.assert_storage(content)
