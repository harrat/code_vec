@skipIf(gz_path is None, "'gzip' not available")
    def test_iter_system(self):
        path = self.root.make_file(suffix='.gz')
        text = 'line1\nline2\nline3'
        fmt = get_format('.gz')
        # Have to open in bytes mode, or it will get wrapped in a
        # TextBuffer, which does not use the underlying __iter__
        with fmt.open_file(path, mode='wb', ext='.gz', use_system=True) as f:
            f.write(text.encode())
        with fmt.open_file(path, mode='rb', ext='.gz', use_system=True) as f:
            lines = list(line.rstrip().decode() for line in iter(f))
        self.assertListEqual(lines, ['line1', 'line2', 'line3'])
