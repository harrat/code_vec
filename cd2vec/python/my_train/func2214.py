def test_write(self):
        from datafactory.exceptions import OutputFileAlreadyExists
        sf1 = self._get_class([1])
        sf1.write(self.filename)

        self.assertEqual(
            open(self.filename).read(),
            sf1.stringify()
        )

        sf2 = self._get_class([2])
        with self.assertRaises(OutputFileAlreadyExists):
            sf2.write(self.filename)

        sf2.write(self.filename, rewrite=True)
        self.assertEqual(
            open(self.filename).read(),
            sf2.stringify()
        )
