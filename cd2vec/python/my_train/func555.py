def test_pathvar(self):
        pv = StrPathVar('id', pattern='[A-Z0-9_]+', default='ABC123')
        assert 'ABC123' == pv(None)

        pv = StrPathVar('id', pattern='[A-Z0-9_]+', optional=True)
        assert '' == pv(None)

        pv = StrPathVar('id', pattern='[A-Z0-9_]+')
        with self.assertRaises(ValueError):
            pv(None)
