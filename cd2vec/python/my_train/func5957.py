def test_invalid_ipv6(self):
        values = [
            "foo",
            "fe80:0000:0000:0000:0204:61ff:fe9d:f156/",
            "fe80:0000:0000:0000:0204:61ff:fe9d:f156/129",
            "fe80:0000:0000:0000:0204:61ff:fe9d:f156/a",
            "fe80:0000:0000:0000:0204:61ff:fe9d:f156/00",
            "fe80:0000:0000:0000:0204:61ff:fe9d:f156/03",
        ]
        ipv6 = copy.deepcopy(self.valid_ipv6)
        for value in values:
            ipv6['value'] = value
            self.assertFalseWithOptions(ipv6)
