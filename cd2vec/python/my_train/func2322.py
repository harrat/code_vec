def test_parse_udf_onedir(tmpdir):
    indir = tmpdir.mkdir('udfonedir')
    outfile = str(indir)+'.iso'
    indir.mkdir('dir1')
    subprocess.call(['genisoimage', '-v', '-v', '-no-pad', '-iso-level', '1',
                     '-udf', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_udf_onedir)

def test_parse_udf_twodirs(tmpdir):
    indir = tmpdir.mkdir('udftwodirs')
    outfile = str(indir)+'.iso'
    indir.mkdir('dir1')
    indir.mkdir('dir2')
    subprocess.call(['genisoimage', '-v', '-v', '-no-pad', '-iso-level', '1',
                     '-udf', '-o', str(outfile), str(indir)])
