def test_parse_deep_rr_symlink(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('sevendeepdirs')
    outfile = str(indir)+'.iso'
    dir7 = indir.mkdir('dir1').mkdir('dir2').mkdir('dir3').mkdir('dir4').mkdir('dir5').mkdir('dir6').mkdir('dir7')
    pwd = os.getcwd()
    os.chdir(str(dir7))
    os.symlink('/usr/share/foo', 'sym')
    os.chdir(pwd)
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-rational-rock', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_deep_rr_symlink)

def test_parse_rr_deep_weird_layout(tmpdir):
    indir = tmpdir.mkdir('rrdeepweird')
    outfile = str(indir) + '.iso'
    absimp = indir.mkdir('astroid').mkdir('astroid').mkdir('tests').mkdir('testdata').mkdir('python3').mkdir('data').mkdir('absimp')
    sidepackage = absimp.mkdir('sidepackage')
    with open(os.path.join(str(absimp), 'string.py'), 'wb') as outfp:
        outfp.write(b'from __future__ import absolute_import, print_functino\nimport string\nprint(string)\n')
    with open(os.path.join(str(sidepackage), '__init__.py'), 'wb') as outfp:
        outfp.write(b'"""a side package with nothing in it\n"""\n')
