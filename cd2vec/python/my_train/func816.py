@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_solution_001(snippy):
        """Import all solution resources.

        Import all solutions. File name is not defined in command line. This
        should result tool internal default file name and format being used.
        """

        content = {
            'data': [
                Solution.KAFKA,
                Solution.BEATS
            ]
        }
        file_content = Content.get_file_content(Content.MKDN, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'solution'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './solutions.mkdn', mode='r', encoding='utf-8')
