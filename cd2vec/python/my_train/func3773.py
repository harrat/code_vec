def test_modify_attribute_kmip_2_0(self):
        """
        Test that a ModifyAttribute request can be processed correctly with
        KMIP 2.0 parameters.
        """
        e = engine.KmipEngine()
        e._protocol_version = contents.ProtocolVersion(2, 0)
        e._attribute_policy._version = e._protocol_version
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        # Confirm that the attribute is set to its default value by
        # fetching the managed object fresh from the database and
        # checking it.
        managed_object = e._get_object_with_access_controls(
            "1",
            enums.Operation.MODIFY_ATTRIBUTE
        )
        self.assertFalse(managed_object.sensitive)

        payload = payloads.ModifyAttributeRequestPayload(
            unique_identifier="1",
            current_attribute=objects.CurrentAttribute(
                attribute=primitives.Boolean(
                    False,
                    tag=enums.Tags.SENSITIVE
                )
            ),
            new_attribute=objects.NewAttribute(
                attribute=primitives.Boolean(
                    True,
                    tag=enums.Tags.SENSITIVE
                )
            )
        )

        response_payload = e._process_modify_attribute(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call(
            "Processing operation: ModifyAttribute"
        )
        self.assertEqual("1", response_payload.unique_identifier)
        self.assertIsNone(response_payload.attribute)

        # Confirm that the attribute was actually set by fetching the
        # managed object fresh from the database and checking it.
        managed_object = e._get_object_with_access_controls(
            response_payload.unique_identifier,
            enums.Operation.MODIFY_ATTRIBUTE
        )
        self.assertTrue(managed_object.sensitive)
