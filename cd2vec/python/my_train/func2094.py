def test_computeLRF1(self):
        # introduce 3 random edits and compute the distance
        t2 = mutateLabeledTree(self.t1, 3)
        res = computeLRF(self.t1,t2)
        self.assertEqual(res,3)

        # introduce 5 random edits and compute the distance
        t2 = mutateLabeledTree(self.t1, 5)
        res = computeLRF(self.t1,t2)
        self.assertEqual(res,5)
