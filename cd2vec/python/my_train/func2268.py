def test_synchronize_join_node(tmpdir):
    """Test join on an input node which has the ``synchronize`` flag set to True."""
    global _products
    _products = []
    tmpdir.chdir()

    # Make the workflow.
    wf = pe.Workflow(name="test")
    # the iterated input node
    inputspec = pe.Node(IdentityInterface(fields=["m", "n"]), name="inputspec")
    inputspec.iterables = [("m", [1, 2]), ("n", [3, 4])]
    inputspec.synchronize = True
    # two pre-join nodes in a parallel iterated path
    inc1 = pe.Node(IncrementInterface(), name="inc1")
    wf.connect(inputspec, "m", inc1, "input1")
    inc2 = pe.Node(IncrementInterface(), name="inc2")
    wf.connect(inputspec, "n", inc2, "input1")
    # the join node
    join = pe.JoinNode(
        IdentityInterface(fields=["vector1", "vector2"]),
        joinsource="inputspec",
        name="join",
    )
    wf.connect(inc1, "output1", join, "vector1")
    wf.connect(inc2, "output1", join, "vector2")
    # a post-join node
    prod = pe.MapNode(ProductInterface(), name="prod", iterfield=["input1", "input2"])
    wf.connect(join, "vector1", prod, "input1")
    wf.connect(join, "vector2", prod, "input2")

    result = wf.run()

    # there are 3 iterables expansions.
    # thus, the expanded graph contains 2 * 2 iteration pre-join nodes, 1 join
    # node and 1 post-join node.
    assert len(result.nodes()) == 6, "The number of expanded nodes is incorrect."
    # the product inputs are [2, 3] and [4, 5]
    assert _products == [8, 15], "The post-join products is incorrect: %s." % _products

