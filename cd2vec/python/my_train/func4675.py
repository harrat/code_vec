@pytest.mark.vcr()
def test_workbench_vulns_resolvable_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vulns(resolvable='nope')
