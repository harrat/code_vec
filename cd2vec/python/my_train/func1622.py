def test_add_start():
    start_time("test")

def test_today_report_err_one(capsys):
    today_report("test")
    captured = capsys.readouterr()
    assert captured.out == "[+] Generating daily report for test...\n[-] You are in the middle of tracking, end this session with `devtracker stop` before generation a report.\n"
