@staticmethod
    @pytest.mark.usefixtures('import-remove')
    def test_cli_delete_snippet_004(snippy):
        """Delete snippet with dgiest.

        Delete snippet with empty message digest when there is only one
        content stored. In this case the last content can be deleted with
        empty digest.
        """

        Content.assert_storage_size(1)
        cause = snippy.run(['snippy', 'delete', '-d', ''])
        assert cause == Cause.ALL_OK
        Content.assert_storage(None)
