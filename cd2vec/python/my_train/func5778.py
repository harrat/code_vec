@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_delete_reference_001(snippy):
        """Delete reference with digest.

        Delete reference with short 16 byte version of message digest.
        """

        content = {
            'data': [
                Reference.GITLOG
            ]
        }
        Content.assert_storage_size(2)
        cause = snippy.run(['snippy', 'delete', '-d', 'cb9225a81eab8ced'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
