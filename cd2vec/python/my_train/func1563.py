def test_ornl_df(self):
        ornl_lat, ornl_long = 35.9313167, -84.3104124
        df = daymet_timeseries(lon=ornl_long, lat=ornl_lat, start_year=2012, end_year=2012)

        self.assertTrue(df.year.count() == 365)
        self.assertTrue("tmax" in df.columns)
        self.assertTrue("tmin" in df.columns)
        self.assertTrue("prcp" in df.columns)
