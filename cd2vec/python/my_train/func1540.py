def test_loads_each_split(self):
        train = Snli(split='train')
        self.assertEqual(len(train), 550_152)
        dev = Snli(split='dev')
        self.assertEqual(len(dev), 10_000)
        test = Snli(split='test')
        self.assertEqual(len(test), 10_000)
