@settings(deadline=pd.Timedelta(milliseconds=5000), max_examples=7)
    @pytest.mark.parametrize(
        "explainer", lazy_fixtures([lime_explainer, shap_explainer])
    )
    @given(regressor=models(), X_y=numpy_X_y_matrices(min_value=-100, max_value=100))
    def test_predict(self, explainer, regressor, X_y):
        X, y = X_y
        regressor.fit(X, y)
        explainer.fit(regressor, X)

        test_matrix = X[:2, :]
        predictions = explainer.predict(test_matrix)
        self._check_predict_output(explainer, predictions, test_matrix)
