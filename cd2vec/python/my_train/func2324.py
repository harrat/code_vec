def test_de_tokens(self):
        results = API("de").retrieve_all()
        for part, part_data in results.items():
            for p in part_data:
                self.assertIsInstance(p, part_classes[part])
                self.assertIsNotNone(p.brand)
        self.assertIsNotNone(results.to_json())
