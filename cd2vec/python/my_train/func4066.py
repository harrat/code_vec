def test_load_img(self):
    img = imgutils.load_img(TestImgUtils.url_1, np.ndarray)
    self.assertLessEqual(img.shape[-1], 3, '# of channels is not as expected')
