def test_invalid_group_names(self):
        ''' Invalid group names. '''
        with self.assertRaisesRegexp(ValueError,
                                     r'\[barchart\] .*group names.*'):
            barchart.draw(self.axes, _data(), group_names=['a', 'b', 'c', 'd'])
