def test_unserialize_with_reading_in_utf8(cf):
    with pytest.raises(UnicodeDecodeError) as e:
        with open("/dev/random") as f:
            cf = cuckoofilter.CuckooFilter.unserialize(f)
