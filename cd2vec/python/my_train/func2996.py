def test_filters():
    q = { Name.family: 'Poaceae' }
    f = Filters.accepted
    unfiltered = powo.search(q)
    filtered = powo.search(q, filters = f)

    assert filtered.size() < unfiltered.size()

