def test_pipeline_raise_error_on_undeclared_dataframe(self):
        with pytest.raises(NameError):
            run('pipeline(df): {drop(.name)}')
