@pytest.mark.flaky(reruns=3, reruns_delay=5)
def test_output_timeout():
    # Check that timeout is determined based on last output, not based
    # on start time.
    code = r"""
import time
import sys
for j in range(3):
    sys.stdout.write('.')
    sys.stdout.flush()
    time.sleep(1.0)
"""
    output = util.check_output([sys.executable, "-c", code], timeout=1.5)
    assert output == '.'*3

    try:
        util.check_output([sys.executable, "-c", code], timeout=0.5)
    except util.ProcessError as e:
        assert e.retcode == util.TIMEOUT_RETCODE
    else:
        assert False, "Expected exception"
