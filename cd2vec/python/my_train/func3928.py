def test_actual_rate_slower_than_rate_limit():
    """Test to check we don't limit a iterator if it's slower already."""

    # Create dummy range of numbers.
    target = range(10)

    # Limit is set to be 100 second, with a range of 10
    # that would result in the list being consumed in 0.1s
    slow_iter = iterables.LimitedIterable(target, limit=100)

    start_time = time.perf_counter()
    # For each iteration sleep 0.3, for 10 iterations thats
    # 3 seconds total sleep time.
    consumed_target = []
    for i in slow_iter:
        time.sleep(0.3)
        consumed_target.append(i)
    end_time = time.perf_counter()

    expected = [i for i in target]
    time_taken = end_time - start_time
    assert consumed_target == expected, "Check to see results are correct."
    assert time_taken > 2.9, "Check to see rate limit works low."
    assert time_taken < 3.1, "Check to see rate limit works high."

