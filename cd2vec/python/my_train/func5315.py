def test_dump_as_dict(self):
        """
        A TaskTemplate instance can be dumped as a dictionary
        """
        task_tmpl = TaskTemplate('my-task-holder', config={'hello': 'world'})
        task_tmpl.topics = ['my-topic']

        expected_dict = {
            'name': 'my-task-holder',
            'timeout': None,
            'topics': ['my-topic'],
            'config': {'hello': 'world'}
        }

        task_dict = task_tmpl.as_dict()
        del task_dict['id']
        self.assertEqual(task_dict, expected_dict)

        # The dumped dict must be loadable by the `from_dict()` classmethod
        other_tmpl = TaskTemplate.from_dict(task_dict)
        self.assertIsInstance(other_tmpl, TaskTemplate)
