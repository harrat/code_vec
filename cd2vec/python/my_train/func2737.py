@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_roster_from_team_class(self, *args, **kwargs):
        flexmock(Team) \
            .should_receive('_parse_team_data') \
            .and_return(None)
        team = Team(team_data=None, rank=1, year='2018')
        mock_abbreviation = mock.PropertyMock(return_value='DET')
        type(team)._abbreviation = mock_abbreviation

        assert len(team.roster.players) == 2

        for player in team.roster.players:
            assert player.name in ['Jimmy Howard', 'Henrik Zetterberg']
        type(team)._abbreviation = None
