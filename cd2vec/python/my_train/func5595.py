def test_collapse_json(self):
		model.factory.auto_id_type = "uuid"
		model.factory.base_url = "http://lod.example.org/museum/"
		model.factory.context_uri = "https://linked.art/ns/v1/linked-art.json"
		p = model.Person()
		p.classified_as = model.Type(ident="http://example.org/Type", label="Test")
		res1 = model.factory.toString(p, compact=False, collapse=60) # all new lines
		res2 = model.factory.toString(p, compact=False, collapse=120) # compact list of type
		self.assertEqual(len(res1.splitlines()), 12)
		self.assertEqual(len(res2.splitlines()), 6)
