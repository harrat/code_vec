def test_check_success(run_reframe, tmp_path, logfile):
    returncode, stdout, _ = run_reframe(more_options=['--save-log-files'])
    assert 'PASSED' in stdout
    assert 'FAILED' not in stdout
    assert returncode == 0
    assert os.path.exists(tmp_path / 'output' / logfile)
    assert os.path.exists(tmp_path / 'report.json')

