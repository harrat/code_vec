def test_recovery_with_secondary_agent(self):
        alias = utils.generate_alias(type=Alias.DOMAIN)
        member = self.client.create_member(alias)
        primary_agent_id = member.get_default_agent()
        secondary_agent = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        unused_secondary_agent = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))

        recovery_rule = RecoveryRule(primary_agent=primary_agent_id,
                                     secondary_agents=[secondary_agent.member_id, unused_secondary_agent.member_id])

        member.add_recovery_rule(recovery_rule)
        # remove all keys
        self.client.CryptoEngine.__storage = {}
        crypto_engine = self.client.CryptoEngine(member.member_id)
        key = crypto_engine.generate_key(Key.PRIVILEGED)
        verification_id = self.client.begin_recovery(alias)
        authorization = MemberRecoveryOperation.Authorization(member_id=member.member_id,
                                                              prev_hash=member.get_last_hash(),
                                                              member_key=key)

        signature = secondary_agent.authorize_recovery(authorization)
        op1 = self.client.get_recovery_authorization(verification_id, 'code', key)
        op2 = MemberRecoveryOperation(authorization=authorization, agent_signature=signature)
        recovered = self.client.complete_recovery(member.member_id, [op1, op2], key, crypto_engine)

        assert member.member_id == recovered.member_id
        assert len(recovered.get_keys()) == 3
        assert len(recovered.get_aliases()) == 0
        assert not self.client.is_alias_exists(alias)

        recovered.verify_alias(verification_id, 'code')
        assert self.client.is_alias_exists(alias)
        assert [alias] == utils.repeated_composite_container_to_list(recovered.get_aliases())
