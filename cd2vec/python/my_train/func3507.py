@pytest.mark.vcr()
def test_workbench_asset_vuln_info_uuid_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.asset_vuln_info('this is not a valid UUID', 1234)
