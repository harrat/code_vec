def test_deploy_yearly(self):
        """
        deploy_yearly
        """
        cronpi.run_every_year("ls", isOverwrite=True).on("january", day=2, time="7:30")
        self.assertEqual(get_job_list()[0], "30 7 2 1 * ls")

        cronpi.run_every_year("ls", isOverwrite=True).on("january", day=2, time="7:30am")
        self.assertEqual(get_job_list()[0], "30 7 2 1 * ls")

        cronpi.run_every_year("ls", isOverwrite=True).on(["jan", "july"], day=[2, 5], time="7:30pm")
        self.assertEqual(get_job_list()[0], "30 19 2,5 1,7 * ls")

        cronpi.run_every_year("ls", isOverwrite=True).on(["jan"], day=[2, 5], time="17:30")
        self.assertEqual(get_job_list()[0], "30 17 2,5 1 * ls")
