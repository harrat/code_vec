@patch('logging.Logger.debug')
    def test_assertion_call_debug(self, mock_debug):
        self.provider.assertion("AnyKey=='any-value'", 'Context', {}, 'resource-id')
        mock_debug.assert_called_once_with("resource-id: AnyKey=='any-value' eval False == False")
