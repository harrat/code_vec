def test_load_file_downloads_file_if_it_doesnt_exist(
        self, monkeypatch, http_folder
    ):
        with monkeypatch.context() as ctx:
            urlretrieve = MagicMock()
            ctx.setattr(urllib.request, "urlretrieve", urlretrieve)
            http_folder.load_file(file_name)

        urlretrieve.assert_called_once_with(
            base_url + file_name, str(http_folder.local_dir / file_name)
        )
