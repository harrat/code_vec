def test_221_write_into_a_created_report_do_not_had_headers(self):
        write_dict_to_csv(tests.PROJECT_INFO_ENHANCED, 'output.csv')
        with open('output.csv', 'r') as f:
            result = list(csv.reader(f))
            self.assertNotEqual(result[0], result[2])
