def test_reset_scopes(self):
        self.client.set_scopes(('.*stackoverflow.*', '.*github.*'))
        self.client.reset_scopes()

        self._make_request('https://www.stackoverflow.com')
        self.assertTrue(self.client.get_last_request())
