@pytest.mark.vcr()
def test_access_groups_list(api):
    count = 0
    access_groups = api.access_groups.list()
    for i in access_groups:
        count += 1
        assert isinstance(i, dict)
        check(i, 'created_at', 'datetime')
        check(i, 'updated_at', 'datetime')
        check(i, 'id', 'uuid')
        check(i, 'name', str)
        check(i, 'all_assets', bool)
        check(i, 'all_users', bool)
        #check(i, 'created_by_uuid', 'uuid') # Will not return for default group
        check(i, 'updated_by_uuid', 'uuid')
        check(i, 'created_by_name', str)
        check(i, 'updated_by_name', str)
        check(i, 'processing_percent_complete', int)
        check(i, 'status', str)
    assert count == access_groups.total
