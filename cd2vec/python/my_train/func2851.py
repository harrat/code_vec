def test_thermostat_types_2_get_resistance_heat_utilization_runtime_rhu(
        core_heating_day_set_type_1_entire, thermostat_type_2):

    with pytest.raises(ValueError):
        thermostat_type_2.get_resistance_heat_utilization_runtime(
            core_heating_day_set_type_1_entire)
