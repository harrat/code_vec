def test_invalid_data_dim(self):
        ''' Invalid data dimension. '''
        for d in [0, 1, 3, 4]:
            with self.assertRaisesRegexp(ValueError, r'\[barchart\] .*dim.*'):
                barchart.draw(self.axes, np.zeros([3] * d))
