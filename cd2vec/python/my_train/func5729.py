def test_decompress_file_compression(self):
        path = self.root.make_file()
        gzfile = Path(str(path) + '.foo')
        with gzip.open(gzfile, 'wt') as o:
            o.write('foo')
        with self.assertRaises(ValueError):
            decompress_file(gzfile)
        path2 = decompress_file(gzfile, compression='gz', keep=False)
        assert path == path2
        assert path.exists()
        assert not gzfile.exists()
        with open(path, 'rt') as i:
            assert i.read() == 'foo'
