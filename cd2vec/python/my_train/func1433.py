def test_return_true_when_config_exists(self):
    """test_config_object_saves_config_file ensures config objs are being written to file"""
    self.func.save_config_to_file(
        {"Username": "test@me.com", "Token": "password"},
        ".oss-index-config")
    self.assertEqual(self.func.check_if_config_exists(), True)
