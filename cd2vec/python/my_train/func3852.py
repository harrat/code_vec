def test_patch_email_change_with_send_activation_email_false(self):
        data = {"email": "ringo@beatles.com"}
        response = self.client.patch(self.base_url, data=data)

        self.assert_status_equal(response, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertEqual(data["email"], self.user.email)
        self.assertTrue(self.user.is_active)
