@staticmethod
    @pytest.mark.usefixtures('yaml', 'default-snippets', 'export-time')
    def test_cli_export_snippet_012(snippy):
        """Export defined snippets.

        Export defined snippet based on search keyword. File name is defined
        in command line as yaml file.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Snippet.FORCED
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--sall', 'force', '-f', 'defined-snippet.yaml'])
            assert cause == Cause.ALL_OK
            Content.assert_yaml(yaml, mock_file, 'defined-snippet.yaml', content)
