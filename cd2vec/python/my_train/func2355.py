def test_reset():
    pidom = PiDom()
    device_name = 'test'
    pidom.synchronize(device_name)
    assert pidom.state(device_name) is False
    pidom.toggle(device_name)
    assert pidom.state(device_name) is True
    pidom.reset()
    assert pidom.state(device_name) is False

