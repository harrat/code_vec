def test_resilient_between_timecaches():
    class ExceptionLeakedThroughResilient(Exception):
        pass

    @timecache(1)
    @resilient(acceptable=ExceptionLeakedThroughResilient, default='default')
    @timecache(1)
    def foo():
        raise ExceptionLeakedThroughResilient()

    assert foo() == 'default'

