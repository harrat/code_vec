def test_kitsune_printer_plain(tmpdir):
    a = tmpdir.join("a.log")
    b = tmpdir.join("bb.log")

    # 'touch' the files
    a.ensure(file=True)
    b.ensure(file=True)

    buf = io.StringIO()

    printer = KitsunePrinter([a.strpath, b.strpath], color=False, stream=buf)
    printer.start()
    time.sleep(DELAY_SECONDS)

    a.write("foo\n", mode="a+")
    time.sleep(DELAY_SECONDS)

    b.write("bar\n", mode="a+")
    time.sleep(DELAY_SECONDS)

    a.write("baz\n", mode="a+")
    time.sleep(DELAY_SECONDS)

    output = buf.getvalue().splitlines()
    assert len(output) == 3
    assert output[0] == "a.log  | foo"
    assert output[1] == "bb.log | bar"
    assert output[2] == "a.log  | baz"

    printer.stop()

    a.write("printer already stopped\n", mode="a+")
    time.sleep(DELAY_SECONDS)

    output = buf.getvalue().splitlines()
    assert len(output) == 3
    assert output[0] == "a.log  | foo"
    assert output[1] == "bb.log | bar"
    assert output[2] == "a.log  | baz"

