def test_build_from_dict(self):
        """
        Create a new task template from a dictionary
        """
        task_dict = {
            "id": "1234",
            "name": "dummy"
        }
        task_tmpl = TaskTemplate.from_dict(task_dict)
        self.assertIsInstance(task_tmpl, TaskTemplate)
        self.assertEqual(task_tmpl.uid, '1234')
        self.assertEqual(task_tmpl.name, 'dummy')
        self.assertEqual(task_tmpl.topics, [])
        self.assertEqual(task_tmpl.config, {})

        # Can also create a task template without ID defined in the dict
        task_dict = {
            "name": "dummy",
            "topics": ["yummy"],
            "config": {"hello": "world"},
            "foo": None,
            "bar": [1, 2, 3, 4]
        }
        task_tmpl = TaskTemplate.from_dict(task_dict)
        self.assertIsInstance(task_tmpl, TaskTemplate)
        # Even if no ID is provided, it must be generated
        self.assertIsInstance(task_tmpl.uid, str)
        self.assertEqual(len(task_tmpl.uid), 36)
        self.assertEqual(task_tmpl.name, 'dummy')
        self.assertEqual(task_tmpl.topics, ["yummy"])
        self.assertEqual(task_tmpl.config, {"hello": "world"})

        # Additional keys in the dict don't harm
        task_dict = {
            "name": "dummy",
            "foo": None,
            "bar": [1, 2, 3, 4]
        }
        task_tmpl = TaskTemplate.from_dict(task_dict)
        self.assertIsInstance(task_tmpl, TaskTemplate)
        # Even if no ID is provided, it must be generated
        self.assertIsInstance(task_tmpl.uid, str)
        self.assertEqual(len(task_tmpl.uid), 36)
        self.assertEqual(task_tmpl.name, 'dummy')
        self.assertEqual(task_tmpl.topics, [])
        self.assertEqual(task_tmpl.config, {})
