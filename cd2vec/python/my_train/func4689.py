def test_legacy_projects_db(tmpdir):
    projects_db_file = tmpdir.join(projects.ProjectsDb.PROJECTS_FILE)

    local_projects_db = projects.LocalProjectsDb()
    foo = pickle.dumps(local_projects_db)

    with projects_db_file.open(mode='wb') as f:
        f.write(foo)

    p = projects.ProjectsDb(tmpdir.strpath)
    with pytest.raises(projects.OutdatedProjectsDbException):
        p.get_projects()
