def test_validate_multiplicity(self):
		model.factory.validate_multiplicity = True
		who = model.Person()
		b1 = model.Birth()
		who.born = b1
		b2 = model.Birth()
		self.assertRaises(model.ProfileError, who.__setattr__, 'born', b2)
		model.factory.validate_multiplicity = False
		who.born = b2
		self.assertEqual(who.born, [b1, b2])
