def test_get_summary_stats():
    """Test for method for getting summary stats from asy_fit"""
    
    # setup
    st = cs.st
    func = st.asy_fit._get_summary_stats #(st.asy_fit.fit)
    inp = [st.asy_fit.fit]
    
    # simple tests
    pbt.does_it_return(func, inp)
    pbt.right_type(func, inp, pd.DataFrame)
    pbt.right_shape(func, inp, (10, 9))

    out = func(*inp)

    # check that function returns the same as during testing given the above
    # inputs
    for key in ['std', 'skew', 'MAD']:
        assert_array_equal(out.loc[:, key], 0)
    
    # same as above
    for key in ['mean', '2nd', '16th', '50th', '84th', '97th']:
        assert_array_equal(out.loc[:,key], np.array([ 1.,  2.,  1.,  0., -2.,  1.,  1.,  1.,  1.,  1.]))
  
def test_prior_function():
    """Tests for the prior function used by asy_fit"""
