def test_recommend_skopt():
    s_opt = SkoptOptimizer()
    n_samples = 9
    s_opt.initialize(['a', 'b', 'c'], a=[0, 1], b=[0, 2], c=[0, 3],
                     popsize=n_samples, rounds=2)

    params = s_opt.ask(n_samples)

    errors = np.random.rand(n_samples)
    s_opt.tell(params, errors)

    ans = s_opt.recommend()
    er_min = (errors).argmin()
    assert_equal(params[er_min], list(ans))

