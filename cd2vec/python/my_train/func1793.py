@patch('jmespath.search')
    def test_assertion_call_search(self, mock_search):
        self.provider.assertion("AnyKey=='any-value'", 'Context', {}, 'resource-id')
        mock_search.assert_called_once_with("AnyKey=='any-value'", {})
