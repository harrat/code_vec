def test_caching_to_data(empty_person: Person):
    assert empty_person.data == {"id": empty_person.tmdb_id}
    data = empty_person.get_all()
    assert empty_person.data == data

