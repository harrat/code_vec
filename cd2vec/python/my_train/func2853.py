def testCmdBuildRTAllVariants(self, projects):

        self._checkFeatureRTAllVariants([
            ['build', '--with-tests', 'no', '--run-tests', 'all'],
            ['build', '--with-tests', 'yes', '--run-tests', 'all'],
            ['build', '--with-tests', 'yes', '--run-tests', 'on-changes'],
        ])
