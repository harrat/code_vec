def test_release_lookup_release():
    r = Release("Ensembl Release 85", "Release 85 of Ensembl", "85", edition_of_wdid="Q1344256")
    assert r.get_or_create() == 'Q27666311'
    assert ('https://query.wikidata.org/sparql', "Q1344256", "85") in Release._release_cache

