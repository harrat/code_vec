def test_get_data_by_state(self):
        '''
        Extract the time column and a data column for each iteration of a state
        '''
        path = os.path.join(os.path.dirname(__file__), '.', 'data')

        output = get_data_by_state(path, dates=["6-19-2013"], state=1, column=1, extension=".xls")  # , "6-20-2013"

        datafile = pd.read_csv(path + "/datalog 6-19-2013.xls", delimiter='\t')
        time_and_data1 = np.array([pd.to_numeric(datafile.iloc[:, 0]),
                                   np.round(pd.to_numeric(datafile.iloc[:, 1]), 5)])
        start_time = time_and_data1[0, 0]

        answer = [time_and_data1[:, 98:175], time_and_data1[:, 220:485], time_and_data1[:, 3039:3304],
                  time_and_data1[:, 5858:6123], time_and_data1[:, 8677:8942], time_and_data1[:, 11496:11761],
                  time_and_data1[:, 14315:14580]]

        for i in range(len(output)):
            output_i = np.round(np.array(output[i]).astype(np.double), 5)
            self.assertSequenceEqual([j[0] for j in output_i], [round(j-start_time, 5) for j in answer[i][0]])
            self.assertSequenceEqual([j[1] for j in output_i], [j for j in answer[i][1]])
