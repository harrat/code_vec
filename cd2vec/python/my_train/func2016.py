def test_biz_factory(self):
        fact = BizFactory('sfn_name1')
        fact.__sfn_client__ = Mock()
        fact.__sfn_arn__ = "arn1"

        fact.create('every1', Every(5, Every.MINUTES), data={"key1": "value1"})
        assert len(fact.biz) == 1
        assert len(fact.trigger_sources) == 1
        assert 'every1' in fact.biz
        fact.create('at2', At('0/10', '*', day_of_month='?', day_of_week='MON-FRI'), data={"key2": "value2"})
        assert len(fact.biz) == 2
        assert len(fact.trigger_sources) == 2
        assert 'at2' in fact.biz

        fact({'detail-type': 'Scheduled Event',
              'resources': ['arn:aws:events:eu-west-1:123456789:rule/sfn_name1-every1']}, {})
        fact.sfn_client.start_execution.assert_called_once_with(input='{"key1": "value1"}', stateMachineArn='arn1')
        fact({'detail-type': 'Scheduled Event',
              'resources': ['arn:aws:events:eu-west-1:123456789:rule/sfn_name1-at2']}, {})
        fact.sfn_client.start_execution.assert_called_with(input='{"key2": "value2"}', stateMachineArn='arn1')
