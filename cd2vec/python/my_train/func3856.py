@pytest.mark.parametrize('key', [
    'drizzle_N', 'drizzle_lwc', 'drizzle_lwf', 'v_drizzle', 'v_air'])
def test_calc_derived_products(class_objects, params_objects, key):
    d_source, d_class, s_width = class_objects
    obj = CalculateProducts(d_source, params_objects)
    dictio = obj._calc_derived_products()
    assert key in dictio.keys()

