def test_redis_delay_task_decorator_invalid_task_id_format(event_loop, redis_instance):

    import random
    import logging

    logger = logging.getLogger("aiotasks")

    class CustomLogger(logging.StreamHandler):

        def __init__(self):
            super(CustomLogger, self).__init__()
            self.content = []

        def emit(self, record):
            self.content.append(record.msg)

    custom = CustomLogger()
    logger.addHandler(custom)

    manager = build_manager(dsn=redis_instance, loop=event_loop)

    task_id = random.randint(10, 1000)

    @manager.task()
    async def task_test_redis_delay_task_decorator_invalid_task_id_format():
        pass

    async def run():
        manager.run()

        # Send an invalid task name
        await manager._redis_poller.lpush(manager.task_list_name,
                                          msgpack.packb(dict(task_id=task_id,
                                                             function="task_test_redis_delay_task_decorator_invalid_task_id_format",
                                                             args=(),
                                                             kwargs={})))

        await manager.wait(timeout=0.5, exit_on_finish=False, wait_timeout=0.1)

    event_loop.run_until_complete(run())
    manager.stop()

    assert "Task ID '{}' has not valid UUID4 format".format(task_id) in custom.content

