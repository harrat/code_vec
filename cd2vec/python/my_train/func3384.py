def test_geoid_earth():
    "Sanity checks for the loaded grid"
    grid = fetch_geoid_earth()
    assert grid.geoid.shape == (361, 721)
    npt.assert_allclose(grid.geoid.min(), -106.257344)
    npt.assert_allclose(grid.geoid.max(), 84.722744)

