def test_init(self):
        """
        Test that the PolicyDirectoryMonitor can be instantiated without error.
        """
        m = monitor.PolicyDirectoryMonitor(
            self.tmp_dir,
            multiprocessing.Manager().dict()
        )

        self.assertIsInstance(
            m.halt_trigger,
            multiprocessing.synchronize.Event
        )
        self.assertEqual(self.tmp_dir, m.policy_directory)
        self.assertEqual({}, m.file_timestamps)
        self.assertEqual({}, m.policy_cache)
        self.assertEqual([], m.policy_files)
        self.assertEqual({}, m.policy_map)
        self.assertEqual([], m.policy_store.keys())
        self.assertEqual(['default', 'public'], m.reserved_policies)
        self.assertIsInstance(m.logger, logging.Logger)
