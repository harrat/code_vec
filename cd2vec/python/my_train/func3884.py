def test_task_get(todolist_app):
	with todolist_app.test_client() as client:
		resp = client.jget('/tasks/1')
		assert resp['status'] == 'success'
