def test_access_token_builder_set_transfer_destinations(self):
        member1 = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        member2 = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        payload = AccessTokenBuilder.create_with_alias(
            member2.get_first_alias()).for_all_transfer_destinations().build()
        access_token = member1.create_access_token(payload)
        result = member1.get_token(access_token.id)
        assert access_token == result
