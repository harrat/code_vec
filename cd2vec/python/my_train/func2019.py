def test_class_name_for_tests_formatted_correctly(self, testdir):
        test_content = """
            import pytest
            class TestClassName(object):
                def test_failing_function(self):
                    assert 0

                def test_passing_function(self):
                    assert 1 == 1
            class TestSecondClass(object):
                def test_passing_function(self):
                    assert 1 == 1
            """
        testdir.makepyfile(test_list_of_tests=test_content,test_list_of_tests2=test_content)
        testdir.makeconftest(self.conftest.read())
        result = testdir.runpytest('--easy')

        expected_result = "test_list_of_tests.py \n  TestClassName \n    failing function (FAILED)\n    passing function (PASSED)\n  TestSecondClass \n    passing function (PASSED)\n\ntest_list_of_tests2.py \n  TestClassName \n    failing function (FAILED)\n    passing function (PASSED)"
        assert expected_result in result.stdout.str()
