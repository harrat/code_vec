@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_delete_snippet_007(snippy):
        """Delete snippet with dgiest.

        Try to delete snippet with short version of digest that does not match
        to any existing message digest.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        cause = snippy.run(['snippy', 'delete', '-d', '123456'])
        assert cause == 'NOK: cannot find content with message digest: 123456'
        Content.assert_storage(content)
