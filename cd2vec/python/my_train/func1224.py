@pytest.mark.skipif(easyagents.backends.core._tf_eager_execution_active, reason="_tf_eager_execution_active")
    @pytest.mark.tforce
    def test_dueling_dqn_tforce(self):
        agents.activate_tensorforce()
        self.train_and_assert(DuelingDqnAgent)
