def test_freeze(self):
    net = nets.resnet18()
    for p in net.parameters():
      self.assertTrue(p.requires_grad, 'initialization already frozen!')
    utils.freeze(net)
    for p in net.parameters():
      self.assertFalse(p.requires_grad, 'not working!')
