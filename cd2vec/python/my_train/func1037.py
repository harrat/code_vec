def test_create_agent(self):
        f = BackendAgentFactoryTest.DebugAgentFactory()
        mc = core.ModelConfig(gym_env_name="CartPole-v0")
        a = f.create_agent(easyagent_type=easyagents.agents.DqnAgent, model_config=mc)
        assert a is not None
