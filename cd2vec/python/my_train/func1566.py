def test_find_with_matches(self):
        level1 = self.root.make_directory()
        level2 = self.root.make_directory(prefix='foo', parent=level1)
        path = self.root.make_path(name='bar123', parent=level2)
        result = cast(Sequence[Tuple[PurePath, Match]], find(
            level1, 'bar(.*)', 'f', recursive=True, return_matches=True))
        assert 1 == len(result)
        assert path == result[0][0]
        assert '123' == result[0][1].group(1)
