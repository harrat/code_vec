def test_parse_udf_subdir_odd(tmpdir):
    indir = tmpdir.mkdir('udfsubdir')
    outfile = str(indir)+'.iso'
    indir.mkdir('dir1').mkdir('subdi1')
    subprocess.call(['genisoimage', '-v', '-v', '-no-pad', '-iso-level', '1',
                     '-udf', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_udf_subdir_odd)

def test_parse_udf_onefile(tmpdir):
    indir = tmpdir.mkdir('udfonefile')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'foo'), 'wb') as outfp:
        outfp.write(b'foo\n')
