def test_describe_experiment(self):
        self.create_default_experiment()
        cluster = Cluster.new('tmux', server_name=_TEST_SERVER)

        with self.assertRaises(ValueError):
            cluster.describe_experiment('Irene')
        exp_dict = cluster.describe_experiment('exp')
        self.assertDictEqual(
                exp_dict,
                {
                    'group': {
                        'hello': {
                            'status': 'live'
                        }
                    },
                    None: {
                        'alone': {
                            'status': 'live'
                        },
                    },
                }
        )
