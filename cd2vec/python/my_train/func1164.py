@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_delete_snippet_013(snippy, capsys):
        """Delete snippet with search keyword matching more than once.

        Delete snippet based on search keyword that results more than one hit.
        In this case the error text is read from stdout and it must contain
        the error string.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--sall', 'docker'])
        out, _ = capsys.readouterr()
        assert cause == 'NOK: search keywords matched 2 times preventing delete operation'
        assert out == 'NOK: search keywords matched 2 times preventing delete operation\n'
        Content.assert_storage(content)
