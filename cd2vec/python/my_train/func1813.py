def test_wait_all_2(self):
        def check():
            sleep(0.05)
            raise ValueError()

        self.assertEqual(wait_all(popen(lambda: 1), popen(check)), [0, 1])
