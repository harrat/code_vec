def test_signal_weakref():
    """
    Test that signals handlers of methods are deleted when their objects get collected
    """
    import gc

    class Foo:
        def on_test(self, a, b):
            a / b

    foo = Foo()
    register_object(foo)

    with pytest.raises(ZeroDivisionError):
        on_test(a=5, b=0, c='c')

    del foo
    gc.collect()

    on_test(a=5, b=0, c='c')

