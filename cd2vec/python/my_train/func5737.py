@mock.patch("IPython.display.display")
def test_reglue_scrap_unattached(mock_display, notebook_result):
    notebook_result.reglue("one", unattached=True)
    mock_display.assert_called_once_with(
        {
            "application/scrapbook.scrap.json+json": {
                "name": "one",
                "data": 1,
                "encoder": "json",
                "version": 1,
            }
        },
        metadata={},
        raw=True,
    )
