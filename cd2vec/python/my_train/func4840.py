def test_preamble_commandset_no_dataset(self):
        """Test reading only preamble and command set"""
        preamble = b"\x00" * 128
        prefix = b"DICM"
        command = (
            b"\x00\x00\x00\x00\x04\x00\x00\x00\x38"
            b"\x00\x00\x00\x00\x00\x02\x00\x12\x00\x00"
            b"\x00\x31\x2e\x32\x2e\x38\x34\x30\x2e\x31"
            b"\x30\x30\x30\x38\x2e\x31\x2e\x31\x00\x00"
            b"\x00\x00\x01\x02\x00\x00\x00\x30\x00\x00"
            b"\x00\x10\x01\x02\x00\x00\x00\x07\x00\x00"
            b"\x00\x00\x08\x02\x00\x00\x00\x01\x01"
        )
        bytestream = preamble + prefix + command

        fp = BytesIO(bytestream)
        ds = dcmread(fp, force=True)
        assert "MessageID" in ds
        assert Dataset() == ds.file_meta
