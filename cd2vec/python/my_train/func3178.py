@all_modes()
def test_set_kernel_cmd(d, mode):
    kern = install(d, "%s --kernel-cmd='a b c d' MOD1"%mode)
    assert kern['k'] == ['a', 'b', 'c', 'd']

@all_modes()
def test_language(d, mode):
    kern = install(d, "%s --language=AAA MOD1"%mode)
    assert kern['kernel']['language'] == 'AAA'
