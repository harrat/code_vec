def test_get_header_overrides(self):
        self.client.set_header_overrides({'User-Agent': 'Test_User_Agent_String'})

        self.assertEqual({'User-Agent': 'Test_User_Agent_String'}, self.client.get_header_overrides())
