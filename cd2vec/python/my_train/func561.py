@freeze_time('2014-01-21')
def test_commit_previous_file_year_format(cli, data_dir, config):
    config.set('taxi', 'nb_previous_files', '2')

    efg = EntriesFileGenerator(data_dir, '%Y.tks')
    efg.expand(datetime.date(2013, 1, 1)).write(
        "01/01/2013\nalias_1 1 january 2013"
    )
    efg.expand(datetime.date(2013, 2, 1)).write(
        "01/02/2013\nalias_1 1 february 2013", mode='a'
    )
    efg.expand(datetime.date(2014, 1, 1)).write(
        "01/01/2014\nalias_1 1 january 2014"
    )
    efg.expand(datetime.date(2014, 2, 1)).write(
        "01/02/2014\nalias_1 1 february 2014", mode='a'
    )
    efg.patch_config(config)
    stdout = cli('commit', args=['--yes'])

    assert 'january 2013' in stdout
    assert 'february 2013' in stdout
    assert 'january 2014' in stdout
    assert 'february 2014' in stdout

