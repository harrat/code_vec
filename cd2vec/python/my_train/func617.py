def test_entry_uses_custom_timesheet(cli, data_dir):
    file_path = os.path.join(str(data_dir), 'timesheet.tks')
    with open(file_path, 'w') as f:
        f.write(
            "20/01/2014\nalias_1 1000-? hello world"
        )

    with freeze_time('2014-01-20 10:45:00'):
        stdout = cli('current', args=['-f%s' % file_path])

    assert stdout.strip() == "alias_1 00h45m"

