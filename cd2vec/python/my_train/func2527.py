def test_parse_joliet_dirs_just_short_ptr_extent(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('jolietjustshortdirs')
    outfile = str(indir)+'.iso'
    numdirs = 215
    for i in range(1, 1+numdirs):
        indir.mkdir('dir%d' % i)
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-J', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_joliet_dirs_just_short_ptr_extent)

def test_parse_joliet_dirs_add_ptr_extent(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('jolietjustshortdirs')
    outfile = str(indir)+'.iso'
    numdirs = 295
    for i in range(1, 1+numdirs):
        indir.mkdir('dir%d' % i)
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-J', '-o', str(outfile), str(indir)])
