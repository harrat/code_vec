def test_cutout(self):
        print('test volume cutout...')
        operator = CutoutOperator(self.volume_path, mip=self.mip)

        offset = (4, 64, 64)
        shape = (28, 320, 320)
        output_bbox = Bbox.from_delta(offset, shape)
        chunk = operator(output_bbox)

        self.assertEqual(offset, chunk.global_offset)
        self.assertTrue(chunk == self.img[4:-4, 64:-64, 64:-64])

        shutil.rmtree('/tmp/test')
