@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_job_env_vars(fixture, request):
    engine = request.getfixturevalue(fixture)

    job = engine.launch(image='alpine',
                        command='echo ${AA} ${BB}',
                        env={'AA': 'hello', 'BB':'world'})
    print(job.rundata)
    job.wait()
    assert job.exitcode == 0
    assert job.stdout.strip() == 'hello world'

