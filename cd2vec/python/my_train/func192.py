def test_write_list_fields_tsv(self):
        from datafactory.exceptions import OutputFileAlreadyExists

        sf = self._get_class(
            [
                {'a': 1, 'b': 2, 'c': 3},
                {'a': 4, 'b': 5, 'c': 6},
            ],
            fields=['a', 'b', 'c'],
            header=['a', 'b', 'c'],
            delimiter='\t',
            lineterminator='\n'
        )

        sf.write(self.filename)
        self.assertEqual(
            open(self.filename).read(),
            (
                "a\tb\tc\n"
                "1\t2\t3\n"
                "4\t5\t6\n"
            )
        )

        with self.assertRaises(OutputFileAlreadyExists):
            sf.write(self.filename)
