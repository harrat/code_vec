def test_setitem(test_setting, test_key, actual_value, is_sensitive):
    test_setting[test_key] = actual_value

    assert test_setting[test_key] == actual_value

    if is_sensitive:
        assert isinstance(test_setting.data[test_key], Sensitive)

