def test_specnorm(self):
        layer_test(
            DynastesDepthwiseConv1D, kwargs={'kernel_size': 3,
                                             'kernel_normalizer': 'spectral',
                                             'kernel_regularizer': 'orthogonal'}, input_shape=(5, 32, 3))
