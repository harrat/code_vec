def test_properties_retrieval(self):
        subsets = ['goantislim_grouping',
                   'gocheck_do_not_annotate',
                   'gocheck_do_not_manually_annotate',
                   'goslim_agr',
                   'goslim_aspergillus',
                   'goslim_candida',
                   'goslim_chembl',
                   'goslim_generic',
                   'goslim_mouse',
                   'goslim_pir',
                   'goslim_plant',
                   'goslim_pombe',
                   'goslim_synapse',
                   'goslim_virus',
                   'goslim_yeast',
                   'gosubset_prok']

        s_subsets = self.client.search(query=','.join(subsets), type='property')
        seen = set()
        unique_subsets = [x for x in s_subsets if
                          x.short_form.lower() not in seen and not seen.add(x.short_form.lower())]
        self.assertEqual(len(unique_subsets), len(subsets))
        for subset in unique_subsets:
            s_subset = self.client.property(identifier=subset.iri)
            self.assertNotEqual(s_subset.definition, s_subset.label)
