def test_flex_alloc_invalid_partition_cmd(make_flexible_job):
    job = make_flexible_job('all', sched_partition='invalid')
    with pytest.raises(JobError):
        prepare_job(job)
