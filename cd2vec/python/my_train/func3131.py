@patch('filestack.uploads.intelligent_ingestion.requests.put')
@patch('filestack.uploads.intelligent_ingestion.requests.post')
def test_upload_part_with_resize(post_mock, put_mock):
    # this mock will work fine for commit request too
    post_mock.return_value = DummyHttpResponse(
        ok=True, json_dict={'url': 'http://upload.url', 'headers': {'upload': 'headers'}}
    )

    put_mock.side_effect = [
        DummyHttpResponse(ok=False),  # fail first attempt, should split file part
        DummyHttpResponse(),  # part 1, chunk 1
        DummyHttpResponse(),  # part 1, chunk 2
    ]

    part = {'seek_point': 0, 'num': 1}
    upload_part(
        'Aaaaapikey', 'file.txt', 'tests/data/doom.mp4', 5415034, 's3', defaultdict(lambda: 'fs-upload.com'), part
    )

    assert post_mock.call_count == 4  # 3x upload, 1 commit
    # 1st attempt
    req_args, req_kwargs = post_mock.call_args_list[0]
    assert req_kwargs['json']['size'] == 5415034
    # 2nd attempt
    req_args, req_kwargs = post_mock.call_args_list[1]
    assert req_kwargs['json']['size'] == 4194304
    # 3rd attempt
    req_args, req_kwargs = post_mock.call_args_list[2]
    assert req_kwargs['json']['size'] == 1220730

