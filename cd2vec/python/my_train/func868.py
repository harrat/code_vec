def test_check_kbd_interrupt(run_reframe):
    returncode, stdout, stderr = run_reframe(
        checkpath=[
            'unittests/resources/checks_unlisted/kbd_interrupt.py'
        ],
        more_options=['-t', 'KeyboardInterruptCheck'],
        local=False,
    )
    assert 'Traceback' not in stdout
    assert 'Traceback' not in stderr
    assert 'FAILED' in stdout
    assert returncode != 0

