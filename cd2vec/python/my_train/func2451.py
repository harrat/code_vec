def test_renegotiation_good(self):
        # Given a server that is NOT vulnerable to insecure reneg
        server_location = ServerNetworkLocationViaDirectConnection.with_ip_address_lookup("www.google.com", 443)
        server_info = ServerConnectivityTester().perform(server_location)

        # When testing for insecure reneg, it succeeds
        result: SessionRenegotiationScanResult = SessionRenegotiationImplementation.scan_server(server_info)

        # And the server is reported as not vulnerable
        assert result.supports_secure_renegotiation
        assert not result.accepts_client_renegotiation

        # And a CLI output can be generated
        assert SessionRenegotiationImplementation.cli_connector_cls.result_to_console_output(result)
