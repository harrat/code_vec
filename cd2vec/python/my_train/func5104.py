@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_delete_solution_010(snippy):
        """Delete solution with data.

        Try to delete solution with content data that does not exist. In this
        case the content data is not truncated.
        """

        content = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--scat', 'solution', '--content', 'not-exists'])
        assert cause == 'NOK: cannot find content with content data: not-exists'
        Content.assert_storage(content)
