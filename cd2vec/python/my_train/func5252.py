def test_relationship_type(self):
        relationship = copy.deepcopy(self.valid_relationship)
        relationship['relationship_type'] = "SOMETHING"
        results = validate_parsed_json(relationship, self.options)
        self.assertEqual(results.is_valid, False)
