def test_simple_signal_object_wo_identifier():

    class Foo():
        def on_test_identifier(self, obj):
            1 / 0
        __init__ = register_object

    f1 = Foo()

    with pytest.raises(ZeroDivisionError):
        on_test_identifier(a=5, b=0, obj='obj1')
        on_test_identifier(a=5, b=0, obj='obj2')

    unregister_object(f1)

