@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'import-kafka')
    def test_api_search_solution_006(server):
        """Search solution with GET.

        Send GET /solutions and search keywords from all attributes sorted
        with two fields. This syntax that separates the sorted fields causes
        the parameter to be processed in string context which must handle
        multiple attributes. In this case the search query matches only to
        two fields.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '7065'
        }
        expect_body = {
            'meta': {
                'count': 2,
                'limit': 2,
                'offset': 0,
                'total': 2
            },
            'data': [{
                'type': 'solution',
                'id': Solution.NGINX_UUID,
                'attributes': Storage.dnginx
            }, {
                'type': 'solution',
                'id': Solution.KAFKA_UUID,
                'attributes': Storage.dkafka
            }]
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/solutions',
            headers={'accept': 'application/json'},
            query_string='sall=docker%2Cnmap&limit=2&sort=-created%2C-brief')
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
