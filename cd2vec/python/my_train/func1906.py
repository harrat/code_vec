def test_getidfobjectlist():
    """py.test for getidfobjectlist"""
    names = ["a", "b", "c", "d", "e"]
    idf = IDF(StringIO(""))
    idf.newidfobject("building", Name="a")
    idf.newidfobject("building", Name="b")
    idf.newidfobject("Site:Location", Name="c")
    idf.newidfobject("ScheduleTypeLimits", Name="d")
    idf.newidfobject("ScheduleTypeLimits", Name="e")
    result = idf_helpers.getidfobjectlist(idf)
    assert [res.Name for res in result] == names

