def test_222_report_regenerated(self):
        generate_report(tests.PROJECT_INFO_ENHANCED, 'output.csv')
        with open('output.csv', 'r') as f:
            result = list(csv.reader(f))
            self.assertEqual(result[2], result[3])
        os.remove('output.csv')
