def test_signin_fails(object):
        with pytest.raises(lds_org.Error) as err:
            lds_org.LDSOrg('CainTheCursed', 'sonofadam', signin=True)
        assert str(err.value).endswith('password failed')
