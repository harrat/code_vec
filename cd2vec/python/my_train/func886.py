def test_errors_are_passed_to_ATOM():
    """Assert that the errors found in models are passed to ATOM."""
    atom = ATOMClassifier(X_bin, y_bin, random_state=1)
    atom.run(['LR', 'LGB'], n_calls=5, n_random_starts=(2, -1))
    assert 'LGB' in atom.errors

