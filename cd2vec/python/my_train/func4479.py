def test__create_report(init_ExpStock_with_args):
    e = init_ExpStock_with_args.__next__()
    e._create_report()

    filename = 'report.txt'
    filepath = os.path.join(e.log_dirname, filename)

    assert os.path.isfile(filepath)

