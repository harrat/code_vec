def test_ing_to_simple_mixture_serialization(valid_ing_to_simple_mixture_predictor_data):
    """Ensure that a serialized IngredientsToSimpleMixturePredictor looks sane."""
    predictor = IngredientsToSimpleMixturePredictor.build(valid_ing_to_simple_mixture_predictor_data)
    serialized = predictor.dump()
    serialized['id'] = valid_ing_to_simple_mixture_predictor_data['id']
    assert serialized == valid_serialization_output(valid_ing_to_simple_mixture_predictor_data)

