def test_new_task_ok(self):
        """
        Various cases which must lead to create asyncio tasks successfully.
        """
        # Create a task from a task holder
        task = new_task('my-task-holder')
        self.assertIsInstance(task, asyncio.Task)

        # Create a task from a simple coroutine
        task = new_task('my-coro-task')
        self.assertIsInstance(task, asyncio.Task)
