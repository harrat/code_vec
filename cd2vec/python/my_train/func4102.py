@staticmethod
    @pytest.mark.usefixtures('default-references', 'export-time')
    def test_cli_export_reference_012(snippy):
        """Export defined reference with digest.

        Export defined reference based on search keyword. File name is defined
        in command line as text file with *.txt file extension.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Content.deepcopy(Reference.REGEXP)
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--sall', 'regexp', '-f', 'defined-reference.txt', '--scat', 'reference'])
            assert cause == Cause.ALL_OK
            Content.assert_text(mock_file, 'defined-reference.txt', content)
