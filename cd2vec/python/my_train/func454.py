def test_not_enough_def_colors(self):
        ''' Not enough default colors. '''
        with self.assertRaisesRegexp(ValueError,
                                     r'\[barchart\] .*default colors.*'):
            barchart.draw(self.axes, [[1] * 100])
