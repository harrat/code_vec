@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'yaml', 'default-snippets', 'import-netcat-utc')
    def test_cli_import_snippet_018(snippy):
        """Import snippets already existing.

        Import snippets from yaml file that is defined from command line. In
        this case two out of three imported snippets are already stored into
        the database. Because existing content is not considered as an error,
        the third snippet is imported successfully with success cause.

        The UUID is modified to avoid the UUID collision which produces error.
        The test verifies that user modified resource attributes do not stop
        importing multiple resources.
        """

        content = {
            'data': [
                Content.deepcopy(Snippet.REMOVE),
                Content.deepcopy(Snippet.FORCED),
                Snippet.NETCAT
            ]
        }
        content['data'][0]['uuid'] = Content.UUID1
        content['data'][1]['uuid'] = Content.UUID2
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '-f', './snippets.yaml'])
            assert cause == Cause.ALL_OK
            content['data'][0]['uuid'] = Snippet.REMOVE_UUID
            content['data'][1]['uuid'] = Snippet.FORCED_UUID
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './snippets.yaml', mode='r', encoding='utf-8')
