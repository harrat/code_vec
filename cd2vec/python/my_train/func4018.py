@mock.patch('thundra.integrations.botocore.BaseIntegration.actual_call')
def test_sqs(mock_actual_call, mock_sqs_response):
    mock_actual_call.return_value = mock_sqs_response
    try:
        sqs = boto3.client('sqs', region_name='us-west-2')
        sqs.send_message(
            QueueUrl='test-queue',
            MessageBody='Hello Thundra!',
            DelaySeconds=123,
        )
    except botocore_errors:
        pass
    finally:
        tracer = ThundraTracer.get_instance()
        span = tracer.get_spans()[0]
        assert span.class_name == 'AWS-SQS'
        assert span.domain_name == 'Messaging'
        assert span.get_tag('operation.type') == 'WRITE'
        assert span.get_tag('aws.sqs.queue.name') == 'test-queue'
        assert span.get_tag('aws.request.name') == 'SendMessage'
        assert span.get_tag(constants.SpanTags['TRACE_LINKS']) == ['MessageID_1']
        assert span.get_tag(constants.AwsSQSTags['MESSAGE']) == 'Hello Thundra!'
        tracer.clear()
