def test_reserved_object_type_infrastructure(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['type'] = "infrastructure"
        results = validate_parsed_json(indicator, self.options)
        self.assertEqual(results.is_valid, False)
