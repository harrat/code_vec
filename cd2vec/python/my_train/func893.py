def test_drivermanager():
    cls = _env.FindClass('java.sql.DriverManager')
    signature = '(Ljava/lang/String;)Ljava/sql/Connection;'
    mid1 = _env.GetStaticMethodID(cls, 'getConnection', signature)
    argtypes, restype = py2jdbc.sig.method_signature(_env, signature)
    conn = restype.call_static(cls, mid1, argtypes, 'jdbc:sqlite::memory:')
    assert conn is not None
    _env.DeleteLocalRef(conn)
    _env.DeleteLocalRef(cls)

