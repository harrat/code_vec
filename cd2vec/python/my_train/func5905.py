def test_experiment_preamble(self):
        self.create_default_experiment(exp_preamble=['echo exp preamble'])
        cluster = Cluster.new('tmux', server_name=_TEST_SERVER)

        l = cluster.get_log('exp', 'hello', process_group='group')
        self.assertIn('exp preamble', l)

        l = cluster.get_log('exp', 'alone')
        self.assertIn('exp preamble', l)
