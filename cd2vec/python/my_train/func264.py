def test_flex_alloc_invalid_constraint(make_flexible_job):
    job = make_flexible_job('all')
    job.options = ['--constraint=invalid']
    with pytest.raises(JobError):
        prepare_job(job)
