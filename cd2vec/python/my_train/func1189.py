@pytest.mark.parametrize(
        "content", [("u'\u0081'"), ("'foo'"), ("b'\\xe2\\x80\\x93'")]
    )
    def test_extra_text_separated(self, testdir, content):
        testdir.makeconftest(
            f"""
            import pytest
            @pytest.hookimpl(hookwrapper=True)
            def pytest_runtest_makereport(item, call):
                outcome = yield
                report = outcome.get_result()
                if report.when == 'call':
                    from pytest_html import extras
                    report.extra = [extras.text({content})]
        """
        )
        testdir.makepyfile("def test_pass(): pass")
        result, html = run(testdir)
        assert result.ret == 0
        src = "assets/test_extra_text_separated.py__test_pass_0_0.txt"
        link = f'<a class="text" href="{src}" target="_blank">'
        assert link in html
        assert os.path.exists(src)
