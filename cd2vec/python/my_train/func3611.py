def test_all_base_estimators():
    """Assert that the pipeline works for all base estimators."""
    atom = ATOMClassifier(X_bin, y_bin, random_state=1)
    for estimator in ('GP', 'ET', 'RF', 'GBRT', GaussianProcessRegressor()):
        atom.run('LR', n_calls=5, bo_params={'base_estimator': estimator})
        assert not atom.errors

