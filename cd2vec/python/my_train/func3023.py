def test_pagination(self):
        """Can we get paginated results?"""
        response = self.get_response('/artists', 200, params={'page': 2})
        assert len(json.loads(response.get_data(as_text=True))['resources']) == 20
