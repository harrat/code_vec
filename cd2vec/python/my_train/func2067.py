def test_inclusion_renderers():
    assert sorted(
        [(order, key) for key, (order, _) in inclusion_renderers.items()]) == [
        (10, '.css'), (20, '.js'), (30, '.ico')]
    _, renderer = inclusion_renderers['.js']
    assert renderer('http://localhost/script.js') == (
         '<script type="text/javascript" src="http://localhost/script.js"></script>')
