def test_exchanges_endpoints():
    ## Testing getting one exchange
    exchange = cryptowatch.exchanges.get("kraken")
    # Test exchange object structure
    assert hasattr(exchange, "exchange")
    assert hasattr(exchange.exchange, "id")
    assert hasattr(exchange.exchange, "active")
    assert hasattr(exchange.exchange, "name")
    assert hasattr(exchange.exchange, "symbol")
    assert hasattr(exchange.exchange, "routes")
    assert type(exchange.exchange.routes) == type(dict())
    # test exchange exchange values
    assert exchange.exchange.id == 4
    assert exchange.exchange.name == "Kraken"
    assert exchange.exchange.active == True
    assert exchange.exchange.symbol == "kraken"
    # Testing listing all exchanges
    exchanges = cryptowatch.exchanges.list()
    assert hasattr(exchanges, "exchanges")
    assert type(exchanges.exchanges) == type(list())
    assert (
        type(exchanges.exchanges[0]) == cryptowatch.resources.exchanges.ExchangeResource
    )
    # This should raise an APIResourceNotFoundError Exception
    with pytest.raises(cryptowatch.errors.APIResourceNotFoundError):
        cryptowatch.exchanges.get("exchangethatdoesntexists")
