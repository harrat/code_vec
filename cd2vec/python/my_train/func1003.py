def test_max_unique_lines():
    sle = SimilarLogErrors()

    # Create random log lines
    max_lines = sle.MAX_COMMON_LINES
    log_text = ''
    for i in range(max_lines+5):
        # random_words = [''.join(random.choices(string.ascii_uppercase + string.digits, k=5)) for i in range(40)]  # Generate 40 5 character words
        random_text = ''.join(random.choices(string.ascii_uppercase + string.digits, k=5)) * 50
        log_text += f'2017/10/10 00:00:34.251 ERROR {random_text}\n'

    # Confirm max_lines is honored properly
    with tempfile.TemporaryDirectory() as tmpdir:
        filepath = create_tmp_log_file(text=log_text, dir_path=tmpdir)
        variables = {}
        variables['LogFiles'] = [filepath]
        result = sle.run(variables)
    print(f"Result:\n{result}")
    assert 'Other Error Lines' in result
    assert 'Count: 5' in result

