def test_load_overlapping(self):
        e0 = env.Environment(name='e0', variables=[('a', '1'), ('b', '2')])
        e1 = env.Environment(name='e1', variables=[('b', '3'), ('c', '4')])
        rt.loadenv(e0, e1)
        assert not rt.is_env_loaded(e0)
        assert rt.is_env_loaded(e1)
