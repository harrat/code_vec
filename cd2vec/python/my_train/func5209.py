def test_append():

    t = tensors
    v = vectors
    m1 = Message(t, v)
    m2 = Message(t, v)
    m3 = Message(t)
    m4 = TensorMessage(t)
    m5 = Message(pd.DataFrame(v))
    m6 = pd.DataFrame(v)

    m0 = Message()
    assert(len(m0) == 0)
    m = m0.append(Message(t))
    assert m == Message(t)
    m = m0.append(Message(v))
    assert m == Message(v)
    m = m0.append(Message(t,v))
    assert m == Message(t,v)

    m = m1.append(m2)
    assert len(m) == 6
    assert m == Message({'a': torch.Tensor([1,2,3,1,2,3]), 'b': torch.Tensor([4,5,6,4,5,6])}, {'c': np.array([7,8,9,7,8,9]), 'd': np.array([10,11,12,10,11,12])})
    m = m3.append(t)
    assert len(m) == 6
    assert m == Message({'a': torch.Tensor([1,2,3,1,2,3]), 'b': torch.Tensor([4,5,6,4,5,6])})
    m = m3.append(m3)
    assert len(m) == 6
    assert m == Message({'a': torch.Tensor([1,2,3,1,2,3]), 'b': torch.Tensor([4,5,6,4,5,6])})
    m = m3.append(m4)
    assert len(m) == 6
    assert m == Message({'a': torch.Tensor([1,2,3,1,2,3]), 'b': torch.Tensor([4,5,6,4,5,6])})

    m = m4.append(t)
    assert len(m) == 6
    assert m == TensorMessage({'a': torch.Tensor([1,2,3,1,2,3]), 'b': torch.Tensor([4,5,6,4,5,6])})
    m = m4.append(m3)
    assert len(m) == 6
    assert m == TensorMessage({'a': torch.Tensor([1,2,3,1,2,3]), 'b': torch.Tensor([4,5,6,4,5,6])})
    m = m4.append(m4)
    assert len(m) == 6
    assert m == TensorMessage({'a': torch.Tensor([1,2,3,1,2,3]), 'b': torch.Tensor([4,5,6,4,5,6])})

    m = m5.append(v)
    assert len(m) == 6
    assert m == Message({'c': np.array([7,8,9,7,8,9]), 'd': np.array([10,11,12,10,11,12])})
    m = m5.append(m5)
    assert len(m) == 6
    assert m == Message({'c': np.array([7,8,9,7,8,9]), 'd': np.array([10,11,12,10,11,12])})
    m = m5.append(m6)
    assert len(m) == 6
    assert m == Message({'c': np.array([7,8,9,7,8,9]), 'd': np.array([10,11,12,10,11,12])})

    # Test type conversions on appending to TensorMessage
    m = m4.append({'a': np.array([42]), 'b': np.array([24])})
    assert len(m) == 4
    assert m == TensorMessage({'a': torch.Tensor([1,2,3,42]), 'b': torch.Tensor([4,5,6,24])})

