def test_get_access_tokens(self):
        # TODO: cleanup
        member1 = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        member2 = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        address = member1.add_address(generate_nonce(), utils.generate_address())
        payload = AccessTokenBuilder.create_with_alias(member1.get_first_alias()).for_address(address.id).build()
        member1.create_access_token(payload)
        payload = AccessTokenBuilder.create_with_alias(member2.get_first_alias()).for_address(address.id).build()
        access_token = member1.create_access_token(payload)
        member1.endorse_token(access_token, Key.STANDARD)
        time.sleep(self.TOKEN_LOOKUP_POLL_FREQUENCY * 2)
        result = member1.get_access_tokens(limit=2, offset=None)
        token_ids = [item.id for item in result.tokens]
        assert access_token.id in token_ids
