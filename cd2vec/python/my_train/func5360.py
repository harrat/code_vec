def test__unpack(self):
        self.model.hidden = self.model._init_hidden()
        X6, hidden = self.model._rnn(self.X5)
        X7 = self.model._unpack(X6)
        # batch size, sequence length, hidden size
        self.assertEqual(X7[0].data.shape, torch.Size([3, 4, 8]))
        # padded values should be zeros
        self.assertTorchEqual(X7[0].data[1][3], torch.zeros(8))
        self.assertTorchEqual(X7[0].data[2][2], torch.zeros(8))
        self.assertTorchEqual(X7[0].data[2][3], torch.zeros(8))
        # batch sizes
        self.assertTorchEqual(X7[1], torch.tensor([4, 3, 2]))
