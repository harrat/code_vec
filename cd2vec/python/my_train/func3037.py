@pytest.mark.vcr()
def test_access_groups_list_wildcard_typeerror(api):
    with pytest.raises(TypeError):
        api.access_groups.list(wildcard=1)
