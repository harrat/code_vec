def test_get_purls_from_cache_with_cache_miss(self):
    """ This test ensures that a) results can be added to the cache
    and b) if a purl is not in the cache, that purl is still in the new purls
    that are returned """
    self.func.maybe_insert_into_cache(self.string_to_coordinatesresult(
        """[{"coordinates":"pkg:conda/pycrypto@2.6.1",
        "reference":"https://ossindex.sonatype.org/component/pkg:conda/pycrypto@2.6.1",
        "vulnerabilities":[{"id":"156d71e4-6ed5-4d5f-ae47-7d57be01d387",
        "title":"[CVE-2019-16056] jake the snake",
        "cvssScore":0.0,"cve":"CVE-2019-16056",
        "reference":"http://www.wrestling.com"}]}]"""))
    fake_purls = self.get_fake_actual_purls()
    fake_purls.add_coordinate('alabaster', '0.7.12', 'conda')
    (new_purls, results) = self.func.get_purls_and_results_from_cache(
        fake_purls)
    self.assertEqual(len(new_purls.get_coordinates()), 1)
    self.assertEqual(isinstance(new_purls, Coordinates), True)
    self.assertEqual(isinstance(results, list), True)
    self.assertEqual(isinstance(results[0], CoordinateResults), True)
    self.assertEqual(isinstance(
        results[0].get_vulnerabilities()[0],
        Vulnerabilities), True)
    self.assertEqual(results[0].get_vulnerabilities()[
        0].get_id(), "156d71e4-6ed5-4d5f-ae47-7d57be01d387")
    self.assertEqual(results[0].get_vulnerabilities()[
        0].get_cve(), "CVE-2019-16056")
    self.assertEqual(results[0].get_coordinates(),
                     "pkg:conda/pycrypto@2.6.1")
    self.assertEqual(results[0].get_reference(
    ), "https://ossindex.sonatype.org/component/pkg:conda/pycrypto@2.6.1")
    self.assertEqual(new_purls.get_coordinates()
                     [('alabaster', '0.7.12', 'conda')], "pkg:conda/alabaster@0.7.12")
