def test_new_task_unknown(self):
        """
        Trying to create a new task with an unknown name must raise a
        `UnknownTaskName` exception.
        """
        task_tmpl = TaskTemplate('dummy')
        with self.assertRaises(UnknownTaskName):
            task_tmpl.new_task('junk-data')
