def test_no_dataset(self):
        """Test reading no elements or preamble produces empty Dataset"""
        bytestream = b""
        fp = BytesIO(bytestream)
        ds = dcmread(fp, force=True)
        assert ds.preamble is None
        assert Dataset() == ds.file_meta
        assert Dataset() == ds[:]
