def test_dump_dict(self):
        exp = self.create_default_experiment(launch=False)
        dump = exp.dump_dict()
        self.assertDictEqual(dump, _TEST_DUMP);
