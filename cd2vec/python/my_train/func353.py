def test_componentsintobranch():
    """py.test for componentsintobranch"""
    tdata = (
        (
            """BRANCH,
             sb0,
             0.0,
             ,
             Pipe:Adiabatic,
             sb0_pipe,
             p_loop Supply Inlet,
             sb0_pipe_outlet,
             Bypass;
             """,
            [("PIPE:ADIABATIC", "pipe1", None), ("PIPE:ADIABATIC", "pipe2", None)],
            "",
            [
                "PIPE:ADIABATIC",
                "pipe1",
                "pipe1_Inlet_Node_Name",
                "pipe1_Outlet_Node_Name",
                "",
                "PIPE:ADIABATIC",
                "pipe2",
                "pipe2_Inlet_Node_Name",
                "pipe2_Outlet_Node_Name",
                "",
            ],
        ),
        # idftxt, complst, fluid, branchcomps
        (
            """BRANCH,
            sb0,
            0.0,
            ,
            Pipe:Adiabatic,
            sb0_pipe,
            p_loop Supply Inlet,
            sb0_pipe_outlet,
            Bypass;
            """,
            [
                ("PIPE:ADIABATIC", "pipe1", None),
                ("CHILLER:ELECTRIC", "chiller", "Chilled_Water_"),
            ],
            "",
            [
                "PIPE:ADIABATIC",
                "pipe1",
                "pipe1_Inlet_Node_Name",
                "pipe1_Outlet_Node_Name",
                "",
                "CHILLER:ELECTRIC",
                "chiller",
                "chiller_Chilled_Water_Inlet_Node_Name",
                "chiller_Chilled_Water_Outlet_Node_Name",
                "",
            ],
        ),
        # idftxt, complst, fluid, branchcomps
    )
    for ii, (idftxt, complst, fluid, branchcomps) in enumerate(tdata):
        fhandle = StringIO(idftxt)
        idf = IDF(fhandle)
        components_thisnodes = [
            (idf.newidfobject(key, Name=nm), thisnode) for key, nm, thisnode in complst
        ]
        fnc = hvacbuilder.initinletoutlet
        components_thisnodes = [
            (fnc(idf, cp, thisnode), thisnode) for cp, thisnode in components_thisnodes
        ]
        branch = idf.idfobjects["BRANCH"][0]
        branch = hvacbuilder.componentsintobranch(
            idf, branch, components_thisnodes, fluid
        )
        assert branch.obj[4:] == branchcomps

