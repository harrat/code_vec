@pytest.mark.vcr()
def test_access_groups_create_all_users_typeerror(api, rules):
    with pytest.raises(TypeError):
        api.access_groups.create('Test', rules, all_users='nope')
