def test_default_plots_None_plotcallback(self):
        agent = agents.PpoAgent("CartPole-v0")
        p = plot.Loss()
        r = plot.Rewards()
        c = agent._add_plot_callbacks([r], None, [p])
        assert not p in c
        assert r in c
