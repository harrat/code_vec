def test_optipng(sphinx_app):
    """Test that optipng is detected."""
    status = sphinx_app._status.getvalue()
    w = sphinx_app._warning.getvalue()
    substr = 'will not be optimized'
    if _has_optipng():
        assert substr not in w
    else:
        assert substr in w
    assert 'optipng version' not in status.lower()  # catch the --version

