def test_sfn_simple_arg_params(self):
        """simplified step function call."""
        tech = TechMS()
        tech.aws_s3_form_data_session = session
        form_data = {
            'text': "hello world",
            'context': {'json': True},
            'files': {'s3': 'bucket/key'},
        }
        data = {'post': '/params', 'form-data': form_data}
        call = TechState.get_call_data(None, data)
        response = tech(call, {})
        assert response['statusCode'] == 200
        assert response['body'] == "post hello world, True and ['key']"
