def test_birthday_attr(person: Person):
    assert isinstance(person.birthday, str)
    assert re.match(r"\d{4}-\d{2}-\d{2}", person.birthday)

