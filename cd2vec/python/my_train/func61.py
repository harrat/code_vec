def test_FixedScansController(self, fragscan_dataset_peaks, fragscan_ps):
        logger.info('Testing FixedScansController')
        assert len(fragscan_dataset_peaks) == N_CHEMS

        ionisation_mode = POSITIVE
        initial_ms1 = 100
        end_ms1 = 100
        num_topN_blocks = 100
        N = 10
        # precursor_mz = 74.0970
        precursor_mz = 128.9536
        ms1_mass_analyser = 'Orbitrap'
        ms2_mass_analyser = 'Orbitrap'
        activation_type = 'HCD'
        isolation_mode = 'Quadrupole'

        gen = ScheduleGenerator(initial_ms1, end_ms1, precursor_mz, num_topN_blocks, N, ms1_mass_analyser,
                                ms2_mass_analyser, activation_type, isolation_mode)
        schedule = gen.schedule
        expected = initial_ms1 + ((N+1) * num_topN_blocks) + end_ms1
        assert len(schedule) == expected

        # create a simulated mass spec without noise and Top-N controller
        mass_spec = IndependentMassSpectrometer(ionisation_mode, fragscan_dataset_peaks, fragscan_ps)
        controller = FixedScansController(ionisation_mode, schedule)
        min_bound, max_bound = get_rt_bounds(fragscan_dataset_peaks, CENTRE_RANGE)
        # max_bound = min_bound + gen.estimate_max_time() + 60

        # create an environment to run both the mass spec and controller
        env = Environment(mass_spec, controller, min_bound, max_bound, progress_bar=True)
        run_environment(env)

        # check that there is at least one non-empty MS1
        check_non_empty_MS1(controller)

        # check that there is at least one MS2 scan -- could be empty
        # a bit difficult to test the non-empty case
        # check_non_empty_MS2(controller)
        assert len(controller.scans[2]) > 0

        filename = 'fixedScansController.mzML'
        check_mzML(env, OUT_DIR, filename)
