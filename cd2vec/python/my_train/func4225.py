def test_cache_check_no_cache(self):
        cache = urlquick.CacheAdapter()
        ret = cache.cache_check("GET", "https://httpbin.org/get", None, {})
        self.assertIsNone(ret)
