def test_mock_lock_connection_open(self):

        if self.MOCK:
            self.device.lock_on_connect = True
            # because there's one single mock file
            # and it is already used for the lock test
            # will tesst if raises LockError on connect
            self.assertRaises(LockError, self.device.lock)
            self.device.lock_on_connect = False
            # enough to see that will try to lock during connect
