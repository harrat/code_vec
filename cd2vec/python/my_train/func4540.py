def test_add_onetime_job_with_trigger_time(my_schedule):
    run_time = datetime.now() + timedelta(seconds=2)
    my_schedule.once(run_time).do(foo, msg='hello')
    if len(my_schedule.jobs) != 1:
        raise AssertionError("Job is not added to schedule.")
    time.sleep(3)  # wait for worker threads to start
    my_schedule.run_pending()
    if len(my_schedule.jobs) != 0:
        raise AssertionError("Job is not executed by schedule.")

