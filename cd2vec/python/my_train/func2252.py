def test_flex_alloc_invalid_partition_opt(make_flexible_job):
    job = make_flexible_job('all')
    job.options = ['--partition=invalid']
    with pytest.raises(JobError):
        prepare_job(job)
