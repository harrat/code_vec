def test_is_allowed_policy_operation_mismatch(self):
        """
        Test that an access check using a policy that does not support the
        specified operation is handled correctly.
        """
        e = engine.KmipEngine()
        e._logger = mock.Mock()
        e._get_enum_string = mock.Mock(side_effect=["Create", "SymmetricKey"])
        e.get_relevant_policy_section = mock.Mock(
            return_value={
                enums.ObjectType.SYMMETRIC_KEY: {
                    enums.Operation.GET: enums.Policy.ALLOW_OWNER
                }
            }
        )

        result = e.is_allowed(
            'test_policy',
            'test_user',
            'test_group',
            'test_user',
            enums.ObjectType.SYMMETRIC_KEY,
            enums.Operation.CREATE
        )

        e._logger.warning.assert_called_once_with(
            "The 'test_policy' policy does not apply to Create operations on "
            "SymmetricKey objects."
        )
        self.assertFalse(result)
