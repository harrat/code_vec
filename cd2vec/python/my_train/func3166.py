@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_update_reference_006(snippy):
        """Update reference with ``digest`` option.

        Try to update reference with empty message digest. Nothing should be
        updated in this case because the empty digest matches to more than
        one reference. Only one content can be updated at the time.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        cause = snippy.run(['snippy', 'update', '--scat', 'reference', '-d', ''])
        assert cause == 'NOK: cannot use empty message digest for update operation'
        Content.assert_storage(content)
