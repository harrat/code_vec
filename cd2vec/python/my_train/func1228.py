def test_run_singularity(d):
    def test_exec(_file, argv):
        assert argv[0] == 'singularity'
        assert '--some-arg=AAA' in argv
        assert 'IMAGE' in argv
    kern = install(d, "singularity --some-arg=AAA IMAGE")
    run(d, kern, test_exec)

    def test_exec(_file, argv):
        assert argv[0] == 'singularity'
        assert is_sublist(argv, ['--bind', os.getcwd()])
        assert is_sublist(argv, ['--pwd', os.getcwd()])
        #assert is_sublist(argv, ['--bind', '/PATH/AAA:/PATH/AAA'])
    kern = install(d, "singularity --pwd IMAGE")
    run(d, kern, test_exec)

