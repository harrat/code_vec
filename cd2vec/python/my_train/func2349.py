def test_autoTick1():
    random.seed(42)

    a = [getNextAddr(), getNextAddr()]

    o1 = TestObj(a[0], [a[1]], TEST_TYPE.AUTO_TICK_1)
    o2 = TestObj(a[1], [a[0]], TEST_TYPE.AUTO_TICK_1)

    assert not o1._isReady()
    assert not o2._isReady()

    time.sleep(4.5)
    assert o1._isReady()
    assert o2._isReady()

    assert o1._getLeader().address in a
    assert o1._getLeader() == o2._getLeader()
    assert o1._isReady()
    assert o2._isReady()

    o1.addValue(150)
    o2.addValue(200)

    time.sleep(1.5)

    assert o1._isReady()
    assert o2._isReady()

    assert o1.getCounter() == 350
    assert o2.getCounter() == 350

    assert o2.addValueSync(10) == 360
    assert o1.addValueSync(20) == 380

    o1._destroy()
    o2._destroy()
    time.sleep(0.5)

