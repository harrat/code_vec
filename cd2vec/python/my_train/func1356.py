def test_write_config():
    """Test writing config to files"""
    # Create some dummy data
    data = {}
    data[KEY] = VALUE

    # Overwrite file from previous test with random data
    assert AUTH.write_config(data, "test.json") is True

    # Try to write in a readonly folder
    assert Auth("config/readonly").write_config(data, "test.json") is False

    # Try to create a new folder
    assert Auth("config/newfolder").write_config(data, "test.json") is True

    # Create new folder in read only will sys.exit
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        Auth("config/readonly/newfolder").write_config(data, "test.json")
    assert pytest_wrapped_e.type == SystemExit

