def test_pheno_assocs():
    q = GolrAssociationQuery(subject=TWIST_ZFIN,
                  object_category='phenotype')
    print("Q={}".format(q))
    print("Q.subject={}".format(q.subject))
    print("Q.evidec={}".format(q.evidence))
    params = q.solr_params()
    print("PARAMS={}".format(params))
    results = q.exec()
    print("RES={}".format(results))
    assert len(results) > 0

