def test_extra_resources(testsys_system):
    @fixtures.custom_prefix('unittests/resources/checks')
    class MyTest(HelloTest):
        def __init__(self):
            super().__init__()
            self.name = type(self).__name__
            self.executable = os.path.join('.', self.name)
            self.local = True

        @rfm.run_after('setup')
        def set_resources(self):
            test.extra_resources = {
                'gpu': {'num_gpus_per_node': 2},
                'datawarp': {'capacity': '100GB',
                             'stagein_src': test.stagedir}
            }
            test.job.options += ['--foo']

    test = MyTest()
    partition = fixtures.partition_by_name('gpu')
    environ = partition.environment('builtin-gcc')
    _run(test, partition, environ)
    expected_job_options = {'--gres=gpu:2',
                            '#DW jobdw capacity=100GB',
                            '#DW stage_in source=%s' % test.stagedir,
                            '--foo'}
    assert expected_job_options == set(test.job.options)

