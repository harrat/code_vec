@staticmethod
    def test_logger_001(logger, caplog, capsys):
        """Test logger basic usage.

        Test case verifies that default log configuration is working.
        By default only log level warning and levels above are printed.
        The logs must be text formatted lines that are not truncated.
        """

        # Test log levels.
        logger.security('testing security level')
        logger.critical('testing critical level')
        logger.error('testing error level')
        logger.warning('testing warning level')
        logger.info('testing info level')
        logger.debug('testing debug level')

        # Test log message length.
        logger.warning('abcdefghij'*100)

        out, err = capsys.readouterr()
        assert not err
        assert not out
        assert len(caplog.records[:]) == 5
        assert 'testing critical level' in caplog.text
        assert 'testing error level' in caplog.text
        assert 'testing warning level' in caplog.text
        assert max(caplog.text.split(), key=len) == 'abcdefghij'*100
        with pytest.raises(Exception):
            json.loads(out)
