def test_text_with_justify(self):
        text = Text.parse('(fp_text user text (at 0.0 0.0) (layer layer) '
                          '(effects (justify mirror)))')
        assert text.justify == 'mirror'
        assert Text.parse(text.to_string()) == text
