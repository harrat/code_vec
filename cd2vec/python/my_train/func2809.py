def test_sim_search(self):
        classes = ['HP:0000739', 'HP:0000740']
        search_results = self.pheno_sim.search(classes)
        assert search_results.matches[0].rank == 1
        assert 0 < search_results.matches[0].score < 100
        assert search_results.matches[0].type in ['gene', 'phenotype', 'disease']
        assert search_results.matches[0].taxon.id is not None
        assert len(search_results.matches[0].pairwise_match) > 0
        assert search_results.matches[0].pairwise_match[0].lcs.IC > 0
