def test_parse_plugins(self) -> None:
        config = self.load_config("custom_plugins.yml")

        expected_layout = {
            "runtime": {"sample": "tests.spec_loaders.TestRuntimeSpecLoader"},
            "artifact": {"cryptocurrency": "tests.spec_loaders.TestInstanceSpecLoader"},
        }

        self.assertEqual(config.plugins, expected_layout)
