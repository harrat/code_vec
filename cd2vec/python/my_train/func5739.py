@staticmethod
    @pytest.mark.usefixtures('yaml', 'import-gitlog', 'import-remove', 'import-beats', 'export-time-all-categories')
    def test_cli_export_reference_020(snippy):
        """Export defaults with ``scat`` option.

        Export snippet, solution and reference defaults with category
        set to ``all`` and so that the defauls will be updated. This must
        store the content from each category to own file that stores the
        default content.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Snippet.REMOVE,
                Solution.BEATS,
                Reference.GITLOG
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--defaults', '--scat', 'all'])
            assert cause == Cause.ALL_OK
            defaults_snippets = pkg_resources.resource_filename('snippy', 'data/defaults/snippets.yaml')
            defaults_solutions = pkg_resources.resource_filename('snippy', 'data/defaults/solutions.yaml')
            defaults_references = pkg_resources.resource_filename('snippy', 'data/defaults/references.yaml')
            Content.assert_yaml(yaml, mock_file, [defaults_snippets, defaults_solutions, defaults_references], content)
