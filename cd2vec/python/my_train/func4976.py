def test_text_with_rotation(self):
        text = Text.parse(
            '(fp_text user text (at 0.0 0.0 0.0) (layer F.SilkS))')
        assert text.at == [0.0, 0.0, 0.0]
        assert text.hide == False
        assert Text.parse(text.to_string()) == text
