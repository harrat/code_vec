def test_sklearn_pipeline(self):
        text_preprocessor = TextPreprocessor(spacy_model_id='en', lemmatize=True)
        tf_idf_vectorizer = TfidfVectorizer(input='content', tokenizer=lambda x: x,
                                            preprocessor=None, lowercase=False)
        bayes_clf = MultinomialNB()

        pipe = Pipeline(steps=[
            ('preprocessing', text_preprocessor),
            ('tf-idf', tf_idf_vectorizer),
            ('clf', bayes_clf)
        ])
        pipe.fit(self.sample_en, self.labels_en)
