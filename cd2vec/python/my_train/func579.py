def test_02_read_2_str(self):
        with AIOContext(1) as ctx, open(self._TEST_FILE_NAME) as fp:
            buffer = 'buffer__'
            block = ReadBlock(fp, buffer)
            block.length = len(buffer)
            self.assertEqual(len(buffer), block.length)
