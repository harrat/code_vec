def test_getting_wdc_format_hour_data_from_wdc(tmpdir):  # pylint: disable=invalid-name
    """
    smoke test WRT 'known good' data
    - download hourly averages of Niemegk from WDC
    - in wdc format
    - put them in a temporary folder
    - compare them with 'known-good' examples
    """
    cadence = 'hour'
    station = 'NGK'
    start_date = date(2015, 4, 1)
    end_date = date(2015, 4, 30)
    service = 'WDC'
    configpath = os.path.join(DATAPATH, 'wdc_minute_data_wdcoutput.ini')
    oraclefile = os.path.join(ORACLEPATH, 'ngk2015.wdc')

    tmppath = str(tmpdir)

    config = cws.ParsedConfigFile(configpath, service)
    form_data = cws.FormData(config)
    with pytest.raises(ValueError):
        form_data.as_dict()
    form_data.set_datasets(start_date, end_date, station, cadence, service)
    req_wdc = cws.DataRequest()
    req_wdc.read_attributes(config)
    assert req_wdc.can_send is False
    assert req_wdc.form_data == {}
    req_wdc.set_form_data(form_data.as_dict())
    assert req_wdc.can_send is True

    resp_wdc = rq.post(
        req_wdc.url, data=req_wdc.form_data, headers=req_wdc.headers
    )

    with zipfile.ZipFile(BytesIO(resp_wdc.content)) as fzip:
        fzip.extractall(tmppath)
    gotfile = os.path.join(tmppath, os.path.basename(oraclefile))
    # custom function due to line-ending vagueries
    assert_all_lines_same(gotfile, oraclefile)

