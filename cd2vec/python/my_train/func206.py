def test_neopixels_rainbow(np):
    strip = np._strip
    np.rainbow(period=T, timeout=T, delay=T / 6, wait=1)
    assert strip.color_data == [0] * 3
    assert strip.setPixelColorRGB.call_count == 7 * 3
    assert strip.show.call_count == 8
    strip.setBrightness.assert_called_once_with(128)
    assert_colors(strip.setPixelColorRGB.call_args_list,
                  [(0, 254, 0, 0), (1, 0, 254, 0), (2, 0, 0, 254),
                   (0, 126, 0, 128), (1, 128, 126, 0), (2, 0, 128, 126),
                   (0, 0, 1, 253), (1, 253, 0, 1), (2, 1, 253, 0),
                   (0, 0, 128, 126), (1, 126, 0, 128), (2, 128, 126, 0),
                   (0, 2, 252, 0), (1, 0, 2, 252), (2, 252, 0, 2),
                   (0, 130, 124, 0), (1, 0, 130, 124), (2, 124, 0, 130),
                   (0, 0, 0, 0), (1, 0, 0, 0), (2, 0, 0, 0)],
                  tol=10)
