def test_expand_1(self):
        expanded_policy = expand_policy(policy=dc(WILDCARD_POLICY_1))
        self.assertEqual(expanded_policy, EXPANDED_POLICY_1)
        policy = {
            "Statement": {
                "NotAction": ["ec2:thispermissiondoesntexist"],
                "Resource": "*",
                "Effect": "Deny",
            }
        }
        expected_policy = {
            "Statement": [
                {
                    "NotAction": ["ec2:thispermissiondoesntexist"],
                    "Resource": "*",
                    "Effect": "Deny",
                }
            ]
        }
        expanded_policy = expand_policy(policy=dc(policy), expand_deny=False)
        self.assertEqual(expanded_policy, expected_policy)
        expanded_policy = expand_policy(policy=dc(policy), expand_deny=True)
        self.assertEqual(type(expanded_policy["Statement"]), list)
