def test_no_modified(self):
        custom_obj = copy.deepcopy(self.valid_custom_object)
        del custom_obj['modified']
        results = validate_parsed_json(custom_obj, self.options)
        self.assertEqual(results.is_valid, False)
