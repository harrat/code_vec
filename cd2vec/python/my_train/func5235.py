def test_version_handle_v_prefix(self):
        runner('git tag -a v1.0.0 -m "test message"')
        v = relic.release.get_info()
        assert 'v' not in v.short
        assert v.short == '1.0.0'
