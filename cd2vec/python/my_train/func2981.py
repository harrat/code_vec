def test_add_answers(self):
        self.question.add_answers(2)
        assert isinstance(self.question.answers, list)
        assert self.question.answers[0] == 2

        with pytest.raises(KeyError):
            self.question.add_answers(4)
