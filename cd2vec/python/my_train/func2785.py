def test_parse_bad_format(capsys):
    context = new_context()
    context = shell.parse(context, input_file + ' text')
    (out, err) = capsys.readouterr()
    assert out.index('Warning') == 0

