def test_FASTA_file(self):
		if os.system('gunzip -c data/ce270.fa.gz > /tmp/test.fasta'):
			raise 'unable to create test file'
		ff = io.FASTA_file('/tmp/test.fasta')
		self.assertIsInstance(ff, io.FASTA_file)
		c4 = ff.get('IV')
		self.assertEqual(len(c4.seq), 174938)
		self.assertEqual(len(ff.ids), 6)
		os.system('rm /tmp/test.fasta')
		with self.assertRaises(io.FASTA_error):
			ff = io.FASTA_file('data/README.md')
		with self.assertRaises(NotImplementedError):
			ff = io.FASTA_file('data/at10.fa.gz')
