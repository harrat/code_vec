def test_get_word_vector(self):
        word = 'test'
        vector = self.embedding.get_word_vector(word)
        self.assertTupleEqual(vector.shape, (300,))
        # self.embedding.get_word_vector.assert_called_once_with(word)
