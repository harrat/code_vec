def test_not_implements_private_access():
    """
    Case: do not implement interface member with mismatched private access.
    Expect: class mismatches interface member access modifier error message.
    """
    class HumanSoulInterface:

        @private
        @staticmethod
        @custom_decorator
        def dream(about, *args, **kwargs):
            pass

    with pytest.raises(ImplementedInterfaceMemberHasIncorrectAccessModifierException) as error:

        @implements(HumanSoulInterface)
        class HumanSoul:

            @staticmethod
            @custom_decorator
            def dream(about, *args, **kwargs):
                pass

    assert 'HumanSoul.dream(about, args, kwargs) mismatches ' \
           'HumanSoulInterface.dream() member access modifier.' == error.value.message
