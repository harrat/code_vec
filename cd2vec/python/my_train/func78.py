def test_false_cycle_graph(self):
        self.graph.delete_edge("c", "a")
        self.graph.add_vertex("d")
        self.graph.add_edge("c", "d", 3)
        assert self.graph.has_cycle() is False
        assert self.graph.has_loop() is False
