def test_exception_to_http_response():
    deactivate_traceback()
    try:
        raise Problem(1000, 'test_title', 'test_detail', 'test_type', 'test_instance', custom='test_custom')
    except Problem as e:
        response = e.to_http_response()
        assert response['statusCode'] == 1000
        assert response['headers'] == {'Content-Type': 'application/problem+json'}
        assert json.loads(response['body']) == {
            'status': 1000, 'title': 'test_title', 'detail': 'test_detail',
            'type': 'test_type', 'instance': 'test_instance', 'custom': 'test_custom'
        }
