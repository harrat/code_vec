@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_delete_snippet_001(snippy):
        """Delete snippet with digest.

        Delete snippet with short 16 byte version of message digest.
        """

        content = {
            'data': [
                Snippet.REMOVE
            ]
        }
        Content.assert_storage_size(2)
        cause = snippy.run(['snippy', 'delete', '-d', '53908d68425c61dc'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
