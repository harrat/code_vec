def test_dispatch__encode_error_with_debug_enabled(self):
        def callback(request):
            raise api.ImmediateHttpResponse(ValueError, HTTPStatus.NOT_MODIFIED, {})

        target = containers.ApiInterfaceBase(debug_enabled=True)
        operation = Operation(callback)

        with pytest.raises(TypeError):
            target.dispatch(operation, MockRequest())
