def test_retrieve_document_not_exist(self, connected_adapter):
        """Check that retrieving document that does not exists raises an exception."""
        with pytest.raises(NotFoundError):
            connected_adapter.retrieve_document('some-document-that-really-does-not-exist')
