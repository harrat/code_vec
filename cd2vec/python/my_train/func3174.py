def test_heatcapacity_1_layer_construction(self):
        self.idf.initreadtxt(single_layer)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        expected = m.Thickness * m.Specific_Heat * m.Density * 0.001
        assert c.heatcapacity == expected
        assert c.heatcapacity == 120
