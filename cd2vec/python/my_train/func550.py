def test_job_negative_ids(travis):

    job = travis.job(81966565)



    job.build_id = -1

    with pytest.raises(TravisError) as exception_info:

        job.build

    assert str(exception_info.value) == "[404] not found"



    repository = job.repository

    assert isinstance(repository, Repo)

    assert job.repository_id == repository.id

    assert repository == job.repository



    job.repository_id = -1

    assert job.repository is None



    log = job.log

    assert isinstance(log, Log)

    assert job.log_id == log.id

    assert log == job.log

