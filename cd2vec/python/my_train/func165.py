@staticmethod
    @pytest.mark.usefixtures('json', 'default-snippets', 'export-time')
    def test_cli_export_snippet_008(snippy):
        """Export defined snippets.

        Export defined snippet based on message digest. File name is defined
        in command line as json file.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Snippet.FORCED
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-d', '53908d68425c61dc', '-f', 'defined-snippet.json'])
            assert cause == Cause.ALL_OK
            Content.assert_json(json, mock_file, 'defined-snippet.json', content)
