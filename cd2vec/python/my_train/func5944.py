@mark.parametrize("status, title, detail, type, instance, headers, kwargs, expected_body, expected_headers",
                  [
                      (
                              1000, 'test_title', 'test_detail', 'test_type', 'test_instance',
                              {'test_header_key': 'test_header_value'}, {'custom': 'test_custom'},
                              {
                                  'status': 1000, 'title': 'test_title', 'detail': 'test_detail',
                                  'type': 'test_type', 'instance': 'test_instance', 'custom': 'test_custom'
                              },
                              {
                                  'Content-Type': 'application/problem+json',
                                  'test_header_key': 'test_header_value'
                              }
                      ),
                      (
                              None, None, None, None, None, None, {},
                              {},
                              {
                                  'Content-Type': 'application/problem+json'
                              }
                      ),
                      (
                              None, None, None, None, None, {'Content-Type': 'text/plain'}, {},
                              {},
                              {
                                  'Content-Type': 'text/plain'
                              }
                      ),
                      (
                              None, None, None, None, None, {'content-type': 'text/plain'}, {},
                              {},
                              {
                                  'content-type': 'text/plain'
                              }
                      )
                  ])
def test_problem_http_response(status, title, detail, type, instance, headers, kwargs, expected_body, expected_headers):
    response = problem_http_response(status, title, detail, type, instance, headers, **kwargs)
    assert response['statusCode'] == status
    assert response['headers'] == expected_headers
    assert json.loads(response['body']) == expected_body

