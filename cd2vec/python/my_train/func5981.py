def test_shell(shell, tmpenvdir, capfd):
    tmpenvdir.join("TEST_SHELL").write("test")
    with py.test.raises(Response) as response:
        shell("envshell", str(tmpenvdir))
    out, err = capfd.readouterr()
    assert response.value.status == 0
    assert "Launching envshell for " in out

    with py.test.raises(Response) as response:
        shell("envshell")
    assert "incorrect number of arguments" in response.value.message

