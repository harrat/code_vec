def test_disease2gene():
    payload = search_associations(subject=LSD,
                                  subject_category='disease',
                                  object_category='gene')
    assocs = payload['associations']
    for a in assocs:
        print(str(a))
    assert len(assocs) > 0
 
def test_species_facet():
    payload = search_associations(subject_category='gene',
                                  object_category='phenotype',
                                  facet_fields=['subject_taxon', 'subject_taxon_label'],
                                  rows=0)
    fcs = payload['facet_counts']
    print(str(fcs))
    assert 'Homo sapiens' in fcs['subject_taxon_label'].keys()
