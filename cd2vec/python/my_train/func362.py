@staticmethod
    @pytest.mark.usefixtures('import-beats', 'update-nginx-utc')
    def test_api_create_solution_003(server):
        """Update Solution with POST that maps to PUT.

        Send POST /solutions/{id} to update existing resource with the
        ``X-HTTP-Method-Override`` header that overrides the operation as
        PUT. In this case the created timestamp must remain in initial
        value and the updated timestamp must be updated to reflect the
        update time.

        In this case the resource ``created`` attribute must remain in the
        initial value and the ``updated`` attribute must be set to reflect
        the update time.

        The ``uuid`` attribute must not be changed from it's initial value.

        Because the HTTP method is PUT, it overrides attributes that are
        not defined with default values. The ``filename`` attribute is set
        to empty value because of this.
        """


        storage = {
            'data': [
                Storage.dnginx
            ]
        }
        storage['data'][0]['filename'] = ''
        storage['data'][0]['updated'] = Content.NGINX_TIME
        storage['data'][0]['uuid'] = Solution.BEATS_UUID
        storage['data'][0]['digest'] = '6d102b92af89d6bd6cb65e8d2a6e486dcff2d0fed015ba6b6afde0c99e74b9bc'
        request_body = {
            'data': {
                'type': 'solution',
                'attributes': {
                    'data': storage['data'][0]['data'],
                    'brief': storage['data'][0]['brief'],
                    'description': storage['data'][0]['description'],
                    'groups': storage['data'][0]['groups'],
                    'tags': storage['data'][0]['tags'],
                    'links': storage['data'][0]['links']
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '2777'
        }
        expect_body = {
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/solutions/' + Solution.BEATS_UUID
            },
            'data': {
                'type': 'solution',
                'id': storage['data'][0]['uuid'],
                'attributes': storage['data'][0]
            }
        }
        result = testing.TestClient(server.server.api).simulate_post(
            path='/api/snippy/rest/solutions/4346ba4c79247430',
            headers={'accept': 'application/vnd.api+json; charset=UTF-8', 'X-HTTP-Method-Override': 'PUT'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
