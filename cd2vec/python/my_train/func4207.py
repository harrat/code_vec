@freeze_time('2017-06-21')
def test_no_inactive_flag_can_be_used_with_used_flag(cli, entries_file, alias_config):
    entries_file.write("""20.06.2017
    inactive1 1 Play ping-pong
    active2 1 Play ping-pong
    """)

    stdout = cli('alias', ['list', '--used', '--no-inactive'])

    assert 'inactive1' not in stdout
    assert 'active2' in stdout

