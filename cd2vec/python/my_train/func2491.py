@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_python_function(fixture, request):
    engine = request.getfixturevalue(fixture)
    pycall = pyccc.PythonCall(function_tests.fn, 5)
    job = engine.launch(PYIMAGE, pycall, interpreter=PYVERSION)
    print(job.rundata)
    job.wait()
    assert job.result == 6

