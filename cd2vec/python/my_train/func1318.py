@mock.patch('urllib.request.urlretrieve')
    def test_cached_download(self, f: Callable):
        def urlretrieve(url, path):
            with open(path, 'w') as f:
                f.write('test')
        f.side_effect = urlretrieve

        cache_path = download.cached_download('https://example.com')

        self.assertEqual(f.call_count, 1)
        args, kwargs = f.call_args
        self.assertEqual(kwargs, {})
        self.assertEqual(len(args), 2)
        # The second argument is a temporary path, and it is removed
        self.assertEqual(args[0], 'https://example.com')

        self.assertTrue(os.path.exists(cache_path))
        with open(cache_path) as f:
            stored_data = f.read()
        self.assertEqual(stored_data, 'test')
