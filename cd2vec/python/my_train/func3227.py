@responses.activate
    @pytest.mark.parametrize("hashtag", ["hashtag1", "hashtag2"])
    def test_get_total_hashtag_medias(self, hashtag):

        amount = 5
        results = 10
        my_test_photo_item = TEST_PHOTO_ITEM.copy()
        my_test_photo_item["like_count"] = self.bot.min_likes_to_like + 1
        my_test_photo_items = []
        for _ in range(results):
            my_test_photo_items.append(my_test_photo_item.copy())
        expect_filtered = 0
        my_test_photo_items[1]["has_liked"] = True
        expect_filtered += 1
        my_test_photo_items[2]["like_count"] = self.bot.max_likes_to_like + 1
        expect_filtered += 1
        my_test_photo_items[3]["like_count"] = self.bot.min_likes_to_like - 1
        expect_filtered += 1
        response_data = {
            "auto_load_more_enabled": True,
            "num_results": results,
            "status": "ok",
            "more_available": True,
            "next_max_id": TEST_PHOTO_ITEM["id"],
            "items": my_test_photo_items,
        }

        responses.add(
            responses.GET,
            (
                "{api_url}feed/tag/{hashtag}/?max_id={max_id}"
                + "&rank_token={rank_token}&ranked_content=true&"
            ).format(
                api_url=API_URL,
                hashtag=hashtag,
                max_id="",
                rank_token=self.bot.api.rank_token,
            ),
            json=response_data,
            status=200,
        )

        medias = self.bot.get_total_hashtag_medias(
            hashtag, amount=amount, filtration=False
        )
        assert medias == [
            test_photo_item["id"] for test_photo_item in my_test_photo_items[:amount]
        ]
        assert len(medias) == amount

        medias = self.bot.get_total_hashtag_medias(
            hashtag, amount=amount, filtration=True
        )
        assert medias == [
            test_photo_item["id"]
            for test_photo_item in my_test_photo_items[:amount]
            if (
                not test_photo_item["has_liked"]
                and test_photo_item["like_count"] < self.bot.max_likes_to_like
                and test_photo_item["like_count"] > self.bot.min_likes_to_like
            )
        ]
        assert len(medias) == amount - expect_filtered
