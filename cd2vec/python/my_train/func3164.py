def test_load_fooof():

    file_name = 'test_fooof_all'

    tfm = load_fooof(file_name, TEST_DATA_PATH)

    assert isinstance(tfm, FOOOF)

    # Check that all elements get loaded
    for result in OBJ_DESC['results']:
        assert not np.all(np.isnan(getattr(tfm, result)))
    for setting in OBJ_DESC['settings']:
        assert getattr(tfm, setting) is not None
    for data in OBJ_DESC['data']:
        assert getattr(tfm, data) is not None
    for meta_dat in OBJ_DESC['meta_data']:
        assert getattr(tfm, meta_dat) is not None

def test_load_fooofgroup():
