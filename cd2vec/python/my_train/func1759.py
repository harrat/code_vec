def test_get_wikitext_twice(self):
        for name in ('wikitext-2', 'wikitext-103'):
            with self.subTest(name=name):
                get_wikitext(name)
                with mock.patch('lineflow.datasets.wikitext.pickle', autospec=True) as mock_pickle:
                    get_wikitext(name)
                mock_pickle.dump.assert_not_called()
                self.assertEqual(mock_pickle.load.call_count, 1)
