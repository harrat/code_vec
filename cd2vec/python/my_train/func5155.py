@pytest.mark.remote
def test_fetch_dataset_json():
    start = 934000000
    end = 934100000
    out = api.fetch_dataset_json(start, end)
    assert not out['events']
    assert set(out['runs'].keys()).issubset({'tenyear', 'S6', 'history'})

