def test_to_string_empty(self):
        addr = brs.BRSAddress()
        assert addr.to_string() == 'BURST-2223-2222-2222-22222'
        assert addr.codeword == [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
