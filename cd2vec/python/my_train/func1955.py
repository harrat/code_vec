@patch('stackify.transport.default.DefaultTransport.create_message')
    @patch('stackify.transport.default.http.HTTPClient.POST')
    def test_not_identified(self, post, logmsg):
        '''The HTTPClient identifies automatically if needed'''
        listener = StackifyListener(queue_=Mock(), config=self.config)
        listener.handle(Mock())
        listener.send_group()
        self.assertTrue(listener.transport._transport.identified)
