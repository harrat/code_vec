def test_get_proxy_without_args(get_proxy, regex_proxy):
    prox_dict = get_proxy()
    assert 'http', 'https' in prox_dict.keys()

    assert regex_proxy(prox_dict)

