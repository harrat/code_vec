@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_solution_011(snippy):
        """Import all solution resources.

        Try to import empty solution template. The operation will fail because
        content templates without any modifications cannot be imported.
        """

        file_content = mock.mock_open(read_data=Const.NEWLINE.join(Solution.TEMPLATE_TEXT))
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '-f', './solution-template.txt'])
            assert cause == 'NOK: content was not stored because it was matching to an empty template'
            Content.assert_storage(None)
            Content.assert_arglist(mock_file, './solution-template.txt', mode='r', encoding='utf-8')
