@pytest.mark.vcr()
def test_scan_export_password_typeerror(api):
    with pytest.raises(TypeError):
        api.scans.export(1, password=1)
