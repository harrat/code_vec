@pytest.mark.parametrize("save_versioning", [True, False])
def test_pickle(tmp_path, save_versioning):
    testobj = "iamateststr"
    pickle_fname = str(tmp_path / "testpickle.pklz")
    savepkl(pickle_fname, testobj, versioning=save_versioning)
    outobj = loadpkl(pickle_fname)
    assert outobj == testobj

