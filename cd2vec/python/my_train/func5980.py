def test_medida_si_2():
    m = Medida("1+-0.1", "ft�").SI()

    assert m.nominal - 0.092903 < 1E-4
    assert m.incerteza - 0.0092903 < 1E-4
    assert unidades_em_texto(m.unidades_originais) == "m�"

def test_medida_si_3():
    m = Medida("1+-0.1", "ft� deg lb h �F A mol^-1").SI()
