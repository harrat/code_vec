def test_search_cv(self):
        pipe = Pipeline(steps=[
            ('txt_prep', TextPreprocessor()),
            ('tf-idf', TfidfVectorizer(input='content', tokenizer=do_nothing,
                                       preprocessor=None, lowercase=False)),
            ('clf', MultinomialNB())
        ])

        param_grid = {
            'txt_prep__spacy_model_id': ['en', 'es'],
            'txt_prep__lemmatize': [True, False],
            'txt_prep__remove_stop_words': [True, False],
            'txt_prep__additional_pipes': [None, [pipe_sample]]
        }

        grid_search = GridSearchCV(pipe, param_grid, n_jobs=-1, iid=False, cv=2)
        grid_search.fit(self.sample_en, self.labels_en)
