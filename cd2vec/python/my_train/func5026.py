def test_id_type(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['id'] = "something--8e2e2d2b-17d4-4cbf-938f-98ee46b3cd3f"
        results = validate_parsed_json(indicator, self.options)
        self.assertEqual(results.is_valid, False)
