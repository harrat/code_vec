def test_skip_system_check_option(run_reframe):
    returncode, stdout, _ = run_reframe(
        checkpath=['unittests/resources/checks/frontend_checks.py'],
        more_options=['--skip-system-check', '-t', 'NoSystemCheck']
    )
    assert 'PASSED' in stdout
    assert returncode == 0

