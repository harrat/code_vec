def test_kbd_interrupt_in_setup_with_limited_concurrency(
        async_runner, make_cases, make_async_exec_ctx):
    ctx = make_async_exec_ctx(2)
    next(ctx)

    runner, _ = async_runner
    with pytest.raises(KeyboardInterrupt):
        runner.runall(make_cases([
            SleepCheck(1), SleepCheck(1), SleepCheck(1),
            KeyboardInterruptCheck(phase='setup')
        ]))

    assert_interrupted_run(runner)

