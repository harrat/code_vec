def test_unrar_simple_cbr():
    file = "samples/simple.cbr"
    extracted_files = extract(file, EXTRACT_TO_DIR)
    assert extracted_files == ['a', 'b'], "extracts a simple cbr"
    assert os.path.isfile(os.path.join(EXTRACT_TO_DIR, 'a'))
    assert os.path.isfile(os.path.join(EXTRACT_TO_DIR, 'b'))

