def test_hosts_delete(self, centreon_con):
        data = dict()
        data['action'] = 'del'
        data['object'] = 'HOST'
        data['values'] = 'my_deleted_host'

        with patch('requests.post') as patched_post:
            centreon_con.hosts.delete('my_deleted_host', post_refresh=False)
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
