@pytest.mark.project
def testItemXMLPackUnpack():

    # Pack
    xContent = etree.SubElement(nwXML, "content")
    theItem.packXML(xContent)
    assert etree.tostring(xContent, pretty_print=False, encoding="utf-8") == (
        b"<content>"
        b"<item handle=\"0123456789abc\" order=\"1\" parent=\"0123456789abc\">"
        b"<name>A Name</name><type>TRASH</type><class>TRASH</class>"
        b"<status>Main</status><expanded>True</expanded>"
        b"</item>"
        b"</content>"
    )

    # Unpack
    assert theItem.unpackXML(xContent[0])
    assert theItem.itemHandle == "0123456789abc"
    assert theItem.parHandle == "0123456789abc"
    assert theItem.itemOrder == 1
    assert theItem.isExpanded
    assert theItem.charCount == 1
    assert theItem.wordCount == 1
    assert theItem.paraCount == 1
    assert theItem.cursorPos == 1
    assert theItem.itemClass == nwItemClass.TRASH
    assert theItem.itemType == nwItemType.TRASH
    assert theItem.itemLayout == nwItemLayout.NOTE

