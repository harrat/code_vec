@responses.activate
    def test_get(self):
        def callback(headers, params, request):
            if params.get("force_update", ["0"]) == ["1"]:
                status, body = 200, u"""
                <issue>
                  <created_at>2020-01-04 14:12:00 UTC</created_at>
                  <name>1160086</name>
                  <tracker>bnc</tracker>
                  <label>bsc#1160086</label>
                  <url>https://bugzilla.suse.com/show_bug.cgi?id=1160086</url>
                  <state>OPEN</state>
                  <summary>f�� bar</summary>
                  <owner>
                    <login>nemo</login>
                    <email>nemo@suse.com</email>
                    <realname>Ca�t�n Nemo</realname>
                  </owner>
                </issue>"""
            else:
                status, body = 200, """
                <issue>
                  <created_at>2020-01-04 14:12:00 UTC</created_at>
                  <name>1160086</name>
                  <tracker>bnc</tracker>
                  <label>bsc#1160086</label>
                  <url>https://bugzilla.suse.com/show_bug.cgi?id=1160086</url>
                </issue>"""
            return status, headers, body

        self.mock_request(
            method=responses.GET,
            url=re.compile(self.osc.url +
                           r'/issue_trackers/bnc/issues/1160086/?.*'),
            callback=CallbackFactory(callback)
        )

        with self.subTest("Manual force update"):
            response = self.osc.issues.get("bnc", 1160086, True)
            self.assertTrue(hasattr(response, "summary"))
            self.assertEqual(len(responses.calls), 2)

        with self.subTest("Manual force update"):
            response = self.osc.issues.get("bnc", 1160086, False)
            self.assertTrue(hasattr(response, "summary"))
            # to whom it may concern: `responses.calls` does not get reset
            # between sub-tests
            self.assertEqual(len(responses.calls),
                             6 if version_info.major < 3 else 4)
