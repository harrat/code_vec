@pytest.mark.skipif(os.environ.get("CONDA_BUILD", "0") == "1",
                    reason="running tests expecting network connection in Conda is not possible. "
                           "See https://github.com/conda/conda/issues/508")
@pytest.mark.skipif(not is_site_reachable("https://pypi.org/simple/cmake/"),
                    reason="pypi.org website not reachable")
def test_setup_requires_keyword_include_cmake(mocker, capsys):

    mock_setup = mocker.patch('skbuild.setuptools_wrap.upstream_setup')

    tmp_dir = _tmpdir('setup_requires_keyword_include_cmake')

    setup_requires = ['cmake>=3.10']

    tmp_dir.join('setup.py').write(textwrap.dedent(
        """
        from skbuild import setup
        setup(
            name="cmake_with_sdist_keyword",
            version="1.2.3",
            description="a minimal example package",
            author='The scikit-build team',
            license="MIT",
            setup_requires=[{setup_requires}]
        )
        """.format(setup_requires=",".join(["'%s'" % package for package in setup_requires]))
    ))
    tmp_dir.join('CMakeLists.txt').write(textwrap.dedent(
        """
        cmake_minimum_required(VERSION 3.5.0)
        project(test NONE)
        install(CODE "execute_process(
          COMMAND \\${CMAKE_COMMAND} -E sleep 0)")
        """
    ))

    with execute_setup_py(tmp_dir, ['build'], disable_languages_test=True):
        assert mock_setup.call_count == 1
        setup_kw = mock_setup.call_args[1]
        assert setup_kw['setup_requires'] == setup_requires
