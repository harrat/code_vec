def test_report_simple():
    runner = CliRunner()
    with mock_context():
        result = runner.invoke(
            cli,
            [
                "--format",
                "simple",
                "analysis",
                "report",
                "ab9092f7-54d0-480f-9b63-1bb1508280e2",
            ],
        )
