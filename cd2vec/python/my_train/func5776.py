def test_maps():
    """ Building required maps to avoid timeout on the primary test """
    for map_name in ('ancmed', 'colonial', 'empire', 'known_world_901', 'modern', 'standard',
                     'standard_france_austria', 'standard_germany_italy', 'world'):
        Map(map_name)

def test_3():
    """ Test case 3. """
    case_data = CaseData('3.json')
    run(case_data, ping_seconds=constants.DEFAULT_PING_SECONDS)
    # We must clear server caches to allow to re-create a Server with same test case but different server attributes.
    Server.__cache__.clear()
