def test_certwarn_true(self, request, httpbin_secure):
        """Test quiet_urllib=False shows warning from urllib3."""
        url = httpbin_secure.url
        http = Http(url=url, certwarn=True, save_history=True)

        with pytest.warns(InsecureRequestWarning):
            http()
