@pytest.mark.parametrize("archive_fixture", ["mock_targz", "mock_zip", "mock_fs"])
@pytest.mark.parametrize("directory,name,version,reqs", sources)
def test_source_dist(
    archive_fixture, directory, name, version, reqs, mock_targz, mock_zip, mocker
):
    mock_build = mocker.patch("req_compile.metadata.source._build_wheel")

    if archive_fixture == "mock_targz":
        archive = mock_targz(directory)
    elif archive_fixture == "mock_zip":
        archive = mock_zip(directory)
    else:
        archive = os.path.join(os.path.dirname(__file__), "source-packages", directory)

    metadata = req_compile.metadata.extract_metadata(archive)
    assert not mock_build.called

    if archive_fixture != "mock_fs":
        assert metadata.name == name
        if version is not None:
            assert metadata.version == pkg_resources.parse_version(version)
    if reqs is not None:
        assert set(metadata.reqs) == set(pkg_resources.parse_requirements(reqs))

