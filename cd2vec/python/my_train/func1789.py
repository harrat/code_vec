@all_modes()
def test_set_python(d, mode):
    kern = install(d, "%s --python=AAA MOD1"%mode)
    assert kern['k'][0] == 'AAA'

@all_modes()
def test_set_kernel_cmd(d, mode):
    kern = install(d, "%s --kernel-cmd='a b c d' MOD1"%mode)
    assert kern['k'] == ['a', 'b', 'c', 'd']
