def test_to_idna_single(self):
        """
        Runs and tests Converter
        """

        domain_to_test = "??etherwallet.com"

        expected = "xn--etherwallet-tv8eq7f.com"
        actual = Converter(domain_to_test).get_converted()

        self.assertEqual(expected, actual)
