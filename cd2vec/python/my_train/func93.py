def test_compile_hooks(local_exec_ctx):
    @fixtures.custom_prefix('unittests/resources/checks')
    class MyTest(HelloTest):
        def __init__(self):
            super().__init__()
            self.name = type(self).__name__
            self.executable = os.path.join('.', self.name)
            self.count = 0

        @rfm.run_before('compile')
        def setflags(self):
            self.count += 1

        @rfm.run_after('compile')
        def check_executable(self):
            exec_file = os.path.join(self.stagedir, self.executable)

            # Make sure that this hook is executed after compile_wait()
            assert os.path.exists(exec_file)

    test = MyTest()
    _run(test, *local_exec_ctx)
    assert test.count == 1

