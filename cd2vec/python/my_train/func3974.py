def test_commandset(self):
        """Test written OK with command set"""
        ds = dcmread(ct_name)
        del ds[:]
        del ds.preamble
        del ds.file_meta
        ds.CommandGroupLength = 8
        ds.MessageID = 1
        ds.MoveDestination = 'SOME_SCP'
        ds.Status = 0x0000
        ds.save_as(self.fp, write_like_original=True)
        self.fp.seek(0)
        with pytest.raises(EOFError):
            self.fp.read(128, need_exact_length=True)
        self.fp.seek(0)
        assert b'DICM' != self.fp.read(4)
        # Ensure Command Set Elements written as little endian implicit VR
        self.fp.seek(0)

        fp = BytesIO(self.fp.getvalue())  # Workaround to avoid #358
        ds_out = dcmread(fp, force=True)
        assert Dataset() == ds_out.file_meta
        assert 'Status' in ds_out
        assert 'PatientID' not in ds_out
        assert Dataset() == ds_out[0x00010000:]
