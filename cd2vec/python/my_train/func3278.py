def test_num_nets(self):
        pcb = Pcb(num_nets=5)
        assert Pcb.parse(pcb.to_string()) == pcb
        pcb.nets.append(Net())
        assert Pcb.parse(pcb.to_string()) == pcb
