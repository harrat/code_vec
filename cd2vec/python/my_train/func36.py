def test_multiple_same_key(thermostats_multiple_same_key):
    metrics = []
    for thermostat in thermostats_multiple_same_key:
        outputs = thermostat.calculate_epa_field_savings_metrics()
        metrics.extend(outputs)
    assert len(metrics) == 4

    for key in metrics[0]:
        assert(metrics[0][key] == metrics[2][key])

    for key in metrics[1]:
        # nan can never be equal, so skip nan
        if not (isinstance(metrics[1][key], float) and np.isnan(metrics[1][key])):
            assert(metrics[1][key] == metrics[3][key])

