def test_parse_unicode_name_two_byte_joliet(tmpdir):
    indir = tmpdir.mkdir('unicodetwobytejoliet')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'f?o'), 'wb') as outfp:
        outfp.write(b'foo\n')

    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-J',
                     '-no-pad', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_unicode_name_two_byte_joliet)

def test_parse_unicode_name_two_byte_udf(tmpdir):
    indir = tmpdir.mkdir('unicodeudftwobyte')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'f?o'), 'wb') as outfp:
        outfp.write(b'foo\n')
