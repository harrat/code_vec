def test_handler_noappend(temp_runtime, logfile):
    runtime = temp_runtime(
        {
            'level': 'info',
            'handlers': [
                {
                    'type': 'file',
                    'name': logfile,
                    'level': 'warning',
                    'format': '[%(asctime)s] %(levelname)s: %(message)s',
                    'datefmt': '%F',
                    'append': False,
                }
            ],
            'handlers_perflog': []
        }

    )
    next(runtime)

    rlog.configure_logging(rt.runtime().site_config)
    rlog.getlogger().warning('foo')
    _close_handlers()

    # Reload logger
    rlog.configure_logging(rt.runtime().site_config)
    rlog.getlogger().warning('bar')

    assert not _found_in_logfile('foo', logfile)
    assert _found_in_logfile('bar', logfile)

