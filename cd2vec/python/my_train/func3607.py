def test_indent_around_function(get_log):

    @logger.indented("hey")
    def foo():
        logger.info("001")

    foo()

    assert get_log() == "hey\n001\nDONE in no-time (hey)\n"

