def test_add_onetime_job_without_trigger_time(my_schedule):
    my_schedule.once().do(foo, msg='hello')
    if len(my_schedule.jobs) != 1:
        raise AssertionError("Job is not added to schedule.")
    time.sleep(1)  # wait for worker threads to start
    my_schedule.run_pending()
    if len(my_schedule.jobs) != 0:
        raise AssertionError("Job is not executed by schedule.")

