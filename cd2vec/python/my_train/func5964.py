def test_airborne_survey():
    """
    Test if the synthetic airborne survey returns the expected survey
    """
    # Expected region for the default data_region
    expected_region = (-4.99975, -4.00003, 56.00011, 56.49997)
    survey = airborne_survey()
    assert set(survey.columns) == set(["longitude", "latitude", "height"])
    assert survey.longitude.size == 5673
    npt.assert_allclose(survey.longitude.min(), expected_region[0])
    npt.assert_allclose(survey.longitude.max(), expected_region[1])
    npt.assert_allclose(survey.latitude.min(), expected_region[2])
    npt.assert_allclose(survey.latitude.max(), expected_region[3])
    npt.assert_allclose(survey.height.min(), 359.0)
    npt.assert_allclose(survey.height.max(), 1255.0)

