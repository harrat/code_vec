def test_pathspec_default_search(self):
        path = self.root.make_file(name='FFF555.txt')
        base = path.parent

        spec = PathSpec(
            DirSpec(template=str(base)),
            FileSpec(
                StrPathVar('id', pattern='[A-Z0-9_]+', invalid=('ABC123',)),
                StrPathVar('ext', pattern='[^\.]+', valid=('txt', 'exe')),
                template='{id}.{ext}'))

        all_paths = spec.find()
        assert 1 == len(all_paths)
        assert path_inst(path, dict(id='FFF555', ext='txt')) == all_paths[0]
