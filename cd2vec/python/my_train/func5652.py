@staticmethod
    def test_cli_create_reference_002(snippy):
        """Try to create reference from CLI.

        Try to create new reference without defining mandatory content the
        ``links`` attribute.
        """

        content = {
            'data': [
                Reference.GITLOG
            ]
        }
        data = 'must not be used'
        brief = content['data'][0]['brief']
        groups = Reference.GITLOG['groups']
        tags = content['data'][0]['tags']
        cause = snippy.run(['snippy', 'create', '--scat', 'reference', '--brief', brief, '--groups', groups, '--tags', tags, '-c', data, '--no-editor'])  # pylint: disable=line-too-long
        assert cause == 'NOK: content was not stored because mandatory content field links is empty'
        Content.assert_storage(None)
