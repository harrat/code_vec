def test_multitype(self):
		from cromulent.vocab import make_multitype_obj, Painting, Drawing
		inst = make_multitype_obj(Painting, Drawing)
		self.assertTrue(isinstance(inst, Painting))
		self.assertTrue(len(inst.classified_as) == 2)
		self.assertTrue(inst.classified_as[1].id == "http://vocab.getty.edu/aat/300033973")

		from cromulent.model import HumanMadeObject

		inst = make_multitype_obj(HumanMadeObject, Painting)
		self.assertTrue(len(inst.classified_as) == 1)
		self.assertTrue(inst.classified_as[0].id == "http://vocab.getty.edu/aat/300033618")
