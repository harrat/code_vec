def test_metric_helper_metadata_exception(self):
        class MyMetric(MetricHelper):
            _name = self.metric_name
            _fields = self.metric_fields

        with pytest.raises(Base10Error) as exc:
            metric = MyMetric(**self.metric_values)

        assert '_metadata is required' == str(exc.value)
