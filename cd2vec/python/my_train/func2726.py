def test_restore_or_delete_policy_delete(self):
        """
        Test that the PolicyDirectoryMonitor can correctly delete policy data
        upon a policy file change.
        """
        m = monitor.PolicyDirectoryMonitor(
            self.tmp_dir,
            multiprocessing.Manager().dict()
        )
        m.logger = mock.MagicMock(logging.Logger)

        m.policy_cache = {
            "policy_A": []
        }
        m.policy_store["policy_A"] = {'{"policy_4"}'}
        m.policy_map["policy_A"] = os.path.join(self.tmp_dir, "policy_4.json")

        m.restore_or_delete_policy("policy_A")

        m.logger.info.assert_called_once_with("Removing policy: policy_A")
        self.assertNotIn("policy_A", m.policy_cache.keys())
        self.assertNotIn("policy_A", m.policy_store.keys())
        self.assertNotIn("policy_A", m.policy_map.keys())
