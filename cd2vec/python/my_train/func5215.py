@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_clean_working_dir(fixture, request):
    """ Because of some weird results that seemed to indicate the wrong run dir
    """
    engine = request.getfixturevalue(fixture)
    job = engine.launch(image='alpine', command='ls')
    print(job.rundata)
    job.wait()
    assert job.stdout.strip() == ''

