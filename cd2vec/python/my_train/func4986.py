@pytest.mark.parametrize('kwargs', [dict(country='UK'),
                                    dict(referrer=False, cookies=True,
                                         user_agent=False)])
def test_error_no_proxy_found(get_proxy, kwargs):
    with pytest.raises(proxipy.NoProxyFound):
        get_proxy(**kwargs)
