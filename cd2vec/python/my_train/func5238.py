def test_docopt_ng_as_magic_docopt_more_magic_global_arguments_and_dot_access():
    doc = """Usage: prog [-vqr] [FILE]
              prog INPUT OUTPUT
              prog --help

    Options:
      -v  print status messages
      -q  report only file names
      -r  show all occurrences of the same error
      --help

    """
    global arguments
    magic_docopt(doc, "-v file.py")
    assert arguments == {"-v": True, "-q": False, "-r": False, "--help": False, "FILE": "file.py", "INPUT": None, "OUTPUT": None}
    assert arguments.v == True
    assert arguments.FILE == "file.py"
    arguments = None
    magic(doc, "-v file.py")
    assert arguments == {"-v": True, "-q": False, "-r": False, "--help": False, "FILE": "file.py", "INPUT": None, "OUTPUT": None}
    assert arguments.v == True
    assert arguments.FILE == "file.py"
    arguments = None
    user_provided_alias_containing_magic(doc, "-v file.py")
    assert arguments == {"-v": True, "-q": False, "-r": False, "--help": False, "FILE": "file.py", "INPUT": None, "OUTPUT": None}

