def test_task_runner(mocked_aggregated):
    """Test task runner in nominal operations."""

    def task(node, pattern, aggregator):
        if node.name != 'node2':
            aggregator._output_queue.put(pattern)

    kwargs = {'aggregator': mocked_aggregated._aggregator}
    result = mocked_aggregated._aggregator._runner(task, 'pattern', **kwargs)
    assert result == ['pattern', 'pattern']

