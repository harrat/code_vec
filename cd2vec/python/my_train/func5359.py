def test_description():
    global cu
    cu.execute("select * from tests")
    assert cu.description == (
        ('id', py2jdbc.INTEGER, MAX_INT, None, 0, 0, False),
        ('name', py2jdbc.VARCHAR, MAX_INT, None, 0, 0, True),
        ('integer_field', py2jdbc.INTEGER, MAX_INT, None, 0, 0, True),
        ('real_field', py2jdbc.REAL, MAX_INT, None, 0, 0, True),
        ('text_field', py2jdbc.VARCHAR, MAX_INT, None, 0, 0, True),
        ('blob_field', py2jdbc.BLOB, MAX_INT, None, 0, 0, True),
    )
    cu.execute("select null from tests")
    assert cu.description == (
        ('null', py2jdbc.NULL, MAX_INT, None, 0, 0, True),
    )
