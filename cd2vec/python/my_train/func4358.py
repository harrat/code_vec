@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_delete_reference_012(snippy):
        """Delete reference with search.

        Delete reference based on search keyword that results one hit. In this
        case the content is deleted.
        """

        content = {
            'data': [
                Reference.REGEXP
            ]
        }
        Content.assert_storage_size(2)
        cause = snippy.run(['snippy', 'delete', '--sall', 'chris', '--scat', 'reference'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
