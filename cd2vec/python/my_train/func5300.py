def test_search_simple(self):
        """
        Test Basic, simple query param
        """
        # test search engine for terms
        results = self.client.search(query='gene_ontology')
        self.assertGreaterEqual(len(results), 15)
        i = 0
        for term in results:
            if i == 14:
                term_2 = term
                term_3 = self.client.detail(term)
                self._checkMixed(term_3)
            i += 1

        self._checkTerms(results)
        term_1 = results[14]
        self.assertEqual(term_2, term_1)
