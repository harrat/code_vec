@freeze_time('2014-01-21')
def test_fix_entries_start_time_without_previous(cli, entries_file):
    entries_file.write("""21/01/2014
fail     -0830  Repair coffee machine
""")
    cli('commit')

    lines = entries_file.readlines()

    assert lines[1] == 'fail     -0830  Repair coffee machine\n'

