def test_cached(cache_obj):
    for _ in range(10):
        cache_obj.test1(8, 0)

    assert len(c) == 1
    assert cache_obj.test1(8, 0) == 1

    for _ in range(10):
        cache_obj.test2()

    assert cache_obj.test2() == 1
    assert len(c) == 2

    c.clear()
    assert len(c) == 0

    # Make sure the cached function is properly wrapped
    assert cache_obj.test2.__doc__ == 'running test2'

