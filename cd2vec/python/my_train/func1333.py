@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_nfl_kicker_returns_requested_career_stats(self, *args, **kwargs):
        # Request the career stats
        player = Player('LutzWi00')
        player = player('')

        for attribute, value in self.kicker_results_career.items():
            assert getattr(player, attribute) == value
