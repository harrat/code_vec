@pytest.mark.vcr()
def test_scan_export_chapters_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.scans.export(1, chapters=['nothing to see here'])
