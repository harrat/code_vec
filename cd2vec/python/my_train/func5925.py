def test_register_same_name(self):
        """
        Trying to register a Tukio task with a name already used must raise a
        `ValueError` exception.
        """
        register('my-coro-task')(other_coro)
        with self.assertRaisesRegex(ValueError, 'already registered'):
            register('my-coro-task')(other_coro)
