@staticmethod
    @pytest.mark.usefixtures('yaml', 'default-references', 'export-time')
    def test_cli_export_reference_018(snippy):
        """Export reference defaults.

        Export reference defaults. All references should be exported into
        predefined file location under tool data folder in yaml format.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--defaults', '--scat', 'reference'])
            assert cause == Cause.ALL_OK
            defaults_references = pkg_resources.resource_filename('snippy', 'data/defaults/references.yaml')
            Content.assert_yaml(yaml, mock_file, defaults_references, content)
