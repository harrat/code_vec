def test_fronius_get_inverter_realtime_data_system(self):
        res = asyncio.get_event_loop().run_until_complete(
            self.fronius.current_system_inverter_data())
        self.assertEqual(res, GET_INVERTER_REALTIME_DATA_SYSTEM)
