def test_no_server(self):
        # set up a fronius client and aiohttp session
        self.session = aiohttp.ClientSession()
        self.fronius = pyfronius.Fronius(self.session, self.url)
        try:
            asyncio.get_event_loop().run_until_complete(
                self.fronius.current_system_meter_data())
            self.fail("No Exception for failed connection to fronius")
        except ConnectionError:
            asyncio.get_event_loop().run_until_complete(
                self.session.close())
