def test_process_del(self):
        class MockProcessListener(EventListener):
            def __init__(self):
                super().__init__()
                self.executed = False

            def execute(self, process: Process, **kwargs) -> None:
                self.executed = True

        listener: MockProcessListener = MockProcessListener()
        p = Process('cat', stdin=PIPE, stdout=PIPE)
        p.register_listener(EventType.CLOSE, listener)
        del p
        self.assertTrue(listener.executed)
