@freeze_time('2014-01-21')
def test_ignore_date_error_previous_day(cli, entries_file):
    entries_file.write("""17/01/2014
alias_1 2 foobar
""")
    stdout = cli('commit')
    assert 'Are you sure' in stdout

