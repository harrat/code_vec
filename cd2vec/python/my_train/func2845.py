def test_set_attribute_with_non_client_modifiable_attribute(self):
        """
        Test that a KmipError is raised when attempting to set the value of
        a attribute not modifiable by the client.
        """
        e = engine.KmipEngine()
        e._protocol_version = contents.ProtocolVersion(2, 0)
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        args = (
            payloads.SetAttributeRequestPayload(
                unique_identifier="1",
                new_attribute=objects.NewAttribute(
                    attribute=primitives.Enumeration(
                        enums.CryptographicAlgorithm,
                        enums.CryptographicAlgorithm.RSA,
                        enums.Tags.CRYPTOGRAPHIC_ALGORITHM
                    )
                )
            ),
        )

        self.assertRaisesRegex(
            exceptions.KmipError,
            "The 'Cryptographic Algorithm' attribute is read-only and cannot "
            "be modified by the client.",
            e._process_set_attribute,
            *args
        )
