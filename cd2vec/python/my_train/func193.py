def test_donor_alignment_hdr_indel_size(donor_alignment_contiguous_insert,
                                        donor_alignment_noncontiguous_insert,
                                        donor_alignment_insert_and_deletion,
                                        donor_alignment_deletion):
    assert donor_alignment_contiguous_insert.hdr_indel_size == 5
    assert donor_alignment_noncontiguous_insert.hdr_indel_size == 7
    assert donor_alignment_insert_and_deletion.hdr_indel_size == 3
    assert donor_alignment_deletion.hdr_indel_size == -4

