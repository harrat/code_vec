@pytest.mark.core
def testConfigSetPanePos(nwTemp,nwRef):
    tmpConf = path.join(nwTemp,"novelwriter.conf")
    refConf = path.join(nwRef, "novelwriter.conf")
    assert theConf.setMainPanePos([0, 0])
    assert theConf.confChanged
    assert theConf.setMainPanePos([300, 800])
    assert theConf.setDocPanePos([400, 400])
    assert theConf.setViewPanePos([500, 150])
    assert theConf.setOutlinePanePos([500, 150])
    assert theConf.saveConfig()
    assert cmpFiles(tmpConf, refConf, [2])
    assert not theConf.confChanged

@pytest.mark.core
def testConfigFlags(nwTemp,nwRef):
    tmpConf = path.join(nwTemp,"novelwriter.conf")
    refConf = path.join(nwRef, "novelwriter.conf")
    assert not theConf.setShowRefPanel(False)
    assert theConf.setShowRefPanel(True)
    assert theConf.confChanged
    assert theConf.saveConfig()
    assert cmpFiles(tmpConf, refConf, [2])
    assert not theConf.confChanged
