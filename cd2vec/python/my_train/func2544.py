def test_save_load(self):
        model_config = core.ModelConfig(_lineworld_name)
        random_agent = tfagents.TfRandomAgent(model_config=model_config)
        tempdir = bcore._get_temp_path()
        bcore._mkdir(tempdir)
        random_agent.save(directory=tempdir, callbacks=[])
        random_agent.load(directory=tempdir, callbacks=[])
        bcore._rmpath(tempdir)
