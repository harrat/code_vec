def test_run_log(self):
        """Test that attempts to access attributes from the parent object"""
        filelog = 'test_run_base_log.py'
        launch = Launcher(filelog, 'logs made out of wood!')
        excode = launch.run()
        assert excode == 0
