def test_ignores_not_set_values(command_line_config):
    with pytest.raises(KeyError):
        return command_line_config['var']
