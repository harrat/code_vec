def test_reducing():
    # Test that the best score never goes up
    start_values = [0.1, 0.1] # Starting guess

    best_score = calculate_score(start_values)
    monitor_called = 0
    
    def monitor(gen, best, pop):
        nonlocal best_score
        nonlocal monitor_called
        score = best[0]
        assert score <= best_score
        best_score = score
        monitor_called += 1
        
    # Call monitor at each generation
    result = optimiser.optimise(start_values,
                                [ControlIndex(0), ControlIndex(1)],
                                calculate_score,
                                maxgen=10,
                                monitor=monitor)
    assert monitor_called > 0
    assert result[0] <= best_score

### Internal components of optimiser algorithm
