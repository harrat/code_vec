@pytest.mark.fast
@pytest.mark.perf
def test_integrate_perf_simps() -> None:
    import time

    n = 10000
    m = np.random.uniform(-10, 10, n)
    b = np.random.uniform(-10, 10, n)
    grid = np.sort(np.random.uniform(-1000, 1000, 1000))
    y = np.empty([n, len(grid)])
    for i in range(n):
        y[i, :] = m[i] * grid + b[i]

    start = time.time()
    for i in range(n):
        _int_simps_nonunif(grid, y[i])
    total_custom = time.time() - start

    start = time.time()
    for i in range(n):
        simps(y[i], x=grid)
    total_lib = time.time() - start

    # just make sure we are at least doing better than scipy
    assert total_custom < total_lib
    print("Custom simps integration time: ", total_custom)
    print("Scipy simps integration time: ", total_lib)

