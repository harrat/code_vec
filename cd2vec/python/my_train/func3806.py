def test_WHOIS_utf_encoding():
    sleep(.1)
    whois = WHOIS("???.??")
    assert str(whois.creation_date()) == '2004-01-06'
    assert whois.registrar() == '????????????'

