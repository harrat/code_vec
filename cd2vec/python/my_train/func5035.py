@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_update_snippet_011(snippy):
        """Update snippet with ``--content`` option.

        Try to update snippet with empty content data. Nothing must be updated
        in this case because there is more than one content stored.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        cause = snippy.run(['snippy', 'update', '-c', ''])
        assert cause == 'NOK: cannot use empty content data for update operation'
        Content.assert_storage(content)
