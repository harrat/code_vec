def test_render_archive():

    entries = [Entry.entry_from_db(
        os.path.join(CONFIG['content_root'], e.get('filename')), e.doc_id) for e in
        DB.posts.all()]

    render_archive(entries[ARCHIVE_SIZE:])
    # pages should not be in the archive
    with open(os.path.join(CONFIG['output_to'], 'archive', 'index.html')) as html_index:
        soup = BeautifulSoup(html_index.read(), 'html.parser')
        assert len(soup.find_all(class_='post')) == 12
