def test_plot_bo():
    """Assert that the plot_bo method work as intended."""
    # When its not fitted
    trainer = TrainerRegressor(['lasso', 'ridge'], metric='r2', bagging=0)
    pytest.raises(NotFittedError, trainer.plot_bo)

    # When called without running the bayesian optimization
    trainer.run(reg_train, reg_test)
    pytest.raises(PermissionError, trainer.plot_bo)

    # When correct
    trainer = TrainerRegressor(['lasso', 'ridge'], metric='r2', n_calls=10)
    trainer.run(reg_train, reg_test)
    trainer.plot_bo(filename=FILE_DIR + 'bagging1', display=False)
    trainer.lasso.plot_bo(filename=FILE_DIR + 'bagging2', display=False)
    assert glob.glob(FILE_DIR + 'bagging1.png')
    assert glob.glob(FILE_DIR + 'bagging2.png')

    # From ATOM
    atom = ATOMRegressor(X_reg, y_reg, random_state=1)
    atom.run('lasso', metric='max_error', n_calls=10)
    atom.plot_bo(filename=FILE_DIR + 'bagging3', display=False)
    atom.lasso.plot_bo(filename=FILE_DIR + 'bagging4', display=False)
    assert glob.glob(FILE_DIR + 'bagging3.png')
    assert glob.glob(FILE_DIR + 'bagging4.png')

