def test_do_package(self):
        # prepare test
        project = misc_file("mixed-ns-project")
        output = tempfile.mkdtemp()
        args = parse_args(["--format", "eu.etsi.osm",
                           "-p", project,
                           "-o", output])
        p = PM.new_packager(args, pkg_format=args.pkg_format)

        # execute
        napdr = p._do_package()

        # checks
        self.assertIsNone(napdr.error)
        packages = os.listdir(output)
        basename = os.path.basename(output)
        basename_ns = "_".join([basename, "osm_nsd"])
        basename_vnf = "_".join([basename, "osm_vnfd"])
        filename_ns = basename_ns + ".tar.gz"
        filename_vnf = basename_vnf + ".tar.gz"
        self.assertIn(filename_ns, packages)
        self.assertIn(filename_vnf, packages)
        ns = ["osm_nsd.yaml",
              "checksums.txt",
              "icons/upb_logo.png",
              "vnf_config",
              "scripts",
              "ns_config",
              "icons"]
        vnf = ["osm_vnfd.yaml",
               "checksums.txt",
               "scripts",
               "images",
               "icons",
               "cloud_init",
               "charms",
               "icons/upb_logo.png",
               "cloud_init/cloud.init",
               "images/mycloudimage.ref"]
        for package in packages:
            with tarfile.open(os.path.join(output, package)) as f:
                member_names = list(
                    map(lambda member: member.name, f.getmembers()))
                if "ns" in package:
                    for member in ns:
                        self.assertIn(os.path.join(basename_ns, member),
                                      member_names)
                elif "vnf" in package:
                    for member in vnf:
                        self.assertIn(os.path.join(basename_vnf, member),
                                      member_names)
