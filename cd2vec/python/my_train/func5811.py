@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'yaml')
    def test_cli_import_snippet_002(snippy):
        """Import all snippet resources.

        Import all snippets from yaml file. File name and format are extracted
        from command line ``--file`` option.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.NETCAT
            ]
        }
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '-f', './all-snippets.yaml'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './all-snippets.yaml', mode='r', encoding='utf-8')
