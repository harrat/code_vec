@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'yaml')
    def test_cli_import_solution_003(snippy):
        """Import all solution resources.

        Import all solutions from yaml file without specifying the solution
        category. File name and format are extracted from command line
        ``--file`` option.
        """

        content = {
            'data': [
                Solution.KAFKA,
                Solution.BEATS
            ]
        }
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '-f', './all-solutions.yaml'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './all-solutions.yaml', mode='r', encoding='utf-8')
