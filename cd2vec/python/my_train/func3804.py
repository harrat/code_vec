def test_open_vocab_format_uppercase(self):
        malware = copy.deepcopy(self.valid_malware)
        malware['malware_types'] += "Ransomware"
        self.assertFalseWithOptions(malware,
                                    disabled='malware-types')
