def test_create_order():
    with alfacoins_mockup_gateway(Root()) as gateway:
        order = gateway.create_order(
            type='litecointestnet',
            amount=1.2345,
            currency='USD',
            order_id=1,
            options={
                'notificationURL': 'http://abc.com/notify',
                'redirectURL': 'http://abc.com/redirect',
                'payerName': 'Bob',
                'payerEmail': 'no_reply@alfacoins.com'
            },
            description='This is a test order',
     )
        assert order == CREATE_ORDER_RESPONSE
