@staticmethod
    def test_cli_create_reference_008(snippy):
        """Try to create reference from CLI.

        Try to create new reference by from command line when the content
        category contains two values. The ``--scat`` option must specify
        an unique content category when new content is created.
        """

        cause = snippy.run(['snippy', 'create', '--scat', 'reference,snippet', '--links', 'http://short', '--no-editor'])
        assert cause == "NOK: content category must be unique when content is created: ('reference', 'snippet')"
        Content.assert_storage(None)
