def test_simple_nonimmediate_deletion(self):
        ''' verify (insert a) vs. (insert b, insert a, delete b) '''

        kvs_empty = dict(self.kvs)
        ref_kvs_empty = dict(self.ref_kvs)

        ''' insert some content and record data structure state '''
        content_a = bytes([self.random.randint(0, 255)
                           for _ in range(2 * 1024 * 1024)])
        k_a = self.seccs.put_content(content_a)

        kvs_with_a = dict(self.kvs)
        ref_kvs_with_a = dict(self.ref_kvs)

        ''' delete other content and ensure that data structure is empty again '''
        self.seccs.delete_content(k_a)
        self.assertEqual(self.kvs, kvs_empty)
        self.assertEqual(self.ref_kvs, ref_kvs_empty)

        ''' insert some content b, then a, then delete b '''
        content_b = bytes([self.random.randint(0, 255)
                           for _ in range(2 * 1024 * 1024)])
        k_b = self.seccs.put_content(content_b)

        self.seccs.put_content(content_a)

        self.seccs.delete_content(k_b)

        ''' data structure must be in the same state as it would have been if only a was inserted '''
        self.assertEqual(self.kvs, kvs_with_a)
        self.assertEqual(self.ref_kvs, ref_kvs_with_a)
