def test_no_type(self):
        custom_obj = copy.deepcopy(self.valid_custom_object)
        del custom_obj['type']
        self.assertRaises(ValidationError, validate_parsed_json, custom_obj, self.options)
