def test_makepipebranch():
    """py.test for makepipebranch"""
    tdata = (
        (
            "p_branch",
            [
                "BRANCH",
                "p_branch",
                0.0,
                "",
                "Pipe:Adiabatic",
                "p_branch_pipe",
                "p_branch_pipe_inlet",
                "p_branch_pipe_outlet",
                "Bypass",
            ],
            [
                "PIPE:ADIABATIC",
                "p_branch_pipe",
                "p_branch_pipe_inlet",
                "p_branch_pipe_outlet",
            ],
        ),  # pb_name, branch_obj, pipe_obj
    )
    for pb_name, branch_obj, pipe_obj in tdata:
        fhandle = StringIO("")
        idf = IDF(fhandle)
        result = hvacbuilder.makepipebranch(idf, pb_name)
        assert result.obj == branch_obj
        thepipe = idf.getobject("PIPE:ADIABATIC", result.Component_1_Name)
        assert thepipe.obj == pipe_obj

