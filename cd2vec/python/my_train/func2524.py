def test_generate(changer):
    with mock.patch.object(changer, 'print_licenses') as mocked_print:
        # list_option option given
        changer.list = True
        changer.generate()
        assert mocked_print.call
        # invalid license
        with mock.patch('os.path.join') as mocked_join:
            changer.license = 'INVALID'
            changer.generate()
            assert mocked_print.call
            mocked_join.assert_not_called()

    changer.projectDir = gettempdir()
    changer.license = 'NEW-BSD'
    changer.list = False
    licFile = os.path.join(changer.projectDir, 'LICENSE')
    if os.path.exists(licFile):
        os.remove(licFile)

    # don't mess with setup.py & setup.cfg tested above already
    with mock.patch('os.path.exists', return_value=False):
        changer.generate()

    assert os.path.exists(licFile)
    os.remove(licFile)

