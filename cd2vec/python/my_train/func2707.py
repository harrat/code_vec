def test_implements_no_implementation_setter():
    """
    Case: do not implement interface member that is setter.
    Expect: class does not implement interface member error message.
    """
    class HumanNameInterface:

        @property
        def name(self):
            return

        @name.setter
        def name(self, new_name):
            return

    with pytest.raises(InterfaceMemberHasNotBeenImplementedException) as error:

        @implements(HumanNameInterface)
        class HumanWithoutImplementation:

            @property
            def name(self):
                return

    assert 'class HumanWithoutImplementation does not implement ' \
           'interface member HumanNameInterface.name(self, new_name)' == error.value.message
