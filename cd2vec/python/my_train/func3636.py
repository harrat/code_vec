def test_no_debug_no_report(self):
        self.base_config["debug"] = False
        self.base_config["report"] = False
        with test_utils.lauch(self.base_config) as cfg:
            self.base_test(cfg)
