def test_check_and_add_error():
    from processor.helper.httpapi.http_utils import check_and_add_error
    from processor.helper.config.rundata_utils import save_currentdata, get_from_currentdata
    save_currentdata(None)
    check_and_add_error(200, 'Failed http get')
    value = get_from_currentdata('errors')
    assert value is None
    check_and_add_error(400, 'Failed http get')
    value = get_from_currentdata('errors')
    assert value is not None

