def test_loadbalanced_delitem_method(mocked_loadbalanced):
    """Test FlaskMultiRedis loadbalanced __delitem__ method."""

    del(mocked_loadbalanced['name'])
    for node in mocked_loadbalanced._redis_nodes:
        assert not hasattr(node, 'name')
    mocked_loadbalanced._redis_nodes = []
    del(mocked_loadbalanced['name'])

