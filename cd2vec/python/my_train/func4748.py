def test_game_history():
    """ Test a complete 1 player game and validate the full history (except last phase) """
    _ = Server()            # Initialize cache to prevent timeouts during tests
    game_path = os.path.join(FILE_FOLDER_NAME, 'game_data_1_history.csv')
    run_with_timeout(lambda: run_game_data(1, ['NO_PRESS', 'IGNORE_ERRORS', 'POWER_CHOICE'], game_path), 60)

def test_game_7():
    """ Test a complete 7 players game """
    _ = Server()            # Initialize cache to prevent timeouts during tests
    game_path = os.path.join(FILE_FOLDER_NAME, 'game_data_7.csv')
    run_with_timeout(lambda: run_game_data(7, ['NO_PRESS', 'IGNORE_ERRORS', 'POWER_CHOICE'], game_path), 60)
