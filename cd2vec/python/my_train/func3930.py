def test_get_tokenized_data_two_indices(self):

        scope = idgen.generate(__name__)



        self.provider.textIndex(scope, { 'title': True })

        self.provider.textIndex(scope, { 'text': True })

        for n in range(20):

            self.provider.insert(scope, idgen.generate(__name__), { "title": piglet(400), "text": wiglaf(400), "order": n })

          

        analysis = self.provider.textAnalysis(scope)

        terms = analysis.terms()

        sample_key = terms[0]

        scores = analysis.scores(sample_key)

        top = analysis.top(sample_key)

        filtered = analysis.filter(lambda p: p.startswith(sample_key[0]))

        filtered_keys = filtered.keys()

        for p in filtered_keys:

            self.assertEqual(p[0], sample_key[0])

            self.assertTrue(p in terms)
