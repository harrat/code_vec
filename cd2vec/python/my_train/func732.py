def test_process_end(self):
        """Test process method for invalid."""
        self.skill.logic = {}
        @self.skill.session_ended
        def sample_func():
            """Decorated function."""
            pass
        actual = self.skill.process(data.SAMPLE_SESSION_ENDED_REQUEST)
        expected = '"response": {"shouldEndSession": true}'
        self.assertRegexpMatches(actual, expected)
