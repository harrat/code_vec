def test_with_none_in_dcard_instance(self, dcard):
        posts = dcard.posts
        assert not posts.ids
        assert not posts.metas
        assert not posts.only_id
