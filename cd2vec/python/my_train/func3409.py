def test_repository_filter_branches_by_older_than(mock_repository, mock_branch_pool):
    all_branches = set(mock_branch_pool.values())
    result = mock_repository.filter_branches(all_branches, older_than=1)
    assert result == [mock_branch_pool['older_than']]

