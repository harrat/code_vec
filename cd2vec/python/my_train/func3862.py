@mock.patch('requests.head', side_effect=mock_request)
    @mock.patch('requests.get', side_effect=mock_pyquery)
    def test_invalid_default_year_reverts_to_previous_year(self,
                                                           *args,
                                                           **kwargs):
        flexmock(utils) \
            .should_receive('_find_year_for_season') \
            .and_return(2019)

        roster = Roster('NOR')

        assert len(roster.players) == 5

        for player in roster.players:
            assert player.name in ['Drew Brees', 'Demario Davis',
                                   'Tommylee Lewis', 'Wil Lutz',
                                   'Thomas Morstead']
