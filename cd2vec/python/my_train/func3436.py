@pytest.mark.skipif(os.name == 'nt', reason='Irrelevant on Windows')
    @mock.patch('ghost._write_passphrase_file', _mock_write_passphrase_file)
    def test_init_permission_denied_on_passphrase(self):
        fd, temp_file = tempfile.mkstemp()
        os.close(fd)
        os.remove(temp_file)
        result = _invoke('init_stash "{0}" -p whatever'.format(temp_file))
        assert 'Expected OSError' in str(result.exception)
        assert 'Removing stale stash and passphrase' in str(result.output)
        assert type(result.exception) == SystemExit
        assert result.exit_code == 1
        # Since the invocation process both creates and deletes the stash
        # in case of failure, there's no way to verify that the file
        # exists in the middle of the test. This is a reasonable assumption
        # though as otherwise the removal statement will not be covered.
        assert not os.path.isfile(temp_file)
