def test_publish_filtered_input_repos(command_tester):
    """publishes the provided repos that pass the filter"""
    fake_publish = FakePublish()
    fake_pulp = fake_publish.pulp_client_controller
    _add_repo(fake_pulp)

    command_tester.test(
        fake_publish.main,
        [
            "test-publish",
            "--pulp-url",
            "https://pulp.example.com",
            "--published-before",
            "2019-09-11",
            "--repo-url-regex",
            "/unit/3/",
            "--repo-ids",
            "repo1,repo2,repo3",
        ],
    )

    # provided repos that were published before 2019-08-11 and
    # has relative url matching '/unit/3/'is published
    assert [hist.repository.id for hist in fake_pulp.publish_history] == ["repo3"]

