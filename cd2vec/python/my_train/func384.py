def test_list_empty_prgenvs_check_and_options(run_reframe):
    returncode, stdout, _ = run_reframe(
        checkpath=['unittests/resources/checks/frontend_checks.py'],
        action='list',
        environs=[],
        more_options=['-n', 'NoPrgEnvCheck'],
    )
    assert 'Found 0 check(s)' in stdout
    assert returncode == 0

