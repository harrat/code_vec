def test_combined_access_verbatim_constraint(make_job, slurm_only):
    job = make_job(sched_access=['--constraint=c1'])
    job.options = ['#SBATCH --constraint=c2', '#SBATCH -C c3']
    prepare_job(job)
    with open(job.script_filename) as fp:
        script_content = fp.read()

    assert re.search(r'(?m)--constraint=c1$', script_content)
    assert re.search(r'(?m)^#SBATCH --constraint=c2$', script_content)
    assert re.search(r'(?m)^#SBATCH -C c3$', script_content)

