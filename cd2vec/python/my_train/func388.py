def test_ebook_save(ebook):
    f = 'test.mobi'
    ebook.save(f)
    assert os.path.isfile(f)
    os.remove(f)

