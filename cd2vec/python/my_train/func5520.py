@staticmethod
    @pytest.mark.usefixtures('import-forced', 'update-remove-utc')
    def test_api_create_snippet_013(server):
        """Update snippet with POST that maps to PUT.

        Send POST /snippets with X-HTTP-Method-Override header to update
        resource. In this case the resource exists and the content is updated.
        """

        storage = {
            'data': [
                Storage.remove
            ]
        }
        storage['data'][0]['uuid'] = Snippet.FORCED_UUID
        request_body = {
            'data': {
                'type': 'snippet',
                'attributes': Request.remove
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '811'
        }
        expect_body = {
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/snippets/' + Snippet.FORCED_UUID
            },
            'data': {
                'type': 'snippet',
                'id': Snippet.FORCED_UUID,
                'attributes': storage['data'][0]
            }
        }
        result = testing.TestClient(server.server.api).simulate_post(
            path='/api/snippy/rest/snippets/53908d68425c61dc',
            headers={'accept': 'application/vnd.api+json', 'X-HTTP-Method-Override': 'PUT'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
