def test_correlations_2d(self):
        data = np.random.multivariate_normal([0, 0], [[1, 0], [0, 1]], size=100000)
        parameters = ["x", "y"]
        c = ChainConsumer()
        c.add_chain(data, parameters=parameters)
        p, cor = c.analysis.get_correlations()
        assert p[0] == "x"
        assert p[1] == "y"
        assert np.isclose(cor[0, 0], 1, atol=1e-2)
        assert np.isclose(cor[1, 1], 1, atol=1e-2)
        assert np.abs(cor[0, 1]) < 0.01
        assert cor.shape == (2, 2)
