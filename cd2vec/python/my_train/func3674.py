def test_toJSON_fast(self):
		model.factory.json_serializer = "fast"
		expect = {'@context': model.factory.context_uri, 
			'id': 'http://lod.example.org/museum/InformationObject/collection', 
			'type': 'InformationObject', 
			'_label': 'Test Object'}
		outj = model.factory.toJSON(self.collection)
		self.assertEqual(expect, outj)
		model.factory.json_serializer = "normal"
