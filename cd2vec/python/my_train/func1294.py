@staticmethod
    @pytest.mark.usefixtures('default-references', 'export-time')
    def test_cli_export_reference_014(snippy):
        """Export references with search keyword.

        Export references based on search keyword. In this case the search
        keyword matchies to two references that must be exported to file
        defined in command line.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Content.deepcopy(Reference.GITLOG),
                Content.deepcopy(Reference.REGEXP)
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--sall', 'howto', '-f', 'defined-reference.text', '--scat', 'reference'])
            assert cause == Cause.ALL_OK
            Content.assert_text(mock_file, 'defined-reference.text', content)
