def test_embeddings_with_gensim(self):
        for emb in self.embeddings_for_testing:
            embeddings = load_wv_with_gensim(emb)
            self.assertEqual(MODELS[emb]['vocab_size'], len(embeddings.vocab))
