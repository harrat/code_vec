@pytest.mark.high
    def test_producers(self):
        self.register.register(self.tasks['producer'])

        self.assertIn(self.tasks['producer'], self.register.producers.values())
