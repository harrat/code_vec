@staticmethod
    @pytest.mark.usefixtures()
    def test_cli_update_snippet_017(snippy, editor_data):
        """Update snippet with editor.

        Update existing snippet directly from command line. In this case the
        snippet contains multiple comments which are same. The update is made
        in Markdown format and editor must be able to show the content. There
        are no changes and the parser must notice that the content was not
        updated.
        """

        Content.store({
            'category': Content.SNIPPET,
            'data': [
                "find . -iregex '.*\\(py\\|robot\\)'  #  Find files.",
                "find . -iregex '.*\\(py\\|robot\\)' -print0 | wc -l --files0-from=-  #  Find files and count lines.",
                "find . -iregex '.*\\(py\\|robot\\)' -print0 | wc -l --files0-from=- | tail -n 1",
                "find . -name '*.py' -print0 | wc -l --files0-from=-  #  Find files and count lines.",
                "find . -name '*.py' -print0 | wc -l --files0-from=- | tail -n 1",
                "find . -name '*.py' -exec cat {} + | wc -l  #  Find files and count lines."],
            'brief': 'Find files and count lines',
            'description': 'Find files with or without regexp pattern and count lines.',
            'groups': ['linux'],
            'tags': ['find', 'linux', 'regexp'],
            'digest': 'dae4e22c3c3858b5616a29be11916112a16994e30bc3e4b93b069bc9a772d889'
        })
        content = {
            'data': [{
                'category': 'snippet',
                'data': ("find . -iregex '.*\\(py\\|robot\\)'  #  Find files.",
                         "find . -iregex '.*\\(py\\|robot\\)' -print0 | wc -l --files0-from=-  #  Find files and count lines.",
                         "find . -iregex '.*\\(py\\|robot\\)' -print0 | wc -l --files0-from=- | tail -n 1",
                         "find . -name '*.py' -print0 | wc -l --files0-from=-  #  Find files and count lines.",
                         "find . -name '*.py' -print0 | wc -l --files0-from=- | tail -n 1",
                         "find . -name '*.py' -exec cat {} + | wc -l  #  Find files and count lines."),
                'brief': 'Find files and count lines',
                'description': 'Find files with or without regexp pattern and count lines.',
                'name': '',
                'groups': ('linux', ),
                'tags': ('find', 'linux', 'regexp'),
                'links': (),
                'source': '',
                'versions': (),
                'languages': (),
                'filename': '',
                'created': '2018-03-02T02:02:02.000001+00:00',
                'updated': '2018-03-02T02:02:02.000001+00:00',
                'uuid': 'a1cd5827-b6ef-4067-b5ac-3ceac07dde9f',
                'digest': 'dae4e22c3c3858b5616a29be11916112a16994e30bc3e4b93b069bc9a772d889'
            }]
        }
        template = (
            '# Find files and count lines @linux',
            '',
            '> Find files with or without regexp pattern and count lines.',
            '',
            '> ',
            '',
            '- Find files.',
            '',
            '    `$ find . -iregex \'.*\\(py\\|robot\\)\'`',
            '',
            '- Find files and count lines.',
            '',
            '    `$ find . -iregex \'.*\\(py\\|robot\\)\' -print0 | wc -l --files0-from=-`',
            '',
            '- <not documented>',
            '',
            '    `$ find . -iregex \'.*\\(py\\|robot\\)\' -print0 | wc -l --files0-from=- | tail -n 1`  ',
            '',
            '- Find files and count lines.',
            '',
            '    `$ find . -name \'*.py\' -print0 | wc -l --files0-from=-`',
            '',
            '- <not documented>',
            '',
            '    `$ find . -name \'*.py\' -print0 | wc -l --files0-from=- | tail -n 1`  ',
            '',
            '- Find files and count lines.',
            '',
            '    `$ find . -name \'*.py\' -exec cat {} + | wc -l`',
            '',
            '## Meta',
            '',
            '> category  : snippet  ',
            'created   : 2018-03-02T02:02:02.000001+00:00  ',
            'digest    : dae4e22c3c3858b5616a29be11916112a16994e30bc3e4b93b069bc9a772d889  ',
            'filename  :  ',
            'languages :  ',
            'name      :  ',
            'source    :  ',
            'tags      : find,linux,regexp  ',
            'updated   : 2018-03-02T02:02:02.000001+00:00  ',
            'uuid      : a1cd5827-b6ef-4067-b5ac-3ceac07dde9f  ',
            'versions  :  ',
            ''
        )
        editor_data.return_value = '\n'.join(template)
        cause = snippy.run(['snippy', 'update', '-d', 'dae4e22c3c3858b5'])
        assert cause == Cause.ALL_OK
        editor_data.assert_called_with('\n'.join(template))
        Content.assert_storage(content)
