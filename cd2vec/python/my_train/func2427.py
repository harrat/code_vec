@pytest.mark.parametrize('limoff,out,pos,exception', [
		([1], 'LIMIT 1',-1,None),
		((10,5), 'LIMIT 10 OFFSET 5',-1,None),
		([1,2,3], None, -1, LimitParseError)
	])
	def testStr(self, limoff, out, pos, exception):
		if exception:
			with pytest.raises(exception):
				Limit(limoff)
		else:
			lim = Limit(limoff)
			assert str(lim) == out
			assert lim.pos == pos
