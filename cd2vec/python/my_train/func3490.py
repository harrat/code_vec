@staticmethod
    def test_cli_search_snippet_048(snippy, capsys):
        """Search snippets with ``--sall`` option.

        Search snippets from all resource attributes. A ``name`` attribute
        of a single snippet produces a match.
        """

        Content.store({
            'category': Const.SNIPPET,
            'data': [
                'tar cvfz mytar.tar.gz --exclude="mytar.tar.gz" ./'],
            'brief': 'Manipulate compressed tar files',
            'name': 'docker',
            'groups': ['linux'],
            'tags': ['howto', 'linux', 'tar', 'untar']
        })
        output = (
            '1. Manipulate compressed tar files @linux [d1c975e194b7c26a]',
            '',
            '   $ tar cvfz mytar.tar.gz --exclude="mytar.tar.gz" ./',
            '',
            '   # howto,linux,tar,untar',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--all', 'docker', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
