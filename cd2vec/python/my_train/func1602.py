def test_parse_raises_not_found(self):
        html_dump = _lookup_online("vvvvxxxx")
        with pytest.raises(WordNotFound) as e:
            _parse_html(html_dump)
            assert str(e.value) == "Nie znaleziono t?umaczenia wpisanej frazy"
