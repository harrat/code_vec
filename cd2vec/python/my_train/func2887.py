def test_show_alias(cli, projects_db):
    output = cli('show', ['alias_1'])
    assert output == ("Your search string alias_1 is an alias to "
                      "my project, my activity (123/456) on the "
                      "test backend.\n")
