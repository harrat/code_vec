def test_get_wrapped_key_missing_key_information(self):
        """
        Test that the right error is thrown when key wrapping is requested
        with no settings in a Get request.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        unwrapped_key = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            128,
            (
                b'\x00\x11\x22\x33\x44\x55\x66\x77'
                b'\x88\x99\xAA\xBB\xCC\xDD\xEE\xFF'
            ),
            [enums.CryptographicUsageMask.ENCRYPT]
        )

        e._data_session.add(unwrapped_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        unwrapped_key_uuid = str(unwrapped_key.unique_identifier)
        payload = payloads.GetRequestPayload(
            unique_identifier=unwrapped_key_uuid,
            key_wrapping_specification=objects.KeyWrappingSpecification(
                wrapping_method=enums.WrappingMethod.ENCRYPT,
                encoding_option=enums.EncodingOption.NO_ENCODING
            )
        )

        args = (payload, )
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            "Either the encryption key information or the MAC/signature key "
            "information must be specified for key wrapping to be performed.",
            e._process_get,
            *args
        )
