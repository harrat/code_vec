def test_fileoutput_with_header(self):
        path = str(self.root.make_file())
        with textoutput(
                path + '{index}.txt', file_output_type=RollingFileOutput,
                header="number\n", lines_per_file=3) as out:
            for i in range(6):
                out.write(str(i))
        with open(path + '0.txt', 'rt') as infile:
            assert 'number\n0\n1\n2\n' == infile.read()
        with open(path + '1.txt', 'rt') as infile:
            assert 'number\n3\n4\n5\n' == infile.read()
