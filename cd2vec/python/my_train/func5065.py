def test_prepare_with_smt(fake_job, slurm_only):
    fake_job.use_smt = True
    prepare_job(fake_job)
    with open(fake_job.script_filename) as fp:
        assert re.search(r'--hint=multithread', fp.read()) is not None
