def test_load_json_fobj():
    """Test loading JSON file, with file object file specifier.
    Loads files from test_save_fm_str.
    """

    file_name = 'test_fooof_all'

    with open(os.path.join(TEST_DATA_PATH, file_name + '.json'), 'r') as f_obj:
        data = load_json(f_obj, '')

    assert data

def test_load_jsonlines():
    """Test loading JSONlines file.
    Loads files from test_save_fg.
    """
