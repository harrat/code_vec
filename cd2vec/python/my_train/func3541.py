@pytest.mark.vcr()
def test_agents_list_sort_direction_typeerror(api):
    with pytest.raises(TypeError):
        api.agents.list(sort=(('uuid', 1),))
