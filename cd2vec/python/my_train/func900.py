def test_heatcapacity_material(self):
        self.idf.initreadtxt(single_layer)
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        expected = m.Thickness * m.Specific_Heat * m.Density * 0.001
        assert m.heatcapacity == expected
        assert m.heatcapacity == 120
