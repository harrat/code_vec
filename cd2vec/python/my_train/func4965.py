def test_put_resource(self):
        """Test HTTP PUTing a resource that already exists (should be
        updated)."""
        response = self.app.put('/tracks/1',
                content_type='application/json',
                data=json.dumps(
                    {'Name': 'Some New Album',
                      'AlbumId': 1,
                      'GenreId': 1,
                      'MediaTypeId': 1,
                      'Milliseconds': 343719,
                      'TrackId': 1,
                      'UnitPrice': 0.99,}))
        assert response.status_code == 204
        response = self.get_response('/tracks/1', 200)
        assert json.loads(
                response.get_data(as_text=True))[u'Name'] == u'Some New Album'
        assert json.loads(
                response.get_data(as_text=True))[u'Composer'] is None
