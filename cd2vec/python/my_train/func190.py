def test_serverx(tcp_client):
    """
    Test serverx object for reading message form client.

    :param tcp_client: function for send message to server
    """
    host, port, client = tcp_client

    def handler(message, reader, writer):
        logger.info(f"got message: `{message}`")
        logger.debug(f"got reader: `{reader}` and `{writer}`")
        writer.write(b"ok")

    run_test_serverx(host, port, client, handler)

