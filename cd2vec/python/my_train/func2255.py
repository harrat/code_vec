@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_delete_reference_004(snippy):
        """Delete reference with uuid.

        Try to delete reference with short content UUID. Since the UUID is
        unique, this must not delete the content.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        Content.assert_storage_size(2)
        cause = snippy.run(['snippy', 'delete', '-u', '16cd5827'])
        assert cause == 'NOK: cannot find content with content uuid: 16cd5827'
        Content.assert_storage(content)
