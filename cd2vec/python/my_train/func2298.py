def test_default_plots_None_nocallback(self):
        agent = agents.PpoAgent("CartPole-v0")
        p = plot.Loss()
        c = agent._add_plot_callbacks([], None, [p])
        assert p in c
