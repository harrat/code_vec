def test_TMVN_sampling_truncated_narrow_limits():
    """
    Test if the sampling method returns appropriate samples for a truncated
    univariate and multivariate normal distribution when the truncation limits
    are narrow.

    """

    # here we focus on testing if the returned samples are between the
    # pre-defined limits in all dimensions

    # univariate case
    ref_mean = 0.5
    ref_std = 0.25
    ref_var = ref_std ** 2.
    lower = 0.1
    upper = 0.6

    samples = tmvn_rvs(ref_mean, ref_var, lower=lower, upper=upper,
                       size=1000)

    sample_min = np.min(samples)
    sample_max = np.max(samples)

    assert sample_min > lower
    assert sample_max < upper

    assert sample_min == pytest.approx(lower, abs=0.01)
    assert sample_max == pytest.approx(upper, abs=0.01)

    # multi-dimensional case
    dims = 5
    ref_mean = np.arange(dims, dtype=np.float64)
    ref_std = np.ones(1) * 0.25
    ref_rho = np.ones((dims, dims)) * 0.3
    ref_COV = np.outer(ref_std, ref_std) * ref_rho
    np.fill_diagonal(ref_COV, ref_std ** 2.)

    lower = ref_mean - ref_std * 2.5
    upper = ref_mean + ref_std * 1.5

    samples = tmvn_rvs(ref_mean, ref_COV, lower=lower, upper=upper,
                       size=1000)

    sample_min = np.amin(samples, axis=0)
    sample_max = np.amax(samples, axis=0)

    assert np.all(sample_min > lower)
    assert np.all(sample_max < upper)

    assert_allclose(sample_min, lower, atol=0.1)
    assert_allclose(sample_max, upper, atol=0.1)

# ------------------------------------------------------------------------------
# mvn_orthotope_density
# ------------------------------------------------------------------------------
def test_MVN_CDF_univariate():
    """
    Test if the MVN CDF function provides accurate results for the special
    univariate case.
