def test_http_call_with_session():
    try:
        url = 'https://httpbin.org/cookies/set/sessioncookie/123456789'
        parsed_url = urlparse(url)
        query = parsed_url.query
        host = parsed_url.netloc
