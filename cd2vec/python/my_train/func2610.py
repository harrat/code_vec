def test_convert_image_to_pdf(api, tmpdir):
    copy_image_file(str(tmpdir))
    event = json.dumps({"uploadId": str(tmpdir),
                        "contentType": "image/png"})
    r = api.requests.post("/convert/pdf", event)
    json_response = json.loads(r.text)
    assert "upload_id" in json_response
    assert "release_date" in json_response
    assert json_response["upload_id"] == str(tmpdir)
    r = api.requests.post("/convert/pdf", event)
    json_response = json.loads(r.text)
    assert "upload_id" in json_response
    assert "release_date" in json_response
    assert json_response["upload_id"] == str(tmpdir)

