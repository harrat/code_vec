def test_strings_are_provided():

    def _obj(mystring):
        assert isinstance(mystring, str)
        return int(mystring[0])

    wrapper = gpyopt.GPyOptObjectiveWrapper(_obj)
    wrapper.set_variable_parameter('mystring', 'categorical', '1234')
    params = gpyopt.BayesianOptimizationParams(
        initial_design_numdata=2,
        max_samples=10,
    )
    best = gpyopt.bayesopt(wrapper, params)
    assert best['mystring'] == '1'

