def test_posivite(api, api_assert):
    r = api.default_exponential()

    data = r['samples']

    assert all([d > 0 for d in data])

