@pytest.mark.parametrize("existing, expected", [
    # No existing commit, we expect all commits in commit order,
    # master branch first
    ([], ["Revision 6", "Revision 4", "Merge stable", "Revision 3",
          "Revision 1", "Revision 5", "Merge master", "Revision 2"]),

    # New commits on each branch
    (["Revision 4", "Merge master"], ["Revision 6", "Revision 5"]),

    # No new commits
    (["Revision 6", "Revision 5"], []),

    # Missing all commits on one branch (case of new branch added in config)
    (["Revision 6"], ["Revision 5", "Merge master", "Revision 2", "Revision 1"]),
], ids=["all", "new", "no-new", "new-branch-added-in-config"])
def test_get_new_branch_commits(two_branch_repo_case, existing, expected):
    dvcs, master, r, conf = two_branch_repo_case

    existing_commits = set()
    for branch in conf.branches:
        for commit in r.get_branch_commits(branch):
            message = dvcs.get_commit_message(commit)
            if message in existing:
                existing_commits.add(commit)

    assert len(existing_commits) == len(existing)

    new_commits = r.get_new_branch_commits(conf.branches, existing_commits)
    commits = [dvcs.get_commit_message(commit) for commit in new_commits]
    assert commits == expected

