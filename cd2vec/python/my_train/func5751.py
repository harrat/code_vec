def test_results(tmpdir):
    tmpdir = six.text_type(tmpdir)

    timestamp1 = datetime.datetime.utcnow()
    duration = 1.5

    resultsdir = join(tmpdir, "results")
    for i in six.moves.xrange(10):
        r = results.Results(
            {'machine': 'foo',
             'arch': 'x86_64'},
            {},
            hex(i),
            i * 1000000,
            '2.7',
            'some-environment-name',
            {})

        x1 = float(i * 0.001)
        x2 = float(i * 0.001)
        x3 = float((i + 1) ** -1)

        values = {
            'suite1.benchmark1': {'result': [x1], 'number': [1],
                                  'samples': [[x1,x1]], 'params': [['a']],
                                  'version': "1", 'profile': b'\x00\xff'},
            'suite1.benchmark2': {'result': [x2], 'number': [1],
                                  'samples': [[x2,x2,x2]], 'params': [],
                                  'version': "1", 'profile': b'\x00\xff'},
            'suite2.benchmark1': {'result': [x3], 'number': [None],
                                  'samples': [None], 'params': [['c']],
                                  'version': None, 'profile': b'\x00\xff'},
            'suite3.benchmark1': {'result': [x1, x2], 'number': [1, 1],
                                  'samples': [[x1,x1], [x2,x2,x3]], 'params': [['c', 'd']],
                                  'version': None, 'profile': b'\x00\xff'}
        }

        for key, val in values.items():
            v = runner.BenchmarkResult(result=val['result'],
                                       samples=val['samples'],
                                       number=val['number'],
                                       profile=val['profile'],
                                       errcode=0,
                                       stderr='')
            benchmark = {'name': key, 'version': val['version'], 'params': val['params']}
            r.add_result(benchmark, v, record_samples=True,
                         started_at=timestamp1, duration=duration)

        # Save / add_existing_results roundtrip
        r.save(resultsdir)

        r2 = results.Results.load(join(resultsdir, r._filename))
        assert r2.date == r.date
        assert r2.commit_hash == r.commit_hash
        assert r2._filename == r._filename

        r3 = results.Results(r.params,
                             r._requirements,
                             r.commit_hash,
                             r.date,
                             r._python,
                             r.env_name,
                             {})
        r3.load_data(resultsdir)

        for rr in [r2, r3]:
            assert rr._results == r._results
            assert rr._stats == _truncate_floats(r._stats)
            assert rr._samples == r._samples
            assert rr._profiles == r._profiles
            assert rr.started_at == r._started_at
            assert rr.duration == _truncate_floats(r._duration)
            assert rr.benchmark_version == r._benchmark_version

        # Check the get_* methods
        assert sorted(r2.get_all_result_keys()) == sorted(values.keys())
        for bench in r2.get_all_result_keys():
            # Get with same parameters as stored
            params = r2.get_result_params(bench)
            assert params == values[bench]['params']
            assert r2.get_result_value(bench, params) == values[bench]['result']
            assert r2.get_result_samples(bench, params) == values[bench]['samples']
            stats = r2.get_result_stats(bench, params)
            if values[bench]['number'][0] is None:
                assert stats == [None]
            else:
                assert stats[0]['number'] == values[bench]['number'][0]

            # Get with different parameters than stored (should return n/a)
            bad_params = [['foo', 'bar']]
            assert r2.get_result_value(bench, bad_params) == [None, None]
            assert r2.get_result_stats(bench, bad_params) == [None, None]
            assert r2.get_result_samples(bench, bad_params) == [None, None]

            # Get profile
            assert r2.get_profile(bench) == b'\x00\xff'

        # Check get_result_keys
        mock_benchmarks = {
            'suite1.benchmark1': {'version': '1'},
            'suite1.benchmark2': {'version': '2'},
            'suite2.benchmark1': {'version': '2'},
        }
        assert sorted(r2.get_result_keys(mock_benchmarks)) == ['suite1.benchmark1',
                                                               'suite2.benchmark1']
