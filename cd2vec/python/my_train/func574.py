def test_get_movie_credits(empty_person: Person):
    credits = empty_person.get_movie_credits()
    assert credits == empty_person.data["movie_credits"]

