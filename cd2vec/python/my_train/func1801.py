def test_invalidate_connections(region_cache):
    region_cache.invalidate_connections()
    assert region_cache._w_conn is None
    assert region_cache._r_conn is None

