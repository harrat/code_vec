def test_host_applytemplate(self, host_load_data):
        host = host_load_data
        data = dict()
        data['action'] = 'applytpl'
        data['object'] = 'HOST'
        data['values'] = "mail-uranus-frontend"

        with patch('requests.post') as patched_post:
            host.applytemplate()
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
