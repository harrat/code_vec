def test_split_path(self):
        parent = self.root.make_directory()
        assert split_path(parent / 'foo', keep_seps=False) == (parent, 'foo')
        assert split_path(parent / 'foo.tar.gz', keep_seps=False) == \
            (parent, 'foo', 'tar', 'gz')
        assert split_path(parent / 'foo.tar.gz', keep_seps=True) == \
            (parent, 'foo', '.tar', '.gz')
