def test_load_restore(self):
        snapshot, _ = rt.loadenv(self.environ)
        os.environ['_var0'] == 'val1'
        os.environ['_var1'] == 'val1'
        os.environ['_var2'] == 'val1'
        os.environ['_var3'] == 'val1'
        if fixtures.has_sane_modules_system():
            self.assertModulesLoaded(self.environ.modules)

        assert rt.is_env_loaded(self.environ)
        snapshot.restore()
        self.environ_save == env.snapshot()
        os.environ['_var0'], 'val0'
        if fixtures.has_sane_modules_system():
            assert not self.modules_system.is_module_loaded('testmod_foo')

        assert not rt.is_env_loaded(self.environ)
