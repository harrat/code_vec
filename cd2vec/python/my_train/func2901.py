def test_get_dir_route(fx_session, fx_stage):
    with fx_stage:
        dir = fx_stage.dir_docs
        assert isinstance(dir, Directory)
        assert len(dir) == 2
        assert frozenset(dir) == frozenset(['abc', 'def'])
        with raises(KeyError):
            dir['not-exist']
        doc = dir['abc']
        assert isinstance(doc, TestDoc)
        assert doc.__revision__.session is fx_session
        assert dir['abc'].__revision__ == doc.__revision__
