def test_flex_alloc_valid_multiple_constraints(make_flexible_job):
    job = make_flexible_job('all')
    job.options = ['-C f1&f3']
    prepare_job(job)
    assert job.num_tasks == 4

