def test_condition(query):
    """
    Test the parsing of logical conditions.
    """

    with pytest.raises(TypeError):
        # test for error if condition is not a string
        query.condition = 2.3

    # test that we only return pulsars with F0 > 100 Hz
    query.condition = 'F0 > 100'

    psrs = query.table
    f0s = psrs['F0']

    assert not np.any(f0s < 100.)

    # test that we only return pulsars with F0 > 100 Hz in binary systems
    query.condition = 'F0 > 100 && type(binary)'

    psrs = query.table
    f0s = psrs['F0']
    binary = psrs['BINARY']

    if type(binary) == MaskedColumn:
        assert not np.any(f0s < 100.) and not np.any(binary.mask)
    else:
        assert not np.any(f0s < 100.)

    # test 'OR'
    query.condition = 'F0 > 100 || type(binary)'

    psrs = query.table
    f0s = psrs['F0']
    binary = psrs['BINARY']

    if type(binary) == MaskedColumn:
        assert np.all(~binary[f0s < 100.].mask)

    # test 'NOT'
    query.condition = 'F0 > 100 and not type(binary)'

    psrs = query.table
    f0s = psrs['F0']
    binary = psrs['BINARY']

    if type(binary) == MaskedColumn:
        assert not np.any(f0s < 100) and np.all(binary.mask)
    else:
        assert not np.any(f0s < 100)

    # test type (not binary)
    query.condition = 'type(HE)'

    psrs = query.table
    types = psrs['TYPE']

    assert np.all(['HE' in ttype for ttype in types])

    # test associations
    query.condition = 'assoc(GC)'

    psrs = query.table
    assocs = psrs['ASSOC']

    assert np.all(['GC' in assoc for assoc in assocs])

    # test binary companion
    query.condition = 'bincomp(MS)'

    psrs = query.table
    bincomps = psrs['BINCOMP']

    assert np.all(['MS' in bincomp for bincomp in bincomps])

    # test exists
    query.condition = None
    allpsrs = query.table
    query.condition = 'exist(PMRA)'

    psrs = query.table
    pmras = psrs['PMRA']

    if type(allpsrs['PMRA']) == MaskedColumn:
        assert len(pmras) == np.sum(~allpsrs['PMRA'].mask)

    # test survey
    query.condition = "survey(gb350)"
    psrs = query.table
    surveys = psrs["SURVEY"]

    assert np.all(["gb350" in survey for survey in surveys])

    # test discovery
    query.condition = "discovery(htru_eff)"
    psrs = query.table
    discoveries = psrs["SURVEY"]

    assert np.all(["htru_eff" == disc.split(",")[0] for disc in discoveries])

    # reset condition
    query.condition = None

