def test_set_header_overrides(self):
        self.client.set_header_overrides({
            'User-Agent': 'Test_User_Agent_String'
        })
        self._make_request('https://www.github.com')

        last_request = self.client.get_last_request()

        self.assertEqual('Test_User_Agent_String', last_request['headers']['User-Agent'])
