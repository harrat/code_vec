def test_registration_context():

    def foo(a, b):
        a / b

    with on_test.registered(foo):
        with pytest.raises(ZeroDivisionError):
            on_test(a=5, b=0, c='c')

    on_test(a=5, b=0, c='c')

