def test_invocation_support_error_set(handler_with_user_error, mock_context, mock_event):
    ConfigProvider.set(config_names.THUNDRA_APPLICATION_STAGE, 'dev')
    thundra, handler = handler_with_user_error

    invocation_plugin = None
    for plugin in thundra.plugins:
        if isinstance(plugin, InvocationPlugin):
            invocation_plugin = plugin

    handler(mock_event, mock_context)

    assert invocation_plugin.invocation_data['erroneous'] is True
    assert invocation_plugin.invocation_data['errorType'] == 'Exception'
    assert invocation_plugin.invocation_data['errorMessage'] == 'test'
