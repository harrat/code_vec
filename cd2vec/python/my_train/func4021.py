@pytest.mark.high
    def test_to_dict(self):
        expected_result = {
            'consumers': {
                'consumer_name': {
                    'description': 'docstring',
                    'module': 'module',
                    'name': 'qualname',
                },
            },
            'producers': {
                'producer_name': {
                    'description': 'description',
                    'module': 'module',
                    'name': 'qualname',
                }
            }
        }

        self.register.register(self.tasks['consumer'])
        self.register.register(self.tasks['producer'])

        result = self.register.to_dict()

        self.assertDictEqual(expected_result, result)
