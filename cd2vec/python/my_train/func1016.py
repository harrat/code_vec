def test_augcat_kdtree_databases():
    '''This tests generation of a checkplot-info augmented LC catalog plus a
    KD-Tree for spatial matching for the LC collection.

    - makes pfpickles
    - removes all but the first five LCs in the test collection
    - makes an LC catalog for these
    - makes checkplot pickles for these plus the pfpickles
    - adds checkplot info to LC catalog -> augmented LC catalog
    - generates KD-Tree
    - generates objectinfo SQLite DB.
    - generates the LCC-Server databases and adds collection to them

    NOTE: it appears this crashes on MacOS 10.13+ when run as part of the full
    pytest suite. Doesn't crash when:
    `pytest test_cli.py::test_augcat_kdtree_databases` is called in isolation.
    Works fine both ways on Linux. Another WTF courtesy of Apple.

    Possibly related:

    - http://sealiesoftware.com/blog/archive/2017/6/5/Objective-C_and_fork_in_macOS_1013.html

    '''

    # remove the test-basedir
    shutil.rmtree('./test-basedir', ignore_errors=True)

    # make a basedir in the current directory
    cli.prepare_basedir('./test-basedir',
                        interactive=False)

    # make a new collection and its directories
    cli.new_collection_directories(
        './test-basedir',
        'test-collection'
    )

    basedir = './test-basedir'
    collection = 'test-collection'
    collection_subdir = os.path.join(basedir, collection)
    lightcurves_subdir = os.path.join(basedir, collection, 'lightcurves')

    with tempfile.TemporaryDirectory() as tempdir:

        # download light curves into a temporary directory
        get_lightcurves(tempdir)

        # copy over the lcformat-description.json, object-db.csv, and
        # lcreadermodule.py to collection_subdir
        dl_lcformat_json = os.path.join(tempdir,
                                        'lcc-server-demo',
                                        'lcformat-description.json')
        dl_readermodule = os.path.join(tempdir,
                                       'lcc-server-demo',
                                       'lcreadermodule.py')
        dl_objectdb = os.path.join(tempdir,
                                   'lcc-server-demo',
                                   'object-db.csv')

        shutil.copy(dl_lcformat_json, collection_subdir)
        shutil.copy(dl_readermodule, collection_subdir)
        shutil.copy(dl_objectdb, collection_subdir)

        # copy over the light curves to the lightcurves_subdir
        os.rmdir(lightcurves_subdir)
        dl_lcdir = os.path.join(tempdir, 'lcc-server-demo', 'lightcurves')
        shutil.copytree(dl_lcdir, lightcurves_subdir)

    # check if we have an lcformat-description.json, object-db.csv, and
    # lcreadermodule.py
    lcformat_json = os.path.join(collection_subdir,'lcformat-description.json')
    objectdb_csv = os.path.join(collection_subdir,'object-db.csv')
    lcreadermodule = os.path.join(collection_subdir,'lcreadermodule.py')

    assert os.path.exists(lcformat_json)
    assert os.path.exists(objectdb_csv)
    assert os.path.exists(lcreadermodule)

    # check if we have all 100 original LCs
    lcfiles = sorted(glob.glob(os.path.join(lightcurves_subdir,'*.csv')))
    assert len(lcfiles) == 100
    assert os.path.basename(lcfiles[0]) == 'HAT-215-0001809-lc.csv'
    assert os.path.basename(lcfiles[-1]) == 'HAT-265-0037533-lc.csv'

    # register the LC format
    from astrobase.lcproc import register_lcformat
    from lccserver.backend.abcat import get_lcformat_description
    lcform = get_lcformat_description(
        os.path.join(collection_subdir,'lcformat-description.json')
    )
    register_lcformat(
        lcform['parsed_formatinfo']['formatkey'],
        lcform['parsed_formatinfo']['fileglob'],
        ['rjd'],
        ['aep_000'],
        ['aie_000'],
        lcform['parsed_formatinfo']['readermodule'],
        lcform['parsed_formatinfo']['readerfunc'],
        readerfunc_kwargs=lcform['parsed_formatinfo']['readerfunc_kwargs'],
        normfunc_module=lcform['parsed_formatinfo']['normmodule'],
        normfunc=lcform['parsed_formatinfo']['normfunc'],
        normfunc_kwargs=lcform['parsed_formatinfo']['normfunc_kwargs'],
        magsarefluxes=lcform['magsarefluxes'],
        overwrite_existing=True
    )

    # generate period-finding pickles for the first 5 LCs
    from astrobase.lcproc.periodsearch import parallel_pf

    parallel_pf(
        lcfiles[:5],
        os.path.join(collection_subdir,
                     'periodfinding'),
        lcformat=lcform['formatkey'],
        pfmethods=('gls',),
        pfkwargs=({},),
        getblssnr=False
    )

    # check if all period-finding pickles exist
    pfpickle_subdir = os.path.join(collection_subdir,
                                   'periodfinding')

    pfpickles = sorted(glob.glob(os.path.join(pfpickle_subdir,
                                              'periodfinding*.pkl')))
    assert len(pfpickles) == 5
    assert (
        os.path.basename(pfpickles[0]) == 'periodfinding-HAT-215-0001809.pkl'
    )
    assert (
        os.path.basename(pfpickles[-1]) == 'periodfinding-HAT-215-0010422.pkl'
    )

    # remove the rest of the LCs in prep for making LC catalog
    for lc in lcfiles[5:]:
        os.remove(lc)

    # make the light curve catalog
    from astrobase.lcproc.catalogs import make_lclist
    lc_catalog = make_lclist(
        lightcurves_subdir,
        os.path.join(collection_subdir,'lclist.pkl'),
        lcformat=lcform['formatkey']
    )
    assert os.path.exists(lc_catalog)

    # make checkplots now
    from astrobase.lcproc.checkplotgen import parallel_cp

    import sys
    if sys.platform == 'darwin':
        import requests
        try:
            requests.get('http://captive.apple.com/hotspot-detect.html',
                         timeout=5.0)
        except Exception as e:
            pass

    parallel_cp(
        pfpickles,
        os.path.join(collection_subdir,'checkplots'),
        os.path.join(collection_subdir, 'lightcurves'),
        fast_mode=10.0,
        lcfnamelist=lcfiles[:5],
        lclistpkl=lc_catalog,
        minobservations=49,
        lcformat=lcform['formatkey']
    )

    # check if the checkplots exist
    checkplot_subdir = os.path.join(collection_subdir,'checkplots')

    checkplotpkls = sorted(glob.glob(os.path.join(checkplot_subdir,
                                                  'checkplot-*.pkl')))
    assert len(checkplotpkls) == 5
    assert (
        os.path.basename(checkplotpkls[0]) ==
        'checkplot-HAT-215-0001809-aep_000.pkl'
    )
    assert (
        os.path.basename(checkplotpkls[-1]) ==
        'checkplot-HAT-215-0010422-aep_000.pkl'
    )

    # generate the augmented LC catalog
    augcat = cli.generate_augmented_lclist_catalog(
        basedir,
        collection,
        lc_catalog,
        'aep_000',
    )
    assert os.path.exists(augcat)

    # generate the KD-Tree
    kdt = cli.generate_catalog_kdtree(
        basedir,
        collection
    )
    assert os.path.exists(kdt)

    # generate the objectinfo SQLite file for this collection
    collection_db = cli.generate_catalog_database(
        basedir,
        collection,
        overwrite_existing=True
    )
    assert os.path.exists(collection_db)

    # generate the LCC index DB
    lcc_index_db = cli.new_lcc_index_db(basedir)
    assert os.path.exists(lcc_index_db)

    # generate the LCC datasets DB
    lcc_datasets_db = cli.new_lcc_datasets_db(basedir)
    assert os.path.exists(lcc_datasets_db)

    # add this collection to the LCC index DB
    cli.add_collection_to_lcc_index(basedir,
                                    collection)

    from lccserver.backend import dbsearch
    collection_info = dbsearch.sqlite_list_collections(
        basedir
    )
    assert collection in collection_info['info']['collection_id']

    # remove the test-basedir
    shutil.rmtree('./test-basedir', ignore_errors=True)

