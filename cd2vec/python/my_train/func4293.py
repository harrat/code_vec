def test_network_traffic_required_fields(self):
        net_traffic = copy.deepcopy(self.valid_net_traffic)
        del net_traffic['objects']['1']['src_ref']
        self.assertFalseWithOptions(net_traffic)

        net_traffic = copy.deepcopy(self.valid_net_traffic)
        del net_traffic['objects']['1']['protocols']
        self.assertFalseWithOptions(net_traffic)
