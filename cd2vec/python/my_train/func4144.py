@pytest.mark.vcr()
def test_workbench_vulns_exploitable_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vulns(exploitable='nope')
