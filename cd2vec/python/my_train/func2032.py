def test_concurrency_none(async_runner, make_cases, make_async_exec_ctx):
    num_checks = 3
    ctx = make_async_exec_ctx(1)
    next(ctx)

    runner, monitor = async_runner
    runner.runall(make_cases([SleepCheck(.5) for i in range(num_checks)]))

    # Ensure that all tests were run and without failures.
    assert num_checks == runner.stats.num_cases()
    assert_runall(runner)
    assert 0 == len(runner.stats.failures())

    # Ensure that a single task was running all the time
    assert 1 == max(monitor.num_tasks)

    # Read the timestamps sorted to permit simple concurrency tests.
    begin_stamps, end_stamps = _read_timestamps(monitor.tasks)

    # Ensure that the jobs were run after the previous job had finished
    # (e.g. begin[1] > end[0]).
    begin_after_end = (b > e
                       for b, e in zip(begin_stamps[1:], end_stamps[:-1]))
    assert all(begin_after_end)

