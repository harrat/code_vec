@pytest.mark.core
def testConfigFlags(nwTemp,nwRef):
    tmpConf = path.join(nwTemp,"novelwriter.conf")
    refConf = path.join(nwRef, "novelwriter.conf")
    assert not theConf.setShowRefPanel(False)
    assert theConf.setShowRefPanel(True)
    assert theConf.confChanged
    assert theConf.saveConfig()
    assert cmpFiles(tmpConf, refConf, [2])
    assert not theConf.confChanged

@pytest.mark.core
def testConfigErrors(nwTemp):
    nonPath = path.join("somewhere","over","the","rainbow")
    assert theConf.initConfig(nonPath, nonPath)
    assert theConf.hasError
    assert not theConf.loadConfig()
    assert not theConf.saveConfig()
    assert not theConf.loadRecentCache()
    assert len(theConf.getErrData()) > 0
