def test_timeout():
    with pytest.raises(utils.ConnectionTimeoutError):
        utils.wait_for_server('localhost', utils.get_available_port(), 2)
