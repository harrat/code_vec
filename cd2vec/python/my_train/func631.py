def test_configure():
    provider = Provider()
    provider.bind(int, str)
    configure(provider=provider)
    assert isinstance(Container.get(), Provider)
    assert Container.get().bindings[int] == str
    with raises(InjectException):
        configure(provider=provider)
