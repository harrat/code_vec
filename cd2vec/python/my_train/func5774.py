@mock.patch('os.path.exists')
    def test_cache_exists(self, f: Callable):
        f.return_value = True
        url = 'https://example.com'
        path = download.cached_download(url)
        self.assertEqual(path, os.path.join(self.temp_dir, '_dl_cache', hashlib.md5(url.encode('utf-8')).hexdigest()))
