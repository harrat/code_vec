def test_alias_no_results(cli, alias_config):
    output = cli('alias', ['list', '12'])
    lines = output.splitlines()

    assert len(lines) == 0

