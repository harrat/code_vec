def test_run(monkeypatch, proc):
    def mock_run_once(*args):
        raise KeyboardInterrupt()

    monkeypatch.setattr(ContinuousSSH, "_run_once", mock_run_once)

    proc.run()
    assert list(get_transitions(proc)) == ["disconnected"]

