def test_config(capsys):
    amplimap.run.main(['--print-config'])
    captured = capsys.readouterr()
    assert 'Reading additional configuration file: {}'.format(os.path.join(packagedir, "sample_data", "config_default.yaml")) in captured.err

def test_config_env(capsys):
    extra_config_path = os.path.join(packagedir, "sample_data", "extra_config.yaml")
    os.environ['AMPLIMAP_CONFIG'] = extra_config_path
    amplimap.run.main(['--print-config'])
    captured = capsys.readouterr()
    os.environ['AMPLIMAP_CONFIG'] = test_config_path #reset, so we don't affect later tests
