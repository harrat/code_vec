def test_delete(tmpdir):
	import copy
	header = ljson.base.generic.Header(header_descriptor)
	table = ljson.base.mem.Table(header, copy.copy(data))
	import os

	filename = os.path.join(str(tmpdir), "table.ljson")

	fio = open(filename, "w+")
	table.save(fio)
	fio.seek(0)
	table = ljson.base.disk.Table.from_file(fio)

	data_ = copy.copy(data)

	del(data_[0])

	del(table[{"name": "peter"}])

	assert list(table) == data_


	table.additem(item_meg)
	del(table[{"name": "meg"}])
	assert list(table) == data_

