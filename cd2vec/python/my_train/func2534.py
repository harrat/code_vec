def test_production_mode(self):

		# model.factory.production_mode()
		# Can't unset the cached hierarchy
		# and it causes the test for the hierarchy to fail
		model.factory.validate_profile = False
		model.factory.validate_properties = False
		model.factory.validate_range = False
		model.factory.validate_multiplicity = False

		p = model.Person()
		p.identified_by = model.Name(value="abc")
		p.part = model.HumanMadeObject()
		js = model.factory.toJSON(p)

		model.factory.production_mode(state=False)
