def test_date_present_entry_present(cli, entries_file):
    entries = """20/01/2014
alias_1 2 foobar
"""
    expected = """20/01/2014
alias_1 2 foobar
alias_1 00:00-? ?
"""

    entries_file.write(entries)
    with freeze_time('2014-01-20'):
        cli('start', ['alias_1'])

    assert entries_file.read() == expected

