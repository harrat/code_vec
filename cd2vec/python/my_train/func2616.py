def test_async():
    from threading import get_ident

    main = get_ident()

    @on_async_test.register(asynchronous=True)
    def a1():
        assert get_ident() != main

    @on_async_test.register()  # follows the current setting
    def a2():
        assert get_ident() != main

    with pytest.raises(AssertionError):
        @on_async_test.register(asynchronous=False)
        def xx():
            assert get_ident() == main

    on_async_test()

