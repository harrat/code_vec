@mock.patch('requests.get', side_effect=mock_pyquery)
    @mock.patch('requests.head', side_effect=mock_request)
    def test_invalid_default_year_reverts_to_previous_year(self,
                                                           *args,
                                                           **kwargs):
        results = {
            'game': 2,
            'boxscore_index': '2017-09-09-michigan',
            'date': 'Sep 9, 2017',
            'time': '12:00 PM',
            'day_of_week': 'Sat',
            'datetime': datetime(2017, 9, 9, 12, 0),
            'location': HOME,
            'rank': 8,
            'opponent_abbr': 'cincinnati',
            'opponent_name': 'Cincinnati',
            'opponent_rank': None,
            'opponent_conference': 'American',
            'result': WIN,
            'points_for': 36,
            'points_against': 14,
            'wins': 2,
            'losses': 0,
            'streak': 'W 2'
        }
        flexmock(Boxscore) \
            .should_receive('_parse_game_data') \
            .and_return(None)
        flexmock(Boxscore) \
            .should_receive('dataframe') \
            .and_return(pd.DataFrame([{'key': 'value'}]))
        flexmock(utils) \
            .should_receive('_find_year_for_season') \
            .and_return(2018)

        schedule = Schedule('MICHIGAN')

        for attribute, value in results.items():
            assert getattr(schedule[1], attribute) == value
