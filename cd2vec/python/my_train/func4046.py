@pytest.mark.parametrize('sets, out,exception', [
		({'a':1}, '"a"=1',None),
		(OrderedDict([
			('a', 1),
			('b[+]', 2)
		]), '"a"=1,"b"="b"+2',None),
		({'@#':None}, None, UpdateParseError)
	])
	def testStr(self, sets, out, exception):
		if exception:
			with pytest.raises(exception):
				str(Set(sets))
		else:
			assert str(Set(sets)) == out
