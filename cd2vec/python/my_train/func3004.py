def test_put_cant_edit_others_attribute(self):
        user = create_user()
        another_user_data = {
            "username": "paul",
            "password": "secret",
            "email": "paul@beatles.com",
        }
        another_user = create_user(**another_user_data)
        another_user_data["password"] = "changed_secret"
        another_user_data["email"] = "paulmc@beatles.com"
        login_user(self.client, user)

        response = self.client.patch(
            path=reverse("user-detail", args=(another_user.pk,)), data=another_user_data
        )

        self.assert_status_equal(response, status.HTTP_404_NOT_FOUND)

        another_user.refresh_from_db()
        assert another_user.email, "paul@beatles.com"
