def test_species_facet():
    payload = search_associations(subject_category='gene',
                                  object_category='phenotype',
                                  facet_fields=['subject_taxon', 'subject_taxon_label'],
                                  rows=0)
    fcs = payload['facet_counts']
    print(str(fcs))
    assert 'Homo sapiens' in fcs['subject_taxon_label'].keys()

