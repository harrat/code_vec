def test_initialization_and_accumulation(self):
        holding = PanamahHolding(id='1234', descricao='teste')
        acesso = PanamahAcesso(id='4321', funcionario_ids=['1', '2'])
        secao = PanamahSecao(id='5555', codigo='6666', descricao='teste')

        b = BatchProcessor('auth', 'secret', '12345')

        self.assertTrue(os.path.exists(ROOT_PATH))
        self.assertTrue(os.path.exists(ACCUMULATED_PATH))
        self.assertTrue(os.path.exists(SENT_PATH))

        b.save(holding)

        self.assertEqual(b.current_batch.length, 1)
        self.assertEqual(b.current_batch.size, 107)
        sleep(.5)
        self.assertAlmostEqual(b.current_batch.age, 0.5, delta=0.2)

        b.save(acesso)

        self.assertEqual(b.current_batch.length, 2)
        self.assertEqual(b.current_batch.size, 221)
        sleep(.5)
        self.assertAlmostEqual(b.current_batch.age, 1, delta=0.2)

        b.save(secao)

        self.assertEqual(b.current_batch.length, 3)
        self.assertEqual(b.current_batch.size, 344)
        sleep(.5)
        self.assertAlmostEqual(b.current_batch.age, 1.5, delta=0.2)

        b.delete(holding)

        self.assertEqual(b.current_batch.length, 4)
        self.assertEqual(b.current_batch.size, 429)
        sleep(.5)
        self.assertAlmostEqual(b.current_batch.age, 2, delta=0.2)

        b.delete(secao)

        self.assertEqual(b.current_batch.length, 5)
        self.assertEqual(b.current_batch.size, 512)
        sleep(.5)
        self.assertAlmostEqual(b.current_batch.age, 2.5, delta=0.2)

        b.delete(acesso)

        self.assertEqual(b.current_batch.length, 6)
        self.assertEqual(b.current_batch.size, 596)
        sleep(.5)
        self.assertAlmostEqual(b.current_batch.age, 3, delta=0.2)

        self.assertEqual(
            b.current_batch.json(),
            '[{"data": {"id": "1234", "descricao": "teste"}, "tipo": "HOLDING", "op": "update", "assinanteId": "12345"}, {"data": {"id": "4321", "funcionarioIds": ["1", "2"]}, "tipo": "ACESSO", "op": "update", "assinanteId": "12345"}, {"data": {"id": "5555", "codigo": "6666", "descricao": "teste"}, "tipo": "SECAO", "op": "update", "assinanteId": "12345"}, {"tipo": "HOLDING", "op": "delete", "data": {"id": "1234"}, "assinanteId": "12345"}, {"tipo": "SECAO", "op": "delete", "data": {"id": "5555"}, "assinanteId": "12345"}, {"tipo": "ACESSO", "op": "delete", "data": {"id": "4321"}, "assinanteId": "12345"}]'
        )

        b.save(holding)

        self.assertEqual(b.current_batch.length, 6)

        self.assertEqual(
            b.current_batch.json(),
            '[{"data": {"id": "4321", "funcionarioIds": ["1", "2"]}, "tipo": "ACESSO", "op": "update", "assinanteId": "12345"}, {"data": {"id": "5555", "codigo": "6666", "descricao": "teste"}, "tipo": "SECAO", "op": "update", "assinanteId": "12345"}, {"tipo": "HOLDING", "op": "delete", "data": {"id": "1234"}, "assinanteId": "12345"}, {"tipo": "SECAO", "op": "delete", "data": {"id": "5555"}, "assinanteId": "12345"}, {"tipo": "ACESSO", "op": "delete", "data": {"id": "4321"}, "assinanteId": "12345"}, {"data": {"id": "1234", "descricao": "teste"}, "tipo": "HOLDING", "op": "update", "assinanteId": "12345"}]'
        )
