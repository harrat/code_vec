def test_nbexec_cli(tmp_path: Path) -> None:
    """Run nbexec to execute a temporary notebook file."""
    runner = CliRunner()
    with runner.isolated_filesystem():
        nb = make_notebook(tmp_path)
        result = runner.invoke(nbexec_cli.nbexec_cli, nb)
        cells = nbformat.reads(result.output, as_version=4).cells
        for cell in cells:
            if cell.cell_type == "code":
                assert cell.execution_count
                for output in cell.outputs:
                    assert output
