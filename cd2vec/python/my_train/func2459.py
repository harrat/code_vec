def test_great(self):
        # f1 = 12288
        r1 = ruler.DirSizeRule("> 12288b")
        r2 = ruler.DirSizeRule("> 12287b")
        r3 = ruler.DirSizeRule(">= 12288b")
        r4 = ruler.DirSizeRule(">= 12289b")

        self.assertFalse(r1.match(self.f1))
        self.assertTrue(r2.match(self.f1))
        self.assertTrue(r3.match(self.f1))
        self.assertFalse(r4.match(self.f1))
