def test_skip_prgenv_check_option(run_reframe):
    returncode, stdout, _ = run_reframe(
        checkpath=['unittests/resources/checks/frontend_checks.py'],
        more_options=['--skip-prgenv-check', '-t', 'NoPrgEnvCheck']
    )
    assert 'PASSED' in stdout
    assert returncode == 0

