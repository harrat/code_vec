def test_get_info_message(self):
        output.setup(False, False, False)
        with utils.capture_sys_output() as (stdout, stderr):
            recs = api.get_info_message()

        self.assertNotIn("Exception", stderr.getvalue())
        self.assertTrue(len(recs) > 0)
