def test_new_task_bad_args(self):
        """
        Trying to create a new task with invalid arguments must raise a
        `TypeError` exception.
        """
        # Missing argument
        task_tmpl = TaskTemplate('my-task-holder')
        with self.assertRaisesRegex(TypeError, 'positional argument'):
            task_tmpl.new_task()

        # Too many arguments
        task_tmpl = TaskTemplate('bad-coro-task')
        with self.assertRaisesRegex(TypeError, 'positional argument'):
            task_tmpl.new_task('junk')
