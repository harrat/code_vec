@pytest.mark.vcr()
def test_workbench_export_plugin_id_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.export(plugin_id='something')
