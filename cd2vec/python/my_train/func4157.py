def test_read_delete(self):
        """
        Tests helpers.File.read along with helpers.File.delete.
        """

        expected = "Hello, World! This has been written by Fun Ilrys."
        File("hi").write(expected)
        actual = File("hi").read()

        self.assertEqual(expected, actual)

        expected = False
        File("hi").delete()
        actual = path.isfile("hi")

        self.assertEqual(expected, actual)
