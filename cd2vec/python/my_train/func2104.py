def test_invalid_data(self):
        ''' Invalid data. '''
        with self.assertRaisesRegexp(ValueError, r'\[barchart\] .*array.*'):
            barchart.draw(self.axes, [[1, 2], [1, 2, 3]])
