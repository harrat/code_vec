def test_save_no_trained_policy_exception(self):
        p1 = PpoAgent(gym_env_name=_line_world_name, fc_layers=(10, 20, 30), backend='tfagents')
        with pytest.raises(Exception):
            p1.save()
