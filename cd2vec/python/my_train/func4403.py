def test_post_html_with_charset(self):
        """Test POSTing a resource with a Content-Type that specifies a
        character set."""
        response = self.app.post('/artists',
                content_type='application/x-www-form-urlencoded',
                charset='UTF-8;',
                headers={'Accept': 'text/html'},
                data={u'Name': u'Jeff Knupp'})
        assert response.status_code == 201
        assert 'Jeff Knupp' in str(response.get_data(as_text=True))
