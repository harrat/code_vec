@pytest.mark.parametrize('os_type', ['win32',
                                     'win64',
                                     'linux32',
                                     'linux64',
                                     'mac64'])
def test_can_get_driver_from_cache(os_type):
    GeckoDriverManager(os_type=os_type).install()
    driver_path = GeckoDriverManager(os_type=os_type).install()
    assert os.path.exists(driver_path)

