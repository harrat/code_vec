def test_home_saves_multiple_goalies_empty_field(self):
        saves = ['29', '3', '30']

        fake_saves = PropertyMock(return_value=saves)
        fake_num_goalies = PropertyMock(return_value=1)
        type(self.boxscore)._home_saves = fake_saves
        type(self.boxscore)._away_goalies = fake_num_goalies

        assert self.boxscore.home_saves == 33
def test_home_saves_multiple_goalies_empty_field(self):
        saves = ['29', '30', '']

        fake_saves = PropertyMock(return_value=saves)
        fake_num_goalies = PropertyMock(return_value=1)
        type(self.boxscore)._home_saves = fake_saves
        type(self.boxscore)._away_goalies = fake_num_goalies

        assert self.boxscore.home_saves == 30
