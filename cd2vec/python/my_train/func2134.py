def test_resourcecfg_add(self, centreon_con):
        values = [
            'resource_test',
            'ressource_value',
            'Central',
            'comment'
        ]
        data = {}
        data['action'] = 'add'
        data['object'] = 'RESOURCECFG'
        data['values'] = values

        with patch('requests.post') as patched_post:
            centreon_con.resourcecfgs.add("resource_test",
                                          "ressource_value",
                                          "Central",
                                          "comment",
                                          post_refresh=False
                                          )
            patched_post.assert_called_with(self.clapi_url, headers=self.headers, data=json.dumps(data), verify=True)
