def test_tree(self):
        temp = TempDir()
        foo = temp.make_directory(name='foo')
        bar = temp.make_directory(name='bar', parent=foo)
        f = temp.make_file(name='baz', parent=bar)
        assert f == temp.absolute_path / 'foo' / 'bar' / 'baz'
        temp.close()
        assert not f.exists()
