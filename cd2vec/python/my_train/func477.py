def test_decrypt(self):
        """
        Test that an Decrypt request can be processed correctly.

        The test vectors used here come from Eric Young's test set for
        Blowfish, via https://www.di-mgt.com.au/cryptopad.html.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        decryption_key = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.TRIPLE_DES,
            128,
            (
                b'\x01\x23\x45\x67\x89\xAB\xCD\xEF'
                b'\xF0\xE1\xD2\xC3\xB4\xA5\x96\x87'
            ),
            [enums.CryptographicUsageMask.DECRYPT]
        )
        decryption_key.state = enums.State.ACTIVE

        e._data_session.add(decryption_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        unique_identifier = str(decryption_key.unique_identifier)
        cryptographic_parameters = attributes.CryptographicParameters(
            block_cipher_mode=enums.BlockCipherMode.CBC,
            padding_method=enums.PaddingMethod.PKCS5,
            cryptographic_algorithm=enums.CryptographicAlgorithm.BLOWFISH
        )
        data = (
            b'\x6B\x77\xB4\xD6\x30\x06\xDE\xE6'
            b'\x05\xB1\x56\xE2\x74\x03\x97\x93'
            b'\x58\xDE\xB9\xE7\x15\x46\x16\xD9'
            b'\x74\x9D\xEC\xBE\xC0\x5D\x26\x4B'
        )
        iv_counter_nonce = b'\xFE\xDC\xBA\x98\x76\x54\x32\x10'

        payload = payloads.DecryptRequestPayload(
            unique_identifier,
            cryptographic_parameters,
            data,
            iv_counter_nonce
        )

        response_payload = e._process_decrypt(payload)

        e._logger.info.assert_any_call("Processing operation: Decrypt")
        self.assertEqual(
            unique_identifier,
            response_payload.unique_identifier
        )
        self.assertEqual(
            (
                b'\x37\x36\x35\x34\x33\x32\x31\x20'
                b'\x4E\x6F\x77\x20\x69\x73\x20\x74'
                b'\x68\x65\x20\x74\x69\x6D\x65\x20'
                b'\x66\x6F\x72\x20\x00'
            ),
            response_payload.data
        )
