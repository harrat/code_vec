def test_set_middle_bad1(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        with self.assertRaises(TypeError):
            audiofile.set_head_middle_tail(middle_length=0.000)
