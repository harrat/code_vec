def test_set_header_overrides_filters_out_header(self):
        self.client.set_header_overrides({
            'User-Agent': None
        })
        self._make_request('https://www.wikipedia.org')

        last_request = self.client.get_last_request()

        self.assertNotIn('User-Agent', last_request['headers'])
