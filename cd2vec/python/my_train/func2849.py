@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'edit-beats')
    def test_cli_create_solution_002(snippy):
        """Try to create solution from CLI.

        Try to create same solution again with exactly the same content data.
        """

        content = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        cause = snippy.run(['snippy', 'create', '--scat', 'solution', '--format', 'text'])
        assert cause == 'NOK: content data already exist with digest 4346ba4c79247430'
        Content.assert_storage(content)
