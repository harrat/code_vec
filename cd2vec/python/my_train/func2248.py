def test_write_cluster_stats(self, caplog, tmp_dir):
        res = wrapper.Output(target_root_name)
        res.extract_results()
        res.aggregate_input_data()
        res.write_class_stats()
        assert os.path.isfile(res.root_out_name + "_stats.tsv")
        ref_file = target_root_path + "_out_stats.tsv"
        assert filecmp.cmp(ref_file,
                           res.root_out_name + "_stats.tsv",
                           shallow=False)
