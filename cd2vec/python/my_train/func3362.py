def test_version_without_tag_autocount(self):
        count = 0x10
        for i in range(count):
            filename = 'file{}.txt'.format(i)
            touch(filename)
            runner('git add {}'.format(filename))
            runner('git commit -m "added {}"'.format(filename))

        v = relic.release.get_info()
        assert v is not None
        assert isinstance(v, relic.git.GitVersion)
        assert '0.0.0.dev' in v.pep386
        assert '+' in v.pep386
        assert int(v.post) == count + 1
