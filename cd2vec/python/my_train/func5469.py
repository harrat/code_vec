def test_defined_wont_fit():
    progress_bar = ProgressBarBytes(2000, max_width=33)
    assert '  0% (0.00/1.95 KiB) [] eta --:-- |' == str(progress_bar)

    progress_bar = ProgressBarBytes(2000, max_width=30)
    assert '  0% (0.00/1.95 KiB) [] eta --:-- /' == str(progress_bar)

