@pytest.mark.vcr()
def test_workbench_asset_vulns_filter_type_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.asset_vulns(str(uuid.uuid4()), filter_type='NOT')
