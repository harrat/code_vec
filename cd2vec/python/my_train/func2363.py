def test_locate_with_cryptographic_length(self):
        """
        Test the Locate operation when the 'Cryptographic Length' attribute
        is given.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        key = (
            b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        )

        obj_a = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            128,
            key,
            name='name1'
        )
        obj_b = pie_objects.SecretData(
            key,
            enums.SecretDataType.PASSWORD
        )

        e._data_session.add(obj_a)
        e._data_session.add(obj_b)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        id_a = str(obj_a.unique_identifier)

        attribute_factory = factory.AttributeFactory()

        # Locate the symmetric key object based on its cryptographic length.
        attrs = [
            attribute_factory.create_attribute(
                enums.AttributeType.CRYPTOGRAPHIC_LENGTH,
                128
            )
        ]
        payload = payloads.LocateRequestPayload(attributes=attrs)
        e._logger.reset_mock()
        response_payload = e._process_locate(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call("Processing operation: Locate")
        e._logger.debug.assert_any_call(
            "Locate filter matched object: {}".format(id_a)
        )
        e._logger.debug.assert_any_call(
            "Failed match: "
            "the specified attribute (Cryptographic Length) is not "
            "applicable for the object's object type (SECRET_DATA)."
        )
        self.assertEqual(1, len(response_payload.unique_identifiers))
        self.assertIn(id_a, response_payload.unique_identifiers)

        # Try to locate a non-existent object based on its cryptographic
        # algorithm.
        attrs = [
            attribute_factory.create_attribute(
                enums.AttributeType.CRYPTOGRAPHIC_LENGTH,
                2048
            )
        ]
        payload = payloads.LocateRequestPayload(attributes=attrs)
        e._logger.reset_mock()
        response_payload = e._process_locate(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call("Processing operation: Locate")
        e._logger.debug.assert_any_call(
            "Failed match: "
            "the specified cryptographic length (2048) does not match "
            "the object's cryptographic length (128)."
        )
        e._logger.debug.assert_any_call(
            "Failed match: "
            "the specified attribute (Cryptographic Length) is not "
            "applicable for the object's object type (SECRET_DATA)."
        )
        self.assertEqual(0, len(response_payload.unique_identifiers))
