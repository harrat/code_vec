def test_that_multiple_compiles_do_not_occur_for_same_source(self):
        # Given
        n_proc = 5
        p = Pool(n_proc)

        # When

        # Note that _check_compile cannot be defined here or even in the
        # class as a nested function or instance method cannot be pickled.

        result = p.map(_check_compile, [self.root]*n_proc)
        p.close()

        # Then
        # The shutil.copy should have been run only once.
        self.assertEqual(sum(result), 1)
