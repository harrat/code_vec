def test_write_cdt(self, caplog, tmp_dir):
        res = wrapper.Output(target_root_name)
        res.extract_results()
        res.aggregate_input_data()
        res.write_cdt()
        assert os.path.isfile(res.root_out_name+".cdt")
        ref_file = target_root_path + "_out.cdt"
        assert filecmp.cmp(ref_file, res.root_out_name + ".cdt", shallow=False)
