def test_wo_title(self):
        true_msg = '[test] finish time: 0.50 sec.'
        f = Mock()

        t = Timer(name='test', callback=f)
        time.sleep(0.1)
        t.start()
        time.sleep(0.5)
        t.stop()

        f.assert_called_once_with(true_msg)
