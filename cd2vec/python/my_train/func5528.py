@staticmethod
    @pytest.mark.usefixtures('import-gitlog', 'update-pytest-utc')
    def test_api_update_reference_001(server):
        """Update one reference with PUT request.

        Send PUT /references/{id} to update existing resource with digest.
        See 'updating content attributes' for the attribute list that can be
        changed by user.
        """

        storage = {
            'data': [
                Storage.pytest
            ]
        }
        storage['data'][0]['created'] = Content.GITLOG_TIME
        storage['data'][0]['updated'] = Content.PYTEST_TIME
        storage['data'][0]['uuid'] = Reference.GITLOG_UUID
        storage['data'][0]['digest'] = Reference.PYTEST_DIGEST
        request_body = {
            'data': {
                'type': 'reference',
                'attributes': {
                    'data': storage['data'][0]['data'],
                    'brief': storage['data'][0]['brief'],
                    'description': storage['data'][0]['description'],
                    'groups': storage['data'][0]['groups'],
                    'tags': storage['data'][0]['tags'],
                    'links': storage['data'][0]['links']
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '715'
        }
        expect_body = {
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/references/' + Reference.GITLOG_UUID
            },
            'data': {
                'type': 'reference',
                'id': storage['data'][0]['uuid'],
                'attributes': storage['data'][0]
            }
        }
        result = testing.TestClient(server.server.api).simulate_put(
            path='/api/snippy/rest/references/5c2071094dbfaa33',
            headers={'accept': 'application/vnd.api+json; charset=UTF-8'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
