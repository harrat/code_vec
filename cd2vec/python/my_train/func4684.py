@freeze_time('2014-01-21')
def test_regroup_entries_setting(cli, config, entries_file):
    config.set('taxi', 'regroup_entries', '0')
    entries_file.write("""20/01/2014
alias_1 0800-0900 Play ping-pong
alias_1 1200-1300 Play ping-pong
""")

    stdout = cli('commit')
    assert line_in(
        "alias_1 1.00  Play ping-pong",
        stdout
    )
