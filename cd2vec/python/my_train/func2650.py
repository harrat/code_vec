def test_one_syllable(self):
        word = 'a'
        data = get_definition(word)
        assert type(data) is dict
        assert len(data) > 0
        assert len(data['sillabe']) == 1
    def test_verb(self):
        word = 'essere'
        data = get_definition(word)
        assert type(data) is dict
        assert len(data) > 0
