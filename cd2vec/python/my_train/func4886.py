def test_invalid_colors(self):
        ''' Invalid colors. '''
        with self.assertRaisesRegexp(ValueError, r'\[barchart\] .*colors.*'):
            barchart.draw(self.axes, _data(), colors=['k'])
