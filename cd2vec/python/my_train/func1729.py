def test_auto_log_queue(auto_logger):
    def proc(q):
        local.name = "the-process"
        q.join()
        advance(100)

    sim = Simulator()
    queue = Queue(name="the-queue")
    sim.add(proc, queue)
    sim.run()
    queue.pop()
    sim.run(10)

    check_log(
        auto_logger,
        (logging.INFO, 0.0, "", "Simulator", sim.name, "add", dict(fn=proc, args=(queue,), kwargs={})),
        (logging.INFO, 0.0, "", "Simulator", sim.name, "run", dict(duration=inf)),
        (logging.INFO, 0.0, "the-process", "Queue", "the-queue", "join", {}),
        (logging.INFO, 0.0, "the-process", "Process", "the-process", "pause", {}),
        (logging.INFO, 0.0, "", "Simulator", sim.name, "stop", {}),
        (logging.INFO, -1.0, "", "Queue", "the-queue", "pop", dict(process="the-process")),
        (logging.INFO, -1.0, "", "Process", "the-process", "resume", {}),
        (logging.INFO, 0.0, "", "Simulator", sim.name, "run", dict(duration=10.0)),
        (logging.INFO, 0.0, "the-process", "Process", "the-process", "advance", dict(delay=100.0)),
        (logging.INFO, 10.0, "", "Simulator", sim.name, "stop", {})
    )
