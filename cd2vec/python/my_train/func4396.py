def test_msg_type_16(self):
        msg = NMEAMessage(b"!AIVDM,1,1,,A,@01uEO@mMk7P<P00,0*18").decode()
        assert msg['type'] == 16
        assert msg['repeat'] == 0
        assert msg['mmsi'] == 2053501
        assert msg['mmsi1'] == 224251000
        assert msg['offset1'] == 200
        assert msg['increment1'] == 0

        assert msg['mmsi2'] == 0
        assert msg['offset2'] == 0
        assert msg['increment1'] == 0
