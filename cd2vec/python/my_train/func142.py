def test_open_atomic(tmpdir, binary_file):
    """Test wkr.os.open_atomic."""
    binary_path = tmpdir.__class__(binary_file)
    assert tmpdir.exists()
    assert binary_path.exists()
    assert binary_path.size() > 0
    original_contents = binary_path.read(mode='rb')
    # now we do an atomic write on binary_file
    with open_atomic(binary_file, fsync=True) as output_file:
        new_contents = b'abcde'
        assert new_contents != original_contents
        output_file.write(new_contents)
        output_file.flush()
        # at this point the original file still hasn't changed
        assert binary_path.read(mode='rb') == original_contents
    # now we close the file, and it's atomically moved into its final
    # location
    assert binary_path.read(mode='rb') == new_contents

