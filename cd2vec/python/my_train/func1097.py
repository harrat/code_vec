def test_request_should_return_json_object():
    ss.stub_server_request(method='GET', url=moyasar.api_url + '/payments',
                           resource=f.payments, status=200)
    response = moyasar.request('GET', moyasar.api_url + '/payments', None)
    assert isinstance(response.json(), dict)

