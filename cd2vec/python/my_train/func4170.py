def test_post_can_timeout(self):
        ab = AuthenticationBase('auth0.com', timeout=0.00001)

        with self.assertRaises(requests.exceptions.Timeout):
            ab.post('https://google.com', data={'a': 'b'}, headers={'c': 'd'})
