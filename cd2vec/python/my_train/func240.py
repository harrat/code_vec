def test_ddt_dataset_with_spacy(self):
        ddt = DDT()  # Load dataset
        corpus = ddt.load_with_spacy()

        num_sents_train = 0
        for paragraph in [paragraph[1] for paragraph in list(corpus.train_tuples)]:
            num_sents_train += len(paragraph)

        self.assertIsInstance(corpus, GoldCorpus)
        self.assertEqual(self.train_len, num_sents_train)
# temporary omitted due to changes in storage
#     def test_wikiann_dataset(self):
#         # Change to a sample of the full wikiann to ease test computation
#         DATASETS['wikiann']['url'] = "https://danlp.s3.eu-central-1.amazonaws.com/test-datasets/da.tar.gz"
#         DATASETS['wikiann']['size'] = 2502
#         DATASETS['wikiann']['md5_checksum'] = 'd0271de38ae23f215b5117450efb9ace'
