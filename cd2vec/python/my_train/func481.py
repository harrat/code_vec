def test_callbacks(self):
        """
        Test all combinations of types acting as observed and observer.

        For each combination of observed and observer we check that all
        observers are called. We also check that after discarding the
        observers subsequent invocations of the observed object do not call
        any observers.
        """

        a = Foo('a', self.buf)
        b = Foo('b', self.buf)
        c = Goo('c', self.buf)
        d = Goo('d', self.buf)

        @observable_function
        def f():
            self.buf.append('f')

        @observable_function
        def g(caller):
            self.buf.append('g%s'%(get_caller_name(caller),))

        # We don't include g in our set of observables because the testing
        # code isn't smart enough to call it with an argument.
        observables = get_observables(a, b, c, d, f)
        observer_sets = get_observer_sets(a, b, c, d, (f, False), (g, True))
        items = get_items(observables, observer_sets)

        for observed, observer_set, expected_buf, final_buf in items:
            for observer, identify_observed in observer_set:
                observed.add_observer(observer,
                    identify_observed=identify_observed)
            observed()
            self.buf.sort()
            assert self.buf == expected_buf
            clear_list(self.buf)
            for observer, _ in observer_set:
                observed.discard_observer(observer)
            observed()
            assert self.buf == final_buf
            clear_list(self.buf)
