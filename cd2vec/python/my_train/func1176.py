def test_addon1():
    """ addon1 has 4 commit after version 8.0.1.0.0 """
    addon1_dir = os.path.join(DATA_DIR, "addon1")
    version = get_git_postversion(addon1_dir, STRATEGY_99_DEVN)
    assert version == "8.0.1.0.0.99.dev4"
    version = get_git_postversion(addon1_dir, STRATEGY_P1_DEVN)
    assert version == "8.0.1.0.1.dev4"

