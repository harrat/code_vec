def test_observer_content_aware_subjective_model(self):
        subjective_model = MaximumLikelihoodEstimationModel.from_dataset_file(
            self.dataset_filepath)
        result = subjective_model.run_modeling(force_subjbias_zeromean=False)

        self.assertAlmostEqual(float(np.nansum(result['content_ambiguity'])), 2.653508643860357, places=4)
        self.assertAlmostEqual(float(np.nanvar(result['content_ambiguity'])), 0.0092892978862108271, places=4)

        self.assertAlmostEqual(float(np.sum(result['observer_bias'])), -0.020313188445860726, places=4)
        self.assertAlmostEqual(float(np.var(result['observer_bias'])), 0.091830942654165318, places=4)

        self.assertAlmostEqual(float(np.sum(result['observer_inconsistency'])), 11.232923468639161, places=4)
        self.assertAlmostEqual(float(np.var(result['observer_inconsistency'])), 0.027721095664357907, places=4)

        self.assertAlmostEqual(float(np.sum(result['quality_scores'])), 177.88599894484821, places=4)
        self.assertAlmostEqual(float(np.var(result['quality_scores'])), 1.4896077857605587, places=4)

        # self.assertAlmostEqual(np.nansum(result['content_ambiguity_std']), 0.30465244947706538, places=4)
        self.assertAlmostEqual(float(np.sum(result['observer_bias_std'])), 2.165903882505483, places=4)
        self.assertAlmostEqual(float(np.sum(result['observer_inconsistency_std'])), 27.520643824238352, places=4)
        self.assertAlmostEqual(float(np.sum(result['quality_scores_std'])), 5.7355563435912256, places=4)
