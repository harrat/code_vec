def test_writer_can_be_read_by_reader(self):
        fname = '/tmp/testobj{}.obj'.format(uuid4())
        self.writer.dump(fname)
        geoms = read_objfile(fname)
        self.assertTrue(self.objname in geoms)
        obj = geoms[self.objname]
        self.assertTrue(np.isclose(np.array(self.verts), np.array(obj['v'])).all())
        self.assertTrue(np.isclose(np.array(self.norms), np.array(obj['vn'])).all())
