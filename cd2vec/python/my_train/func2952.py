def test_rss_parser():
    my_opener = urllib2.build_opener(TestHTTPHandler)
    urllib2.install_opener(my_opener)
    crawled_feed, data_for_crawl = parse_rss2(
        rss_xml,
        'http://sourcetest.com/rss.xml'
    )
    feed = read(Feed, write(crawled_feed, as_bytes=True))
    assert crawled_feed.id == feed.id
    title = crawled_feed.title
    assert title.type == feed.title.type
    assert title.value == feed.title.value
    links = crawled_feed.links
    assert links[1].mimetype == feed.links[1].mimetype
    assert links[1].relation == feed.links[1].relation
    assert links[1].uri == feed.links[1].uri
    rights = crawled_feed.rights
    assert rights.type == feed.rights.type
    assert rights.value == feed.rights.value
    contributors = crawled_feed.contributors
    assert contributors[0].name == feed.contributors[0].name
    assert contributors[0].email == feed.contributors[0].email
    assert contributors[1].name == feed.contributors[1].name
    assert contributors[1].email == feed.contributors[1].email
    updated_at = crawled_feed.updated_at
    assert updated_at == feed.updated_at
    categories = crawled_feed.categories
    assert categories[0].term == feed.categories[0].term
    entries = crawled_feed.entries
    assert entries[0].title.type == feed.entries[0].title.type
    assert entries[0].title.value == feed.entries[0].title.value
    assert entries[0].links[0].mimetype == feed.entries[0].links[0].mimetype
    assert entries[0].links[0].relation == feed.entries[0].links[0].relation
    assert entries[0].links[0].uri == feed.entries[0].links[0].uri
    assert entries[0].content.value == feed.entries[0].content.value
    assert entries[0].authors[0].name == feed.entries[0].authors[0].name
    assert entries[0].authors[0].email == feed.entries[0].authors[0].email
    assert entries[0].links[1].mimetype == feed.entries[0].links[1].mimetype
    assert entries[0].links[1].uri == feed.entries[0].links[1].uri
    assert entries[0].id == feed.entries[0].id
    assert (entries[0].published_at ==
            entries[0].updated_at ==
            feed.entries[0].published_at ==
            feed.entries[0].updated_at)
    source = entries[0].source
    assert source.title.type == feed.entries[0].source.title.type
    assert source.title.value == feed.entries[0].source.title.value
    assert source.links[1].mimetype == feed.entries[0].source.links[1].mimetype
    assert source.links[1].uri == feed.entries[0].source.links[1].uri
    assert source.links[1].relation == feed.entries[0].source.links[1].relation
    assert source.subtitle.type == feed.entries[0].source.subtitle.type
    assert source.subtitle.value == feed.entries[0].source.subtitle.value
    assert not source.entries

