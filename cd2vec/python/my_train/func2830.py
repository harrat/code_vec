def test_whole_body_post_ok(client, with_urlpatterns, routes: djhug.Routes):
    class RespModel(Body):
        product_id: int
        quantity: int

    @routes.post("<str:name>/")
    def view(request, name: str, body: RespModel):
        return {"name": name, "body": body.dict()}

    with_urlpatterns(list(routes.get_urlpatterns()))

    resp: HttpResponse = client.post("/purchase/", data={"product_id": 123})

    assert resp.status_code == 400, resp.content

    resp: HttpResponse = client.post("/purchase/", data={"product_id": 123, "quantity": 3})
    assert resp.status_code == 201, resp.content
    assert json.loads(resp.content) == {"name": "purchase", "body": {"product_id": 123, "quantity": 3}}

