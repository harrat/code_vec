def test_get_can_timeout(self):
        ab = AuthenticationBase('auth0.com', timeout=0.00001)

        with self.assertRaises(requests.exceptions.Timeout):
            ab.get('https://google.com', params={'a': 'b'}, headers={'c': 'd'})
