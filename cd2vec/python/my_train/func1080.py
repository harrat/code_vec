def test_sub_paths(self, test_client):
        rv = test_client.get("/api/v1/bananas/sub")
        assert "200" in str(rv.status)
        assert rv.get_json()["data"] == "sub"

        rv = test_client.get("/api/v1/test/sub_two")
        assert "401" in str(rv.status)
