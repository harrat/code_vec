@staticmethod
    def test_cli_search_snippet_040(snippy, capsys):
        """Print snippet with aligned comments.

        Print snippet with comments that must not trigger comment aligment
        because there is only one comment after one command. This verifies
        also that only one comment is aligned correctly.

        In this case, a colored print is used.
        """

        Content.store({
            'category': Const.SNIPPET,
            'data': [
                'tar cvfz mytar.tar.gz --exclude="mytar.tar.gz" ./',
                'tar tvf mytar.tar.gz # List content of compressed tar.',
                'tar xfO mytar.tar.gz manifest.json# Cat file in compressed tar.',
                't',
                '',
                'tar -xf mytar.tar.gz manifest.json'],
            'brief': 'Manipulate compressed tar files',
            'groups': ['linux'],
            'tags': ['howto', 'linux', 'tar', 'untar']
        })
        output = (
            '1. Manipulate compressed tar files @linux [0897e0e180afa68f]',
            '',
            '   $ tar cvfz mytar.tar.gz --exclude="mytar.tar.gz" ./',
            '   $ tar tvf mytar.tar.gz  #  List content of compressed tar.',
            '   $ tar xfO mytar.tar.gz manifest.json# Cat file in compressed tar.',
            '   $ t',
            '   $ ',
            '   $ tar -xf mytar.tar.gz manifest.json',
            '',
            '   # howto,linux,tar,untar',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--stag', 'tar'])
        out, err = capsys.readouterr()
        out = Helper.remove_ansi(out)
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
