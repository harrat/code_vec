def test_get_html(self):
        """Test getting HTML version of a resource rather than JSON."""
        response = self.get_response('/artists/1',
                200,
                headers={'Accept': 'text/html'})
        assert self.is_html_response(response)
