def test_paramstyle():
    assert py2jdbc.paramstyle == 'qmark', \
        "paramstyle is '%s', should be 'qmark'" % py2jdbc.paramstyle
