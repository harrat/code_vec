def test_delete_dimension(self):
        self.worksheet.clear()
        rows = self.worksheet.rows
        self.worksheet.update_row(10, [1, 2, 3, 4, 5])
        self.worksheet.delete_rows(10)
        assert self.worksheet.get_value((9, 2)) != 2
        assert self.worksheet.rows == rows - 1

        cols = self.worksheet.cols
        self.worksheet.update_col(10, [1, 2, 3, 4, 5])
        self.worksheet.delete_cols(10)
        assert self.worksheet.get_value((10, 2)) != 2
        assert self.worksheet.cols == cols - 1
