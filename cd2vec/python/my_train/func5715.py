def test_github_auth():

    with pytest.raises(TravisError) as exception_info:

        TravisPy.github_auth('invalid')

    assert str(exception_info.value) == '[403] not a Travis user'

