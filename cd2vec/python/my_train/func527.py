def test_image_hook_atari() -> None:
    c = rainy.Config()
    hook = lib.hooks.ImageWriterHook(
        out_dir="/tmp/rainy-acvp/imagehook-atari", transpose=(2, 0, 1)
    )
    c.eval_hooks.append(hook)
    c.set_net_fn("dqn", net.value.dqn_conv())
    c.set_env(lambda: envs.Atari("Breakout"))
    c.eval_env = envs.Atari("Breakout")
    ag = agents.DQNAgent(c)
    c.initialize_hooks()
    _ = ag.eval_episode()
    ag.close()
    episodes = np.load(hook.out_dir.joinpath("ep1.npz"))
    assert episodes["states"][0].shape == (3, 210, 160)
    assert len(episodes["actions"].shape) == 1

