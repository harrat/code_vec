def test_classification_report_imbalanced_multiclass_with_unicode_label():
    y_true, y_pred, _ = make_prediction(binary=False)

    labels = np.array(["blue\xa2", "green\xa2", "red\xa2"])
    y_true = labels[y_true]
    y_pred = labels[y_pred]

    expected_report = (
        "pre rec spe f1 geo iba sup blue� 0.83 0.79 0.92 0.81 "
        "0.85 0.72 24 green� 0.33 0.10 0.86 0.15 0.29 0.08 31 "
        "red� 0.42 0.90 0.55 0.57 0.70 0.51 20 avg / total "
        "0.51 0.53 0.80 0.47 0.58 0.40 75"
    )
    if np_version[:3] < (1, 7, 0):
        with pytest.raises(RuntimeError, match="NumPy < 1.7.0"):
            classification_report_imbalanced(y_true, y_pred)
    else:
        report = classification_report_imbalanced(y_true, y_pred)
        assert _format_report(report) == expected_report
