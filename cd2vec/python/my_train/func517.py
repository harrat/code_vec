@mock.patch("IPython.display.display")
def test_scraps_report(mock_display, notebook_collection):
    notebook_collection.scraps_report()
    mock_display.assert_has_calls(
        [
            mock.call(AnyMarkdownWith("### result1")),
            mock.call(AnyMarkdownWith("#### output")),
            mock.call({"text/plain": "'Hello World!'"}, metadata={}, raw=True),
            mock.call(AnyMarkdownWith("#### one_only")),
            mock.call({"text/plain": "'Just here!'"}, metadata={}, raw=True),
            mock.call(AnyMarkdownWith("<hr>")),
            mock.call(AnyMarkdownWith("### result2")),
            mock.call(AnyMarkdownWith("#### output")),
            mock.call({"text/plain": "'Hello World 2!'"}, metadata={}, raw=True),
            mock.call(AnyMarkdownWith("#### two_only")),
            mock.call({"text/plain": "'Just here!'"}, metadata={}, raw=True),
        ]
    )
