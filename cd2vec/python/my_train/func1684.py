def test_switch():
    pidom = PiDom()
    device_name = 'test'
    pidom.synchronize(device_name)
    assert pidom.state(device_name) is False
    pidom.switch_on(device_name)
    assert pidom.state(device_name) is True
    pidom.switch_off(device_name)
    assert pidom.state(device_name) is False

