def test_subscribe_to_channel(self, mocker):
        socket = BitMEXWebsocket()
        message = {
            "success": "true",
            "subscribe": "instrument:XBTH17",
            "request": {
                "op": "subscribe",
                "args": ["instrument:XBTH17"]
            }
        }

        handler = mocker.stub()
        socket.on('subscribe', handler)
        socket.on_message(json.dumps(message))
        handler.assert_called_once()
