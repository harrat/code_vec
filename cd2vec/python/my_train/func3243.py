def test_ncaaf_schedule_returns_requested_match_from_date(self):
        match_two = self.schedule(datetime(2017, 9, 9))

        for attribute, value in self.results.items():
            assert getattr(match_two, attribute) == value
