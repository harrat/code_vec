@pytest.mark.parametrize("test_id,case,mutation_mode", [
    ['1', 'pos', 'ref'],
    ['1', 'pos', 'germline'],
    ['1', 'pos', 'somatic'],
    ['1', 'pos', 'somatic_and_germline'],
    ['1', 'neg', 'ref'],
    ['1', 'neg', 'germline'],
    ['1', 'neg', 'somatic'],
    ['1', 'neg', 'somatic_and_germline']
])

# test build mode
def test_end_to_end_build(test_id, case, mutation_mode, tmpdir):
    data_dir = os.path.join(os.path.dirname(__file__), 'test{}'.format(test_id), 'data')
    out_dir = str(tmpdir)
    sample_dir_build = os.path.join(os.path.dirname(__file__), 'test{}'.format(test_id),'build','{}'.format(case),'test{}{}'.format(test_id,case))

    my_args_build = ['build','--samples', 'test{}{}'.format(test_id,case),
               '--output-dir', out_dir,
               '--splice-path',
               '{}/{}graph/spladder/genes_graph_conf3.merge_graphs.pickle'.format(
                   data_dir, case),
               '--count-path',
               '{}/{}graph/spladder/genes_graph_conf3.merge_graphs.count.hdf5'.format(
                   data_dir, case),
               '--ann-path', '{}/test{}{}.gtf'.format(data_dir, test_id, case),
               '--ref-path', '{}/test{}{}.fa'.format(data_dir, test_id, case),
               '--germline', '{}/test{}{}.vcf'.format(data_dir, test_id, case),
               '--somatic', '{}/test{}{}.maf'.format(data_dir, test_id, case),
               '--mutation-mode', mutation_mode,
               '--kmer', '4']

    my_args = my_args_build
    sample_dir = sample_dir_build
    main_immuno.split_mode(my_args)
    _assert_files_equal(
        os.path.join(sample_dir, '{}_metadata.tsv.gz'.format(mutation_mode)),
        os.path.join(out_dir, 'test{}{}'.format(test_id, case), '{}_metadata.tsv.gz'.format(mutation_mode)))
    # peptide
    _assert_files_equal(
        os.path.join(sample_dir, '{}_peptides.fa'.format(mutation_mode)),
        os.path.join(out_dir, 'test{}{}'.format(test_id, case), '{}_peptides.fa'.format(mutation_mode)))
    _assert_files_equal(
        os.path.join(sample_dir, '{}_back_peptides.fa'.format(mutation_mode)),
        os.path.join(out_dir, 'test{}{}'.format(test_id, case), '{}_back_peptides.fa'.format(mutation_mode)))
    #kmer
    _assert_files_equal(
        os.path.join(sample_dir, '{}_back_kmer.txt'.format(mutation_mode)),
        os.path.join(out_dir, 'test{}{}'.format(test_id, case), '{}_back_kmer.txt'.format(mutation_mode)))
    _assert_files_equal(
        os.path.join(sample_dir, '{}_junction_kmer.txt'.format(mutation_mode)),
        os.path.join(out_dir, 'test{}{}'.format(test_id, case), '{}_junction_kmer.txt'.format(mutation_mode)))
    _assert_files_equal(
        os.path.join(sample_dir, 'gene_expression_detail.tsv'),
        os.path.join(out_dir, 'test{}{}'.format(test_id, case), 'gene_expression_detail.tsv'))
