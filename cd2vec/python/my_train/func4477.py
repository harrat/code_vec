@pytest.mark.vcr()
def test_scan_export_format_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.scans.export(1, format='something else')
