@pytest.mark.parametrize('preset_response', RESPONSE_PRESET_TEXTS)
    def test_preset_reply_response(self, bot, bot_chat_id, generate_new_update, payload, preset_response):
        response = _PresetReplyResponse(preset_response)
        message_to_reply_to = bot.send_message(chat_id=bot_chat_id, text='Message to reply to')
        update = generate_new_update(chat_id=bot_chat_id, message_id=message_to_reply_to.message_id)

        message = response.respond(bot, update, payload)
        assert message
        assert is_acceptable(message.text, preset_response)
        assert message.reply_to_message.message_id == message_to_reply_to.message_id
