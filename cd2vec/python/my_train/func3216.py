def test_environ_snapshot(self):
        rt.loadenv(self.environ, self.environ_other)
        self.environ_save.restore()
        assert self.environ_save == env.snapshot()
        assert not rt.is_env_loaded(self.environ)
        assert not rt.is_env_loaded(self.environ_other)
