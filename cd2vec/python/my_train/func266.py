def test_post_receive(git_dir):
    git_dir.chdir()
    head = dzonegit.get_head()
    revisions = "{} {} refs/heads/master\n".format(
        "0000000000000000000000000000000000000000",
        head,
    )
    stdin = StringIO(revisions)
    codir = git_dir.join("co")
    subprocess.call(["git", "config", "dzonegit.checkoutpath", str(codir)])
    subprocess.call([
        "git", "config", "dzonegit.reconfigcmd",
        "echo TEST >{}/test".format(codir),
    ])
    dzonegit.post_receive(stdin)
    assert codir.join("dummy.zone").check()
    assert codir.join("test").read() == "TEST\n"
    # Test reconfig after renaming the file
    codir.join("test").write("")
    subprocess.call(["git", "mv", "dummy.zone", "dummy.zone.old"])
    subprocess.call(["git", "commit", "-m", "rename dummy zone"])
    revisions = "{} {} refs/heads/master\n".format(
        head,
        dzonegit.get_head(),
    )
    stdin = StringIO(revisions)
    dzonegit.post_receive(stdin)
    assert codir.join("test").read() == "TEST\n"

