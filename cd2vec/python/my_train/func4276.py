@all_modes()
def test_basic(d, mode):
    kern = install(d, "%s MOD1"%mode)
    #assert kern['argv'][0] == 'envkernel'  # defined above
    assert kern['ek'][1:3] == [mode, 'run']

@all_modes()
def test_display_name(d, mode):
    kern = install(d, "%s --display-name=AAA MOD1"%mode)
    assert kern['kernel']['display_name'] == 'AAA'
