@skipIf(gz_path is None, "'gzip' not available")
    def test_system_gzip(self):
        self.write_read_file('.gz', True)
