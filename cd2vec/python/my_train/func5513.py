def test_amazonscraper_get_100_products():
    products = amazonscraper.search(
                                keywords="Python",
                                max_product_nb=100)

    assert len(products) == 100

