def test_fetch_one():
    global cu
    cu.execute("delete from tests")
    cu.execute("insert into tests(id, name, text_field) values (?, ?, ?)",
               (14, 'test_fetch_one', 'foo'))
    cu.execute("select name from tests")
    row = cu.fetchone()
    assert row[0] == 'test_fetch_one'
    row = cu.fetchone()
    assert row is None

