def test_update_cells(self):
        self.worksheet.update_values(crange='A1:B2', values=[[1, 2], [3, 4]])
        assert self.worksheet.cell((1, 1)).value == str(1)
        self.worksheet.resize(1, 1)
        self.worksheet.update_values(crange='A1', values=[[1, 2, 5], [3, 4, 6], [3, 4, 61]], extend=True)
        assert self.worksheet.cols == 3
        assert self.worksheet.rows == 3
        assert self.worksheet.cell((3, 3)).value == '61'

        self.worksheet.resize(30, 30)
        cells = [pygsheets.Cell('A1', 10), pygsheets.Cell('A2', 12)]
        self.worksheet.update_values(cell_list=cells)
        assert self.worksheet.cell((1, 1)).value == str(cells[0].value)
