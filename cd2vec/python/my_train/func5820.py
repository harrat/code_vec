def test_tweet_none_media_TwitterError(coo_mock_instance):
    with pytest.raises(TwitterError):
        coo_mock_instance.tweet(["mock"], media=None)
