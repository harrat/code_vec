def test_hamed_rao_modification_test(NoTrendData, TrendData, arbitrary_1d_data):
    # check with no trend data
    NoTrendRes = mk.hamed_rao_modification_test(NoTrendData)
    assert NoTrendRes.trend == 'no trend'
    assert NoTrendRes.h == False
    assert NoTrendRes.p == 1.0
    assert NoTrendRes.z == 0
    assert NoTrendRes.Tau == 0.0
    assert NoTrendRes.s == 0.0
    
    # check with trendy data
    TrendRes = mk.hamed_rao_modification_test(TrendData)
    assert TrendRes.trend == 'increasing'
    assert TrendRes.h == True
    assert TrendRes.p == 0.0
    assert TrendRes.Tau == 1.0
    assert TrendRes.s == 64620.0
    
    # check with arbitrary data
    result = mk.hamed_rao_modification_test(arbitrary_1d_data)
    assert result.trend == 'decreasing'
    assert result.h == True
    assert result.p == 0.00012203829241275166
    assert result.z == -3.8419950613710894
    assert result.Tau == -0.03153167653875869
    assert result.s == -1959.0
    assert result.var_s == 259723.81316716125
    
def test_hamed_rao_modification_test_lag3(NoTrendData, TrendData, arbitrary_1d_data):
    # check with no trend data
    NoTrendRes = mk.hamed_rao_modification_test(NoTrendData, lag=3)
    assert NoTrendRes.trend == 'no trend'
    assert NoTrendRes.h == False
    assert NoTrendRes.p == 1.0
    assert NoTrendRes.z == 0
    assert NoTrendRes.Tau == 0.0
    assert NoTrendRes.s == 0.0
