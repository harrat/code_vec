@freeze_time('2014-01-21')
def test_commit_date(cli, entries_file):
    entries_file.write("""21/01/2014
alias_1 2 foobar

20/01/2014
alias_1 1 previous day entry
""")
    stdout = cli('commit', args=['--since=21.01.2014', '--until=21.01.2014'])
    assert 'previous day entry' not in stdout

    stdout = cli('commit')
    assert 'previous day entry' in stdout

