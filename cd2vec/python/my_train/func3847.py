def test_manual_context_noccr():
    Breathe.add_commands(
        CommandContext("test") | AppContext("italy"),
        {"spaghetti": DoNothing()},
        ccr=False
    )
    # Loaded rule should be referencing the original
    # "test" context loaded above, which should already be
    # active
    engine.mimic(["spaghetti"])
    engine.mimic(["disable", "test"])
    with pytest.raises(MimicFailure):
        engine.mimic(["spaghetti"])
        engine.mimic(["pizza", "curry"])
    engine.mimic(["spaghetti"], executable="italy")

def test_negated_context():
    Breathe.add_commands(
        ~(CommandContext("america") | AppContext("england")),
        {"steak": DoNothing(),
        }
    )
    engine.mimic(["steak"])
    with pytest.raises(MimicFailure):
        engine.mimic(["steak"], executable="england")
    engine.mimic(["enable", "america"])
    with pytest.raises(MimicFailure):
        engine.mimic(["steak"])
