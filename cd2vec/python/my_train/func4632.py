def test_ufactor_roofvegetation_construction(self):
        self.idf.initreadtxt(roof_vegetation)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL:ROOFVEGETATION", "RoofVegetation")
        expected = 1 / (
            INSIDE_FILM_R + m.Thickness / m.Conductivity_of_Dry_Soil + OUTSIDE_FILM_R
        )
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            assert c.ufactor == expected
            assert c.ufactor == 1 / 0.35
            # check that a UserWarning is raised
            assert issubclass(w[-1].category, UserWarning)
