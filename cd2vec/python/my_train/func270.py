def test_from_output_script(self):

        self.assertEqual(
            addr.from_output_script(helpers.OP_IF['output_script']),
            helpers.OP_IF['p2sh'])
        self.assertEqual(
            addr.from_output_script(
                helpers.P2WSH['ser']['ins'][0]['pk_script']),
            helpers.P2WSH['human']['ins'][0]['addr'])
        self.assertEqual(
            addr.from_output_script(helpers.PK['ser'][0]['pkh_output']),
            helpers.ADDR[0]['p2pkh'])
        self.assertEqual(
            addr.from_output_script(helpers.P2WPKH_ADDR['output']),
            helpers.P2WPKH_ADDR['address'])

        with self.assertRaises(ValueError) as context:
            addr.from_output_script(b'\x8e' * 34)
        self.assertIn(
            'Cannot parse address from script.',
            str(context.exception))
