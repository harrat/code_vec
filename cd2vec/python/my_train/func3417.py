def test_delete_attribute_from_managed_object_undeletable_attribute(self):
        """
        Test that a PermissionDenied error is raised when attempting to delete
        a required attribute from a managed object.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        managed_object = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )
        args = (managed_object, ("Cryptographic Algorithm", None, None))
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            "Cannot delete a required attribute.",
            e._delete_attribute_from_managed_object,
            *args
        )
