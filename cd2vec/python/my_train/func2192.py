@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_job_with_callback_and_references(fixture, request):
    def _callback(job):
        return job.function_result.obj

    testobj = MyRefObj()
    testobj.obj = MyRefObj()

    fn = pyccc.PythonCall(testobj.tagme)
    engine = request.getfixturevalue(fixture)
    job = engine.launch(image=PYIMAGE, command=fn,
                        interpreter=PYVERSION, when_finished=_callback, persist_references=True)
    print(job.rundata)
    job.wait()

    assert job.function_result is testobj
    assert job.result is testobj.obj
    assert job.function_result.obj is testobj.obj

    # A surprising result but correct behavior - because we replace the object with its reference,
    # it is unchanged.
    assert not hasattr(job.result, 'tag')
    assert not hasattr(testobj, 'tag')
    assert hasattr(job.updated_object, 'tag')

