@pytest.mark.system
    def test_get_episode(self):
        res = TVDB().episodes(183284)
        assert res.episode_name == 'Terror of the Zygons (2)'
        assert res.directors == ['Douglas Camfield']
