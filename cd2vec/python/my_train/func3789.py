def test_create_member(self):
        alias = utils.generate_alias(type=Alias.DOMAIN)  # Alias.EMAIL failed
        member = self.client.create_member(alias)
        keys = member.get_keys()
        assert len(keys) == 3
        assert len(member.get_aliases()) != 0
