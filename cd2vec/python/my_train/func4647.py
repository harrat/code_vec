def test_train(self):
        for backend in get_backends(RandomAgent):
            random_agent = RandomAgent(_line_world_name, backend=backend)
            tc: core.TrainContext = random_agent.train([log.Duration(), log.Iteration()],
                                                       num_iterations=10,
                                                       max_steps_per_episode=100,
                                                       default_plots=False)
            r = max_avg_rewards(tc)
            assert r >= 0
