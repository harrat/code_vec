def test_insert_into_cache_expired_ttl(self):
    """test_insert_into_cache_expired_ttl ensures that maybe_insert_into_cache
    inserts results into cache if the time to live is expired"""
    database = TinyDB('/tmp/.ossindex/jake.json')
    coordinate_query = Query()
    response = self.string_to_coordinatesresult(
        """[{"coordinates":"pkg:conda/pycrypto@2.6.1",
        "reference":"https://ossindex.sonatype.org/component/pkg:conda/pycrypto@2.6.1",
        "vulnerabilities":[]}]""")
    self.func.maybe_insert_into_cache(response)
    result_expired = database.search(
        coordinate_query.purl == "pkg:conda/pycrypto@2.6.1")
    time_unwind = parse(result_expired[0]['ttl']) - timedelta(hours=13)
    database.update({'ttl': time_unwind.isoformat()},
                    coordinate_query.purl == "pkg:conda/pycrypto@2.6.1")

    next_response = self.string_to_coordinatesresult(
        """[{"coordinates":"pkg:conda/pycrypto@2.6.1",
        "reference":"https://ossindex.sonatype.org/component/pkg:conda/pycrypto@2.6.1",
        "vulnerabilities":[]}]""")
    (cached, num_cached) = self.func.maybe_insert_into_cache(next_response)
    self.assertEqual(cached, True)
    self.assertEqual(num_cached, 1)
    database.close()
