@needs_unix_socket_mark
def test_forkserver_preimport(tmpdir):
    tmpdir = six.text_type(tmpdir)
    os.chdir(tmpdir)

    os.makedirs('benchmark')

    d = {}
    d.update(ASV_CONF_JSON)
    d['env_dir'] = "env"
    d['benchmark_dir'] = 'benchmark'
    d['repo'] = 'None'
    conf = config.Config.from_json(d)

    env = environment.ExistingEnvironment(conf, sys.executable, {}, {})

    #
    # Normal benchmark suite
    #

    with open(os.path.join('benchmark', '__init__.py'), 'w') as f:
        f.write("print('message')")

    spawner = runner.ForkServer(env, os.path.abspath('benchmark'))
    try:
        success, out = spawner.preimport()
    finally:
        spawner.close()

    assert success == True
    assert out.rstrip() == "message"

    #
    # Benchmark suite that crashes the forkserver
    #

    # NOTE: the .pyc files need to be removed, as Python 2 pyc caching
    # has problems with overwriting files rapidly
    clear_pyc('benchmark')

    with open(os.path.join('benchmark', '__init__.py'), 'w') as f:
        f.write("import os, sys; print('egassem'); sys.stdout.flush(); os._exit(0)")

    spawner = runner.ForkServer(env, os.path.abspath('benchmark'))
    try:
        success, out = spawner.preimport()
    finally:
        spawner.close()

    assert success == False
    assert out.startswith('asv: benchmark runner crashed')

    #
    # Benchmark suite that has an unimportable file
    #

    clear_pyc('benchmark')

    with open(os.path.join('benchmark', '__init__.py'), 'w') as f:
        pass

    with open(os.path.join('benchmark', 'bad.py'), 'w') as f:
        f.write("raise RuntimeError()")

    spawner = runner.ForkServer(env, os.path.abspath('benchmark'))
    try:
        success, out = spawner.preimport()
    finally:
        spawner.close()

    assert success == True
    assert out.startswith('Traceback')

