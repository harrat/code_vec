def test_description_entry_present(cli, entries_file):
    entries = """20/01/2014
alias_1 10:00-? Play tennis
"""
    expected = """20/01/2014
alias_1 10:00-10:15 Play ping-pong
"""

    entries_file.write(entries)
    with freeze_time('2014-01-20 10:10:00'):
        cli('stop', ['Play ping-pong'])

    assert entries_file.read() == expected

