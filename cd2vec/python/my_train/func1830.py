@pytest.mark.parametrize("a_node, expected_node", [
    (mapped_mock_task_node(),
     mapped_mock_headline_node({'asana_id': str(MOCK_TASK_ID)})),
    (mapped_mock_task_node({'notes': 'my stuff'}),
     mapped_mock_headline_node({'asana_id': str(MOCK_TASK_ID),
                                'paragraph': 'my stuff'})),
    (mapped_mock_task_node({'completed': True}),
     mapped_mock_headline_node({'asana_id': str(MOCK_TASK_ID),
                                'todo_type': 'DONE'})),
    (mapped_mock_task_node({'completed': False}),
     mapped_mock_headline_node({'asana_id': str(MOCK_TASK_ID),
                                'todo_type': 'TODO'})),
    (mapped_mock_task_node({'due_on': '2016-11-19'}),
     mapped_mock_headline_node({'asana_id': str(MOCK_TASK_ID),
                                'deadline': '2016-11-19'})),
    (mapped_mock_task_node({'due_at': '2016-11-20T03:34:00.000Z'}),
     mapped_mock_headline_node({'asana_id': str(MOCK_TASK_ID),
                                'deadline': '2016-11-20T03:34:00.000Z'})),
    (mapped_mock_task_node({'project_id': 261881552216474}),
     mapped_mock_headline_node({'asana_id': str(MOCK_TASK_ID),
                                'asana_project_id': '261881552216474'})),
    (mapped_mock_project_node({'name': 'My Inbox'}),
     mapped_mock_filename_node({'id': 'My Inbox',
                                'asana_project_id': str(MOCK_PROJECT_ID)})),
    (mapped_mock_task_node({'tags': {'@work'}}),
     mapped_mock_headline_node({'asana_id': str(MOCK_TASK_ID),
                                'tags': {'@work'}})),
    (mapped_mock_task_node({'tags': set()}),
     mapped_mock_headline_node({'asana_id': str(MOCK_TASK_ID),
                                'tags': set()})),
    (mapped_mock_task_node({'notes': ''}),
     mapped_mock_headline_node({'asana_id': str(MOCK_TASK_ID),
                                'paragraph': ''}))])
def test_o2a_behind_make_fn(a_node, expected_node, behind_source):
    "Does the Org Source have a special make_fn method?"
    o_node = behind_source.make_fn(a_node)
    assert_trees_equal_p(o_node, expected_node, ['id'])

