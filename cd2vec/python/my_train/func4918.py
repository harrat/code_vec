@mock.patch("yunomi.core.meter.time")
    def test_meter_calls_decorator(self, time_mock):
        time_mock.return_value = 0
        @meter_calls
        def test():
            pass

        for i in xrange(10):
            test()
        time_mock.return_value = 10
        self.assertAlmostEqual(meter("test_calls").get_mean_rate(), 1.0)
