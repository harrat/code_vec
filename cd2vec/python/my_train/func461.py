@pytest.mark.vcr()
def test_scan_import_scan(api):
    fobj = api.scans.export(SCAN_ID_WITH_RESULTS)
    api.scans.import_scan(fobj)

#@pytest.mark.vcr()
#def test_scan_launch_scanid_typeerror(api):
#    with pytest.raises(TypeError):
#        api.scans.launch('nope')
