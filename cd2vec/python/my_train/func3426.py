def test_commandline(self):
        cfg = LayeredConfig(self.yamlsource, Commandline())
        self.assertEqual("value", cfg.section.subsection.key)

        cmdline = ["./foo.py", "--foo=bar"]
        cfg = LayeredConfig(self.yamlsource, Commandline(cmdline))
        self.assertEqual("value", cfg.section.subsection.key)

        cmdline = ["./foo.py", "--foo=bar", "--section-subsection-key=other"]
        cfg = LayeredConfig(self.yamlsource, Commandline(cmdline))
        self.assertEqual("other", cfg.section.subsection.key)
