@staticmethod
    @pytest.mark.usefixtures('import-beats', 'caller')
    def test_api_update_solution_005(server):
        """Try to update solution with malformed request.

        Try to send PUT /solutions/{id} to update resource with a client
        generated resource ID. In this case the ID looks like a valid message
        digest.
        """

        storage = {
            'data': [
                Solution.BEATS
            ]
        }
        request_body = {
            'data': {
                'type': 'solution',
                'id': '59c5861b51701c2f52abad1a7965e4503875b2668a4df12f6c3386ef9d535970',
                'attributes': {
                    'data': Request.dnginx['data'],
                    'brief': Request.dnginx['brief'],
                    'groups': Request.dnginx['groups'],
                    'tags': Request.dnginx['tags'],
                    'links': Request.dnginx['links']
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '387'
        }
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '403',
                'statusString': '403 Forbidden',
                'module': 'snippy.testing.testing:123',
                'title': 'client generated resource id is not supported, remove member data.id'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_put(
            path='/api/snippy/rest/solutions/4346ba4c79247430',
            headers={'accept': 'application/json'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_403
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
