def test_exists_in_zooma_but_not_included_in_ot(ontclient):
    #Zooma finds a mapping, but the _is_included could fail
    assert ontclient.find_term('Failure to thrive') == 'http://purl.obolibrary.org/obo/HP_0001508'
