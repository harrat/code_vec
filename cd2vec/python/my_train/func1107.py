def test_contains():
	header = ljson.base.generic.Header(header_descriptor)
	table = ljson.base.mem.Table(header, data)


	assert {"lname": "griffin"} in table
	assert not {"lname": "griffindor"} in table

