def test_github_repo_branch_sha():
    environment = Environment(extensions=['jinja2_github.GitHubRepoBranchShaExtension'])

    template = environment.from_string(
        "{% github_repo_branch_sha 'jcfr/jinja2-github' %}"
    )

    assert template.render() != ""

    template = environment.from_string(
        "{% github_repo_branch_sha 'jcfr/jinja2-github', 'test-branch' %}"
    )

    assert template.render() == "188aeda35dcab7277ad37e1657e2b2780b6c8777"

