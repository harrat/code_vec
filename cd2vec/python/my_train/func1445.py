def test_plot_bo():
    """Assert than plot_bo runs without errors."""
    atom = ATOMClassifier(X_bin, y_bin, random_state=1)
    atom.run(['LR', 'LDA'], n_calls=25, bo_params={'plot_bo': True})
    assert not atom.errors

