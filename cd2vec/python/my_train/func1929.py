def test_return_type_none():
    obj = B2()
    another = B()
    assert obj.get() == "a"
    assert id(obj.a) == id(another.a)

