def test_author_search(self):
        author = sfpl.Search('J.K. Rowling', _type='author')
        results = author.getResults()

        for result in next(results):
            self.assertTrue('Rowling, J. K' in result.author)
