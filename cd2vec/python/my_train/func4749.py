def test_inference_pipeline(self):
        # run pipeline by composing functions
        print('cutout image chunk...')
        cutout_operator = CutoutOperator(
            self.input_volume_path,
            mip=self.mip,
            expand_margin_size=self.cropping_margin_size)
        chunk = cutout_operator(self.output_bbox)

        print('mask input...')
        mask_input_operator = MaskOperator(self.input_mask_volume_path,
                                           self.input_mask_mip,
                                           self.mip,
                                           inverse=False)
        chunk = mask_input_operator(chunk)

        print('run convnet inference...')
        with Inferencer(None, None, self.patch_size,
                        num_output_channels=3,
                        input_size=chunk.shape,
                        output_patch_overlap=self.patch_overlap,
                        framework='identity',
                        batch_size=5,
                        dtype='float32') as inferencer:
            print(inferencer.compute_device)
            chunk = inferencer(chunk)
        print('after inference: {}'.format(chunk.slices))

        print('crop the marging...')
        chunk = chunk.crop_margin(output_bbox=self.output_bbox)
        print('after crop: {}'.format(chunk.slices))

        print('mask the output...')
        mask_output_operator = MaskOperator(self.output_mask_volume_path,
                                            self.output_mask_mip,
                                            self.mip,
                                            inverse=False)
        chunk = mask_output_operator(chunk)
        print('after masking: {}'.format(chunk.slices))

        print('save to output volume...')
        save_operator = SaveOperator(self.output_volume_path,
                                     self.mip,
                                     upload_log=True,
                                     create_thumbnail=True)
        save_operator(chunk, log={'timer': {'save': 34}})
        print('after saving: {}'.format(chunk.slices))

        # evaluate the output
        print('start evaluation...')
        out = self.output_vol[self.output_bbox.to_slices()[::-1] +
                              (slice(0, 3), )]
        out = np.asarray(out)
        out = out[:, :, :, 0] * 255
        out = out.astype(np.uint8)
        out = np.transpose(out)

        # ignore the patch overlap around the border
        img = self.img[4:-4, 64:-64, 64:-64]

        # check that the masked region are all zero
        # input mask validation
        self.assertTrue(np.alltrue(out[:2, :8, :8] == 0))
        # output mask validation
        self.assertTrue(np.alltrue(out[-2:-8:, -8:] == 0))

        # ignore the masked part of output
        img = img[2:-2, 8:-8, 8:-8]
        out = out[2:-2, 8:-8, 8:-8]

        # the value can only be close since there is mask error
        self.assertTrue(np.alltrue(np.isclose(img, out, atol=1)))

        # clean up
        shutil.rmtree('/tmp/input')
        shutil.rmtree('/tmp/input-mask')
        shutil.rmtree('/tmp/output-mask')
        shutil.rmtree('/tmp/output')
