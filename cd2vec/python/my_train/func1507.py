@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'import-remove', 'import-gitlog')
    def test_cli_export_solution_035(snippy):
        """Export all solutions.

        Try to export content only from solution and reference categories. The
        solution category name is not correctly spelled which must faile the
        operation.
        """

        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solutions,reference'])
            assert cause == "NOK: content categories ('reference', 'solutions') are not a subset of ('snippet', 'solution', 'reference')"
            mock_file.assert_not_called()
            file_handle = mock_file.return_value.__enter__.return_value
            file_handle.write.assert_not_called()
