def test_factory():
    """
    test ontology factory using wikidata as source and using PTSD.

    """
    f = OntologyFactory()
    ont = f.create('wdq:Q544006')
    for n in ont.nodes():
        print('{} "{}"'.format(n,ont.label(n)))
    qids = ont.search('anxiety%')
    assert len(qids) > 0
    print(qids)
    nodes = ont.traverse_nodes(qids, up=True, down=True)
    print(nodes)
    assert len(nodes) > 0
    labels = [ont.label(n) for n in nodes]
    print(labels)
    # Note: it's possible wd may change rendering this false
    assert 'fear of frogs' in labels
    from ontobio.io.ontol_renderers import GraphRenderer
    w = GraphRenderer.create('tree')
    w.write(ont, query_ids=qids)

