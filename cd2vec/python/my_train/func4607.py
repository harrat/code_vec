def test_executemany_sequence():
    cu.execute("delete from tests")
    cu.executemany("insert into tests(id, name, real_field) values (?, ?, ?)", [
        (i + 1, 'test_executemany_sequence', 3.14 * i)
        for i in range(10)
    ])
    assert cu.rowcount == 10

