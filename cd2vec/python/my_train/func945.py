def test_report_json_pretty_blacklist():
    runner = CliRunner()
    with mock_context():
        result = runner.invoke(
            cli,
            [
                "--format",
                "json-pretty",
                "analysis",
                "report",
                "--swc-blacklist",
                "SWC-110",
                "ab9092f7-54d0-480f-9b63-1bb1508280e2",
            ],
        )
