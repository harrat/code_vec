def test_simple_signal_registration():

    @on_test.register
    def foo(a, b):
        a / b

    with pytest.raises(ZeroDivisionError):
        on_test(a=5, b=0, c='c')

    on_test.unregister(foo)
    on_test(a=5, b=0, c='c')

