def test_traffic(self):
        port = self.xm.session.reserve_ports([self.port1])[self.port1]
        port.load_config(path.join(path.dirname(__file__), 'configs', 'test_config_loopback.xpc'))

        self.xm.session.clear_stats()
        port_stats = port.read_port_stats()
        print(json.dumps(port_stats, indent=1))
        assert port_stats['pt_total']['packets'] == 0
        self.xm.session.start_traffic()
        time.sleep(1)
        stream_stats = port.streams[0].read_stats()
        print(json.dumps(stream_stats, indent=1))
        assert abs(1000 - stream_stats['pps']) < 111
        tpld_stats = port.tplds[1].read_stats()
        print(json.dumps(tpld_stats, indent=1))
        assert abs(1000 - tpld_stats['pr_tpldtraffic']['pps']) < 111
        self.xm.session.stop_traffic()

        # todo: add assertions

        ports_stats = XenaPortsStats(self.xm.session)
        ports_stats.read_stats()
        print(ports_stats.statistics.dumps())
        print(json.dumps(ports_stats.get_flat_stats(), indent=1))

        streams_stats = XenaStreamsStats(self.xm.session)
        streams_stats.read_stats()
        print(streams_stats.statistics.dumps())
        print(json.dumps(streams_stats.get_flat_stats(), indent=1))

        tplds_stats = XenaTpldsStats(self.xm.session)
        tplds_stats.read_stats()
        print(tplds_stats.statistics.dumps())
        print(json.dumps(tplds_stats.get_flat_stats(), indent=1))
