def test_set_middle2(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        audiofile.set_head_middle_tail(middle_length=TimeValue("10.000"))
        self.assertEqual(audiofile.all_length, 1331)
        self.assertEqual(audiofile.head_length, 0)
        self.assertEqual(audiofile.middle_length, 250)
        self.assertEqual(audiofile.tail_length, 1081)
