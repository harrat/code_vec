def test_server_error(self):
        api = TransferApi(auth=correct_auth, use_test_env=True)

        def should_raise():
            api.create_transfer('example.org', authCode='wrong_auth', purchasePrice=10.99)

        self.assertRaises(exceptions.ServerError, should_raise)
