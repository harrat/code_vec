@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'export-time')
    def test_cli_export_solution_025(snippy):
        """Export solutions with search keyword.

        Export defined solution based on search keyword. File name is defined
        in solution metadata and in command line -f|--file option. This should
        result the file name and format defined by the command line option. In
        this case the text format file extension is 'txt'.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Content.deepcopy(Solution.BEATS)
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '--sall', 'beats', '-f', './defined-solution.txt'])
            assert cause == Cause.ALL_OK
            Content.assert_text(mock_file, './defined-solution.txt', content)
