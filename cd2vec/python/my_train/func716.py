def test_insert(mi, potus_seq):
    assert [potus_seq[4]] == mi.get('first_name', 'Barack')
    assert [potus_seq[2]] == mi.get('assumed_ofc_at', 43)
    assert None is mi.get('assumed_ofc_at', 0)
    assert None is mi.get('assumed_ofc_at', 786)
    assert [potus_seq[3]] == mi.get('last_name', 'Bush')
    assert [potus_seq[5]] == mi.get('first_name', 'Franklin')
    assert [potus_seq[1]] == mi.get('first_name', 'Thomas')

    for potus in mi.get('first_name', 'George'):
        assert potus in [potus_seq[3], potus_seq[0]]

