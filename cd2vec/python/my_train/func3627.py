@mock.patch('requests.get', side_effect=mock_pyquery)
    @mock.patch('requests.head', side_effect=mock_request)
    def test_invalid_default_year_reverts_to_previous_year(self,
                                                           *args,
                                                           **kwargs):
        results = {
            'game': 2,
            'boxscore_index': '201610280NOP',
            'date': 'Fri, Oct 28, 2016',
            'time': '9:30p',
            'datetime': datetime(2016, 10, 28),
            'location': AWAY,
            'opponent_abbr': 'NOP',
            'opponent_name': 'New Orleans Pelicans',
            'result': WIN,
            'points_scored': 122,
            'points_allowed': 114,
            'wins': 1,
            'losses': 1,
            'streak': 'W 1'
        }
        flexmock(Boxscore) \
            .should_receive('_parse_game_data') \
            .and_return(None)
        flexmock(Boxscore) \
            .should_receive('dataframe') \
            .and_return(pd.DataFrame([{'key': 'value'}]))
        flexmock(utils) \
            .should_receive('_find_year_for_season') \
            .and_return(2018)

        schedule = Schedule('GSW')

        for attribute, value in results.items():
            assert getattr(schedule[1], attribute) == value
