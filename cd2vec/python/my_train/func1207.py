def test_thermostat_type_3_get_core_heating_days(thermostat_type_3):
    core_heating_day_sets = thermostat_type_3.get_core_heating_days(
            method="year_mid_to_mid")
    assert len(core_heating_day_sets) == 5

