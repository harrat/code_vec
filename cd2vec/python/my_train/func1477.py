@pytest.mark.parametrize(("atts", "expected"), REPR_ARGS)
def test_node_repr(atts, expected):
    """Test Node repr."""
    node = Node(atts)
    assert node.__repr__() == "Node(" + expected + ")"

@pytest.mark.parametrize(("atts", "expected"), REPR_ARGS)
def test_node_get_atts(atts, expected):
    """Test Node get_atts method."""
    node = Node(atts)
    if 'perms' not in atts:
        atts['perms'] = None
    if 'owner' not in atts:
        atts['owner'] = None
    if 'group' not in atts:
        atts['group'] = None
    assert node.get_atts(string=True) == expected
    assert node.get_atts(string=False) == atts
