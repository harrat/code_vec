def test_mock_queue_unique():
    assert mock_queue.qsize() == 0

    task = MockTask()
    task.unique = True
    task.uri = 'unique1'
    task2 = MockTask()
    task2.unique = True
    task2.uri = 'unique1'

    assert task.unique_hash() == task2.unique_hash()

    mock_queue.put(task)
    with pytest.raises(TaskAlreadyInQueueException):
        mock_queue.put(task2)

    task3 = MockTask()
    task3.unique = True
    task3.uri = 'unique2'
    mock_queue.put(task3)
    assert mock_queue.qsize() == 2

