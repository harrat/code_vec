def test_reserved_property_addresses(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['addresses'] = "Something"
        results = validate_parsed_json(indicator, self.options)
        self.assertEqual(results.is_valid, False)
