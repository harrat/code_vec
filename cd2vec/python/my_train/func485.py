def test_ds_joint_hist_smoke(ds_NDs):
    with seaborn.axes_style("ticks"):
        fig1 = _do_jointplots(ds_NDs, hist=True)
        return fig1
