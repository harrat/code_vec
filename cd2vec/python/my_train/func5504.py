def test_retrieve_clean_subcat():
    res = category_members.retrieve(
        'Category:Presidents_of_the_United_States', clean_subcat_names=True)
    assert all('Category:' not in x['name'] for x in res)

