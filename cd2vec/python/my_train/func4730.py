def test_pil_image(self):
        my_image_loader = ImgLoader()

        downloaded_pil = my_image_loader.get_image_as_pil('https://imgur.com/a/jpwwwq6')

        self.assertIsNotNone(downloaded_pil)
        self.assertEqual(800, downloaded_pil.width)
        self.assertEqual(600, downloaded_pil.height)
