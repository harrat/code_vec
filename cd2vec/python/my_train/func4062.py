@staticmethod
    @pytest.mark.usefixtures('yaml', 'default-references', 'export-time')
    def test_cli_export_reference_002(snippy):
        """Export all references.

        Export all references into yaml formatted file as  defined in command
        line option. In this case the reference category is explicitly defined.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-f', './defined-references.yaml', '--scat', 'reference'])
            assert cause == Cause.ALL_OK
            Content.assert_yaml(yaml, mock_file, './defined-references.yaml', content)
