def test_checkpath_symlink(run_reframe, tmp_path):
    # FIXME: This should move to test_loader.py
    checks_symlink = tmp_path / 'checks_symlink'
    os.symlink(os.path.abspath('unittests/resources/checks'),
               checks_symlink)

    returncode, stdout, _ = run_reframe(
        action='list',
        more_options=['-R'],
        checkpath=['unittests/resources/checks', str(checks_symlink)]
    )
    num_checks_default = re.search(
        r'Found (\d+) check', stdout, re.MULTILINE).group(1)
    num_checks_in_checkdir = re.search(
        r'Found (\d+) check', stdout, re.MULTILINE).group(1)
    assert num_checks_in_checkdir == num_checks_default

