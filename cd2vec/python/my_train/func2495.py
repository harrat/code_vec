def test_immutability(self):
        # Check emit_load_commands()
        _, commands = rt.loadenv(self.environ)

        # Try to modify the returned list of commands
        commands.append('foo')
        assert 'foo' not in rt.loadenv(self.environ)[1]

        # Test ProgEnvironment
        prgenv = env.ProgEnvironment('foo_prgenv')
        assert isinstance(prgenv, env.Environment)
        with pytest.raises(AttributeError):
            prgenv.cc = 'gcc'

        with pytest.raises(AttributeError):
            prgenv.cxx = 'g++'

        with pytest.raises(AttributeError):
            prgenv.ftn = 'gfortran'

        with pytest.raises(AttributeError):
            prgenv.nvcc = 'clang'

        with pytest.raises(AttributeError):
            prgenv.cppflags = ['-DFOO']

        with pytest.raises(AttributeError):
            prgenv.cflags = ['-O1']

        with pytest.raises(AttributeError):
            prgenv.cxxflags = ['-O1']

        with pytest.raises(AttributeError):
            prgenv.fflags = ['-O1']

        with pytest.raises(AttributeError):
            prgenv.ldflags = ['-lm']
