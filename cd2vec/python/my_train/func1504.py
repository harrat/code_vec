def test_epsilon():
    assert(abs(geom.eps - 0.0001) < 0.0001)
    geom.set_tolerance(0.01)
    assert(abs(geom.eps - 0.01) < 0.00001)
    with pytest.raises(TypeError):
        geom.set_tolerance('0.001')
    with pytest.raises(ValueError):
        geom.set_tolerance(0)
    with pytest.raises(ValueError):
        geom.set_tolerance(-1)
