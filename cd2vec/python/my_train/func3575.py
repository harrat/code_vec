def test_pattern_custom_invalid_format(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['pattern'] = """[ab:yz = 'something']"""
        self.assertFalseWithOptions(indicator)
