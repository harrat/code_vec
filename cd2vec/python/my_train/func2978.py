@pytest.mark.core
def testConfigSetConfPath(nwTemp):
    assert theConf.setConfPath(None)
    assert not theConf.setConfPath(path.join("somewhere","over","the","rainbow"))
    assert theConf.setConfPath(path.join(nwTemp,"novelwriter.conf"))
    assert theConf.confPath == nwTemp
    assert theConf.confFile == "novelwriter.conf"
    assert not theConf.confChanged

@pytest.mark.core
def testConfigSetDataPath(nwTemp):
    assert theConf.setDataPath(None)
    assert not theConf.setDataPath(path.join("somewhere","over","the","rainbow"))
    assert theConf.setDataPath(nwTemp)
    assert theConf.dataPath == nwTemp
    assert not theConf.confChanged
