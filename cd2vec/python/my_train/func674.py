@patch('stackify.transport.default.http.HTTPClient.send')
    def test_default_send_url(self, mock_send):
        config = {
            'application': 'test_appname',
            'environment': 'test_environment',
            'api_key': 'test_apikey',
            'api_url': 'test_apiurl',
        }

        transport = configure_transport(**config)
        message = transport.create_message(logging.makeLogRecord({'mgs': 'message'}))
        group_message = transport.create_group_message([message])
        transport.send(group_message)

        assert mock_send.called
        assert mock_send.call_args_list[0][0][0] == '/Log/Save'
