@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_update_solution_007(snippy):
        """Update solution with ``--digest`` option.

        Try to update solution with empty message digest. Nothing should be
        updated in this case because the empty digest matches to more than
        one solution. Only one content can be updated at the time.
        """

        content = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        cause = snippy.run(['snippy', 'update', '--scat', 'solution', '-d', ''])
        assert cause == 'NOK: cannot use empty message digest for update operation'
        Content.assert_storage(content)
