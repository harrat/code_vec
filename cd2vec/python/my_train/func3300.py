def test_backup_unmocked_commands(self):
        bupper.main(bupper.parse_args([
            '-s', self.source_dir,
            '-l', self.local_dir,
            '-r', self.remote_dir,
        ]))
        local_dir_contents = os.listdir(self.local_dir)
        remote_dir_contents = os.listdir(self.remote_dir)
        assert len(local_dir_contents) == 4
        assert len(remote_dir_contents) == 2
        local_fns = [fn for fn in local_dir_contents if fn.startswith('2017')]
        assert set(local_fns) == set(remote_dir_contents)
        path_1 = join(self.source_dir, 'example/path/to')
        path_2 = join(self.source_dir, 'other/backup')
        assert set(local_fns) == set([
            utils.get_backup_archive_filename('ISO', path_1),
            utils.get_backup_archive_filename('ISO', path_2),
        ])
