def test_parse_unicode_name_udf(tmpdir):
    indir = tmpdir.mkdir('unicodeudf')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'f�o'), 'wb') as outfp:
        outfp.write(b'foo\n')

    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-udf',
                     '-no-pad', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_unicode_name_udf)

def test_parse_unicode_name_two_byte(tmpdir):
    indir = tmpdir.mkdir('unicode')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'f?o'), 'wb') as outfp:
        outfp.write(b'foo\n')
