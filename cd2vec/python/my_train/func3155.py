def test_deposit(self):
        deposited_dict = {'result': 'ok'}
        deposit_dict = ac.deposit(asset="SWTH", amount=0.000001, private_key=kp)
        deposit_dict.pop('transaction_hash')
        self.assertDictEqual(deposit_dict, deposited_dict)
        deposit_dict = ac.deposit(asset="GAS", amount=0.000001, private_key=kp)
        deposit_dict.pop('transaction_hash')
        self.assertDictEqual(deposit_dict, deposited_dict)
