def test_ncaaf_kicker_returns_expected_kicking_stats(self):
        stats = {
            'extra_points_made': 91,
            'extra_points_attempted': 92,
            'extra_point_percentage': 98.9,
            'field_goals_made': 33,
            'field_goals_attempted': 45,
            'field_goal_percentage': 73.3,
        }

        player = Player('jd-dillinger-1')

        assert player.name == 'J.D. Dillinger'
        for attribute, value in stats.items():
            assert getattr(player, attribute) == value
