def test_stock_quote_fails_no_key(self, block_keys):
        with pytest.raises(IEXAuthenticationError):
            a = Stock("AAPL")
            a.get_quote()
