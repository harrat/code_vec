def test_is_converged(class_objects):
    d_source, d_class, s_width = class_objects
    obj = DrizzleSolving(d_source, d_class, s_width)
    ind = (1, 2)
    dia_init = np.array([[1, 3, 2], [3, 1, 2]])
    dia = 1
    compare = False
    assert obj._is_converged(ind, dia, dia_init) == compare

