@can_only_run_on_linux_64
    def test_vulnerable(self):
        # Given a server that is vulnerable to CCS injection
        with LegacyOpenSslServer() as server:
            server_location = ServerNetworkLocationViaDirectConnection(
                hostname=server.hostname, ip_address=server.ip_address, port=server.port
            )
            server_info = ServerConnectivityTester().perform(server_location)

            # When testing for CCS injection, it succeeds
            result = OpenSslCcsInjectionImplementation.scan_server(server_info)

        # And the server is reported as vulnerable
        assert result.is_vulnerable_to_ccs_injection

        # And a CLI output can be generated
        assert OpenSslCcsInjectionImplementation.cli_connector_cls.result_to_console_output(result)
