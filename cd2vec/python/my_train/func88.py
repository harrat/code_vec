def test_error_ex3():
    """test_error_ex3"""
    result = subprocess.run(["python3", "./GC_analysis/GC_analysis.py",
                             "-i", "./tests/ex3.fasta",
                             "-o", "./tests/ex3.fasta.wig",
                             "-w", "5",
                             "-s", "5"], stderr=subprocess.PIPE, stdout=subprocess.PIPE)

    assert result.stderr.split(b"\n")[-2] == b"TypeError"

    assert result.returncode == 1

    assert result.stdout[-28:] == b" contains no sequence data.\n"

