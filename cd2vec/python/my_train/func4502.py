def test_masterpwd(self):
        with self.assertRaises(Exception):
            InRamEncryptedKeyStore()
        config = InRamConfigurationStore()
        config["password_storage"] = "environment"
        keys = InRamEncryptedKeyStore(config=config)
        self.assertTrue(keys.locked())
        keys.unlock("foobar")
        keys.password = "FOoo"
        with self.assertRaises(Exception):
            keys._decrypt_masterpassword()
        keys.lock()

        with self.assertRaises(WrongMasterPasswordException):
            keys.unlock("foobar2")

        with self.assertRaises(Exception):
            keys._get_encrypted_masterpassword()

        self.assertFalse(keys.unlocked())

        os.environ["UNLOCK"] = "foobar"
        self.assertTrue(keys.unlocked())

        self.assertFalse(keys.locked())
