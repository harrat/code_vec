def test_overflow_eta_caching():
    eta._NOW = lambda: 1411868721.5
    progress_bar = ProgressBarWget(500000000000, eta_every=4)
    assert ' 0% [                  ] 0           --.-KiB/s              ' == str(progress_bar)

    eta._NOW = lambda: 1411868722.0
    progress_bar.numerator = 1000000
    assert ' 0% [                  ] 1,000,000   --.-KiB/s              ' == str(progress_bar)

    eta._NOW = lambda: 1411868722.5
    progress_bar.numerator = 3000000
    assert ' 0% [                  ] 3,000,000   --.-KiB/s              ' == str(progress_bar)

    eta._NOW = lambda: 1411868723.0
    progress_bar.numerator = 6000000
    assert ' 0% [                  ] 6,000,000   --.-KiB/s              ' == str(progress_bar)

    eta._NOW = lambda: 1411868723.5
    progress_bar.numerator = 13000000
    assert ' 0% [                  ] 13,000,000  13.4MiB/s  eta 17h 48m ' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 93000000
    assert ' 0% [                  ] 93,000,000   152MiB/s  eta 17h 48m ' == str(progress_bar)

    eta._NOW = lambda: 1411868724.5
    progress_bar.numerator = 193000000
    assert ' 0% [                  ] 193,000,000  190MiB/s  eta 17h 48m ' == str(progress_bar)

    eta._NOW = lambda: 1411868725.0
    progress_bar.numerator = 300000000
    assert ' 0% [                  ] 300,000,000  204MiB/s  eta 17h 48m ' == str(progress_bar)

    eta._NOW = lambda: 1411868725.5
    progress_bar.numerator = 700000000
    assert ' 0% [                  ] 700,000,000  762MiB/s  eta 49m 50s ' == str(progress_bar)

    eta._NOW = lambda: 1411868726.0
    progress_bar.numerator = 1400000000
    assert ' 0% [                ] 1,400,000,000 1.30GiB/s  eta 49m 50s ' == str(progress_bar)

    eta._NOW = lambda: 1411868726.5
    progress_bar.numerator = 2500000000
    assert ' 0% [                ] 2,500,000,000 2.05GiB/s  eta 49m 50s ' == str(progress_bar)

    eta._NOW = lambda: 1411868727.0
    progress_bar.numerator = 9999999999
    assert ' 1% [                ] 9,999,999,999 14.0GiB/s  eta 49m 50s ' == str(progress_bar)

    eta._NOW = lambda: 1411868727.5
    progress_bar.numerator = 10000000000
    assert ' 2% [               ] 10,000,000,000   2.00B/s  eta 5m 12s  ' == str(progress_bar)

    eta._NOW = lambda: 1411868728.0
    progress_bar.numerator = 10000000001
    assert ' 2% [               ] 10,000,000,001   2.00B/s  eta 5m 12s  ' == str(progress_bar)

    eta._NOW = lambda: 1411868728.5
    progress_bar.numerator = 10000000002
    assert ' 2% [               ] 10,000,000,002   2.00B/s  eta 5m 12s  ' == str(progress_bar)

    eta._NOW = lambda: 1411868729.0
    progress_bar.numerator = 100000000002
    assert '20% [=>            ] 100,000,000,002  167GiB/s  eta 5m 12s  ' == str(progress_bar)

    eta._NOW = lambda: 1411868729.0
    progress_bar.numerator = 400000000002
    assert '80% [==========>   ] 400,000,000,002  726GiB/s  eta 8s      ' == str(progress_bar)

    eta._NOW = lambda: 1411868729.0
    progress_bar.numerator = 500000000000
    assert '100%[=============>] 500,000,000,000 62.1GiB/s   in 8s      ' == str(progress_bar)

