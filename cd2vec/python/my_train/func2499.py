def test_get_dates():
    """Test to see that it returns the correct dates for given game id's"""
    assert json_schedule.get_dates([2015010002])[0] == {'game_id': 2015010002, 'date': '2015-09-20', 
                                                        'start_time': datetime.datetime(2015, 9, 20, 20, 30), 
                                                        'venue': 'Bridgestone Arena', 'home_team': 'NSH', 
                                                        'away_team': 'FLA', 'home_score': 5, 'away_score': 2, 
                                                        'status': 'Final'}
    assert json_schedule.get_dates([2017020275])[0] == {'game_id': 2017020275, 'date': '2017-11-15', 
                                                        'start_time': datetime.datetime(2017, 11, 16, 0, 30), 
                                                        'venue': 'Little Caesars Arena', 'home_team': 'DET', 
                                                        'away_team': 'CGY', 'home_score': 8, 'away_score': 2, 
                                                        'status': 'Final'}
    assert json_schedule.get_dates([2014030416])[0] == {'game_id': 2014030416, 'date': '2015-06-15', 
                                                        'start_time': datetime.datetime(2015, 6, 16, 0, 0), 
                                                        'venue': 'United Center', 'home_team': 'CHI', 
                                                        'away_team': 'T.B', 'home_score': 2, 'away_score': 0, 
                                                        'status': 'Final'}
