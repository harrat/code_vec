def test_plugin_hdfs_add():
    dict1 = si_app.get_data_source_dict(ds_name=hdfs_ds_dict['ds_name'])
    assert dict1 is None

    si_app.add_data_soruce(hdfs_ds_dict)
    dict1 = si_app.get_data_source_dict(ds_name=hdfs_ds_dict['ds_name'])
    assert dict1['ds_param']['hdfs_web_url'] ==  'http://localhost:50070'

