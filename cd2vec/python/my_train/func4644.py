@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'default-solutions', 'import-kafka', 'update-kafka-utc', 'export-time')
    def test_cli_export_solution_019(snippy):
        """Export defined solution with digest.

        Export defined solution based on message digest. Content file name is
        not defined in resource ``filename`` attribute or with command line
        option ``-f|--file``. The exported filename uses the default Markdown
        format and filename.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Content.deepcopy(Solution.KAFKA)
            ]
        }
        content['data'][0]['filename'] = Const.EMPTY
        content['data'][0]['digest'] = '0a2b29d2fde6b900375d68be93ac6142c5adafb27fb5d6294fab465090d82504'
        file_content = Content.get_file_content(Content.TEXT, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '-d', 'ee3f2ab7c63d6965', '-f', 'kafka.text'])
            assert cause == Cause.ALL_OK

        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '-d', '0a2b29d2fde6b900'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './solutions.mkdn', content)
