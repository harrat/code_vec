def test_getreferingobjs(self):
        """py.test for getreferingobjs"""
        thedata = (
            (
                """  Zone,
        Box,  !- Name
        0.0,  !- Direction of Relative North {deg}
        0.288184,  !- X Origin {m}
        0.756604,  !- Y Origin {m}
        0.0,  !- Z Origin {m}
        ,  !- Type
        1;  !- Multiplier

      BuildingSurface:Detailed,
        N_Wall,  !- Name
        Wall,  !- Surface Type
        Exterior Wall,  !- Construction Name
        Box,  !- Zone Name
        Outdoors,  !- Outside Boundary Condition
        ,  !- Outside Boundary Condition Object
        SunExposed,  !- Sun Exposure
        WindExposed,  !- Wind Exposure
        ,  !- View Factor to Ground
        1,  !- Number of Vertices
        5.000000000000,  !- Vertex 1 X-coordinate {m}
        6.000000000000,  !- Vertex 1 Y-coordinate {m}
        3.000000000000;  !- Vertex 1 Z-coordinate {m}

      WALL:EXTERIOR,
          WallExterior,                    !- Name
          ,                         !- Construction Name
          Box,                         !- Zone Name
          ,                         !- Azimuth Angle
          90;                       !- Tilt Angle

        BUILDINGSURFACE:DETAILED,
            EWall,                    !- Name
            ,                         !- Surface Type
            ,                         !- Construction Name
            BOX,                         !- Zone Name
            OtherBox,                         !- Outside Boundary Condition
            ,                         !- Outside Boundary Condition Object
            SunExposed,               !- Sun Exposure
            WindExposed,              !- Wind Exposure
            autocalculate,            !- View Factor to Ground
            autocalculate;            !- Number of Vertices

        BUILDINGSURFACE:DETAILED,
            EWall1,                    !- Name
            ,                         !- Surface Type
            ,                         !- Construction Name
            BOX_other,                         !- Zone Name
            OtherBox,                         !- Outside Boundary Condition
            ,                         !- Outside Boundary Condition Object
            SunExposed,               !- Sun Exposure
            WindExposed,              !- Wind Exposure
            autocalculate,            !- View Factor to Ground
            autocalculate;            !- Number of Vertices
      HVACTemplate:Thermostat,
        Constant Setpoint Thermostat,  !- Name
        ,                        !- Heating Setpoint Schedule Name
        20,                      !- Constant Heating Setpoint {C}
        ,                        !- Cooling Setpoint Schedule Name
        25;                      !- Constant Cooling Setpoint {C}

    FENESTRATIONSURFACE:DETAILED,
        Window1,                  !- Name
        ,                         !- Surface Type
        ,                         !- Construction Name
        EWall1,                         !- Building Surface Name
        ,                         !- Outside Boundary Condition Object
        autocalculate,            !- View Factor to Ground
        ,                         !- Shading Control Name
        ,                         !- Frame and Divider Name
        1.0,                      !- Multiplier
        autocalculate;            !- Number of Vertices
      """,
                "Box",
                ["N_Wall", "EWall", "WallExterior"],
            ),  # idftxt, zname, surfnamelst
        )
        for idftxt, zname, surfnamelst in thedata:
            # import pdb; pdb.set_trace()
            idf = IDF(StringIO(idftxt))
            zone = idf.getobject("zone", zname)
            kwargs = {}
            result = zone.getreferingobjs(**kwargs)
            rnames = [item.Name for item in result]
            rnames.sort()
            surfnamelst.sort()
            assert rnames == surfnamelst
        for idftxt, zname, surfnamelst in thedata:
            idf = IDF(StringIO(idftxt))
            zone = idf.getobject("zone", zname)
            kwargs = {"iddgroups": ["Thermal Zones and Surfaces"]}
            result = zone.getreferingobjs(**kwargs)
            rnames = [item.Name for item in result]
            rnames.sort()
            surfnamelst.sort()
            assert rnames == surfnamelst
        for idftxt, zname, surfnamelst in thedata:
            idf = IDF(StringIO(idftxt))
            zone = idf.getobject("zone", zname)
            kwargs = {"fields": ["Zone_Name"]}
            result = zone.getreferingobjs(**kwargs)
            rnames = [item.Name for item in result]
            rnames.sort()
            surfnamelst.sort()
            assert rnames == surfnamelst
        for idftxt, zname, surfnamelst in thedata:
            idf = IDF(StringIO(idftxt))
            zone = idf.getobject("zone", zname)
            kwargs = {
                "fields": ["Zone_Name"],
                "iddgroups": ["Thermal Zones and Surfaces"],
            }
            result = zone.getreferingobjs(**kwargs)
            rnames = [item.Name for item in result]
            rnames.sort()
            surfnamelst.sort()
            assert rnames == surfnamelst
        # use the above idftxt and try other to get other references.
        for idftxt, zname, surfnamelst in thedata:
            idf = IDF(StringIO(idftxt))
            wname = "EWall1"
            windownamelist = ["Window1"]
            wall = idf.getobject("BUILDINGSURFACE:DETAILED", wname)
            kwargs = {
                "fields": ["Building_Surface_Name"],
                "iddgroups": ["Thermal Zones and Surfaces"],
            }
            result = wall.getreferingobjs(**kwargs)
            rnames = [item.Name for item in result]
            rnames.sort()
            surfnamelst.sort()
            assert rnames == windownamelist
