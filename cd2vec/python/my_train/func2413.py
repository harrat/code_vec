def test_mobile_sender_required(self):
        """
        Check to make sure user has a noreply mobile address is set.
        Even if you have suppression on, you must provide some kind of sender number if mobile is selected.
        """
        api_settings.PASSWORDLESS_AUTH_TYPES = ['MOBILE']
        mobile_response = self.client.post(self.mobile_url, self.mobile_data)
        self.assertEqual(mobile_response.status_code, status.HTTP_400_BAD_REQUEST)
