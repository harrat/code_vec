def test_parse_udf_overflow_dir_extent(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('udfoverflow')
    outfile = str(indir)+'.iso'
    numdirs = 46
    for i in range(1, 1+numdirs):
        indir.mkdir('dir%d' % i)
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-udf', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_udf_overflow_dir_extent)

def test_parse_multi_hard_link(tmpdir):
    indir = tmpdir.mkdir('jolietudfnofiles')
    outfile = str(indir)+'.iso'
