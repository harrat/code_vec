def test_disable_encoding(self):
        # Explicitly set the accept-encoding to gzip
        self.client.set_header_overrides({
            'Accept-Encoding': 'gzip'
        })

        self._make_request('https://www.google.com/')

        requests = self.client.get_requests()

        # No Content-Encoding header implies 'identity'
        self.assertEqual(
            'identity',
            requests[0]['response']['headers'].get('Content-Encoding', 'identity')
        )
