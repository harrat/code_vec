def test_MVN_MLE_baseline():
    """
    Test if the max. likelihood estimates of a multivariate normal distribution
    are sufficiently accurate in the baseline case with no truncation and no
    censoring.

    """
    # univariate case
    ref_mean = 0.5
    ref_std = 0.25
    ref_var = ref_std ** 2.

    # generate samples
    samples = tmvn_rvs(ref_mean, ref_var,
                       size=1000)

    # estimate the parameters of the distribution
    mu, var = tmvn_MLE(samples)

    assert ref_mean == pytest.approx(mu, abs=0.05)
    assert ref_var == pytest.approx(var, rel=0.2)

    # multi-dimensional case
    dims = 3
    ref_mean = np.arange(dims, dtype=np.float64)
    ref_std = np.ones(dims) * 0.5
    ref_rho = np.ones((dims, dims)) * 0.5
    np.fill_diagonal(ref_rho, 1.0)
    ref_COV = np.outer(ref_std, ref_std) * ref_rho

    samples = tmvn_rvs(ref_mean, ref_COV, size=100)

    test_mu, test_COV = tmvn_MLE(np.transpose(samples))
    test_std = np.sqrt(test_COV.diagonal())
    test_rho = test_COV/np.outer(test_std,test_std)

    assert_allclose(test_mu, ref_mean, atol=0.3)
    assert_allclose(test_std**2., ref_std**2., rtol=0.5)
    assert_allclose(test_rho, ref_rho, atol=0.3)

