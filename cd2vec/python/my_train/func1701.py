def test_signal_weakref_context_manager_delete_during_with_identifier():
    import gc

    result = []

    class Foo:
        @contextmanager
        def on_ctx_test_identifier(self, before, after):
            result.append(before)
            yield
            result.append(after)

    foo = Foo()
    foo.obj = 'obj'
    register_object(foo)

    foo2 = Foo()
    foo2.obj = 'otherobj'
    register_object(foo2)

    with on_ctx_test_identifier(before=1, after=2, obj='obj'):
        assert result == [1]
    assert result == [1, 2]

    with on_ctx_test_identifier(before=3, after=4, obj='obj'):
        assert result == [1, 2, 3]
        del foo
        del foo2
        gc.collect()
    # The context manager should keep the signal handler alive
    assert result == [1, 2, 3, 4]

