@responses.activate
    @patch("time.sleep", return_value=None)
    def test_unlike_user(self, patched_time_sleep):
        unliked_at_start = self.bot.total["unlikes"]
        user_id = 1234567890
        response_data = {"status": "ok", "user": TEST_USERNAME_INFO_ITEM}
        responses.add(
            responses.GET,
            "{api_url}users/{username}/usernameinfo/".format(
                api_url=API_URL, username=user_id
            ),
            status=200,
            json=response_data,
        )
        my_test_photo_items = []
        results = 5
        for i in range(results):
            my_test_photo_items.append(TEST_PHOTO_ITEM.copy())
            my_test_photo_items[i]["pk"] = TEST_PHOTO_ITEM["id"] + i
            if i % 2:
                my_test_photo_items[i]["has_liked"] = False
            else:
                my_test_photo_items[i]["has_liked"] = True
        response_data = {
            "auto_load_more_enabled": True,
            "num_results": results,
            "status": "ok",
            "more_available": False,
            "items": my_test_photo_items,
        }
        responses.add(
            responses.GET,
            "{api_url}feed/user/{user_id}/".format(api_url=API_URL, user_id=user_id),
            json=response_data,
            status=200,
        )
        for my_test_photo_item in my_test_photo_items:
            responses.add(
                responses.POST,
                "{api_url}media/{media_id}/unlike/".format(
                    api_url=API_URL, media_id=my_test_photo_item["id"]
                ),
                json="{'status': 'ok'}",
                status=200,
            )
        broken_items = self.bot.unlike_user(user_id)
        test_unliked = self.bot.total["unlikes"] == unliked_at_start + len(
            my_test_photo_items
        )
        test_broken = broken_items == []
        assert test_broken and test_unliked
