def test_parse_udf_onefileonedir(tmpdir):
    indir = tmpdir.mkdir('udfonefileonedir')
    outfile = str(indir)+'.iso'
    indir.mkdir('dir1')
    with open(os.path.join(str(indir), 'foo'), 'wb') as outfp:
        outfp.write(b'foo\n')

    subprocess.call(['genisoimage', '-v', '-v', '-no-pad', '-iso-level', '1',
                     '-udf', '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_udf_onefileonedir)

def test_parse_udf_dir_spillover(tmpdir):
    indir = tmpdir.mkdir('udfdirspillover')
    outfile = str(indir)+'.iso'
    for i in range(ord('a'), ord('v')):
        dirname = chr(i) * 64
        indir.mkdir(dirname)
