def test_attribute_types(valid_player):

    assert isinstance(valid_player.exp, int)

    assert isinstance(valid_player.skill_values, list)

    assert isinstance(valid_player.exists, bool)

    assert isinstance(valid_player.title, str)

