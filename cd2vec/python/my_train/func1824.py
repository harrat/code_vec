def test_requesting_pending_resources(self):
        b = BatchProcessor('auth', 'secret', '12345')

        with mock.patch.object(b.client, 'get') as get_method:
            get_method.return_value = Response(200, {
                "00934509022": {
                    "HOLDING": [
                        "07128945000132"
                    ],
                    "LOJA": [
                        "111"
                    ],
                    "PRODUTO": [
                        "1"
                    ],
                    "SECAO": [
                        "xxxx"
                    ]
                },
                "02541926375": {
                    "LOJA": [
                        "111",
                        "2345"
                    ],
                    "PRODUTO": [
                        "1"
                    ],
                    "SECAO": [
                        "xxxx"
                    ]
                }
            })
