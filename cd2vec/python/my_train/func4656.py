def test_plain_output(caplog, capsys):
    logger_name = log.current_logger_name()
    assert logger_name == "tests.test_logx"
    FIRST_MESSAGE = "first message"
    log.info(FIRST_MESSAGE)
    assert m(caplog) == FIRST_MESSAGE
    out, err = capsys.readouterr()
    assert out == FIRST_MESSAGE + "\n"
    assert not err

