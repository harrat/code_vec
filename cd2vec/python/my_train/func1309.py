def test_tag_render():
    p = DB.posts.get(doc_id=1)
    entry = Entry.entry_from_db(
        os.path.join(CONFIG['content_root'], p.get('filename')), 1)

    tags = entry.tags

    assert list(map(str, tags)) == ['buf', 'foo', 'bar', 'baz']
    # the entries are wrongly sorted, need to look at that
    assert tags[0].render()
    assert len(list(tags[0].entries))

    assert len(DB.posts.all()) == 22

