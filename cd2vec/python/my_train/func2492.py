def test_version_pep386_release(self):
        runner('git tag -a 1.0.0 -m "test message"')
        v = relic.release.get_info()
        assert v.pep386 == '1.0.0'
        assert not v.dirty
        assert v.post == '0'
