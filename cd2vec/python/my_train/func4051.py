def test_defined_rounded():
    progress_bar = ProgressBarBytes(1023)

    assert '  0% (    0/1,023 B) [               ] eta --:-- /' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 1022
    assert ' 99% (1,022/1,023 B) [############## ] eta --:-- -' == str(progress_bar)

    eta._NOW = lambda: 1411868724.5
    progress_bar.numerator = 1023
    assert '100% (1,023/1,023 B) [###############] eta --:-- \\' == str(progress_bar)
    assert '100% (1,023/1,023 B) [###############] eta --:-- |' == str(progress_bar)

