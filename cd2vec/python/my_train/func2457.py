def test_increment_micro(self):
        os.environ["RELEASE_TYPE"] = "micro"
        v1 = VersionUtils.increment(self.v1)
        v2 = VersionUtils.increment(self.v2)
        v3 = VersionUtils.increment(self.v3)
        v4 = VersionUtils.increment(self.v4)
        v5 = VersionUtils.increment(self.v5)
        v6 = VersionUtils.increment(self.v6)
        self.assertEqual(v1, "1!1.2.4")
        self.assertEqual(v2, "1.2.4")
        self.assertEqual(v3, "1.2.3")
        self.assertEqual(v4, "1.2.1")
        self.assertEqual(v5, "2014.0.1")
        self.assertEqual(v6, "2.1.4")
