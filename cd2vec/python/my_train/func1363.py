def test_config_object_saves_config_file(self):
    """test_config_object_saves_config_file ensures config objs are being written to file"""
    result = self.func.save_config_to_file(
        {"Username": "test@me.com", "Token": "password"},
        ".oss-index-config")
    self.assertEqual(result, True)
