@mark.django_db
def test_detail(client_with_perms):
    client = client_with_perms(
        "testapp.view_dragonfly", "testapp.change_dragonfly", "testapp.delete_dragonfly"
    )
    alpha = Dragonfly.objects.create(name="alpha", age=47)
    Sighting.objects.create(name="Berlin", dragonfly=alpha)
    Sighting.objects.create(name="Paris", dragonfly=alpha)

    links = DragonflyViewSet().links

    response_content = client.get(links["detail"].reverse(alpha)).content.decode(
        "utf-8"
    )

    assert "alpha" in response_content
    assert "Title of sightings"
    assert "Berlin" in response_content
    assert "Paris" in response_content

    assert 'href="{}"'.format(links["list"].reverse(alpha)) in response_content
    assert 'href="{}"'.format(links["update"].reverse(alpha)) in response_content
    assert 'href="{}"'.format(links["delete"].reverse(alpha)) in response_content

