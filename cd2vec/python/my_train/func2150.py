def test_tee_fileoutput_binary(self):
        file1 = self.root.make_file(suffix='.gz')
        file2 = self.root.make_file()
        with byteoutput(
                (file1, file2),
                file_output_type=TeeFileOutput) as o:
            o.writelines((b'foo', b'bar', b'baz'))
        with gzip.open(file1, 'rb') as i:
            assert b'foo\nbar\nbaz\n' == i.read()
        with open(file2, 'rb') as i:
            assert b'foo\nbar\nbaz\n' == i.read()

        with textoutput((file1, file2), file_output_type=TeeFileOutput) as o:
            o.writelines((b'foo', b'bar', b'baz'))
        with gzip.open(file1, 'rt') as i:
            assert 'foo\nbar\nbaz\n' == i.read()
        with open(file2, 'rt') as i:
            assert 'foo\nbar\nbaz\n' == i.read()

        with byteoutput((file1, file2), file_output_type=TeeFileOutput) as o:
            o.writelines(('foo', b'bar', b'baz'))
        with gzip.open(file1, 'rb') as i:
            assert b'foo\nbar\nbaz\n' == i.read()
        with open(file2, 'rb') as i:
            assert b'foo\nbar\nbaz\n' == i.read()
