@staticmethod
    @pytest.mark.usefixtures('yaml', 'default-references', 'export-time')
    def test_cli_export_reference_005(snippy):
        """Export all references.

        Export defined reference based on message digest. File name is defined
        in command line as yaml file.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Reference.REGEXP
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-d', 'cb9225a81eab8ced', '-f', 'defined-reference.yml'])
            assert cause == Cause.ALL_OK
            Content.assert_yaml(yaml, mock_file, 'defined-reference.yml', content)
