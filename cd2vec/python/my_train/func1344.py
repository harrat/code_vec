def test_endpoints(self):
        """Help detect changes in endpoints.
        Get comparison text with
        $ python lds_org.py | grep http | sed -e 's/^[ \t]*//' | sort
        """
        lds = lds_org.LDSOrg()
        assert len(lds.endpoints) > 20
        assert len(list(iter(lds))) > 20
        urls = sorted(v for v in lds.endpoints.values()
                      if v.startswith('http'))
        static = sorted('''https://lds.qualtrics.com/SE/?SID=SV_esOCWpQ67JUc6na
https://play.google.com/store/apps/details?id=org.lds.ldstools
https://signin.lds.org/login.html
https://wsmobile1.lds.org/CP/CalendarProxyService/v1/Event/{}
https://wsmobile1.lds.org/CP/CalendarProxyService/v1/Events/{}-{}
https://wsmobile1.lds.org/CP/CalendarProxyService/v1/Locations?blah=adm/appVersion
https://wsmobile1.lds.org/CP/CalendarProxyService/v1/Subscribed
https://www.lds.org
https://www.lds.org/callings/melchizedek-priesthood/records-and-technology-support/lds-tools-release-notifications?lang=eng#android
https://www.lds.org/callings/melchizedek-priesthood/records-and-technology-support/lds-tools-release-notifications?lang=eng#ios
https://www.lds.org/mls/mbr/services/recommend/endowed-members?unitNumber={unit}&lang=eng
https://www.lds.org/mls/mbr/services/report/action-interview-list/full/unit/{unit}/?lang=eng
https://www.lds.org/mls/mbr/services/report/action-interview-list/unit/{unit}/?lang=eng
https://www.lds.org/mls/mbr/services/report/birthday-list/unit/{unit}/?month=1&months=12&organization=selectAll&lang=eng
https://www.lds.org/mls/mbr/services/report/membership-record/{member}?lang=eng
https://www.lds.org/mls/mbr/services/report/membership-records?unitNumber={unit}&lang=eng
https://www.lds.org/mls/mbr/services/report/members-moved-in/unit/{unit}/{}?lang=eng
https://www.lds.org/mls/mbr/services/report/members-with-callings?unitNumber={unit}&lang=eng
https://www.lds.org/mls/mbr/services/report/unit-statistics?unitNumber={unit}&lang=eng
https://www.lds.org/mobilecalendar/heartbeat
https://www.lds.org/mobilecalendar/services/lucrs/cal/allColors
https://www.lds.org/mobilecalendar/services/lucrs/cal/{}/color/{}/
https://www.lds.org/mobilecalendar/services/lucrs/cal/subscribed
https://www.lds.org/mobilecalendar/services/lucrs/evt/{}
https://www.lds.org/mobilecalendar/services/lucrs/evt/calendar/{}-{}
https://www.lds.org/mobilecalendar/services/lucrs/loc/locations
https://www.lds.org/mobilecalendar/services/lucrs/mem/currentUserOptions/{}
https://www.lds.org/mobiledirectory/heartbeat
https://www.lds.org/mobiledirectory/services/ludrs/1.1/mem/mobile/current-user-id
https://www.lds.org/mobiledirectory/services/ludrs/1.1/mem/mobile/current-user-id
https://www.lds.org/mobiledirectory/services/ludrs/1.1/mem/mobile/current-user-unitNo
https://www.lds.org/mobiledirectory/services/ludrs/1.1/mem/mobile/member-assignments
https://www.lds.org/mobiledirectory/services/ludrs/1.1/mem/mobile/member-detaillist/{unit}
https://www.lds.org/mobiledirectory/services/ludrs/1.1/mem/mobile/member-detaillist-with-callings/{unit}
https://www.lds.org/mobiledirectory/services/ludrs/1.1/photo/url/{member}/{}
https://www.lds.org/mobiledirectory/services/ludrs/1.1/unit/mobile/current-user-units
https://www.lds.org/mobiledirectory/services/ludrs/1.1/unit/unit-leadershiplist/{unit}
https://www.lds.org/mobiledirectory/services/ludrs/1.1/unit/unit-leadershiplist/{unit}
https://www.lds.org/mobiledirectory/services/ludrs/unit/current-user-stake-wards
https://www.lds.org/mobiledirectory/services/ludrs/unit/stake-leadership-positions
https://www.lds.org/mobiledirectory/services/v2/ldstools/current-user-detail
https://www.lds.org/mobiledirectory/services/v2/ldstools/member-detaillist-with-callings/{unit}
http://tech.lds.org/mobile/ldstools/leader-access-1.0.json
http://tech.lds.org/mobile/ldstools/lists.json
http://www.ldsmobile.org/lt-ios-help/?feed=rss2
http://www.lds.org/callings/melchizedek-priesthood/records-and-technology-support/lds-tools-faq?lang=eng
http://www.lds.org/callings/melchizedek-priesthood/records-and-technology-support/lds-tools-faq?lang=eng
http://www.lds.org/legal/privacy?lang=eng
http://www.lds.org/legal/terms?lang=eng
http://www.lds.org/signinout/?lang=eng&signmeout'''.splitlines())
        assert urls == static
