def test_runall_skip_prgenv_check(make_runner, make_cases, common_exec_ctx):
    runner = make_runner()
    runner.runall(make_cases(skip_environ_check=True))
    stats = runner.stats
    assert 9 == stats.num_cases()
    assert_runall(runner)
    assert 5 == len(stats.failures())
    assert 2 == num_failures_stage(runner, 'setup')
    assert 1 == num_failures_stage(runner, 'sanity')
    assert 1 == num_failures_stage(runner, 'performance')
    assert 1 == num_failures_stage(runner, 'cleanup')

