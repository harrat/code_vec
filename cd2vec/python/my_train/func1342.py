def test_wrapUnWrapPrecision(self):
        """
        Wrap and unwrap should have no rounding errors at least up to
        a precision value of 12.
        Rounding errors seem to start occuring at a precision value of 14.
        """
        for p in range(5, 13):
            for i in range(1000):
                self._wrapUnWrap(p)
        with self.assertRaisesRegex(
                FontMathWarning,
                "Matrix round-tripping failed for precision value"):
            for p in range(14, 16):
                for i in range(1000):
                    self._wrapUnWrap(p)
