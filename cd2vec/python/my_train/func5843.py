def test_run_continuous(selector: Type[MockSelector], proc: ContinuousSSH, caplog: Any, popen: Type[MockPopen]) -> None:
    selector.events = [selectors.EVENT_READ] * 3
    popen._stdout = b"Entering interactive session\ndebug1: Your server is not responding\ndone"
    proc._run_once()

    assert list(get_transitions(proc)) == ["connecting", "connected", "disconnected"]

