def test_ambiguous_element_in_sequence_explicit_using_attribute(self):
        """Test that writing a sequence with an ambiguous element
        as explicit transfer syntax works if accessing the tag via keyword."""
        # regression test for #804
        ds = self.dataset_with_modality_lut_sequence(pixel_repr=0)
        ds.is_implicit_VR = False
        fp = BytesIO()
        ds.save_as(fp, write_like_original=True)
        ds = dcmread(fp, force=True)
        assert 'US' == ds.ModalityLUTSequence[0][0x00283002].VR

        ds = self.dataset_with_modality_lut_sequence(pixel_repr=1)
        ds.is_implicit_VR = False
        fp = BytesIO()
        ds.save_as(fp, write_like_original=True)
        ds = dcmread(fp, force=True)
        assert 'SS' == ds.ModalityLUTSequence[0][0x00283002].VR
