def test_quit_exception():
    @core.system()
    def quit_timeout(delta_t, data):
        data["tick"] += 1
        if data["tick"] == 4:
            raise core.Quit

    controller = core.ECSController()
    controller.register_system(quit_timeout)
    controller["tick"] = 0

    controller.run()
    assert controller["tick"] == 4

