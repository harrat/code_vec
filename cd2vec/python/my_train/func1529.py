@pytest.mark.vcr()
def test_workbench_asset_vuln_output(api):
    assets = api.workbenches.assets()
    outputs = api.workbenches.asset_vuln_output(assets[0]['id'], 19506)
    assert isinstance(outputs, list)
    o = outputs[0]
    check(o, 'plugin_output', str)
    check(o, 'states', list)
    for i in o['states']:
        check(i, 'name', str)
        check(i, 'results', list)
        for j in i['results']:
            check(j, 'application_protocol', str, allow_none=True)
            check(j, 'assets', list)
            for k in j['assets']:
                check(k, 'first_seen', 'datetime')
                check(k, 'fqdn', str, allow_none=True)
                check(k, 'hostname', str)
                check(k, 'id', 'uuid')
                check(k, 'ipv4', str, allow_none=True)
                check(k, 'last_seen', 'datetime')
                check(k, 'netbios_name', str, allow_none=True)
                check(k, 'uuid', 'uuid')
            check(j, 'port', int)
            check(j, 'severity', int)
            check(j, 'transport_protocol', str)

@pytest.mark.vcr()
def test_workbench_vuln_assets(api):
    assets = api.workbenches.vuln_assets()
    assert isinstance(assets, list)
    a = assets[0]
    check(a, 'agent_name', list)
    check(a, 'fqdn', list)
    check(a, 'id', 'uuid')
    check(a, 'ipv4', list)
    check(a, 'ipv6', list)
    check(a, 'last_seen', 'datetime')
    check(a, 'netbios_name', list)
    check(a, 'severities', list)
    for i in a['severities']:
        check(i, 'count', int)
        check(i, 'level', int)
        check(i, 'name', str)
    check(a, 'total', int)
