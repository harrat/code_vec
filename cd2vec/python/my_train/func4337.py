@pytest.mark.vcr()
def test_workbench_vulns(api):
    vulns = api.workbenches.vulns()
    assert isinstance(vulns, list)
    v = vulns[0]
    check(v, 'accepted_count', int)
    check(v, 'counts_by_severity', list)
    for i in v['counts_by_severity']:
        check(i, 'count', int)
        check(i, 'value', int)
    check(v, 'plugin_family', str)
    check(v, 'plugin_id', int)
    check(v, 'plugin_name', str)
    check(v, 'recasted_count', int)
    check(v, 'vulnerability_state', str)

@pytest.mark.vcr()
def test_workbench_vuln_info_age_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vuln_info(19506, age='none')
