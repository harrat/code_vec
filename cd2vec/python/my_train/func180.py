@staticmethod
    @pytest.mark.usefixtures('json', 'default-solutions', 'export-time')
    def test_cli_export_solution_003(snippy):
        """Export all solutions.

        Export all solutions into defined json file. File name and format are
        defined in command line.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '-f', './all-solutions.json'])
            assert cause == Cause.ALL_OK
            Content.assert_json(json, mock_file, './all-solutions.json', content)
