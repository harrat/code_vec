@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'default-solutions-utc')
    def test_cli_import_solution_009(snippy):
        """Import all solution resources.

        Import all solutions from txt file without specifying the solution
        category. File name and format are extracted from command line option
        ``--file``. File extension is '*.text' in this case.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.BEATS),
                Content.deepcopy(Solution.NGINX)
            ]
        }
        content['data'][0]['uuid'] = Content.UUID1
        content['data'][1]['uuid'] = Content.UUID2
        file_content = Content.get_file_content(Content.TEXT, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '-f', './all-solutions.text'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './all-solutions.text', mode='r', encoding='utf-8')
