def test_get_data_by_time(self):
        '''
        Extract column(s) of data between given starting and ending days and times
        '''
        path = os.path.join(os.path.dirname(__file__), '.', 'data')

        data_day1 = pd.read_csv(path + '/datalog 6-14-2018.xls', delimiter='\t')
        data_day1 = np.round([pd.to_numeric(data_day1.iloc[:, 0]), pd.to_numeric(data_day1.iloc[:, 4])], 5)
        data_day1 = [data_day1[0].tolist(), data_day1[1].tolist()]

        data_day2 = pd.read_csv(path + '/datalog 6-15-2018.xls', delimiter='\t')
        data_day2 = np.round([pd.to_numeric(data_day2.iloc[:, 0]), pd.to_numeric(data_day2.iloc[:, 4])], 5)
        data_day2 = [data_day2[0].tolist(), data_day2[1].tolist()]
        data_day2[0][0] = 0  # to remove scientific notation "e-"

        # SINGLE COLUMN, ONE DAY
        output = get_data_by_time(path=path, columns=0, dates="6-14-2018", start_time="12:20", end_time="13:00", extension=".xls")
        self.assertSequenceEqual(np.round(output, 5).tolist(), data_day1[0][1041:1282])

        # SINGLE COLUMN, TWO DAYS
        output = get_data_by_time(path=path, columns=0, dates=["6-14-2018", "6-15-2018"],
                                  start_time="12:20", end_time="10:50", extension=".xls")
        time_column = data_day1[0][1041:] + np.round(np.array(data_day2[0][:3901])+1, 5).tolist()
        self.assertSequenceEqual(np.round(output, 5).tolist(), time_column)

        # MULTI COLUMN, ONE DAY
        output = get_data_by_time(path=path, columns=[0, 4], dates=["6-14-2018"], start_time="12:20",
                                  end_time="13:00", extension=".xls")
        self.assertSequenceEqual(np.round(output[0], 5).tolist(), data_day1[0][1041:1282])
        self.assertSequenceEqual(np.round(output[1], 5).tolist(), data_day1[1][1041:1282])

        # MULTI COLUMN, TWO DAYS
        output = get_data_by_time(path=path, columns=[0, 4], dates=["6-14-2018", "6-15-2018"],
                                  start_time="12:20", end_time="10:50", extension=".xls")
        time_column = data_day1[0][1041:] + np.round(np.array(data_day2[0][:3901])+1, 5).tolist()
        self.assertSequenceEqual(np.round(output[0], 5).tolist(), time_column)
        self.assertSequenceEqual(np.round(output[1], 5).tolist(), data_day1[1][1041:]+data_day2[1][:3901])
