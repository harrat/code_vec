def test_models_multiclass():
    """Assert that the fit method works with all models for multiclass."""
    for model in [m for m in MODEL_NAMES if m not in ONLY_REGRESSION]:
        if model == 'CatB':
            continue  # CatBoost fails with a weird error
        atom = ATOMClassifier(X_class2, y_class2, test_size=0.24, random_state=1)
        atom.run(models=model,
                 metric='f1_micro',
                 n_calls=2,
                 n_random_starts=1,
                 bo_params={'base_estimator': 'rf', 'cv': 1})
        assert not atom.errors
        assert hasattr(atom, model) and hasattr(atom, model.lower())

