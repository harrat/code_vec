def test_success(self, client):
        response = client.get(self.url)
        assert response.status_code == 200, response.content.decode('utf-8')
        assert response['content-type'] == 'text/html; charset=utf-8'
