@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_update_reference_008(snippy):
        """Update reference with ``uuid`` option.

        Try to update reference based on uuid that cannot be found.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        cause = snippy.run(['snippy', 'update', '--scat', 'reference', '-u', '9999994'])
        assert cause == 'NOK: cannot find content with content uuid: 9999994'
        Content.assert_storage(content)
