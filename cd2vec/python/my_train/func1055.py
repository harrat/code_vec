def test_temp_file_name_delete(tmpdir):
    """Test wkr.os.temp_file_name where we manually delete the tmpfile."""
    with temp_file_name(directory=tmpdir.strpath) as newpath:
        # write to the file
        with open(newpath, 'wb') as output_file:
            output_file.write(b'abcde')
        # delete the file
        os.remove(newpath)
    # should not raise an exception
