@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_api_search_reference_001(server):
        """Search reference with GET.

        Send GET /references and search keywords from all attributes. The
        search query matches to two references and both of them are returned.
        The search is sorted based on one attribute. The search result limit
        defined in the search query is not exceeded.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '1312'
        }
        expect_body = {
            'meta': {
                'count': 2,
                'limit': 20,
                'offset': 0,
                'total': 2
            },
            'data': [{
                'type': 'reference',
                'id': Reference.GITLOG_UUID,
                'attributes': Storage.gitlog
            }, {
                'type': 'reference',
                'id': Reference.REGEXP_UUID,
                'attributes': Reference.REGEXP
            }]
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/references',
            headers={'accept': 'application/vnd.api+json; charset=UTF-8'},
            query_string='sall=commit%2Cregular&limit=20&sort=brief')
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
