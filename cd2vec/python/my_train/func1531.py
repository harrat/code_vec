def test_md5():
    with patch('sys.stdout', new=StringIO()) as fake_out:
        tb = ThreatButt()
        tb.bespoke_md5('d41d8cd98f00b204e9800998ecf8427e')
        assert len(fake_out.getvalue())
