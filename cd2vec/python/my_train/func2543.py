@vcr.use_cassette("tests/vcr_cassettes/test_user_password_fails.yaml")
@patch("getpass.getpass")
def test_user_input_password_found(getpass, capsys):
    getpass.return_value = "test"
    with pytest.raises(SystemExit):
        import keepassxc_pwned.__main__
    captured = capsys.readouterr()
    captured_lines = captured.out.splitlines()
    assert len(captured_lines) == 1
    assert captured_lines[0] == "Found password 76479 times!"

