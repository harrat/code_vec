@pytest.mark.parametrize('threshold, expected_length', [
    # threshold of zero should just return the full probe dataframe
    (0.0, 58692),
    # default threshold
    (0.5, 38176),
    # threshold of one should NOT return an empty dataframe (that's useless)
    # this will return all the probes that are greater than background noise
    # across ALL samples from ALL provided donors
    (1.0, 11878),
    # threshold is clipped to [0, 1] so these should be identical to [0, 1]
    (-1.0, 58692),
    (2.0, 11878),
])
def test_filter_probes(testfiles, threshold, expected_length):
    # set up a few useful variables
    pacall = flatten_dict(testfiles, 'pacall')
    probe_file = first_entry(testfiles, 'probes')
    samples = flatten_dict(testfiles, 'annotation')
    probe_df = abagen.io.read_probes(probe_file)

    # should work with either a filename _or_ a dataframe
    filtered = probes_.filter_probes(pacall, samples, probe_file,
                                     threshold=threshold)
    pd.testing.assert_frame_equal(
        filtered,
        probes_.filter_probes(pacall, samples, probe_df, threshold=threshold)
    )

    # provided threshold returns expected output
    cols = [
        'probe_name', 'gene_id', 'gene_symbol', 'gene_name', 'entrez_id',
        'chromosome'
    ]
    assert np.all(filtered.columns == cols)
    assert filtered.index.name == 'probe_id'
    assert len(filtered) == expected_length

