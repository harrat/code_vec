def test_release_new_item_no_write():
    r = Release("Ensembl Release 85", "Release 85 of Ensembl", "XXX", edition_of_wdid='Q1344256')
    try:
        r.get_or_create()
    except ValueError as e:
        assert "login required to create item" == str(e)
