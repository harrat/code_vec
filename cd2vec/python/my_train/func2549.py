def test_topics_dict(self):
        offset_update_tuple = "topic1.23=1000"
        expected_topics_dict = {
            "topic1": {23: 1000},
        }
        OffsetSet.topics_dict(offset_update_tuple)
        assert OffsetSet.new_offsets_dict == expected_topics_dict
