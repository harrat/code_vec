def test_indicator_no_pattern_type(self):
        indicator = copy.deepcopy(self.valid_indicator)
        del indicator["pattern_type"]

        self.assertFalseWithOptions(indicator)
