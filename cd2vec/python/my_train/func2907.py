def test_version_post_incremented_after_commit(self):
        runner('git tag -a 1.0.0 -m "test message"')
        touch('testfile3')
        runner('git add testfile3')
        runner('git commit -m "add testfile"')
        v = relic.release.get_info()
        assert isinstance(v.post, str)
        post = int(v.post)
        assert post > 0
