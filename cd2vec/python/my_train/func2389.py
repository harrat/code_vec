@pytest.mark.vcr()
def test_agents_list_filter_type_typeerror(api):
    with pytest.raises(TypeError):
        api.agents.list(filter_type=1)
