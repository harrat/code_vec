def test_destroy_proxy(self):
        self.client.destroy_proxy()

        with self.assertRaises(urllib.error.URLError):
            self._make_request('http://github.com')
