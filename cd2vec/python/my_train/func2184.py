@staticmethod
    @pytest.mark.usefixtures('yaml')
    def test_cli_import_solution_029(snippy):
        """Import all content defaults.

        Import snippet, solution and reference defaults.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Solution.NGINX,
                Reference.GITLOG
            ]
        }
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '--scat', 'all', '--defaults'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            defaults = []
            defaults.append(mock.call(pkg_resources.resource_filename('snippy', 'data/defaults/snippets.yaml'), encoding='utf-8', mode='r'))
            defaults.append(mock.call(pkg_resources.resource_filename('snippy', 'data/defaults/solutions.yaml'), encoding='utf-8', mode='r'))
            defaults.append(mock.call(pkg_resources.resource_filename('snippy', 'data/defaults/references.yaml'), encoding='utf-8', mode='r'))
            for default in defaults:
                assert default in mock_file.call_args_list
