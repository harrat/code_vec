def test_reference_default(dummytest, sanity_file,
                           perf_file, dummy_gpu_exec_ctx):
    sanity_file.write_text('result = success\n')
    perf_file.write_text('perf1 = 1.3\n'
                         'perf2 = 1.8\n'
                         'perf3 = 3.3\n')
    dummytest.reference = {
        '*': {
            'value1': (1.4, -0.1, 0.1, None),
            'value2': (1.7, -0.1, 0.1, None),
            'value3': (3.1, -0.1, 0.1, None),
        }
    }
    _run_sanity(dummytest, *dummy_gpu_exec_ctx)

