def test_lines_read(text_file, random_lines):
    """Test the wkr.lines method on reading text files."""
    encoding = re.match(r'.+\.([^.]+)\.txt$', text_file).group(1)
    # read with string decoding
    expected_output = [line + u'\n' for line in random_lines]
    read_lines = list(wkr.lines(text_file, encoding))
    assert all(isinstance(x, text_type) for x in read_lines)
    assert read_lines == expected_output
    # try reading without string decoding
    expected_output = list(
        BytesIO((u'\n'.join(random_lines) + u'\n').encode(encoding)))
    read_lines = list(wkr.lines(text_file, None))
    assert all(isinstance(x, binary_type) for x in read_lines)
    assert read_lines == expected_output

