def test_observable_object_reserved_property(self):
        observed_data = copy.deepcopy(self.valid_observed_data)
        observed_data['objects']['0']['type'] = 'action'
        self.assertFalseWithOptions(observed_data)

        observed_data['objects']['0']['type'] = 'file'
        observed_data['objects']['0']['action'] = True
        self.assertFalseWithOptions(observed_data)
