def test_new_reference_image_markdown_data(self):
        md_file = MdUtils(file_name="Test_file", title="")
        expected_image_1 = '![image_1][reference]'
        expected_image_2 = '![image_2]'
        image_1 = md_file.new_reference_image(text='image_1', path='../image.png', reference_tag="reference")
        image_2 = md_file.new_reference_image(text='image_2', path='../image_2.png')

        expected_created_data = "\n\n\n" \
                                "  \n{}".format(expected_image_1) + \
                                "  \n{}".format(expected_image_2) + \
                                "\n\n\n" \
                                "[image_2]: ../image_2.png\n" \
                                "[reference]: ../image.png\n"

        md_file.new_line(image_1)
        md_file.new_line(image_2)
        md_file.create_md_file()

        actual_created_data = MarkDownFile.read_file("Test_file")

        self.assertEqual(expected_created_data, actual_created_data)
