def test_save_load(self):
        for backend in get_backends(PpoAgent):
            ppo = agents.PpoAgent(gym_env_name=_step_count_name, backend=backend)
            ppo.train([duration._SingleEpisode()], default_plots=False)
            temp_dir = ppo.save()
            ppo = agents.load(temp_dir)
            ppo.play(default_plots=False, num_episodes=1, callbacks=[])
            easyagents.backends.core._rmpath(temp_dir)
