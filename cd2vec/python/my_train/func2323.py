def test_custom_loglevels(self):
        self.logger_without_check.info('foo')
        self.logger_without_check.verbose('bar')

        assert os.path.exists(self.logfile)
        assert self.found_in_logfile('info')
        assert self.found_in_logfile('verbose')
        assert self.found_in_logfile('reframe')
