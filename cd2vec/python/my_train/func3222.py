@staticmethod
    @pytest.mark.usefixtures('snippy', 'create-beats-utc')
    def test_cli_create_solution_001(snippy):
        """Create solution from CLI.

        Create new solution by defining all content parameters from command
        line. Creating solution uses editor by default only if the data field
        is not defined. In this case editor is not used.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.BEATS)
            ]
        }
        content['data'][0]['description'] = ''
        content['data'][0]['filename'] = ''
        content['data'][0]['uuid'] = Content.UUID1
        content['data'][0]['digest'] = '509c7e11b568283ae985ff038cbabbe94e9cea8b058215b243ae0463b0497e5c'
        data = Const.DELIMITER_DATA.join(content['data'][0]['data'])
        brief = content['data'][0]['brief']
        groups = Const.DELIMITER_GROUPS.join(content['data'][0]['groups'])
        tags = Const.DELIMITER_TAGS.join(content['data'][0]['tags'])
        links = Const.DELIMITER_LINKS.join(content['data'][0]['links'])
        cause = snippy.run(['snippy', 'create', '--scat', 'solution', '--content', data, '--brief', brief, '--groups', groups, '--tags', tags, '--links', links, '--format', 'text'])  # pylint: disable=line-too-long
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
