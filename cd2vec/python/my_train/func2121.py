def test__on_node_create(self):
        with self.graph:
            noop1 = self.graph.create_node('NoOp')
            self.assertEqual(noop1.name, 'NoOp1')
