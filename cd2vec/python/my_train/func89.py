def test_defined_hour():
    progress_bar = ProgressBar(2000)

    assert '  0% (    0/2,000) [       ] eta --:-- /' == str(progress_bar)

    eta._NOW = lambda: 1411868722.0
    progress_bar.numerator = 1
    assert '  0% (    1/2,000) [       ] eta --:-- -' == str(progress_bar)

    eta._NOW = lambda: 1411868724.0
    progress_bar.numerator = 2
    assert '  0% (    2/2,000) [     ] eta 1:06:36 \\' == str(progress_bar)

