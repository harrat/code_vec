def test_concurrency_limited(async_runner, make_cases, make_async_exec_ctx):
    # The number of checks must be <= 2*max_jobs.
    num_checks, max_jobs = 5, 3
    ctx = make_async_exec_ctx(max_jobs)
    next(ctx)

    runner, monitor = async_runner
    runner.runall(make_cases([SleepCheck(.5) for i in range(num_checks)]))

    # Ensure that all tests were run and without failures.
    assert num_checks == runner.stats.num_cases()
    assert_runall(runner)
    assert 0 == len(runner.stats.failures())

    # Ensure that maximum concurrency was reached as fast as possible
    assert max_jobs == max(monitor.num_tasks)
    assert max_jobs == monitor.num_tasks[max_jobs]

    begin_stamps, end_stamps = _read_timestamps(monitor.tasks)

    # Ensure that the jobs after the first #max_jobs were each run after
    # one of the previous #max_jobs jobs had finished
    # (e.g. begin[max_jobs] > end[0]).
    # Note: we may ensure this strictly as we may ensure serial behaviour.
    begin_after_end = (b > e for b, e in zip(begin_stamps[max_jobs:],
                                             end_stamps[:-max_jobs]))
    assert all(begin_after_end)

    # NOTE: to ensure that these remaining jobs were also run
    # in parallel one could do the command hereafter; however, it would
    # require to substantially increase the sleep time (in SleepCheck),
    # because of the delays in rescheduling (1s, 2s, 3s, 1s, 2s,...).
    # We currently prefer not to do this last concurrency test to avoid an
    # important prolongation of the unit test execution time.
    # self.assertTrue(self.begin_stamps[-1] < self.end_stamps[max_jobs])

    # Warn if the first #max_jobs jobs were not run in parallel; the
    # corresponding strict check would be:
    # self.assertTrue(self.begin_stamps[max_jobs-1] <= self.end_stamps[0])
    if begin_stamps[max_jobs-1] > end_stamps[0]:
        pytest.skip('the system seems too loaded.')

