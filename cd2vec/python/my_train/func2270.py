def test_disassociate_policy_and_file(self):
        """
        Test that the PolicyDirectoryMonitor can correctly unlink a policy and
        a policy file in its tracking structures.
        """
        m = monitor.PolicyDirectoryMonitor(
            self.tmp_dir,
            multiprocessing.Manager().dict()
        )

        m.policy_cache = {
            "policy_A": [
                (
                    1480043060.870089,
                    os.path.join(self.tmp_dir, "policy_1.json"),
                    {}
                ),
                (
                    1480043062.02171,
                    os.path.join(self.tmp_dir, "policy_2.json"),
                    {}
                ),
                (
                    1480043062.645776,
                    os.path.join(self.tmp_dir, "policy_1.json"),
                    {}
                ),
                (
                    1480043063.453713,
                    os.path.join(self.tmp_dir, "policy_3.json"),
                    {}
                )
            ],
            "policy_B": [
                (
                    1480043123.65311,
                    os.path.join(self.tmp_dir, "policy_1.json"),
                    {}
                )
            ]
        }

        m.disassociate_policy_and_file(
            "policy_A",
            os.path.join(self.tmp_dir, "policy_1.json")
        )

        self.assertEqual(
            [
                (
                    1480043062.02171,
                    os.path.join(self.tmp_dir, "policy_2.json"),
                    {}
                ),
                (
                    1480043063.453713,
                    os.path.join(self.tmp_dir, "policy_3.json"),
                    {}
                )
            ],
            m.policy_cache.get("policy_A", [])
        )
        self.assertEqual(
            [
                (
                    1480043123.65311,
                    os.path.join(self.tmp_dir, "policy_1.json"),
                    {}
                )
            ],
            m.policy_cache.get("policy_B", [])
        )
