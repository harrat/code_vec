@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_search_snippet_058(snippy, capsys):
        """Search snippets with ``--sall`` option.

        Search snippets with a string that contains multiple words. The search
        criteria ``docker.ps`` must match to only snippet.
        """

        output = (
            '1. Remove all docker containers with volumes @docker [54e41e9b52a02b63]',
            '',
            '   $ docker rm --volumes $(docker ps --all --quiet)',
            '',
            '   # cleanup,container,docker,docker-ce,moby',
            '   > https://docs.docker.com/engine/reference/commandline/rm/',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', 'docker.ps', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
