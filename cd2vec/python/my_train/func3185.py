def test_single_fileoutput(self):
        file1 = self.root.make_file(suffix='.gz')
        with textoutput(file1) as o:
            o.writelines(('foo', 'bar', 'baz'))
        with gzip.open(file1, 'rt') as i:
            assert 'foo\nbar\nbaz\n' == i.read()
