def test_resolve_with_multiple_AAAA_records(self):
        r = SecureDNS(query_type='AAAA')
        answers = r.resolve('www.mit.edu')
        assert len(answers) == 2
