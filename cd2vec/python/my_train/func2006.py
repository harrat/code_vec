def test_simple_performance(self):
        template = '''
        #if @test:
            test is true
        #else:
            test is false
        #end
        #block Person:
        name: @person.name
        #end
        #for @person in @people:
            #Person
        #end'''
        namespace = {
            'test': True,
            'people': [
                {'name': 'Iris'},
                {'name': 'Sasha'}]}
        compiled = TRender(template)
        start = time.time()
        for _ in range(10 ** 4):
            compiled.render(namespace)
        finished = time.time() - start
        # This should be easy below 1.0, even on slower computers
        self.assertLess(finished, 1.0)
