def test_missing_agent(self):

        self.jc = JolokiaClient('http://google.com', 'myuser', 'mypasswd')
        kwargs = {'mbean': 'java.lang:Memory', 'attribute': 'HeapMemoryUsage'}
        resp = self.jc.get_attribute(**kwargs)

        assert resp.status_code != 200
