def test_nbless_cli_out(tmp_path: Path) -> None:
    """Run nbless to create and execute a notebook with a custom filename."""
    runner = CliRunner()
    with runner.isolated_filesystem():
        files = make_files(tmp_path)
        args = files + ["-o", "blessed.ipynb"]
        runner.invoke(nbless_cli.nbless_cli, args)
        cells = nbformat.read("blessed.ipynb", as_version=4).cells
        assert [c.cell_type for c in cells] == ["markdown", "code", "markdown"]
        for cell, tempfile in zip(cells, files):
            assert cell.source == Path(tempfile).read_text()
