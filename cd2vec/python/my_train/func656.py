def test_get_param_overrides(self):
        self.client.set_param_overrides({'foo': 'bar'})

        self.assertEqual({'foo': 'bar'}, self.client.get_param_overrides())
