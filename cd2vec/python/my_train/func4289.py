@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_search_solution_003(snippy, capsys):
        """Search solutions with ``filter`` option.

        Search all content with regexp filter.
        """

        output = (
            '1. Debugging Elastic Beats @beats [4346ba4c79247430]',
            Const.NEWLINE.join(Solution.BEATS_OUTPUT),
            '   :',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--scat', 'solution', '--sall', '.', '--filter', '.*(\\$.*filebeat)', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
