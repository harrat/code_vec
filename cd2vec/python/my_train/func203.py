@mark.django_db
def test_list_search(client_with_perms):
    client = client_with_perms("testapp.view_dragonfly")
    Dragonfly.objects.create(name="alpha", age=12)
    Dragonfly.objects.create(name="omega", age=99)
    response = client.get(DragonflyViewSet().links["list"].reverse() + "?q=alpha")
    assert b"alpha" in response.content
    assert b"omega" not in response.content

