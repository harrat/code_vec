def test_radex_grid_run():
    """Ensure that the output of RADEX grid run is correct."""
    ds = ndradex.run('co.dat', ['1-0', '2-1', '3-2'],
                     [100, 200, 300], **COMMONS)
    assert np.isclose(ds['I'].sel(QN_ul='1-0', T_kin=100), 1.36)
    assert np.isclose(ds['F'].sel(QN_ul='1-0', T_kin=100), 2.684e-8)

