def test_invalid_type_starting_character_in_instance(self):
        new_object = copy.deepcopy(self.valid_custom_object)
        new_object['type'] = 'X-example-com-customobject'
        new_object['id'] = new_object['type'] + '--' + new_object['id'].split('--')[1]

        self.assertFalseWithOptions(new_object)
