def test_demean():
    """Runs the high-dimensional case.
    """
    p = 2
    n = 13
    sigma = np.eye(p, p)
    data = np.random.multivariate_normal(np.zeros(p), sigma, n)

    sigma_tilde = nls.shrink_cov(data)
    S = np.sum(sigma_tilde[np.eye(p) == 0]) / n_choose_k(p, 2) / np.sum(np.diag(sigma_tilde)) * p
    assert S < 1  # assert that the diagonal is the major contributor

