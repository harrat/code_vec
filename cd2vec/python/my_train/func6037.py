@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'import-kafka', 'import-pytest')
    def test_api_search_groups_003(server):
        """Get unique content based on ``groups`` attribute.

        Send GET /groups to get unique groups. In this case the ``limit``
        query parameter is set to zero. The limit parameter does not have
        any effect in the groups API endpoint.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '82'
        }
        expect_body = {
            'data': {
                'type': 'groups',
                'attributes': {
                    'groups': {
                        'docker': 3,
                        'python': 1
                    }
                }
            }
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/groups',
            headers={'accept': 'application/vnd.api+json'},
            query_string='limit=0')
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
