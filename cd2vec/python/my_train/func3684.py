def test_get_scopes(self):
        self.client.set_scopes(('.*stackoverflow.*', '.*github.*'))

        self.assertEqual(['.*stackoverflow.*', '.*github.*'], self.client.get_scopes())
