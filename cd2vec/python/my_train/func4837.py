def test_CalculateMedian(create_analyze_pair_file_in_directory):

    mean = misc.CalculateMedian()

    with pytest.raises(TypeError):
        mean.run()

    mean.inputs.in_files = example_data("ds003_sub-01_mc.nii.gz")
    eg = mean.run()

    assert os.path.exists(eg.outputs.median_files)
    assert nb.load(eg.outputs.median_files)

