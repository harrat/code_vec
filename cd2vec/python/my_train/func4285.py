def test_get_attr_derivative(self, tank_type):
        """Test setter for class Derivative"""
        attr_name = "Initial_temperature_of_node_1"
        assert tank_type.derivatives[attr_name].value.m == 50.0
