def test_pysal_holed_polygon(self):
        # West Modesto CDP, Polygon with Holes
        geodata = self.conn.mapservice.query(layer=36, where="PLACE=84578")
        numpy.testing.assert_allclose(
            geodata.total_bounds,
            numpy.array([-13476102.0034, 4523871.6742, -13471164.8727, 4527488.4349]),
        )
