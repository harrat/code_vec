def test_tell_skopt():
    s_opt = SkoptOptimizer()
    n_samples = 9
    s_opt.initialize(['a', 'b', 'c'], a=[0, 1], b=[0, 2], c=[0, 3],
                     popsize=n_samples, rounds=2)

    params = s_opt.ask(n_samples)

    errors = np.random.rand(n_samples)
    s_opt.tell(params, errors)
    assert_equal(s_opt.optim.Xi, params)
    assert_equal(s_opt.optim.yi, errors)

