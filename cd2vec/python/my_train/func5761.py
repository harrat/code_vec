def test_middle_begin_good3(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        audiofile.middle_begin = 1331
        self.assertEqual(audiofile.all_length, 1331)
        self.assertEqual(audiofile.head_length, 1331)
        self.assertEqual(audiofile.middle_length, 0)
        self.assertEqual(audiofile.tail_length, 0)
