def test_update(query):
    """
    Test update method.
    """

    # no name set (and column not a Series)
    column = 1
    with pytest.raises(ValueError):
        query.update(column)

    with pytest.raises(ValueError):
        query.update('dummystring', name='F0')

    # add an additional column using a numpy array
    newcol = np.ones(query.catalogue_len)
    newname = "TEST1"
    numcols = len(query.columns)

    assert newname not in query.columns

    query.update(newcol, name=newname)

    assert newname in query.columns
    assert len(query.columns) == (numcols + 1)
    assert np.all(query.catalogue[newname] == 1.)

    # add an additional columns using a Series
    numcols = len(query.columns)
    newname = "TEST2"
    newseries = Series(np.full(query.catalogue_len, 0.5), name=newname)

    assert newname not in query.columns

    query.update(newseries)

    assert newname in query.columns
    assert len(query.columns) == (numcols + 1)
    assert np.all(query.catalogue[newname] == 0.5)

    # make sure only NaNs get updated
    newname = "TEST3"
    newseries = Series(np.full(query.catalogue_len, np.nan), name=newname)
    newseries[0] = 1.
    query.update(newseries)

    assert query.catalogue[newname][0] == 1
    assert np.all(np.isnan(query.catalogue[newname][1:]))

    newseries = np.full(query.catalogue_len, 2.)
    query.update(newseries, name=newname)

    assert query.catalogue[newname][0] == 1
    assert np.all(query.catalogue[newname][1:] == 2.)

    # check that overwrite works
    query.update(newseries, name=newname, overwrite=True)

    assert np.all(query.catalogue[newname] == 2.)

