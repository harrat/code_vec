def test(self):
        r1 = ruler.OrRule([
            {
                "name": "bye"
            },
            {
                "file_size": "> 1024b"
            }
        ])

        r2 = ruler.OrRule({
            "name": "bye",
            "file_size": "> 1024b"
        })

        r3 = ruler.OrRule({
            "name": "bye",
            "file_size": "< 1024b"
        })

        self.assertTrue(r1.match("/tmp/nihao"))
        self.assertTrue(r2.match("/tmp/nihao"))
        self.assertFalse(r3.match("/tmp/nihao"))
