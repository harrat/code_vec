@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_api_search_solution_017(server):
        """Search solution with GET.

        Search solutions and accept gzip compressed response. Note that the
        response length cannot be checked because the compression efficiency
        can change between Python versions. because of this, the response
        lenght is tested only that it should be less than the uncompressed
        response.
        """

        expect_body = {
            'meta': {
                'count': 2,
                'limit': 20,
                'offset': 0,
                'total': 2
            },
            'data': [{
                'type': 'solution',
                'id': Solution.BEATS_UUID,
                'attributes': Storage.ebeats
            }, {
                'type': 'solution',
                'id': Solution.NGINX_UUID,
                'attributes': Storage.dnginx
            }]
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/solutions',
            headers={'accept': 'application/vnd.api+json; charset=UTF-8', 'accept-encoding': 'gzip'},
            query_string='sall=nginx%2CElastic&limit=20&sort=brief')
        result_body = zlib.decompress(result.content, 16+zlib.MAX_WBITS).decode("utf-8")
        assert result.status == falcon.HTTP_200
        assert result.headers['content-type'] == 'application/vnd.api+json; charset=UTF-8'
        assert result.headers['content-encoding'] == 'gzip'
        assert int(result.headers['content-length']) < len(result_body)
        Content.assert_restapi(json.loads(result_body), expect_body)
