def test_execute_delete(self):
        sqs_mock = MagicMock()
        callback_mock = MagicMock(return_value=True)

        with patch("boto3.client", return_value=sqs_mock) as _:
            _execute({}, self._get_queue_url(),
                     callback_mock, None, self._get_responses(1)["Messages"], True)

        callback_mock.assert_called_once_with(self._get_body(0))

        sqs_mock.delete_message.assert_called_once_with(
            QueueUrl=self._get_queue_url(), ReceiptHandle=self._get_receipt_handle(0)
        )
