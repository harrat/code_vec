def test_exceptions():
    global cx
    assert cx.Warning == py2jdbc.Warning
    assert cx.Error == py2jdbc.Error
    assert cx.InterfaceError == py2jdbc.InterfaceError
    assert cx.DatabaseError == py2jdbc.DatabaseError
    assert cx.DataError == py2jdbc.DataError
    assert cx.OperationalError == py2jdbc.OperationalError
    assert cx.IntegrityError == py2jdbc.IntegrityError
    assert cx.InternalError == py2jdbc.InternalError
    assert cx.ProgrammingError == py2jdbc.ProgrammingError
    assert cx.NotSupportedError == py2jdbc.NotSupportedError

