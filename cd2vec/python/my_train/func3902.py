def test_deco_timeout(cache_obj):
    '''Test custom decorator timeout'''
    c.clear()

    # Cache the result, which should use a 2s timeout, as opposed to the
    # default of 1s.
    tstart = time.time()
    cache_obj.test2()

    # Wait up to 5s for the GC thread to clear `test2()`.
    while len(c) and ((time.time() - tstart) < 5):
        time.sleep(0.1)  # Give GC a chance to run

    tend = time.time()

    # The defined timeout is 2, and gc_thread_wait is 0.5, so the max we
    # should really be waiting is 2.5 (ish).  The mininum is 2-ish.
    # NOTE: I've had a hell of time with duration variance on Travis-CI, which
    # is why the range is so big.
    time_diff = tend - tstart
    print('actual time: %s' % time_diff)
    assert 1.4 < time_diff < 3.5

