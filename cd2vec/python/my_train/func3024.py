@staticmethod
    def test_process_script__sanity_package():
        """
        Given
            - An invalid "script" file located at invalid_file_structures where commonfields object is not a dict.

        When
            - parsing script files

        Then
            - an exception will be raised
        """
        test_file_path = os.path.join(TESTS_DIR, 'test_files',
                                      'Packs', 'DummyPack', 'Scripts', 'DummyScript')
        res = process_script(test_file_path, True)
        assert len(res) == 1
        data = res[0]

        assert IsEqualFunctions.is_lists_equal(list(data.keys()), list(TestScripts.PACK_SCRIPT_DATA.keys()))

        const_data = TestScripts.PACK_SCRIPT_DATA.get('DummyScript')
        returned_data = data.get('DummyScript')

        assert IsEqualFunctions.is_dicts_equal(returned_data, const_data)
