def test_path_validation(capsys, rm_test_path):
    """Test path validation with auto construction."""
    p = ConfigPath(TEST_PATH)
    p.validate()
    captured = capsys.readouterr()
    assert captured.out == \
        "The path '/tmp/test/foo' is required by your configuration.\n"
