@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_delete_reference_005(snippy):
        """Delete reference with uuid.

        Try to delete content with empty uuid string.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        cause = snippy.run(['snippy', 'delete', '-u', ''])
        assert cause == 'NOK: cannot use empty content uuid for delete operation'
        Content.assert_storage(content)
