def test_framework_config():
    configini = '%s/config.ini' % TESTSDIR
    with open(configini) as f:
        tests_configini = f.read()
    configfile = framework_config()
    with open(configfile) as f:
        prod_configini = f.read()
    assert tests_configini == prod_configini

