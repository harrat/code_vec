@responses.activate
def test_cli_with_allow_codes(runner, valid_urls):
    reset_globals()
    urls = (
        ('http://www.test1.com', 200),
        ('http://www.test3.com', 500),
        ('http://www.test2.com', 404),
    )
    for url, code in urls:
        responses.add(responses.HEAD, url, status=code)

    with runner.isolated_filesystem():
        with open('valid.md', 'w') as f:
            f.write(valid_urls)
