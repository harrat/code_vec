def test_since_and_until_parameters_limit_dates_to_boundaries(cli, entries_file):
    entries_file.write("""20/01/2014
alias_1 1 Invisible entry

21/01/2014
alias_1 1 Visible entry

22/01/2014
alias_1 1 Visible entry

23/01/2014
alias_1 1 Invisible entry
""")
    stdout = cli('status', ['--since=21.01.2014', '--until=22.01.2014'])

    assert 'Invisible entry' not in stdout

