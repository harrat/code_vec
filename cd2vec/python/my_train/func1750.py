def test_away_two_point_field_goal_attempts_calc(self):
        fake_none = PropertyMock(return_value=None)
        fake_int = PropertyMock(return_value=5)

        type(self.boxscore)._away_field_goal_attempts = fake_none
        type(self.boxscore)._away_three_point_field_goal_attempts = fake_none

        assert self.boxscore.away_two_point_field_goal_attempts is None

        type(self.boxscore)._away_three_point_field_goal_attempts = fake_int
        assert self.boxscore.away_two_point_field_goal_attempts is None

        type(self.boxscore)._away_field_goal_attempts = fake_int
        type(self.boxscore)._away_three_point_field_goal_attempts = fake_none

        assert self.boxscore.away_two_point_field_goal_attempts is None

        type(self.boxscore)._away_field_goal_attempts = fake_int
        type(self.boxscore)._away_three_point_field_goal_attempts = fake_int

        assert isinstance(
            self.boxscore.away_two_point_field_goal_attempts, int)
