@staticmethod
    @pytest.mark.usefixtures('import-forced', 'update-remove-utc')
    def test_api_update_snippet_007(server):
        """Update one snippet with PATCH request.

        Send PATCH /snippets/{id} to update existing resource with specified
        digest. The PATCH request contains only mandatory the data attribute.
        All other attributes must be returned with their previous stored values.
        """

        storage = {
            'data': [
                Storage.forced
            ]
        }
        storage['data'][0]['data'] = Snippet.REMOVE['data']
        storage['data'][0]['created'] = Content.FORCED_TIME
        storage['data'][0]['updated'] = Content.REMOVE_TIME
        storage['data'][0]['uuid'] = Snippet.FORCED_UUID
        storage['data'][0]['digest'] = 'a9e137c08aee09852797a974ef91b871c48915fecf25b2e89c5bdba4885b2bd2'
        request_body = {
            'data': {
                'type': 'snippet',
                'attributes': {
                    'data': Snippet.REMOVE['data']
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '903'
        }
        expect_body = {
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/snippets/' + Snippet.FORCED_UUID
            },
            'data': {
                'type': 'snippet',
                'id': storage['data'][0]['uuid'],
                'attributes': storage['data'][0]
            }
        }
        result = testing.TestClient(server.server.api).simulate_patch(
            path='/api/snippy/rest/snippets/53908d68425c61dc',
            headers={'accept': 'application/vnd.api+json'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
