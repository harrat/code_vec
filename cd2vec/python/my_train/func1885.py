def test_add():
    a = other_targets_ds + random_target_ds
    n = a.num_samplets
    n1 = other_targets_ds.num_samplets
    n2 = random_target_ds.num_samplets
    assert n1 + n2 == n

    assert set(a.samplet_ids) == set(
            other_targets_ds.samplet_ids + random_target_ds.samplet_ids)
    assert a.num_features == other_targets_ds.num_features == \
           random_target_ds.num_features
    assert all(a.feature_names == other_targets_ds.feature_names)

    comb_ds = test_dataset + same_ids_new_feat
    comb_names = np.concatenate([test_dataset.feature_names,
                                 same_ids_new_feat.feature_names])
    if not all(comb_ds.feature_names == comb_names):
        raise ValueError('feature names were not carried forward in combining two '
                         'datasets with same IDs and different feature names!')
