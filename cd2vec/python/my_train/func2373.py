def test_middle_begin_good1(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        audiofile.middle_begin = 0
        self.assertEqual(audiofile.all_length, 1331)
        self.assertEqual(audiofile.head_length, 0)
        self.assertEqual(audiofile.middle_length, 1331)
        self.assertEqual(audiofile.tail_length, 0)
