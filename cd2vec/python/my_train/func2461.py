@pytest.mark.parametrize("action", ['sdist', 'bdist_wheel'])
@pytest.mark.parametrize("hide_listing", [True, False])
def test_hide_listing(action, hide_listing, capfd):

    cmd = [action]
    if hide_listing:
        cmd.insert(0, "--hide-listing")

    @project_setup_py_test("test-hide-listing", cmd, verbose_git=False, disable_languages_test=True)
    def run():
        pass

    run()

    out, _ = capfd.readouterr()
    if hide_listing:
        assert to_platform_path("bonjour/__init__.py") not in out
    else:
        assert to_platform_path("bonjour/__init__.py") in out

    if action == "sdist":
        assert "copied 10 files" in out
    elif action == "bdist_wheel":
        assert "copied 6 files" in out  # build_py
        assert "copied 9 files" in out  # install_lib
        assert "copied 0 files" in out  # install_scripts
