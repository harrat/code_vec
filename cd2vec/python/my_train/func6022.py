def test_metric_helper_name_exception(self):
        class MyMetric(MetricHelper):
            _fields = self.metric_fields
            _metadata = self.metric_metadata

        with pytest.raises(Base10Error) as exc:
            metric = MyMetric(**self.metric_values)

        assert '_name is required' == str(exc.value)
