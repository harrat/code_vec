@pytest.mark.parametrize("output_file", ["test_file.json"])
def test_feed_processor_with_output_file(output_file):
    feed_processor = core.FeedProcessor()
    feed_processor(output=output_file)
    file_path = Path(output_file)
    assert file_path.exists()
    os.remove(str(file_path.resolve()))

