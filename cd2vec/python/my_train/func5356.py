def test_sentbucket_cache(self):
    cached_stats1 = compare_mt_main.generate_sentence_bucketed_report(self.ref, [self.out1], to_cache=True)
    cached_stats2 = compare_mt_main.generate_sentence_bucketed_report(self.ref, [self.out2], to_cache=True)
    self.assertTrue('stats' in cached_stats1)
    self.assertTrue('stats' in cached_stats2)
    ori_report = compare_mt_main.generate_sentence_bucketed_report(self.ref, [self.out1, self.out2])
    cached_report = compare_mt_main.generate_sentence_bucketed_report(self.ref, [self.out1, self.out2], cache_dicts=[cached_stats1, cached_stats2])
    self.assertTrue(cached_report.sys_stats == ori_report.sys_stats)
