def test_metric_helper_handles_time_field(self):
        class MyMetric(MetricHelper):
            _name = self.metric_name
            _fields = self.metric_fields + ['time']
            _metadata = self.metric_metadata

        metric = MyMetric(**self.metric_values)

        assert metric.fields == self.metric_fields
