def test_get_pending_resources(self):
        processor = BatchProcessor('auth', 'secret', '12345')

        def fake_request_pending_resources(start=0, count=100, concat=None):
            resources = {
                "00934509022": {
                    "SECAO": [
                        "zzzz"
                    ]
                },
                "02541926375": {
                    "LOJA": [
                        "3333"
                    ],
                    "PRODUTO": [
                        "2"
                    ]
                }
            }
            if start == 0:
                return (resources, 2)
            else:
                return (resources, 0)
        with mock.patch.object(processor, 'request_pending_resources', side_effect=fake_request_pending_resources):
            resources = processor.get_pending_resources()
            self.assertEqual(len(resources), 2)
            self.assertTrue(isinstance(
                resources['00934509022'][0], PanamahSecao))
            self.assertTrue(isinstance(
                resources['02541926375'][0], PanamahLoja))
            self.assertTrue(isinstance(
                resources['02541926375'][1], PanamahProduto))
