def test_deserialize():
    """Test deserialize correctly builds UidModel from registry"""
    uid_a = str(uuid.uuid4())
    uid_b = str(uuid.uuid4())
    dates = [datetime.datetime(2019, 1, i) for i in range(1, 5)]
    string_dates = [properties.DateTime.to_json(d) for d in dates]
    input_dict = {
        uid_a: {
            'date_created': string_dates[0],
            'date_modified': string_dates[1],
            'my_int': 0,
            'my_model': uid_b,
            'uid': uid_a,
            '__class__': 'MyModelWithIntAndInstance',
        },
        uid_b: {
            'date_created': string_dates[2],
            'date_modified': string_dates[3],
            'my_int': 1,
            'uid': uid_b,
            '__class__': 'MyModelWithInt',
        },
        '__root__': uid_a,
    }
    model_a = omf.base.UidModel.deserialize(input_dict, trusted=True)
    assert isinstance(model_a, MyModelWithIntAndInstance)
    #pylint: disable=no-member
    assert str(model_a.uid) == uid_a
    assert model_a.date_created == dates[0]
    assert model_a.date_modified == dates[1]
    assert model_a.my_int == 0
    assert isinstance(model_a.my_model, MyModelWithInt)
    assert model_a.my_model.date_created == dates[2]
    assert model_a.my_model.date_modified == dates[3]
    assert model_a.my_model.my_int == 1
    input_dict['__root__'] = uid_b
    properties.extras.HasUID._INSTANCES = {}                                   #pylint: disable=protected-access
    model_b = omf.base.UidModel.deserialize(input_dict, trusted=True)
    assert properties.equal(model_b, model_a.my_model)
    #pylint: enable=no-member
