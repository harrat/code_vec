def test_invalid_observable_embedded_timestamp(self):
        observed_data = {
            "type": "x509-certificate",
            "id": "x509-certificate--ff1e0780-358c-5808-a8c7-d0fca4ef6ef4",
            "x509_v3_extensions": {
              "private_key_usage_period_not_before": "2016-11-31T08:17:27.000000Z"
            }
        }
        self.assertFalseWithOptions(observed_data)
