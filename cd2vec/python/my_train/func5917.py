def test_bit_reg_linkage(data):
    #get first available register
    w = (m for m in data)
    m = next(w)
    rs = (r for r in m)
    r = next(rs)
    print("Register: ", r)

    print(f"Register access: {r.access}")

    v = 0x12345678
    r.value = v
    for ref in r:
        print("bitfield: ", ref.bf)
        field_expected_value = (r.value >> ref.reg_offset) & ref.bf.mask
        assert field_expected_value == ref.bf.value

    new_value = 12345678
    for ref in r:
        ref.bf.value = (new_value >> ref.reg_offset) & ref.bf.mask
    assert r.value == new_value

