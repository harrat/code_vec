def test_import():
    gqlmod.enable_gql_import()
    import queries  # noqa
    prov = MockGithubProvider()
    with _mock_provider('github', prov):
        queries.Login()
        assert prov.last_vars['__previews'] == set()
