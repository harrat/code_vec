def test_train(self):
        model_config = core.ModelConfig("CartPole-v0")
        tc = core.StepsTrainContext()
        dqn_agent = tfagents.TfDqnAgent(model_config=model_config)
        dqn_agent.train(train_context=tc, callbacks=[duration.Fast(), log.Iteration()])
