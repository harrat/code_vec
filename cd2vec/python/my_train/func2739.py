def test_toString_fast(self):
		# Should only be trusted in python 3
		if sys.version_info.major >= 3 and sys.version_info.minor >= 6:
			expect = u'{"@context":"'+model.factory.context_uri+'","id":"http://lod.example.org/museum/InformationObject/collection","type":"InformationObject","_label":"Test Object"}'
			model.factory.json_serializer = "fast"		
			outs = model.factory.toString(self.collection)
			model.factory.json_serializer = "normal"
			self.assertEqual(expect, outs)
		else:
			print("Skipping toString_fast test in Python 2.x")
