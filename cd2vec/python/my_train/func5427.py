def test_gradient_variance(self):
        X_test = np.random.rand(10, self.X.shape[1])

        def wrapper(x):
            v = self.model.predict([x])[1]
            return v

        def wrapper_grad(x):
            return self.model.predictive_variance_gradient(x)

        grad = self.model.predictive_variance_gradient(X_test[0])
        assert grad.shape[0] == X_test.shape[1]

        for xi in X_test:
            err = check_grad(wrapper, wrapper_grad, xi, epsilon=1e-6)
            assert err < 1e-5
