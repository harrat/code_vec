@pytest.mark.parametrize(
    "name,obj,data,encoder,metadata,display",
    [
        (
            "foobarbaz",
            "foo,bar,baz",
            {"text/plain": "'foo,bar,baz'"},
            "display",  # Prevent data saves
            {"scrapbook": {"name": "foobarbaz", "data": False, "display": True}},
            None,  # This should default into True
        ),
        (
            "tinypng",
            Image(filename=get_fixture_path("tiny.png")),
            {
                "image/png": (
                    "iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAIAAAD91JpzAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gwRBREo2qqE0wAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAAFklEQVQI12P8//8/AwMDEwMDAwMDAwAkBgMBvR7jugAAAABJRU5ErkJggg==\n"  # noqa: E501
                    if six.PY3
                    else "\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x02\x00\x00\x00\x02\x08\x02\x00\x00\x00\xfd\xd4\x9as\x00\x00\x00\tpHYs\x00\x00\x0b\x13\x00\x00\x0b\x13\x01\x00\x9a\x9c\x18\x00\x00\x00\x07tIME\x07\xe2\x0c\x11\x05\x11(\xda\xaa\x84\xd3\x00\x00\x00\x1diTXtComment\x00\x00\x00\x00\x00Created with GIMPd.e\x07\x00\x00\x00\x16IDAT\x08\xd7c\xfc\xff\xff?\x03\x03\x03\x13\x03\x03\x03\x03\x03\x03\x00$\x06\x03\x01\xbd\x1e\xe3\xba\x00\x00\x00\x00IEND\xaeB`\x82"  # noqa: E501
                ),
                "text/plain": "<IPython.core.display.Image object>",
            },
            "display",  # Prevent data saves
            {"scrapbook": {"name": "tinypng", "data": False, "display": True}},
            True,
        ),
        (
            "tinypng",
            Image(filename=get_fixture_path("tiny.png")),
            {"text/plain": "<IPython.core.display.Image object>"},
            "display",  # Prevent data saves
            {"scrapbook": {"name": "tinypng", "data": False, "display": True}},
            ("text/plain",),  # Pick content of display
        ),
        (
            "tinypng",
            Image(filename=get_fixture_path("tiny.png")),
            {"text/plain": "<IPython.core.display.Image object>"},
            "display",  # Prevent data saves
            {"scrapbook": {"name": "tinypng", "data": False, "display": True}},
            {"exclude": "image/png"},  # Exclude content of display
        ),
        (
            "tinypng",
            Image(filename=get_fixture_path("tiny.png")),
            {},  # Should have no matching outputs
            "display",  # Prevent data saves
            {"scrapbook": {"name": "tinypng", "data": False, "display": True}},
            ("n/a",),  # Pick content of display
        ),
    ],
)
@mock.patch("IPython.display.display")
def test_glue_display_only(mock_display, name, obj, data, encoder, metadata, display):
    glue(name, obj, encoder, display)
    mock_display.assert_called_once_with(data, metadata=metadata, raw=True)

