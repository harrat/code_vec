def test_ds_joint_kde_rug_hist_smoke(ds_NDs):
    with seaborn.axes_style("ticks"):
        fig4 = _do_jointplots(ds_NDs, hist=True, kde=True, rug=True)
        return fig4
