def test_load_local_data_from_local_with_cache(self):
        with UseModel(self.foo_model) as model:
            res = cherry.base._load_data_from_local(self.foo_model)
        self.assertEqual(res['data'], 'bar')
