def test_custom_property_name_strict(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['foobar'] = "abc123"
        self.assertFalseWithOptions(indicator, strict_properties=True)
