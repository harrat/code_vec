def test_default_level(self) -> None:
        logger = get_logger(__name__)
        self.assertEqual(logger.level, logging.INFO)
