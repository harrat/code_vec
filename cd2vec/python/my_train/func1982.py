def test_supports_system(hellotest, testsys_system):
    hellotest.valid_systems = ['*']
    assert hellotest.supports_system('gpu')
    assert hellotest.supports_system('login')
    assert hellotest.supports_system('testsys:gpu')
    assert hellotest.supports_system('testsys:login')

    hellotest.valid_systems = ['*:*']
    assert hellotest.supports_system('gpu')
    assert hellotest.supports_system('login')
    assert hellotest.supports_system('testsys:gpu')
    assert hellotest.supports_system('testsys:login')

    hellotest.valid_systems = ['testsys']
    assert hellotest.supports_system('gpu')
    assert hellotest.supports_system('login')
    assert hellotest.supports_system('testsys:gpu')
    assert hellotest.supports_system('testsys:login')

    hellotest.valid_systems = ['testsys:gpu']
    assert hellotest.supports_system('gpu')
    assert not hellotest.supports_system('login')
    assert hellotest.supports_system('testsys:gpu')
    assert not hellotest.supports_system('testsys:login')

    hellotest.valid_systems = ['testsys:login']
    assert not hellotest.supports_system('gpu')
    assert hellotest.supports_system('login')
    assert not hellotest.supports_system('testsys:gpu')
    assert hellotest.supports_system('testsys:login')

    hellotest.valid_systems = ['foo']
    assert not hellotest.supports_system('gpu')
    assert not hellotest.supports_system('login')
    assert not hellotest.supports_system('testsys:gpu')
    assert not hellotest.supports_system('testsys:login')

    hellotest.valid_systems = ['*:gpu']
    assert hellotest.supports_system('testsys:gpu')
    assert hellotest.supports_system('foo:gpu')
    assert not hellotest.supports_system('testsys:cpu')
    assert not hellotest.supports_system('testsys:login')

    hellotest.valid_systems = ['testsys:*']
    assert hellotest.supports_system('testsys:login')
    assert hellotest.supports_system('gpu')
    assert not hellotest.supports_system('foo:gpu')

