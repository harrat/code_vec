def test_hellocheck_local(hellotest, local_exec_ctx):
    # Test also the prebuild/postbuild functionality
    hellotest.prebuild_cmds = ['touch prebuild', 'mkdir prebuild_dir']
    hellotest.postbuild_cmds = ['touch postbuild', 'mkdir postbuild_dir']
    hellotest.keep_files = ['prebuild', 'postbuild',
                            'prebuild_dir', 'postbuild_dir']

    # Force local execution of the test; just for testing .local
    hellotest.local = True
    _run(hellotest, *local_exec_ctx)
    must_keep = [
        hellotest.stdout.evaluate(),
        hellotest.stderr.evaluate(),
        hellotest.build_stdout.evaluate(),
        hellotest.build_stderr.evaluate(),
        hellotest.job.script_filename,
        *hellotest.keep_files
    ]
    for f in must_keep:
        assert os.path.exists(os.path.join(hellotest.outputdir, f))

