def test_rate_sink():
    sink = RateSink()
    logger = Logger('testlog', sinks=[sink])

    for i in range(10):
        with logger.info('sleeping', reraise=False):
            time.sleep(0.02)
            if i % 2:
                raise ValueError()
    test_rates = sink.get_rates()['testlog']['sleeping']
    # TODO: these are a little flaky, esp when moving between
    # environments, runtimes, and with/without coverage, hence the
    # range
    all_lower_limit = 40 if IS_PYPY else 48
    assert all_lower_limit <= round(test_rates['__all__']) <= 51
    assert 22 <= round(test_rates['exception']) <= 26

    counts = sink.get_total_counts()
    assert counts['__all__'] == 10

    assert repr(sink)

