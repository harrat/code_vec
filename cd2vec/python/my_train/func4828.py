def test_publish_url_regex_filtered_repos(command_tester):
    """publishes repos with relative url matching the regex"""
    fake_publish = FakePublish()
    fake_pulp = fake_publish.pulp_client_controller
    _add_repo(fake_pulp)

    command_tester.test(
        fake_publish.main,
        [
            "test-publish",
            "--pulp-url",
            "https://pulp.example.com",
            "--repo-url-regex",
            "/unit/2/",
        ],
    )

    # repo with relative url matching '/unit/2/' is published
    assert [hist.repository.id for hist in fake_pulp.publish_history] == ["repo2"]

