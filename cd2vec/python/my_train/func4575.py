def test_get_request_body_empty(self):
        self._make_request('https://www.amazon.com')
        last_request = self.client.get_last_request()

        body = self.client.get_request_body(last_request['id'])

        self.assertEqual(b'', body)
