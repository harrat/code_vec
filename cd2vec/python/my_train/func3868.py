def test_hist_calls_decorator(self):
        @hist_calls
        def test(n):
            return n

        for i in xrange(1, 11):
            test(i)

        _histogram = histogram("test_calls")
        snapshot = _histogram.get_snapshot()
        self.assertAlmostEqual(_histogram.get_mean(), 5.5)
        self.assertEqual(_histogram.get_max(), 10)
        self.assertEqual(_histogram.get_min(), 1)
        self.assertAlmostEqual(_histogram.get_std_dev(), 3.02765, places=5)
        self.assertAlmostEqual(_histogram.get_variance(), 9.16667, places=5)
        self.assertAlmostEqual(snapshot.get_75th_percentile(), 8.25)
        self.assertAlmostEqual(snapshot.get_98th_percentile(), 10.0)
        self.assertAlmostEqual(snapshot.get_99th_percentile(), 10.0)
        self.assertAlmostEqual(snapshot.get_999th_percentile(), 10.0)
