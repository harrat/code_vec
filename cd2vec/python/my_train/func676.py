def test_get_best_texture_for_id(self):
        # Create Cam3

        location = [4.28, 3.58, 0]
        rotation = [-90, 180, -52.2]
        focal_length = 35
        sensor_width = 32
        sensor_height = 18
        screen_width = 1920
        screen_height = 1080

        cam3 = Camera(focal_length, screen_width, screen_height, sensor_width, sensor_height, location, rotation,
                      "EULER")

        texture3 = Texture('/fake_texture.png', cam3)
        self.mapper.textures.append(texture3)

        self.mapper.start_visibility_analysis()
        self.mapper.set_duplicates_for_textures()

        self.assertEqual(self.mapper.get_best_texture_for_duplicate_triangle(2), 2,
                         "Best Texture for triangle 2 is at index 2")
        self.assertEqual(self.mapper.get_best_texture_for_duplicate_triangle(8), 1,
                         "Best Texture for triangle 8 is at index 1")
        self.assertEqual(self.mapper.get_best_texture_for_duplicate_triangle(5), 0,
                         "Best Texture for triangle 5 is at index 0")
        self.assertEqual(self.mapper.get_best_texture_for_duplicate_triangle(11), 0,
                         "Best Texture for triangle 11 is at index 0")
