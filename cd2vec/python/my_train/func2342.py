def test_kbd_interrupt_within_test(make_runner, make_cases, common_exec_ctx):
    runner = make_runner()
    check = KeyboardInterruptCheck()
    with pytest.raises(KeyboardInterrupt):
        runner.runall(make_cases([KeyboardInterruptCheck()]))

    stats = runner.stats
    assert 1 == len(stats.failures())
    assert_all_dead(runner)

