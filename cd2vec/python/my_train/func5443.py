def test_modify_attribute_kmip_2_0_with_multivalued_no_attr_match(self):
        """
        Test that a KmipError is raised when attempting to modify an
        non-existent attribute value.
        """
        e = engine.KmipEngine()
        e._protocol_version = contents.ProtocolVersion(2, 0)
        e._attribute_policy._version = e._protocol_version
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        args = (
            payloads.ModifyAttributeRequestPayload(
                unique_identifier="1",
                current_attribute=objects.CurrentAttribute(
                    attribute=attributes.Name(
                        name_value=attributes.Name.NameValue("Invalid Key")
                    )
                ),
                new_attribute=objects.NewAttribute(
                    attribute=attributes.Name(
                        name_value=attributes.Name.NameValue("Modified Name")
                    )
                )
            ),
        )
        self.assertRaisesRegex(
            exceptions.KmipError,
            "The specified current attribute could not be found on the "
            "managed object.",
            e._process_modify_attribute,
            *args
        )
