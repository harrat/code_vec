def test_terminate_running(self):
        """Test that attempts to terminate process"""
        fileback = "test_run_base_back.py"
        launch = Launcher(fileback, "NonUniform Resource Locator",
                          'project.zip', DEBUG)
        launch.run(True)
        print("Entering busyloop")
        while not launch.process_code_running:
            pass
        print("Exiting busyloop")
        time.sleep(0.5)
        can_terminate = launch.process_terminate()
        assert launch.process_exitcode == -15
        assert can_terminate
