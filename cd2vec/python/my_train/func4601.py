@pytest.mark.high
    def test_to_json(self):
        expected_result = json.dumps(OrderedDict(sorted({
            'consumers': {
                'consumer_name': {
                    'description': 'docstring',
                    'module': 'module',
                    'name': 'qualname',
                },
            },
            'producers': {
                'producer_name': {
                    'description': 'description',
                    'module': 'module',
                    'name': 'qualname',
                }
            }
        }.items())))

        self.register.register(self.tasks['consumer'])
        self.register.register(self.tasks['producer'])

        result = self.register.to_json()

        self.assertEqual(expected_result, result)
