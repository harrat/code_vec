@mock.patch("IPython.display.display")
def test_scraps_report_with_data(mock_display, notebook_collection):
    notebook_collection.scraps_report(include_data=True)
    mock_display.assert_has_calls(
        [
            mock.call(AnyMarkdownWith("### result1")),
            mock.call(AnyMarkdownWith("#### output")),
            mock.call({"text/plain": "'Hello World!'"}, metadata={}, raw=True),
            mock.call(AnyMarkdownWith("#### one_only")),
            mock.call({"text/plain": "'Just here!'"}, metadata={}, raw=True),
            mock.call(AnyMarkdownWith("#### one")),
            mock.call("1"),
            mock.call(AnyMarkdownWith("#### number")),
            mock.call("1"),
            mock.call(AnyMarkdownWith("#### list")),
            mock.call("[1, 2, 3]"),
            mock.call(AnyMarkdownWith("#### dict")),
            mock.call("{'a': 1, 'b': 2}" if six.PY3 else "{u'a': 1, u'b': 2}"),
            mock.call(AnyMarkdownWith("<hr>")),
            mock.call(AnyMarkdownWith("### result2")),
            mock.call(AnyMarkdownWith("#### output")),
            mock.call({"text/plain": "'Hello World 2!'"}, metadata={}, raw=True),
            mock.call(AnyMarkdownWith("#### two_only")),
            mock.call({"text/plain": "'Just here!'"}, metadata={}, raw=True),
            mock.call(AnyMarkdownWith("#### two")),
            mock.call("2"),
            mock.call(AnyMarkdownWith("#### number")),
            mock.call("2"),
            mock.call(AnyMarkdownWith("#### list")),
            mock.call("[4, 5, 6]"),
            mock.call(AnyMarkdownWith("#### dict")),
            mock.call("{'a': 3, 'b': 4}" if six.PY3 else "{u'a': 3, u'b': 4}"),
        ]
    )
