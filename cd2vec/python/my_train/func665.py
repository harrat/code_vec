def test_directory_walk(self):
        results = list(stowage.directory_walk(self.dir, 'test_out'))
        results_set = set(results)
        assert len(results_set) == len(results)  # ensure no dupes
        assert results_set == self.results
