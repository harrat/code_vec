def test_invalid_settings():
    with pytest.raises(ConfigurationError):
        MongoDBStorage({"port": "string"})
