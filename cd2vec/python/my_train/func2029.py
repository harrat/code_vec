def test_kwargs_parameter_scoring():
    """Assert that the kwargs parameter works as intended (add scoring acronym)."""
    fs = FeatureSelector(strategy='RFECV',
                         solver='lgb_class',
                         scoring='auc',
                         n_features=12)
    X = fs.fit_transform(X_bin, y_bin)
    assert X.shape[1] == 24

