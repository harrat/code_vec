def test_sink_timeout(self, sink):
        sink_data(sink, log_message_data[:1])
        time.sleep(4)
        sink.logs_service.put_log_events.assert_called_with(
            logEvents=log_message_data[:1],
            logGroupName='log_group_name',
            logStreamName='log_stream_name',
            sequenceToken=first_token,
        )
        sink_data(sink, log_message_data[1:2])
        sink.shutdown()
        sink.logs_service.put_log_events.assert_called_with(
            logEvents=log_message_data[1:2],
            logGroupName='log_group_name',
            logStreamName='log_stream_name',
            sequenceToken=second_token,
        )
