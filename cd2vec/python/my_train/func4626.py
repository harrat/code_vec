def test_version_handle_purge_nightmare_pattern(self):
        runner('git tag -a itsover9000-v1.0.0_codingaward -m "test message"')
        purge_pattern = ['itsover9000-', '_codingaward']
        v = relic.release.get_info(purge_pattern)
        for p in purge_pattern:
            assert p not in v.short

        # Should have been purged via regex
        assert not v.short.startswith('v')
        assert v.short == '1.0.0'
