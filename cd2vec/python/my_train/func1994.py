def test_importanize_grouped_no_add_lines(self) -> None:
        self.config.after_imports_normalize_new_lines = False
        result = next(
            run_importanize_on_source(
                self.input_text,
                RuntimeConfig(formatter_name="grouped", _config=self.config),
            )
        )

        assert result.organized == self.output_grouped_no_add_lines.read_text()
        assert result.has_changes
        assert result.is_success
