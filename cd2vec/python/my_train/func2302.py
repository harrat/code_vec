@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_api_search_snippet_field_002(server):
        """Get specific snippet field.

        Send GET /snippets/{id}/brief for existing snippet. In this case
        the URI digest is only 10 octets. The returned link must contain 16
        octet digest in the link.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '262'
        }
        expect_body = {
            'data': {
                'type': 'snippet',
                'id': Snippet.REMOVE_UUID,
                'attributes': {
                    'brief': Storage.remove['brief']
                }
            },
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/snippets/11cd5827-b6ef-4067-b5ac-3ceac07dde9f/brief'
            }
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/snippets/54e41e9b52/brief',
            headers={'accept': 'application/vnd.api+json'})
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
