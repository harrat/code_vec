@pytest.mark.parametrize('key, columns', [
    ('genes', gene_cols),
    ('ontology', ont_cols),
    ('counts', None),
    ('tpm', None),
    ('annotation', rna_annot_cols)
])
def test_readrnaseq(rnafiles, key, columns):
    for d, fn in flatten_dict(rnafiles, key).items():
        func = getattr(io, 'read_{}'.format(key))

        # check file exists
        assert op.exists(fn)

        # check loading from filepath
        data = func(fn)
        assert isinstance(data, pd.DataFrame)

        # check loading from dataframe (should return same object)
        data2 = func(data)
        assert id(data) == id(data2)

        # check that copy parameter works as expected
        data3 = func(data, copy=True)
        assert isinstance(data3, pd.DataFrame)
        assert id(data) != id(data3)

        # confirm columns are as expected
        if columns is not None:
            assert np.all(columns == data.columns)

        # confirm errors
        with pytest.raises(TypeError):
            func(1)

        with pytest.raises(TypeError):
            func([1, 2, 3])

        with pytest.raises(FileNotFoundError):
            func('notafile')
