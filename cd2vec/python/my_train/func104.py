@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'export-time')
    def test_cli_export_snippet_006(snippy):
        """Export defined snippets.

        Export defined snippet based on message digest. File name is not
        defined in command line -f|--file option. This should result usage
        of default file name and format
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Snippet.FORCED
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-d', '53908d68425c61dc'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './snippets.mkdn', content)
