def test_early_stopping():
    """Assert than early stopping works."""
    atom = ATOMClassifier(X_bin, y_bin, random_state=1)
    atom.run(['XGB', 'LGB', 'CatB'], n_calls=5, bo_params={'early_stopping': 0.1,
                                                           'cv': 1})
    for model in atom.models_:
        assert isinstance(model.evals, dict)

