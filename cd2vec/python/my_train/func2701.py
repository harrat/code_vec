def test_counts(fake_exp):
    counts = fake_exp._counts
    assert counts['containers'] == 4
    assert counts['cells'] == 24
    assert counts['colonies'] == 4
    assert counts['lineages'] == 12  # number of leaves when no filter is applied

