@pytest.mark.parametrize("input", [b"+"])
def test_decode_invalid_raising_error_handler(input):
    decoder = IncrementalDecoder(errors="raises")
    with pytest.raises(EncodingError):
        decoder.decode(input)
    assert (b"", 0) == decoder.getstate()

