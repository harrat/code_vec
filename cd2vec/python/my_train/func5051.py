def test_play(self):
        model_config = core.ModelConfig(_lineworld_name)
        random_agent = tfagents.TfRandomAgent(model_config=model_config)
        pc = core.PlayContext()
        pc.max_steps_per_episode = 10
        pc.num_episodes = 1
        random_agent.play(play_context=pc, callbacks=[])
        assert pc.num_episodes == 1
