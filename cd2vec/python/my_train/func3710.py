def test_custom_node(app_custom_node):
    """Test that we can pass a custom node configuration."""

    redis = FlaskMultiRedis(app_custom_node)
    kwargs = redis.connection_pool.connection_kwargs
    assert kwargs['port'] == 16379
    assert kwargs['db'] == 9
    assert kwargs['password'] == 'password'
    assert kwargs['socket_timeout'] == 2
    assert kwargs['ssl_keyfile'] == 'ssl/rediskey.pem'
    assert kwargs['ssl_certfile'] == 'ssl/rediscert.pem'
    assert kwargs['ssl_ca_certs'] == 'ssl/rediscert.pem'
    assert kwargs['ssl_cert_reqs'] == 'required'

