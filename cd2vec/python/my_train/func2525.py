@staticmethod
    @pytest.mark.usefixtures('import-gitlog', 'caller')
    def test_api_update_reference_005(server):
        """Try to update reference with malformed request.

        Try to send PUT /references/{id} to update reference with client
        generated resource ID. In this case the ID looks like a valid message
        digest.
        """

        storage = {
            'data': [
                Storage.gitlog
            ]
        }
        request_body = {
            'data': {
                'type': 'reference',
                'id': Reference.REGEXP_DIGEST,
                'attributes': {
                    'data': Request.regexp['data'],
                    'brief': Request.regexp['brief'],
                    'groups': Request.regexp['groups'],
                    'tags': Request.regexp['tags'],
                    'links': Request.regexp['links']
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '387'
        }
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '403',
                'statusString': '403 Forbidden',
                'module': 'snippy.testing.testing:123',
                'title': 'client generated resource id is not supported, remove member data.id'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_put(
            path='/api/snippy/rest/references/5c2071094dbfaa33',
            headers={'accept': 'application/json'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_403
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
