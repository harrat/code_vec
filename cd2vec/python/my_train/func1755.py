def test_simulated_values(self):
        self.agent.initializeSim()
        self.agent.simulate()

        self.assertAlmostEqual(self.agent.MPCnow[1],
                               0.5711503906043797)

        self.assertAlmostEqual(self.agent.aLvlNow[1],
                               0.18438326264597635)
