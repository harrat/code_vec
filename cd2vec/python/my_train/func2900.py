def test_exec_process(self):
        inp = self.root.make_file(suffix='.gz')
        with gzip.open(inp, 'wt') as o:
            o.write('foo')
        out = self.root.make_file(suffix='.gz')
        exec_process('cat', stdin=inp, stdout=out)
        with gzip.open(out, 'rt') as o:
            assert 'foo' == o.read()
