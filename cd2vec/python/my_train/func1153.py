@pytest.mark.vcr()
def test_workbench_export_plugin_id(api):
    fobj = api.workbenches.export(plugin_id=19506)
    assert isinstance(fobj, BytesIO)

@pytest.mark.vcr()
def test_workbench_export_asset_uuid(api):
    assets = api.workbenches.assets()
    fobj = api.workbenches.export(asset_uuid=assets[0]['id'])
    assert isinstance(fobj, BytesIO)
