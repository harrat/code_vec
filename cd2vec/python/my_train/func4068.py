def test_alias_search_mapping_exact(cli, alias_config):
    output = cli('alias', ['list', '--no-inactive', 'active1'])
    assert output == "[test] active1 -> 43/1 (active project, activity 1)\n"

