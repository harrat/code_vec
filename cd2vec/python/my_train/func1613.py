@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_callback_after_python_job(fixture, request):
    def _callback(job):
        return job.function_result - 1

    fn = pyccc.PythonCall(function_tests.fn, 3.0)
    engine = request.getfixturevalue(fixture)
    job = engine.launch(image=PYIMAGE, command=fn, interpreter=PYVERSION, when_finished=_callback)
    print(job.rundata)
    job.wait()

    assert job.function_result == 4.0
    assert job.result == 3.0

