def test_own_data():

    config.plotit = 1

    config.printit = 1

    config.plot_type = 'plotly'

    config.show_plot = 0

    config.other_columns = 0

    config.multiprocessing = 0

    result = predictit.main.predict(data=np.random.randn(5, 100), predicts=3, return_type='detailed_dictionary')

    assert not np.isnan(np.min(result['best']))

    return result

