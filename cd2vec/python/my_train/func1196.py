@pytest.mark.vcr()
def test_access_groups_list_wildcard_fields_typeerror(api):
    with pytest.raises(TypeError):
        api.access_groups.list(wildcard_fields='nope')
