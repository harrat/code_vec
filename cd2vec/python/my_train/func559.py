def test_agent(self):
        # Have one consumer type inherit relevant objects from the economy,
        # then solve their microeconomic problem
        self.agent.getEconomyData(self.economy)
        self.agent.solve()
        self.assertAlmostEqual(self.agent.solution[0].cFunc[0](10., self.economy.MSS),
                               2.5635896520991377)
