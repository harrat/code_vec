def test_write_overwrite_delete(self):
        """
        Tests helpers.File.write along with helpers.File.delete.
        """

        expected = "Hello, World! I'm PyFunceble"
        File("hi").write(expected)

        with open("hi") as file:
            actual = file.read()

        self.assertEqual(expected, actual)

        expected = "Hello, World! Python is great, you should consider learning it!"
        File("hi").write(expected)

        with open("hi") as file:
            actual = file.read()

        self.assertEqual(expected, actual)

        expected = False
        File("hi").delete()
        actual = path.isfile("hi")

        self.assertEqual(expected, actual)
