@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_export_reference_028(snippy):
        """Export all references.

        Try to export all references into file format that is not supported.
        In this case the file format contains just one additional letter to
        supported json format.
        """

        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-f', 'foo.jsontext'])
            assert cause == 'NOK: cannot identify file format for file: foo.jsontext'
            mock_file.assert_not_called()
            file_handle = mock_file.return_value.__enter__.return_value
            file_handle.write.assert_not_called()
