def test_valid_standard_links():
    module_to_attributes = pl.read_json_data('standard/module_to_attributes.json')
    links = set([rel['linkToStandard'] for rel in module_to_attributes])
    assert not get_invalid_urls(links)

