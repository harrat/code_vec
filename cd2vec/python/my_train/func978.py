def test_flex_alloc_partition_idle(make_flexible_job):
    job = make_flexible_job('idle', sched_partition='p2')
    with pytest.raises(JobError):
        prepare_job(job)
