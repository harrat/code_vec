@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_function_with_renamed_module_var(fixture, request):
    result = _runcall(fixture, request, function_tests.fn_with_renamed_attr, 'a')
    assert not result

