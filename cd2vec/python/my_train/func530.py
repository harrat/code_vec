@pytest.mark.parametrize("archive_fixture", ["mock_targz", "mock_zip", "mock_zip"])
def test_extractor(archive_fixture, tmpdir, mock_targz, mock_zip):
    directory = "comtypes-1.1.7"
    if archive_fixture == "mock_targz":
        archive = mock_targz(directory)
    elif archive_fixture == "mock_zip":
        archive = mock_zip(directory)
    else:
        archive = os.path.abspath(os.path.join("source-packages", directory))

    if archive_fixture == "mock_targz":
        extractor = req_compile.metadata.extractor.TarExtractor("gz", archive)
        prefix = directory + "/"
    elif archive_fixture == "mock_zip":
        extractor = req_compile.metadata.extractor.ZipExtractor(archive)
        prefix = directory + "/"
    else:
        extractor = req_compile.metadata.extractor.NonExtractor(archive)
        prefix = ""

    with contextlib.closing(extractor):
        all_names = set(extractor.names())
        assert all_names == {
            prefix + "README",
            prefix + "setup.py",
            prefix + "comtypes/__init__.py",
            prefix + "test/setup.py",
        }
