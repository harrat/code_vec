def test_top_level_command_failure():
    Breathe.add_commands(
        AppContext("china"),
        {
            "not marked top level <sequence1> and <sequence2> [<n>]": DoNothing()
            + Exec("sequence1")
            + DoNothing()
            + Exec("sequence2")* Repeat("n")
        },
        extras=[CommandsRef("sequence1"), CommandsRef("sequence2", 2), IntegerRef("n", 1, 10, 1)],
        top_level=False,
    )
    assert len(Breathe.top_level_commands) == 2

def test_clear():
    Breathe.clear()
