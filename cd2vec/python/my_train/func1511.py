def test_del(x):
    del x[123]

    # assert x == {'foo': 'bar', 'baz': 'qux', 'def': 456}
    with raises(KeyError):
        assert x[123]
