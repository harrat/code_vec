def test_iso_links(self):
        c = Crawl("http://thredds.axiomdatascience.com/thredds/global.xml")
        isos = [s.get("url") for d in c.datasets for s in d.services if s.get("service").lower() == "iso"]
        assert "?dataset=" in isos[0]
        assert "&catalog=" in isos[0]
