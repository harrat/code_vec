def test_wait_all_3(self):
        def check():
            sleep(0.05)
            raise ValueError()

        self.assertEqual(wait_all(popen(check), popen(lambda: 1)), [1, 0])
