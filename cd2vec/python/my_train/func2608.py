def test_no_candidates(mocked_responses, tmpdir):
    wheeldir = str(tmpdir)
    mocked_responses.add(responses.GET, INDEX_URL + "/garbage/", status=404)
    repo = PyPIRepository(INDEX_URL, wheeldir)

    candidates = repo.get_candidates(pkg_resources.Requirement.parse("garbage"))

    assert candidates == []
    assert len(mocked_responses.calls) == 1

