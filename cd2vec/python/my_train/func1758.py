@freeze_time('2014-01-20')
def test_regrouped_entries(cli, entries_file):
    entries_file.write("""20/01/2014
alias_1 0800-0900 Play ping-pong
alias_1 1200-1300 Play ping-pong
""")

    stdout = cli('status')
    assert line_in(
        "alias_1 2.00  Play ping-pong",
        stdout
    )
