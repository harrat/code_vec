def test_parse_bad_file_structure_version(tmpdir):
    indir = tmpdir.mkdir('badfilestructureversion')
    outfile = str(indir)+'.iso'
    subprocess.call(['genisoimage', '-v', '-v', '-no-pad', '-iso-level', '1',
                     '-o', str(outfile), str(indir)])

    with open(str(outfile), 'r+b') as fp:
        fp.seek(16*2048 + 881)
        fp.write(b'\x02')

    do_a_test(tmpdir, outfile, check_nofiles)

def test_parse_get_file_from_iso_not_initialized(tmpdir):
    iso = pycdlib.PyCdlib()
