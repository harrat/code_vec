def test_host_setinstance(self, host_load_data):
        host = host_load_data
        with open(resource_dir / 'test_host_poller.json') as instances:
            instance = Poller(json.load(instances))

        data = dict()
        data['action'] = 'setinstance'
        data['object'] = 'HOST'
        data['values'] = ["mail-uranus-frontend", "Central"]

        with patch('requests.post') as patched_post:
            host.setinstance(instance)
            patched_post.assert_called_with(
                self.clapi_url,
                headers=self.headers,
                data=json.dumps(data),
                verify=True
            )
