def test_scraps_collection_dataframe(notebook_result):
    expected_df = pd.DataFrame(
        [
            ("one", 1, "json", None),
            ("number", 1, "json", None),
            ("list", [1, 2, 3], "json", None),
            ("dict", {"a": 1, "b": 2}, "json", None),
            ("output", None, "display", AnyDict()),
            ("one_only", None, "display", AnyDict()),
        ],
        columns=["name", "data", "encoder", "display"],
    )
    assert_frame_equal(notebook_result.scraps.dataframe, expected_df, check_exact=True)

