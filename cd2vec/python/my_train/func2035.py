def test_get_executable_path(self):
        exe = self.root.make_file(suffix=".exe")
        exe_path = EXECUTABLE_CACHE.get_path(exe)
        assert exe_path is not None
        assert exe_path == EXECUTABLE_CACHE.get_path(exe.name)
        EXECUTABLE_CACHE.cache.clear()
        EXECUTABLE_CACHE.add_search_path(exe.parent)
        assert exe_path == EXECUTABLE_CACHE.get_path(exe.name)
        # TODO: how to test this fully, since we can't be sure of what
        # executables will be available on the installed system?
