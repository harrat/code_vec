def test_handler_types(self):
        assert issubclass(logging.Handler, rlog.Handler)
        assert issubclass(logging.StreamHandler, rlog.Handler)
        assert issubclass(logging.FileHandler, rlog.Handler)
        assert issubclass(logging.handlers.RotatingFileHandler, rlog.Handler)

        # Try to instantiate rlog.Handler
        with pytest.raises(TypeError):
            rlog.Handler()
