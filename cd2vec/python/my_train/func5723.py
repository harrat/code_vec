@pytest.mark.parametrize('signing_info_args', itertools.chain(
    itertools.combinations(SIGNING_INFO_ARGS, 1),
    itertools.combinations(SIGNING_INFO_ARGS, 2),
    itertools.combinations(SIGNING_INFO_ARGS, 3),
))
@mock.patch.object(AndroidAppBundle, 'find_paths', mock_find_paths)
def test_build_apks_incomplete_signing_info_arg(signing_info_args, android_app_bundle, cli_argument_group):
    signing_info_kwargs = {}
    for signing_info_arg, signing_info_arg_value in signing_info_args:
        signing_info_arg.register(cli_argument_group)
        signing_info_kwargs[signing_info_arg.key] = signing_info_arg_value

    with pytest.raises(argparse.ArgumentError) as exception_info:
        android_app_bundle.build_apks(pathlib.Path('android_app_bundle.aab'), **signing_info_kwargs)

    expected_invalid_flag = AndroidAppBundleArgument.KEYSTORE_PATH.flags[0]
    expected_error_message = 'Either all signing info arguments should be specified, or none of them should'
    assert exception_info.value.argument_name == expected_invalid_flag
    assert exception_info.value.message == expected_error_message

