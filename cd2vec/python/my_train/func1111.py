def test_get_attribute_list_with_no_arguments(self):
        """
        Test that a GetAttributeList request with no arguments can be
        processed correctly.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()
        e._id_placeholder = '1'

        payload = payloads.GetAttributeListRequestPayload()

        response_payload = e._process_get_attribute_list(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call(
            "Processing operation: GetAttributeList"
        )
        self.assertEqual(
            '1',
            response_payload.unique_identifier
        )
        self.assertEqual(
            9,
            len(response_payload.attribute_names)
        )
        self.assertIn(
            "Object Type",
            response_payload.attribute_names
        )
        self.assertIn(
            "Name",
            response_payload.attribute_names
        )
        self.assertIn(
            "Cryptographic Algorithm",
            response_payload.attribute_names
        )
        self.assertIn(
            "Cryptographic Length",
            response_payload.attribute_names
        )
        self.assertIn(
            "Operation Policy Name",
            response_payload.attribute_names
        )
        self.assertIn(
            "Cryptographic Usage Mask",
            response_payload.attribute_names
        )
        self.assertIn(
            "State",
            response_payload.attribute_names
        )
        self.assertIn(
            "Unique Identifier",
            response_payload.attribute_names
        )
        self.assertIn(
            "Initial Date",
            response_payload.attribute_names
        )
