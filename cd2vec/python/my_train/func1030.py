@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_delete_solution_006(snippy):
        """Delete solution with digest.

        Try to delete solution with message digest that cannot be found.
        """

        content = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--scat', 'solution', '-d', '123456789abcdef0'])
        assert cause == 'NOK: cannot find content with message digest: 123456789abcdef0'
        Content.assert_storage(content)
