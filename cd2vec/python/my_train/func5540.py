def test_get_last_entries():

    assert len(DB.posts.all()) == 22
    le, all = _get_last_entries(DB, 10)
    assert [e.id for e in le] == list(range(22, 12, -1))

