def test_execute_wrong_sql_arg():
    global cu
    with pytest.raises(ValueError):
        cu.execute(42)
