@pytest.mark.unit
    def test_patched_to_dataset_creates_expected_folders_and_files(self):
        if self.datasetPath.is_dir():
            shutil.rmtree(self.datasetPath)

        patched_to_dataset(str(self.patchPath), str(self.datasetPath), 'unique')

        assert self.datasetPath.is_dir()

        # Dataset folder merges all the patch folders generated
        assert len([item for item in self.datasetPath.iterdir()]) == 12+24
