def test_experiment_datatype(fake_exp):
    assert len(fake_exp.datatype) == 4
    assert 'cellID' in list(zip(*fake_exp.datatype))[0]

