@pytest.mark.parametrize('deck_id', [1])
def test_get_cards_to_review(deck_id: int) -> None:
    deck = spacedr.get_deck_by_id(deck_id)
    cards_to_review = spacedr.get_cards_to_review(deck)

    for card in cards_to_review:
        assert card.deck_id == deck.id
        assert card.due_date != None
        assert card.reviews_count != 0

