@mock.patch('google.cloud.pubsub_v1.PublisherClient')
    def test_topic_path(self, mock_client):
        pub = Publisher()
        pub._Publisher__publisher = mock.MagicMock()
        pub._topic_path(TOPIC_NAME)

        expected = [
            mock.call.topic_path(
                ENV_MOCK['GCP_PROJECT_ID'],
                TOPIC_NAME
            )
        ]
        self.assertEquals(pub._Publisher__publisher.mock_calls, expected)
