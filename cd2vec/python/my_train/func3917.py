def test_to_idna_multiple(self):
        """
        Runs and tests Converter.
        """

        domains_to_test = [
            "b?llogram.com",
            "bittr�?.com",
            "cryptopi?.com",
            "coinb?se.com",
            "c?yptopia.com",
            "0.0.0.0 ??etherwallet.com",
            "0.0.0.0/8",
        ]

        expected = [
            "xn--bllogram-g80d.com",
            "xn--bittr-fsa6124c.com",
            "xn--cryptopi-ux0d.com",
            "xn--coinbse-30c.com",
            "xn--cyptopia-4e0d.com",
            "0.0.0.0 xn--etherwallet-tv8eq7f.com",
            "0.0.0.0/8",
        ]
        actual = Converter(domains_to_test).get_converted()

        self.assertEqual(expected, actual)
