@staticmethod
    def test_version_option_002(capsys, caplog):
        """Test printing tool version.

        Output tool version with short option. Only the version must be
        printed and nothing else. The print must be send to stdout.
        """

        snippy = Snippy(['snippy', '-v'])
        snippy.run()
        snippy.release()
        out, err = capsys.readouterr()
        assert out == __version__ + Const.NEWLINE
        assert not err
        assert not caplog.records[:]
        Content.delete()
