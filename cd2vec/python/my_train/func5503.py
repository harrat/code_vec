def test_get_log(self):
        self.create_default_experiment()
        cluster = Cluster.new('tmux', server_name=_TEST_SERVER)
        l = cluster.get_log('exp', 'hello', process_group='group')
        self.assertIn('Hello World!', l)
