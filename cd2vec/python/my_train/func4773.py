@pytest.mark.vcr()
def test_agents_list_limit_typeerror(api):
    with pytest.raises(TypeError):
        api.agents.list(limit='nope')
