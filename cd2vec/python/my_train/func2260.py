def test_flex_alloc_valid_reservation_option(make_flexible_job):
    job = make_flexible_job('all', sched_access=['--constraint=f2'])
    job.options = ['--reservation=dummy']
    prepare_job(job)
    assert job.num_tasks == 4

