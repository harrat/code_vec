@pytest.mark.remote
def test_timeline_url():
    # check that unknown IFO results in no matches
    with pytest.raises(ValueError):
        timeline.timeline_url('X1', 1126259446, 1126259478)
