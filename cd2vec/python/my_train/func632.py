def test_parse_hash(self):
        self.assertEqual(addr.parse_hash(helpers.OP_IF['p2sh']),
                         helpers.OP_IF['script_hash'])
        self.assertEqual(addr.parse_hash(helpers.MSIG_2_2['p2sh']),
                         helpers.MSIG_2_2['script_hash'])
        self.assertEqual(
            addr.parse_hash(
                helpers.P2WSH['human']['ins'][0]['addr']),
            helpers.P2WSH['ser']['ins'][0]['pk_script'][2:])
        self.assertEqual(addr.parse_hash(helpers.P2WPKH_ADDR['address']),
                         helpers.P2WPKH_ADDR['pkh'])
        self.assertEqual(addr.parse_hash(helpers.ADDR[0]['p2pkh']),
                         helpers.PK['ser'][0]['pkh'])

        with self.assertRaises(ValueError) as context:
            addr.parse('bc1blahblahblah')

        self.assertIn('Unsupported address format. Got: ',
                      str(context.exception))

        # Test cash addr code
        riemann.select_network('bitcoin_cash_main')
        self.assertEqual(
            addr.parse_hash(helpers.OP_IF['p2sh']),
            helpers.OP_IF['script_hash'])

        self.assertEqual(
            addr.parse_hash(helpers.OP_IF['cashaddr']),
            helpers.OP_IF['script_hash'])

        self.assertEqual(
            addr.parse_hash(helpers.CASHADDR['p2pkh']),
            utils.hash160(helpers.CASHADDR['pubkey']))
