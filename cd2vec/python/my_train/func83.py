def test_heatcapacity_roofvegetation_construction(self):
        self.idf.initreadtxt(roof_vegetation)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL:ROOFVEGETATION", "RoofVegetation")
        expected = (
            m.Thickness * m.Specific_Heat_of_Dry_Soil * m.Density_of_Dry_Soil * 0.001
        )
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            # check that a UserWarning is raised
            assert almostequal(c.heatcapacity, expected, places=2)
            assert almostequal(c.heatcapacity, 120, places=2)
            assert issubclass(w[-1].category, UserWarning)
