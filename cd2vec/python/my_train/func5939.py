def test_get_relevant_policy_section_no_group(self):
        """
        Test that the lookup for a policy with no group specified is handled
        correctly.
        """
        e = engine.KmipEngine()
        e._operation_policies = {
            'test_policy': {
                'preset': {
                    enums.ObjectType.SYMMETRIC_KEY: {
                        enums.Operation.GET: enums.Policy.ALLOW_OWNER
                    }
                }
            }
        }

        expected = {
            enums.ObjectType.SYMMETRIC_KEY: {
                enums.Operation.GET: enums.Policy.ALLOW_OWNER
            }
        }

        result = e.get_relevant_policy_section('test_policy')
        self.assertEqual(expected, result)
