@pytest.mark.parametrize('joins,mtable,out,exception', [
		(OrderedDict([
			('t1', 'f'),
			('t2(t3)', 'f'),
			('[>]t4(t5)', 'f'),
			('t6', ['f1', 'f2']),
			('t7', {'f1': 'f2'}),
			('t8', {'f1': 'f2'})
		]), TableFrom('mt'), 'INNER JOIN "t1" ON "t1"."f"="mt"."f" INNER JOIN "t2" AS "t3" ON "t3"."f"="mt"."f" LEFT JOIN "t4" AS "t5" ON "t5"."f"="mt"."f" INNER JOIN "t6" ON "t6"."f1"="mt"."f1" AND "t6"."f2"="mt"."f2" INNER JOIN "t7" ON "t7"."f1"="mt"."f2" INNER JOIN "t8" ON "t8"."f1"="mt"."f2"',None),
		({'t2': 'f'}, 't1', 'INNER JOIN "t2" ON "t2"."f"="t1"."f"',None)
	])
	def testJoin(self, joins, mtable, out, exception):
		if exception:
			with pytest.raises(exception):
				str(Join(joins, mtable))
		else:
			assert str(Join(joins, mtable)) == out
