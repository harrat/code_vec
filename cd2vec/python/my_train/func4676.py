def test_compute_ordered_dissimilarity_matrix():
    # given
    iris = datasets.load_iris()
    iris_dataset = iris.data
    expected_ordered_matrix = np.load(TEST_DIR / 'data/iris_vat.npy')

    # when
    ordered_matrix = compute_ordered_dissimilarity_matrix(iris_dataset)

    # then
    np.testing.assert_allclose(ordered_matrix, expected_ordered_matrix, atol=0.1)

