def test_new_checkbox_list(self):
        md_file = MdUtils(file_name="Test_file", title="")
        md_file.new_checkbox_list(self.complex_items)
        md_file.create_md_file()

        self.assertEqual(self.expected_list.replace('-', '- [ ]'), MarkDownFile.read_file('Test_file.md'))
