def test_command_delete(self, centreon_con):
        data = {}
        data['action'] = 'del'
        data['object'] = 'CMD'
        data['values'] = 'command_test'

        with patch('requests.post') as patched_post:
            centreon_con.commands.delete('command_test', post_refresh=False)
            patched_post.assert_called_with(self.clapi_url, headers=self.headers, data=json.dumps(data), verify=True)
