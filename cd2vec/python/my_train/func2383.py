@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'yaml')
    def test_cli_import_solution_020(snippy):
        """Import solution.

        Import new solution from yaml file.
        """

        content = {
            'data': [
                Solution.NGINX
            ]
        }
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '-f', 'one-solution.yaml'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-solution.yaml', mode='r', encoding='utf-8')
