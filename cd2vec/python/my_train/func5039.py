@pytest.mark.vcr()
def test_workbench_asset_vuln_info_invalid_filter(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.asset_vuln_info(str(uuid.uuid4()), 19506,
            ('operating_system', 'contains', 'Linux'))
