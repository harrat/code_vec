@pytest.mark.remote
def test_fetch_event_json_error():
    with pytest.raises(ValueError):
        api.fetch_event_json("GW150914-v3", version=1)
    with pytest.raises(ValueError):
        api.fetch_event_json("GW150914-v3", catalog="test")
    with pytest.raises(ValueError):
        api.fetch_event_json("blah")
