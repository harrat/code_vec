@patch('pdfebc_core.compress.LOGGER')
    def test_compress_too_small_pdf(self, mock_logger):
        with tempfile.TemporaryDirectory(dir=self.trash_can.name) as tmpoutdir:
            pdf_file = create_temporary_files_with_suffixes(self.trash_can.name,
                                                            files_per_suffix=1)[0]
            pdf_file.close()
            output_path = os.path.join(tmpoutdir, os.path.basename(pdf_file.name))
            pdfebc_core.compress.compress_pdf(pdf_file.name, output_path, self.gs_binary)
            expected_not_compressing_message = pdfebc_core.compress.NOT_COMPRESSING.format(
                pdf_file.name, 0,
                pdfebc_core.compress.FILE_SIZE_LOWER_LIMIT)
            expected_done_message = pdfebc_core.compress.FILE_DONE.format(output_path)
            mock_logger.info.assert_any_call(expected_not_compressing_message)
            mock_logger.info.assert_any_call(expected_done_message)
