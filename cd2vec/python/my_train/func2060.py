def test_compute_acm_real_mfcc(self):
        af = AudioFileMFCC(self.AUDIO_FILE)
        aligner = DTWAligner(real_wave_mfcc=af)
        with self.assertRaises(DTWAlignerNotInitialized):
            aligner.compute_accumulated_cost_matrix()
