def test_mode(self):
        # with self.assertRaises(IOError):
        #    with TempDir(permissions=None) as temp:
        #        _ = temp.mode
        with TempDir('r') as temp:
            # Raises error because the tempdir is read-only
            with self.assertRaises(PermissionError):
                temp.make_file(name='bar')
        # Should be able to create the tempdir with existing read-only files
        with TempDir(
                'r', [TempPathDescriptor(name='foo', contents='foo')]) as d:
            assert d.absolute_path.exists()
            assert (d.absolute_path / 'foo').exists()
            with open(d.absolute_path / 'foo', 'rt') as i:
                assert 'foo' == i.read()
