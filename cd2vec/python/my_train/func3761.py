def test_estimator_kwargs():
    """Assert that the kwargs provided are correctly passed to the estimator."""
    atom = ATOMClassifier(X_bin, y_bin, random_state=1)
    atom.run('LR', n_calls=5, bo_params={'acq_func': 'EI'})
    assert not atom.errors

