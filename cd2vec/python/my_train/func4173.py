@pytest.mark.project
def testProjectNewSample(nwTempSample, nwLipsum, nwRef, nwTemp):
    projData = {
        "projName": "Test Sample",
        "projTitle": "Test Novel",
        "projAuthors": "Jane Doe\nJohn Doh\n",
        "projPath": nwTempSample,
        "popSample": True,
        "popMinimal": False,
        "popCustom": False,
    }
    theProject.mainConf = theConf
    assert theProject.newProject(projData)
    assert theProject.openProject(nwTempSample)
    assert theProject.projName == "Sample Project"
    assert theProject.saveProject()
    assert theProject.closeProject()

