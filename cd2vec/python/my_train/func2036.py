def test_simple():
    generated = generate(ClassWithYDefault)()
    assert generated.y == ClassWithYDefault.y  # default value provided
    assert (
        generated.x != int_generator()
    )  # no default value provided, so it will use the generator
