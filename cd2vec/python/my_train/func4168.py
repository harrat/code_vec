def test_get_headers(self):
    """test_get_headers ensures headers are being retrieved correctly"""
    self.assertEqual(self.func.get_headers(), {
        "Content-type":
        "application/vnd.ossindex.component-report-request.v1+json",
        "User-Agent":
        "jake"})
