def test_external_ids_attrs(person: Person):
    assert person.imdb_id == person.data["external_ids"]["imdb_id"]
    assert person.freebase_mid == person.data["external_ids"]["freebase_mid"]
    assert person.freebase_id == person.data["external_ids"]["freebase_id"]
    assert person.tvrage_id == person.data["external_ids"]["tvrage_id"]
    assert person.facebook_id == person.data["external_ids"]["facebook_id"]
    assert person.instagram_id == person.data["external_ids"]["instagram_id"]
    assert person.twitter_id == person.data["external_ids"]["twitter_id"]

