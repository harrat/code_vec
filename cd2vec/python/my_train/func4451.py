def test_already_open_file(binary_file):
    """Test that wkr.open passes through files that are already open."""
    with open(binary_file, 'rb') as input_file:
        for mode in ['r', 'a', 'w', 'rb', 'ab', 'wb']:
            assert wkr.open(input_file, mode) is input_file
