def test_commented_line(self):
        """
        Tests that the comments are returned normally.
        """

        comments = [
            "# Hello, World!",
            "# This is another commented line",
            "cryptopi?.com  # This is a comment at the end of a line.",
            "# This is a commented line with bittr�?.com",
            "# cryptopi?.com",
        ]

        expected = [
            "# Hello, World!",
            "# This is another commented line",
            "xn--cryptopi-ux0d.com # This is a comment at the end of a line.",
            "# This is a commented line with bittr�?.com",
            "# cryptopi?.com",
        ]

        actual = Converter(comments).get_converted()

        self.assertEqual(expected, actual)
