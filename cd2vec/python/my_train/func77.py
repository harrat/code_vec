@given(dataframes=n_time_series_with_same_index(min_n=5))
    def test_fit_predict_basic_top_down(
        self, dataframes, hierarchical_basic_top_down_model
    ):
        hierarchical_basic_top_down_model.fit(dataframes).predict()
