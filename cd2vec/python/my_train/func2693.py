def test_synchronization_coordinator_failing_context_manager():
    class MyException(Exception):
        pass

    @contextmanager
    def foo(should_fail, _sync=SYNC):
        if should_fail:
            raise MyException()
        else:
            yield

    inside_executed = False
    with pytest.raises(MultiException[MyException]):
        with MultiObject([False, True]).call(foo):
            inside_executed = True

    assert not inside_executed, 'CM body executed even though __enter__ failed in one thread'

