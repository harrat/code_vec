def test_training_percent(self):

        """Tests that the setting the training percent affects the distributions."""

        self._check_distribution(DataSuplier(list(range(10000))), 0.2)

        self._check_distribution(DataSuplier(list(range(10000)), 0.5), 0.5)
