def test_get_features_bad_id(self):
        with pytest.raises(ValueError):
            mackinac.get_genome_features('900.900')
