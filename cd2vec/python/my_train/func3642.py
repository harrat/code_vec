def test_convert_original_lightcurves():
    '''
    This tests convert_original_lightcurves.

    '''

    # remove the test-basedir
    shutil.rmtree('./test-basedir', ignore_errors=True)

    # make a basedir in the current directory
    cli.prepare_basedir('./test-basedir',
                        interactive=False)

    # make a new collection and its directories
    cli.new_collection_directories(
        './test-basedir',
        'test-collection'
    )

    basedir = './test-basedir'
    collection = 'test-collection'
    collection_subdir = os.path.join(basedir, collection)
    lightcurves_subdir = os.path.join(basedir, collection, 'lightcurves')

    with tempfile.TemporaryDirectory() as tempdir:

        # download light curves into a temporary directory
        get_lightcurves(tempdir)

        # copy over the lcformat-description.json, object-db.csv, and
        # lcreadermodule.py to collection_subdir
        dl_lcformat_json = os.path.join(tempdir,
                                        'lcc-server-demo',
                                        'lcformat-description.json')
        dl_readermodule = os.path.join(tempdir,
                                       'lcc-server-demo',
                                       'lcreadermodule.py')
        dl_objectdb = os.path.join(tempdir,
                                   'lcc-server-demo',
                                   'object-db.csv')

        shutil.copy(dl_lcformat_json, collection_subdir)
        shutil.copy(dl_readermodule, collection_subdir)
        shutil.copy(dl_objectdb, collection_subdir)

        # copy over the light curves to the lightcurves_subdir
        os.rmdir(lightcurves_subdir)
        dl_lcdir = os.path.join(tempdir, 'lcc-server-demo', 'lightcurves')
        shutil.copytree(dl_lcdir, lightcurves_subdir)

    # check if we have an lcformat-description.json, object-db.csv, and
    # lcreadermodule.py
    lcformat_json = os.path.join(collection_subdir,'lcformat-description.json')
    objectdb_csv = os.path.join(collection_subdir,'object-db.csv')
    lcreadermodule = os.path.join(collection_subdir,'lcreadermodule.py')

    assert os.path.exists(lcformat_json)
    assert os.path.exists(objectdb_csv)
    assert os.path.exists(lcreadermodule)

    # check if we have all 100 original LCs
    lcfiles = sorted(glob.glob(os.path.join(lightcurves_subdir,'*.csv')))
    assert len(lcfiles) == 100
    assert os.path.basename(lcfiles[0]) == 'HAT-215-0001809-lc.csv'
    assert os.path.basename(lcfiles[-1]) == 'HAT-265-0037533-lc.csv'

    # run convert_original_lightcurves on 5 LCs
    cli.convert_original_lightcurves(basedir,
                                     collection,
                                     max_lcs=5)

    # check if all of the original LCs have converted LCs
    assert os.path.exists(os.path.join(lightcurves_subdir,
                                       'HAT-215-0001809-csvlc.gz'))
    assert os.path.exists(os.path.join(lightcurves_subdir,
                                       'HAT-215-0004605-csvlc.gz'))
    assert os.path.exists(os.path.join(lightcurves_subdir,
                                       'HAT-215-0005039-csvlc.gz'))
    assert os.path.exists(os.path.join(lightcurves_subdir,
                                       'HAT-215-0005050-csvlc.gz'))
    assert os.path.exists(os.path.join(lightcurves_subdir,
                                       'HAT-215-0010422-csvlc.gz'))


    # check if the converted LC is readable
    lcd = hatlc.read_csvlc(os.path.join(lightcurves_subdir,
                                        'HAT-215-0001809-csvlc.gz'))
    assert lcd['objectid'] == 'HAT-215-0001809'
    assert lcd['objectinfo']['ndet'] == 11901
    assert lcd['columns'] == ['rjd',
                              'stf',
                              'xcc',
                              'ycc',
                              'aim_000',
                              'aim_001',
                              'aim_002',
                              'aie_000',
                              'aie_001',
                              'aie_002',
                              'aep_000',
                              'aep_001',
                              'aep_002']

    # remove the test-basedir
    shutil.rmtree('./test-basedir', ignore_errors=True)

