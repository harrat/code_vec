def test_compare_addition():
    num_ops = 10000
    op_range = range(0, num_ops)
    random.seed('test')

    data = map(lambda _: (random.randint(0, 1000), random.randint(0, 30)), range(0, 1000))
    intervals = map(lambda v: Interval(v[0], v[0] + v[1]), data)
    interval_list = list(intervals)
    pairs_map = map(lambda _:(random.choice(interval_list), random.choice(interval_list)), op_range)
    pairs = list(pairs_map)
    trips_map = map(lambda _:(random.choice(interval_list), random.choice(interval_list), random.choice(interval_list)),
                    op_range)
    trips = list(trips_map)

    op_1 = lambda: [combine_intervals(pair[0], pair[1]) for pair in pairs]
    op_2 = lambda: [get_minimum_bounding_interval(pair[0], pair[1]) for pair in pairs]
    op_3 = lambda: [pair[0] + pair[1] for pair in pairs]

    op_3x_1 = lambda: [combine_intervals(combine_intervals(trip[0], trip[1]), trip[2]) for trip in trips]
    op_3x_2 = lambda: [combine_three_intervals(trip[0], trip[1], trip[2]) for trip in trips]

    repetitions = 20

    op_1_result = timeit.timeit(op_1, number=repetitions)
    op_2_result = timeit.timeit(op_2, number=repetitions)
    op_3_result = timeit.timeit(op_3, number=repetitions)
    op_3x_1_result = timeit.timeit(op_3x_1, number=repetitions)
    op_3x_2_result = timeit.timeit(op_3x_2, number=repetitions)

    assert op_3x_2_result < op_3x_1_result
    assert op_1_result < op_2_result
    assert op_3_result > op_1_result
    # assert op_3_result < op_2_result
