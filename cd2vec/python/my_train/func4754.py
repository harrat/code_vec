def test_get_env_reqfile_case1(tmpdir):
    p = tmpdir.mkdir("sub").join("hello.txt")  # type: LocalPath
    p.write("content")
    paths = ["_estins.txt", p.strpath]

    # case 1: returns first existing path
    f = file_utils.get_env_reqfile(*paths)
    assert f == paths[1]

