def test_signal_weakref_complex_descriptors():
    import gc
    from easypy.lockstep import lockstep

    class Foo:
        @lockstep
        def on_test(self, a, b):
            a / b

    foo = Foo()
    register_object(foo)

    with pytest.raises(ZeroDivisionError):
        on_test(a=5, b=0, c='c')

    del foo
    gc.collect()

    on_test(a=5, b=0, c='c')

