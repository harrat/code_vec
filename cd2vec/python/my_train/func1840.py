@pytest.mark.skipif(easyagents.backends.core._tf_eager_execution_active, reason="_tf_eager_execution_active")
    @pytest.mark.tforce
    def test_train_tforce(self):
        agents.activate_tensorforce()
        self.test_train()
