def test_sdf_workspace():
    workspace = sample_circle_workspaces(nb_circles=10)
    signed_distance_field = SignedDistanceWorkspaceMap(workspace)
    assert check_jacobian_against_finite_difference(signed_distance_field)
    assert check_hessian_against_finite_difference(signed_distance_field)

