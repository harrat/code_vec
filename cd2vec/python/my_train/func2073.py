def test_cleanup():
    def delete_folder(pth):
        for sub in pth.iterdir():
            if sub.is_dir():
                delete_folder(sub)
            else:
                sub.unlink()
        pth.rmdir()

    delete_folder(pathlib.Path('list_dir/'))
    delete_folder(pathlib.Path('test_dir/'))
    pathlib.Path('load_file.yml').unlink()
    pathlib.Path('write_read_file.yml').unlink()

