def test_file_invalid_is_encrypted(self):
        observed_data = copy.deepcopy(self.valid_observed_data)
        observed_data['objects']['2'] = {
            "type": "file",
            "hashes": {
                "MD5": "8D98A25E9D0662B1F4CA3BF22D6F53E9"
            },
            "is_encrypted": False,
            "encryption_algorithm": "RSA"
        }
        self.assertFalseWithOptions(observed_data)
