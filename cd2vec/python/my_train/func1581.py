def test_delete_attribute_with_kmip_2_0(self):
        """
        Test that a DeleteAttribute request can be processed correctly
        when using KMIP 2.0 payload features.
        """
        e = engine.KmipEngine()
        e._protocol_version = contents.ProtocolVersion(2, 0)
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        attribute_factory = factory.AttributeFactory()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )
        object_group = attribute_factory.create_attribute(
            enums.AttributeType.OBJECT_GROUP,
            "Object Group 1"
        )

        e._data_session.add(secret)
        e._set_attribute_on_managed_object(
            secret,
            (
                "Object Group",
                [object_group.attribute_value]
            )
        )
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        # Confirm that the attribute was actually added by fetching the
        # managed object fresh from the database and checking it.
        managed_object = e._get_object_with_access_controls(
            "1",
            enums.Operation.DELETE_ATTRIBUTE
        )
        self.assertEqual(1, len(managed_object.names))
        self.assertEqual(1, len(managed_object.object_groups))

        payload = payloads.DeleteAttributeRequestPayload(
            unique_identifier="1",
            current_attribute=objects.CurrentAttribute(
                attribute=primitives.TextString(
                    value="Object Group 1",
                    tag=enums.Tags.OBJECT_GROUP
                )
            ),
            attribute_reference=objects.AttributeReference(
                vendor_identification="Vendor 1",
                attribute_name="Object Group"
            )
        )

        response_payload = e._process_delete_attribute(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call(
            "Processing operation: DeleteAttribute"
        )
        self.assertEqual(
            "1",
            response_payload.unique_identifier
        )
        self.assertIsNone(response_payload.attribute)

        # Confirm that the attribute was actually deleted by fetching the
        # managed object fresh from the database and checking it.
        managed_object = e._get_object_with_access_controls(
            response_payload.unique_identifier,
            enums.Operation.DELETE_ATTRIBUTE
        )
        self.assertEqual(0, len(managed_object.object_groups))

        payload = payloads.DeleteAttributeRequestPayload(
            unique_identifier="1",
            attribute_reference=objects.AttributeReference(
                vendor_identification="Vendor 1",
                attribute_name="Name"
            )
        )

        response_payload = e._process_delete_attribute(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call(
            "Processing operation: DeleteAttribute"
        )
        self.assertEqual(
            "1",
            response_payload.unique_identifier
        )
        self.assertIsNone(response_payload.attribute)

        # Confirm that the attribute was actually deleted by fetching the
        # managed object fresh from the database and checking it.
        managed_object = e._get_object_with_access_controls(
            response_payload.unique_identifier,
            enums.Operation.DELETE_ATTRIBUTE
        )
        self.assertEqual(0, len(managed_object.names))
