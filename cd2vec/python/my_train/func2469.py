def test_postimg_cc_download(self):
        with open('tests/testdata/imgload_postimg.png', 'rb') as img_file:
            sample_img_bytes = img_file.read()
        my_image_loader = ImgLoader()

        dl_img_bytes = my_image_loader.get_image_as_bytes('https://postimg.cc/NyHVjkSZ')
        self.assertEqual(sample_img_bytes, dl_img_bytes)
