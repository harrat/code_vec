def test_missing_labels(testfiles, atlas):
    # remove some labels from atlas image so numbers are non-sequential
    remove = [10, 20, 60]

    # subset atlas image
    img = check_img(atlas['image'])
    img_data = np.asarray(img.dataobj)
    for i in remove:
        img_data[img_data == i] = 0
    img = img.__class__(img_data, img.affine)

    # subset atlas info
    info = pd.read_csv(atlas['info'])
    info = info[~info['id'].isin(remove)]

    # test get expression
    out, counts = allen.get_expression_data(img, info,
                                            exact=False, return_counts=True,
                                            donors=['12876', '15496'])
    assert out.index.name == 'label'
    assert out.columns.name == 'gene_symbol'
    assert len(out) == len(info)

    assert isinstance(counts, pd.DataFrame)
    assert counts.shape == (len(info), len(testfiles))

