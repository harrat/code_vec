def test_load_json_str():
    """Test loading JSON file, with str file specifier.
    Loads files from test_save_fm_str.
    """

    file_name = 'test_fooof_all'

    data = load_json(file_name, TEST_DATA_PATH)

    assert data

def test_load_json_fobj():
    """Test loading JSON file, with file object file specifier.
    Loads files from test_save_fm_str.
    """
