@pytest.mark.vcr()
def test_access_groups_list_sort_direction_typeerror(api):
    with pytest.raises(TypeError):
        api.access_groups.list(sort=(('uuid', 1),))
