@pytest.mark.vcr()
def test_workbench_asset_vuln_info_age_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.asset_vuln_info(str(uuid.uuid4()), 19506, age='none')
