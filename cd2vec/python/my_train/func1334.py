def test_sequence_parameters():
    """Assert that every model get his corresponding parameters."""
    trainer = TrainerClassifier(['LR', 'LDA', 'LGB'],
                                n_calls=(2, 3, 4),
                                n_random_starts=(1, 2, 3),
                                bagging=[2, 5, 7])
    trainer.run(bin_train, bin_test)
    assert len(trainer.LR.bo) == 2
    assert sum(trainer.LDA.bo.index.str.startswith('Random')) == 2
    assert len(trainer.lgb.score_bagging) == 7

