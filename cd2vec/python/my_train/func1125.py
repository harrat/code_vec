def test_launch_experiment(self):
        self.create_default_experiment()
        # Confirm the launch of experiment on tmux side.
        self.assertListEqual([s.name for s in self.server.sessions], ['exp'])

        # One window for each of: default, group:hello, alone
        sess = self.server.sessions[0]
        self.assertCountEqual(
                [tmux.cluster._DEFAULT_WINDOW, 'group:hello', 'alone'],
                [w.name for w in sess.windows])
