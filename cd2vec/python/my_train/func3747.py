def test_pattern_file_output(self):
        path = self.root.make_file()

        def get_tokens(line):
            return dict(zip(('a', 'b'), line.split(' ')))

        with textoutput(
                str(path) + '{a}.{b}.txt',
                file_output_type=PatternFileOutput,
                token_func=get_tokens) as out:
            for a in range(2):
                for b in range(2):
                    out.writeline(f'{a} {b}')

        for a in range(2):
            for b in range(2):
                with open(str(path) + f'{a}.{b}.txt', 'rt') as infile:
                    assert f'{a} {b}\n' == infile.read()
