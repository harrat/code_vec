def test_image():
    print('test downsample and upload...')
    # compute parameters
    mip = 0
    size = (16, 512, 512)

    # create image dataset using cloud-volume
    img = np.random.randint(np.iinfo(np.uint8).max, 
                            size=size, dtype=np.uint8)
    chunk = Chunk(img, global_offset=[2, 32, 32])
    hierarchical_downsample(chunk, layer_type='image')
    
def test_psd_map():
    print('test downsample and upload...')
    # compute parameters
    mip = 0
    size = (16, 512, 512)
