def test_integrity_error():
    assert issubclass(py2jdbc.OperationalError, py2jdbc.DatabaseError), \
        "OperationalError is not a subclass of DatabaseError"
