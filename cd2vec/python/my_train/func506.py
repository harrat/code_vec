def test_outside_repo(self, cli_runner, tmpdir):
        with chdir(tmpdir.strpath):
            result = cli_runner.invoke(cli.use, ["lint"])
        assert "No Therapist configuration file was found." in result.output
        assert result.exception
        assert result.exit_code == 1
