@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_snippet_017(snippy):
        """Import snippet from text template.

        Try to import snippet template without any changes. This should result
        error text for end user and no files should be read. The error text
        must be the same for all content types.
        """

        file_content = mock.mock_open(read_data=Const.NEWLINE.join(Snippet.TEMPLATE_TEXT))
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '--template', '--format', 'text'])
            assert cause == 'NOK: content was not stored because it was matching to an empty template'
            Content.assert_storage(None)
            Content.assert_arglist(mock_file, './snippet-template.text', mode='r', encoding='utf-8')
