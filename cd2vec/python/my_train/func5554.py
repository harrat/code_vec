def test_create_refund(self):
        payment_request = self.client.create_payment(
            payee_payment_reference='0123456789',
            callback_url='https://example.com/api/swishcb/paymentrequests',
            amount=100,
            currency='SEK',
            message='Kingston USB Flash Drive 8 GB'
        )
        time.sleep(5)
        payment = self.client.get_payment(payment_request.id)
        refund = self.client.create_refund(
            original_payment_reference=payment.payment_reference,
            amount=100,
            currency='SEK',
            callback_url='https://example.com/api/swishcb/refunds',
            payer_payment_reference='0123456789',
            message='Refund for Kingston USB Flash Drive 8 GB'
        )
        self.assertIsNotNone(refund.id)
        self.assertIsNotNone(refund.location)
