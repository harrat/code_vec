@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_builtin_function(fixture, request):
    mylist = [3, 2, 1]
    result = _runcall(fixture, request, sorted, mylist)
    assert result == [1,2,3]

