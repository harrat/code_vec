def test_download(self):
        r = chazutsu.datasets.MovieReview.subjectivity().download(DATA_ROOT)
        target, data = r.test_data(split_target=True)
        self.assertEqual(target.shape[0], data.shape[0])

        r.make_vocab(vocab_size=1000)
        X, y = r.column("review").as_word_seq(fixed_len=20).to_batch("train", with_target=True)
        self.assertEqual(y.shape, (len(y), 1))
        self.assertEqual(X.shape, (len(y), 20, len(r.vocab)))

        backed = r.column("review").back(X)
        print(backed[:3])
        shutil.rmtree(r.root)
