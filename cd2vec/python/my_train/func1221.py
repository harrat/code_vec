def test_rsvp_lsp_add(self):
        self.model.add_rsvp_lsp('A', 'B', 'lsp_a_b_1')
        self.model.update_simulation()
        self.assertEqual(self.model.__repr__(),
                         'FlexModel(Interfaces: 40, Nodes: 10, Demands: 15, RSVP_LSPs: 5)')
