@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_nfl_olb_receiver_requested_career_stats(self, *args, **kwargs):
        # Request the career stats
        player = Player('LewiTo00')
        player = player('')

        for attribute, value in self.receiver_results_career.items():
            assert getattr(player, attribute) == value
