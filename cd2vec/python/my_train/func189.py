def test_alpha_one(self):
        prior = .3
        x, y, x_t, y_t = gen_twonorm_ssl(n_l=100, prior_l=.5, 
                                         n_u=300, prior_u=prior,
                                         n_t=100)
        x_l = x[y != 0, :]
        y_l = y[y != 0]
        x_u = x[y == 0, :]
        priorh = cpe_ene.cpe(x_l, y_l, x_u)
        prior_lower, prior_upper = prior - .1, prior + .1
        self.assertTrue(prior_lower < priorh and priorh < prior_upper)
