@pytest.mark.parametrize('method', [
    'center', 'zscore', 'minmax', 'sigmoid', 'scaled_sigmoid',
    'scaled_sigmoid_quantiles', 'robust_sigmoid', 'scaled_robust_sigmoid',
    'mixed_sigmoid'
])
def test_normalize_expression_real(testfiles, method):
    # load in data and add some NaN values for "realness"
    micro = [
        io.read_microarray(f).T
        for f in flatten_dict(testfiles, 'microarray').values()
    ]
    inds = [[5, 15, 25], [0, 10, 20]]
    for n, idx in enumerate(inds):
        micro[n].iloc[idx] = np.nan

    minmax = [
        'minmax', 'scaled_sigmoid', 'scaled_sigmoid_quantiles',
        'scaled_robust_sigmoid', 'mixed_sigmoid'
    ]

    out = correct.normalize_expression(micro, norm=method)
    for exp, idx in zip(out, inds):
        assert np.all(np.isnan(exp.iloc[idx]))
        exp = exp.dropna(axis=1, how='all')
        if method in minmax:
            assert np.allclose(exp.max(axis=0), 1)
            assert np.allclose(exp.min(axis=0), 0)
        elif method == 'robust_sigmoid':
            assert np.all(exp.max(axis=0) <= 1)
            assert np.all(exp.min(axis=0) >= 0)
        elif method in ['center', 'zscore']:
            assert np.allclose(exp.mean(axis=0), 0)
            if method == 'zscore':
                assert np.allclose(exp.std(axis=0, ddof=1), 1)

    # # batch correct: force means identical
    # out = correct.normalize_expression(micro, norm='batch')
    # assert np.allclose(*[e.mean(axis=0, skipna=True) for e in out])
    # # the NaN values should still be there, though
    # for exp, idx in zip(out, inds):
    #     assert np.all(np.isnan(exp.iloc[idx]))

    # invalid norm parameter
    with pytest.raises(ValueError):
        correct.normalize_expression(micro, norm='notanorm')
