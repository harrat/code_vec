@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'json', 'import-nginx', 'update-kafka-utc')
    def test_cli_import_solution_015(snippy):
        """Import solution based on message digest.

        Import defined solution based on message digest. File name is defined
        from command line as json file which contain one solution. One line in
        the content data was updated.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.NGINX)
            ]
        }
        content['data'][0]['data'] = content['data'][0]['data'][:4] + ('    # Changed.',) + content['data'][0]['data'][5:]
        content['data'][0]['updated'] = Content.KAFKA_TIME
        content['data'][0]['digest'] = 'c64d9cd40c15d5ce9905b282bf26c53c2ffdc32c1a7f268d6cf31364ef889a8a'
        file_content = Content.get_file_content(Content.JSON, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            json.load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '-d', '6cfe47a8880a8f81', '-f', 'one-solution.json'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-solution.json', mode='r', encoding='utf-8')
