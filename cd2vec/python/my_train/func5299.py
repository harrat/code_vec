def test_delete_attribute_from_managed_object_unsupported_attribute(self):
        """
        Test that an ItemNotFound error is raised when attempting to delete an
        unsupported attribute from a managed object.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        managed_object = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )
        args = (managed_object, ("Digital Signature Algorithm", None, None))
        self.assertRaisesRegex(
            exceptions.ItemNotFound,
            "The 'Digital Signature Algorithm' attribute is not applicable "
            "to 'SymmetricKey' objects.",
            e._delete_attribute_from_managed_object,
            *args
        )
