def test_track_date_attributes(self):
        """
        Test date attribute value tracking with a simple dictionary.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        date_values = {}

        # Verify the first date given is considered the starting range value.
        e._track_date_attributes(
            enums.AttributeType.INITIAL_DATE,
            date_values,
            1563564519
        )
        self.assertEqual(1563564519, date_values["start"])

        # Verify the second date given is considered the ending range value.
        e._track_date_attributes(
            enums.AttributeType.INITIAL_DATE,
            date_values,
            1563564521
        )
        self.assertEqual(1563564519, date_values["start"])
        self.assertEqual(1563564521, date_values["end"])

        # Verify that the third date given triggers an exception.
        args = (enums.AttributeType.INITIAL_DATE, date_values, 1563564520)
        six.assertRaisesRegex(
            self,
            exceptions.InvalidField,
            "Too many Initial Date attributes provided. "
            "Include one for an exact match. "
            "Include two for a ranged match.",
            e._track_date_attributes,
            *args
        )

        # Verify that a lower second date is interpreted as the new start date.
        date_values = {}
        date_values["start"] = 1563564521
        e._track_date_attributes(
            enums.AttributeType.INITIAL_DATE,
            date_values,
            1563564519
        )
        self.assertEqual(1563564519, date_values["start"])
        self.assertEqual(1563564521, date_values["end"])
