@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_deleted_file_is_not_returned(fixture, request):
    engine = request.getfixturevalue(fixture)
    job = engine.launch(image='alpine', command='mv a b',
                        inputs={'a': pyccc.LocalFile(os.path.join(THISDIR, 'data', 'a'))})
    job.wait()
    outputs = job.get_output()
    assert 'a' not in outputs
    assert 'b' in outputs

