def test_get_operations_insert_end(setup_text_files):
    text_file0 = parse_text_file("id", "name", "temp/text0")
    create_text_file("temp/text0", "line1\nline2\nline3")
    text_file1 = parse_text_file("id", "name", "temp/text0")
    ops = text_file0.get_operations(text_file1)
    assert len(ops["file"]) == 0
    assert len(ops["inserted"]) == 1
    assert ops["inserted"] == [[2]]
    assert len(ops["changed"]) == 0
    assert len(ops["removed"]) == 0

def test_get_operations_insert_middle(setup_text_files):
    text_file0 = parse_text_file("id", "name", "temp/text0")
    create_text_file("temp/text0", "line1\nline3\nline2")
    text_file1 = parse_text_file("id", "name", "temp/text0")
    ops = text_file0.get_operations(text_file1)
    assert len(ops["file"]) == 0
    assert len(ops["inserted"]) == 1
    assert ops["inserted"] == [[1]]
    assert len(ops["changed"]) == 0
    assert len(ops["removed"]) == 0
