@pytest.mark.vcr()
def test_workbench_export_format_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.export(format='something else')
