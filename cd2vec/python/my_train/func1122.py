def test_it_should_load_and_merge_lists_of_paths(self, examples):
        paths = sorted(examples.get_many(SORTED_FILES))

        config = load(paths)

        assert_that(config, has_entry('section', has_entry('key', 'second')))
