def test_file_upload(sp_upload):
    """Test file upload."""
    data_file = Path(_TEST_DATA).joinpath("syslog_data.csv")
    sp_upload.upload_file(data_file, "test_upload", "test_upload")

