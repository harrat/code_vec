def test_initialize_tracking_structures(self):
        """
        Test that the PolicyDirectoryMonitor can correctly initialize/reset the
        various tracking structures used for file monitoring.
        """
        m = monitor.PolicyDirectoryMonitor(
            self.tmp_dir,
            multiprocessing.Manager().dict()
        )

        m.file_timestamps["a"] = 1234
        m.policy_cache["a"] = (123.12, "b", {"c": 2})
        m.policy_files = ["a", "b"]
        m.policy_map["a"] = "b"
        m.policy_store["a"] = {"c": 2}
        m.policy_store["default"] = {"c": 3}

        m.initialize_tracking_structures()

        self.assertEqual({}, m.file_timestamps)
        self.assertEqual({}, m.policy_cache)
        self.assertEqual([], m.policy_files)
        self.assertEqual({}, m.policy_map)
        self.assertEqual(["default"], m.policy_store.keys())
        self.assertEqual({"c": 3}, m.policy_store.get("default"))
