@staticmethod
    @pytest.mark.usefixtures('yaml', 'default-solutions', 'export-time')
    def test_cli_export_solution_010(snippy):
        """Export defined solution with digest.

        Export defined solution based on message digest. File name is defined
        in solution metadata and in command line -f|--file option. This should
        result the file name and format defined by the command line option.
        In this case the created file format is yaml.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Solution.BEATS
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '-d', '4346ba4c79247430', '-f', './defined-solution.yaml'])
            assert cause == Cause.ALL_OK
            Content.assert_yaml(yaml, mock_file, './defined-solution.yaml', content)
