@patch('os.name', 'nt')
@patch('sys.stderr', new_callable=StringIO)
@patch.object(safe_print, 'open', create=True)
def test_safe_print_ignore_color_on_windows(open: MagicMock, stderr: MagicMock):
    safe_print('Text', color=colorama.Fore.RED)
    assert colorama.Fore.RED not in stderr.getvalue()

