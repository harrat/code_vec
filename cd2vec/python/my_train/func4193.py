def test_get_annotation_suff(self):
        classes = ['HP:0000739', 'HP:0000740']
        negated_classes = []
        annot_suff = self.annot_scorer.get_annotation_sufficiency(classes, negated_classes)
        assert 0 < annot_suff.simple_score < 1
        assert 0 < annot_suff.scaled_score < 1
        assert 0 < annot_suff.categorical_score < 1
