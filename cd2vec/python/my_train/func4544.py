def test_ls(mbed, testrepos):
    assertls(mbed, 'test1', [
        "[mbed]",
        "test1",
        "`- test2",
        "   `- test3",
        "      `- test4",
    ])
