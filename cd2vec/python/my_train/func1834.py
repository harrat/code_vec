def test_get_meta(self):
        """Test simple HTTP GET"""
        response = self.get_response('/', 200)
        assert 'meta' in json.loads(response.get_data(as_text=True))['artists']
