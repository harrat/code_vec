def test_go_assocs_compact():
    assocs = search_associations_compact(subject=TWIST_ZFIN,
                                          object_category='function'
    )
    assert len(assocs) == 1
    a = assocs[0]
    assert a['subject'] == TWIST_ZFIN
    objs = a['objects']
    assert 'GO:0002040' in objs

    # test reciprocal query
    for obj in objs:
        print("TEST FOR {}".format(obj))
        rassocs = search_associations_compact(object=obj,
                                              ##subject_category='gene',   # this is sometimes gene_product, protein.. in GO
                                              object_category='function',
                                              subject_taxon=DANIO,
                                              rows=-1)
        for a in rassocs:
            print("  QUERY FOR {} -> {}".format(obj,a))
        m = [a for a in rassocs if a['subject'] == TWIST_ZFIN]
        assert len(m) == 1
    
