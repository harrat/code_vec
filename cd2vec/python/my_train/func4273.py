def test_veceq():
    A = ((0,), (3.33334,), (-12000, -57.42))
    B = ((0,), (3.33333,), (-12000, -57.42))
    for a, b in zip(A, B):
        assert(geom.Vector(a) == geom.Vector(b))
    geom.set_tolerance(0.000001)
    A = ((-10000, 10000), (3.33334,), (2, 3, 4))
    B = ((10000, -10000), (3.33333,), (3, 3, 3))
    for a, b in zip(A, B):
        assert(geom.Vector(a) != geom.Vector(b))

def test_vecneg():
    cases = ((0.0,), (1,), (-2, -3.0), (34.5, -22, 130))
    expected = ((0.0,), (-1,), (2, 3.0), (-34.5, 22, -130))
    for cv, ce in zip(cases, expected):
        v = geom.Vector(cv)
        e = geom.Vector(ce)
        assert(e == -v)
