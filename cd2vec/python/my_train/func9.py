@pytest.mark.parametrize('mask, cutoff, result', cutoff_mask_list)
def test_2bspecies_count(structure, mask, cutoff, result):
    if mask is True:
        mask = generate_mask(cutoff)
    else:
        mask = None

    env_test = AtomicEnvironment(structure=structure,
                                 atom=0,
                                 cutoffs=cutoff,
                                 cutoffs_mask=mask)
    assert (len(structure.positions) == len(structure.coded_species))
    print(env_test.__dict__)

    assert (len(env_test.bond_array_2) == len(env_test.etypes))
    assert (isinstance(env_test.etypes[0], np.int8))
    assert (len(env_test.bond_array_2) == result[0])

    if len(cutoff) > 1:
        assert (np.sum(env_test.triplet_counts) == result[1])

