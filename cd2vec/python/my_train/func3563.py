def test_synchronize():
    pidom = PiDom()
    assert len(pidom.devices) == 0
    pidom.synchronize('test')
    assert len(pidom.devices) == 1
    device_name = pidom.devices[0]
    assert device_name == 'test'
    device_id = pidom._register[device_name]['device_id']
    assert device_id not in pidom._id_available
    pidom.unsynchronize('test')
    assert len(pidom.devices) == 0
    assert device_id in pidom._id_available

