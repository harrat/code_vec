def test_loader_template_links(client):
    content = client.get('/loader').data.decode('utf-8')

    assert 'jquery-ui.min.js' in content
    assert 'jquery-ui.css' in content

