def testCustomScore(self):
        def score_with_datatype_subset(datatypes, results):
            for r in results:
                datatype_scores = r['association_score']['datatypes']
                filtered_scores = [datatype_scores[dt] for dt in datatypes]
                custom_score = HarmonicSumScorer.harmonic_sum(filtered_scores)
                if custom_score:
                    yield (round(custom_score, 3), r['disease']['id'], dict(zip(datatypes, filtered_scores)))

        target_symbol = 'BRAF'
        response = self.client.get_associations_for_target(target_symbol)
        self.assertGreater(len(response), 0)
        for i, filtered_data in enumerate(score_with_datatype_subset(['genetic_association',
                                                                      'known_drug',
                                                                      'somatic_mutation'],
                                                                     response)):
            self.assertGreater(filtered_data[0], 0.)

        self.assertLess(i, len(response))
