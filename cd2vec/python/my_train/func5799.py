def test_read_write_identical(self):
        """Test the written bytes matches the read bytes."""
        for dcm_in in [rtplan_name, rtdose_name, ct_name, mr_name, jpeg_name,
                       no_ts, unicode_name, multiPN_name]:
            with open(dcm_in, 'rb') as f:
                bytes_in = BytesIO(f.read())
                ds_in = dcmread(bytes_in)
                bytes_out = BytesIO()
                ds_in.save_as(bytes_out, write_like_original=True)
                self.compare_bytes(bytes_in.getvalue(), bytes_out.getvalue())
