def test_set_header_overrides_case_insensitive(self):
        self.client.set_header_overrides({
            'user-agent': 'Test_User_Agent_String'  # Lowercase header name
        })
        self._make_request('https://www.bbc.co.uk')

        last_request = self.client.get_last_request()

        self.assertEqual('Test_User_Agent_String', last_request['headers']['User-Agent'])
