@pytest.mark.high
    def test_consumers(self):
        self.register.register(self.tasks['consumer'])

        self.assertIn(self.tasks['consumer'], self.register.consumers.values())
