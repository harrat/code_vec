def test_import_after_start():
    with freeze_time('2012-01-14'):
        assert 'tests.another_module' not in sys.modules.keys()
        from tests import another_module

        # Reals
        assert another_module.get_datetime() is datetime.datetime
        assert another_module.get_datetime() is FakeDatetime
        assert another_module.get_date() is datetime.date
        assert another_module.get_date() is FakeDate
        assert another_module.get_time() is time.time
        assert another_module.get_time() is fake_time
        assert another_module.get_localtime() is time.localtime
        assert another_module.get_localtime() is fake_localtime
        assert another_module.get_gmtime() is time.gmtime
        assert another_module.get_gmtime() is fake_gmtime
        assert another_module.get_strftime() is time.strftime
        assert another_module.get_strftime() is fake_strftime

        # Fakes
        assert another_module.get_fake_datetime() is FakeDatetime
        assert another_module.get_fake_date() is FakeDate
        assert another_module.get_fake_time() is fake_time
        assert another_module.get_fake_localtime() is fake_localtime
        assert another_module.get_fake_gmtime() is fake_gmtime
        assert another_module.get_fake_strftime() is fake_strftime

    # Reals
    assert another_module.get_datetime() is datetime.datetime
    assert not another_module.get_datetime() is FakeDatetime
    assert another_module.get_date() is datetime.date
    assert not another_module.get_date() is FakeDate
    assert another_module.get_time() is time.time
    assert not another_module.get_time() is fake_time
    assert another_module.get_localtime() is time.localtime
    assert not another_module.get_localtime() is fake_localtime
    assert another_module.get_gmtime() is time.gmtime
    assert not another_module.get_gmtime() is fake_gmtime
    assert another_module.get_strftime() is time.strftime
    assert not another_module.get_strftime() is fake_strftime

    # Fakes
    assert another_module.get_fake_datetime() is FakeDatetime
    assert another_module.get_fake_date() is FakeDate
    assert another_module.get_fake_time() is fake_time
    assert another_module.get_fake_localtime() is fake_localtime
    assert another_module.get_fake_gmtime() is fake_gmtime
    assert another_module.get_fake_strftime() is fake_strftime

