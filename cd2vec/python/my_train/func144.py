def test_init_stash_in_current_dir(self):
        """Test this because it depends on the ability
        of the storage to understand whether it should or should not create
        a directory.
        """
        prev_dir = os.getcwd()
        stash_dir = tempfile.mkdtemp()
        os.chdir(stash_dir)
        stash_path = os.path.join(stash_dir, 'stash.json')
        try:
            storage = ghost.TinyDBStorage(stash_path)
            stash = ghost.Stash(storage)
            assert os.path.isfile(stash_path) is False
            stash.init()
            assert os.path.isfile(stash_path) is True
        finally:
            os.chdir(prev_dir)
            shutil.rmtree(stash_dir, ignore_errors=True)
