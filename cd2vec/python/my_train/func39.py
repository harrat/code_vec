@pytest.mark.parametrize(('experiment', 'attributes'), [
    (986, None), (986, ATTRIBUTES), (986, 'all'),
    (69782969, None), (69782969, ATTRIBUTES), (69782969, 'all')
])
def test_get_unionization_from_experiment(experiment, attributes):
    with pytest.raises(ValueError):
        mouse._get_unionization_from_experiment(-100, structures=STRUCTURES)

    # get data from provided experiment
    data = mouse._get_unionization_from_experiment(experiment,
                                                   structures=STRUCTURES,
                                                   attributes=attributes)
    data.index = data.index.droplevel('gene_id')

    if attributes is None:
        attributes = ['expression_density']
    elif attributes == 'all':
        attributes = mouse._UNIONIZATION_ATTRIBUTES

    assert len(data.columns) == len(attributes)
    for attr in set(EXPERIMENTS[experiment].keys()).intersection(attributes):
        assert np.allclose(np.asarray(data.loc[STRUCTURES, attr]),
                           EXPERIMENTS[experiment][attr])
