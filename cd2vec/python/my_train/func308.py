@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_api_search_reference_010(server):
        """Search reference without search parameters.

        Send GET /references without defining search keywords. In this case
        all content should be returned.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '1312'
        }
        expect_body = {
            'meta': {
                'count': 2,
                'limit': 20,
                'offset': 0,
                'total': 2
            },
            'data': [{
                'type': 'reference',
                'id': Reference.GITLOG_UUID,
                'attributes': Storage.gitlog
            }, {
                'type': 'reference',
                'id': Reference.REGEXP_UUID,
                'attributes': Reference.REGEXP
            }]
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/references',
            headers={'accept': 'application/json'},
            query_string='limit=20&sort=brief')
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
