def test_get_environment_packages(self, venv):
        """Test get environment packages."""
        venv.install("selinon==1.1.0")
        assert {"package_name": "selinon", "package_version": "1.1.0"} in get_environment_packages(venv.python)
