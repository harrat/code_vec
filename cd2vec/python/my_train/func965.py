def test_closest_obs_station_uncached():
    """ Test some example locations and check against expected names """

    st = closest_obs_station(-27.470125, 153.021072, rescrape=True)
    assert "BRISBANE" in st.site_name

