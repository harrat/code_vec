def test_specnorm(self):
        layer_test(
            DynastesConv2DTranspose,
            kwargs={'filters': 7, 'kernel_size': (3, 3), 'strides': (2, 2), 'padding': 'same',
                    'kernel_normalizer': 'spectral',
                    'kernel_regularizer': 'orthogonal'},
            input_shape=(None, 16, 16, 5),
            expected_output_shape=(None, 32, 32, 7)
        )
