def test_best(self):
        agent = agents.PpoAgent(_line_world_name)
        best = save.Best()
        agent.train([best],
                    num_iterations_between_eval=1, num_episodes_per_iteration=10, num_iterations=3,
                    default_plots=False)
        os.path.isdir(best.directory)
        assert len(best.saved_agents) > 0
        (episode,reward,dir) = best.saved_agents[0]
        os.path.isdir(dir)
        agent2 = easyagents.agents.load(dir)
        assert agent2
        agent2.evaluate()
