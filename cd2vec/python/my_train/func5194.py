@staticmethod
    @pytest.mark.usefixtures('default-references', 'export-time')
    def test_cli_export_reference_004(snippy):
        """Export all references.

        Export defined reference based on message digest. File name is not
        defined in command line -f|--file option. This should result usage
        of default file name and format even when the content category is
        not explicitly defined from command line.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Reference.REGEXP
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-d', 'cb9225a81eab8ced'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './references.mkdn', content)
