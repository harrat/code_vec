def test_lock_raises_LockError(self):

        """Tests if DB already locked raises LockError"""

        if self.MOCK:
            self.assertRaises(LockError, self.device.lock)
            self.assertFalse(self.device.locked)
        else:
            self.device.unlock()  # make sure the config is not locked
            same_device = IOSXR(
                self.HOSTNAME,
                self.USERNAME,
                self.PASSWORD,
                port=self.PORT,
                lock=self.LOCK,
                logfile=self.LOG,
                timeout=self.TIMEOUT,
            )
            same_device.open()
            same_device.lock()
            # the other instance locks the config DB
