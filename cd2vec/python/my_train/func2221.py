@pytest.mark.parametrize('preset_response', RESPONSE_PRESET_TEXTS)
    def test_preset_message_response(self, bot, bot_chat_id, generate_new_update, payload, preset_response):
        response = _PresetMessageResponse(preset_response)
        update = generate_new_update(chat_id=bot_chat_id)

        message = response.respond(bot, update, payload)
        assert message
        assert is_acceptable(message.text, preset_response)
