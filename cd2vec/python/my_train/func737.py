def test_validate_args_testmode():
    args = argparse.Namespace(
        action="delete",
        value="4111111111111111",
        user_id="1234567890",
        api_key="abcd123456",
        token_scheme="nTOKENfour",
        test_mode=None
    )

    errors = tokenex.cli.validate_args(args)
    assert len(errors) == 0
    assert args == argparse.Namespace(
        action="delete",
        value="4111111111111111",
        user_id="1234567890",
        api_key="abcd123456",
        token_scheme="nTOKENfour",
        test_mode=False
    )
