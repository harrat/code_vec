def test_preprocess_args_wrong_topic(self, mock_kafka_client):
        args = mock.Mock(
            groupid="some_group",
            topic="topic34",
            partitions=None
        )
        with self.mock_get_topics() as mock_get_topics, mock.patch.object(
            sys,
            "exit",
            autospec=True,
        ) as mock_exit:
            OffsetManagerBase.preprocess_args(
                args.groupid,
                args.topic,
                args.partitions,
                mock.Mock(),
                mock_kafka_client
            )
            assert mock_get_topics.called
            assert mock_exit.called
