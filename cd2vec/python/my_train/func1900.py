def test_parse_joliet_encoded_system_identifier(tmpdir):
    indir = tmpdir.mkdir('jolietsysident')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'user-data'), 'wb') as outfp:
        outfp.write(b'''\
#cloud-config
password: password
chpasswd: { expire: False }
ssh_pwauth: True
''')

    with open(os.path.join(str(indir), 'meta-data'), 'wb') as outfp:
        outfp.write(b'''\
local-hostname: cloudimg
''')

    subprocess.call(['genisoimage', '-v', '-v', '-no-pad', '-iso-level', '4',
                     '-J', '-rational-rock', '-sysid', 'LINUX', '-volid', 'cidata',
                     '-o', str(outfile), str(indir)])

    do_a_test(tmpdir, outfile, check_joliet_ident_encoding)

def test_parse_joliet_hidden_iso_file(tmpdir):
    indir = tmpdir.mkdir('joliethiddeniso')
    outfile = str(indir)+'.iso'
    with open(os.path.join(str(indir), 'foo'), 'wb') as outfp:
        outfp.write(b'foo\n')
