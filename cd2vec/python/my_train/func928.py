def test_reloading():
    with open(file_path, "w") as f:
        f.write("""
from breathe import Breathe
from ..testutils import DoNothing

Breathe.add_commands(
    None,
    {
        "parsnip": DoNothing(),
    }
)
"""
        )
    # I have no idea why this is necessary, it's a total hack
    if PY2:
        os.remove(file_path + "c")
    engine.mimic("rebuild everything test")
    with pytest.raises(MimicFailure):
        engine.mimic("apple")
    engine.mimic("parsnip")
    assert len(Breathe.modules) == 1

