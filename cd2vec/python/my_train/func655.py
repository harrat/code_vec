@pytest.mark.parametrize("fn", [
    "test/files/rar3-symlink-unix.rar",
    "test/files/rar5-symlink-unix.rar",
])
def test_symlink(fn, tmp_path):
    with rarfile.RarFile(fn) as rf:
        assert get_props(rf, "data.txt") == "F--"
        assert get_props(rf, "data_link") == "--L"
        assert get_props(rf, "random_link") == "--L"
