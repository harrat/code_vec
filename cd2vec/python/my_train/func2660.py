def test_add_ssl(self, mock_sp, client: AppsModule, shared):
        app = shared['app']  # type: AppModel

        with pytest.raises(ValidationError):
            client.add_ssl(app.id)  # missing params

            client.add_ssl(app.id, key=123, cert='sslcert', cacerts='sslcacert')  # invalid parameter type

        mock_sp.return_value = AppMock('add_ssl')
        response = client.add_ssl(app.id, key='sslkey', cert='sslcert', cacerts=None)

        assert response.key == 'sslkey'
        assert response.cert == 'sslcert'

        app.ssl = response
