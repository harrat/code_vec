def test_post_can_timeout(self):
        rc = RestClient(jwt='a-token', telemetry=False, timeout=0.00001)

        with self.assertRaises(requests.exceptions.Timeout):
            rc.post('http://google.com')
