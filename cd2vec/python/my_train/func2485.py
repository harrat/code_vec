def test_rule_without_fix(self):
        cli_args = _create_arg_parser().parse_args(["test_rule_without_fix"])
        reload(config)
        config.REPORTER = MemoryReporter
        linter = Linter(cli_args, config)
        lines, reporter = linter.lint_lines("test_rule_without_fix", self.TEST_INPUT)

        self.assertEqual(reporter.found_issues[Category.CONVENTION], 0)
        self.assertEqual(reporter.found_issues[Category.REFACTOR], 0)
        self.assertEqual(reporter.found_issues[Category.WARNING], 1)
        self.assertEqual(reporter.found_issues[Category.ERROR], 0)
        self.assertEqual(reporter.found_issues[Category.FATAL], 0)

        self.assertEqual(lines, self.TEST_INPUT)

        self.assertEqual(reporter.messages[0].line_number, 0)
        self.assertEqual(reporter.messages[0].column, 0)
        self.assertEqual(reporter.messages[0].message, "complete open task (This is a open task)")
        self.assertEqual(reporter.messages[0].code, "open-task")
