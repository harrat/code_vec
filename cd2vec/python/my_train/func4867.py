def test_message_attributes(sns_event):
    msg_attr = sns_event.first_record.sns.message_attributes
    assert msg_attr.get("Test").value == "TestString"
    assert msg_attr.get("Test").type == "String"

