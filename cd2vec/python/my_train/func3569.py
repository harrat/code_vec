def test_modify_attribute_with_multivalued_no_index_match(self):
        """
        Test that a KmipError is raised when attempting to modify an attribute
        based on an index that doesn't exist.
        """
        e = engine.KmipEngine()
        e._protocol_version = contents.ProtocolVersion(1, 4)
        e._attribute_policy._version = e._protocol_version
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        attribute_factory = factory.AttributeFactory()

        args = (
            payloads.ModifyAttributeRequestPayload(
                unique_identifier="1",
                attribute=attribute_factory.create_attribute(
                    enums.AttributeType.NAME,
                    "Modified Name",
                    index=1
                )
            ),
        )
        self.assertRaisesRegex(
            exceptions.KmipError,
            "No matching attribute instance could be found for the specified "
            "attribute index.",
            e._process_modify_attribute,
            *args
        )
