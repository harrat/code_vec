def test_make_p2pkh_address(self):
        a = addr.make_p2pkh_address(b'\x00' * 65)
        self.assertEqual(a, helpers.ADDR[0]['p2pkh'])
        b = addr.make_p2pkh_address(b'\x11' * 65)
        self.assertEqual(b, helpers.ADDR[1]['p2pkh'])
