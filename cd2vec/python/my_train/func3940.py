def test_delete_attribute_from_managed_object_bad_single_value(self):
        """
        Test that an InvalidField error is raised when attempting to delete
        a single-valued attribute from a managed object.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        managed_object = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )
        args = (
            managed_object,
            (
                "Contact Information",
                None,
                None
            )
        )
        self.assertRaisesRegex(
            exceptions.InvalidField,
            "The 'Contact Information' attribute is not supported",
            e._delete_attribute_from_managed_object,
            *args
        )
