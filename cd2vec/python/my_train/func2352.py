def test_warning_banana_bad_n():

    for n in ["1", "2", "3"]:

        pfo_study_in = os.path.join(root_dir, "test_data", "bru_banana_bad_" + n)
        pfo_study_out = os.path.join(root_dir, "test_data", "nifti_banana")

        bru = Bruker2Nifti(pfo_study_in, pfo_study_out, study_name="banana")
        bru.correct_slope = True
        bru.verbose = 2
        if sys.version_info.major == 2:
            with pytest.raises(OSError):
                bru.convert()
        else:
            with pytest.raises(FileExistsError):
                bru.convert()
