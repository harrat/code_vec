def test_extract_polarity_v1(self):
        d = chazutsu.datasets.MovieReview.polarity_v1()
        dataset_root, extracted = d.save_and_extract(DATA_ROOT)
        path = d.prepare(dataset_root, extracted)

        pos = 0
        neg = 0

        try:
            with open(path, encoding="utf-8") as f:
                for ln in f:
                    els = ln.strip().split("\t")
                    if len(els) != 2:
                        raise Exception("data file is not constructed by label and text.")
                    if els[0] == "1":
                        pos += 1
                    else:
                        neg += 1
        except Exception as ex:
            d.clear_trush()
            self.fail(ex)
        count = d.get_line_count(path)

        d.clear_trush()
        # pos=1000, neg=1000
        self.assertEqual(count, 5331 + 5331)
        self.assertEqual(pos, 5331)
        self.assertEqual(neg, 5331)
