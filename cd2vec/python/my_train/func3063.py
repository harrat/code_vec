def test_get_rewrite_rules(self):
        self.client.set_rewrite_rules([
            (r'http://www.stackoverflow.com(.*)', r'https://www.github.com\1'),
        ])

        self.assertEqual([
            [r'http://www.stackoverflow.com(.*)', r'https://www.github.com\1'],
        ], self.client.get_rewrite_rules())
