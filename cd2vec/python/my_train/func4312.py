def test_checks():
    """Test checks on gettext files."""
    po_check = PoCheck()
    result = po_check.check_files([local_path('fr.po'),
                                   local_path('fr_errors.po')])

    # be sure we have 2 files in result
    assert len(result) == 2

    # first file has no errors
    assert not result[0][1]

    # second file has 10 errors
    assert len(result[1][1]) == 10
    errors = {}
    for report in result[1][1]:
        errors[report.idmsg] = errors.get(report.idmsg, 0) + 1
    assert errors['lines'] == 2
    assert errors['punct'] == 2
    assert errors['whitespace'] == 4
    assert errors['whitespace_eol'] == 2

