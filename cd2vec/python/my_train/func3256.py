def test_yahoo_DataReader(self):
        start = datetime(2010, 1, 1)
        end = datetime(2015, 5, 9)
        # yahoo will adjust for dividends by default
        result = web.DataReader("AAPL", "yahoo-actions", start, end)

        exp_idx = pd.DatetimeIndex(
            [
                "2015-05-07",
                "2015-02-05",
                "2014-11-06",
                "2014-08-07",
                "2014-06-09",
                "2014-05-08",
                "2014-02-06",
                "2013-11-06",
                "2013-08-08",
                "2013-05-09",
                "2013-02-07",
                "2012-11-07",
                "2012-08-09",
            ]
        )

        exp = pd.DataFrame(
            {
                "action": [
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "SPLIT",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                ],
                "value": [
                    0.52,
                    0.47,
                    0.47,
                    0.47,
                    0.14285714,
                    0.47,
                    0.43571,
                    0.43571,
                    0.43571,
                    0.43571,
                    0.37857,
                    0.37857,
                    0.37857,
                ],
            },
            index=exp_idx,
        )
        exp.index.name = "Date"
        tm.assert_frame_equal(result.reindex_like(exp).round(2), exp.round(2))

        # where dividends are not adjusted for splits
        result = web.get_data_yahoo_actions("AAPL", start, end, adjust_dividends=False)

        exp = pd.DataFrame(
            {
                "action": [
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "SPLIT",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                    "DIVIDEND",
                ],
                "value": [
                    0.52,
                    0.47,
                    0.47,
                    0.47,
                    0.14285714,
                    3.29,
                    3.05,
                    3.05,
                    3.05,
                    3.05,
                    2.65,
                    2.65,
                    2.65,
                ],
            },
            index=exp_idx,
        )
        exp.index.name = "Date"
        tm.assert_frame_equal(result.reindex_like(exp).round(4), exp.round(4))

        # test cases with "1/0" split ratio in actions -
        # no split, just chnage symbol from POT to NTR
        start = datetime(2017, 12, 30)
        end = datetime(2018, 12, 30)

        result = web.DataReader("NTR", "yahoo-actions", start, end)

        exp_idx = pd.DatetimeIndex(
            ["2018-12-28", "2018-09-27", "2018-06-28", "2018-03-28", "2018-01-02"]
        )

        exp = pd.DataFrame(
            {
                "action": ["DIVIDEND", "DIVIDEND", "DIVIDEND", "DIVIDEND", "SPLIT"],
                "value": [0.43, 0.40, 0.40, 0.40, 1.00],
            },
            index=exp_idx,
        )
        exp.index.name = "Date"
        tm.assert_frame_equal(result.reindex_like(exp).round(2), exp.round(2))
