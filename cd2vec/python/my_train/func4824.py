@staticmethod
    @pytest.mark.usefixtures('import-gitlog', 'update-regexp-utc')
    def test_api_update_reference_006(server):
        """Update one reference with PATCH request.

        Send PATCH /references/{id} to update existing snippet with digest.
        The PATCH request contains only mandatory links attribute. All other
        attributes that can be updated must be returned with their previous
        values.

        The mocked updated timestamp is intentionally the same as with the
        original content to create variation from normal where the timestamp
        is different.
        """

        storage = {
            'data': [
                Storage.gitlog
            ]
        }
        storage['data'][0]['links'] = Storage.regexp['links']
        storage['data'][0]['created'] = Content.GITLOG_TIME
        storage['data'][0]['updated'] = Content.REGEXP_TIME
        storage['data'][0]['uuid'] = Reference.GITLOG_UUID
        storage['data'][0]['digest'] = '915d0aa75703093ccb347755bfb597a16c0774b9b70626948dd378bd01310dec'
        request_body = {
            'data': {
                'type': 'reference',
                'attributes': {
                    'links': Request.regexp['links'],
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '763'
        }
        expect_body = {
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/references/' + Reference.GITLOG_UUID
            },
            'data': {
                'type': 'reference',
                'id': storage['data'][0]['uuid'],
                'attributes': storage['data'][0]
            }
        }
        result = testing.TestClient(server.server.api).simulate_patch(
            path='/api/snippy/rest/references/5c2071094dbfaa33',
            headers={'accept': 'application/vnd.api+json; charset=UTF-8'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
