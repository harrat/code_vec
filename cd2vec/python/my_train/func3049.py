@mock.patch('cherry.classifyer.Classify._classify')
    @mock.patch('cherry.classifyer.load_cache')
    def test_load_cache(self, mock_load, mock_classify):
        res = cherry.classifyer.Classify(model='foo', text=['random text'])
        mock_load.assert_not_called()
