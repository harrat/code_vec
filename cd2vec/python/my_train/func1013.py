def test_execute_with_env_override(self):
        self.store.exe = 'env'
        self.store.environment = {'FOO': 'bar'}
        data = self.store._execute('--null', '')
        assert b'\0FOO=bar\0' in data
        assert 'FOO' not in os.environ
