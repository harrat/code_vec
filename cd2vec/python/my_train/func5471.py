@staticmethod
    @pytest.mark.usefixtures('import-kafka-mkdn', 'update-three-kafka-utc')
    def test_cli_update_solution_016(snippy, editor_data):
        """Update Markdown native solution with editor.

        Update existing text formatted solution first in text format, then
        in Markdown format and then again in text format without making any
        changes. The content must not change when updated between different
        formats.

        Each update must generate different timestamp in content ``updated``
        attribute.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.KAFKA_MKDN)
            ]
        }
        content['data'][0]['brief'] = 'Testing docker log drivers again'
        content['data'][0]['digest'] = 'b5e5242d971f561675558981b4f25d1e822db282145c4246e3bd50111146096c'
        template = Content.dump_text(content['data'][0])
        editor_data.return_value = template
        content['data'][0]['updated'] = '2017-10-20T06:16:27.000001+00:00'
        cause = snippy.run(['snippy', 'update', '-d', 'c54c8a896b94ea35e', '--format', 'text', '--brief', 'Testing docker log drivers again'])
        editor_data.assert_called_with(template)
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)

        content['data'][0]['brief'] = 'Testing docker log drivers again in mkdn'
        content['data'][0]['digest'] = '243e51c8b99c80fb73c30b8d72618f8c3bc094df04184da8209f147138067083'
        template = Content.dump_mkdn(content['data'][0])
        editor_data.return_value = template
        content['data'][0]['updated'] = '2017-11-20T06:16:27.000001+00:00'
        cause = snippy.run(['snippy', 'update', '-d', 'b5e5242d971f5616', '--format', 'mkdn', '--brief', 'Testing docker log drivers again in mkdn'])  # pylint: disable=line-too-long
        editor_data.assert_called_with(template)
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)

        content['data'][0]['brief'] = 'Testing docker log drivers again'
        content['data'][0]['digest'] = 'b5e5242d971f561675558981b4f25d1e822db282145c4246e3bd50111146096c'
        template = Content.dump_text(content['data'][0])
        editor_data.return_value = template
        content['data'][0]['updated'] = '2017-12-20T06:16:27.000001+00:00'
        cause = snippy.run(['snippy', 'update', '-d', '243e51c8b99c80fb', '--format', 'text', '--brief', 'Testing docker log drivers again'])
        editor_data.assert_called_with(template)
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
