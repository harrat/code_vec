def test_failure_stats(run_reframe):
    returncode, stdout, stderr = run_reframe(
        checkpath=['unittests/resources/checks/frontend_checks.py'],
        more_options=['-t', 'SanityFailureCheck', '--failure-stats']
    )
    assert r'FAILURE STATISTICS' in stdout
    assert r'sanity        1     [SanityFailureCheck' in stdout
    assert 'Traceback' not in stdout
    assert 'Traceback' not in stderr
    assert returncode != 0

