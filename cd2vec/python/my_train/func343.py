@responses.activate
    def test_host_gethostgroup(self, host_load_data):
        host = host_load_data
        with open(resource_dir / "test_host_hostgroups.json") as data:
            wsresponses = json.load(data)
        responses.add(responses.POST,
                          self.clapi_url,
                          json=wsresponses, status=200, content_type='application/json')
        _, res = host.gethostgroup()
        assert res['centreon-prj'].id == "115"
