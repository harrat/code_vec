@pytest.mark.parametrize(
        "function, names",
        [
            (
                create_directory,
                ["/files/index.py", "/files/foo/index.py", "/files/bar/_variable.py"],
            ),
            (create_single_file, ["/files/index.py"]),
        ],
    )
    def test_file_clipped_names(self, function, names, tmpdir):
        function(tmpdir)
        factory = FunctionPathFactory(tmpdir.strpath)
        clipped = [fp.clipped_path for fp in factory.function_paths]
        assert clipped == names
