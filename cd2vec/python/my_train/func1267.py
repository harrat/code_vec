def test_end_to_end(self):
        git_source = GitSource.from_data({
            'type': 'git',
            'url': self.remote_repo_dir,
            'ref': 'master',
            'pull_submodules': True
        })
        os.makedirs(self.tmp_dir, exist_ok=True)
        local_repo_dir = git_source.retrieve(run_cmd, work_dir=self.tmp_dir)
        self.assertNotEqual(local_repo_dir, self.remote_repo_dir)
        lines = subprocess.check_output(['git', 'remote', '-v'],
                                        cwd=local_repo_dir).decode().splitlines()
        self.assertTrue(any(self.remote_repo_dir in x for x in lines))
