def test_trim(self):
        intervals = [
            [None, None, TimeValue("53.3")],
            [TimeValue("1.0"), None, TimeValue("52.3")],
            [None, TimeValue("52.3"), TimeValue("52.3")],
            [TimeValue("1.0"), TimeValue("51.3"), TimeValue("51.3")],
            [TimeValue("0.0"), None, TimeValue("53.3")],
            [None, TimeValue("60.0"), TimeValue("53.3")],
            [TimeValue("-1.0"), None, TimeValue("53.3")],
            [TimeValue("0.0"), TimeValue("-60.0"), TimeValue("0.0")],
            [TimeValue("10.0"), TimeValue("50.0"), TimeValue("43.3")]
        ]

        for interval in intervals:
            audiofile = self.load(self.AUDIO_FILE_WAVE, rs=True)
            audiofile.trim(interval[0], interval[1])
            self.assertAlmostEqual(audiofile.audio_length, interval[2], places=1)   # 53.315918
            audiofile.clear_data()
