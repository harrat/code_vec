def test_nfl_game_limited_information(self):
        fields = {
            'attendance': 22000,
            'date': 'Sunday Sep 8, 1940',
            'duration': None,
            'stadium': 'Forbes Field',
            'time': None
        }

        mock_field = """Sunday Sep 8, 1940
Stadium: Forbes Field
Attendance: 22,000
Logos via Sports Logos.net / About logos
"""

        m = MockBoxscoreData(MockField(mock_field))

        self.boxscore._parse_game_date_and_location(m)
        for field, value in fields.items():
            assert getattr(self.boxscore, field) == value
