def test_without_tukio_factory(self):
        """
        When the Tukio task factory is not set in the loop, all coroutines must
        be wrapped in a regular `asyncio.Task` object regardless of whether
        they are registered Tukio tasks or not.
        """
        # Reset to default task factory
        self.loop.set_task_factory(None)

        # Create and run a Tukio task implemented as a task holder
        task = asyncio.ensure_future(self.holder.do_it('bar'))
        self.assertIsInstance(task, asyncio.Task)
        res = self.loop.run_until_complete(task)
        self.assertEqual(res, MY_TASK_HOLDER_RES)

        # Create and run a Tukio task implemented as a coroutine
        task = asyncio.ensure_future(my_coro_task(None))
        self.assertIsInstance(task, asyncio.Task)
        res = self.loop.run_until_complete(task)
        self.assertEqual(res, MY_CORO_TASK_RES)

        # Create and run a regular `asyncio.Task`
        task = asyncio.ensure_future(other_coro(None))
        self.assertIsInstance(task, asyncio.Task)
        res = self.loop.run_until_complete(task)
        self.assertEqual(res, None)

        # Run a generator (e.g. as returned by `asyncio.wait`)
        # The task factory is also called in this situation and is passed the
        # generator object.
        t1 = asyncio.ensure_future(my_coro_task(None))
        t2 = asyncio.ensure_future(other_coro(None))
        self.assertIsInstance(t1, asyncio.Task)
        self.assertIsInstance(t2, asyncio.Task)
        gen = asyncio.wait([t1, t2])
        # A task is created inside `asyncio.run_until_complete` anyway...
        task = asyncio.ensure_future(gen)
        self.assertIsInstance(task, asyncio.Task)
        res = self.loop.run_until_complete(task)
        self.assertEqual(res, ({t1, t2}, set()))
