def test_flex_alloc_default_partition_all(make_flexible_job):
    job = make_flexible_job('all')
    prepare_job(job)
    assert job.num_tasks == 16

