@pytest.mark.skipif(not is_connected(), reason="Need an internet connection")
def test_match_to_alias():
    imports = ["dateutil"]
    requirements = ["python-dateutil"]
    aliases, unsed_req = match_to_alias(imports, requirements)
    print(requirements)
    print(aliases)
    for i, r in zip(imports, requirements):
        assert aliases[i] == r
    assert unsed_req == []

