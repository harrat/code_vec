def test_hybrid_eltorito_remove_with_dir(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('eltoritonofiles')
    outfile = str(indir)+'.iso'
    indir.mkdir('a')
    with open(os.path.join(str(indir), 'boot'), 'wb') as outfp:
        outfp.write(b'boot\n')
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-c', 'boot.cat', '-b', 'boot', '-no-emul-boot',
                     '-o', str(outfile), str(indir)])

    # Now open up the ISO with pycdlib and check some things out.
    iso = pycdlib.PyCdlib()

    iso.open(str(outfile))

    iso.rm_eltorito()
    iso.rm_file('/BOOT.;1')
    iso.rm_directory('/A')

    do_a_test(iso, check_nofiles)

    iso.close()

def test_hybrid_modify_in_place_dirrecord_spillover(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('modifyinplaceonefile')
    outfile = str(indir)+'.iso'
    dir1 = indir.mkdir('dir1')
    for i in range(1, 49):
        fname = os.path.join(str(dir1), 'foo%.2d' % (i))
        with open(fname, 'wb') as outfp:
            outfp.write(b'f\n')
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-o', str(outfile), str(indir)])
