@staticmethod
    @pytest.mark.parametrize('server', [['server', '--server-host', 'localhost:8080', '-q']], indirect=True)
    @pytest.mark.usefixtures('default-references')
    def test_api_search_reference_012(server):
        """Search reference with GET.

        Send GET /references and search keywords from all attributes. The
        search query matches to two references and both of them are returned.
        The response JSON is sent as pretty printed.

        TODO: The groups refactoring changed the lenght from 2196 to 2278.
              Why so much? Is there a problem in the result JSON?

              This was checked (but what was it?) and this is a left over
              comment?
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '2362'
        }
        expect_body = {
            'meta': {
                'count': 2,
                'limit': 20,
                'offset': 0,
                'total': 2
            },
            'data': [{
                'type': 'reference',
                'id': Reference.GITLOG_UUID,
                'attributes': Storage.gitlog
            }, {
                'type': 'reference',
                'id': Reference.REGEXP_UUID,
                'attributes': Reference.REGEXP
            }]
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/references',
            headers={'accept': 'application/vnd.api+json; charset=UTF-8'},
            query_string='sall=python%2CGIT&limit=20&sort=brief')
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
