@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'import-nginx', 'update-kafka-utc')
    def test_cli_import_solution_017(snippy):
        """Import solution based on message digest.

        Import defined solution based on message digest. File name is defined
        from command line as text file which contain one solution. One line
        in the content data was updated. The file extension is '*.text' in
        this case.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.NGINX)
            ]
        }
        content['data'][0]['data'] = content['data'][0]['data'][:4] + ('    # Changed.',) + content['data'][0]['data'][5:]
        content['data'][0]['description'] = 'Changed.'
        content['data'][0]['updated'] = Content.KAFKA_TIME
        content['data'][0]['digest'] = 'ce3f7a0ab75dc74f7bbea68ae323c29b2361965975c0c8d34897551149d29118'
        file_content = Content.get_file_content(Content.TEXT, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '-d', '6cfe47a8880a8f81', '-f', 'one-solution.text'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-solution.text', mode='r', encoding='utf-8')
