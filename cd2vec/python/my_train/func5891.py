def test_extract_rating(self):
        d = chazutsu.datasets.MovieReview.rating()
        dataset_root, extracted = d.save_and_extract(DATA_ROOT)
        path = d.prepare(dataset_root, extracted)

        try:
            with open(path, encoding="utf-8") as f:
                for ln in f:
                    els = ln.strip().split("\t")
                    if len(els) != 2:
                        raise Exception("data file is not constructed by label and text.")
        except Exception as ex:
            d.clear_trush()
            self.fail(ex)

        count = d.get_line_count(path)

        d.clear_trush()
        self.assertTrue(count > 0)
