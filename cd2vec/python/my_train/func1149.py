def test_no_permissions(self):
        with self.assertRaises(IOError):
            path = self.root.make_file(permissions='r')
            check_access(path, 'w')
