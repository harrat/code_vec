def test_abspath_home(self):
        home = os.path.expanduser("~")
        assert abspath(Path('~/foo')) == Path(home) / 'foo'
