def test_read_relic_info(self):
        runner('git tag -a 1.0.0 -m "test message"')
        # Generate RELIC-INFO
        relic.release.get_info()
        shutil.rmtree('.git', onerror=onerror)
        # Read it back without git
        v = relic.release.get_info()
        assert v.short == '1.0.0'
