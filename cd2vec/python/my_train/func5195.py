def test_add_sheets_from_response(self):
        dummy_response_create = json.loads(open_fixture("add_sheets.json"))
        self.spreadsheet._add_sheets_from_response(response=dummy_response_create, reply_type="addSheet")
        assert len(self.spreadsheet.sheets) == 7
        dummy_response_duplicate = json.loads(open_fixture("duplicate_sheets.json"))
        self.spreadsheet._add_sheets_from_response(response=dummy_response_duplicate, reply_type="duplicateSheet")
        assert len(self.spreadsheet.sheets) == 8
