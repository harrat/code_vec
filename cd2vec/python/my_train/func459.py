def test_SGD(use_GPU):
    inputs = np.asarray([[0, 0], [0, 1], [1, 0], [1, 1]], dtype=np.float32)
    targets = np.asarray([[0], [1], [1], [0]], dtype=np.float32)

    ff = hf.FFNet([2, 5, 1], debug=False, use_GPU=use_GPU)

    ff.run_epochs(inputs, targets, optimizer=hf.opt.SGD(l_rate=1),
                  max_epochs=10000, print_period=None)

    outputs = ff.forward(inputs, ff.W)

    assert ff.loss.batch_loss(outputs, targets) < 1e-3

