def test_no_params(self):
        tech = TechMS()

        data = {'get': '/test'}
        call = TechState.get_call_data(None, data)
        res = tech(call, {})
        assert res['statusCode'] == 200
