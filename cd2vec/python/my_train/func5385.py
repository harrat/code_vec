def test_add_multiple_choices(cli, data_dir, config):
    p1 = Project(1, 'test project', Project.STATUS_ACTIVE)
    p1.activities = [Activity(2, 'test activity')]
    p2 = Project(2, 'test project 2', Project.STATUS_ACTIVE)
    p2.activities = [Activity(3, 'test activity 2')]
    p = ProjectsDb(str(data_dir))
    p.update([p1, p2])

    cli('project', ['alias', 'test project'], input='1\ntest_alias')

    with open(config.path, 'r') as f:
        lines = f.readlines()

    assert 'test_alias = 2/3\n' in lines

def test_add_inactive_project(cli, data_dir):
    project = Project(1, 'test project', Project.STATUS_FINISHED)
    project.activities = [Activity(2, 'test activity')]
    p = ProjectsDb(str(data_dir))
    p.update([project])
