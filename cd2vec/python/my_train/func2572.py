def test_get_with_sort(self):
        """Test simple HTTP GET"""
        response = self.get_response('/artists', 200, params={'sort': 'Name'})
        assert json.loads(response.get_data(as_text=True))[u'resources'][0]['Name'] == 'A Cor Do Som'
