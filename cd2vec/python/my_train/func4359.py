def test_call(planner):
    # pylint: disable=invalid-name,protected-access
    state = None
    for timestep in range(HORIZON):
        action = planner(state, timestep)

        assert len(action) == len(planner._plan)
        for (_, a1), a2 in zip(action.items(), planner._plan):
            assert np.allclose(a1, a2[timestep])

