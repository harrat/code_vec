@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_function_with_closure_mod(fixture, request):
    od = _runcall(fixture, request, function_tests.fn_withmod, [('a', 'b'), ('c', 'd')])
    assert od.__class__.__name__ == 'OrderedDict'
    assert list(od.keys()) == ['a', 'c']
    assert list(od.values()) == ['b', 'd']

