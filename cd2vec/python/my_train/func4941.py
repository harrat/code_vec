@pytest.mark.skipif(not hasattr(time, "clock"),
                    reason="time.clock was removed in Python 3.8")
@utils.cpython_only
def test_ticking_time_clock():
    with freeze_time('2012-01-14 03:21:34', tick=True):
        first = time.clock()
        time.sleep(0.001)  # Deal with potential clock resolution problems
        with freeze_time('2012-01-14 03:21:35', tick=True):
            second = time.clock()
            time.sleep(0.001)  # Deal with potential clock resolution problems
