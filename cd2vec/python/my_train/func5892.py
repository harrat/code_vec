def test_write_wizir_catalog():
    d = tmpdir()
    fpath = path.join(d.path, 'mark_a.reg')
    std_catalog = get_catalog('MARK_A')
    sl.write_ds9_regions(std_catalog, fpath, WCS=True)
    cat = sl.read_ds9_regions(fpath)
    assert len(cat) == len(std_catalog)

