def test_argument_filters_search(cli, projects_db):
    lines = cli('project', ['list', 'active']).splitlines()

    assert set(lines) == set([
        'A [test]   43 active project',
        'A [test]   44 2nd active project',
    ])
