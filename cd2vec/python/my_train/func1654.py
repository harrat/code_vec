def test_is_allowed_by_operation_policy_denied(self):
        """
        Test that access denied by operation policy is processed correctly.
        """
        e = engine.KmipEngine()
        e.is_allowed = mock.Mock(return_value=False)

        result = e._is_allowed_by_operation_policy(
            'test_policy',
            ['test_user', ['test_group_A', 'test_group_B']],
            'test_user',
            enums.ObjectType.SYMMETRIC_KEY,
            enums.Operation.GET
        )

        e.is_allowed.assert_any_call(
            'test_policy',
            'test_user',
            'test_group_A',
            'test_user',
            enums.ObjectType.SYMMETRIC_KEY,
            enums.Operation.GET
        )
        e.is_allowed.assert_any_call(
            'test_policy',
            'test_user',
            'test_group_B',
            'test_user',
            enums.ObjectType.SYMMETRIC_KEY,
            enums.Operation.GET
        )
        self.assertFalse(result)
