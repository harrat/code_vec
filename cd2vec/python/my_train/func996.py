def test_retrieve_from_element(self):
        element = BeautifulSoup(PAGE.strip(), "html.parser")
        c = Conference("http://dummy")
        result = c.retrieve_from_element(element, "J00-1")
        papers = result.papers
        self.assertEqual(len(papers), 1)
        for k in result.papers:
            self.assertEqual(k, "J00-1")
            self.assertEqual(len(papers[k]), 7)
            self.assertEqual(papers[k][0].title, "Introduction to the Special issue on finite state methods in NLP")
            self.assertEqual(papers[k][-1].title, "Advertisements")
