def test_prepare_no_smt(fake_job, slurm_only):
    fake_job.use_smt = None
    prepare_job(fake_job)
    with open(fake_job.script_filename) as fp:
        assert re.search(r'--hint', fp.read()) is None
