def test_stop_with_non_parsable_entry(cli, entries_file):
    entries = """20/01/2014
alias 10:00-ff:ff Play ping-pong
"""

    entries_file.write(entries)
    with freeze_time('2014-01-20'):
        output = cli('stop', ['Play ping-pong'])

    assert output.startswith('Error: Parse error')

