def test_fake_default_path():
    assert not os.path.isfile(settings.DB)

    ua = UserAgent(cache=True, use_cache_server=False)

    assert settings.DB == ua.path

    assert os.path.isfile(settings.DB)

