def test_pickle_dumpsloads_nested():
    orig = udict({'one': {'two': 'one->two'}})
    unpickled = pickle.loads(pickle.dumps(orig))
    assert items(unpickled) == items(orig)

