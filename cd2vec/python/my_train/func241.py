@pytest.mark.parametrize('name,description,id', [('test', None, 1),
                                                 ('what', 'nothing', 2)],)
def test_create_deck(name: str, description: str, id: int) -> None:
    spacedr.create_deck(name, description)
    deck = spacedr.get_deck_by_id(id)

    assert deck.name == name
    assert deck.description == description

