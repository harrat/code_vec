@freeze_time('2014-02-21')
def test_commit_previous_file_previous_month(cli, data_dir, config):
    efg = EntriesFileGenerator(data_dir, '%m_%Y.tks')
    efg.expand(datetime.date(2014, 1, 1)).write(
        "01/01/2014\nalias_1 2 january"
    )
    efg.expand(datetime.date(2014, 2, 1)).write(
        "01/02/2014\nalias_1 4 february"
    )
    efg.patch_config(config)

    stdout = cli('commit', ['--yes'])

    assert 'january' in stdout
    assert 'february' in stdout

