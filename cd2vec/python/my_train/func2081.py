def test_count_calls_decorator(self):
        @count_calls
        def test():
            pass

        for i in xrange(10):
            test()
        self.assertEqual(counter("test_calls").get_count(), 10)
