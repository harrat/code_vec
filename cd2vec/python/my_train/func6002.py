def test_guard_deleted_file_restores_contents_test_file(self):
        @guard(TestFileGuardDecorator.TEST_TEXT_FILE_PATH)
        def function_that_deletes_the_file():
            os.remove(TestFileGuardDecorator.TEST_TEXT_FILE_PATH)

            # make sure that the file does not exist after running the function
            is_file = path.is_file()
            self.assertFalse(is_file, 'File does exist.')

        # make sure that the file is existent
        path = Path(TestFileGuardDecorator.TEST_TEXT_FILE_PATH)
        is_file = path.is_file()
        self.assertTrue(is_file, 'File does not exist.')

        function_that_deletes_the_file()

        self._assert_file_content_equals(TestFileGuardDecorator.TEST_FILE_CONTENTS)
