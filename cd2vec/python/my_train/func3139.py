def test_player_contract_returns_contract(self):
        contract = self.player.contract

        expected = {
            '2006': {
                'age': '23',
                'team': 'Detroit Tigers',
                'salary': '$980,000'
            },
            '2007': {
                'age': '24',
                'team': 'Detroit Tigers',
                'salary': '$1,030,000'
            },
            '2008': {
                'age': '25',
                'team': 'Detroit Tigers',
                'salary': '$1,130,000'
            },
            '2009': {
                'age': '26',
                'team': 'Detroit Tigers',
                'salary': '$3,675,000'
            },
            '2010': {
                'age': '27',
                'team': 'Detroit Tigers',
                'salary': '$6,850,000'
            },
            '2011': {
                'age': '28',
                'team': 'Detroit Tigers',
                'salary': '$12,850,000'
            },
            '2012': {
                'age': '29',
                'team': 'Detroit Tigers',
                'salary': '$20,000,000'
            },
            '2013': {
                'age': '30',
                'team': 'Detroit Tigers',
                'salary': '$20,000,000'
            },
            '2014': {
                'age': '31',
                'team': 'Detroit Tigers',
                'salary': '$20,000,000'
            },
            '2015': {
                'age': '32',
                'team': 'Detroit Tigers',
                'salary': '$28,000,000'
            },
            '2016': {
                'age': '33',
                'team': 'Detroit Tigers',
                'salary': '$28,000,000'
            },
            '2017': {
                'age': '34',
                'team': 'Houston Astros',
                'salary': '$28,000,000'
            },
            '2018': {
                'age': '35',
                'team': 'Houston Astros',
                'salary': '$28,000,000'
            },
            '2019': {
                'age': '36',
                'team': 'Houston Astros',
                'salary': '$28,000,000'
            }
            }

        assert contract == expected
