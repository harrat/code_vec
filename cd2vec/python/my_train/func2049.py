def test_lca_merge(tmpdir):
    # setup saga and get init hash
    os.chdir(tmpdir)
    init_blurb = run_cmd("saga init")
    init_hash = blurb_to_hash(str(tmpdir), init_blurb)
    # make a branch a1, a2, and b
    run_cmd("saga branch a1")
    run_cmd("saga branch a2")
    run_cmd("saga branch b")
    # make one commit on a1 and a2
    run_cmd("saga checkout a1")
    run_cmd("saga commit --allow-empty -m \"ack\"")
    # but we add a random file on a2 so the commit has is different
    run_cmd("saga checkout a2")
    random_file("file")
    run_cmd("saga add file")
    run_cmd("saga commit -m \"ack\"")
    # and then merge them 
    merge_blurb = run_cmd("saga merge a1")
    merge_hash = blurb_to_hash(str(tmpdir), merge_blurb)
    # make one commit on b
    run_cmd("saga checkout b")
    b_blurb = run_cmd("saga commit --allow-empty -m \"back\"")
    b_hash = blurb_to_hash(str(tmpdir), b_blurb)
    # check that the lca is the initial commit, from both
    repo = Repository(Path(tmpdir))
    repo.debug()
    commit_graph = CommitGraph(repo)
    need_merge, lcas = commit_graph.least_common_ancestors(merge_hash, b_hash)
    assert len(lcas) == 1
    assert lcas.pop() == init_hash

