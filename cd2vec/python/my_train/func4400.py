def test_flex_alloc_nodes_negative(make_flexible_job):
    job = make_flexible_job(-1, sched_access=['--constraint=f1'])
    with pytest.raises(JobError):
        prepare_job(job)
