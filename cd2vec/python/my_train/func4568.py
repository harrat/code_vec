def test_pin_numbering_lock(self):
        """
        Test that pin_numbering can't be changed after a motor's definition
        """
        import l293d as d
        d.Config.pin_numbering = 'BcM'
        m1 = d.DC(4, 5, 6)
        error = 'No error'
        try:
            d.Config.pin_numbering = 'BoaRD'
        except ValueError as e:
            error = str(e)
        self.assertEqual(
            error, 'Pin numbering format cannot be changed '
                   'if motors already exist. Set this at '
                   'the start of your script.')
        m1.remove()
        d.Config.pin_numbering = 'BOARD'
        reload(d.driver)
