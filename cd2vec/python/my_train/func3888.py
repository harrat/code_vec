def test_read(self):
        self.maxDiff = None
        with tempfile.NamedTemporaryFile() as source:
            source.write(DEFAULT_CONTENT.encode("ascii"))
            source.flush()
            dns = Hostapd(source.name)
            dns.read()
            self.assertDictEqual(dns.config, DEFAULT_CONFIG)
