def test_not_multiple_instance(self):
		who = model.Person()
		n = model.Name(content="Test")
		who.identified_by = n

		model.factory.multiple_instances_per_property = "error"
		self.assertRaises(model.DataError, who.__setattr__, 'identified_by', n)
		self.assertEqual(who.identified_by, [n])

		model.factory.multiple_instances_per_property = "drop"
		who.identified_by = n
		self.assertEqual(who.identified_by, [n,n])		
		# and check that only serialized once
		js = model.factory.toJSON(who)
		self.assertEqual(len(js['identified_by']), 1)

		model.factory.multiple_instances_per_property = "allow"
		js = model.factory.toJSON(who)
		self.assertEqual(len(js['identified_by']), 2)
