@staticmethod
    @pytest.mark.usefixtures('import-gitlog', 'caller')
    def test_api_update_reference_004(server):
        """Try to update reference with malformed request.

        Try to send PUT /references/{id} to update resource with malformed
        JSON request.
        """

        storage = {
            'data': [
                Storage.gitlog
            ]
        }
        request_body = {
            'data': Const.NEWLINE.join(Request.regexp['data']),
            'brief': Request.regexp['brief'],
            'groups': Request.regexp['groups'],
            'tags': Const.DELIMITER_TAGS.join(Request.regexp['tags']),
            'links': Const.DELIMITER_LINKS.join(Request.regexp['links'])
        }
        expect_headers_p3 = {'content-type': 'application/vnd.api+json; charset=UTF-8', 'content-length': '1257'}
        expect_headers_p2 = {'content-type': 'application/vnd.api+json; charset=UTF-8', 'content-length': '1297'}
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '400',
                'statusString': '400 Bad Request',
                'module': 'snippy.testing.testing:123',
                'title': 'json media validation failed'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_put(
            path='/api/snippy/rest/references/5c2071094dbfaa33',
            headers={'accept': 'application/json'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_400
        assert result.headers in (expect_headers_p2, expect_headers_p3)
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
