def test_custom_property_name_long(self):
        indicator = copy.deepcopy(self.valid_indicator)
        long_property_name = 'my_new_property_' * 16
        indicator[long_property_name] = "abc123"
        results = validate_parsed_json(indicator, self.options)
        self.assertEqual(results.is_valid, False)
