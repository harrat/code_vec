@pytest.mark.remote
def test_get_urls_deprecated_tag():
    # test `tag` prints a warning
    pytest.deprecated_call(
        locate.get_urls,
        "L1",
        1187007040,
        1187009088,
        tag="TEST",
    )
