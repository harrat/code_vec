@staticmethod
    @pytest.mark.usefixtures('exists_true', 'access_true')
    def test_export_shell_completion_002(snippy):
        """Export bash completion script.

        Export Bash completion script. In this case the ``--file`` option is
        set to local path so that only the filename is changed from the
        default.
        """

        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            cause = snippy.run(['snippy', 'export', '--complete', 'bash', '-f', './snippy'])
            assert cause == Cause.ALL_OK
            Content.assert_arglist(mock_file, './snippy', mode='w', encoding='utf-8')
            file_handle = mock_file.return_value.__enter__.return_value
            file_handle.write.assert_called_with(Content.COMPLETE_BASH)
