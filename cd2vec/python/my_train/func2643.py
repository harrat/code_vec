def test_signing_data(self):
        c = Crypto(1024)
        msg = "There is nothing permanent except change."
        keys = c.generate_keys()
        bad_keys = c.generate_keys()
        sig = Crypto.sign(keys["private"], msg)
        assert(Crypto.verify_signed(keys["public"], sig, msg))
        assert(Crypto.verify_signed(keys["public"], sig, msg + "1") == False)
        assert(Crypto.verify_signed(bad_keys["public"], sig, msg) == False)
