def test_subprocess_environment_preserved(subprocess_engine, set_env_var):
    job = subprocess_engine.launch(command='echo $NULL123', image='python:2.7-slim')
    job.wait()
    assert job.stdout.strip() == 'nullabc'

