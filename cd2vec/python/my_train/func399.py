def test_Bootstrap(generate_nikamaps):
    filenames = generate_nikamaps

    primary_header = fits.getheader(filenames[0], 0)
    weighted_noises = primary_header["NOISE"] / np.sqrt(np.arange(1, primary_header["NMAPS"] + 1))

    # Weighted average
    bs = Bootstrap(filenames, n=1, n_bootstrap=10)
    data = bs()

    # Trouble to find a proper test for this
