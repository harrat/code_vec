def test_cmake_initial_cache_as_global_option(tmpdir):
    project = "hello-no-language"
    prepare_project(project, tmpdir)
    initialize_git_repo_and_commit(tmpdir, verbose=True)

    initial_cache = tmpdir.join("initial-cache.txt")
    initial_cache.write("""set(MY_CMAKE_VARIABLE "1" CACHE BOOL "My cache variable")""")

    try:
        with execute_setup_py(tmpdir, ["-C%s" % str(initial_cache), "build"], disable_languages_test=True):
            pass
    except SystemExit as exc:
        assert exc.code == 0

    cmakecache_txt = tmpdir.join(CMAKE_BUILD_DIR(), "CMakeCache.txt")
    assert cmakecache_txt.exists()
    assert get_cmakecache_variables(str(cmakecache_txt)).get('MY_CMAKE_VARIABLE', (None, None)) == ('BOOL', '1')

