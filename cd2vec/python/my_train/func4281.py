@mock.patch('thundra.integrations.botocore.BaseIntegration.actual_call')
def test_sns_message_masked(mock_actual_call, mock_sns_response):
    mock_actual_call.return_value = mock_sns_response
    ConfigProvider.set(config_names.THUNDRA_TRACE_INTEGRATIONS_AWS_SNS_MESSAGE_MASK, 'true')

    try:
        sns = boto3.client('sns', region_name='us-west-2')
        sns.publish(
            TopicArn='Test-topic',
            Message='Hello Thundra!',
        )
    except botocore_errors:
        pass
    finally:
        tracer = ThundraTracer.get_instance()
        span = tracer.get_spans()[0]
        assert span.class_name == 'AWS-SNS'
        assert span.domain_name == 'Messaging'
        assert span.get_tag('operation.type') == 'WRITE'
        assert span.get_tag('aws.sns.topic.name') == 'Test-topic'
        assert span.get_tag('aws.request.name') == 'Publish'
        assert span.get_tag(constants.SpanTags['TRACE_LINKS']) == ['MessageID_1']
        assert span.get_tag(constants.AwsSNSTags['MESSAGE']) == None
        tracer.clear()
