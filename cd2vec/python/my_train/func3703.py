def test_post_no_html_form_data(self):
        """Test POSTing a resource with no form data."""
        response = self.app.post('/artists',
                data=dict())
        assert response.status_code == 400
