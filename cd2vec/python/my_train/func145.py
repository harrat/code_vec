def test_fg_load():
    """Test load into FOOOFGroup. Note: loads files from test_core_io."""

    file_name_res = 'test_fooofgroup_res'
    file_name_set = 'test_fooofgroup_set'
    file_name_dat = 'test_fooofgroup_dat'

    # Test loading just results
    tfg = FOOOFGroup(verbose=False)
    tfg.load(file_name_res, TEST_DATA_PATH)
    assert len(tfg.group_results) > 0
    # Test that settings and data are None
    #   Except for aperiodic mode, which can be inferred from the data
    for setting in OBJ_DESC['settings']:
        if setting is not 'aperiodic_mode':
            assert getattr(tfg, setting) is None
    assert tfg.power_spectra is None

    # Test loading just settings
    tfg = FOOOFGroup(verbose=False)
    tfg.load(file_name_set, TEST_DATA_PATH)
    for setting in OBJ_DESC['settings']:
        assert getattr(tfg, setting) is not None
    # Test that results and data are None
    for result in OBJ_DESC['results']:
        assert np.all(np.isnan(getattr(tfg, result)))
    assert tfg.power_spectra is None

    # Test loading just data
    tfg = FOOOFGroup(verbose=False)
    tfg.load(file_name_dat, TEST_DATA_PATH)
    assert tfg.power_spectra is not None
    # Test that settings and results are None
    for setting in OBJ_DESC['settings']:
        assert getattr(tfg, setting) is None
    for result in OBJ_DESC['results']:
        assert np.all(np.isnan(getattr(tfg, result)))

    # Test loading all elements
    tfg = FOOOFGroup(verbose=False)
    file_name_all = 'test_fooofgroup_all'
    tfg.load(file_name_all, TEST_DATA_PATH)
    assert len(tfg.group_results) > 0
    for setting in OBJ_DESC['settings']:
        assert getattr(tfg, setting) is not None
    assert tfg.power_spectra is not None
    for meta_dat in OBJ_DESC['meta_data']:
        assert getattr(tfg, meta_dat) is not None

def test_fg_report(skip_if_no_mpl):
    """Check that running the top level model method runs."""
