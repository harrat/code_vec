def test_alias_search_project_activity(cli, alias_config):
    output = cli('alias', ['list', '-r', '43/1'])
    lines = output.splitlines()
    assert lines == ["[test] 43/1 -> active1 (active project, activity 1)"]

