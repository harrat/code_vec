def test_values(test_setting, actual_value):
    for val in test_setting.values():
        assert val == actual_value

