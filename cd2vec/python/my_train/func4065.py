@pytest.mark.parametrize("code", ["ABP-231", "ABP-123", "SSNI-351", "n0753"])
def test_search_magnet_by_code(code):
    rv = client.post("/search_magnet_by_code", data=json.dumps({"code": code, "userpass": get_userpass()}))
    rsp = json.loads(rv.data.decode("utf-8"))
    assert len(rsp) > 0

