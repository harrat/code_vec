@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'default-solutions', 'import-kafka-utc', 'export-time')
    def test_cli_export_solution_020(snippy):
        """Export defined solution with digest.

        Export defined solution based on message digest. File name is not
        defined from command line ``-f|--file`` option. In this case there
        are extra spaces around the ``filename`` attribute which does not
        prevent using the stored filename.

        Because the filename is available, the export filename must follow
        ``filename`` attribute and file format.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Content.deepcopy(Solution.KAFKA)
            ]
        }
        content['data'][0]['filename'] = ' kubernetes-docker-log-driver-kafka.txt '
        content['data'][0]['digest'] = 'ee3f2ab7c63d6965ac2531003807f00caee178f6e1cbb870105c7df86e6d5be2'
        file_content = Content.get_file_content(Content.TEXT, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '-f', './kafka.text'])
            assert cause == Cause.ALL_OK

        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '-d', 'ee3f2ab7c63d6965'])
            assert cause == Cause.ALL_OK
            Content.assert_text(mock_file, 'kubernetes-docker-log-driver-kafka.txt', content)
