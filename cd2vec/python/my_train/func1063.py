def test_generate_file_tree(self):

        runner = CliRunner()
        result = runner.invoke(create_project, ["Gatsby"], input="y\n")
        self.assertEqual(result.exit_code, 0)

        self.assertTrue(os.path.isdir("gatsby/project/Gatsby/"))
        rmtree("gatsby")
