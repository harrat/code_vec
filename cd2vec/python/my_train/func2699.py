def test_complexity_entropy_shannon(self):
        shannon = nk.complexity_entropy_shannon(self.signal)
        self.assertAlmostEqual(shannon, 8.81, places=1)
