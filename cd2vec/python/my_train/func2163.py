def test_get_whitespace_name(interp):
    name = interp.get_whitespace_name("Group1")
    assert name == "Group1"
    name = interp.get_whitespace_name(" Group2")
    assert name == " Group2"

