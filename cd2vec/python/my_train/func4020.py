def test_02_read_w_non_zero_offset(self):
        with AIOContext(1) as ctx, open(self._TEST_FILE_NAME) as fp:
            block = ReadBlock(fp, bytearray(64))
            block.offset = 1
            self.assertEqual(1, block.offset)
