def test_nfl_game_information(self):
        fields = {
            'attendance': 62881,
            'date': 'Thursday Nov 8, 2018',
            'duration': '2:49',
            'stadium': 'Heinz Field',
            'time': '8:20pm'
        }

        mock_field = """Thursday Nov 8, 2018
Start Time: 8:20pm
Stadium: Heinz Field
Attendance: 62,881
Time of Game: 2:49
Logos via Sports Logos.net / About logos
"""

        m = MockBoxscoreData(MockField(mock_field))

        self.boxscore._parse_game_date_and_location(m)
        for field, value in fields.items():
            assert getattr(self.boxscore, field) == value
