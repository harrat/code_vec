def test_compress_file_no_compression(self):
        path = self.root.make_file()
        with open(path, 'wt') as o:
            o.write('foo')
        gzfile = Path(str(path) + '.gz')
        gzfile2 = compress_file(path, gzfile, keep=True)
        assert gzfile == gzfile2
        assert path.exists()
        assert gzfile.exists()
        with gzip.open(gzfile, 'rt') as i:
            assert i.read() == 'foo'
