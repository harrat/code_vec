@pytest.mark.slow
def test_ranking():
    fb = FitBert(model_name="distilbert-base-uncased")
    assert callable(fb.fitb)

    sentences = [
        "When she started talking about her ex-boyfriends, he looked like a ***mask*** out of water",
        "The boy was warned that if he misbehaved in the class, he would have to pay ***mask***.",
        "I am surprised that you have ***mask*** patience.",
    ]

    options = [
        ["frog", "fish"],
        ["the drummer", "the flutist", "the piper"],
        ["such a", "so", "such"],
    ]
    answers = ["fish", "the piper", "such"]
    for sentence, option, answer in zip(sentences, options, answers):
        ranked_options = fb.rank(sentence, option)
        assert ranked_options[0] == answer, "It should rank options"

    sentence = "Psychology includes the study of conscious and unconscious phenomena, as well as ***mask*** and thought."
    options = ["feelings"]
    answer = "feeling"

    ranked_options = fb.rank(sentence, options, True)
    assert ranked_options[0] == answer, "It should find and rank related options"

