def test_find_term_excludes(ontclient):
    assert not ontclient.find_term('breast')

def test_suggest_hp_term_not_excluded(ontclient):
    assert ontclient.find_term('hypogammaglobulinemia') == 'http://www.orpha.net/ORDO/Orphanet_229720'
