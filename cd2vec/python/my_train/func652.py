def test_delete_attribute_from_managed_object_unsupported_multivalue(self):
        """
        Test that an InvalidField error is raised when attempting to delete an
        unsupported multivalued attribute.
        """
        # TODO (peterhamilton) Remove this test once all multivalued attributes
        # are supported.
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._logger = mock.MagicMock()

        managed_object = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )
        args = (managed_object, ("Link", None, None))
        self.assertRaisesRegex(
            exceptions.InvalidField,
            "The 'Link' attribute is not supported.",
            e._delete_attribute_from_managed_object,
            *args
        )
