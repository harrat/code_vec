@pytest.mark.parametrize("pwl_module",
                         [SlopedPWL, MonoSlopedPWL, PointPWL, MonoPointPWL])
@pytest.mark.parametrize("num_channels", [1, 3])
@pytest.mark.parametrize("num_breakpoints", [1, 2, 3, 4])
def test_pwl_is_continous(pwl_module, num_channels, num_breakpoints):
    module = pwl_module(
        num_channels=num_channels, num_breakpoints=num_breakpoints)
    with torch.no_grad():
        for parameter in module.parameters():
            parameter.normal_()
    x = torch.linspace(
        -4., 4., steps=10000).unsqueeze(1).expand(-1, num_channels)
    y = module(x)
    dy = torch.roll(y, shifts=-1, dims=0) - y
    dx = torch.roll(x, shifts=-1, dims=0) - x
    grad = dy / dx
    if isinstance(module, (PointPWL, MonoPointPWL)):
        allowed_grad = torch.max(4 / module.get_spreads())
    else:
        allowed_grad = 4
    assert torch.max(abs(grad)) < allowed_grad

