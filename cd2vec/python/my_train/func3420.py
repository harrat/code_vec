def test_get_filepath_with_config_dir(self):
        """Test _get_filepath method with config_dir set."""
        conf = TESTConfig()
        config_dir = 'test'
        with mock.patch('yamlconf.config.os.path.exists') as mock_exists:
            # no filename, not found in basepath
            mock_exists.side_effect = [False, True]
            expected = os.path.join(
                TEST_CONFIG_ROOT, config_dir, 'config.yaml')
            result = conf._get_filepath(config_dir=config_dir)
            self.assertEqual(expected, result)
