@patch('jupyter_sigplot.sigplot.Plot.sync_command_and_arguments')
def test_overlay_href(traitlet_set_mock):
    plot = Plot()
    plot.overlay_href('bar|baz')
    assert traitlet_set_mock.call_count == 2
    for call_args in traitlet_set_mock.call_args_list:
        assert call_args[0][0]['command'] == 'overlay_href'
        assert len(call_args[0][0]['arguments']) == 1
        assert call_args[0][0]['arguments'][0] in ('bar', 'baz')

