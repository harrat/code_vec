def test_dynamodb_statement_mask():
    ConfigProvider.set(config_names.THUNDRA_TRACE_INTEGRATIONS_AWS_DYNAMODB_STATEMENT_MASK, 'true')
    try:
        # Make a request over the table
        dynamodb = boto3.resource('dynamodb', region_name='eu-west-2')
        table = dynamodb.Table('test-table')
        table.get_item(
            Key={
                'username': 'janedoe',
                'age': 22,
                'colors': ['red', 'green', 'blue'],
                'numbers': [3, 7],
                'data': b'dGhpcyB0ZXh0IGlzIGJhc2U2NC1lbmNvZGVk',
                'others': [b'foo', b'bar'],
            }
        )
