def test_scrap_dataframe(notebook_result):
    expected_df = pd.DataFrame(
        [
            ("one", 1, "json", None, "result1.ipynb"),
            ("number", 1, "json", None, "result1.ipynb"),
            ("list", [1, 2, 3], "json", None, "result1.ipynb"),
            ("dict", {"a": 1, "b": 2}, "json", None, "result1.ipynb"),
            ("output", None, "display", AnyDict(), "result1.ipynb"),
            ("one_only", None, "display", AnyDict(), "result1.ipynb"),
        ],
        columns=["name", "data", "encoder", "display", "filename"],
    )
    assert_frame_equal(
        notebook_result.scrap_dataframe,
        expected_df,
        # Python 2.7 gets confused here with AnyDict / None sometimes
        check_exact=True,
        check_column_type=False,
    )
