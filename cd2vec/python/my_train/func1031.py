def test_attributes_transmission_from_loadbalanced_node(loadbalanced):
    """Test that attributes from loadbalanced nodes are
    available directly from FlaskMultiRedis object."""

    node = loadbalanced._redis_nodes[0]
    assert loadbalanced.connection_pool is node.connection_pool

