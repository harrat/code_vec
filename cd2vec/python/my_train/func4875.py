def test_dirspec(self):
        null = DirSpec()
        assert '{dir}' == null.template
        assert 'dir' in null.path_vars

        level1 = self.root.make_directory(name='ABC123')
        level2 = self.root.make_directory(parent=level1, name='AAA')
        base = level1.parent

        spec = DirSpec(
            PathPathVar('root'),
            StrPathVar('subdir', pattern='[A-Z0-9_]+', invalid=('XYZ999',)),
            StrPathVar('leaf', pattern='[^_]+', valid=('AAA', 'BBB')),
            template=os.path.join('{root}', '{subdir}', '{leaf}'))

        # get a single dir
        pathinst = spec(root=base, subdir='ABC123', leaf='AAA')
        assert \
            path_inst(level2, dict(root=base, subdir='ABC123', leaf='AAA')) == \
            pathinst
        assert base == pathinst['root']
        assert 'ABC123' == pathinst['subdir']
        assert 'AAA' == pathinst['leaf']

        with self.assertRaises(ValueError):
            spec(root=base, subdir='abc123', leaf='AAA')

        with self.assertRaises(ValueError):
            spec(root=base, subdir='ABC123', leaf='CCC')

        with self.assertRaises(ValueError):
            spec(root=base, subdir='XYZ999', leaf='AAA')

        pathinst = spec.parse(level2)
        assert \
            path_inst(level2, dict(root=base, subdir='ABC123', leaf='AAA')) == \
            pathinst

        path = self.root.make_file(parent=level2)
        pathinst = spec.parse(path, fullpath=True)
        assert \
            path_inst(level2, dict(root=base, subdir='ABC123', leaf='AAA')) == \
            pathinst

        path2 = self.root.make_directory(name='abc123')
        with self.assertRaises(ValueError):
            spec.parse(path2)

        all_paths = spec.find(base, recursive=True)
        assert 1 == len(all_paths)
        assert \
            path_inst(level2, dict(root=base, subdir='ABC123', leaf='AAA')) == \
            all_paths[0]
