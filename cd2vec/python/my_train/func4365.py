def test_output_item_type(results):
    assert all(isinstance(item, (Movie, Show, Person)) for item in results[0])

