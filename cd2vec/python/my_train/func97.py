@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'default-solutions', 'import-netcat')
    def test_cli_search_snippet_020(snippy, capsys):
        """Search snippets with ``filter`` option.

        Search all content with a regexp filter. The filter removes all
        content from the search result returned with the `sall` option
        that do not match to the regexp in any of the resource field. In
        this case there are no regexp matches like '.*' included into the
        filter.
        """

        output = (
            '1. Remove all docker containers with volumes @docker [54e41e9b52a02b63]',
            '',
            '   $ docker rm --volumes $(docker ps --all --quiet)',
            '',
            '   # cleanup,container,docker,docker-ce,moby',
            '   > https://docs.docker.com/engine/reference/commandline/rm/',
            '',
            '2. Remove docker image with force @docker [53908d68425c61dc]',
            '',
            '   $ docker rm --force redis',
            '',
            '   # cleanup,container,docker,docker-ce,moby',
            '   > https://docs.docker.com/engine/reference/commandline/rm/',
            '   > https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--sall', '.', '--filter', 'engine', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
