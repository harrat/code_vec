def test_set_single_scopes(self):
        self.client.set_scopes('.*stackoverflow.*')

        self._make_request('https://stackoverflow.com')

        last_request = self.client.get_last_request()

        self.assertEqual('https://stackoverflow.com/', last_request['url'])
        self.assertEqual('stackoverflow.com', last_request['headers']['Host'])

        self._make_request('https://github.com')

        last_request = self.client.get_last_request()

        self.assertEqual('https://stackoverflow.com/', last_request['url'])
        self.assertEqual('stackoverflow.com', last_request['headers']['Host'])
        self.assertNotEqual('https://github.com/', last_request['url'])
        self.assertNotEqual('github.com', last_request['headers']['Host'])
