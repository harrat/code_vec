def test_get_relevant_policy_section_group_not_supported(self):
        """
        Test that the lookup for a policy with a group specified but not
        supported is handled correctly.
        """
        e = engine.KmipEngine()
        e._logger = mock.MagicMock()
        e._operation_policies = {
            'test_policy': {
                'preset': {
                    enums.ObjectType.SYMMETRIC_KEY: {
                        enums.Operation.GET: enums.Policy.ALLOW_OWNER
                    }
                },
                'groups': {
                    'test_group_B': {
                        enums.ObjectType.CERTIFICATE: {
                            enums.Operation.CREATE: enums.Policy.ALLOW_ALL
                        }
                    }
                }
            }
        }

        result = e.get_relevant_policy_section('test_policy', 'test_group_A')

        e._logger.debug.assert_called_once_with(
            "The 'test_policy' policy does not support group 'test_group_A'."
        )
        self.assertIsNone(result)
