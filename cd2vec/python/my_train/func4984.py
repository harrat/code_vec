@staticmethod
    @pytest.mark.usefixtures('import-gitlog', 'update-pytest-utc')
    def test_api_update_reference_002(server):
        """Update one reference with PUT request.

        Send PUT /references/{id} to update existing resource. The PUT
        request contains only the mandatory links attribute. All other
        attributes must be set to their default values in the HTTP response.
        """

        storage = {
            'data': [{
                'category': 'reference',
                'data': (),
                'brief': '',
                'description': '',
                'name': '',
                'groups': ('default',),
                'tags': (),
                'links': Reference.PYTEST['links'],
                'source': '',
                'versions': (),
                'languages': (),
                'filename': '',
                'created': Content.GITLOG_TIME,
                'updated': Content.PYTEST_TIME,
                'uuid': Reference.GITLOG_UUID,
                'digest': '4a868cc74e3d32a4340e1a2fd17d0df815777c67f827aeedbb35869b740dd720'
            }]
        }
        request_body = {
            'data': {
                'type': 'snippet',
                'attributes': {
                    'links': storage['data'][0]['links'],
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '667'
        }
        expect_body = {
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/references/' + Reference.GITLOG_UUID
            },
            'data': {
                'type': 'reference',
                'id': storage['data'][0]['uuid'],
                'attributes': storage['data'][0]
            }
        }
        result = testing.TestClient(server.server.api).simulate_put(
            path='/api/snippy/rest/references/5c2071094dbfaa33',
            headers={'accept': 'application/vnd.api+json; charset=UTF-8'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
