def testfanpower_bhp():
    """py.test for fanpower_bhp in idf"""
    idf = IDF(StringIO(vavfan))
    thefans = idf.idfobjects["Fan:VariableVolume".upper()]
    thefan = thefans[0]
    bhp = thefan.f_fanpower_bhp
    assert almostequal(bhp, 2.40306611606)
    # test autosize
    thefan.Maximum_Flow_Rate = "autosize"
    bhp = thefan.f_fanpower_bhp
    assert bhp == "autosize"

