def test_process_integration__sanity(self):
        """
        Given
            - A valid script package folder located at Packs/DummyPack/Scripts/DummyScript.

        When
            - parsing script files

        Then
            - integration data will be collected properly
        """
        non_unified_file_path = os.path.join(TESTS_DIR, 'test_files',
                                             'DummyPack', 'Integrations', 'DummyIntegration')

        res = process_integration(non_unified_file_path, True)
        assert len(res) == 1
        non_unified_integration_data = res[0]

        unified_file_path = os.path.join(TESTS_DIR, 'test_files',
                                         'DummyPack', 'Integrations', 'integration-DummyIntegration.yml')

        res = process_integration(unified_file_path, True)
        assert len(res) == 1
        unified_integration_data = res[0]

        test_pairs = [
            (non_unified_integration_data, TestIntegrations.INTEGRATION_DATA),
            (unified_integration_data, TestIntegrations.UNIFIED_INTEGRATION_DATA)
        ]

        for returned, constant in test_pairs:

            assert IsEqualFunctions.is_lists_equal(list(returned.keys()), list(constant.keys()))

            const_data = constant.get('Dummy Integration')
            returned_data = returned.get('Dummy Integration')

            assert IsEqualFunctions.is_dicts_equal(returned_data, const_data)
