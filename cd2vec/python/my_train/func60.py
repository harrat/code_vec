@pytest.mark.parametrize('document,document_id', ResultBaseTest.get_solver_results())
    def test_store_document(self, adapter, document, document_id):
        """Test to store document."""
        return self.store_retrieve_document_test(adapter, document, document_id)
