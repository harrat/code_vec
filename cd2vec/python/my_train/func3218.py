def test_get_root(self):
        # Need to do a different test for posix vs windows
        if os.sep == '/':
            assert '/' == get_root()
            assert '/' == get_root(PosixPath('/foo/bar/baz'))
        else:
            script_drive = os.path.splitdrive(sys.executable)[0]
            assert script_drive == get_root()
            assert 'C:\\' == get_root(WindowsPath('C:\\foo\\bar\\baz'))
