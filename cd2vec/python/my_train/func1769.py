def test_bench_as_decorator_trackmem_does_print_timing_info_to_stdout_in_non_default(self, capsys):
        """ capsys variable is a pytest function to capture system error and output
        """
        @bench.trackmem(verbose=True)
        def foo_mem(numrows):
            rows = []
            for _ in xrange(numrows):
                rows.append({'a': str(uuid.uuid4()), 'b': str(uuid.uuid4()), 'c': str(uuid.uuid4())})
            return rows
        _ = foo_mem(10**3)
        captured_stdouterr = capsys.readouterr()
        trackmemfoo_stdout_regex = re.compile(r"@trackmem: foo_mem start: (\d+\.\d+) end: (\d+\.\d+) used: (\d+\.\d+) (B|KB|MB|GB)")
        assert trackmemfoo_stdout_regex.match(captured_stdouterr.out) is not None
