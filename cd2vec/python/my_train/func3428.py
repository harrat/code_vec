def test_get_container_colors(self):
        """
        Tests the dockerprettyps.get_container_colors() method which runs all containers throuh the get_color method(),
        to try and assign a semi unique color to an instance based on it's container name.

        """
        containers = test_ps_data.ps_containers
        colorless_containers = []
        for c in containers:
            c.pop('color')
            colorless_containers.append(c)

        color_containers = dockerprettyps.get_container_colors(colorless_containers)
        for c in color_containers:
            assert 'color' in c
            assert isinstance(c['color'], str)
