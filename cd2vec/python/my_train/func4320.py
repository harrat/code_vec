def test_timestamp_option(run_reframe):
    from datetime import datetime

    timefmt = datetime.now().strftime('xxx_%F')
    returncode, stdout, _ = run_reframe(
        checkpath=['unittests/resources/checks'],
        ignore_check_conflicts=False,
        action='list',
        more_options=['-R', '--timestamp=xxx_%F']
    )
    assert returncode != 0
    assert timefmt in stdout

