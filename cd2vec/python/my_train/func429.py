def test_app_with_wrong_credentials(self):
        response = self.sync_post('/', self.body + 's')
        self.assert_status_in('Authentication failed.', json.loads(to_str(response.body))) # noqa
