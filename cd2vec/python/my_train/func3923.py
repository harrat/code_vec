def test_observable_object_custom_properties_strict(self):
        observed_data = copy.deepcopy(self.valid_object)
        observed_data['x_x_foo'] = "bar"
        self.assertFalseWithOptions(observed_data, strict_properties=True)
