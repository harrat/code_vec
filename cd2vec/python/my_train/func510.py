def test_addon1_uncommitted_change():
    """ test with a local uncommitted change without version change """
    addon1_dir = os.path.join(DATA_DIR, "addon1")
    manifest_path = os.path.join(addon1_dir, "__openerp__.py")
    with open(manifest_path) as f:
        manifest = f.read()
    try:
        with open(manifest_path, "w") as f:
            f.write(manifest.replace("summary", "great summary"))
        version = get_git_postversion(addon1_dir, STRATEGY_99_DEVN)
        assert version == "8.0.1.0.0.99.dev5"
        version = get_git_postversion(addon1_dir, STRATEGY_P1_DEVN)
        assert version == "8.0.1.0.1.dev5"
    finally:
        with open(manifest_path, "w") as f:
            f.write(manifest)
