def test_score_cache_bootstrap(self):
    cached_stats1 = compare_mt_main.generate_score_report(self.ref, [self.out1], to_cache=True)
    cached_stats2 = compare_mt_main.generate_score_report(self.ref, [self.out2], to_cache=True)
    self.assertTrue('scores' in cached_stats1 and 'strs' in cached_stats1 and 'sign_stats' in cached_stats1)
    self.assertTrue('scores' in cached_stats2 and 'strs' in cached_stats2 and 'sign_stats' in cached_stats2)
    self.assertAlmostEqual(cached_stats1['scores'], 22.44, places=1)
    reporters.sys_names = [f'sys{i+1}' for i in range(2)]
    cached_report = compare_mt_main.generate_score_report(self.ref, [self.out1, self.out2], cache_dicts=[cached_stats1, cached_stats2], bootstrap=5, title='Aggregate Scores')
    ori_report = compare_mt_main.generate_score_report(self.ref, [self.out1, self.out2], bootstrap=5, title='Aggregate Scores')
    self.assertTrue(cached_report.scores == ori_report.scores)
    self.assertTrue(cached_report.strs == ori_report.strs)
    self.assertTrue(cached_report.wins == ori_report.wins)
