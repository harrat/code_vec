def test_revoke(self):
        """
        Test that an Revoke request can be processed correctly.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        managed_object = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )
        managed_object.state = enums.State.ACTIVE
        e._data_session.add(managed_object)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        object_id = str(managed_object.unique_identifier)

        reason_unspecified = objects.RevocationReason(
            code=enums.RevocationReasonCode.UNSPECIFIED)
        reason_compromise = objects.RevocationReason(
            code=enums.RevocationReasonCode.KEY_COMPROMISE)
        date = primitives.DateTime(
            tag=enums.Tags.COMPROMISE_OCCURRENCE_DATE, value=6)

        # Test that reason UNSPECIFIED will put object into state
        # DEACTIVATED
        payload = payloads.RevokeRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(object_id),
            revocation_reason=reason_unspecified,
            compromise_occurrence_date=date)

        response_payload = e._process_revoke(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call(
            "Processing operation: Revoke"
        )
        self.assertEqual(
            str(object_id),
            response_payload.unique_identifier.value
        )

        symmetric_key = e._data_session.query(
            pie_objects.SymmetricKey
        ).filter(
            pie_objects.ManagedObject.unique_identifier == object_id
        ).one()

        self.assertEqual(enums.State.DEACTIVATED, symmetric_key.state)

        # Test that reason KEY_COMPROMISE will put object not in DESTROYED
        # state into state COMPROMISED
        payload = payloads.RevokeRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(object_id),
            revocation_reason=reason_compromise,
            compromise_occurrence_date=date)

        e._logger.reset_mock()

        response_payload = e._process_revoke(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call(
            "Processing operation: Revoke"
        )
        self.assertEqual(
            str(object_id),
            response_payload.unique_identifier.value
        )

        symmetric_key = e._data_session.query(
            pie_objects.SymmetricKey
        ).filter(
            pie_objects.ManagedObject.unique_identifier == object_id
        ).one()

        self.assertEqual(enums.State.COMPROMISED, symmetric_key.state)

        # Test that reason KEY_COMPROMISE will put object in DESTROYED
        # state into state DESTROYED_COMPROMISED
        symmetric_key.state = enums.State.DESTROYED
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()
        e._logger.reset_mock()

        payload = payloads.RevokeRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(object_id),
            revocation_reason=reason_compromise,
            compromise_occurrence_date=date)
        response_payload = e._process_revoke(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call(
            "Processing operation: Revoke"
        )
        self.assertEqual(
            str(object_id),
            response_payload.unique_identifier.value
        )
        symmetric_key = e._data_session.query(
            pie_objects.SymmetricKey
        ).filter(
            pie_objects.ManagedObject.unique_identifier == object_id
        ).one()

        self.assertEqual(enums.State.DESTROYED_COMPROMISED,
                         symmetric_key.state)

        # Test that the ID placeholder can also be used to specify revocation.
        symmetric_key.state = enums.State.ACTIVE
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()
        e._logger.reset_mock()

        e._id_placeholder = str(object_id)
        payload = payloads.RevokeRequestPayload(
            revocation_reason=reason_unspecified,
            compromise_occurrence_date=date)

        response_payload = e._process_revoke(payload)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        e._logger.info.assert_any_call(
            "Processing operation: Revoke"
        )
        self.assertEqual(
            str(object_id),
            response_payload.unique_identifier.value
        )

        symmetric_key = e._data_session.query(
            pie_objects.SymmetricKey
        ).filter(
            pie_objects.ManagedObject.unique_identifier == object_id
        ).one()

        self.assertEqual(enums.State.DEACTIVATED, symmetric_key.state)
