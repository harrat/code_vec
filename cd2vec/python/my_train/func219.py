@pytest.mark.parametrize('r, status, message', (
        (MockRequest(headers={'content-type': 'application/xml', 'accepts': 'application/json'}),
         422, 'Unprocessable Entity'),
        (MockRequest(headers={'content-type': 'application/json', 'accepts': 'application/xml'}),
         406, 'URI not available in preferred format'),
        (MockRequest(method=Method.POST), 405, 'Specified method is invalid for this resource'),
    ))
    def test_dispatch__invalid_headers(self, r, status, message):
        target = containers.ApiInterfaceBase()
        operation = Operation(mock_callback)
        actual = target.dispatch(operation, r)

        assert actual.status == status
        assert actual.body == message
