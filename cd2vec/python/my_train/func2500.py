def test_get_squad_v1(self):
        raw = get_squad(version=1)
        self.assertIn('train', raw)
        self.assertEqual(len(raw['train']), 87_599)
        self.assertIn('dev', raw)
        self.assertEqual(len(raw['dev']), 10_570)
