def test_have_name(self):
        assert 1 == self.count_checks(filters.have_name('check1'))
        assert 3 == self.count_checks(filters.have_name('check'))
        assert 2 == self.count_checks(filters.have_name(r'\S*1|\S*3'))
        assert 0 == self.count_checks(filters.have_name('Check'))
        assert 3 == self.count_checks(filters.have_name('(?i)Check'))
        assert 2 == self.count_checks(filters.have_name('(?i)check1|CHECK2'))
