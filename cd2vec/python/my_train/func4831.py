@responses.activate
    def test_resourcecfg_building_line(self, centreon_con):
        with open(resource_dir / 'test_resourcecfg_list.json') as data:
            wsresponses = json.load(data)
        responses.add(responses.POST,
                      'http://api.domain.tld/centreon/api/index.php?action=action&object=centreon_clapi',
                      json=wsresponses, status=200, content_type='application/json')

        _, res = centreon_con.resourcecfgs.get('_HOSTSNMPVERSION')
        assert res.name == "$_HOSTSNMPVERSION$"
