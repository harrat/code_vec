@staticmethod
    def test_cli_search_snippet_038(snippy, capsys):
        """Print snippet with aligned comments.

        Print snippet which has commends on every command. In this case the
        comments must be all aligned evenly after each command.
        """

        Content.store({
            'category': Const.SNIPPET,
            'data': [
                'tar cvfz mytar.tar.gz --exclude="mytar.tar.gz" ./  #  Compress folder excluding the tar.',
                'tar tvf mytar.tar.gz  #  List content of compressed tar.',
                'tar xfO mytar.tar.gz manifest.json  #  Cat file in compressed tar.',
                'tar -zxvf mytar.tar.gz --exclude "./mytar.tar.gz"  #  Extract and exclude one file.',
                'tar -xf mytar.tar.gz manifest.json  #  Extract only one file.'],
            'brief': 'Manipulate compressed tar files',
            'groups': ['linux'],
            'tags': ['howto', 'linux', 'tar', 'untar']
        })
        output = (
            '1. Manipulate compressed tar files @linux [61014e2d1ec56a9a]',
            '',
            '   $ tar cvfz mytar.tar.gz --exclude="mytar.tar.gz" ./  #  Compress folder excluding the tar.',
            '   $ tar tvf mytar.tar.gz                               #  List content of compressed tar.',
            '   $ tar xfO mytar.tar.gz manifest.json                 #  Cat file in compressed tar.',
            '   $ tar -zxvf mytar.tar.gz --exclude "./mytar.tar.gz"  #  Extract and exclude one file.',
            '   $ tar -xf mytar.tar.gz manifest.json                 #  Extract only one file.',
            '',
            '   # howto,linux,tar,untar',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--stag', 'tar', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
