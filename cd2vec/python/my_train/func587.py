def test_wdeployment_details():
    apic = APIClient(ignore_env=True)
    workspace_deployments, page_count = apic.get_wdeployments(page=1, max_per_page=1)

    # This assertion is redundant with test_deployments_list(), but it
    # is among preconditions for the test below.
    assert len(workspace_deployments) > 0

    wdeployment_id = workspace_deployments[0]['id']
    details = apic.get_wdeployment_info(wdeployment_id)
    assert 'id' in details
    assert 'type' in details

