def test_incorrect_cve_format(self):
        vulnerability = copy.deepcopy(self.valid_vulnerability)
        ext_refs = vulnerability['external_references']
        ext_refs[0]['external_id'] = "2016-1234"
        self.assertFalseWithOptions(vulnerability)
