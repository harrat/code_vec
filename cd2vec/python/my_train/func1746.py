@staticmethod
    @pytest.mark.usefixtures('import-forced', 'update-netcat-utc')
    def test_api_update_snippet_006(server):
        """Updated snippet and verify created and updated timestamps.

        Send PUT /snippets/{id} to update existing resource with specified
        digest. This test verifies that the created timestamp does not change
        and the updated timestamp changes when the content is updated.
        """

        storage = {
            'data': [
                Storage.netcat
            ]
        }
        storage['data'][0]['created'] = Content.FORCED_TIME
        storage['data'][0]['updated'] = Content.NETCAT_TIME
        storage['data'][0]['uuid'] = Snippet.FORCED_UUID
        storage['data'][0]['digest'] = Snippet.NETCAT_DIGEST
        request_body = {
            'data': {
                'type': 'snippet',
                'attributes': {
                    'data': storage['data'][0]['data'],
                    'brief': storage['data'][0]['brief'],
                    'groups': storage['data'][0]['groups'],
                    'tags': storage['data'][0]['tags'],
                    'links': storage['data'][0]['links']
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '779'
        }
        expect_body = {
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/snippets/' + Snippet.FORCED_UUID
            },
            'data': {
                'type': 'snippet',
                'id': storage['data'][0]['uuid'],
                'attributes': storage['data'][0]
            }
        }
        expect_body['data']['attributes']['updated'] = Content.NETCAT_TIME
        result = testing.TestClient(server.server.api).simulate_put(
            path='/api/snippy/rest/snippets/53908d68425c61dc',
            headers={'accept': 'application/json'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
