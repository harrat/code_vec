def test_memory_delay_add_task_non_coroutine_as_input(event_loop):
    import logging

    logger = logging.getLogger("aiotasks")

    class CustomLogger(logging.StreamHandler):
        def __init__(self):
            super(CustomLogger, self).__init__()
            self.content = []

        def emit(self, record):
            self.content.append(record.msg)

    custom = CustomLogger()
    logger.addHandler(custom)

    manager = build_manager(dsn="memory://", loop=event_loop)

    def task_test_memory_delay_add_task_non_coroutine_as_input():
        pass

    async def run():
        manager.run()

        # Add task without decorator
        manager.add_task(task_test_memory_delay_add_task_non_coroutine_as_input,
                         name="custom_task_test_memory_delay_task_decorator_oks")

    event_loop.run_until_complete(run())
    manager.stop()

    assert "Function 'task_test_memory_delay_add_task_non_coroutine_as_input' is not a coroutine and can't be added as a task" in custom.content

    # assert "custom_task_test_memory_delay_task_decorator_oks" in manager._tasks.keys()
