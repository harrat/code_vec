def test_get_can_timeout(self):
        rc = RestClient(jwt='a-token', telemetry=False, timeout=0.00001)

        with self.assertRaises(requests.exceptions.Timeout):
            rc.get('http://google.com')
