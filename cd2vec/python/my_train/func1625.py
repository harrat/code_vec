@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'json')
    def test_cli_import_reference_004(snippy):
        """Import all reference resources.

        Import all references from json file. File name and format are extracted
        from command line ``--file`` option.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        file_content = Content.get_file_content(Content.JSON, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            json.load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '--scat', 'reference', '-f', './all-references.json'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './all-references.json', mode='r', encoding='utf-8')
