def test_irrit_resume():
    """Probabilistic test: may fail very occasionally"""
    count_empty = 0
    l = list(range(10))
    for _ in range(100):
        i = irrit(range(10), resume=True)
        l1 = [n for n in i]
        if len(l1) == 0:
            count_empty += 1
        elif len(l1) < 10:
            l2 = [n for n in i]
            assert l1 + l2 == l
        else:
            assert l1 == l
    assert 0 < count_empty < 100

