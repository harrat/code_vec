def test_anyset_init2(self):
        items = [1, 2, {1}, 4, 4, {1}]
        result = AnySet(items)
        expected = ({1, 2, 4}, {6008617170096987129613061240357171817: {1}})
        assert expected == result
