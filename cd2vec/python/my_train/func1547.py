def test_call_private_method(self):
        """Test the time it takes to call private methods as compared to methods without access modifier."""
        time_without_modifier = timeit.timeit("c.public_method()", setup="""
from access_modifiers import privatemethod
class C:
    def private_method(self):
        pass
    def public_method(self):
        self.private_method()
c = C()
""", number=self.number)

        time_with_modifier = timeit.timeit("c.public_method()", setup="""
from access_modifiers import privatemethod
class C:
    @privatemethod
    def private_method(self):
        pass
    def public_method(self):
        self.private_method()
c = C()
""", number=self.number)
        self.assertLess(time_with_modifier, time_without_modifier * 15)
