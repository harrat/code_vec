def test_success() -> None:
    reportfilepath = "/tmp/reportfile_{}".format(randint(0, 9999))

    with open(reportfilepath, "w") as f:
        f.write("50 50")

    p = Popen(
        ["python", "-m", "typecov", "100", reportfilepath], stdout=PIPE, stderr=PIPE
    )
    stdout, stderr = p.communicate()

    os.remove(reportfilepath)

    assert p.returncode == 0
    assert stdout.decode() == "Total type coverage: 100.0%\n"
    assert stderr.decode() == ""

