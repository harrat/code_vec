def testVersionCmd(self, tmpdir):
        cmdLine = ['version']
        self.cwd = str(tmpdir.realpath())
        exitcode, stdout, _ = runZm(self, cmdLine)
        assert exitcode == 0
        assert 'version' in stdout
