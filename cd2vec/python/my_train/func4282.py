def test_filelike_deferred(self):
        """Deferred values work with file-like objects."""
        with open(ct_name, "rb") as fp:
            data = fp.read()
        filelike = io.BytesIO(data)
        dataset = pydicom.dcmread(filelike, defer_size=1024)
        assert 32768 == len(dataset.PixelData)
