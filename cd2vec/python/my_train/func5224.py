def test_guess_format(self):
        with self.assertRaises(ValueError):
            guess_file_format(STDOUT)
        with self.assertRaises(ValueError):
            guess_file_format(STDERR)
        path = self.root.make_file(suffix='.gz')
        with gzip.open(path, 'wt') as o:
            o.write('foo')
        assert guess_file_format(path) == 'gzip'
        path = self.root.make_file()
        with gzip.open(path, 'wt') as o:
            o.write('foo')
        assert guess_file_format(path) == 'gzip'
