def test_ufactor_infraredtransparent_construction(self):
        self.idf.initreadtxt(infrared_transparent)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        m = self.idf.getobject("MATERIAL", "TestMaterial")
        expected = 1 / (
            INSIDE_FILM_R
            + m.Thickness / m.Conductivity
            + m.Thickness / m.Conductivity
            + OUTSIDE_FILM_R
        )
        assert almostequal(c.ufactor, expected, places=2)
        assert almostequal(c.ufactor, 1 / 0.55, places=2)
