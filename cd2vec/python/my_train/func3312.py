def test_checks_fuzzy():
    """Test checks on a gettext file including fuzzy strings."""
    po_check = PoCheck()
    po_check.set_check('fuzzy', True)
    result = po_check.check_files([local_path('fr_errors.po')])

    # be sure we have one file in result
    assert len(result) == 1

    # the file has 11 errors (with the fuzzy string)
    assert len(result[0][1]) == 11

