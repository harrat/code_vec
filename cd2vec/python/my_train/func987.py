def test_playlist_writer(self):
        output_dir = os.path.join(THIS_DIR, 'output')
        os.makedirs(output_dir)
        pw = PlaylistWriter(output_dir, 'foo')
        pw.write(os.path.join(output_dir, 'foo', 'track.mp3'))
        pw.write(os.path.join(output_dir, 'track2.mp3'))
        pw.close()
        pl_path = os.path.join(output_dir, 'foo.m3u')
        self.assertTrue(os.path.exists(pl_path))

        with open(pl_path) as f:
            lines = f.readlines()
            self.assertEqual(
                ['foo/track.mp3\n', 'track2.mp3\n'],
                lines
            )
        os.remove(pl_path)
        shutil.rmtree(output_dir)
