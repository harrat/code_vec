@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_reference_008(snippy):
        """Import all reference resources.

        Try to import empty reference template. The operation will fail because
        content templates without help texts and without any modifications cannot
        be imported.
        """

        file_content = mock.mock_open(read_data=Const.NEWLINE.join(Reference.TEMPLATE))
        with mock.patch('snippy.content.migrate.io.open', file_content, create=True) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'reference', '-f', './reference-template.text'])
            assert cause == 'NOK: content was not stored because it was matching to an empty template'
            Content.assert_storage(None)
            Content.assert_arglist(mock_file, './reference-template.text', mode='r', encoding='utf-8')
