def test_prep_str_murmur3_64bit(self):
        obj = "a"
        expected_result = {
            obj: 424475663186367154
        }
        result = DeepHash(obj, ignore_string_type_changes=True, hasher=DeepHash.murmur3_64bit)
        assert expected_result == result
