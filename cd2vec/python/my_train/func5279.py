def test_train_traincontext(self):
        agent = BackendAgentTest.DebugAgent()
        count = easyagents.callbacks.log._CallbackCounts()
        tc = self.tc
        tc.num_iterations = 3
        tc.num_episodes_per_iteration = 2
        tc.max_steps_per_episode = 10
        tc.num_episodes_per_eval = 5
        tc.num_iterations_between_eval = 2
        agent.train(train_context=tc, callbacks=[count])
        assert count.train_begin_count == count.train_end_count == 1
        assert count.train_iteration_begin_count == count.train_iteration_end_count == 3
        assert tc.episodes_done_in_training == 6
        assert tc.episodes_done_in_iteration == 2
        assert tc.episodes_done_in_training in tc.loss
        assert tc.steps_done_in_training == tc.num_iterations * tc.num_episodes_per_iteration * \
               tc.max_steps_per_episode
        assert tc.steps_done_in_iteration == tc.num_episodes_per_iteration * tc.max_steps_per_episode

        assert 0 in tc.eval_rewards
        assert 0 in tc.eval_steps
        assert tc.num_iterations_between_eval * tc.num_episodes_per_iteration in tc.eval_rewards
        assert tc.num_iterations_between_eval * tc.num_episodes_per_iteration in tc.eval_steps
        assert tc.episodes_done_in_training in tc.eval_rewards
        assert tc.episodes_done_in_training in tc.eval_steps

        assert count.gym_init_begin_count == count.gym_init_end_count > 0
        assert count.gym_reset_begin_count == count.gym_reset_end_count > 0
        assert count.gym_step_begin_count == count.gym_step_end_count > 0
