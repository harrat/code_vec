def test_simple_signal_object_identifier():

    on_test_identifier(obj='xxx')

    class Foo():
        def on_test_identifier(self, a):
            a / self.b
        __init__ = register_object

    f1 = Foo()
    f2 = Foo()

    f1.b = 1
    f2.b = 0

    with pytest.raises(MissingIdentifier):
        on_test_identifier(a=5, b=0, c='c')

    on_test_identifier(a=5, b=0, obj=f1)

    with pytest.raises(ZeroDivisionError):
        on_test_identifier(a=5, b=0, obj=f2)

    unregister_object(f1)
    unregister_object(f2)

    with on_test_identifier.registered(f2.on_test_identifier):
        with pytest.raises(ZeroDivisionError):
            on_test_identifier(a=5, b=0, obj=f2)
