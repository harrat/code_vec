def test_read(config_files):
    assert read() == {
        'Vsphere Session': {
            'vcdriver_host': '',
            'vcdriver_port': '443',
            'vcdriver_username': '',
            'vcdriver_password': ''
        },
        'Virtual Machine Deployment': {
            'vcdriver_resource_pool': '',
            'vcdriver_data_store': '',
            'vcdriver_data_store_threshold': '0',
            'vcdriver_folder': ''
        },
        'Virtual Machine Remote Management': {
            'vcdriver_vm_ssh_username': '',
            'vcdriver_vm_ssh_password': '',
            'vcdriver_vm_winrm_username': '',
            'vcdriver_vm_winrm_password': ''
        }
    }
    load('config_file_3.cfg')
    assert read() == {
        'Vsphere Session': {
            'vcdriver_host': '',
            'vcdriver_port': '443',
            'vcdriver_username': '',
            'vcdriver_password': 'myway'
        },
        'Virtual Machine Deployment': {
            'vcdriver_resource_pool': '',
            'vcdriver_data_store': '',
            'vcdriver_data_store_threshold': '0',
            'vcdriver_folder': ''
        },
        'Virtual Machine Remote Management': {
            'vcdriver_vm_ssh_username': '',
            'vcdriver_vm_ssh_password': '',
            'vcdriver_vm_winrm_username': '',
            'vcdriver_vm_winrm_password': ''
        }
    }
