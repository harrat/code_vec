@log_info
    def test_07_get_local_ip(self):
        """Test get local IP."""
        ensure_autodiscover()
        ip_checker_cls = list(registry.registry.values())[0]

        ip_checker = ip_checker_cls(verbose=False)
        local_ip = ip_checker.get_local_ip()
        self.assertIsNotNone(local_ip)
