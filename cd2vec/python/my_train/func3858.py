def test_list_option_with_default(self):
        self.assertListEqual(
            self.get_opts('test').opt_list_with_default,
            []
        )
        self.assertListEqual(
            self.get_opts(
                'test', '--opt-list-with-default', '1', 'b', '3'
            ).opt_list_with_default,
            ['1', 'b', '3']
        )
