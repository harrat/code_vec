@pytest.mark.skipif(not HAS_TIME_NS,
                    reason="time.time_ns is present only on 3.7 and above")
def test_time_ns():
    freezer = freeze_time("2012-01-14")
    local_time = datetime.datetime(2012, 1, 14)
    utc_time = local_time - datetime.timedelta(seconds=time.timezone)
    expected_timestamp = time.mktime(utc_time.timetuple())

    freezer.start()
    assert time.time() == expected_timestamp
    assert time.time_ns() == expected_timestamp * 1e9
    freezer.stop()
    assert time.time() != expected_timestamp
    assert time.time_ns() != expected_timestamp * 1e9

