def test_implements_no_implementation_getter():
    """
    Case: do not implement interface member that is getter.
    Expect: class does not implement interface member error message.
    """
    class HumanNameInterface:

        @property
        def name(self):
            return

    with pytest.raises(InterfaceMemberHasNotBeenImplementedException) as error:

        @implements(HumanNameInterface)
        class HumanWithoutImplementation:
            pass

    assert 'class HumanWithoutImplementation does not implement ' \
           'interface member HumanNameInterface.name(self)' == error.value.message
