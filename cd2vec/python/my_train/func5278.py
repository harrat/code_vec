def test_set_querystring_overrides(self):
        self.client.set_querystring_overrides('foo=baz')

        self._make_request('https://httpbin.org/?foo=bar&spam=eggs')

        last_request = self.client.get_last_request()

        query = urlsplit(last_request['url'])[3]
        self.assertEqual('foo=baz', query)
