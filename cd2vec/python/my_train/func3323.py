def test_sku_create(self):

        res = self.client.sku.create(
            shop_id=1096,
            upc='123456755444',
            name='??300g',
            status=0,
            left_num=100,
            sale_price=1220,
            photos=[
                {
                    'is_master': 1,
                    'url': 'https://open-be.ele.me/1.jpg'
                }
            ]
        )
        self.assertEqual(res['errno'], 0)
