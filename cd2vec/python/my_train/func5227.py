@mock.patch('time.sleep')
def test_delay_variaton(mocked_time):
    try:
        tracer = ThundraTracer.get_instance()
        with tracer.start_active_span(operation_name='foo', finish_on_close=True) as scope:
            span = scope.span
            delay = 100
            variation = 50
            lsl = LatencyInjectorSpanListener(delay=delay, variation=variation)
