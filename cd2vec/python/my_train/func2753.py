@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'import-remove', 'import-gitlog', 'export-time')
    def test_cli_export_solution_034(snippy):
        """Export all references.

        Export content only from reference category when the operation
        category is set to solution. In this case the search category must
        override the content category and only references are exported.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Reference.GITLOG
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '--scat', 'reference'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './references.mkdn', content)
