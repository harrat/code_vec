@pytest.mark.vcr()
def test_access_groups_list_filter_name_typeerror(api):
    with pytest.raises(TypeError):
        api.access_groups.list((1, 'match', 'win'))
