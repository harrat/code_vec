def test_copy_music_dryrun(playlist_current, music_files, dump_music_path):
    playlist_path = str(playlist_current)
    files = list(M3uDump.parse_playlist(playlist_path))

    search_path_files = M3uDump.get_search_path_files(str(music_files))

    playlist = M3uDump.fix_playlist(search_path_files, files)

    M3uDump.copy_music(playlist, dump_music_path, True)

    assert os.path.exists(
        os.path.join(dump_music_path, 'dummy001.mp3')) is False
    assert os.path.exists(
        os.path.join(dump_music_path, 'dummy002.mp3')) is False
    assert os.path.exists(os.path.join(dump_music_path, '??? ??.mp3')) is False
    assert os.path.exists(os.path.join(dump_music_path, '??????.mp3')) is False

