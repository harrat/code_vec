def test_write_read_bytes_python(self):
        for fmt in ('.gz', '.bz2', '.xz'):
            with self.subTest(fmt=fmt):
                self.write_read_file(fmt, False, 'b')
