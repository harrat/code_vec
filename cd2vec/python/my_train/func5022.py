def test_name_runonly_test():
    class MyTest(rfm.RunOnlyRegressionTest):
        def __init__(self, a, b):
            self.a = a
            self.b = b

    test = MyTest(1, 2)
    assert os.path.abspath(os.path.dirname(__file__)) == test.prefix
    assert 'test_name_runonly_test.<locals>.MyTest_1_2' == test.name

