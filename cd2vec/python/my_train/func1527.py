def test_refname2key():
    """py.test for refname2key"""
    tdata = (
        (
            "TransformerNames",
            ["ElectricLoadCenter:Distribution".upper()],
        ),  # refname, key
        (
            "AllCurves",
            [
                "PUMP:VARIABLESPEED",
                "PUMP:CONSTANTSPEED",
                "BOILER:HOTWATER",
                "ENERGYMANAGEMENTSYSTEM:CURVEORTABLEINDEXVARIABLE",
            ],
        ),  # refname, key
    )
    for refname, key in tdata:
        fhandle = StringIO("")
        idf = IDF(fhandle)
        result = modeleditor.refname2key(idf, refname)
        assert result == key

