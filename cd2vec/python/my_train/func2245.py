@pytest.mark.vcr()
def test_scan_export_chapters_typeerror(api):
    with pytest.raises(TypeError):
        api.scans.export(1, chapters=1)
