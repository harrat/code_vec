def test_dynamo_db_connect_prod():
    setattr(dynamo_db, 'engine', None)
    os.environ['AWS_REGION'] = 'us-west-99'
    os.environ.pop('CI')

    class EngineSpy(object):

        def connect(self, *args, **kwargs):
            raise Exception('Should not call this method.')

        def connect_to_region(self, *args, **kwargs):
            assert 'session' not in kwargs, 'Session is not provided to engine.'

    setattr(dynamo_db, 'Engine', EngineSpy)
    dynamo_db.connect()

