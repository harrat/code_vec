def test_check_config_dir():
    """Test directory related operations"""
    # Check no folder exists already
    assert AUTH.check_config_dir() is False

    # Create directory
    assert AUTH.create_config_directory() is True

    # Verify directory exists
    assert AUTH.check_config_dir() is True

    # Create read only directory
    Auth(config_dir="config/readonly").create_config_directory()
    os.chmod("config/readonly", 444)

    # Test writing to read only
    assert Auth(
        config_dir="config/readonly/test").create_config_directory() is False
