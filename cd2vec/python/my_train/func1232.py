def test_quadratic():
    # Check that a quadratic can be solved
    # Note that the optimiser uses random numbers so
    # there is a small chance of this test failing even if correct
    start_values = [0.1, 0.1] # Starting guess

    # The optimiser can control two coefficients,
    # and should use calculate_score to evaluate solutions
    result = optimiser.optimise(start_values,
                                [ControlIndex(0), ControlIndex(1)],
                                calculate_score, maxgen=300)

    # Answer should be close to (1,2)
    assert abs(result[0] - 1.0) < 1e-2 and abs(result[1] - 2.0) < 1e-2

def test_reducing():
    # Test that the best score never goes up
    start_values = [0.1, 0.1] # Starting guess
