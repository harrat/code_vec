def test_colors_style_only_return_starting_ansi_code():
    primary = colors.style('primary')

    assert primary == '\x1b[38;2;137;89;168m'

