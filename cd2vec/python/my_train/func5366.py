@pytest.mark.vcr()
def test_workbench_asset_vuln_info_filter_type_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.asset_vuln_info(str(uuid.uuid4()), 19506, filter_type=123)
