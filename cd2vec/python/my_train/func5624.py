@pytest.mark.parametrize('py_obj, json_obj', many_list)
def test_loads_many(py_obj, json_obj):
    assert jsonextra.loads(json_obj) == py_obj

