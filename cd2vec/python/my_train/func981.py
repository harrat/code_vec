def test_get_relevant_policy_section_group(self):
        """
        Test that the lookup for a policy with a group specified is handled
        correctly.
        """
        e = engine.KmipEngine()
        e._operation_policies = {
            'test_policy': {
                'preset': {
                    enums.ObjectType.SYMMETRIC_KEY: {
                        enums.Operation.GET: enums.Policy.ALLOW_OWNER
                    }
                },
                'groups': {
                    'test_group': {
                        enums.ObjectType.CERTIFICATE: {
                            enums.Operation.CREATE: enums.Policy.ALLOW_ALL
                        }
                    }
                }
            }
        }

        expected = {
            enums.ObjectType.CERTIFICATE: {
                enums.Operation.CREATE: enums.Policy.ALLOW_ALL
            }
        }

        result = e.get_relevant_policy_section('test_policy', 'test_group')
        self.assertEqual(expected, result)
