@pytest.mark.parametrize('site,artist,title', [
    (azlyrics, 'slayer', 'live undead'),
    (genius, 'rammstein', 'rosenrot'),
    (letras, 'havok', 'afterburner'),
    (lyricscom, 'dark tranquillity', 'atom heart 243.5'),
    (lyricsmode, 'motorhead', 'like a nightmare'),
    (lyricswikia, 'in flames', 'everything counts'),
    (metalarchives, 'black sabbath', 'master of insanity'),
    (metrolyrics, 'flotsam and jetsam', 'fade to black'),
    (musixmatch, 'pantera', 'psycho holiday'),
    (songlyrics, 'sylosis', 'stained humanity'),
    (vagalume, 'epica', 'unchain utopia'),
])
def test_scrape(site, artist, title):
    """
    Test all the scraping methods, each of which should return a set of lyrics
    for a known-to-be-found song.
    """
    if not check_site_available(site):
        pytest.skip('This site is not available')
    song = Song(artist=artist, title=title)
    try:
        lyrics = site(song)
    except RemoteDisconnected:
        pytest.skip('Remote disconnected')
    assert lyrics != ''

