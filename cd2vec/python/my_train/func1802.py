@project_setup_py_test("hello-no-language", ["--skip-cmake", "build"], disable_languages_test=True)
def test_skip_cmake_arg(capfd):
    out, _ = capfd.readouterr()
    assert "Generating done" not in out

