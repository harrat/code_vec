def test_set_existing_attribute_gets_overridden(config, mock_env, sentinel):
    mock_env({'ATTRIBUTE_INT': '1'})
    config.ATTRIBUTE_INT = sentinel
    assert config.ATTRIBUTE_INT == 1

