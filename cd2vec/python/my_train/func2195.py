def test_hybrid_udf_zero_udf_file_entry(tmpdir):
    indir = tmpdir.mkdir('udfzerofileentry')
    outfile = str(indir)+'.iso'

    with open(os.path.join(str(indir), 'foo'), 'wb') as outfp:
        outfp.write(b'foo\n')

    subprocess.call(['genisoimage', '-v', '-v', '-no-pad', '-iso-level', '3',
                     '-udf', '-o', str(outfile), str(indir)])

    # Now open up the ISO and zero out the UDF File Entry
    with open(str(outfile), 'r+b') as fp:
        fp.seek(261*2048)
        fp.write(b'\x00'*2048)

    iso = pycdlib.PyCdlib()

    iso.open(str(outfile))

    do_a_test(iso, check_udf_zeroed_file_entry)

    iso.close()

def test_hybrid_udf_rm_zero_udf_file_entry(tmpdir):
    indir = tmpdir.mkdir('udfzerofileentry')
    outfile = str(indir)+'.iso'
