@pytest.mark.vcr()
def test_agents_list_wildcard_fields_typeerror(api):
    with pytest.raises(TypeError):
        api.agents.list(wildcard_fields='nope')
