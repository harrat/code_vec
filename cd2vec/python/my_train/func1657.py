def test_tdigest_as_bytes():
    digest = from_bytes(java_bytes)
    b = as_bytes(digest)

    digest = from_bytes(b)
    _test_digest(digest)

    assert len(b) == len(java_bytes)
    assert b == java_bytes

