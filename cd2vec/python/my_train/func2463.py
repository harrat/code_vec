def test_invalidate_region(region_cache, region):
    region['key'] = 'value'
    region_cache.region('root').invalidate()
    assert 'key' not in region
    assert region._region_cache.conn.hget(region.name, 'key') is None

    sb = region.region('sub')
    sb['key2'] = 'value'
    region.invalidate()

    assert region._region_cache.conn.hget(sb.name, 'key2') is None
    assert 'key2' not in sb

