def test_init(self, seqprop_with_i, sequence_id):
        """Test initializing with just an ID"""
        assert seqprop_with_i.id == sequence_id

        # If just an ID initialized, everything should be empty
        assert seqprop_with_i.seq == None
        assert seqprop_with_i.name == '<unknown name>'
        assert seqprop_with_i.description == '<unknown description>'
        assert len(seqprop_with_i.annotations) == 0
        assert len(seqprop_with_i.letter_annotations) == 0
        assert len(seqprop_with_i.features) == 0

        # Files should not exist and raise errors if accessed
        assert seqprop_with_i.sequence_file == None
        with pytest.raises(IOError):
            seqprop_with_i.sequence_dir
        with pytest.raises(IOError):
            seqprop_with_i.sequence_path
        assert seqprop_with_i.metadata_file == None
        with pytest.raises(IOError):
            seqprop_with_i.metadata_dir
        with pytest.raises(IOError):
            seqprop_with_i.metadata_path
        assert seqprop_with_i.feature_file == None
        with pytest.raises(IOError):
            seqprop_with_i.feature_dir
        with pytest.raises(IOError):
            seqprop_with_i.feature_path
