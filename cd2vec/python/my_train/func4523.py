def test_methods_transmission_from_aggregator(aggregated):
    """Test that methods from aggregator are available
    directly from FlaskMultiRedis object (aggregate)."""

    assert isinstance(aggregated.keys.__self__, Aggregator)

