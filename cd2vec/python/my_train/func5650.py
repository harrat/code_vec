@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'import-kafka')
    def test_api_create_solution_005(server):
        """Update solution with POST that maps to DELETE.

        Send POST /solutions with the ``X-HTTP-Method-Override`` header to delete
        solution. In this case the resource exists and the content is deleted.
        """

        storage = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        expect_headers = {}
        result = testing.TestClient(server.server.api).simulate_post(
            path='/api/snippy/rest/solutions/ee3f2ab7c63d696',
            headers={'accept': 'application/json', 'X-HTTP-Method-Override': 'DELETE'})
        assert result.status == falcon.HTTP_204
        assert result.headers == expect_headers
        assert not result.text
        Content.assert_storage(storage)
