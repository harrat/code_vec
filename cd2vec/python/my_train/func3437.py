def test_create_payment_error(self):
        with self.assertRaises(swish.SwishError):
            self.client.create_payment(
                payee_payment_reference='0123456789',
                callback_url='https://example.com/api/swishcb/paymentrequests',
                amount=100,
                currency='SEK',
                message='BE18'
            )
