def test_publish_repos_published_before_a_date(command_tester):
    """publishes repos that were published before the given date"""
    fake_publish = FakePublish()
    fake_pulp = fake_publish.pulp_client_controller
    _add_repo(fake_pulp)

    command_tester.test(
        fake_publish.main,
        [
            "test-publish",
            "--pulp-url",
            "https://pulp.example.com",
            "--published-before",
            "2019-09-08",
        ],
    )

    # repo published before 2019-08-09 is published
    assert [hist.repository.id for hist in fake_pulp.publish_history] == ["repo3"]

