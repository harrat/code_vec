@pytest.mark.parametrize('port,wrong_param', [(80, False), (8080, True)])
def test_get_proxy_with_args(get_proxy, regex_proxy, port, wrong_param):
    prox_dict = get_proxy(country='US', port=port, wrong_param=wrong_param)
    assert 'http', 'https' in prox_dict.keys()

    assert regex_proxy(prox_dict, port)

