@pytest.mark.vcr()
def test_workbench_asset_vulns_filtered(api):
    assets = api.workbenches.assets()
    vulns = api.workbenches.asset_vulns(assets[0]['id'],
        ('severity', 'eq', 'Info'))
    assert isinstance(vulns, list)
    v = vulns[0]
    check(v, 'accepted_count', int)
    check(v, 'count', int)
    check(v, 'counts_by_severity', list)
    for i in v['counts_by_severity']:
        check(i, 'count', int)
        check(i, 'value', int)
    check(v, 'plugin_family', str)
    check(v, 'plugin_id', int)
    check(v, 'plugin_name', str)
    check(v, 'severity', int)
    check(v, 'vulnerability_state', str)

@pytest.mark.vcr()
def test_workbench_asset_vuln_info_uuid_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.asset_vuln_info(1, 1)
