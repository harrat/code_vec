def test_equity():
    ev = Evaluator()

    h1, h2, draw = ev.equity(3000000, [(14, 4), (14, 2)], [(8, 3), (9, 3)])
    assert 77.12 <= h1 <= 77.32
    assert 22.37 <= h2 <= 22.57

    # h1, h2, draw = ev.equity(3000000, [(10, 3), (11, 3)], [(8, 3), (9, 3)])
    # assert 66.53 <= h1 <= 66.73
    # assert 32.01 <= h2 <= 32.21

    h1, h2, draw = ev.equity(3000000, [(2, 2), (3, 1)], [(4, 3), (5, 4)])
    assert 29.7 <= h1 <= 29.9
    assert 49.77 <= h2 <= 49.97
