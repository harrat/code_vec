def test_start(self):
        start(self.path, self.args, self.all_repo_addons, self.config)
        records = [Record.__str__(r) for r in ReportManager.getEnabledReporters()[0].reports]

        # Comparing the whitelist with the list of output we get from addon-checker tool
        for white_str in self.whitelist:
            for value in records:
                if white_str.lower() == value.lower():
                    break
            else:
                flag = False
        else:
            flag = True

        self.assertTrue(flag)
