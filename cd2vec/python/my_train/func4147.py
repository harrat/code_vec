def test_incorrect_cve_source_name(self):
        vulnerability = copy.deepcopy(self.valid_vulnerability)
        ext_refs = vulnerability['external_references']
        ext_refs[0]['source_name'] = "CVE"
        self.assertFalseWithOptions(vulnerability)
