def test_get_attribute_list_not_allowed_by_policy(self):
        """
        Test that an unallowed request is handled correctly by
        GetAttributeList.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=False)
        e._logger = mock.MagicMock()
        e._client_identity = 'test'

        obj_a = pie_objects.OpaqueObject(b'', enums.OpaqueDataType.NONE)
        obj_a._owner = 'admin'

        e._data_session.add(obj_a)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        id_a = str(obj_a.unique_identifier)
        payload = payloads.GetAttributeListRequestPayload(
            unique_identifier=id_a
        )

        # Test by specifying the ID of the object whose attributes should
        # be retrieved.
        args = [payload]
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            "Could not locate object: {0}".format(id_a),
            e._process_get_attribute_list,
            *args
        )
