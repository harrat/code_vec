@pytest.mark.vcr()
def test_workbench_vuln_info_plugin_id_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vuln_info('something')
