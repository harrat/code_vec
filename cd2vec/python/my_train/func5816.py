def test_set_permissions(self):
        path = self.root.make_file()
        with self.assertRaises(ValueError):
            set_permissions(path, 'z')
        set_permissions(path, 'r')
        with self.assertRaises(IOError):
            check_access(path, 'w')
