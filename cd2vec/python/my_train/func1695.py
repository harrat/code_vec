def test_duplicate_obj_id_strings(self):
        """Test an error is raised for non unique string Ids"""

        dummy_release = csp.DR3()
        dummy_release.release = '234'
        combined_data = CombinedDataset(csp.DR3(), dummy_release)
        with self.assertRaises(RuntimeError):
            combined_data.get_data_for_id('2010ae')
