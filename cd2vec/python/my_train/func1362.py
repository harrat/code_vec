def test_rotate_then_translate(self):
        transform = CompositeTransform()
        transform.rotate(np.array([1., 2., 3.]))
        transform.translate(np.array([3., 2., 1.]))

        v = np.array([1., 0., 0.]).reshape(-1, 3)

        # Forward.
        np.testing.assert_allclose(
            np.array([2.30507944, 1.80799303, 1.69297817]).reshape(-1, 3),
            transform(v))
        # Reverse.
        np.testing.assert_allclose(
            np.array([1.08087689, -1.45082159, -2.3930779]).reshape(-1, 3),
            transform(v, reverse=True))
