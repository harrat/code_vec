def test_add_and_get_trusted_beneficiary(self):
        self.member1.add_trusted_beneficiary(self.member2.member_id)
        beneficiaries = self.member1.get_trusted_beneficiaries()
        beneficiary_id = [beneficiary.payload.member_id for beneficiary in beneficiaries]
        assert [self.member2.member_id] == beneficiary_id

        self.member1.add_trusted_beneficiary(self.member3.member_id)
        beneficiaries = self.member1.get_trusted_beneficiaries()
        beneficiary_id = [beneficiary.payload.member_id for beneficiary in beneficiaries]
        assert [self.member2.member_id, self.member3.member_id] == beneficiary_id
