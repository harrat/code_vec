@pytest.mark.vcr()
def test_workbench_asset_vulns_invalid_filter(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.asset_vulns(str(uuid.uuid4()),
            ('operating_system', 'contains', 'Linux'))
