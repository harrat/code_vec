def test_open_process(self):
        with self.assertRaises(ValueError):
            xopen('|cat', 'wt', allow_subprocesses=False)
        with open_('|cat', 'wt') as p:
            p.write('foo\n')
        assert b'foo\n' == p.stdout
