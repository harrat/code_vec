def test_masked(test_key, test_setting, is_sensitive, actual_value):
    """ Ensure accessing Settings.masked() elements via __getitem__ returns the underlying value when the test_key points
    to a Sensitive object"""

    masked_settings = test_setting.masked()

    if is_sensitive:
        assert (
            masked_settings.get(test_key) != actual_value
        ), "Shouldn't get a sensitive value back from masked settings"
        assert re.match(
            r"\*+", masked_settings.get(test_key)
        ), "Should get a bunch of stars back from a sensitive value in masked settings"
    else:
        assert masked_settings.get(test_key) == actual_value
