def test_set_rudder(self):
        assert self.boat.rudder_angle == 20
        content = json.dumps({'value': 32})
        request = self._post_string(content, endpoint='/rudder')
        status = json.loads(request.read().decode("utf-8"))
        assert self.boat.rudder_angle == 32
