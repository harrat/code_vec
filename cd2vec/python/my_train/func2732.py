@pytest.mark.vcr()
def test_access_groups_list_filter_type_typeerror(api):
    with pytest.raises(TypeError):
        api.access_groups.list(filter_type=1)
