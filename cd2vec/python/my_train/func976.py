@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'yaml', 'import-gitlog', 'update-pytest-utc')
    def test_cli_import_reference_010(snippy):
        """Import reference based on message digest.

        Import defined reference based on message digest. File name is defined
        from command line as YAML file which contain one reference. Links in
        the reference was updated. Content updated field must be updated with a
        new timestamp.
        """

        content = {
            'data': [
                Content.deepcopy(Reference.GITLOG)
            ]
        }
        content['data'][0]['links'] = ('https://updated-link.html',)
        content['data'][0]['updated'] = Content.PYTEST_TIME
        content['data'][0]['digest'] = 'fafd46eca7ca239bcbff8f1ba3e8cf806cadfbc9e267cdf6ccd3e23e356f9f8d'
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '--scat', 'reference', '-d', '5c2071094dbfaa33', '-f', 'one-reference.yaml'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-reference.yaml', mode='r', encoding='utf-8')
