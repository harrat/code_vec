@pytest.mark.vcr()
def test_workbench_asset_info(api):
    assets = api.workbenches.assets()
    asset = api.workbenches.asset_info(assets[0]['id'])

@pytest.mark.vcr()
def test_workbench_asset_vulns_uuid_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.asset_vulns(1)
