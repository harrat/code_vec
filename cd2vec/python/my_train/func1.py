def test_increment_minor(self):
        os.environ["RELEASE_TYPE"] = "minor"
        v1 = VersionUtils.increment(self.v1)
        v2 = VersionUtils.increment(self.v2)
        v3 = VersionUtils.increment(self.v3)
        v4 = VersionUtils.increment(self.v4)
        v5 = VersionUtils.increment(self.v5)
        v6 = VersionUtils.increment(self.v6)
        self.assertEqual(v1, "1!1.3.0")
        self.assertEqual(v2, "1.3.0")
        self.assertEqual(v3, "1.3.0")
        self.assertEqual(v4, "1.3")
        self.assertEqual(v5, "2014.1")
        self.assertEqual(v6, "2.2.0")
