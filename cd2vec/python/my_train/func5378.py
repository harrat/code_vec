def test_logger_levels(self):
        self.logger_with_check.setLevel('verbose')
        self.logger_with_check.setLevel(rlog.VERBOSE)

        self.logger_with_check.debug('bar')
        self.logger_with_check.verbose('foo')

        assert not self.found_in_logfile('bar')
        assert self.found_in_logfile('foo')
