def test_auth_flow(self):
        member1 = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        member2 = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        payload = AccessTokenBuilder.create_with_alias(member2.get_first_alias()).for_all().build()

        access_token = member1.create_access_token(payload)
        token = member1.get_token(access_token.id)
        request_id = generate_nonce()
        original_state = generate_nonce()
        csrf_token = generate_nonce()

        token_request_url = self.client.generate_token_request_url(request_id, original_state, csrf_token)
        state = urllib.parse.urlparse(token_request_url).query.split('=')[1]
        signature = member1.sign_token_request_state(request_id, token.id, state)
        path = 'path?tokenId={}&state={}&signature={}'.format(token.id, state,
                                                              urllib.parse.quote(
                                                                  proto_message_to_bytes(signature).decode()))
        token_request_callback_url = 'http://localhost:80/' + path
        callback = self.client.parse_token_request_callback_url(token_request_callback_url, csrf_token)
        assert original_state == callback.state
