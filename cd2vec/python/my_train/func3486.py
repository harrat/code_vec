def test_get_app(self, mock_sp, client: AppsModule, shared):
        mock_sp.return_value = AppMock('get_app')
        app = shared['app']

        response = client.get_app(app.id)
        assert response.id == app.id
