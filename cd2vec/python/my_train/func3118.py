def test_implements_no_implementation_instance_method():
    """
    Case: do not implement interface member that is method.
    Expect: class does not implement interface member error message.
    """
    class HumanBasicsInterface:

        def love(self, who, *args, **kwargs):
            pass

    with pytest.raises(InterfaceMemberHasNotBeenImplementedException) as error:

        @implements(HumanBasicsInterface)
        class HumanWithoutImplementation:
            pass

    assert 'class HumanWithoutImplementation does not implement ' \
           'interface member HumanBasicsInterface.love(self, who, args, kwargs)' == error.value.message
