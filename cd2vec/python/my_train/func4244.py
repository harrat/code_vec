def test_validate_types_removal(
    extracted_data_dict_1,
    expected_extracted_data_dict_with_removal,
    drm_model_tax_coupon_with_inline_groups,
):
    """
    Unit: tests validate_types when there are type conflicts and
          some fields are removed.
    """
    # creates a deep copy of the fixture so mutations wont affect next tests
    extracted_data_dict = copy.deepcopy(extracted_data_dict_1)

    extracted_data_dict["fields"]["some_int"] = "abc123"  # messes int casting

    # function invocation
    validate_types(extracted_data_dict, drm_model_tax_coupon_with_inline_groups)

    assert extracted_data_dict == expected_extracted_data_dict_with_removal

