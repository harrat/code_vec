def test_metrics_v1_api(client, mocker,
                        mock_puppetdb_environments,
                        mock_puppetdb_default_nodes):
    query_data = {
        'version': [{'version': '4.2.0'}],
        'mbean': [
            {
                'validate': {
                    'data': {
                        'java.lang:type=Memory': {},
                        'puppetlabs.puppetdb.population:name=num-nodes': {},
                    },
                    'checks': {
                    }
                }
            }
        ]
    }
    dbquery = MockDbQuery(query_data)
    mocker.patch.object(app.puppetdb, '_query', side_effect=dbquery.get)
    rv = client.get('/metrics')

    soup = BeautifulSoup(rv.data, 'html.parser')
    assert soup.title.contents[0] == 'Puppetboard'
    ul_list = soup.find_all('ul', attrs={'class': 'ui list searchable'})
    assert len(ul_list) == 1
    vals = ul_list[0].find_all('a')

    assert len(vals) == 2
    assert vals[0].string == 'java.lang:type=Memory'
    assert vals[1].string == 'puppetlabs.puppetdb.population:name=num-nodes'

    assert rv.status_code == 200

