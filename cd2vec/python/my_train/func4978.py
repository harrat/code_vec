def it_validates_ssl(self, response):
        h = RequestHandler('')
        with patch('ubersmith.api.RequestHandler.session') as session:
            session.post.return_value = response
            h.process_request('uber.method_list')
            assert session.post.call_args[1]['verify'] is True
