def test_autofill_with_specified_file(cli, config, entries_file):
    """
    Edit with specified date should not autofill it.
    """
    config.set('taxi', 'auto_fill_days', '0,1,2,3,4,5,6')
    cli('edit', args=['--file=%s' % str(entries_file)])

    assert entries_file.read() == ''

