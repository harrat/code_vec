def test_pysal_polygon(self):
        # Isla Vista CDP, Polygon
        geodata = self.conn.mapservice.query(layer=36, where="PLACE=36868")
        numpy.testing.assert_allclose(
            geodata.total_bounds,
            numpy.array([-13345123.4905, 4083187.9895, -13340209.9595, 4085919.385]),
        )
