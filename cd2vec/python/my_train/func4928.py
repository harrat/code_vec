def test_post_html_response(self):
        """Test POSTing a resource via form parameters and requesting the
        response be HTML formatted."""
        response = self.app.post('/artists',
                headers={'Accept': 'text/html'},
                data={u'Name': u'Jeff Knupp'})
        assert response.status_code == 201
        assert 'Jeff Knupp' in str(response.get_data(as_text=True))
