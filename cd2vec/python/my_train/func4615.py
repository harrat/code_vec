@pytest.mark.remote
def test_download():
    data = catalog.download("GWTC-1-confident")
    assert "GW150914" in data["data"]

    # check that the cache works properly
    data2 = catalog.download("GWTC-1-confident")
    assert data2 is data

