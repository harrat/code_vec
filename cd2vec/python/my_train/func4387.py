def test_stream_stats(self):
        """ For this test we need back-to-back ports. """
        ports = self.xm.session.reserve_ports([self.port1, self.port2])
        ports[self.port1].load_config(path.join(path.dirname(__file__), 'configs', 'test_config_1.xpc'))
        ports[self.port2].load_config(path.join(path.dirname(__file__), 'configs', 'test_config_2.xpc'))

        self.xm.session.start_traffic(blocking=True)

        tpld_stats = XenaTpldsStats(self.xm.session)
        print(tpld_stats.read_stats().dumps())

        streams_stats = XenaStreamsStats(self.xm.session)
        streams_stats.read_stats()
        print(streams_stats.tx_statistics.dumps())
        print(streams_stats.statistics.dumps())
        # Access TX counter using stream name or stream object, from tx_statistics or statistics.
        assert(streams_stats.tx_statistics['Stream 1-1']['packets'] == 8000)
        assert(streams_stats.tx_statistics[ports[self.port1].streams[0]]['packets'] == 8000)
        assert(streams_stats.statistics[ports[self.port1].streams[0]]['tx']['packets'] == 8000)
        assert(streams_stats.statistics['Stream 1-1']['tx']['packets'] == 8000)
        assert(streams_stats.statistics['/'.join(self.port1.split('/')[1:]) + '/0']['tx']['packets'] == 8000)
        # Access RX counter with port name on RX side or directly.
        assert(streams_stats.statistics['Stream 1-1']['rx']['pr_tpldtraffic']['pac'] == 8000)
        assert(streams_stats.statistics['Stream 1-1']['rx'][ports[self.port2]]['pr_tpldtraffic']['pac'] == 8000)
        assert(streams_stats.statistics['Stream 1-1']['rx'][self.port2]['pr_tpldtraffic']['pac'] == 8000)
