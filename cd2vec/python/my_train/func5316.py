@pytest.mark.parametrize("modname",
                [modname for modname in NAMES_EXISTING if '.' in modname])
def test_load_lazysub_on_fullmodule(modname, lazy_opts):
    level, modclass, errors = lazy_opts
    base = modname.split('.')[0]
    # A fully loaded base...
    importlib.import_module(base)
    # and a lazy sub...
    mod = lazy_import.lazy_module(modname, error_strings=errors,
                                   lazy_mod_class=modclass, level=level)
    if level == 'base':
        assert not isinstance(mod, modclass)
    else:
        assert isinstance(mod, modclass)
