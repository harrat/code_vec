def test_domain(self):
        """
        This method will test domain2idna.domain()
        """

        BaseStdout.setUp(self)

        expected = self.domain_to_test[-1]

        subjects(self.domain_to_test[0], None)
        actual = sys.stdout.getvalue()

        self.assertEqual(expected, actual)
