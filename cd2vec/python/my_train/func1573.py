def test_run(planner):
    action = planner.compiler.default_action()

    plan_ = planner.run()
    assert plan_ is not None

    assert len(plan_) == len(action)
    for plan_action, action_tensor in zip(plan_, action):
        assert isinstance(plan_action, np.ndarray)
        assert plan_action.shape == (HORIZON, *action_tensor.shape[1:])

