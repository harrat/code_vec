def test_run_defaults(cellpainting, runner):
    opts = ["ingest"]

    if cellpainting["munge"]:
        opts += ["--munge"]
    else:
        opts += ["--no-munge"]

    opts += [cellpainting["data_dir"]]

    with backports.tempfile.TemporaryDirectory() as temp_dir:
        sqlite_file = os.path.join(temp_dir, "test.db")
