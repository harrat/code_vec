def test_user_defined_endpoint(self):
        """Make sure user-defined endpoint exists."""
        response = self.get_response('/styles', 200)
        assert len(json.loads(response.get_data(as_text=True))[u'resources']) == 25
