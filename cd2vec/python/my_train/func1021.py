def test_bundle_linear():
    nn = NN(3)
    nn.add_layer(Layer(1, Linear, glorot_uniform(), uniform(0, 0)))
    x = np.array(
        [[0.14131787,  0.31549032,  0.33582581,  0.16351758,  0.23220519],
         [0.34221643,  0.36613729,  0.9500988,  0.74681656,  0.08620996],
         [0.69490933,  0.05386328,  0.08184282,  0.83205677,  0.6950512]])
    y = 2.*x[0] + 3.*x[1] - 0.5*x[2]
    pbm = ProximalBundleMethod(MSE)
    pbm.train(nn, x.T, y.T, mu=0.001, m_L=0.3, m_R=0.7, t_bar=0.5, gamma=0,
              accuracy_tolerance=1e-10, max_iterations=500)
    np.testing.assert_almost_equal(nn.predict([0, 1, 2]), 2, decimal=3)

