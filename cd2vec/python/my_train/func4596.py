def test_execute_env_function_env(self, venv):
        """Check propagation of environment variables to the underlying virtual environment."""
        assert execute_env_function(venv.python, _func_env, is_json=False, env={"FOO": "BAR"}) == "BAR"
