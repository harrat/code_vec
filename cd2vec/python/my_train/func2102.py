def test_ftol_effect(self, optimizer):
        """Test early stopping with ftol"""
        optm, params = optimizer
        params["ftol"] = 0.01
        opt = optm(**params)
        opt.optimize(objective_function, iters=iterations, **kwargs)
        assert len(opt.cost_history) <= iterations
