def test_copy_job_cancel(card):
    src_dir, destinations = card

    job = CopyJob(src_dir, destinations)
    job.cancel()

    while job.finished is not True:
        sleep(0.1)

    # Only hash files should be present
    for dest in destinations:
        assert len(list((dest / "src").glob("**/*"))) == 2

