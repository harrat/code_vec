def test_flex_alloc_not_enough_nodes_constraint_partition(make_flexible_job):
    job = make_flexible_job('all')
    job.options = ['-C f1&f2', '--partition=p1,p2']
    job.num_tasks = -8
    with pytest.raises(JobError):
        prepare_job(job)
