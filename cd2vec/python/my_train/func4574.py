def test_dev_with_repo_subdir(capsys, basic_conf_with_subdir):
    """
    Same as test_dev, but with the Python project inside a subdirectory.
    """
    tmpdir, local, conf = basic_conf_with_subdir

    # Test Dev runs
    tools.run_asv_with_conf(conf, 'dev', '--quick',
                            '--bench=time_secondary.track_value',
                            _machine_file=join(tmpdir, 'asv-machine.json'))
    text, err = capsys.readouterr()

    # Benchmarks were found and run
    assert re.search(r"time_secondary.track_value\s+42.0", text)

    # Check that it did not clone or install
    assert "Cloning" not in text
    assert "Installing" not in text

