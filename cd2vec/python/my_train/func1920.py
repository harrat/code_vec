@log_info
    def test_01_autodiscover(self):
        """Test ``autodiscover``."""
        autodiscover()
        self.assertTrue(len(registry.registry) > 0)
