def test_mirror_samples_real(testfiles):
    annotation = flatten_dict(testfiles, 'annotation').values()
    ontology = flatten_dict(testfiles, 'ontology').values()
    orig = [363, 470]
    for an, on, o in zip(annotation, ontology, orig):
        out = samples_.mirror_samples(an, on)
        # there should be more than the original # of samples but less than or
        # equal to 2x that number (we can't MORE than duplicate)
        assert len(out) > o and len(out) <= o * 2

