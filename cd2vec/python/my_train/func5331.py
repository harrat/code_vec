def test_app_store_footer_popup_displayed(instagram, element):
    """ Verify footer popup detection works """
    instagram.driver.execute_script.return_value = 100

    side_effects = [[], [element, ], [element, ], [element, ]]
    instagram.driver.find_elements_by_css_selector.side_effect = side_effects

    element.is_displayed.side_effect = [False, True, True, True]

    loc_side_effect = [SERE, {'y': 110}, {'y': 90}]
    type(element).location = PropertyMock(side_effect=loc_side_effect)

    instagram.app_store_footer_popup_displayed is False
    instagram.app_store_footer_popup_displayed is False
    instagram.app_store_footer_popup_displayed is False
    instagram.app_store_footer_popup_displayed is True

