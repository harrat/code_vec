@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'export-time')
    def test_cli_export_solution_026(snippy):
        """Export solutions with search keyword.

        Export solutions based on search keyword. In this case the search
        keyword matchies to two solutions that must be exported to file
        defined in command line.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Content.deepcopy(Solution.BEATS),
                Content.deepcopy(Solution.NGINX)
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '--sall', 'howto', '-f', './defined-solutions.txt'])
            assert cause == Cause.ALL_OK
            Content.assert_text(mock_file, './defined-solutions.txt', content)
