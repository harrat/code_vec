def test_play_playcontext(self):
        agent = BackendAgentTest.DebugAgent()
        count = easyagents.callbacks.log._CallbackCounts()
        agent.play(play_context=self.pc, callbacks=[count])
        assert self.pc.play_done is True
        assert self.pc.episodes_done == self.pc.num_episodes
        assert self.pc.steps_done == self.pc.num_episodes * self.pc.max_steps_per_episode == 10
        assert self.pc.steps_done_in_episode == self.pc.max_steps_per_episode == 5
