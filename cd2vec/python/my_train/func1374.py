def test_uuid_resolver(self):
        """Test dump and load of uuid objects."""

        uuid_in = {'test': uuid.UUID(TEST_UUID)}

        yaml.add_representer(uuid.UUID, uuid_representer, Dumper=TestDumper)
        uuid_add_implicit_resolver(loader=TestLoader, dumper=TestDumper)

        self.assertEqual(yaml.dump(uuid_in, Dumper=TestDumper),
                         'test: cc3702ca-699a-4aa6-8226-4c938f294d9b\n')
        self.assertEqual(yaml.load(UUID_OUT, Loader=TestLoader),
                         EXPECTED_UUID_OBJ)
