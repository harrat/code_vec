@staticmethod
    @pytest.mark.usefixtures('import-gitlog', 'update-pytest-utc')
    def test_cli_update_reference_016(snippy, edited_gitlog):
        """Update reference with editor.

        Update existing resource without doing any changes to the resource.
        The ``updated`` timestamp must not be changed because the resource
        was not changed.
        """

        content = {
            'data': [
                Reference.GITLOG
            ]
        }
        edited_gitlog.return_value = Content.dump_text(content['data'][0])
        cause = snippy.run(['snippy', 'update', '-d', '5c2071094dbfaa', '--format', 'text'])
        edited_gitlog.assert_called_with(Content.dump_text(Reference.GITLOG))
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
