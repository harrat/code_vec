def test_name_attr(person: Person):
    assert isinstance(person.name, str)
    assert person.name == person.data["name"]

