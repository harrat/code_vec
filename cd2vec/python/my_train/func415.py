def test_unsupported_chunksize(self):
        kvs = dict()
        crypto_wrapper = seccs.crypto_wrapper.SHA_256()
        self.assertRaises(
            seccs.UnsupportedChunkSizeError, seccs.SecCSLite, 63, kvs, crypto_wrapper)
