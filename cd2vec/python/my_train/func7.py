def test_simple_signal_object_registration():

    class Foo():
        def on_test(self, a, b):
            a / b
        __init__ = register_object

    f = Foo()

    with pytest.raises(ZeroDivisionError):
        on_test(a=5, b=0, c='c')

    unregister_object(f)

