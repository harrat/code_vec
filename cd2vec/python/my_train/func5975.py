def test_open_safe(self):
        with self.assertRaises(IOError):
            with open_('foobar', mode='r', errors=True) as _:
                pass
        with self.assertRaises(ValueError):
            with open_(cast(IOBase, None), mode='r', errors=True) as _:
                pass
        with open_('foobar', mode='r', errors=False) as fh:
            self.assertIsNone(fh)
        with open_(cast(IOBase, None), mode='r', errors=False) as fh:
            self.assertIsNone(fh)
