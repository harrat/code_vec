def test_call_alias(config_file):
    alias = "tests"
    command = "pytest /path/to/tests"
    config_file.add_section(alias)
    config_file.set(alias, command)
    call_alias = call_recorder(lambda x, y, z: (y, z))
    call_alias(config_file, alias, command)

    assert call_alias.calls == [call(config_file, alias, command)]

