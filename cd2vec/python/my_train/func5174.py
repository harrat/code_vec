def test_hybrid_rr_and_eltorito_nofiles2(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('rreltoritonofiles2')
    outfile = str(indir)+'.iso'
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-rational-rock', '-o', str(outfile), str(indir)])

    # Now open up the ISO with pycdlib and check some things out.
    iso = pycdlib.PyCdlib()

    iso.open(str(outfile))

    bootstr = b'boot\n'
    iso.add_fp(BytesIO(bootstr), len(bootstr), '/BOOT.;1', rr_name='boot')
    iso.add_eltorito('/BOOT.;1', '/BOOT.CAT;1')

    do_a_test(iso, check_rr_and_eltorito_nofiles)

    iso.close()

def test_hybrid_rr_and_eltorito_onefile(tmpdir):
    # First set things up, and generate the ISO with genisoimage.
    indir = tmpdir.mkdir('rreltoritoonefile')
    outfile = str(indir)+'.iso'
    subprocess.call(['genisoimage', '-v', '-v', '-iso-level', '1', '-no-pad',
                     '-rational-rock', '-o', str(outfile), str(indir)])
