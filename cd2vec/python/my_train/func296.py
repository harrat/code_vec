@pytest.mark.parametrize('deck_id,num', [(1, 1), (1, 3)])
def test_get_cards_to_study(deck_id: int, num: int) -> None:
    deck = spacedr.get_deck_by_id(deck_id)
    cards_to_study = spacedr.get_cards_to_study(deck, num)

    assert len(cards_to_study) <= num

    for card in cards_to_study:
        assert card.deck_id == deck.id
        assert card.level == 0
        assert card.due_date == None
        assert card.reviews_count == 0

