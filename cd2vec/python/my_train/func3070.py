def test_misformed(self):
        with pytest.raises(InvalidIdentifierError) as exc_info:
            api.enhance_metadata( apple_serial='XXBADSERIALXX', )

        with pytest.raises(BadRequestError) as exc_info:
            api.enhance_metadata( apple_serial=None, )
