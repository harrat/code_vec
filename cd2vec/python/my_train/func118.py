def test_edit():
	import copy
	data_ = copy.copy(data)
	header = ljson.base.generic.Header(header_descriptor)
	table = ljson.base.mem.Table(header, data_)

	table[{"lname": "griffin"}]["lname"] = "Griffin"

	data_ = []

	for row in data:
		row_ = copy.copy(row)
		row_["lname"] = "Griffin"
		data_.append(row_)

	assert list(table) == data_

	table.additem(item_meg)
	assert list(table) == data_ + [item_meg]

