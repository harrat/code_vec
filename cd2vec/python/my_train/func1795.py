@pytest.mark.minimal
def test_available():
    """
    Check that files in registry are available for download
    """
    for fname in REGISTRY.registry_files:
        assert REGISTRY.is_available(fname)

