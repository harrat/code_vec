def test_photostatus(client):
    """Test the GetPhotoStatus SOAPAction.

    Generates `credential` from the test_config upload_key and macaddress and
    snonce obtained in in `test_startsession` above.
    """

    headers = {'SOAPAction': '"urn:GetPhotoStatus"'}

    credential = create_credential("0a1b2c3d4e5f", snonce, "abcd",
                                   from_eyefi=True)
    response = client.post("/api/soap/eyefilm/v1", headers=headers,
                           data=get_photo_status.format(credential=credential))
    assert response.status_code == 200

