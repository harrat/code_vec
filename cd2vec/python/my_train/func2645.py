def test_version_dirty_false_if_not_dirty(self):
        v = relic.release.get_info()
        assert isinstance(v.dirty, bool)
        assert not v.dirty
