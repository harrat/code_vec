def test__rnn(self):
        self.model.hidden = self.model._init_hidden()
        X6, hidden = self.model._rnn(self.X5)
        # (9, 8) is length of packed sequence and dimension of hidden
        self.assertEqual(X6.data.shape, (9, 8))
        self.assertEqual(hidden[0].shape, (2, 3, 4))
        self.assertEqual(hidden[1].shape, (2, 3, 4))
