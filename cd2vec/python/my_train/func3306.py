@pytest.mark.parametrize('key, value', [
    ('Do', 10),
    ('mu', -1),
    ('S', 93.7247943)])
def test_update_result_tables(class_objects, key, value):
    d_source, d_class, s_width = class_objects
    obj = DrizzleSolving(d_source, d_class, s_width)
    ind = (0, 1)
    dia = 10
    lut = (0, 1)
    obj._update_result_tables(ind, dia, lut)
    testing.assert_almost_equal(obj.params[key][ind], value)

