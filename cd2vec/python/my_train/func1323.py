def test_read_config():
    """Test opening files"""
    # Try to open a non existant config
    assert AUTH.read_config('nofile.json') is False

    # Try a directory
    assert AUTH.read_config('readonly') is False

    # Try a good file
    assert isinstance(AUTH.read_config("test.json"), dict) is True

    # Try a bad file
    with open("config/bad_file.json", 'w') as outfile:
        outfile.write("Thisisbaddata")
    assert AUTH.read_config("bad_file.json") is False

