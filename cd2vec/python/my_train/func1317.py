@staticmethod
    @pytest.mark.usefixtures('import-beats', 'caller')
    def test_api_update_solution_003(server):
        """Update one solution with PUT request.

        Try to send PUT /solutions/{id} to update resource with ``id`` in
        URI that is not found.
        """

        storage = {
            'data': [
                Storage.ebeats
            ]
        }
        request_body = {
            'data': {
                'type': 'snippet',
                'attributes': {
                    'data': Request.dnginx['data'],
                    'brief': Request.dnginx['brief'],
                    'groups': Request.dnginx['groups'],
                    'tags': Request.dnginx['tags'],
                    'links': Request.dnginx['links']
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '377'
        }
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '404', 'statusString': '404 Not Found', 'module': 'snippy.testing.testing:123',
                'title': 'cannot find content with content identity: 101010101010101'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_put(
            path='/api/snippy/rest/solutions/101010101010101',
            headers={'accept': 'application/json'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_404
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
