def test_utils_load_cached(path):
    _load = utils.load

    with mock.patch(
        'fake_useragent.utils.load',
        side_effect=_load,
    ) as mocked:
        data = utils.load_cached(path, use_cache_server=False)

        mocked.assert_called()

    expected = {
        'randomize': mock.ANY,
        'browsers': {
            'chrome': mock.ANY,
            'firefox': mock.ANY,
            'opera': mock.ANY,
            'safari': mock.ANY,
            'internetexplorer': mock.ANY,
        },
    }

    assert expected == data

    expected = data

    with mock.patch('fake_useragent.utils.load') as mocked:
        data = utils.load_cached(path, use_cache_server=False)

        mocked.assert_not_called()

    assert expected == data

