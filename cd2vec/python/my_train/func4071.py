def test_set(bloomtime_fixture, caplog):
    caplog.set_level(logging.DEBUG)

    bloomtime_fixture.set('foo')
    # We know 9 hashes should be set.
    total = 0
    for i in bloomtime_fixture._container:
        if i > 0:
            total += 1
    assert total == 9

