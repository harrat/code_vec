@pytest.mark.parametrize('kernels', list_to_test)
@pytest.mark.parametrize('diff_cutoff', multi_cut)
def test_check_sig_scale(kernels, diff_cutoff):
    """Check whether the grouping is properly assign
    with four environments

    * env1 and env2 are computed from two structures with four
    atoms each. There are two species 1, 2
    * env1_t and env2_t are derived from the same structure, but
      species 2 atoms are removed.
    * only the sigma of 1-1 are non-zero
    * so using env1 and env1_t should produce the same value
    * if the separate group of hyperparameter is properly
      applied, the result should be 2**2 times of
      the reference
    """

    d1 = 1
    d2 = 2
    tol = 1e-4
    scale = 2

    cutoffs, hyps0, hm = generate_diff_hm(kernels, diff_cutoff)

    delta = 1e-8
    env1, env1_t = generate_mb_twin_envs(cutoffs, np.eye(3)*100, delta, d1, hm)
    env2, env2_t = generate_mb_twin_envs(cutoffs, np.eye(3)*100, delta, d2, hm)
    env1 = env1[0][0]
    env2 = env2[0][0]
    env1_t = env1_t[0][0]
    env2_t = env2_t[0][0]

    # make the second sigma zero
    hyps1 = np.copy(hyps0)
    hyps0[0::4] = 0  # 1e-8
    hyps1[0::4] = 0  # 1e-8
    hyps1[1::4] *= scale

    kernel, kg, en_kernel, force_en_kernel, _, _, _ = str_to_kernel_set(
        kernels, 'mc', hm)

    args0 = from_mask_to_args(hyps0, cutoffs, hm)
    args1 = from_mask_to_args(hyps1, cutoffs, hm)

    reference = en_kernel(env1, env2, *args0)
    result = en_kernel(env1_t, env2_t, *args1)
    print(en_kernel.__name__, result, reference)
    if (reference != 0):
        assert isclose(result/reference, scale**2, rtol=tol)

    reference = force_en_kernel(env1, env2, d1, *args0)
    result = force_en_kernel(env1_t, env2_t, d1, *args1)
    print(force_en_kernel.__name__, result, reference)
    if (reference != 0):
        assert isclose(result/reference, scale**2, rtol=tol)

    reference = kernel(env1, env2, d1, d2, *args0)
    result = kernel(env1_t, env2_t, d1, d2, *args1)
    print(kernel.__name__, result, reference)
    if (reference != 0):
        assert isclose(result/reference, scale**2, rtol=tol)

    reference = kg(env1, env2, d1, d2, *args0)
    result = kg(env1_t, env2_t, d1, d2, *args1)
    print(kg.__name__, result, reference)
    if (reference[0] != 0):
        assert isclose(result[0]/reference[0], scale**2, rtol=tol)
    for idx in range(reference[1].shape[0]):
        # check sig0
        if (reference[1][idx] != 0 and (idx % 4) == 0):
            assert isclose(result[1][idx]/reference[1][idx], scale, rtol=tol)
        # check the rest, but skip sig 1
        elif (reference[1][idx] != 0 and (idx % 4) != 1):
            assert isclose(result[1][idx]/reference[1]
                           [idx], scale**2, rtol=tol)
