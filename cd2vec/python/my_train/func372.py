@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_delete_snippet_011(snippy):
        """Delete snippet with data.

        Try to delete snippet with empty content data. Nothing should be
        deleted in this case because there is more than one content left.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--content', ''])
        assert cause == 'NOK: cannot use empty content data for delete operation'
        Content.assert_storage(content)
