def test_check_sanity_failure(run_reframe, tmp_path):
    returncode, stdout, stderr = run_reframe(
        checkpath=['unittests/resources/checks/frontend_checks.py'],
        more_options=['-t', 'SanityFailureCheck']
    )
    assert 'FAILED' in stdout

    # This is a normal failure, it should not raise any exception
    assert 'Traceback' not in stdout
    assert 'Traceback' not in stderr
    assert returncode != 0
    assert os.path.exists(
        tmp_path / 'stage' / 'generic' / 'default' /
        'builtin-gcc' / 'SanityFailureCheck'
    )
