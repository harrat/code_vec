def test_crawl(self, crawler_process_mock):
        result = self.crawl_manager.crawl()
        self.assertIsInstance(result, Deferred)
        self.assertGreater(len(result.callbacks), 0)
        self.assertEqual(
            result.callbacks[0][0][0], self.crawl_manager.return_items)
