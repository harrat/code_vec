def test_find(self):
        self._make_request(
            'https://stackoverflow.com/questions/tagged/django?page=2&sort=newest&pagesize=15')
        self._make_request(
            'https://docs.python.org/3.4/library/http.client.html')
        self._make_request('https://www.google.com')

        self.assertEqual(
            'https://stackoverflow.com/questions/tagged/django?page=2&sort=newest&pagesize=15',
            self.client.find('/questions/tagged/django')['url']
        )
        self.assertEqual(
            'https://docs.python.org/3.4/library/http.client.html',
            self.client.find('/3.4/library/http.client.html')['url']
        )
        self.assertEqual(
            'https://www.google.com/',
            self.client.find('https://www.google.com')['url']
        )
