def test_not_supported_error():
    assert issubclass(py2jdbc.NotSupportedError, py2jdbc.DatabaseError), \
        "NotSupportedError is not a subclass of DatabaseError"
