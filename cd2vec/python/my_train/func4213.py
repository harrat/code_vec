@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_reference_023(snippy):
        """Import reference from Markdown template.

        Try to import reference template without any changes. This should result
        error text for end user and no files should be read. The error text must
        be the same for all content types.
        """

        file_content = mock.mock_open(read_data=Const.NEWLINE.join(Reference.TEMPLATE_MKDN))
        with mock.patch('snippy.content.migrate.io.open', file_content, create=True) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'reference', '--template'])
            assert cause == 'NOK: content was not stored because it was matching to an empty template'
            Content.assert_storage(None)
            Content.assert_arglist(mock_file, './reference-template.mkdn', mode='r', encoding='utf-8')
