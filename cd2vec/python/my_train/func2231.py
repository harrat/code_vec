@pytest.mark.parametrize("number_of_elements, number_of_features, gamma, number_of_iterations, standardize",(
                        (20, 5, 1, 100, False),
                        (50, 10, 0.8, 500, False),
                        (1000, 3, 1, 1000, False),
                        (20, 5, 1, 100, True),
                        (50, 10, 0.8, 500, True),
                        (1000, 3, 1, 1000, True),
))
def test_train_deltr_synthetic_data(number_of_elements, number_of_features, gamma, number_of_iterations, standardize):

    # create a dataset
    sdc = SyntheticDatasetCreator(20, {'protected_feature': 2}, list(range(number_of_features-1)))
    data = sdc.dataset

    # score the elements based on some predefined weights
    weights = [10*w for w in range(number_of_features)]
    data['judgement'] = data.apply(lambda row: np.dot(row, weights), axis=1)

    # add query and document ids
    data['id'] = pd.Series([1] * number_of_elements)
    data['doc_id'] = data['doc_id'] = pd.Series(range(number_of_elements))

    # arrange the field names
    data = data[['id', 'doc_id', 'protected_feature'] + list(range(number_of_features-1)) + ['judgement']]

    # sort the elements by the judgement in a descending fashion
    data = data.sort_values(['judgement'], ascending=[0])

    # train a model
    d = Deltr('protected_feature', gamma, number_of_iterations, standardize=standardize)
    d.train(data)

    assert d.log != []

    if len(d.log) > 1:
        current = d.log[0].loss
        for log in d.log[1:]:
            assert log.loss <= current
            current = log.loss
    else:
        assert d.log[0]
