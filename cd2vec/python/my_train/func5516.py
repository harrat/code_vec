def test_eq_assoccomm():
    x, y = var(), var()

    ac = "commassoc_op"

    commutative.index.clear()
    commutative.facts.clear()

    fact(commutative, ac)
    fact(associative, ac)

    assert run(0, True, eq_assoccomm(1, 1)) == (True,)
    assert run(0, True, eq_assoccomm((1,), (1,))) == (True,)
    assert run(0, True, eq_assoccomm(x, (1,))) == (True,)
    assert run(0, True, eq_assoccomm((1,), x)) == (True,)

    # Assoc only
    assert run(0, True, eq_assoccomm((ac, 1, (ac, 2, 3)), (ac, (ac, 1, 2), 3))) == (
        True,
    )
    # Commute only
    assert run(0, True, eq_assoccomm((ac, 1, (ac, 2, 3)), (ac, (ac, 3, 2), 1))) == (
        True,
    )
    # Both
    assert run(0, True, eq_assoccomm((ac, 1, (ac, 3, 2)), (ac, (ac, 1, 2), 3))) == (
        True,
    )

    exp_res = set(
        (
            (ac, 1, 3, 2),
            (ac, 1, 2, 3),
            (ac, 2, 1, 3),
            (ac, 2, 3, 1),
            (ac, 3, 1, 2),
            (ac, 3, 2, 1),
            (ac, 1, (ac, 2, 3)),
            (ac, 1, (ac, 3, 2)),
            (ac, 2, (ac, 1, 3)),
            (ac, 2, (ac, 3, 1)),
            (ac, 3, (ac, 1, 2)),
            (ac, 3, (ac, 2, 1)),
            (ac, (ac, 2, 3), 1),
            (ac, (ac, 3, 2), 1),
            (ac, (ac, 1, 3), 2),
            (ac, (ac, 3, 1), 2),
            (ac, (ac, 1, 2), 3),
            (ac, (ac, 2, 1), 3),
        )
    )
    assert set(run(0, x, eq_assoccomm((ac, 1, (ac, 2, 3)), x))) == exp_res
    assert set(run(0, x, eq_assoccomm((ac, 1, 3, 2), x))) == exp_res
    assert set(run(0, x, eq_assoccomm((ac, 2, (ac, 3, 1)), x))) == exp_res
    # LHS variations
    assert set(run(0, x, eq_assoccomm(x, (ac, 1, (ac, 2, 3))))) == exp_res

    assert run(0, (x, y), eq_assoccomm((ac, (ac, 1, x), y), (ac, 2, (ac, 3, 1)))) == (
        (2, 3),
        (3, 2),
    )

    assert run(0, True, eq_assoccomm((ac, (ac, 1, 2), 3), (ac, 1, 2, 3))) == (True,)
    assert run(0, True, eq_assoccomm((ac, 3, (ac, 1, 2)), (ac, 1, 2, 3))) == (True,)
    assert run(0, True, eq_assoccomm((ac, 1, 1), ("other_op", 1, 1))) == ()

    assert run(0, x, eq_assoccomm((ac, 3, (ac, 1, 2)), (ac, 1, x, 3))) == (2,)

    # Both arguments unground
    op_lv = var()
    z = var()
    res = run(4, (x, y), eq_assoccomm(x, y))
    exp_res_form = (
        (etuple(op_lv, x, y), etuple(op_lv, y, x)),
        (y, y),
        (etuple(etuple(op_lv, x, y)), etuple(etuple(op_lv, y, x)),),
        (etuple(op_lv, x, y, z), etuple(op_lv, etuple(op_lv, x, y), z),),
    )

    for a, b in zip(res, exp_res_form):
        s = unify(a, b)
        assert (
            op_lv not in s
            or (s[op_lv],) in associative.facts
            or (s[op_lv],) in commutative.facts
        )
        assert s is not False, (a, b)
        assert all(isvar(i) for i in reify((x, y, z), s))

