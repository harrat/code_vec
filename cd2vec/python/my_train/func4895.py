@given(dataframes=n_time_series_with_same_index(min_n=5))
    def test_fit_predict_basic_middle_out_fp_method(
        self, dataframes, hierarchical_basic_middle_out_model_fp_method
    ):
        hierarchical_basic_middle_out_model_fp_method.fit(dataframes).predict(
            dataframes
        )
