def test_read_models_from_module():
    expected = {
        'WeatherReport': WEATHER_REPORT_DEFINITION,
        'WeatherStats': WEATHER_STATS_DEF,
        'WeatherPrivateData': WEATHER_PRIVATE_DATA
    }
    data = schematics_to_swagger.read_models_from_module(models)
    assert expected == data

