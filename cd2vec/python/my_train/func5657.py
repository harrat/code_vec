def test_revoke_missing_revocation_reason(self):
        """
        Test that the right error is generated when a revocation request is
        received with a missing revocation reason.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        managed_object = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )
        e._data_session.add(managed_object)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        self.assertEqual(enums.State.PRE_ACTIVE, managed_object.state)

        object_id = str(managed_object.unique_identifier)

        date = primitives.DateTime(
            tag=enums.Tags.COMPROMISE_OCCURRENCE_DATE, value=6)

        payload = payloads.RevokeRequestPayload(
            unique_identifier=attributes.UniqueIdentifier(object_id),
            revocation_reason=None,
            compromise_occurrence_date=date)
        payload.revocation_reason = None

        args = (payload, )
        self.assertRaisesRegex(
            exceptions.InvalidField,
            "revocation reason code must be specified",
            e._process_revoke,
            *args
        )
