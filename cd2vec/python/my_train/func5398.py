@pytest.mark.parametrize('name,description,id', [('what', 'nothing', 1),
                                                 ('test', None, 2)])
def test_edit_deck(name: str, description: str, id: int) -> None:
    deck = spacedr.get_deck_by_id(id)
    spacedr.edit_deck(deck, name=name, description=description)

    assert deck.name == name
    assert deck.description == description or 'nothing'

