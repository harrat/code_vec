def test_execute_env_function_raise_on_error(self, venv):
        """Assert raising an error (error propagation turned on by default)."""
        with pytest.raises(ValueError) as exc:
            execute_env_function(venv.python, _func_err)
            assert str(exc).endswith("this is error message")
