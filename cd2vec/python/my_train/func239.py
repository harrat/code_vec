@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_update_solution_001(snippy, edited_beats):
        """Update solution with ``--digest`` option.

        Update solution with short message digest. Only the content data is
        updated. Because the description tag was changed, the ``description``
        attribute is not read and it results an empty string.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.BEATS),
                Solution.NGINX
            ]
        }
        content['data'][0]['data'] = tuple([line.replace('## Description', '## updated desc') for line in content['data'][0]['data']])
        content['data'][0]['description'] = ''
        content['data'][0]['digest'] = '23312e20cb961d46b3fb0ac5a63dacfbb16f13a220b48250019977940e9720f3'
        edited_beats.return_value = Content.dump_text(content['data'][0])
        cause = snippy.run(['snippy', 'update', '--scat', 'solution', '-d', '4346ba4c79247430', '--format', 'text'])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
