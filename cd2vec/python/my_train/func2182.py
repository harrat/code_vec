def test_should_fail_to_load_missing_rc(self):
        directory = os.path.join(DATA_DIR, 'temp', 'missing')
        os.mkdir(directory)
        rc = None
        with pytest.raises(Exception) as my_error:
            rc = factory.load(directory)
        assert 'Not a resource container. Missing manifest.yaml' in str(my_error.value)
        assert rc is None
