@mock.patch('thundra.integrations.botocore.BaseIntegration.actual_call')
def test_eventbridge_put_events(mock_actual_call, mock_eventbridge_put_events_response):
    mock_actual_call.return_value = mock_eventbridge_put_events_response
    tracer = ThundraTracer.get_instance()
    tracer.clear()
    try:
        client = boto3.client('events', region_name='us-west-2')
        client.put_events(
            Entries=[
                {
                    'Time': datetime(2020, 1, 1),
                    'Source': 'test.aws.lambda',
                    'Resources': [
                        'test',
                    ],
                    'DetailType': 'mydetail',
                    'Detail': '{}',
                    'EventBusName': 'default'
                },
            ]
        )
    except Exception as e:
        print(e)
    finally:
        span = tracer.get_spans()[0]
        assert span.class_name == constants.ClassNames['EVENTBRIDGE']
        assert span.domain_name == constants.DomainNames['MESSAGING']
        assert span.operation_name == 'default'
        assert span.get_tag(constants.SpanTags['OPERATION_TYPE']) == 'WRITE'
        assert span.get_tag(constants.AwsEventBridgeTags['ENTRIES']) == [{
                    'Time': datetime.timestamp(datetime(2020, 1, 1)),
                    'Source': 'test.aws.lambda',
                    'Resources': [
                        'test',
                    ],
                    'DetailType': 'mydetail',
                    'Detail': '{}',
                    'EventBusName': 'default'
                }]
        assert span.get_tag(constants.AwsSDKTags['REQUEST_NAME']) == 'PutEvents'
        assert span.get_tag(constants.SpanTags['RESOURCE_NAMES']) == ['mydetail']
        assert span.get_tag(constants.SpanTags['TRACE_LINKS']) == ['test-event-id']
