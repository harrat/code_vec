def test_multiplicity(self):
		model.factory.process_multiplicity = True
		who = model.Actor()
		mmo = model.HumanMadeObject()
		prod = model.Production()
		mmo.produced_by = prod
		who.current_owner_of = mmo
		mmo.current_owner = who
		self.assertEqual(mmo.current_owner, [who])
		self.assertEqual(who.current_owner_of, [mmo])		
		self.assertEqual(mmo.produced_by, prod)
