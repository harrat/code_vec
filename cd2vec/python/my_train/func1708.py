@freeze_time('2014-01-22')
def test_commit_confirmation_date_order(cli, entries_file):
    entries_file.write("""20/01/2014
alias_1 2 Play ping-pong

18/01/2014
alias_1 2 Play ping-pong

19/01/2014
alias_1 1 Play ping-pong
""")
    stdout = cli('commit', input='y').splitlines()
    dates = [stdout[2], stdout[4], stdout[6]]
    assert dates[0].endswith('18 January')
    assert dates[1].endswith('19 January')
    assert dates[2].endswith('20 January')

