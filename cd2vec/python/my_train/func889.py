def test_execute_param_sequence():
    class L(object):
        def __len__(self):
            return 1

        def __getitem__(self, x):
            assert x == 0
            return 'test_execute_param_sequence'

    cu.execute("insert into tests(id, name) values (9, 'test_execute_param_sequence')")
    cu.execute("select name from tests where name=?", L())
    row = cu.fetchone()
    assert row[0] == 'test_execute_param_sequence'

