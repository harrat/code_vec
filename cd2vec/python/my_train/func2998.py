def test_flex_alloc_constraint_idle(make_flexible_job):
    job = make_flexible_job('idle')
    job.options = ['--constraint=f1']
    prepare_job(job)
    assert job.num_tasks == 8

