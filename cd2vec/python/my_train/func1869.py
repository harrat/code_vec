def test_event_listeners(self):
        class MockEventListener(EventListener):
            def __init__(self):
                super().__init__()
                self.executed = False

            def execute(self, file_wrapper: FileLikeWrapper, **kwargs):
                self.executed = True

        std_listener: MockEventListener = MockEventListener()
        with intercept_stdin('foo'):
            f = xopen(STDIN, context_wrapper=True)
            try:
                cast(EventManager, f).register_listener(
                    EventType.CLOSE, std_listener)
            finally:
                f.close()
            self.assertTrue(std_listener.executed)

        file_listener: MockEventListener = MockEventListener()
        path = self.root.make_file()
        f = xopen(path, 'w', context_wrapper=True)
        try:
            cast(EventManager, f).register_listener(
                EventType.CLOSE, file_listener)
        finally:
            f.close()
        self.assertTrue(file_listener.executed)
