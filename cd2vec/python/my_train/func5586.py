@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_api_search_solution_014(server):
        """Search solution without search parameters.

        Send GET /solutions without defining search parameters. In this
        case all content should be returned.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '4847'
        }
        expect_body = {
            'meta': {
                'count': 2,
                'limit': 20,
                'offset': 0,
                'total': 2
            },
            'data': [{
                'type': 'solution',
                'id': Solution.BEATS_UUID,
                'attributes': Storage.ebeats
            }, {
                'type': 'solution',
                'id': Solution.NGINX_UUID,
                'attributes': Storage.dnginx
            }]
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/solutions',
            headers={'accept': 'application/json'},
            query_string='limit=20&sort=brief')
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
