def test_middle_end_bad1(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        with self.assertRaises(ValueError):
            audiofile.middle_end = -1
