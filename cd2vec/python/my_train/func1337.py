@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_update_snippet_010(snippy):
        """Update snippet with ``--content`` option.

        Try to update snippet based on content data that is not found.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        cause = snippy.run(['snippy', 'update', '-c', 'snippet not existing'])
        assert cause == 'NOK: cannot find content with content data: snippet not existing'
        Content.assert_storage(content)
