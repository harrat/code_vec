def test_xopen_file(self):
        with self.assertRaises(IOError):
            xopen('foobar', 'r')
        path = self.root.make_file(suffix='.gz')
        with xopen(path, 'rU') as i:
            assert 'rt' == i.mode
        with xopen(path, 'w', compression=True, context_wrapper=True) as o:
            assert cast(FileLikeWrapper, o).compression == 'gzip'
            o.write('foo')
        with gzip.open(path, 'rt') as i:
            assert i.read() == 'foo'
        with self.assertRaises(ValueError):
            with xopen(path, 'rt', compression='bz2', validate=True):
                pass
        existing_file = self.root.make_file(contents='abc')
        with xopen(existing_file, 'wt', overwrite=True) as out:
            out.write('def')
        with self.assertRaises(ValueError):
            with xopen(existing_file, 'wt', overwrite=False):
                pass
