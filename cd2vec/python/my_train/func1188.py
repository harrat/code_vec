def test_no_preamble_command_group_dataset(self):
        """Test correct read of group 0 and 2 elements with no preamble."""
        bytestream = (
            b"\x02\x00\x02\x00\x55\x49\x16\x00\x31\x2e\x32\x2e"
            b"\x38\x34\x30\x2e\x31\x30\x30\x30\x38\x2e\x35\x2e"
            b"\x31\x2e\x31\x2e\x39\x00\x02\x00\x10\x00\x55\x49"
            b"\x12\x00\x31\x2e\x32\x2e\x38\x34\x30\x2e\x31\x30"
            b"\x30\x30\x38\x2e\x31\x2e\x32\x00"
            b"\x20\x20\x10\x00\x02\x00\x00\x00\x01\x00\x20\x20"
            b"\x20\x00\x06\x00\x00\x00\x4e\x4f\x52\x4d\x41\x4c"
            b"\x00\x00\x10\x01\x02\x00\x00\x00\x03\x00"
        )

        fp = BytesIO(bytestream)
        ds = dcmread(fp, force=True)
        assert "MediaStorageSOPClassUID" in ds.file_meta
        assert ImplicitVRLittleEndian == ds.file_meta.TransferSyntaxUID
        assert "NORMAL" == ds.Polarity
        assert 1 == ds.ImageBoxPosition
        assert 3 == ds.MessageID
