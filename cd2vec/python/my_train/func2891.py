def test_setup(self):
        if fixtures.has_sane_modules_system():
            assert len(self.environ.modules) == 1
            assert 'testmod_foo' in self.environ.modules

        assert len(self.environ.variables.keys()) == 3
        assert self.environ.variables['_var0'] == 'val1'

        # No variable expansion, if environment is not loaded
        self.environ.variables['_var2'] == '$_var0'
        self.environ.variables['_var3'] == '${_var1}'
