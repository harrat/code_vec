@pytest.mark.skipif(not HAS_CLOCK,
                    reason="time.clock was removed in Python 3.8")
def test_time_clock():
    with freeze_time('2012-01-14 03:21:34'):
        assert time.clock() == 0
