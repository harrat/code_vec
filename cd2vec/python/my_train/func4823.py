@pytest.mark.parametrize('id', [2])
def test_delete_deck(id: int) -> None:
    deck = spacedr.get_deck_by_id(id)
    spacedr.delete_deck(deck)

    with pytest.raises(spacedr.errors.DeckNotFoundError):
        spacedr.get_deck_by_id(id)
