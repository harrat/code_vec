@staticmethod
    @pytest.mark.usefixtures('create-gitlog-utc')
    def test_cli_create_reference_001(snippy):
        """Create reference from CLI.

        Create a new reference by defining all content attributes from command
        line. The content ``data`` attribute must not be used when creating new
        reference content.
        """

        content = {
            'data': [
                Content.deepcopy(Reference.GITLOG)
            ]
        }
        content['data'][0]['uuid'] = Content.UUID1
        data = 'must not be used'
        brief = content['data'][0]['brief']
        groups = content['data'][0]['groups']
        tags = content['data'][0]['tags']
        links = Const.DELIMITER_LINKS.join(content['data'][0]['links'])
        cause = snippy.run(['snippy', 'create', '--scat', 'reference', '--links', links, '-b', brief, '-g', groups, '-t', tags, '-c', data])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
