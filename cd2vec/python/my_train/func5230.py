def test_bundle_object_categories(self):
        bundle = copy.deepcopy(self.valid_bundle)
        bundle['identities'] = bundle['objects']
        del bundle['objects']
        self.assertFalseWithOptions(bundle)
