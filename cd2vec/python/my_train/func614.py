def test_open_vocab_format_uppercase(self):
        malware = copy.deepcopy(self.valid_malware)
        malware['labels'] += "Ransomware"
        self.assertFalseWithOptions(malware,
                                    disabled='malware-label')
