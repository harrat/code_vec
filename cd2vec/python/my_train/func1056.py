def test_trueanom(self):
        n = int(1e6)
        E = np.random.rand(n)*2*np.pi
        e = np.random.rand(n)
        nu = trueanom(E,e)

        self.assertTrue(np.max(np.abs(e + (1-e**2)/(1+e*np.cos(nu))*np.cos(nu) - np.cos(E))) < 1e-9)
