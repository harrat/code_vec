def test_actual_http_request_with_override():

    f2c = Featureflagclient(
        "https://featureflag.tech/node/exampleflag.json",
        {
            "text": "overridden"
        }
    )

    assert f2c.get("trueBoolean") is True
    assert f2c.get("text") == "overridden"

