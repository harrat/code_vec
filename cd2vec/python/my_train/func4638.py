def test_start_b_end_b(self):
        self.grange.start = 'A2'
        self.grange.end = 'D5'
        assert self.grange.start == pygsheets.Address('A2')
        assert self.grange.end == pygsheets.Address('D5')
        assert self.grange.label == self.worksheet.title + '!' + 'A2' + ':' + 'D5'
