def test_raise_if_not_type(self):
        given = random.choice(instances)
        exclude = given.__class__
        with self.assertRaises(ValueError):
            raise_if_not_type(given, filter_if_in(types, excludes=[exclude]))
