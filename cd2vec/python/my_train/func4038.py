def test_environment_env_matrix():
    # (build_vars, non_build_vars, environ_count, build_count)
    configs = [
        ({}, {}, 1, 1),
        ({"var1": ["val1"]}, {}, 1, 1),
        ({"var1": ["val1", "val2", "val3"]}, {}, 3, 3),
        ({"var1": ["val1", "val2"], "var2": ['val3', 'val4']}, {}, 4, 4),
        ({"var1": ["val1", "val2"], "var2": ['val3', None]}, {}, 4, 4),
        ({"var1": ["val1", "val2"]}, {"var2": ['val3', None]}, 4, 2),
        ({"var1": ["val1", "val2"], "var2": ['val3', 'val4']},
         {"var3": ['val5', None]}, 8, 4),
    ]

    for build_vars, non_build_vars, environ_count, build_count in configs:
        conf = config.Config()

        conf.matrix = {
            "env": build_vars,
            "env_nobuild": non_build_vars,
        }
        environments = list(environment.get_environments(conf, None))

        assert len(environments) == environ_count
        assert len(set(e.dir_name for e in environments)) == build_count

