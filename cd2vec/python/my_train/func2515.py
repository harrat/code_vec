def test_name2idfobject():
    """py.test for name2idfobject"""
    idf = IDF(StringIO(""))
    plantloopname = "plantloopname"
    branchname = "branchname"
    pumpname = "pumpname"
    zonename = "zonename"
    plantloop = idf.newidfobject(
        "PlantLoop",
        Name=plantloopname,
        Plant_Side_Inlet_Node_Name="CW Supply Inlet Node",
    )
    branch = idf.newidfobject(
        "Branch", Name=branchname, Component_1_Inlet_Node_Name="CW Supply Inlet Node"
    )
    pump = idf.newidfobject(
        "Pump:VariableSpeed", Name=pumpname, Inlet_Node_Name="CW Supply Inlet Node"
    )
    zone = idf.newidfobject("zone", Name=zonename)
    simulation = idf.newidfobject("SimulationControl")
    # - test
    names = [plantloopname, branchname, pumpname, zonename]
    idfobjs = [plantloop, branch, pump, zone]
    for name, idfobj in zip(names, idfobjs):
        result = idf_helpers.name2idfobject(idf, Name=name)
        assert result == idfobj
    # test when objkeys!=None
    objkey = "ZoneHVAC:EquipmentConnections"
    equipconnections = idf.newidfobject(objkey, Zone_Name=zonename)
    result = idf_helpers.name2idfobject(idf, Zone_Name=zonename, objkeys=[objkey])
    assert result == equipconnections

