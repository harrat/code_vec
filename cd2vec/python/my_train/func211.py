@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'export-time')
    def test_cli_export_snippet_002(snippy):
        """Export all snippets.

        Export all snippets without defining target file name from command
        line. In this case the content category is defined explicitly.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'snippet'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './snippets.mkdn', content)
