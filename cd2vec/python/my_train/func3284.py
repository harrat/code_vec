def test_mock_module_with_two_updates():
    """
    This test should demonstrate adding two INFOs, one to the class and one to its function.
    It should add the module one first, and then the function.

    It should make sure that it uses the highest id found.
    """
    with FileCleaner('./mock_functions/test_module_stuff.py'):
        # Pretend we just ran into this when cycling through the files
        files_to_check = ['mock_functions/test_module_stuff.py']
