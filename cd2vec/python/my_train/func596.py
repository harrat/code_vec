def test_get_document_listing_empty(self, connected_adapter):
        """Test listing of documents stored on Ceph."""
        assert list(connected_adapter.get_document_listing()) == []
