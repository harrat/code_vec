def test_have_prgenv(self):
        assert 1 == self.count_checks(filters.have_prgenv('env1|env2'))
        assert 2 == self.count_checks(filters.have_prgenv('env3'))
        assert 1 == self.count_checks(filters.have_prgenv('env4'))
        assert 3 == self.count_checks(filters.have_prgenv('env1|env3'))
