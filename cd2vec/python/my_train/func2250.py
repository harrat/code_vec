def test_that_decorator_calls_funcion(self):
        value_1 = 'uno'
        value_2 = 'dos'

        mocked_func = Mock()

        pre_decorated = guard(TestFileGuardDecorator.TEST_TEXT_FILE_PATH)
        decorated = pre_decorated(mocked_func)
        result = decorated(value_1, value_2)

        mocked_func.assert_called_once_with(value_1, value_2)
