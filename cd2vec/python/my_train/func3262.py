def test_hexagon_jac():
    environment = EnvBox()
    polygon = hexagon(scale=.5)
    f = SignedDistance2DMap(polygon)
    for _ in range(100):
        p = environment.sample_uniform()
        J = f.jacobian(p)
        J_diff = finite_difference_jacobian(f, p)
        assert check_is_close(J, J_diff, 1e-4)
        H = f.hessian(p)
        H_diff = finite_difference_hessian(f, p)
        assert check_is_close(H, H_diff, 1e-4)

