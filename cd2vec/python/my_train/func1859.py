def test__write_logs(init_ExpStock_with_args):
    e = init_ExpStock_with_args.__next__()
    filename = 'params.txt'
    filepath = os.path.join(e.log_dirname, filename)

    e._write_logs(filename, e.params)
    assert os.path.isfile(filepath)
    with open(filepath) as f:
        data = f.read()
    print(data)
    assert data == '''a = This is a test param
b = 12345
c = {'Name': 'Chie Hayashida'}
'''
