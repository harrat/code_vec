def test_page(self):
        pcb = Pcb()
        assert Pcb.parse(pcb.to_string()) == pcb
        pcb.page_type = 'A4'
        assert Pcb.parse(pcb.to_string()) == pcb
        pcb.page_type = [200, 200]
        assert Pcb.parse(pcb.to_string()) == pcb
