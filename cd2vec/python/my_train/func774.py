def test_images():

    client = ApiClient(configuration)
    apis = ExactAPIs(client)

    imageset =  apis.image_sets_api.list_image_sets().results[0].id

    # Delete all images
    product_id=1

    allImagesInSet = apis.images_api.list_images(omit="annotations").results
    for img in allImagesInSet:
        apis.images_api.destroy_image(id=img.id)

    allImagesInSet = apis.images_api.list_images(omit="annotations").results
    assert(len(allImagesInSet)==0) # all images gone

    # generate dummy image
    dummy=np.random.randint(0,255, (200,200,3))
    cv2.imwrite('dummy.png', dummy)

    apis.images_api.create_image(file_path='dummy.png', image_type=0, image_set=imageset).results

    allImagesInSet = apis.images_api.list_images(omit="annotations").results
    # select first image in imageset
    imageid = allImagesInSet[0].id

    assert(apis.images_api.retrieve_image(id=imageid).filename=='dummy.tiff')

    apis.images_api.download_image(id=imageid, target_path='./dummy-retrieved.png',original_image=True)
    retr = cv2.imread('dummy-retrieved.png')

    assert(np.all(retr==dummy))

    apis.images_api.destroy_image(id=imageid)

    allImagesInSet = apis.images_api.list_images(omit="annotations").results


    assert(len(allImagesInSet)==0) # all images gone


    os.remove('dummy.png')
    os.remove('dummy-retrieved.png')

def cleanup():
