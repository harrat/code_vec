def test_num_columns(query):
    """
    Test that the number of columns if correct.
    """

    query.query_params = []
    assert len(query.query_params) == 0

    with pytest.raises(TypeError):
        # test error for non-string parameter
        query.query_params = 1.2

    with pytest.raises(TypeError):
        # test error for non-string parameter in list
        query.query_params = ['F0', 1.3]

    query.query_params = 'F0'
    query.include_errs = False

    # number of columns should be 1
    assert len(query.table.columns) == 1

    # name of column should be 'F0'
    assert query.table.keys()[0] == 'F0'

    query.include_errs = True

    # number of columns should be 2
    assert len(query.table.columns) == 2
    assert 'F0' in query.table.keys() and 'F0_ERR' in query.table.keys()

    query.query_params = ['F0', 'F1']

    # number of columns should be 4
    assert len(query.table.columns) == 4

