def test_stop_in_the_past(cli, entries_file):
    entries = """20/01/2014
alias_1 10:00-? ?
"""

    entries_file.write(entries)
    with freeze_time('2014-01-20 09:30:00'):
        output = cli('stop', ['Play ping-pong'])

    assert output == 'Error: You are trying to stop an activity in the future\n'

