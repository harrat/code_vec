@pytest.mark.vcr()
def test_scan_export_filter_type_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.scans.export(1, filter_type='nothing')
