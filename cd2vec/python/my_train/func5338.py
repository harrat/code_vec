def test_execute_arg_float():
    global cu
    cu.execute("insert into tests(id, name, real_field) values (?, ?, ?)",
               (3, 'test_execute_arg_float', 2500.32))
    cu.execute("select real_field from tests where id = ?", (3,))
    row = cu.fetchone()
    assert row[0] == 2500.32

