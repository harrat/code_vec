def test_add():
    a = other_classes_ds + random_class_ds
    n = a.num_samples
    n1 = other_classes_ds.num_samples
    n2 = random_class_ds.num_samples
    assert n1 + n2 == n

    assert set(a.sample_ids) == set(other_classes_ds.sample_ids+random_class_ds.sample_ids)
    assert a.num_features == other_classes_ds.num_features == random_class_ds.num_features
    assert all(a.feature_names == other_classes_ds.feature_names)

    comb_ds = test_dataset + same_ids_new_feat
    comb_names = np.concatenate([ test_dataset.feature_names,
                            same_ids_new_feat.feature_names])
    if not all(comb_ds.feature_names == comb_names):
        raise ValueError('feature names were not carried forward in combining two '
                         'datasets with same IDs and different feature names!')
