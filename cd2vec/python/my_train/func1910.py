def test_read_state_with_metafile(self):
        path = os.path.join(os.path.dirname(__file__), '.', 'data', 'Test Meta File.txt')

        def avg_with_units(lst):
            num = np.size(lst)
            acc = 0
            for i in lst:
                acc = i + acc

            return acc / num

        ids, answer = read_state_with_metafile(avg_with_units, 1, 28, path, [], ".xls", "mg/L")

        self.assertSequenceEqual(["1", "2"], ids.tolist())
        self.assertSequenceEqual([5.445427082723495, 5.459751965314751]*u.mg/u.L, answer)
