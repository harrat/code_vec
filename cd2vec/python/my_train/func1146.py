def test_indent_around_generator(get_log):

    @logger.indented("hey")
    def gen():
        logger.info("000")
        yield 1
        yield 2

    for i in gen():
        logger.info("%03d", i)
        break

    assert get_log() == "hey\n000\n001\nDONE in no-time (hey)\n"

