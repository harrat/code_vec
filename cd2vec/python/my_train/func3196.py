@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_update_reference_013(snippy, edited_gitlog):
        """Update reference with ``digest`` option.

        Try to update reference with empty string read from editor.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        edited_gitlog.return_value = ''
        cause = snippy.run(['snippy', 'update', '--scat', 'reference', '-d', '5c2071094dbfaa33', '--format', 'text'])
        assert cause == 'NOK: could not identify content category - please keep template tags in place'
        Content.assert_storage(content)
