def test_public_and_secure_channels_are_in_channels_list(self, mocker):
        header_mock = mocker.patch(
            'bitmex_websocket._bitmex_websocket.BitMEXWebsocket.header')
        header_mock.return_value = []

        channels = [
            Channels.connected, SecureChannels.margin]
        instrument = Instrument(should_auth=True, channels=channels)

        assert channels == instrument.channels
