@pytest.mark.remote
def test_event_gps():
    assert datasets.event_gps('GW170817') == 1187008882.4
    with pytest.raises(ValueError) as exc:
        datasets.event_gps('GW123456')
    assert str(exc.value) == 'no event dataset found for \'GW123456\''

