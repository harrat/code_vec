@pytest.mark.parametrize('level', [1, 2, 3, None])
    def test_get_product_order_book(self, client, level):
        r = client.get_product_order_book('BTC-USD', level=level)
        assert type(r) is dict
        assert 'asks' in r
        assert 'bids' in r

        if level in (1, None) and (len(r['asks']) > 1 or len(r['bids']) > 1):
            pytest.fail('Fail: Level 1 should only return the best ask and bid')

        if level is 2 and (len(r['asks']) > 50 or len(r['bids']) > 50):
            pytest.fail('Fail: Level 2 should only return the top 50 asks and bids')

        if level is 3 and (len(r['asks']) < 50 or len(r['bids']) < 50):
            pytest.fail('Fail: Level 3 should return the full order book')
