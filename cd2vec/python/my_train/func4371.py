@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_search_reference_003(snippy, capsys):
        """Search references with ``digest`` option.

        Search reference by explicitly defining short message digest.
        """

        output = (
            '1. How to write commit messages @git [5c2071094dbfaa33]',
            '',
            '   > https://chris.beams.io/posts/git-commit/',
            '   # commit,git,howto',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--scat', 'reference', '--digest', '5c20', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
