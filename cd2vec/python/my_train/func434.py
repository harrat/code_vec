@staticmethod
    @pytest.mark.usefixtures('import-remove', 'update-forced-utc')
    def test_cli_update_snippet_013(snippy, editor_data):
        """Update snippet with editor.

        Update existing snippet from editor so that content fields are given
        from command line. Editor must show the content template with field
        values received from command line parameters.
        """

        content = {
            'data': [
                Content.deepcopy(Snippet.REMOVE)
            ]
        }
        content['data'][0]['brief'] = 'brief cli'
        content['data'][0]['groups'] = ('cli-group',)
        content['data'][0]['tags'] = ('cli-tag',)
        content['data'][0]['links'] = ('https://cli-link',)
        content['data'][0]['digest'] = '613e163028a17645a7dfabbe159f05d14db7588259229dd8d08e949cdc668373'
        template = (
            '# brief cli @cli-group',
            '',
            '> ',
            '',
            '> [1] https://cli-link',
            '',
            '`$ docker rm --volumes $(docker ps --all --quiet)`',
            '',
            '## Meta',
            '',
            '> category  : snippet  ',
            'created   : 2017-10-14T19:56:31.000001+00:00  ',
            'digest    : 613e163028a17645a7dfabbe159f05d14db7588259229dd8d08e949cdc668373  ',
            'filename  :  ',
            'languages :  ',
            'name      :  ',
            'source    :  ',
            'tags      : cli-tag  ',
            'updated   : 2017-10-14T19:56:31.000001+00:00  ',
            'uuid      : 11cd5827-b6ef-4067-b5ac-3ceac07dde9f  ',
            'versions  :  ',
            ''
        )
        editor_data.return_value = '\n'.join(template)
        cause = snippy.run(['snippy', 'update', '-d', '54e41e9b52a02b63', '-t', 'cli-tag', '-b', 'brief cli', '-g', 'cli-group', '-l', 'https://cli-link'])  # pylint: disable=line-too-long
        assert cause == Cause.ALL_OK
        editor_data.assert_called_with('\n'.join(template))
        Content.assert_storage(content)
