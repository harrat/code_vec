def test_metric_helper_fields_exception(self):
        class MyMetric(MetricHelper):
            _name = self.metric_name
            _metadata = self.metric_metadata

        with pytest.raises(Base10Error) as exc:
            metric = MyMetric(**self.metric_values)

        assert '_fields is required' == str(exc.value)
