def test_send_heartbeat_success(self):
        self.ddb_table.update_item = mock.MagicMock('update_item')
        self.lock_client.acquire_lock('key')
        time.sleep(200/1000) # 200 millis
        self.ddb_table.update_item.assert_called()
