@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'export-time')
    def test_cli_export_snippet_018(snippy):
        """Export defined snippet with content data.

        Export defined snippet based on content data. File name is not defined
        in command line -f|--file option. This should result usage of default
        file name and format snippet.text.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Snippet.REMOVE
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--content', 'docker rm --volumes $(docker ps --all --quiet)'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './snippets.mkdn', content)
