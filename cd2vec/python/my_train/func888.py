@project_setup_py_test("hello-pure", ["bdist_wheel"], disable_languages_test=True)
def test_hello_pure_wheel():
    expected_content = [
        'hello/__init__.py'
    ]

    expected_distribution_name = 'hello_pure-1.2.3'

    whls = glob.glob('dist/*.whl')
    assert len(whls) == 1
    check_wheel_content(whls[0], expected_distribution_name, expected_content, pure=True)

