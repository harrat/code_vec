def test_with_tukio_factory(self):
        """
        The tukio task factory must create `TukioTask` when the coroutine is a
        registered Tukio task, else it must create a regular `asyncio.Task`
        object.
        """
        self.loop.set_task_factory(tukio_factory)

        # Create and run a `TukioTask` from a registered task holder
        task = asyncio.ensure_future(self.holder.do_it('yopla'))
        self.assertIsInstance(task, TukioTask)
        res = self.loop.run_until_complete(task)
        self.assertEqual(res, MY_TASK_HOLDER_RES)
        self.assertEqual(task._outputs, MY_TASK_HOLDER_RES)

        # Create and run a `TukioTask` from a registered coroutine
        task = asyncio.ensure_future(my_coro_task(None))
        self.assertIsInstance(task, TukioTask)
        res = self.loop.run_until_complete(task)
        self.assertEqual(res, MY_CORO_TASK_RES)

        # Run a regular coroutine
        task = asyncio.ensure_future(other_coro(None))
        self.assertIsInstance(task, asyncio.Task)
        res = self.loop.run_until_complete(task)
        self.assertEqual(res, None)

        # Run a generator (e.g. as returned by `asyncio.wait`)
        # The task factory is also called in this situation and is passed the
        # generator object.
        t1 = asyncio.ensure_future(my_coro_task(None))
        t2 = asyncio.ensure_future(other_coro(None))
        self.assertIsInstance(t1, TukioTask)
        self.assertIsInstance(t2, asyncio.Task)
        gen = asyncio.wait([t1, t2])
        # A task is created inside `asyncio.run_until_complete` anyway...
        task = asyncio.ensure_future(gen)
        self.assertIsInstance(task, asyncio.Task)
        res = self.loop.run_until_complete(task)
        self.assertEqual(res, ({t1, t2}, set()))

        # Tukio task factory does not affect futures passed to `ensure_future`
        future = asyncio.ensure_future(asyncio.Future())
        self.assertIsInstance(future, asyncio.Future)
