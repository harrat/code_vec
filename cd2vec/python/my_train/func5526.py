@pytest.mark.parametrize("key", ["BUILDING", "Building", "building", "BuIlDiNg"])
def test_del(base_idf, key):
    idf = base_idf
    idf.newidfobject(key, Name="Building")
    assert key in idf.idfobjects
    del idf.idfobjects["building"]
    assert key not in idf.idfobjects

