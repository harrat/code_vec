def test_total_time():
    now = datetime.now()
    later = datetime.now() + timedelta(minutes=15)
    assert total_time(now.strftime("%H:%M:%S"), later.strftime("%H:%M:%S")) == timedelta(minutes=15)

