def test_con_prod_1(sim):
    """Level: tests put/get in 1 Producer/ 1 Consumer scenario"""
    buffer=Level(initialBuffered=0,sim=sim)
    p=Producer(sim=sim)
    sim.activate(p,p.produce(buffer))
    c=Consumer(sim=sim)
    sim.activate(c,c.consume(buffer))
    sim.simulate(until=100)
    assert Producer.produced-Consumer.consumed==buffer.amount,\
        "items produced/consumed/buffered do not tally: %s %s %s"\
            %(Producer.produced,Consumer.consumed,buffer.amount)
def test_con_prod_1(sim):
    """Store: tests put/get in 1 Producer/ 1 Consumer scenario"""
    buffer=Store(initialBuffered=[],sim=sim)
    p=ProducerWidget(sim=sim)
    sim.activate(p,p.produce(buffer))
    c=ConsumerWidget(sim=sim)
    sim.activate(c,c.consume(buffer))
    sim.simulate(until=100)
    assert \
       ProducerWidget.produced-ConsumerWidget.consumed==buffer.nrBuffered,\
       "items produced/consumed/buffered do not tally: %s %s %s"\
       %(ProducerWidget.produced,ConsumerWidget.consumed,buffer.nrBuffered)
