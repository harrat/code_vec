def test_print_toml(self):
        config = TestConfigBase()
        from container_app_conf.formatter.toml import TomlFormatter
        output = config.print(TomlFormatter())
        self.assertIsNotNone(output)
