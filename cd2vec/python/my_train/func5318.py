@pytest.mark.vcr()
def test_agents_list(api):
    count = 0
    agents = api.agents.list()
    for i in agents:
        count += 1
        check(i, 'distro', str)
        check(i, 'id', int)
        check(i, 'ip', str)
        check(i, 'linked_on', int)
        check(i, 'name', str)
        check(i, 'platform', str)
        check(i, 'status', str)
        check(i, 'uuid', 'uuid')
    assert count == agents.total

@pytest.mark.vcr()
def test_agents_details_scanner_id_typeerror(api):
    with pytest.raises(TypeError):
        api.agents.details(scanner_id='nope')
