def test_edit(tmpdir):
	import copy
	data_ = copy.deepcopy(data)

	filename = os.path.join(str(tmpdir), "table.ljson")

	header = ljson.base.generic.Header(header_descriptor)
	table = ljson.base.mem.Table(header, data_)

	fio = open(filename, "w+")

	table.save(fio)
	fio.seek(0)

	table = ljson.base.disk.Table.from_file(fio)
	table[{"lname": "griffin"}]["lname"] = "Griffin"
	
	data_ = []

	for row in data:
		row_ = copy.copy(row)
		row_["lname"] = "Griffin"
		data_.append(row_)

	assert list(table) == data_

	table.additem(item_meg)

	assert list(table) == data_ + [item_meg]
	
	fio = table.file

	fio.seek(0)

	table_in = ljson.base.mem.Table.from_file(fio)
	table._first_next_call = True

	assert list(table) == list(table_in)

	assert table[{"lname": "griffin"}].getone("name") == "meg"
	assert table[{"lname": "griffin"}].getone() == item_meg

	fio.seek(0)
	print(fio.readline()) # get rid of the header
	content = fio.read()
	print(content)
	fio.seek(0)
	fio.truncate(0)
	fio.write(content)
	fio.seek(0)
	table = ljson.base.disk.Table.from_file(fio)
	assert list(table) == list(table_in)

	import pytest

	with pytest.raises(RuntimeError):
		for item in table:
			table[{"name": item["name"]}]["name"] = item["name"].upper()

		for d in data_:
			d["name"] = d["name"].upper()
		assert list(table) == data_
	table.close()

