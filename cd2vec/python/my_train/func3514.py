@mock.patch('thundra.integrations.botocore.BaseIntegration.actual_call')
def test_athena_start_query_execution(mock_actual_call, mock_athena_start_query_exec_response):
    mock_actual_call.return_value = mock_athena_start_query_exec_response
    tracer = ThundraTracer.get_instance()
    tracer.clear()

    database = "test"
    table = "persons"
    query = "SELECT * FROM %s.%s where age = 10;" % (database, table)
    s3_output = 's3://athena-test-bucket/results/'

    try:
        client = boto3.client('athena', region_name='us-west-2')
        client.start_query_execution(
            QueryString=query,
            QueryExecutionContext={
                'Database': database
            },
            ResultConfiguration={
                'OutputLocation': s3_output,
            }
        )
    except:
        pass
    finally:
        span = tracer.get_spans()[0]
        assert span.class_name == 'AWS-Athena'
        assert span.domain_name == 'DB'
        assert span.get_tag(constants.SpanTags['OPERATION_TYPE']) == 'WRITE'
        assert span.get_tag(constants.AwsSDKTags['REQUEST_NAME']) == 'StartQueryExecution'
        assert span.get_tag(constants.SpanTags['DB_INSTANCE']) == database
        assert span.get_tag(constants.AthenaTags['S3_OUTPUT_LOCATION']) == s3_output
        assert span.get_tag(constants.AthenaTags['REQUEST_QUERY_EXECUTION_IDS']) == None
        assert span.get_tag(constants.AthenaTags['RESPONSE_QUERY_EXECUTION_IDS']) == [
            "98765432-1111-1111-1111-12345678910"]
        assert span.get_tag(constants.DBTags['DB_STATEMENT']) == query
        tracer.clear()
