def test_delete_attribute_with_invalid_current_attribute(self):
        """
        Test that an ItemNotFound error is raised when attempting to delete
        an invalid current attribute from a managed object.
        """
        e = engine.KmipEngine()
        e._protocol_version = contents.ProtocolVersion(2, 0)
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        current_attribute = objects.CurrentAttribute()
        current_attribute._attribute = primitives.TextString(
            value="Object Group 1",
            tag=enums.Tags.CURRENT_ATTRIBUTE
        )
        args = (
            payloads.DeleteAttributeRequestPayload(
                unique_identifier="1",
                current_attribute=current_attribute
            ),
        )

        self.assertRaisesRegex(
            exceptions.ItemNotFound,
            "No attribute with the specified name exists.",
            e._process_delete_attribute,
            *args
        )
