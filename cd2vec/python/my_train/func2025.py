def test_database_unavailable(self):
        # create the database
        with open(self.base_config["sqlite_config"]["database"], 'a'):
            pass
        # make it unavailable
        os.chmod(self.base_config["sqlite_config"]["database"], 0)
        # lauch policyd-rate-limit with the database navailable
        with test_utils.lauch(self.base_config) as cfg:
            # as long as the database is unavailable, all response should be dunno
            for i in range(20):
                data = test_utils.send_policyd_request(cfg["SOCKET"], sasl_username="test")
                self.assertEqual(data.strip(), b"action=dunno")
            # make the database available, it should be initialized upon the next request
            os.chmod(self.base_config["sqlite_config"]["database"], 0o644)
            # these requests should be counted
            for i in range(10):
                data = test_utils.send_policyd_request(cfg["SOCKET"], sasl_username="test")
                self.assertEqual(data.strip(), b"action=dunno")
            # the eleventh counted requests should fail
            data = test_utils.send_policyd_request(cfg["SOCKET"], sasl_username="test")
            self.assertEqual(data.strip(), b"action=defer_if_permit Rate limit reach, retry later")
