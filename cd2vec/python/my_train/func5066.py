@pytest.mark.parametrize("attribute, value_type, new_value_in, new_value_out", [
    ("ATTRIBUTE_STR", str, 'new value', 'new value'),
    ("ATTRIBUTE_INT", int, '12345', 12345),
    ("ATTRIBUTE_FLOAT", float, '3.14', 3.14),
    ("ATTRIBUTE_BOOL", bool, 'yes', True),
])
def test_env_override(config, mock_env, attribute, value_type, new_value_in, new_value_out):
    mock_env({attribute: new_value_in})
    assert getattr(config, attribute) == new_value_out
    assert type(getattr(config, attribute)) == value_type

