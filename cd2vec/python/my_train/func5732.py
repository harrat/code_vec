@mock.patch('thundra.integrations.requests.RequestsIntegration.actual_call')
def test_apigw_call_v2(mock_actual_call):
    mock_actual_call.return_value = requests.Response()
    mock_actual_call.return_value.headers = {"apigw-requestid": "test_id", "x-thundra-resource-name": "test"}
    try:
        url = 'https://1a23bcdefg.execute-api.us-west-2.amazonaws.com/dev/test'
        parsed_url = urlparse(url)
        path = parsed_url.path
        query = parsed_url.query
        host = parsed_url.netloc
