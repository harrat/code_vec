@pytest.mark.parametrize("code", ["ABP-231", "ABP-123", "SSNI-351"])
def test_search_by_code(code):
    mock_message = MockMessage(mock_user, mock_chat.id, "/search")
    mock_update = MockUpdate(mock_message)
    search(mock_bot, mock_update, [code])
    received = mock_user.look_received()
    assert requests.get(received[0]["photo"], proxies=proxy).status_code == 200
    assert (
        requests.get(
            received[0]["reply_markup"]["inline_keyboard"][0][0]["url"],
            proxies=proxy
        ).status_code
        == 200
    )
