def test_pass_in_retries(make_runner, make_cases, tmp_path, common_exec_ctx):
    tmpfile = tmp_path / 'out.txt'
    tmpfile.write_text('0\n')
    runner = make_runner(max_retries=3)
    pass_run_no = 2
    runner.runall(make_cases([RetriesCheck(pass_run_no, tmpfile)]))

    # Ensure that the test passed after retries in run `pass_run_no`
    assert 1 == runner.stats.num_cases()
    assert_runall(runner)
    assert 1 == len(runner.stats.failures(run=0))
    assert pass_run_no == rt.runtime().current_run
    assert 0 == len(runner.stats.failures())

