@mark.django_db
def test_delete(client_with_perms):
    client = client_with_perms("testapp.delete_dragonfly")
    alpha = Dragonfly.objects.create(name="alpha", age=47)
    delete_url = DragonflyViewSet().links["delete"].reverse(alpha)
    response = client.get(delete_url)
    assert b"Are you sure you want to delete" in response.content
    assert b"alpha" in response.content

    response = client.post(delete_url)

    assert response.status_code == 302
    assert response["location"] == DragonflyViewSet().links["list"].reverse()

    assert not Dragonfly.objects.filter(name="alpha").exists()

