def test_clear_param_overrides(self):
        self.client.set_param_overrides({'foo': 'baz'})
        self.client.clear_param_overrides()
        self._make_request('https://www.stackoverflow.com')

        last_request = self.client.get_last_request()

        query = urlsplit(last_request['url']).query
        self.assertEqual('', query)
