@mock.patch('socket.socket')
    def test_new_socket_connection_ipv4(self, mock_socket: mock.Mock) -> None:
        conn = new_socket_connection(self.addr_ipv4)
        mock_socket.assert_called_with(socket.AF_INET, socket.SOCK_STREAM, 0)
        self.assertEqual(conn, mock_socket.return_value)
        mock_socket.return_value.connect.assert_called_with(self.addr_ipv4)
