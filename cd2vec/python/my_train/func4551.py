@freeze_time('2018-04-01')
def test_variables(init_ExpStock):
    e = init_ExpStock.__next__()
    assert e.stock_root_dir == 'experiments'
    assert e.params == []
    assert e.memo == ''
    assert e.git_check == True
    print(datetime.now())
    print(e.log_dirname)
    assert e.log_dirname == os.path.abspath('experiments/20180401_000000_Untitled')

def test_variables_with_args(init_ExpStock_with_args):
    e = init_ExpStock_with_args.__next__()
    #patched_time = datetime.now()
    #mocker.patch.object(e._set_dirname, autospec='experiments/20180401')
    assert e.stock_root_dir == 'experiments'
    assert e.params == [('a', 'This is a test param'),
            ('b', 12345),
            ('c', {'Name': 'Chie Hayashida'})
            ]
    assert e.memo == 'This is a test memo'
    assert e.git_check == False
    print(datetime.now())
    print(e.log_dirname)
    assert e.log_dirname == os.path.abspath('test_logs/test')
