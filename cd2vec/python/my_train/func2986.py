def test_failed_open():
    with pytest.raises(py2jdbc.OperationalError):
        py2jdbc.connect('jdbc:sqlite:/foo/bar/bla/23534/mydb.db')
