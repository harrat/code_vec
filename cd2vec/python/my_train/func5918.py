def test_create_delete_dnssec(self):
        # cleanup existing data
        try:
            api.delete_dnssec(sample1.digest)
        except exceptions.NamecomError:
            pass

        result = api.create_dnssec(
            keyTag=sample1.keyTag,
            algorithm=sample1.algorithm,
            digestType=sample1.digestType,
            digest=sample1.digest
        )

        dnssec = result.dnssec
        self.assertEqual(dnssec, sample1)

        result = api.delete_dnssec(sample1.digest)
        self.assertEqual(result.status_code, 200)
