@pytest.mark.parametrize(
    "cmake_install_dir, expected_failed, error_code_type", (
        (None, True, str),
        ('', True, str),
        (str(py.path.local.get_temproot().join('scikit-build')), True, SKBuildError),
        ('banana', False, str)
    )
)
def test_cmake_install_dir_keyword(
        cmake_install_dir, expected_failed, error_code_type, capsys):

    # -------------------------------------------------------------------------
    # "SOURCE" tree layout:
    #
    # ROOT/
    #
    #     CMakeLists.txt
    #     setup.py
    #
    #     apple/
    #         __init__.py
    #
    # -------------------------------------------------------------------------
    # "BINARY" distribution layout
    #
    # ROOT/
    #
    #     apple/
    #         __init__.py
    #

    tmp_dir = _tmpdir('cmake_install_dir_keyword')

    setup_kwarg = ''
    if cmake_install_dir is not None:
        setup_kwarg = 'cmake_install_dir=r\'{}\''.format(cmake_install_dir)

    tmp_dir.join('setup.py').write(textwrap.dedent(
        """
        from skbuild import setup
        setup(
            name="test_cmake_install_dir",
            version="1.2.3",
            description="a package testing use of cmake_install_dir",
            author='The scikit-build team',
            license="MIT",
            packages=['apple', 'banana'],
            {setup_kwarg}
        )
        """.format(setup_kwarg=setup_kwarg)
    ))

    # Install location purposely set to "." so that we can test
    # usage of "cmake_install_dir" skbuild.setup keyword.
    tmp_dir.join('CMakeLists.txt').write(textwrap.dedent(
        """
        cmake_minimum_required(VERSION 3.5.0)
        project(banana NONE)
        file(WRITE "${CMAKE_BINARY_DIR}/__init__.py" "")
        install(FILES "${CMAKE_BINARY_DIR}/__init__.py" DESTINATION ".")
        """
    ))

    tmp_dir.ensure('apple', '__init__.py')

    failed = False
    message = ""
    try:
        with execute_setup_py(tmp_dir, ['build'], disable_languages_test=True):
            pass
    except SystemExit as e:
        # Error is not of type SKBuildError, it is expected to be
        # raised by distutils.core.setup
        failed = isinstance(e.code, error_code_type)
        message = str(e)

    out, _ = capsys.readouterr()

    assert failed == expected_failed
    if failed:
        if error_code_type == str:
            assert message == "error: package directory " \
                              "'{}' does not exist".format(
                                    os.path.join(CMAKE_INSTALL_DIR(), 'banana'))
        else:
            assert message.strip().startswith(
                "setup parameter 'cmake_install_dir' "
                "is set to an absolute path.")
    else:
        init_py = to_platform_path("{}/banana/__init__.py".format(CMAKE_INSTALL_DIR()))
        assert "copying {}".format(init_py) in out
