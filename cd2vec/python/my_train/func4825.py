def test_local_alias(cli, alias_config):
    alias_config.set('local_aliases', '__pingpong', '')
    output = cli('alias', ['list'])
    assert '[local] __pingpong -> not mapped' in output

