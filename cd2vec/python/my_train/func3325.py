@pytest.mark.high
    def test_register_consumer(self):
        self.register.register(self.tasks['consumer'])

        self.assertIn(self.tasks['consumer'], self.register.consumers.values())
