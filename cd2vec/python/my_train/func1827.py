def test_align():
    """
    Text lexical mapping
    """
    factory = OntologyFactory()
    print("Creating ont")
    ont1 = factory.create('ma')
    ont2 = factory.create('zfa')
    lexmap = LexicalMapEngine()

    lexmap.index_ontology(ont1)
    lexmap.index_ontology(ont2)

    print(lexmap.lmap)
    print(ont1.all_synonyms())
    print(ont2.all_synonyms())
    g = lexmap.get_xref_graph()
    for (x,y,d) in g.edges(data=True):
        print("{}<->{} :: {}".format(x,y,d))
    for x in g.nodes():
        print("{} --> {}".format(x,lexmap.grouped_mappings(x)))

