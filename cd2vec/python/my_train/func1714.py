def test_delete(self):
        self.create_default_experiment()

        cluster = Cluster.new('tmux', server_name=_TEST_SERVER)
        with self.assertRaises(ValueError):
            # cluster.delete(None)
            # cluster.delete('')
            cluster.delete('Irene')
        self.assertListEqual(cluster.list_experiments(), ['exp'])
        cluster.delete('exp')
        self.assertListEqual(cluster.list_experiments(), [])
