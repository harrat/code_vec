def test_fake_update_not_use_cache_server():
    ua = UserAgent(cache=False, use_cache_server=False)

    denied_urls = [
        'https://www.w3schools.com/browsers/browsers_stats.asp',
        'http://useragentstring.com/pages/useragentstring.php',
    ]

    with mock.patch(
        'fake_useragent.utils.Request',
        side_effect=partial(_request, denied_urls=denied_urls),
    ):
        with pytest.raises(FakeUserAgentError):
            ua.update()
