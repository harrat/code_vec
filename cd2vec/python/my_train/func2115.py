def test_create_access_token_idempotent(self):
        member1 = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        address = member1.add_address(generate_nonce(), utils.generate_address())
        payload = AccessTokenBuilder.create_with_alias(member1.get_first_alias()).for_address(address.id).build()
        member1.endorse_token(member1.create_access_token(payload), Key.STANDARD)
        member1.endorse_token(member1.create_access_token(payload), Key.STANDARD)
        time.sleep(self.TOKEN_LOOKUP_POLL_FREQUENCY * 2)
        result = member1.get_access_tokens(2, None)
        assert len(result.tokens) == 1
