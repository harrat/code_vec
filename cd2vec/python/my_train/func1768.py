def test_bot_ai():
    bot: BotAI = random_bot_object
    # Test initial bot attributes at game start

    # Properties from _prepare_start
    assert 1 <= bot.player_id <= 2
    assert isinstance(bot.race, Race)
    assert isinstance(bot.enemy_race, Race)

    # Properties from _prepare_step
    assert bot.units.amount == bot.workers.amount
    assert bot.structures.amount == bot.townhalls.amount
    assert bot.workers.amount == 12
    assert bot.townhalls.amount == 1
    assert bot.gas_buildings.amount == 0
    assert bot.minerals == 50
    assert bot.vespene == 0
    assert bot.supply_army == 0
    assert bot.supply_workers == 12
    assert bot.supply_cap == 15
    assert bot.supply_used == 12
    assert bot.supply_left == 3
    assert bot.idle_worker_count == 0
    assert bot.army_count == 0

    # Test properties updated by "_prepare_units" function
    assert not bot.blips
    assert bot.units
    assert bot.structures
    assert not bot.enemy_units
    assert not bot.enemy_structures
    assert bot.mineral_field
    assert bot.vespene_geyser
    assert bot.resources
    assert len(bot.destructables) >= 0
    assert isinstance(bot.destructables, (list, set, dict))
    assert len(bot.watchtowers) >= 0
    assert bot.all_units
    assert bot.workers
    assert bot.townhalls
    assert not bot.gas_buildings

    # Test bot_ai functions
    assert bot.time == 0
    assert bot.time_formatted in {"0:00", "00:00"}
    assert bot.start_location is None  # Is populated by main.py
    bot._game_info.player_start_location = bot.townhalls.random.position
    assert bot.townhalls.random.position not in bot.enemy_start_locations
    assert bot.enemy_units == Units([], bot)
    assert bot.enemy_structures == Units([], bot)
    bot._game_info.map_ramps, bot._game_info.vision_blockers = bot._game_info._find_ramps_and_vision_blockers()
    assert bot.main_base_ramp  # Test if any ramp was found

    # The following functions need to be tested by autotest_bot.py because they use API query which isn't available here as this file only uses the pickle files and is not able to interact with the API as SC2 is not running while this test runs
    assert bot.can_feed(UnitTypeId.MARINE)
    assert bot.can_feed(UnitTypeId.SIEGETANK)
    assert not bot.can_feed(UnitTypeId.THOR)
    assert not bot.can_feed(UnitTypeId.BATTLECRUISER)
    assert not bot.can_feed(UnitTypeId.IMMORTAL)
    assert bot.can_afford(UnitTypeId.ZERGLING)
    assert bot.can_afford(UnitTypeId.MARINE)
    assert bot.can_afford(UnitTypeId.SCV)
    assert bot.can_afford(UnitTypeId.DRONE)
    assert bot.can_afford(UnitTypeId.PROBE)
    assert bot.can_afford(AbilityId.COMMANDCENTERTRAIN_SCV)
    assert bot.can_afford(UnitTypeId.MARINE)
    assert not bot.can_afford(UnitTypeId.SIEGETANK)
    assert not bot.can_afford(UnitTypeId.BATTLECRUISER)
    assert not bot.can_afford(UnitTypeId.MARAUDER)
    assert not bot.can_afford(UpgradeId.WARPGATERESEARCH)
    assert not bot.can_afford(AbilityId.RESEARCH_WARPGATE)

    # Store old values for minerals, vespene
    old_values = bot.minerals, bot.vespene, bot.supply_cap, bot.supply_left, bot.supply_used
    bot.vespene = 50
    assert bot.can_afford(UpgradeId.WARPGATERESEARCH)
    assert bot.can_afford(AbilityId.RESEARCH_WARPGATE)
    bot.minerals = 150
    bot.supply_cap = 15
    bot.supply_left = -1
    bot.supply_used = 16
    # Confirm that units that don't cost supply can be built while at negative supply using can_afford function
    assert bot.can_afford(UnitTypeId.GATEWAY)
    assert bot.can_afford(UnitTypeId.PYLON)
    assert bot.can_afford(UnitTypeId.OVERLORD)
    assert bot.can_afford(UnitTypeId.BANELING)
    assert not bot.can_afford(UnitTypeId.ZERGLING)
    assert not bot.can_afford(UnitTypeId.MARINE)
    bot.minerals, bot.vespene, bot.supply_cap, bot.supply_left, bot.supply_used = old_values

    worker = bot.workers.random
    assert bot.select_build_worker(worker.position) == worker
    for w in bot.workers:
        if w == worker:
            continue
        assert bot.select_build_worker(w.position) != worker
    assert bot.already_pending_upgrade(UpgradeId.STIMPACK) == 0
    assert bot.already_pending(UpgradeId.STIMPACK) == 0
    assert bot.already_pending(UnitTypeId.SCV) == 0
    assert 0 < bot.get_terrain_height(worker)
    assert bot.in_placement_grid(worker)
    assert bot.in_pathing_grid(worker)
    # The pickle data was created by a terran bot, so there is no creep under any worker
    assert not bot.has_creep(worker)
    # Why did this stop working, not visible on first frame?
    assert bot.is_visible(worker), f"Visibility value at worker is {bot.state.visibility[worker.position.rounded]}"

    # Check price for morphing units and upgrades
    cost_100 = [
        AbilityId.ARMORYRESEARCH_TERRANSHIPWEAPONSLEVEL1,
        UpgradeId.TERRANSHIPWEAPONSLEVEL1,
        AbilityId.ARMORYRESEARCH_TERRANVEHICLEPLATINGLEVEL1,
        UpgradeId.TERRANVEHICLEARMORSLEVEL1,
        AbilityId.ARMORYRESEARCH_TERRANVEHICLEWEAPONSLEVEL1,
        UpgradeId.TERRANVEHICLEWEAPONSLEVEL1,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYARMORLEVEL1,
        UpgradeId.TERRANINFANTRYARMORSLEVEL1,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL1,
        UpgradeId.TERRANINFANTRYWEAPONSLEVEL1,
        AbilityId.RESEARCH_ZERGMELEEWEAPONSLEVEL1,
        UpgradeId.ZERGMELEEWEAPONSLEVEL1,
        AbilityId.RESEARCH_ZERGMISSILEWEAPONSLEVEL1,
        UpgradeId.ZERGMISSILEWEAPONSLEVEL1,
        AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL1,
        UpgradeId.PROTOSSGROUNDARMORSLEVEL1,
        AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL1,
        UpgradeId.PROTOSSGROUNDWEAPONSLEVEL1,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL1,
        UpgradeId.TERRANINFANTRYWEAPONSLEVEL1,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL1,
        UpgradeId.TERRANINFANTRYWEAPONSLEVEL1,
        AbilityId.RESEARCH_ZERGFLYERATTACKLEVEL1,
        UpgradeId.ZERGFLYERWEAPONSLEVEL1,
        AbilityId.CYBERNETICSCORERESEARCH_PROTOSSAIRWEAPONSLEVEL1,
        UpgradeId.PROTOSSAIRWEAPONSLEVEL1,
    ]
    cost_175 = [
        AbilityId.ARMORYRESEARCH_TERRANSHIPWEAPONSLEVEL2,
        UpgradeId.TERRANSHIPWEAPONSLEVEL2,
        AbilityId.ARMORYRESEARCH_TERRANVEHICLEPLATINGLEVEL2,
        UpgradeId.TERRANVEHICLEARMORSLEVEL2,
        AbilityId.ARMORYRESEARCH_TERRANVEHICLEWEAPONSLEVEL2,
        UpgradeId.TERRANVEHICLEWEAPONSLEVEL2,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYARMORLEVEL2,
        UpgradeId.TERRANINFANTRYARMORSLEVEL2,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL2,
        UpgradeId.TERRANINFANTRYWEAPONSLEVEL2,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL2,
        UpgradeId.TERRANINFANTRYWEAPONSLEVEL2,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL2,
        UpgradeId.TERRANINFANTRYWEAPONSLEVEL2,
        AbilityId.RESEARCH_ZERGFLYERATTACKLEVEL2,
        UpgradeId.ZERGFLYERWEAPONSLEVEL2,
        AbilityId.CYBERNETICSCORERESEARCH_PROTOSSAIRWEAPONSLEVEL2,
        UpgradeId.PROTOSSAIRWEAPONSLEVEL2,
    ]
    cost_200 = [
        AbilityId.RESEARCH_ZERGMELEEWEAPONSLEVEL3,
        UpgradeId.ZERGMELEEWEAPONSLEVEL3,
        AbilityId.RESEARCH_ZERGMISSILEWEAPONSLEVEL3,
        UpgradeId.ZERGMISSILEWEAPONSLEVEL3,
        AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL3,
        UpgradeId.PROTOSSGROUNDARMORSLEVEL3,
        AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL3,
        UpgradeId.PROTOSSGROUNDWEAPONSLEVEL3,
    ]
    cost_250 = [
        AbilityId.ARMORYRESEARCH_TERRANSHIPWEAPONSLEVEL3,
        UpgradeId.TERRANSHIPWEAPONSLEVEL3,
        AbilityId.ARMORYRESEARCH_TERRANVEHICLEPLATINGLEVEL3,
        UpgradeId.TERRANVEHICLEARMORSLEVEL3,
        AbilityId.ARMORYRESEARCH_TERRANVEHICLEWEAPONSLEVEL3,
        UpgradeId.TERRANVEHICLEWEAPONSLEVEL3,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYARMORLEVEL3,
        UpgradeId.TERRANINFANTRYARMORSLEVEL3,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL3,
        UpgradeId.TERRANINFANTRYWEAPONSLEVEL3,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL3,
        UpgradeId.TERRANINFANTRYWEAPONSLEVEL3,
        AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL3,
        UpgradeId.TERRANINFANTRYWEAPONSLEVEL3,
        AbilityId.RESEARCH_ZERGFLYERATTACKLEVEL3,
        UpgradeId.ZERGFLYERWEAPONSLEVEL3,
        AbilityId.CYBERNETICSCORERESEARCH_PROTOSSAIRWEAPONSLEVEL3,
        UpgradeId.PROTOSSAIRWEAPONSLEVEL3,
    ]

    cost_150 = [
        AbilityId.RESEARCH_ZERGGROUNDARMORLEVEL1,
        UpgradeId.ZERGGROUNDARMORSLEVEL1,
        AbilityId.FORGERESEARCH_PROTOSSSHIELDSLEVEL1,
        UpgradeId.PROTOSSSHIELDSLEVEL1,
        AbilityId.RESEARCH_ZERGFLYERARMORLEVEL1,
        UpgradeId.ZERGFLYERARMORSLEVEL1,
        AbilityId.FORGERESEARCH_PROTOSSSHIELDSLEVEL1,
        UpgradeId.PROTOSSSHIELDSLEVEL1,
        AbilityId.RESEARCH_ZERGMELEEWEAPONSLEVEL2,
        UpgradeId.ZERGMELEEWEAPONSLEVEL2,
        AbilityId.RESEARCH_ZERGMISSILEWEAPONSLEVEL2,
        UpgradeId.ZERGMISSILEWEAPONSLEVEL2,
        AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL2,
        UpgradeId.PROTOSSGROUNDARMORSLEVEL2,
        AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL2,
        UpgradeId.PROTOSSGROUNDWEAPONSLEVEL2,
    ]
    cost_225 = [
        AbilityId.FORGERESEARCH_PROTOSSSHIELDSLEVEL2,
        UpgradeId.PROTOSSSHIELDSLEVEL2,
        AbilityId.FORGERESEARCH_PROTOSSSHIELDSLEVEL2,
        UpgradeId.PROTOSSSHIELDSLEVEL2,
        AbilityId.RESEARCH_ZERGGROUNDARMORLEVEL2,
        UpgradeId.ZERGGROUNDARMORSLEVEL2,
        AbilityId.RESEARCH_ZERGFLYERARMORLEVEL2,
        UpgradeId.ZERGFLYERARMORSLEVEL2,
    ]
    cost_300 = [
        AbilityId.RESEARCH_ZERGGROUNDARMORLEVEL3,
        UpgradeId.ZERGGROUNDARMORSLEVEL3,
        AbilityId.FORGERESEARCH_PROTOSSSHIELDSLEVEL3,
        UpgradeId.PROTOSSSHIELDSLEVEL3,
        AbilityId.RESEARCH_ZERGFLYERARMORLEVEL3,
        UpgradeId.ZERGFLYERARMORSLEVEL3,
        AbilityId.FORGERESEARCH_PROTOSSSHIELDSLEVEL3,
        UpgradeId.PROTOSSSHIELDSLEVEL3,
    ]
    cost_list = [100, 175, 200, 250, 150, 225, 300]

    def calc_cost(item_id) -> Cost:
        if isinstance(item_id, AbilityId):
            return bot.game_data.calculate_ability_cost(item_id)
        elif isinstance(item_id, UpgradeId):
            return bot.game_data.upgrades[item_id.value].cost
        elif isinstance(item_id, UnitTypeId):
            creation_ability: AbilityId = bot.game_data.units[item_id.value].creation_ability
            return bot.game_data.calculate_ability_cost(creation_ability)

    def assert_cost(item_id, real_cost: Cost):
        assert calc_cost(item_id) == real_cost, f"Cost of {item_id} should be {real_cost} but is {calc_cost(item_id)}"

    for items, cost in zip([cost_100, cost_175, cost_200, cost_250, cost_150, cost_225, cost_300], cost_list):
        real_cost2: Cost = Cost(cost, cost)
        for item in items:
            assert_cost(item, real_cost2)
            assert (
                bot.calculate_cost(item) == real_cost2
            ), f"Cost of {item} should be {real_cost2} but is {calc_cost(item)}"

    # Do not use the generic research abilities in the bot when testing if you can afford it as these are wrong
    assert_cost(AbilityId.RESEARCH_ZERGFLYERARMOR, Cost(0, 0))
    assert_cost(AbilityId.RESEARCH_ZERGFLYERATTACK, Cost(0, 0))
    assert_cost(AbilityId.RESEARCH_ZERGGROUNDARMOR, Cost(0, 0))
    assert_cost(AbilityId.RESEARCH_ZERGMELEEWEAPONS, Cost(0, 0))
    assert_cost(AbilityId.RESEARCH_ZERGMISSILEWEAPONS, Cost(0, 0))
    assert_cost(AbilityId.RESEARCH_TERRANINFANTRYARMOR, Cost(0, 0))
    assert_cost(AbilityId.RESEARCH_TERRANINFANTRYWEAPONS, Cost(0, 0))
    assert_cost(AbilityId.RESEARCH_PROTOSSGROUNDARMOR, Cost(0, 0))
    assert_cost(AbilityId.RESEARCH_PROTOSSGROUNDWEAPONS, Cost(0, 0))
    assert_cost(AbilityId.RESEARCH_PROTOSSSHIELDS, Cost(0, 0))
    assert_cost(AbilityId.RESEARCH_TERRANSHIPWEAPONS, Cost(0, 0))
    assert_cost(AbilityId.RESEARCH_TERRANVEHICLEWEAPONS, Cost(0, 0))

    # Somehow this is 0, returned by the API
    assert_cost(AbilityId.BUILD_REACTOR, Cost(0, 0))
    # UnitTypeId.REACTOR has no creation ability (None)
    # assert_cost(UnitTypeId.REACTOR, Cost(50, 50))

    assert_cost(AbilityId.BUILD_REACTOR_BARRACKS, Cost(50, 50))
    assert_cost(UnitTypeId.BARRACKSREACTOR, Cost(50, 50))
    assert_cost(AbilityId.UPGRADETOORBITAL_ORBITALCOMMAND, Cost(150, 0))
    assert_cost(UnitTypeId.ORBITALCOMMAND, Cost(150, 0))
    assert_cost(AbilityId.UPGRADETOORBITAL_ORBITALCOMMAND, Cost(150, 0))

    assert bot.calculate_unit_value(UnitTypeId.ORBITALCOMMAND) == Cost(550, 0)
    assert bot.calculate_unit_value(UnitTypeId.RAVAGER) == Cost(100, 100)
    assert bot.calculate_unit_value(UnitTypeId.ARCHON) == Cost(175, 275)
    assert bot.calculate_unit_value(UnitTypeId.ADEPTPHASESHIFT) == Cost(0, 0)
    assert bot.calculate_unit_value(UnitTypeId.AUTOTURRET) == Cost(100, 0)
    assert bot.calculate_unit_value(UnitTypeId.INFESTORTERRAN) == Cost(0, 0)
    assert bot.calculate_unit_value(UnitTypeId.INFESTORTERRANBURROWED) == Cost(0, 0)
    assert bot.calculate_unit_value(UnitTypeId.LARVA) == Cost(0, 0)
    assert bot.calculate_unit_value(UnitTypeId.EGG) == Cost(0, 0)
    assert bot.calculate_unit_value(UnitTypeId.LOCUSTMP) == Cost(0, 0)
    assert bot.calculate_unit_value(UnitTypeId.LOCUSTMPFLYING) == Cost(0, 0)
    assert bot.calculate_unit_value(UnitTypeId.BROODLING) == Cost(0, 0)
    # Other and effects
    assert bot.calculate_unit_value(UnitTypeId.KD8CHARGE) == Cost(0, 0)
    assert bot.calculate_unit_value(UnitTypeId.RAVAGERCORROSIVEBILEMISSILE) == Cost(0, 0)
    assert bot.calculate_unit_value(UnitTypeId.VIPERACGLUESCREENDUMMY) == Cost(0, 0)

    assert bot.calculate_cost(UnitTypeId.BROODLORD) == Cost(150, 150)
    assert bot.calculate_cost(UnitTypeId.RAVAGER) == Cost(25, 75)
    assert bot.calculate_cost(UnitTypeId.BANELING) == Cost(25, 25)
    assert bot.calculate_cost(UnitTypeId.ORBITALCOMMAND) == Cost(150, 0)
    assert bot.calculate_cost(AbilityId.UPGRADETOORBITAL_ORBITALCOMMAND) == Cost(150, 0)
    assert bot.calculate_cost(UnitTypeId.REACTOR) == Cost(50, 50)
    assert bot.calculate_cost(UnitTypeId.TECHLAB) == Cost(50, 25)
    assert bot.calculate_cost(UnitTypeId.QUEEN) == Cost(150, 0)
    assert bot.calculate_cost(UnitTypeId.HATCHERY) == Cost(300, 0)
    assert bot.calculate_cost(UnitTypeId.LAIR) == Cost(150, 100)
    assert bot.calculate_cost(UnitTypeId.HIVE) == Cost(200, 150)
    assert bot.calculate_cost(UnitTypeId.DRONE) == Cost(50, 0)
    assert bot.calculate_cost(UnitTypeId.SCV) == Cost(50, 0)
    assert bot.calculate_cost(UnitTypeId.PROBE) == Cost(50, 0)
    assert bot.calculate_cost(UnitTypeId.SPIRE) == Cost(200, 200)
    assert bot.calculate_cost(UnitTypeId.ARCHON) == bot.calculate_unit_value(UnitTypeId.ARCHON)

    # The following are morph abilities that may need a fix
    assert_cost(AbilityId.MORPHTOBROODLORD_BROODLORD, Cost(300, 250))
    assert_cost(UnitTypeId.BROODLORD, Cost(300, 250))
    assert_cost(AbilityId.MORPHTORAVAGER_RAVAGER, Cost(100, 100))
    assert_cost(AbilityId.MORPHTOBROODLORD_BROODLORD, Cost(300, 250))
    assert_cost(AbilityId.MORPHZERGLINGTOBANELING_BANELING, Cost(50, 25))

    assert Cost(100, 50) == 2 * Cost(50, 25)
    assert Cost(100, 50) == Cost(50, 25) * 2

    assert bot.calculate_supply_cost(UnitTypeId.BARRACKS) == 0
    assert bot.calculate_supply_cost(UnitTypeId.HATCHERY) == 0
    assert bot.calculate_supply_cost(UnitTypeId.OVERLORD) == 0
    assert bot.calculate_supply_cost(UnitTypeId.ZERGLING) == 1
    assert bot.calculate_supply_cost(UnitTypeId.MARINE) == 1
    assert bot.calculate_supply_cost(UnitTypeId.BANELING) == 0
    assert bot.calculate_supply_cost(UnitTypeId.QUEEN) == 2
    assert bot.calculate_supply_cost(UnitTypeId.ROACH) == 2
    assert bot.calculate_supply_cost(UnitTypeId.RAVAGER) == 1
    assert bot.calculate_supply_cost(UnitTypeId.CORRUPTOR) == 2
    assert bot.calculate_supply_cost(UnitTypeId.BROODLORD) == 2
    assert bot.calculate_supply_cost(UnitTypeId.HYDRALISK) == 2
    assert bot.calculate_supply_cost(UnitTypeId.LURKERMP) == 1

