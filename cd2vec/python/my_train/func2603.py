def test_read_compressed_file(compressed_file):
    """Test that wkr.open can read compressed file formats."""
    with wkr.open(compressed_file, 'rb') as input_file:
        data = input_file.read()
        assert isinstance(data, binary_type)
        assert data == BINARY_DATA
