def test_givenProviderFunctionWithReturnType_whenInitializingServiceLocator_thenFunctionIsBoundByReturnType(self):
        binder = ProviderBinder("", Registry())

        binder.bind(self.serviceLocator)

        self.assertEqual((SomeClass, provider_function), self.result[0])
