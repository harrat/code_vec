def test_read_lines(self):
        self.assertListEqual(list(read_lines(Path('foobar'), errors=False)), [])

        path = self.root.make_file()
        with open(path, 'wt') as o:
            o.write("1\n2\n3")
        self.assertListEqual(
            list(read_lines(path)),
            ['1', '2', '3'])
        self.assertListEqual(
            list(read_lines(path, convert=int)),
            [1, 2, 3])
