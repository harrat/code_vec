def test_interactive():
    def interactive_assert(message, condition):
        interactive_mock_message = MockMessage(mock_user, mock_chat.id, message)
        interactive_mock_update = MockUpdate(interactive_mock_message)
        Interactive.message(mock_bot, interactive_mock_update)
        interactive_received = mock_user.look_received()
        assert condition(interactive_received)

    # test interactive start
    mock_message = MockMessage(mock_user, mock_chat.id, "/start")
    mock_update = MockUpdate(mock_message)
    start(mock_bot, mock_update)
    received = mock_user.look_received()
    assert len(received) == 1
    assert received[0]["text"] == "Hi, how can I help you?"

    # test interactive search by code
    interactive_assert(
        "Search",
        lambda rcv: len(rcv) == 1 and "Search a code or an actress." in rcv[0]["text"],
    )
    interactive_assert(
        "ABP-123",
        lambda rcv: len(rcv) == 1 and requests.get(rcv[0]["photo"], proxies=proxy).status_code == 200,
    )

    # test interactive search by actress
    interactive_assert(
        "Search",
        lambda rcv: len(rcv) == 1 and "Search a code or an actress." in rcv[0]["text"],
    )
    interactive_assert(
        "?????",
        lambda rcv: len(rcv) == 1 and rcv[0]["text"] == "How many results? [Integer]",
    )
    interactive_assert(
        "5",
        lambda rcv: len(rcv) > 0
        and sum(map(lambda y: requests.get(y["photo"], proxies=proxy).status_code == 200, rcv)) > 0,
    )

    # test interactive search newly released
    interactive_assert(
        "New",
        lambda rcv: len(rcv) == 1 and "How many results? [Integer]" == rcv[0]["text"],
    )
    interactive_assert(
        "5",
        lambda rcv: len(rcv) > 0
        and sum(map(lambda y: requests.get(y["photo"], proxies=proxy).status_code == 200, rcv)) > 0,
    )

    # test interactive search random
    interactive_assert(
        "Random",
        lambda rcv: len(rcv) == 1 and "Search a code or an actress." in rcv[0]["text"],
    )

    # test interactive search magnets
    interactive_assert(
        "Magnet", lambda rcv: "Search a code. e.g. ABP-231" == rcv[0]["text"]
    )
    interactive_assert("ABP-123", lambda rcv: len(rcv) > 1)

    # test interactive search brief
    interactive_assert(
        "Brief", lambda rcv: "Search a code. e.g. ABP-231" == rcv[0]["text"]
    )
    interactive_assert(
        "ABP-123",
        lambda rcv: len(rcv) == 1 and requests.get(rcv[0]["photo"], proxies=proxy).status_code == 200,
    )
