def test_file(self):
        """
        This method will test domain2idna.file().
        """

        file_to_pass = "this_file_is_a_ghost"
        BaseStdout.setUp(self)

        expected = False
        actual = path.isfile(file_to_pass)

        self.assertEqual(expected, actual)

        File(file_to_pass).write("\n".join(self.domains_to_test))

        expected = True
        actual = path.isfile(file_to_pass)

        self.assertEqual(expected, actual)

        expected = "\n".join(self.domains_to_test)
        actual = File(file_to_pass).read()

        self.assertEqual(expected, actual)

        expected = "\n".join(self.converted) + "\n"
        file(file_to_pass, None)
        actual = sys.stdout.getvalue()

        self.assertEqual(expected, actual)

        File(file_to_pass).delete()

        expected = False
        actual = path.isfile(file_to_pass)

        self.assertEqual(expected, actual)
