def test_url_match(self):
        for r in test_rules:
            route = Route(r.rule, ["GET"], temp, {
                "checking_param": False
            })
            with self.subTest(matching=r.matching):
                t, _ = route.match(r.matching, "GET")
                self.assertIsNotNone(t)
            with self.subTest(not_matching=r.not_matching):
                t, _ = route.match(r.not_matching, "GET")
                self.assertIsNone(t)
