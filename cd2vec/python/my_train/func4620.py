def test_player_contract_returns_contract(self):
        contract = self.player.contract

        expected = {
            '2012': {
                'age': '22',
                'team': 'Houston Astros',
                'salary': '$483,000'
            },
            '2013': {
                'age': '23',
                'team': 'Houston Astros',
                'salary': '$505,700'
            },
            '2014': {
                'age': '24',
                'team': 'Houston Astros',
                'salary': '$1,250,000'
            },
            '2015': {
                'age': '25',
                'team': 'Houston Astros',
                'salary': '$2,500,000'
            },
            '2016': {
                'age': '26',
                'team': 'Houston Astros',
                'salary': '$3,500,000'
            },
            '2017': {
                'age': '27',
                'team': 'Houston Astros',
                'salary': '$4,500,000'
            },
            '2018': {
                'age': '28',
                'team': 'Houston Astros',
                'salary': '$9,000,000'
            },
            '2019': {
                'age': '29',
                'team': 'Houston Astros',
                'salary': '$9,500,000'
            },
            '2020': {
                'age': '30',
                'team': 'Houston Astros',
                'salary': '$29,000,000'
            },
            '2021': {
                'age': '31',
                'team': 'Houston Astros',
                'salary': '$29,000,000'
            },
            '2022': {
                'age': '32',
                'team': 'Houston Astros',
                'salary': '$29,000,000'
            },
            '2023': {
                'age': '33',
                'team': 'Houston Astros',
                'salary': '$29,000,000'
            },
            '2024': {
                'age': '34',
                'team': 'Houston Astros',
                'salary': '$29,000,000'
            }
            }

        assert contract == expected
