def test_error_handling():
    """Assert that models with errors are removed from pipeline."""
    trainer = TrainerClassifier(['LR', 'LDA'], n_calls=4, n_random_starts=[2, -1])
    trainer.run(bin_train, bin_test)
    assert trainer.errors.get('LDA')
    assert 'lDA' not in trainer.models
    assert 'LDA' not in trainer.results.index

