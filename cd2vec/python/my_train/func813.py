@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'caller')
    def test_api_search_tags_002(server):
        """Try to get unique content based on ``tags`` attribute.

        Try to send GET /groups to get unique groups. In this case the ``scat``
        query parameter limits the search to solution category. There are no
        resources stored in the searched category.
        """

        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '363'
        }
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '404',
                'statusString': '404 Not Found',
                'module': 'snippy.testing.testing:123',
                'title': 'cannot find unique fields for tags attribute'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_get(
            path='/api/snippy/rest/tags',
            headers={'accept': 'application/vnd.api+json'},
            query_string='scat=solution')
        assert result.status == falcon.HTTP_404
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
