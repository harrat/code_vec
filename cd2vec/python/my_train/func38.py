def test_main_from_config():



    config.update(config_original)

    config.update({

        'data_source': 'csv',

        'csv_full_path': 'https://datahub.io/core/global-temp/r/monthly.csv',

        'predicted_column': 'Mean',

        'datetime_index': 'Date',

        'freq': 'M',

        'return_type': 'all_dataframe',

        'max_imported_length': 1000,

        'plotit': 1,

        'plot_type': 'matplotlib',

        'show_plot': 0,

        'optimization': 1,

        'optimization_variable': 'data_transform',

        'optimization_values': [0, 'difference'],

        'print_number_of_models': 10,

        'analyzeit': 1,

        'last_row': 0,

        'correlation_threshold': 0.2,

        'optimizeit': 0,

        'standardizeit': '01',

        'multiprocessing': 'process',

        'smoothit': (19, 2),

        'power_transformed': 1,

        'analyze_seasonal_decompose': {'period': 32, 'model': 'additive'},



        'used_models': [

            "Bayes ridge regression",

        ]

    })



    result = predictit.main.predict()

    assert not result.dropna().empty

    return result

