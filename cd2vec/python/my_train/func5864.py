def test_downwards_compatibility(self):
        """
            Test the compatibility of the current version of `EasyJWT` with older versions of `EasyJWT`.

            Expected Result: The verification succeeds without errors. All data is restored.
        """

        for folder_entry in listdir(TOKEN_FOLDER):
            # Skip all folders.
            if isdir(folder_entry):
                continue

            # Skip all files not ending in 'jwt' and the external token.
            if not folder_entry.endswith('jwt') or folder_entry == 'external.jwt':
                continue

            # Get the token and its version.
            token, version = self.get_token_and_version_from_file(folder_entry)
            message = 'Token v' + version

            # Try to verify the token.
            with self.assertNotRaises(EasyJWTError, message):
                token_object = CompatibilityToken.verify(token, KEY, ISSUER, AUDIENCE)

            # Verify the claim set.
            self.assertIsNotNone(token_object, message)
            self.verify_claim_set(token_object, message)
