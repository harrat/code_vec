def test_dont_memoize():
    assert foo({}) == {}
    assert foo not in MEMO
    assert any([m.__name__ == "fib" for m in MEMO])

