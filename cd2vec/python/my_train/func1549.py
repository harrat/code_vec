def test_dependencies_with_retries(make_runner, dep_cases, common_exec_ctx):
    runner = make_runner(max_retries=2)
    runner.runall(dep_cases)
    assert_dependency_run(runner)

