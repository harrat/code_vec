def test_write_json(json_file):
    with atomic.atomic_write(str(json_file)) as temp_file:
        with open(str(json_file)) as products_file:
            # get the JSON data into memory
            products_data = json.load(products_file)
        # now process the JSON data
        products_data.append(
            {'uuid': "2299d69e-deba-11e8-bded-680715cce955",
             'price': 111.0,
             'name': "Test Product"
             })
        json.dump(products_data, temp_file)

    with open(str(json_file)) as f:
        products_data = json.load(f)
    test = [i for i in products_data if i["uuid"] == "2299d69e-deba-11e8-bded-680715cce955"]
    assert test[0]["name"] == 'Test Product'

