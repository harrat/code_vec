def test_configure(self):
        def wrapper(a, b, c) -> Iterable:
            return []

        configure(progress=True, progress_wrapper=wrapper,
                  system_progress=True, system_progress_wrapper='foo',
                  threads=2, executable_path=[Path('foo')])

        assert wrapper == ITERABLE_PROGRESS.wrapper
        assert ('foo',) == PROCESS_PROGRESS.wrapper
        assert 2 == THREADS.threads
        assert Path('foo') in EXECUTABLE_CACHE.search_path

        configure(threads=False)
        assert 1 == THREADS.threads

        import multiprocessing
        configure(threads=True)
        assert multiprocessing.cpu_count() == THREADS.threads
