def test_ignore_warnings_in_start():
    """Make sure that modules being introspected in start() does not emit warnings."""
    with assert_module_with_emitted_warning():
        freezer = freeze_time(datetime.datetime(2016, 10, 27, 9, 56))
