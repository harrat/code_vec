def test_check_availability(self):
        result = api.check_availability(domainNames=[sample1.domainName])

        results = result.results
        self.assertTrue(len(results) == 1)

        search_result = results[0]
        self.assertTrue(search_result.domainName, sample1.domainName)
