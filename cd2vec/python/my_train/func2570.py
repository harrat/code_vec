def test_visibility_analysis(self):
        exp_vis_ids1 = [5, 11]
        exp_vis_ids2 = [2, 8]

        self.mapper.start_visibility_analysis()

        try:
            np.testing.assert_equal(self.mapper.textures[0].vis_triangle_indices, exp_vis_ids1)
            np.testing.assert_equal(self.mapper.textures[1].vis_triangle_indices, exp_vis_ids2)
            res = True
        except AssertionError as err:
            res = False
            print(err)
        self.assertTrue(res)

        exp_vis_vertices1 = np.array([[1, 1, -1],
                                      [-1, 1, 1],
                                      [1, 1, 1],
                                      [1, 1, -1],
                                      [-1, 1, -1],
                                      [-1, 1, 1]])

        exp_vis_vertices2 = np.array([[1, 1, 1],
                                      [1, -1, -1],
                                      [1, 1, -1],
                                      [1, 1, 1],
                                      [1, -1, 1],
                                      [1, -1, -1]])

        try:
            np.testing.assert_equal(self.mapper.textures[0].visible_vertices, exp_vis_vertices1)
            np.testing.assert_equal(self.mapper.textures[1].visible_vertices, exp_vis_vertices2)
            res = True
        except AssertionError as err:
            res = False
            print(err)
        self.assertTrue(res)
