def test_spindles_detect(self):
        """Test spindles_detect"""
        #######################################################################
        # SINGLE CHANNEL
        #######################################################################
        freq_sp = [(11, 16), [12, 14]]
        freq_broad = [(0.5, 30), [1, 25]]
        duration = [(0.3, 2.5), [0.5, 3]]
        min_distance = [None, 0, 500]
        prod_args = product(freq_sp, freq_broad, duration, min_distance)

        for i, (s, b, d, m) in enumerate(prod_args):
            spindles_detect(data, sf, freq_sp=s, duration=d,
                            freq_broad=b, min_distance=m)

        sp = spindles_detect(data, sf, verbose=True)
        assert sp.summary().shape[0] == 2
        sp.get_mask()
        sp.get_sync_events()
        sp.get_sync_events(time_before=10)  # Invalid time window
        sp.plot_average(ci=None, filt=(None, 30))  # Skip bootstrapping
        np.testing.assert_array_equal(np.squeeze(sp._data), data)
        assert sp._sf == sf
        sp.summary(grp_chan=True, grp_stage=True, aggfunc='median', sort=False)

        # Test with custom thresholds
        spindles_detect(data, sf, thresh={'rel_pow': 0.25})
        spindles_detect(data, sf, thresh={'rms': 1.25})
        spindles_detect(data, sf, thresh={'rel_pow': 0.25, 'corr': .60})

        # Test with disabled thresholds
        spindles_detect(data, sf, thresh={'rel_pow': None})
        spindles_detect(data, sf, thresh={'corr': None}, verbose='debug')
        spindles_detect(data, sf, thresh={'rms': None})
        spindles_detect(data, sf, thresh={'rms': None, 'corr': None})
        spindles_detect(data, sf, thresh={'rms': None, 'rel_pow': None})
        spindles_detect(data, sf, thresh={'corr': None, 'rel_pow': None})

        # Test with hypnogram
        spindles_detect(data, sf, hypno=np.ones(data.size))

        # Single channel with Isolation Forest + hypnogram
        sp = spindles_detect(data_full[1, :], sf, hypno=hypno_full,
                             remove_outliers=True)

        with self.assertLogs('yasa', level='WARNING'):
            spindles_detect(data_n3, sf)
        # assert sp is None --> Fails?

        # Ensure that the two warnings are tested
        with self.assertLogs('yasa', level='WARNING'):
            sp = spindles_detect(data_n3, sf, thresh={'corr': .95})
        assert sp is None

        # Test with wrong data amplitude (1)
        with self.assertLogs('yasa', level='ERROR'):
            sp = spindles_detect(data_n3 / 1e6, sf)
        assert sp is None

        # Test with wrong data amplitude (2)
        with self.assertLogs('yasa', level='ERROR'):
            sp = spindles_detect(data_n3 * 1e6, sf)
        assert sp is None

        # Test with a random array
        with self.assertLogs('yasa', level='ERROR'):
            sp = spindles_detect(np.random.random(size=1000), sf)
        assert sp is None

        # No values in hypno intersect with include
        with pytest.raises(AssertionError):
            sp = spindles_detect(data, sf, include=2,
                                 hypno=np.zeros(data.size, dtype=int))

        #######################################################################
        # MULTI CHANNEL
        #######################################################################

        sp = spindles_detect(data_full, sf, chan_full)
        sp.get_mask()
        sp.get_sync_events(filt=(12, 15))
        sp.summary()
        sp.summary(grp_chan=True)
        sp.plot_average(ci=None)
        assert sp._data.shape == sp._data_filt.shape
        np.testing.assert_array_equal(sp._data, data_full)
        assert sp._sf == sf
        sp_no_out = spindles_detect(data_full, sf, chan_full,
                                    remove_outliers=True)
        sp_multi = spindles_detect(data_full, sf, chan_full,
                                   multi_only=True)
        assert sp_multi.summary().shape[0] < sp.summary().shape[0]
        assert sp_no_out.summary().shape[0] < sp.summary().shape[0]

        # Test with hypnogram
        sp = spindles_detect(data_full, sf, hypno=hypno_full, include=2)
        sp.summary(grp_chan=False, grp_stage=False)
        sp.summary(grp_chan=False, grp_stage=True, aggfunc='median')
        sp.summary(grp_chan=True, grp_stage=False)
        sp.summary(grp_chan=True, grp_stage=True, sort=False)

        # Using a MNE raw object (and disabling one threshold)
        spindles_detect(data_mne, thresh={'corr': None, 'rms': 3})
        spindles_detect(data_mne, hypno=hypno_mne, include=2,
                        verbose=True)

        plt.close('all')
