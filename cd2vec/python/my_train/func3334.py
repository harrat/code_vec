def test_get_single_result_with_artist_and_song_title(self):
        result = runner.invoke(songtext.cli, [
            '-a', 'Paramore',
            '-t', 'Where the Lines Overlap',
            '--no-pager'
        ])
        assert result.exit_code == 0
        assert 'Paramore: Where The Lines Overlap' in result.output
