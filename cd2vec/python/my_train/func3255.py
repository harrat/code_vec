@pytest.mark.parametrize('key,val,mtable,out,exception',[
		('@#F', None, None, None, JoinParseError),
		('t', ['t.f'], None, None, JoinParseError),
		('t', ('t.f', 'f2'), None, None, JoinParseError),
		('t', ('t.f', 'f2'), None, None, JoinParseError),
		('t', 'f', None, None, JoinParseError),
		('t', 'f', TableFrom('mt'), 'INNER JOIN "t" ON "t"."f"="mt"."f"',None),
		('t', {'t1.f': 'mt.f2'}, None, None, JoinParseError),
		('[<>]t(t1)', ['f1', 'f2'], TableFrom('mt'), 'FULL OUTER JOIN "t" AS "t1" ON "t1"."f1"="mt"."f1" AND "t1"."f2"="mt"."f2"',None),
		('[<]t', {'f': 'f2'}, TableFrom('mt', alias='main'), 'RIGHT JOIN "t" ON "t"."f"="main"."f2"',None),
	])
	def testTerm(self, key, val, mtable, out, exception):
		if exception:
			with pytest.raises(exception):
				JoinTerm(key, val, mtable)
		else:
			assert str(JoinTerm(key, val, mtable)) == out
