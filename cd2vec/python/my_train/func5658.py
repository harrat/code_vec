def test_evaluate_model_on_path_subtokens():
    f = tempfile.TemporaryDirectory()
    actual = evaluate_on_path(load_default_model(),
                              Path(project_dir) /'data' /'dev' /'valid',
                              save_to=Path(f.name), full_tokens=False,
                              batch_size=3)

    total = actual.total()
    assert int(total['Entropy']) == 13
    assert total['n_samples'] == 2839

