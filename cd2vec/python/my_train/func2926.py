def test_split(self):
        true_msg = '[test] [exp1] split time:  0.50 sec.'
        f = Mock()

        t = Timer(name='test')
        time.sleep(0.2)
        t.split()
        time.sleep(0.3)
        t.split('exp1', callback=f)
        time.sleep(0.1)
        t.stop()

        self.assertAlmostEqual(t.elapsed, 0.6, 1)
        f.assert_called_once_with(true_msg)
