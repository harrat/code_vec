def test_TestCaseExtension_ReturnOnlyCornerCase(self):
        """Test for only corner case"""
        ext = '.case'
        path = 'tests'
        result = list(taeper.scantree(path, ext))
        expected = ['tests/data/corner.case', 'tests/data/fail/corner.case']
        for x, y in zip(expected, result):
            self.assertEqual(x, y)
