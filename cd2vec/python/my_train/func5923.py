@pytest.mark.core
def testConfigSave(nwTemp,nwRef):
    tmpConf = path.join(nwTemp,"novelwriter.conf")
    refConf = path.join(nwRef, "novelwriter.conf")
    assert theConf.confPath == nwTemp
    assert theConf.saveConfig()
    assert cmpFiles(tmpConf, refConf, [2])
    assert not theConf.confChanged

@pytest.mark.core
def testConfigSetConfPath(nwTemp):
    assert theConf.setConfPath(None)
    assert not theConf.setConfPath(path.join("somewhere","over","the","rainbow"))
    assert theConf.setConfPath(path.join(nwTemp,"novelwriter.conf"))
    assert theConf.confPath == nwTemp
    assert theConf.confFile == "novelwriter.conf"
    assert not theConf.confChanged
