def test_invalid_type_name(self):
        custom_obj = copy.deepcopy(self.valid_custom_object)
        custom_obj['type'] = "corpo_ration"
        custom_obj['id'] = "corpo_ration--4527e5de-8572-446a-a57a-706f15467461"
        results = validate_parsed_json(custom_obj, self.options)
        self.assertEqual(results.is_valid, False)

        custom_obj['type'] = "corpor@tion"
        custom_obj['id'] = "corpor@tion--4527e5de-8572-446a-a57a-706f15467461"
        results = validate_parsed_json(custom_obj, self.options)
        self.assertEqual(results.is_valid, False)

        self.assertFalseWithOptions(custom_obj, enabled='custom-prefix-lax')
        self.assertFalseWithOptions(custom_obj, disabled='custom-prefix-lax')
