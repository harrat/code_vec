def test_idfobjectkeys():
    """py.test for idfobjectkeys"""
    expected = [
        "LEAD INPUT",
        "SIMULATION DATA",
        "VERSION",
        "SIMULATIONCONTROL",
        "BUILDING",
    ]
    idf = IDF(StringIO(""))
    result = idf_helpers.idfobjectkeys(idf)
    assert result[:5] == expected

