def test_output_has_Show_objects(results):
    assert any([isinstance(item, Show) for item in results[0]])

