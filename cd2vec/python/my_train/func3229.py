def test_cleanup_on_gc():
    def f():
        s = vault_dev.server()
        s.start()
        return s.process
    p = f()
    gc.collect()
    assert p.poll() < 0

