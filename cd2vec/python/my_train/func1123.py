def test_already_in_progress_raises_error(cert_and_key, ip_address):
    c = bankid.BankIDJSONClient(certificates=cert_and_key, test_server=True)
    pn = _get_random_personal_number()
    out = c.authenticate(ip_address, pn)
    with pytest.raises(bankid.exceptions.AlreadyInProgressError):
        out2 = c.authenticate(ip_address, pn)
