@staticmethod
    @pytest.mark.usefixtures('default-snippets', 'export-time')
    def test_cli_export_snippet_026(snippy):
        """Export snippets with search keyword.

        Export snippets based on search keyword. In this case the search
        keyword matches to two snippets that must be exported to default
        file since the -f|-file option is not used.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--sall', 'docker'])
            assert cause == Cause.ALL_OK
            Content.assert_mkdn(mock_file, './snippets.mkdn', content)
