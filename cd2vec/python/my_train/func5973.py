def test_get_squad_v2(self):
        raw = get_squad(version=2)
        self.assertIn('train', raw)
        self.assertEqual(len(raw['train']), 130_319)
        self.assertIn('dev', raw)
        self.assertEqual(len(raw['dev']), 11_873)
