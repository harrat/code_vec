def test_register_no_id(self):
        '''
        Test that License classes cannot be registered without id
        '''
        class FooLicense(base.License):
            pass

        with pytest.raises(AttributeError):
            licenraptor.register(FooLicense)
