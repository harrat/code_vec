def test_remove_cmd_run(mocker):
    run = mocker.patch.object(commands.UninstallCommand, "run")  # type: MagicMock
    save = mocker.patch.object(commands.file, "save")  # type: MagicMock
    mocker.patch.object(
        commands, "get_orphaned_packages", return_value=["pkg1", "pkg2"]
    )  # type: MagicMock

    # run method
    cmd = commands.UninstallCommandPlus("u", "u")
    opts, args = cmd.parse_args(["dev"])
    cmd.run(opts, args)

    run.assert_called_once_with(opts, ["dev", "pkg1", "pkg2"])
    save.assert_called_once()

