def test_cancel_with_grace(minimal_job, scheduler, local_only):
    # This test emulates a spawned process that ignores the SIGTERM signal
    # and also spawns another process:
    #
    #   reframe --- local job script --- sleep 10
    #                  (TERM IGN)
    #
    # We expect the job not to be cancelled immediately, since it ignores
    # the gracious signal we are sending it. However, we expect it to be
    # killed immediately after the grace period of 2 seconds expires.
    #
    # We also check that the additional spawned process is also killed.
    minimal_job.time_limit = '1m'
    minimal_job.scheduler._cancel_grace_period = 2
    prepare_job(minimal_job,
                command='sleep 5 &',
                pre_run=['trap -- "" TERM'],
                post_run=['echo $!', 'wait'])
    minimal_job.submit()

    # Stall a bit here to let the the spawned process start and install its
    # signal handler for SIGTERM
    time.sleep(1)

    t_grace = datetime.now()
    minimal_job.cancel()
    t_grace = datetime.now() - t_grace

    minimal_job.wait()
    # Read pid of spawned sleep
    with open(minimal_job.stdout) as fp:
        sleep_pid = int(fp.read())

    assert t_grace.total_seconds() >= 2
    assert t_grace.total_seconds() < 5
    assert minimal_job.state == 'TIMEOUT'

    # Verify that the spawned sleep is killed, too
    assert_process_died(sleep_pid)

