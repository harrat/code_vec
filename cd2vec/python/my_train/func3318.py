def test_no_arguments_lists_projects(cli, projects_db):
    lines = cli('project').splitlines()

    assert set(lines) == set([
        'N [test]   42 not started project',
        'A [test]   43 active project',
        'A [test]   44 2nd active project',
    ])
