def test_timer_run():
    timer = cvb.Timer()
    time.sleep(1)
    assert abs(timer.since_start() - 1) < 1e-2
    time.sleep(1)
    assert abs(timer.since_last_check() - 1) < 1e-2
    assert abs(timer.since_start() - 2) < 1e-2
    timer = cvb.Timer(False)
    with pytest.raises(cvb.TimerError):
        timer.since_start()
    with pytest.raises(cvb.TimerError):
        timer.since_last_check()
