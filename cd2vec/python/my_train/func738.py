def test_run_d80_const_sphere():
    print("test_run_d80:")
    dim = 80
    mean = np.ones([dim, 1]) * 10
    sigma = 2.0
    lamb = 32 # note that lamb (sample size) should be even number
    allowable_evals = (48.8 + 1.4*3) * 1e3 # 2 sigma
    iteration_number = int(allowable_evals / lamb) + 1

    cr = CRFMNES(dim, const_sphere, mean, sigma, lamb)
    x_best, f_best = cr.optimize(iteration_number)
    print("f_best:{}".format(f_best))
    assert f_best < 1e-12

