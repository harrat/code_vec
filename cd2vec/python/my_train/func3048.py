def test_empty_list(self):
        indicator = copy.deepcopy(self.valid_indicator)
        indicator['my_new_property'] = []
        results = validate_parsed_json(indicator, self.options)
        self.assertEqual(results.is_valid, False)
