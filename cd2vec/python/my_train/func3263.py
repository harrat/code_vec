def test_function_log(self):
        """Test log function."""
        self.assertEqual(ucal.evaluate('log(exp(1))'), '1')
        self.assertEqual(ucal.evaluate('log(1)'), '0')
        self.assertRaises(ucal.QuantityError, ucal.evaluate, 'log(1m)')
