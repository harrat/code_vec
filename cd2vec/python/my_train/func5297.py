@staticmethod
    def test_airports():
        fr = flightradar24.Api()
        airports = fr.get_airports()
        assert airports['rows'] is not None
        assert len(airports['rows']) > 100  # Expect more than 100 airports
        check_jfk = 0
        for airport in airports['rows']:
            if airport['iata'] == "JFK":
                check_jfk = 1
        assert check_jfk == 1
