def test_full_create(c):
    env_zip = os.path.join(c.conda_envs, env_name+'.zip')
    assert env_zip == c.create_env(env_name, packages=[
        'python=3', 'numpy', 'nomkl'], remove=True)
    assert os.path.getsize(env_zip) > 500000  # ensures zipfile has non-0 size
    assert zipfile.is_zipfile(env_zip)

    f = zipfile.ZipFile(env_zip, 'r')
    try:
        assert f.getinfo('test_env/bin/python')
    finally:
        f.close()
