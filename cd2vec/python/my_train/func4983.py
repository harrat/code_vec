def test_set_head_middle(self):
        audiofile = self.load(self.AUDIO_FILE_WAVE)
        audiofile.set_head_middle_tail(head_length=TimeValue("2.000"), middle_length=TimeValue("18.000"))
        self.assertEqual(audiofile.all_length, 1331)
        self.assertEqual(audiofile.head_length, 50)
        self.assertEqual(audiofile.middle_length, 450)
        self.assertEqual(audiofile.tail_length, 831)
