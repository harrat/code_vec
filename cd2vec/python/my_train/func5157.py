def test_loading_failure():
    with open(file_path, "w") as f:
        f.write("""
from breathe import Breathe
from ..testutils import DoNothing

Breathe.add_commands(,,,
    None,
    {
        "apple": DoNothing(),
    }
)
"""
        )
    modules = {
        "tests": {
            "my_grammar": ["fruit"],
        }
    }
    Breathe.load_modules(modules)
    assert len(Breathe.modules) == 1
    assert len(Breathe.core_commands) == 0

def test_loading():
    with open(file_path, "w") as f:
        f.write("""
from breathe import Breathe
from ..testutils import DoNothing
