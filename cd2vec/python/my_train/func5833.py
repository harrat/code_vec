def test_prep_str_murmur3_128bit(self):
        obj = "a"
        expected_result = {
            obj: 119173504597196970070553896747624927922
        }
        result = DeepHash(obj, ignore_string_type_changes=True, hasher=DeepHash.murmur3_128bit)
        assert expected_result == result
