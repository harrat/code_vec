@pytest.mark.parametrize("skill, result",

                         [

                             ('woodcutting', 100),

                             ('WoOdcuttIng', 100),

                             ('constitution', 1_154),

                             ('CoNsTiTutioN', 1_154),

                             ('attack', 0),

                             ('aTTacK', 0),

                             (3, 1_154),

                             (8, 100),

                             (0, 0),

                             (26, 0),

                             (13, 0),

                         ])

def test_skill_exp(valid_player, skill, result):

    assert valid_player.skill(skill).exp == result

