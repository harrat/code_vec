def test_get_sts_cobp_a(self):
        # Building permits - annual data (2010 = 100)
        df = web.DataReader(
            "sts_cobp_a",
            "eurostat",
            start=pd.Timestamp("2000-01-01"),
            end=pd.Timestamp("2013-01-01"),
        )

        idx = pd.date_range("2000-01-01", "2013-01-01", freq="AS", name="TIME_PERIOD")
        ne_name = (
            "Index, 2010=100",
            "Building permits - m2 of useful floor area",
            "Unadjusted data (i.e. neither seasonally adjusted nor "
            "calendar adjusted data)",
            "Non-residential buildings, except office buildings",
            "Netherlands",
            "Annual",
        )
        ne_values = [
            200.0,
            186.5,
            127.3,
            130.7,
            143.3,
            147.8,
            176.7,
            227.4,
            199.4,
            128.5,
            100.0,
            113.8,
            89.3,
            77.6,
        ]
        ne = pd.Series(ne_values, name=ne_name, index=idx)

        uk_name = (
            "Index, 2010=100",
            "Building permits - m2 of useful floor area",
            "Unadjusted data (i.e. neither seasonally adjusted nor "
            "calendar adjusted data)",
            "Non-residential buildings, except office buildings",
            "United Kingdom",
            "Annual",
        )
        uk_values = [
            112.5,
            113.3,
            110.2,
            112.1,
            119.1,
            112.7,
            113.1,
            121.8,
            114.0,
            105.9,
            100.0,
            98.6,
            103.7,
            81.3,
        ]
        uk = pd.Series(uk_values, name=uk_name, index=idx)

        for expected in [ne, uk]:
            expected.index.freq = None
            result = df[expected.name]
            tm.assert_series_equal(result, expected)
