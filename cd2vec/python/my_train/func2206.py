def test_check_writable_file(self):
        writable = self.root.make_file(permissions='w')
        non_writable = self.root.make_file(permissions='r')
        check_writable_file(writable)
        with self.assertRaises(IOError):
            check_writable_file(non_writable)
        parent = self.root.make_directory()
        check_writable_file(parent / 'foo')
        subdir_path = parent / 'bar' / 'foo'
        check_writable_file(subdir_path)
        assert subdir_path.parent.exists()
        with self.assertRaises(IOError):
            parent = self.root.make_directory(permissions='r')
            check_writable_file(parent / 'foo')
        assert safe_check_writable_file(writable)
        assert safe_check_writable_file(non_writable) is None
