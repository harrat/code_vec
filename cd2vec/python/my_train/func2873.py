@pytest.mark.project
def testProjectNewCustom(nwTempCustom, nwRef, nwTemp):
    projData = {
        "projName": "Test Custom",
        "projTitle": "Test Novel",
        "projAuthors": "Jane Doe\nJohn Doh\n",
        "projPath": nwTempCustom,
        "popSample": False,
        "popMinimal": False,
        "popCustom": True,
        "addRoots": [
            nwItemClass.PLOT,
            nwItemClass.CHARACTER,
            nwItemClass.WORLD,
            nwItemClass.TIMELINE,
            nwItemClass.OBJECT,
            nwItemClass.ENTITY,
        ],
        "numChapters": 3,
        "numScenes": 3,
        "chFolders": True,
    }
    theProject.mainConf = theConf
    theProject.projTree.setSeed(42)
    assert theProject.newProject(projData)
    assert theProject.saveProject()
    assert theProject.closeProject()
    projFile = path.join(nwTempCustom, "nwProject.nwx")
    refFile  = path.join(nwRef, "proj", "4_nwProject.nwx")
    assert cmpFiles(projFile, refFile, [2, 6, 7, 8])

@pytest.mark.project
def testProjectNewSample(nwTempSample, nwLipsum, nwRef, nwTemp):
    projData = {
        "projName": "Test Sample",
        "projTitle": "Test Novel",
        "projAuthors": "Jane Doe\nJohn Doh\n",
        "projPath": nwTempSample,
        "popSample": True,
        "popMinimal": False,
        "popCustom": False,
    }
    theProject.mainConf = theConf
    assert theProject.newProject(projData)
    assert theProject.openProject(nwTempSample)
    assert theProject.projName == "Sample Project"
    assert theProject.saveProject()
    assert theProject.closeProject()
