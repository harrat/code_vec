def test_fail_init_two_stashes_passphrase_file_exists(self,
                                                          stash_path,
                                                          temp_file_path):
        _invoke('init_stash "{0}"'.format(stash_path))
        result = _invoke('init_stash "{0}" -b sqlalchemy'.format(
            temp_file_path))

        assert 'Overwriting might prevent you' in result.output
        assert result.exit_code == 1
