@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_search_solution_001(snippy, capsys):
        """Search solutions with ``sall`` option.

        Search solutions from all content fields. The match is made from one
        solution content data.
        """

        output = (
            '1. Debugging Elastic Beats @beats [4346ba4c79247430]',
            Const.NEWLINE.join(Solution.BEATS_OUTPUT),
            '   :',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--scat', 'solution', '--sall', 'filebeat', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
