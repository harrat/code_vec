def test_unsigned_witness_tx(self):
        outpoint = simple.outpoint(
            tx_id=helpers.P2WPKH['human']['ins'][0]['hash'],
            index=helpers.P2WPKH['human']['ins'][0]['index'])
        tx_in = simple.unsigned_input(
            outpoint=outpoint,
            sequence=helpers.P2WPKH['human']['ins'][0]['sequence'])
        tx_out = simple.output(
            helpers.P2WPKH['human']['outs'][0]['value'],
            helpers.P2WPKH['human']['outs'][0]['addr'])
        tx = simple.unsigned_witness_tx(
            tx_ins=[tx_in],
            tx_outs=[tx_out],
            lock_time=helpers.P2WPKH['human']['locktime'])

        self.assertEqual(tx, helpers.P2WPKH['ser']['tx']['unsigned'])
