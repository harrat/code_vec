@pytest.mark.vcr()
def test_workbench_vuln_info_filter_type_unexpectedvalueerror(api):
    with pytest.raises(UnexpectedValueError):
        api.workbenches.vuln_info(19506, filter_type='NOT')
