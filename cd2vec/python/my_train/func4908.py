@pytest.mark.parametrize("modname, errors, cnames, fn",
        itertools.product(NAMES_EXISTING, ERROR_MSGS, CALLABLE_NAMES,
                          CALLABLE_ALIASES))
def test_callable_missing(modname, errors, cnames, fn):
    _check_not_loaded(modname)
    basename = lazy_import.module_basename(modname)
    if isinstance(cnames, six.string_types):
        lazys = (fn(modname+"."+cnames, error_strings=errors),)
        cnames = (cnames, )
    else:
        lazys = fn(modname, *cnames, error_strings=errors)
    for lazy, cname in zip(lazys, cnames):
        if errors is None:
            expected_err = lazy_import._MSG_CALLABLE.format(module=modname,
                                                   caller=__name__,
                                                   install_name=basename,
                                                   callable=cname)
        else:
            errors['callable'] = cname
            expected_err = errors["msg_callable"].format(**errors)
        _check_callable_missing(lazy, msg=expected_err)
    
@pytest.mark.parametrize("modname, errors, cnames, fn",
        itertools.product(NAMES_EXISTING, ERROR_MSGS, CALLABLE_NAMES,
                          CALLABLE_ALIASES))
def test_error_callable_as_baseclass(modname, errors, cnames, fn):
    _check_not_loaded(modname)
    if isinstance(cnames, six.string_types):
        lazys = (fn(modname+"."+cnames, error_strings=errors),)
    else:
        lazys = fn(modname, *cnames, error_strings=errors)
    for lazy in lazys:
        with pytest.raises(NotImplementedError) as excinfo:
            class TestClass(lazy):
                pass
