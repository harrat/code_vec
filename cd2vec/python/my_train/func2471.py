def test_calc_lwf(class_objects, params_objects):
    # TODO: fix this
    d_source, d_class, s_width = class_objects
    obj = CalculateProducts(d_source, params_objects)
    lwc_in = np.array([[0.001, 0.001, 0.002],
                       [0.003, 0.002, 0.001]])
    compare = np.array([[0.001, 0.005508, 0.011016],
                        [0.016524, 0.011016, 0.001]])
    testing.assert_array_almost_equal(obj._calc_lwf(lwc_in), compare)

