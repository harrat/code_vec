def test_alternate_hebrew_date():
    zmanim = hebcal.Zmanim(ti)
    ti.alternate_nighttime = zmanim.night_72
    assert str(ti.alternate_hebrew_date()) == "(5779, 7, 10)"
    assert ti.hebrew_day() == 11

