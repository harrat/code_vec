@pytest.mark.parametrize('fixture', fixture_types['engine'])
def test_file_glob(fixture, request):
    engine = request.getfixturevalue(fixture)
    job = engine.launch('alpine', 'touch a.txt b c d.txt e.gif')
    print(job.rundata)
    job.wait()

    assert set(job.get_output().keys()) <= set('a.txt b c d.txt e.gif'.split())
    assert set(job.glob_output('*.txt').keys()) == set('a.txt d.txt'.split())

