def test_deathday_attr(person: Person):
    assert isinstance(person.deathday, (str, type(None)))

