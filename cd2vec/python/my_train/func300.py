def test_duplicate_sheet(self):
        # Important! This test needs to be executed after test_create_sheets, as it clones that sheet #
        self.spreadsheet.duplicate_sheet(new_sheet_name="cloned_sheet", sheet_name="test_sheet")
        assert len(self.spreadsheet.sheets) == 5
