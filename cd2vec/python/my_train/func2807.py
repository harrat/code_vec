@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'json')
    def test_cli_import_solution_021(snippy):
        """Import solution.

        Import new solution from json file.
        """

        content = {
            'data': [
                Solution.NGINX
            ]
        }
        file_content = Content.get_file_content(Content.JSON, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            json.load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '--scat', 'solution', '-f', 'one-solution.json'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-solution.json', mode='r', encoding='utf-8')
