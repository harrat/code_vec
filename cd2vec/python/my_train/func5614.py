def test_loadbalanced_setitem_method(mocked_loadbalanced):
    """Test FlaskMultiRedis loadbalanced __setitem__ method."""

    mocked_loadbalanced['name'] = 'node0'
    assert 'node0' in [x.name for x in mocked_loadbalanced._redis_nodes]
    mocked_loadbalanced._redis_nodes = []
    mocked_loadbalanced['name'] = 'node0'

