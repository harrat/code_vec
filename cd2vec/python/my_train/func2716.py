def test_find_best_solution_pipelines(self):

        from sklearn.ensemble import RandomForestClassifier
        from sklearn.svm import SVC
        from sklearn.linear_model import PassiveAggressiveClassifier
        from sklearn.feature_selection import SelectKBest, f_classif
        from sklearn.preprocessing import RobustScaler, StandardScaler
        from sklearn.datasets import load_digits
        from sklearn.model_selection import cross_validate
        import lazygrid as lg
        import pandas as pd

        # lg.database.drop_db("./database/database.sqlite")
        X, y = load_digits(return_X_y=True)
        X = pd.DataFrame(X)

        preprocessors = [StandardScaler()]
        feature_selectors = [SelectKBest(score_func=f_classif, k=1), SelectKBest(score_func=f_classif, k=10)]
        classifiers = [RandomForestClassifier(random_state=42)]

        elements = [preprocessors, feature_selectors, classifiers]

        pipelines = lg.grid.generate_grid(elements)
        val_scores = []
        for pipeline in pipelines:
            scores = cross_validate(pipeline, X, y, cv=10, n_jobs=5)
            val_scores.append(scores["test_score"])

        best_idx, best_solutions_idx, pvalues = lg.statistics.find_best_solution(val_scores)

        self.assertEqual(best_idx, 1)
        self.assertEqual(best_solutions_idx, [0, 1, 4, 5])
