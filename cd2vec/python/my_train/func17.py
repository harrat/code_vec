def test_translate_multi_new_mod(client):
    eng.STORAGE[text] = {'en': 'falsely modified'}
    resp = eng.translate(
        text=text,
        dest=[dest, 'es'],
        src=src
    )
    assert resp == {
        dest: translation,
        'es': translation_es
    }
