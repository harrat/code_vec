def test_renamenodes():
    """py.test for renamenodes"""
    idftxt = """PIPE:ADIABATIC,
         np1,
         np1_inlet,
         np1_outlet;
         !- ['np1_outlet', 'np1_np2_node'];

    BRANCH,
         sb0,
         0.0,
         ,
         Pipe:Adiabatic,
         np1,
         np1_inlet,
         np1_outlet,
         Bypass;
    """
    outtxt = """PIPE:ADIABATIC,
         np1,
         np1_inlet,
         np1_np2_node;
         !- ['np1_outlet', 'np1_np2_node'];

    BRANCH,
         sb0,
         0.0,
         ,
         Pipe:Adiabatic,
         np1,
         np1_inlet,
         np1_np2_node,
         Bypass;
    """
    # !- ['np1_outlet', 'np1_np2_node'];
    fhandle = StringIO(idftxt)
    idf = IDF(fhandle)
    pipe = idf.idfobjects["PIPE:ADIABATIC"][0]
    pipe.Outlet_Node_Name = [
        "np1_outlet",
        "np1_np2_node",
    ]  # this is the first step of the replace
    hvacbuilder.renamenodes(idf, fieldtype="node")
    outidf = IDF(StringIO(outtxt))
    result = idf.idfobjects["PIPE:ADIABATIC"][0].obj
    assert result == outidf.idfobjects["PIPE:ADIABATIC"][0].obj

