def test_get_permutations_with_shuffle_is_suffiently_shuffled():
    """
    Catches the case where urls are shuffled one index after the other

    e.g
    LightCalmSloth
    LightCalmFerret
    LightCalmToad
    ...
    """
    test_key = 'aaA' # Use the real dictionaries as the tests ones are too small
    min_count = 70 # If there are less than 70 unique words per position then fail

    beautifurl = Beautifurl()
    shuffled = beautifurl.get_permutations(test_key, shuffle=True)
    urls_as_lists = map(lambda x: re.findall('[A-Z][a-z]+', x), itertools.islice(shuffled, 100))
    words_by_position = defaultdict(list)
    for url in urls_as_lists:
        for (i, word) in enumerate(url):
            words_by_position[i].append(word)

    for i in range(len(test_key)): # Test dictionary in order
        count = len(set(words_by_position[i]))
        assert count > min_count

def test_get_permutations_elements_are_string():
    beautifurl = Beautifurl(dictionary_path='test/dictionaries')
    actual = beautifurl.get_permutations('tat')
    for element in actual:
        assert element.__class__ == str
