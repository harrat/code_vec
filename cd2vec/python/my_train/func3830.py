def test_execute_env_function_json(self, venv):
        """Check parsing a JSON output produced when running a function."""
        assert execute_env_function(venv.python, _func_json, is_json=True, param="bar") == {"foo": "bar"}
