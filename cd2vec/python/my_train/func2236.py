def test_task_updation(todolist_app):
	with todolist_app.test_client() as client:
		modified_title = "Eat spinach from tin"
		resp = client.jput(
			'/tasks/1', {
				"title": modified_title
			}
		)
		assert resp['status'] == 'success'
		assert 'id' in resp['result']
		assert resp['result']['title'] == modified_title
