@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_update_reference_005(snippy):
        """Update reference with ``digest`` option.

        Try to update reference with message digest that cannot be found. No
        changes must be made to stored content.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        cause = snippy.run(['snippy', 'update', '--scat', 'reference', '-d', '123456789abcdef0'])
        assert cause == 'NOK: cannot find content with message digest: 123456789abcdef0'
        Content.assert_storage(content)
