def test_create_random_dirs(self):
        iterative_gaussian_tree(self.basedir.name, -10, 2, 3, None)
        dirs, files = self.get_content()
        self.assertEqual(len(files), 0)
        self.assertGreater(len(dirs), 1)
