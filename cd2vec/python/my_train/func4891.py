def test_observable_object_reserved_property(self):
        observed_data = copy.deepcopy(self.valid_object)
        observed_data['type'] = 'action'
        self.assertFalseWithOptions(observed_data)

        observed_data['type'] = 'file'
        observed_data['action'] = True
        self.assertFalseWithOptions(observed_data)
