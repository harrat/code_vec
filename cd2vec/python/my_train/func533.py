def test_load(self):
        locale.setlocale(locale.LC_ALL, '')
        # file not defined
        gp = parser.GamryParser()
        self.assertRaises(AssertionError, gp.load)
        self.assertEqual(gp.fname, None)

        # file defined, make sure data is loaded properly
        gp = parser.GamryParser()
        gp.load(filename='tests/cv_data.dta')
        self.assertEqual(gp.fname, 'tests/cv_data.dta')

        gp = parser.GamryParser(filename='tests/cv_data.dta')
        gp.load()
        self.assertEqual(gp.curve_count, 5)
        curve1 = gp.curves[0]
        self.assertEqual(curve1['T'].iloc[0], 0.1)
        self.assertEqual(curve1['T'].iloc[-1], 1.0)
        self.assertEqual(curve1.index.dtype, np.int64)

        curve5 = gp.curves[-1]
        self.assertEqual(curve5.index[-1], 49)
        self.assertEqual(curve5['T'].iloc[-1], 601.1)
        self.assertEqual(curve5['Vf'].iloc[-1], 0.889001)
        self.assertEqual(curve5['Im'].iloc[-1], 2.622720e-07)
        self.assertEqual(curve5['Sig'].iloc[-1], 0.890)
        self.assertEqual(curve5['IERange'].iloc[-1], 5)
