def test_write_bytes(self):
        path = self.root.make_file()
        linesep_len = len(os.linesep)
        assert 3 == write_bytes([b'foo'], path)
        assert list(read_bytes(path)) == [b'foo']
        path = self.root.make_file()
        assert 9 + (2*linesep_len) == \
            write_bytes(('foo', 'bar', 'baz'), path, sep=None)
        self.assertEqual(
            os.linesep.encode().join((b'foo', b'bar', b'baz')),
            b''.join(read_bytes(path)))
        path = self.root.make_file(permissions='r')
        assert -1 == write_bytes([b'foo'], path, errors=False)
