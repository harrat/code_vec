@mock.patch('thundra.integrations.botocore.BaseIntegration.actual_call')
def test_kinesis(mock_actual_call, mock_kinesis_response):
    mock_actual_call.return_value = mock_kinesis_response

    region = 'us-west-2'
    shard_id = 'shardId--000000000000'
    sequence_number = '49568167373333333333333333333333333333333333333333333333'

    try:
        kinesis = boto3.client('kinesis', region_name=region)
        kinesis.put_record(
            Data='STRING_VALUE',
            PartitionKey='STRING_VALUE',
            StreamName='STRING_VALUE',
            ExplicitHashKey='STRING_VALUE',
            SequenceNumberForOrdering='STRING_VALUE'
        )
    except botocore_errors:
        pass
    finally:
        tracer = ThundraTracer.get_instance()
        span = tracer.get_spans()[0]
        assert span.class_name == 'AWS-Kinesis'
        assert span.domain_name == 'Stream'
        assert span.get_tag('operation.type') == 'WRITE'
        assert span.get_tag('aws.kinesis.stream.name') == 'STRING_VALUE'
        assert span.get_tag('aws.request.name') == 'PutRecord'
        assert span.get_tag(constants.SpanTags['TRACE_LINKS']) == [
            region + ':' + 'STRING_VALUE:' + shard_id + ':' + sequence_number]
        tracer.clear()
