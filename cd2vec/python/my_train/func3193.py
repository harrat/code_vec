def test_clear_header_overrides(self):
        self.client.set_header_overrides({
            'User-Agent': 'Test_User_Agent_String'
        })
        self.client.clear_header_overrides()
        self._make_request('https://www.stackoverflow.com')

        last_request = self.client.get_last_request()

        self.assertNotEqual(
            'Test_User_Agent_String', last_request['headers']['User-Agent']
        )
