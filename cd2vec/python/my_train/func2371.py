def test_dynamo_db_connect_ci():
    setattr(dynamo_db, 'engine', None)
    os.environ['AWS_REGION'] = 'us-west-99'
    os.environ['CI'] = 'True'
    os.environ.pop('USE_LOCAL_DB')

    class EngineSpy(object):

        def connect(self, *args, **kwargs):
            raise Exception('Should not call this method.')

        def connect_to_region(self, *args, **kwargs):
            assert 'session' in kwargs, 'Session is provided to engine.'

    setattr(dynamo_db, 'Engine', EngineSpy)
    dynamo_db.connect()

