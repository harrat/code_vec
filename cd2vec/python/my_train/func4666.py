def test_read_resource(self):
        r = Resource(DATA_ROOT)
        for t in self.TEST_FILES:
            file = self.TEST_FILES[t]
            path = os.path.join(DATA_ROOT, file)
            ans = ""
            if t == "train":
                ans = r.train_file_path
            elif t == "test":
                ans = r.test_file_path
            elif t == "sample":
                ans = r.sample_file_path
            elif t == "data":
                ans = r.data_file_path
            self.assertEqual(ans, path)
