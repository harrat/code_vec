def test_loads_v1_each_split(self):
        train = Squad(split='train', version=1)
        self.assertEqual(len(train), 87_599)
        dev = Squad(split='dev', version=1)
        self.assertEqual(len(dev), 10_570)
