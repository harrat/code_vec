def test_get_cnn_dailymail_twice(self):
        get_cnn_dailymail()
        with mock.patch('lineflow.datasets.cnn_dailymail.pickle', autospec=True) as \
                mock_pickle:
            get_cnn_dailymail()
        mock_pickle.dump.assert_not_called()
        self.assertEqual(mock_pickle.load.call_count, 1)
