@given(data=st.data(), dataframes=n_time_series_with_same_index(min_n=5))
    def test_fit_predict_top_down_sgf_tree_by_hands(
        self, data, dataframes, time_series_forecasting_model1_no_cache
    ):
        model = data.draw(
            hierarchical_top_down_model_sgf_tree_by_hand(
                time_series_forecasting_model1_no_cache
            )
        )
        model.fit(dataframes).predict()
