@staticmethod
    @pytest.mark.usefixtures('default-references')
    def test_cli_search_reference_005(snippy, capsys):
        """Search references with ``uuid`` option.

        Try to search reference by using resource uuid in short form that
        does not have the last digit. The short form must not be accepted
        and no results must be returned. The UUID is intended to be used
        as fully matching identity.
        """

        output = 'NOK: cannot find content with given search criteria\n'
        cause = snippy.run(['snippy', 'search', '--scat', 'reference', '--uuid', '31cd5827-b6ef-4067-b5ac-3ceac07dde9', '--no-ansi'])
        out, err = capsys.readouterr()
        assert cause == 'NOK: cannot find content with given search criteria'
        assert out == output
        assert not err
