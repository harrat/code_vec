def test_rearrange():
    t = codec.parse('''
        (a / alpha
           :ARG0 (b / beta
                    :ARG0 (g / gamma)
                    :ARG1 (d / delta))
           :ARG0-of d
           :ARG1 (e / epsilon))''')

    rearrange(t, model.original_order)
    assert codec.format(t) == (
        '(a / alpha\n'
        '   :ARG0 (b / beta\n'
        '            :ARG0 (g / gamma)\n'
        '            :ARG1 (d / delta))\n'
        '   :ARG0-of d\n'
        '   :ARG1 (e / epsilon))')

    rearrange(t, model.random_order)
    assert codec.format(t) == (
        '(a / alpha\n'
        '   :ARG0-of d\n'
        '   :ARG1 (e / epsilon)\n'
        '   :ARG0 (b / beta\n'
        '            :ARG0 (g / gamma)\n'
        '            :ARG1 (d / delta)))')

    rearrange(t, model.canonical_order)
    assert codec.format(t) == (
        '(a / alpha\n'
        '   :ARG0 (b / beta\n'
        '            :ARG0 (g / gamma)\n'
        '            :ARG1 (d / delta))\n'
        '   :ARG1 (e / epsilon)\n'
        '   :ARG0-of d)')
