def test_instance_resolved_dependency_is_cleaned_up_when_instance_is_cleaned_up_2(self, basic_configurator, monkey_patcher):
        monkey_patcher()

        container = basic_configurator.get_container()
        dependency = basic_configurator.get_dependency_wrapper()

        d = dependency(lifetime=DependencyLifetimeEnum.INSTANCE)
        @d
        def a():
            return type("A", (), {})()

        class B:
            def __init__(self, a):
                pass

        container.wire_dependencies(B)
        gc.collect()

        if not COLLECTION_EVENT.wait(5):
            raise Exception("collection event was not set")
        assert not d.services
