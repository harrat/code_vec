def test_valid_references():
    references_dict = pl.read_json_data('standard/references.json')
    urls = list(references_dict.keys())
    assert not get_invalid_urls(urls)

