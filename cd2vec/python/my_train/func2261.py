def test_aggregator_scan_iter_method(mocked_aggregated):
    """Test aggregator scan_iter method."""

    for node in mocked_aggregated._aggregator._redis_nodes:
        node.set('value', 'pattern')
    results = [x for x in mocked_aggregated.scan_iter('value')]
    assert results == ['pattern', 'pattern', 'pattern']

