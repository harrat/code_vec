def test_from_url(self):
        url = "https://en.wikipedia.org/wiki/Grace_Hopper"
        r = requests.get(url)
        content = r.text
        soup = bs(content, 'lxml')

        sauce = Sauce.from_url(url)
        self.assertEqual(soup, sauce)
