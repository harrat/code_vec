@staticmethod
    @pytest.mark.usefixtures('default-references', 'import-remove', 'import-beats')
    def test_cli_search_reference_013(snippy, capsys):
        """Search references with ``sall`` option.

        Search content from all fields. The content category is defined with
        ``all`` so the search must result a hit from each category.
        """

        output = (
            '1. Debugging Elastic Beats @beats [4346ba4c79247430]',
            Const.NEWLINE.join(Solution.BEATS_OUTPUT),
            '   : ',
            '',
            '2. Python regular expression @python [cb9225a81eab8ced]',
            '',
            '   > https://www.cheatography.com/davechild/cheat-sheets/regular-expressions/',
            '   > https://pythex.org/',
            '   # howto,online,python,regexp',
            '',
            '3. Remove all docker containers with volumes @docker [54e41e9b52a02b63]',
            '',
            '   $ docker rm --volumes $(docker ps --all --quiet)',
            '',
            '   # cleanup,container,docker,docker-ce,moby',
            '   > https://docs.docker.com/engine/reference/commandline/rm/',
            '',
            'OK',
            ''
        )
        cause = snippy.run(['snippy', 'search', '--sall', 'regexp,docker,beats', '--no-ansi', '--scat', 'all'])
        out, err = capsys.readouterr()
        assert cause == Cause.ALL_OK
        assert out == Const.NEWLINE.join(output)
        assert not err
