def functional_test_file_removed(self):
        git_file, git_dir = get_clean_dir()
        pwstore.git_drop(git_dir.name, git_file.name)
        try:
            assert not os.path.exists(git_file.name)
        finally:
            git_dir.cleanup()
