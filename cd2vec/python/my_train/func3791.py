def test_binary_file_content_restored_on_change(self):

        binary_frozen = os.path.join(self.FROZEN_RESOURCES_PATH, self.EXECUTABLE_BINARY_NAME)
        binary_path = os.path.join(self.RESOURCES_PATH, self.EXECUTABLE_BINARY_NAME)

        self._assert_files_equal(binary_path, binary_frozen)

        @guard(binary_path)
        def function_that_changes_the_file():
            lines_to_write = ['hello, world\n']
            with open(binary_path, 'w') as file:
                file.writelines(lines_to_write)

            self._assert_file_content_equals(binary_path, lines_to_write)

        function_that_changes_the_file()
        self._assert_files_equal(binary_path, binary_frozen)
