def test_flex_alloc_valid_constraint_partition(make_flexible_job):
    job = make_flexible_job('all')
    job.options = ['-C f1&f2', '--partition=p1,p2']
    prepare_job(job)
    assert job.num_tasks == 4

