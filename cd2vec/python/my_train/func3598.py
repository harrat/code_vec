def test_run_with_policy_overloading(self):
        """
        Test that the PolicyDirectoryMonitor can load policy files and track
        them properly, even when one policy overloads another existing policy.
        """
        m = monitor.PolicyDirectoryMonitor(
            self.tmp_dir,
            multiprocessing.Manager().dict()
        )
        m.logger = mock.MagicMock(logging.Logger)
        m.halt_trigger = mock.MagicMock(multiprocessing.synchronize.Event)
        m.halt_trigger.is_set.side_effect = [False, True]

        write_file(self.tmp_dir, "policy_1.json", POLICY_1)
        write_file(self.tmp_dir, "policy_2.json", POLICY_2)
        write_file(self.tmp_dir, "policy_3.json", POLICY_3)

        self.assertEqual({}, m.file_timestamps)
        self.assertEqual({}, m.policy_cache)
        self.assertEqual([], m.policy_files)
        self.assertEqual({}, m.policy_map)
        self.assertEqual([], m.policy_store.keys())

        m.run()

        m.logger.info.assert_any_call(
            "Starting up the operation policy file monitor."
        )
        m.logger.info.assert_any_call(
            "Loading policies for file: {}".format(
                os.path.join(self.tmp_dir, "policy_1.json")
            )
        )
        m.logger.info.assert_any_call("Loading policy: policy_A")
        m.logger.info.assert_any_call(
            "Loading policies for file: {}".format(
                os.path.join(self.tmp_dir, "policy_2.json")
            )
        )
        m.logger.info.assert_any_call("Loading policy: policy_B")
        m.logger.info.assert_any_call("Loading policy: policy_C")
        m.logger.info.assert_any_call(
            "Loading policies for file: {}".format(
                os.path.join(self.tmp_dir, "policy_3.json")
            )
        )
        m.logger.info.assert_any_call("Loading policy: policy_B")
        m.logger.debug.assert_any_call(
            "Policy 'policy_B' overwrites an existing policy."
        )
        m.logger.info.assert_any_call(
            "Stopping the operation policy file monitor."
        )

        self.assertEqual(3, len(m.policy_files))
        path = os.path.join(self.tmp_dir, "policy_1.json")
        self.assertEqual(
            os.path.getmtime(path),
            m.file_timestamps.get(path, None)
        )
        self.assertIn(path, m.policy_files)
        self.assertEqual(path, m.policy_map.get("policy_A", None))

        path = os.path.join(self.tmp_dir, "policy_2.json")
        self.assertEqual(
            os.path.getmtime(path),
            m.file_timestamps.get(path, None)
        )
        self.assertIn(path, m.policy_files)
        self.assertEqual(path, m.policy_map.get("policy_C", None))

        path = os.path.join(self.tmp_dir, "policy_3.json")
        self.assertEqual(
            os.path.getmtime(path),
            m.file_timestamps.get(path, None)
        )
        self.assertIn(path, m.policy_files)
        self.assertEqual(path, m.policy_map.get("policy_B", None))

        cache = m.policy_cache.get("policy_A")
        self.assertEqual(0, len(cache))
        cache = m.policy_cache.get("policy_B")
        self.assertEqual(1, len(cache))
        self.assertEqual(
            os.path.join(self.tmp_dir, "policy_2.json"),
            cache[0][1]
        )
        self.assertEqual(
            {
                'groups': {
                    'group_B': {
                        enums.ObjectType.SYMMETRIC_KEY: {
                            enums.Operation.GET:
                                enums.Policy.ALLOW_ALL,
                            enums.Operation.LOCATE:
                                enums.Policy.ALLOW_ALL,
                            enums.Operation.DESTROY:
                                enums.Policy.ALLOW_ALL
                        }
                    }
                }
            },
            cache[0][2]
        )

        self.assertEqual(3, len(m.policy_store.keys()))
        self.assertEqual(
            {
                "groups": {
                    "group_A": {
                        enums.ObjectType.SYMMETRIC_KEY: {
                            enums.Operation.GET: enums.Policy.ALLOW_ALL,
                            enums.Operation.DESTROY: enums.Policy.ALLOW_ALL
                        }
                    }
                }
            },
            m.policy_store.get("policy_A", None)
        )
        self.assertEqual(
            {
                "groups": {
                    "group_B": {
                        enums.ObjectType.SYMMETRIC_KEY: {
                            enums.Operation.GET: enums.Policy.DISALLOW_ALL,
                            enums.Operation.LOCATE: enums.Policy.DISALLOW_ALL,
                            enums.Operation.DESTROY: enums.Policy.DISALLOW_ALL
                        }
                    }
                }
            },
            m.policy_store.get("policy_B", None)
        )
        self.assertEqual(
            {
                "groups": {
                    "group_C": {
                        enums.ObjectType.SYMMETRIC_KEY: {
                            enums.Operation.GET: enums.Policy.ALLOW_ALL,
                            enums.Operation.DESTROY: enums.Policy.DISALLOW_ALL
                        }
                    }
                }
            },
            m.policy_store.get("policy_C", None)
        )
