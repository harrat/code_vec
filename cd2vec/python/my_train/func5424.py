def test_hosts_file_format(self):
        """
        Tests that the hosts file format is always respected.
        """

        given = [
            "0.0.0.0 b?llogram.com",
            "127.0.0.1 bittr�?.com",
            "0.0.0.0 cryptopi?.com",
            "127.0.0.1 coinb?se.com",
            "0.0.0.0 c?yptopia.com",
        ]

        expected = [
            "0.0.0.0 xn--bllogram-g80d.com",
            "127.0.0.1 xn--bittr-fsa6124c.com",
            "0.0.0.0 xn--cryptopi-ux0d.com",
            "127.0.0.1 xn--coinbse-30c.com",
            "0.0.0.0 xn--cyptopia-4e0d.com",
        ]

        actual = Converter(given).get_converted()

        self.assertEqual(expected, actual)

        given = "0.0.0.0 b?llogram.com"

        expected = "0.0.0.0 xn--bllogram-g80d.com"

        actual = Converter(given).get_converted()

        self.assertEqual(expected, actual)
