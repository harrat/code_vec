def test_getobjectswithnode():
    """py.test for getobjectswithnode"""
    idf = IDF(StringIO(""))
    nodekeys = idf_helpers.getidfkeyswithnodes()
    plantloop = idf.newidfobject(
        "PlantLoop",
        Name="Chilled Water Loop",
        Plant_Side_Inlet_Node_Name="CW Supply Inlet Node",
    )
    branch = idf.newidfobject(
        "Branch",
        Name="CW Pump Branch",
        Component_1_Inlet_Node_Name="CW Supply Inlet Node",
    )
    pump = idf.newidfobject(
        "Pump:VariableSpeed",
        Name="CW Circ Pump",
        Inlet_Node_Name="CW Supply Inlet Node",
    )
    zone = idf.newidfobject("zone")
    foundobjs = idf_helpers.getobjectswithnode(idf, nodekeys, "CW Supply Inlet Node")
    expected = [plantloop, branch, pump]
    expectedset = set([item.key for item in expected])
    resultset = set([item.key for item in foundobjs])
    assert resultset == expectedset
    expectedset = set([item.Name for item in expected])
    resultset = set([item.Name for item in foundobjs])
    assert resultset == expectedset

