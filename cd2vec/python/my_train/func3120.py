def test_mkdir_p_double(tmpdir):
    """Test mkdir_p on creating two directories."""
    path1 = tmpdir.join('newdir2')
    assert not path1.exists()
    path2 = path1.join('subdir')
    assert not path2.exists()
    mkdir_p(path2.strpath)
    assert path1.exists()
    assert path2.exists()

