def test_implements_no_implementation_deleter():
    """
    Case: do not implement interface member that is deleter.
    Expect: class does not implement interface member error message.
    """
    class HumanNameInterface:

        @property
        def name(self):
            return

        @name.deleter
        def name(self):
            return

    with pytest.raises(InterfaceMemberHasNotBeenImplementedException) as error:

        @implements(HumanNameInterface)
        class HumanWithoutImplementation:

            @property
            def name(self):
                return

    assert 'class HumanWithoutImplementation does not implement ' \
           'interface member HumanNameInterface.name(self)' == error.value.message
