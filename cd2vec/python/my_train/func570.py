def test_show_project_id(cli, projects_db):
    output = cli('show', ['42'])
    assert output == ("Your search string 42 is the project not "
                      "started project.\n")
