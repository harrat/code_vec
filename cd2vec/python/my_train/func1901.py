@pytest.mark.local
def test_main():
    from notebook.notebookapp import NotebookApp
    with mock.patch.object(NotebookApp, 'start', lambda self: None):
        pexnb.main([])
    app = NotebookApp.instance()
    NotebookApp.clear_instance()
    assert isinstance(app.kernel_spec_manager, pexnb.PEXKernelSpecManager)

