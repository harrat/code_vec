@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_nfl_olb_returns_requested_career_stats(self, *args, **kwargs):
        # Request the career stats
        player = Player('DaviDe00')
        player = player('')

        for attribute, value in self.olb_results_career.items():
            assert getattr(player, attribute) == value
