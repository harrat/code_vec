@staticmethod
    @pytest.mark.usefixtures('default-references', 'import-pytest')
    def test_api_create_reference_006(server):
        """Update Reference resource with POST that maps to DELETE.

        Send POST /references with the ``X-HTTP-Method-Override`` header to
        delete a resource. In this case the resource exists and the content is
        deleted.
        """

        storage = {
            'data': [
                Storage.gitlog,
                Storage.regexp
            ]
        }
        expect_headers = {}
        result = testing.TestClient(server.server.api).simulate_post(
            path='/api/snippy/rest/references/1f9d9496005736ef',
            headers={'accept': 'application/json', 'X-HTTP-Method-Override': 'DELETE'})
        assert result.status == falcon.HTTP_204
        assert result.headers == expect_headers
        assert not result.text
        Content.assert_storage(storage)
