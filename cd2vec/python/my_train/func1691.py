def test_calc_v_air(class_objects, params_objects):
    d_source, d_class, s_width = class_objects
    obj = CalculateProducts(d_source, params_objects)
    d_v = np.array([[2.0, 2.0, 4.0], [1.0, 3.0, 5.0]])
    obj.ind_drizzle = (np.array([0, 1]), np.array([1, 2]))
    compare = np.array([[-2.0, 0.0, -4.0],
                        [-1.0, -3.0, -2.0]])
    testing.assert_array_almost_equal(obj._calc_v_air(d_v), compare)

