def test_movie_cast_attr(person: Person):
    assert isinstance(person.movie_cast, list)
    movie, credit = person.movie_cast[0]
    assert isinstance(movie, Movie)
    assert isinstance(credit, Credit)

