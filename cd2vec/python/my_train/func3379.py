@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_delete_solution_007(snippy):
        """Delete solution with digest.

        Try to delete solution with empty message digest. Nothing should be
        deleted in this case because there is more than one content stored.
        """

        content = {
            'data': [
                Solution.BEATS,
                Solution.NGINX
            ]
        }
        cause = snippy.run(['snippy', 'delete', '--scat', 'solution', '-d', ''])
        assert cause == 'NOK: cannot use empty message digest for delete operation'
        Content.assert_storage(content)
