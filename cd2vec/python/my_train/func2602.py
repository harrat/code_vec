@staticmethod
    def test_zones():
        fr = flightradar24.Api()
        zones = fr.get_zones()
        assert zones['europe'] is not None
        check_uk = 0
        for subzone_name, subzone_details in zones['europe']['subzones'].items():
            if subzone_name == 'uk':
                check_uk = 1
        assert check_uk == 1
