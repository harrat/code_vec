def testFindTopLevelBuildConfDir(tmpdir):

    startdir = str(tmpdir.realpath())
    assert starter.findTopLevelBuildConfDir(startdir) is None

    dir1 = tmpdir.mkdir("dir1")
    dir2 = dir1.mkdir("dir2")
    dir3 = dir2.mkdir("dir3")
    dir4 = dir3.mkdir("dir4")

    assert starter.findTopLevelBuildConfDir(str(dir4)) is None

    buildconf = joinpath(str(dir4), 'buildconf.py')
    with open(buildconf, 'w+') as file:
        file.write("buildconf")
    assert starter.findTopLevelBuildConfDir(str(dir4)) == str(dir4)
    assert starter.findTopLevelBuildConfDir(str(dir3)) is None

    buildconf = joinpath(str(dir3), 'buildconf.yaml')
    with open(buildconf, 'w+') as file:
        file.write("buildconf")
    assert starter.findTopLevelBuildConfDir(str(dir4)) == str(dir3)
    assert starter.findTopLevelBuildConfDir(str(dir3)) == str(dir3)
    assert starter.findTopLevelBuildConfDir(str(dir2)) is None

    buildconf = joinpath(str(dir1), 'buildconf.yml')
    with open(buildconf, 'w+') as file:
        file.write("buildconf")
    assert starter.findTopLevelBuildConfDir(str(dir4)) == str(dir1)
    assert starter.findTopLevelBuildConfDir(str(dir3)) == str(dir1)
    assert starter.findTopLevelBuildConfDir(str(dir2)) == str(dir1)
    assert starter.findTopLevelBuildConfDir(str(dir1)) == str(dir1)

