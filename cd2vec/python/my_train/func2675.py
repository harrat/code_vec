def test_insert(mi, emp_seq):
    assert emp_seq[3] == mi.get('first_name', 'Shawn')
    assert emp_seq[0] == mi.get('emp_id', 786)
    assert mi.get('emp_id', 321) is None
    assert emp_seq[0] == mi.get('first_name', 'Steve')
    assert emp_seq[1] == mi.get('last_name', 'Hart')
    assert emp_seq[2] == mi.get('last_name', 'Ramon')

