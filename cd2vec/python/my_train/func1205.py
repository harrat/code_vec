def test_e2e_scenario_4(self):
        """
        Testing for free, price that excluded from scenario 1~4
        """
        res = app("com.choco.silentmode")

        self.assertFalse(res["free"])
        self.assertEqual(1.49, res["price"])
