def test_get_action(planner):
    # pylint: disable=protected-access
    env = rddlgym.make(planner.rddl, mode=rddlgym.GYM)

    with tf.Session(graph=planner.compiler.graph) as sess:
        sess.run(tf.global_variables_initializer())
        state = env.observation_space.sample()
        batch_state = planner._get_batch_initial_state(state)
        samples = utils.evaluate_noise_samples_as_inputs(
            sess, planner.simulator.samples
        )
        feed_dict = {
            planner.initial_state: batch_state,
            planner.simulator.noise: samples,
            planner.steps_to_go: HORIZON,
        }
        actions_ = planner._get_action(planner.trajectory.actions, feed_dict)
        action_fluents = planner.compiler.default_action_fluents
        assert isinstance(actions_, OrderedDict)
        assert len(actions_) == len(action_fluents)
        for action_, action_fluent in zip(actions_.values(), action_fluents):
            assert tf.dtypes.as_dtype(action_.dtype) == action_fluent[1].dtype
            assert list(action_.shape) == list(action_fluent[1].shape.fluent_shape)
