@staticmethod
    @pytest.mark.usefixtures('import-forced', 'caller')
    def test_api_update_snippet_005(server):
        """Try to update snippet with malformed request.

        Try to send PUT /snippets/{id} to update snippet with malformed
        JSON request.
        """

        storage = {
            'data': [
                Storage.forced
            ]
        }
        request_body = {
            'data': Const.NEWLINE.join(Request.remove['data']),
            'brief': Request.remove['brief'],
            'groups': Request.remove['groups'],
            'tags': Const.DELIMITER_TAGS.join(Request.remove['tags']),
            'links': Const.DELIMITER_LINKS.join(Request.remove['links'])
        }
        expect_headers_p3 = {'content-type': 'application/vnd.api+json; charset=UTF-8', 'content-length': '1328'}
        expect_headers_p2 = {'content-type': 'application/vnd.api+json; charset=UTF-8', 'content-length': '1371'}
        expect_body = {
            'meta': Content.get_api_meta(),
            'errors': [{
                'status': '400',
                'statusString': '400 Bad Request',
                'module': 'snippy.testing.testing:123',
                'title': 'json media validation failed'
            }]
        }
        result = testing.TestClient(server.server.api).simulate_put(
            path='/api/snippy/rest/snippets/53908d68425c61dc',
            headers={'accept': 'application/json'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_400
        assert result.headers in (expect_headers_p2, expect_headers_p3)
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
