def test_successful_http_call():
    try:
        url = 'https://jsonplaceholder.typicode.com/users'
        parsed_url = urlparse(url)
        path = parsed_url.path
        query = parsed_url.query
        host = parsed_url.netloc
