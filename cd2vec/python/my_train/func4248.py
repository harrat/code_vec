@pytest.mark.parametrize("distribution_type", ('pure', 'skbuild'))
def test_script_keyword(distribution_type, capsys):

    # -------------------------------------------------------------------------
    #
    # "SOURCE" tree layout for "pure" distribution:
    #
    # ROOT/
    #     setup.py
    #     foo.py
    #     bar.py
    #
    # "SOURCE" tree layout for "pure" distribution:
    #
    # ROOT/
    #     setup.py
    #     CMakeLists.txt
    #
    # -------------------------------------------------------------------------
    # "BINARY" distribution layout is identical for both
    #
    # ROOT/
    #     foo.py
    #     bar.py
    #

    tmp_dir = _tmpdir('script_keyword')

    tmp_dir.join('setup.py').write(textwrap.dedent(
        """
        from skbuild import setup
        setup(
            name="test_script_keyword",
            version="1.2.3",
            description="a package testing use of script keyword",
            author='The scikit-build team',
            license="MIT",
            scripts=['foo.py', 'bar.py']
        )
        """
    ))

    if distribution_type == 'skbuild':
        tmp_dir.join('CMakeLists.txt').write(textwrap.dedent(
            """
            cmake_minimum_required(VERSION 3.5.0)
            project(foo NONE)
            file(WRITE "${CMAKE_BINARY_DIR}/foo.py" "# foo.py")
            file(WRITE "${CMAKE_BINARY_DIR}/bar.py" "# bar.py")
            install(
                FILES
                    "${CMAKE_BINARY_DIR}/foo.py"
                    "${CMAKE_BINARY_DIR}/bar.py"
                DESTINATION "."
                )
            """
        ))

        messages = [
            "copying {}/{}.py -> "
            "{}/setuptools/scripts-".format(CMAKE_INSTALL_DIR(), module, SKBUILD_DIR())
            for module in ['foo', 'bar']]

    elif distribution_type == 'pure':
        tmp_dir.join('foo.py').write("# foo.py")
        tmp_dir.join('bar.py').write("# bar.py")

        messages = [
            "copying {}.py -> "
            "{}/setuptools/scripts-".format(module, SKBUILD_DIR())
            for module in ['foo', 'bar']]

    with execute_setup_py(tmp_dir, ['build'], disable_languages_test=True):
        pass

    out, _ = capsys.readouterr()
    for message in messages:
        assert to_platform_path(message) in out

