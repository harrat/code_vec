def test_signature_verify_inactive_signing_key(self):
        """
        Test that the right error is thrown when an inactive signing key
        is specified with a SignatureVerify request.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()
        e._cryptography_engine.logger = mock.MagicMock()

        signing_key = pie_objects.PublicKey(
            enums.CryptographicAlgorithm.RSA,
            1120,
            (
                b'\x30\x81\x89\x02\x81\x81\x00\xac\x13\xd9\xfd\xae\x7b\x73\x35'
                b'\xb6\x9c\xd9\x85\x67\xe9\x64\x7d\x99\xbf\x37\x3a\x9e\x05\xce'
                b'\x34\x35\xd6\x64\x65\xf3\x28\xb7\xf7\x33\x4b\x79\x2a\xee\x7e'
                b'\xfa\x04\x4e\xbc\x4c\x7a\x30\xb2\x1a\x5d\x7a\x89\xcd\xb3\xa3'
                b'\x0d\xfc\xd9\xfe\xe9\x99\x5e\x09\x41\x5e\xdc\x0b\xf9\xe5\xb4'
                b'\xc3\xf7\x4f\xf5\x3f\xb4\xd2\x94\x41\xbf\x1b\x7e\xd6\xcb\xdd'
                b'\x4a\x47\xf9\x25\x22\x69\xe1\x64\x6f\x6c\x1a\xee\x05\x14\xe9'
                b'\x3f\x6c\xb9\xdf\x71\xd0\x6c\x06\x0a\x21\x04\xb4\x7b\x72\x60'
                b'\xac\x37\xc1\x06\x86\x1d\xc7\x8c\xa5\xa2\x5f\xaa\x9c\xb2\xe3'
                b'\x02\x03\x01\x00\x01'
            ),
            masks=[
                enums.CryptographicUsageMask.SIGN,
                enums.CryptographicUsageMask.VERIFY
            ]
        )

        e._data_session.add(signing_key)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        unique_identifier = str(signing_key.unique_identifier)
        payload = payloads.SignatureVerifyRequestPayload(
            unique_identifier=unique_identifier,
            cryptographic_parameters=attributes.CryptographicParameters(
                padding_method=enums.PaddingMethod.PSS,
                digital_signature_algorithm=enums.DigitalSignatureAlgorithm.
                SHA1_WITH_RSA_ENCRYPTION
            ),
            data=b'',
            signature_data=b''
        )

        args = (payload,)
        self.assertRaisesRegex(
            exceptions.PermissionDenied,
            "The signing key must be in the Active state to be used for "
            "signature verification.",
            e._process_signature_verify,
            *args
        )
