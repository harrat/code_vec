@staticmethod
    @pytest.mark.usefixtures('default-solutions', 'export-time')
    def test_cli_export_solution_016(snippy):
        """Export defined solution with digest.

        Export defined solution based on message digest. File name is defined
        in solution metadata and in command line -f|--file option. This should
        result the file name and format defined by the command line option. In
        this case the text format file extension is 'text'.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Content.deepcopy(Solution.BEATS)
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '--scat', 'solution', '-d', '4346ba4c79247430', '-f', './defined-solution.text'])
            assert cause == Cause.ALL_OK
            Content.assert_text(mock_file, './defined-solution.text', content)
