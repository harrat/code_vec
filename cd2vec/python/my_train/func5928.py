def test_fetch_stats(self):
        """
        Test that we're getting stats back and they're the
        correct type
        """
        assert isinstance(self.owlsim2_api.statistics.mean_mean_ic, float)
        assert isinstance(self.owlsim2_api.statistics.individual_count, int)
