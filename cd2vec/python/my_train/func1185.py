def test_get_pulsars(query):
    """
    Test the 'Pulsars' class.
    """

    psrs = query.get_pulsars()

    assert len(psrs) == query.num_pulsars

    # check Crab frequency
    f01 = query.get_pulsar('J0534+2200')['F0'][0]
    f02 = psrs['J0534+2200'].F0  # frequency attribute

    assert f01 == f02

    # test removing a pulsar
    crab = psrs.pop('J0534+2200')

    f03 = crab.F0
    assert f03 == f01
    assert len(psrs) == (query.num_pulsars - 1)

    # get the ephemeris string for the Crab pulsar
    crabeph = query.get_ephemeris('J0534+2200AB')  # wrong name
    assert crabeph is None

    crabeph = query.get_ephemeris('J0534+2200')

    assert isinstance(crabeph, string_types)

    for line in crabeph.split('\n'):
        if line.split()[0].strip() == 'F0':
            f0str = line.split()[1].strip()
            break

    assert f01 == float(f0str)

