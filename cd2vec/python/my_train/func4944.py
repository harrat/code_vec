@pytest.mark.vcr()
def test_scan_export_file_object(api):
    from tenable.reports.nessusv2 import NessusReportv2
    fn = '{}.nessus'.format(uuid.uuid4())
    with open(fn, 'wb') as fobj:
        api.scans.export(SCAN_ID_WITH_RESULTS, fobj=fobj)

    with open(fn, 'rb') as fobj:
        counter = 0
        for i in NessusReportv2(fobj):
            counter += 1
            if counter > 10:
                break
    os.remove(fn)

#@pytest.mark.vcr()
#def test_scan_host_details_scan_id_typeerror(api):
#    with pytest.raises(TypeError):
#        api.scans.host_details('nope', 1)
