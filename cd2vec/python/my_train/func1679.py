@pytest.mark.core
def testConfigLoad():
    assert theConf.loadConfig()
    assert not theConf.confChanged

@pytest.mark.core
def testConfigSetWinSize(nwTemp,nwRef):
    tmpConf = path.join(nwTemp,"novelwriter.conf")
    refConf = path.join(nwRef, "novelwriter.conf")
    assert theConf.setWinSize(1105, 655)
    assert not theConf.confChanged
    assert theConf.setWinSize(70,70)
    assert theConf.confChanged
    assert theConf.setWinSize(1100, 650)
    assert theConf.saveConfig()
    assert cmpFiles(tmpConf, refConf, [2])
    assert not theConf.confChanged
