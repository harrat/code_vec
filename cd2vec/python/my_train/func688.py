def test_long_period_of_inactivity_should_not_corrupt_sampling_state(self):
        twisted_clock = Clock()
        sample = ExponentiallyDecayingSample(10, 0.015, twisted_clock.seconds)
        for i in xrange(1000):
            sample.update(1000 + i)
            twisted_clock.advance(0.1)

        self.assertTrue(sample.get_snapshot().size() == 10)
        self._assert_all_values_between(sample, 1000, 2000)

        twisted_clock.advance(15*3600)
        sample.update(2000)
        self.assertTrue(sample.get_snapshot().size() == 2)
        self._assert_all_values_between(sample, 1000, 3000)

        for i in xrange(1000):
            sample.update(3000 + i)
            twisted_clock.advance(0.1)

        self.assertTrue(sample.get_snapshot().size() == 10)
        self._assert_all_values_between(sample, 3000, 4000)
