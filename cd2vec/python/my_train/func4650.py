def test_methods():
    ds1 = NumpyDataset(ar)
    ds1.__repr__()
    assert ds1.__str__() == "[(1, 1) (2, 2) (3, 3)]"

    assert ds1.nentries == 3
    assert ds1.nevents  == 3

    assert ds1.variables == ["x", "y"]
    assert ds1.keys()    == ["x", "y"]

    ds1.to_file('npds.npz')
    ds2 = NumpyDataset.from_file('npds.npz')
    assert ds2.provenance[0].__repr__() == '<FileOrigin (1 file)>'
    os.remove('npds.npz')
    with pytest.raises(IOError):
        ds = NumpyDataset.from_file('non_existent_file')
    ds3 = ds1.copy()

    assert ds1['x'].tolist() == [1,2,3]
    assert ds1.x.tolist() == [1,2,3]
    assert ds1.x.name == "x"
    assert ds1.x.provenance[0].detail == repr(ds1)

    ds1.z = np.ones((3,))
    assert ds1['z'].tolist() == [1,1,1]
    assert ds1.z.tolist() == [1,1,1]
    assert ds1.provenance[-1].__repr__() == "<Transformation(Array z has been created)>"

    ds1['w'] = np.zeros((3,))
    assert ds1.provenance[-1].__repr__() == "<Transformation(Array w has been created)>"
    assert ds1['w'].tolist() == [0,0,0]
    assert ds1.w.tolist() == [0,0,0]

    with pytest.raises(ValueError):
        ds1.__setitem__('h',np.ones((4,)))
    with pytest.raises(ValueError):
        ds1.__setitem__('k',"array")

    ds1.z = ds1.w
    assert ds1.provenance[-1].__repr__() =="<Transformation(Array z has been replaced by w)>"
    ds1.w = np.array([6,7,8])
    assert ds1.provenance[-1].__repr__() =="<Transformation(Array w has been replaced by array([6, 7, 8]))>"

