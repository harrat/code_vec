@staticmethod
    @pytest.mark.usefixtures('snippy', 'default-snippets')
    def test_debug_print_001(capsys):
        """Test printing the content.

        Test printing content with print. This is a development test which
        must directly print the snippets with the test case helper method.
        """

        output = (
            '1. Remove all docker containers with volumes @docker [54e41e9b52a02b63]',
            '',
            '   $ docker rm --volumes $(docker ps --all --quiet)',
            '',
            '   # cleanup,container,docker,docker-ce,moby',
            '   > https://docs.docker.com/engine/reference/commandline/rm/',
            '',
            '   ! category    : snippet',
            '   ! created     : 2017-10-14T19:56:31.000001+00:00',
            '   ! description : ',
            '   ! digest      : 54e41e9b52a02b631b5c65a6a053fcbabc77ccd42b02c64fdfbc76efdb18e319 (True)',
            '   ! filename    : ',
            '   ! id          : a1cd5827-b6ef-4067-b5ac-3ceac07dde9f',
            '   ! languages   : ',
            '   ! name        : ',
            '   ! source      : ',
            '   ! updated     : 2017-10-14T19:56:31.000001+00:00',
            '   ! uuid        : 11cd5827-b6ef-4067-b5ac-3ceac07dde9f',
            '   ! versions    : ',
            '',
            '2. Remove docker image with force @docker [53908d68425c61dc]',
            '',
            '   $ docker rm --force redis',
            '',
            '   # cleanup,container,docker,docker-ce,moby',
            '   > https://docs.docker.com/engine/reference/commandline/rm/',
            '   > https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes',
            '',
            '   ! category    : snippet',
            '   ! created     : 2017-10-14T19:56:31.000001+00:00',
            '   ! description : ',
            '   ! digest      : 53908d68425c61dc310c9ce49d530bd858c5be197990491ca20dbe888e6deac5 (True)',
            '   ! filename    : ',
            '   ! id          : a2cd5827-b6ef-4067-b5ac-3ceac07dde9f',
            '   ! languages   : ',
            '   ! name        : ',
            '   ! source      : ',
            '   ! updated     : 2017-10-14T19:56:31.000001+00:00',
            '   ! uuid        : 12cd5827-b6ef-4067-b5ac-3ceac07dde9f',
            '   ! versions    : ',
            '',
            '# collection meta',
            '   ! total : 2'
        )
        print(Content.output())  # Part of the test.
        out, err = capsys.readouterr()
        out = Helper.remove_ansi(out)
        assert Const.NEWLINE.join(output) in out
        assert not err
