def test_dispatch__error_with_debug_enabled(self):
        def callback(request):
            raise ValueError()

        target = containers.ApiInterfaceBase(debug_enabled=True)
        operation = Operation(callback)

        with pytest.raises(ValueError):
            target.dispatch(operation, MockRequest())
