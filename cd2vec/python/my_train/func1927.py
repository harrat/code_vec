def test_all_callbacks():
    """Assert that all callbacks pre-defined callbacks work as intended."""
    atom = ATOMClassifier(X_bin, y_bin, random_state=1)
    atom.run('LR', n_calls=5, bo_params={'max_time': 50, 'delta_x': 5, 'delta_y': 5})
    assert not atom.errors

