def test_patch_new_resource_html(self):
        """Send HTTP PATCH for a resource which doesn't exist (should be
        created)."""
        response = self.app.patch('/artists/276',
                data={'Name': 'Jeff Knupp'},
                headers={'Accept': 'text/html'})
        assert response.status_code == 201
        assert self.is_html_response(response)
