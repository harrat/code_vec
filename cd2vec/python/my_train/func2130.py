@pytest.mark.parametrize('key, val, out, exception', [
		(Raw('f=1'), None, 'f=1', None),
		('aiw@#$', None, None, WhereParseError),
		('!s.t.f[!] # comments', 1, 'NOT "s"."t"."f" <> 1', None),
		('f|count[>]', 1, 'COUNT("f") > 1', None),
	])
	def testTerm(self, key, val, out, exception):
		if exception:
			with pytest.raises(exception):
				str(WhereTerm(key, val))
		else:
			assert str(WhereTerm(key, val)) == out
