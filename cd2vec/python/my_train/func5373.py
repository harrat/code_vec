def test_custom_dimensions():
    """Assert that the pipeline works with custom dimensions."""
    dim = [Integer(100, 1000, name='max_iter')]

    atom = ATOMClassifier(X_bin, y_bin, random_state=1)
    atom.run('LR', n_calls=5, bo_params={'dimensions': dim})
    assert list(atom.lr.best_params.keys()) == ['max_iter']

