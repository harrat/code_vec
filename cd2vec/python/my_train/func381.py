def test_geweke_index():
    data = np.vstack((np.random.normal(loc=0.0, size=100000),
                      np.random.normal(loc=1.0, size=100000))).T
    consumer = ChainConsumer()
    consumer.add_chain(data, walkers=20, name="c1")
    assert consumer.diagnostic.geweke(chain=0)

