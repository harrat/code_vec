def test_MVN_MLE_truncated():
    """
    Test if the max. likelihood estimates of a multivariate normal distribution
    are sufficiently accurate in cases with truncation and uncensored data.

    """
    # univariate case
    ref_mean = 0.5
    ref_std = 0.25
    ref_var = ref_std ** 2.
    tr_lower = 0.35
    tr_upper = 1.25

    # generate samples of a TMVN distribution
    # (assume the tmvn_rvs function works properly)
    samples = tmvn_rvs(ref_mean, ref_var, lower=tr_lower, upper=tr_upper,
                       size=1000)

    # estimate the parameters of the distribution
    mu, var = tmvn_MLE(samples, tr_lower=tr_lower, tr_upper=tr_upper)

    assert ref_mean == pytest.approx(mu, abs=0.1)
    assert ref_var == pytest.approx(var, rel=0.3)

    # multi-dimensional case
    dims = 3
    ref_mean = np.arange(dims, dtype=np.float64)
    ref_std = np.ones(dims) * 0.25
    ref_rho = np.ones((dims, dims)) * 0.5
    np.fill_diagonal(ref_rho, 1.0)
    ref_COV = np.outer(ref_std, ref_std) * ref_rho

    tr_lower = ref_mean - 1.5 * ref_std
    tr_upper = ref_mean + 8.5 * ref_std

    tr_lower[2] = -np.inf
    tr_upper[0] = np.inf

    samples = tmvn_rvs(ref_mean, ref_COV,
                       lower=tr_lower, upper=tr_upper,
                       size=500)

    test_mu, test_COV = tmvn_MLE(np.transpose(samples),
                                 tr_lower=tr_lower, tr_upper=tr_upper,)
    test_std = np.sqrt(test_COV.diagonal())
    test_rho = test_COV / np.outer(test_std, test_std)

    assert_allclose(test_mu, ref_mean, atol=0.15)
    assert_allclose(test_std ** 2., ref_std ** 2., rtol=0.5)
    assert_allclose(test_rho, ref_rho, atol=0.4)

def test_MVN_MLE_truncated_and_censored():
    """
    Test if the max. likelihood estimates of a multivariate normal distribution
    are sufficiently accurate in cases with truncation and censored data.
