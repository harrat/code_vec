def test_set_new_attribute_gets_overridden(config, mock_env, sentinel):
    mock_env({'ATTRIBUTE_NEW': 'hello attr'})
    config.ATTRIBUTE_NEW = sentinel
    assert config.ATTRIBUTE_NEW == 'hello attr'

