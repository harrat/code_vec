def test_parse_runtimes(self) -> None:
        config = self.load_config("custom_runtimes.yml")

        runtimes = config.runtimes()
        expected_layout = {"rust": 0, "example": 2, "other_example": 42}
        self.assertEqual(runtimes, expected_layout)

        self.assertTrue("cryptocurrency" in config.artifacts)
        self.assertTrue("other_cryptocurrency" in config.artifacts)
        self.assertEqual(config.artifacts["cryptocurrency"].runtime, "example")
        self.assertEqual(config.artifacts["cryptocurrency"].runtime_id, 2)
        self.assertEqual(config.artifacts["other_cryptocurrency"].runtime, "other_example")
        self.assertEqual(config.artifacts["other_cryptocurrency"].runtime_id, 42)
