def test_apify_init(app, apify):
    assert 'apify' in app.extensions
    assert apify.blueprint is not None
    assert apify.logger is logging.getLogger('flask-apify')
    assert apify.preprocessor_funcs == [set_best_serializer,]
    assert apify.postprocessor_funcs == []
    assert apify.finalizer_funcs == []
    assert apify.serializers['text/html'] is to_html
    assert apify.serializers['application/json'] is to_json
    assert apify.serializers['application/javascript'] is to_javascript
    assert apify.serializers['application/json-p'] is to_javascript
    assert apify.serializers['text/json-p'] is to_javascript

