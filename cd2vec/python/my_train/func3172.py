def test_report_file_with_sessionid(run_reframe, tmp_path):
    returncode, stdout, _ = run_reframe(
        more_options=[
            f'--save-log-files',
            f'--report-file={tmp_path / "rfm-report-{sessionid}.json"}'
        ]
    )
    assert returncode == 0
    assert os.path.exists(tmp_path / 'rfm-report-0.json')

