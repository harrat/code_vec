def test_homepage_attr(person: Person):
    assert isinstance(person.homepage, (str, type(None)))

