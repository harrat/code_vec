def test_import_after_add(mbed, testrepos):
    test_add(mbed, testrepos)
    mkcommit('test1')

    test1 = testrepos[0]
    popen(['python', mbed, 'import', test1, 'testimport', "-vv"])

    assertls(mbed, 'testimport', [
        "[mbed]",
        "testimport",
        "|- test2",
        "|  `- test3",
        "|     `- test4",
        "`- test3",
        "   `- test4",
    ])
