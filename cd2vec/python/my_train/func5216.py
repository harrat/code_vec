def test_home_team_wins(self):
        fake_away_runs = PropertyMock(return_value=3)
        fake_home_runs = PropertyMock(return_value=6)
        type(self.boxscore)._away_runs = fake_away_runs
        type(self.boxscore)._home_runs = fake_home_runs

        assert self.boxscore.winner == HOME
