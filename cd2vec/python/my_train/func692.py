def test_run_with_adding_new_file(self):
        """
        Test that the PolicyDirectoryMonitor can load policy files and track
        them properly, even when a new policy file is added while tracking is
        active.
        """
        m = monitor.PolicyDirectoryMonitor(
            self.tmp_dir,
            multiprocessing.Manager().dict()
        )
        m.logger = mock.MagicMock(logging.Logger)
        m.halt_trigger = mock.MagicMock(multiprocessing.synchronize.Event)
        m.halt_trigger.is_set.side_effect = side_effects(
            [
                False,
                build_write_effect(self.tmp_dir, "policy_2.json", POLICY_2),
                True
            ]
        )

        write_file(self.tmp_dir, "policy_1.json", POLICY_1)

        self.assertEqual({}, m.file_timestamps)
        self.assertEqual({}, m.policy_cache)
        self.assertEqual([], m.policy_files)
        self.assertEqual({}, m.policy_map)
        self.assertEqual([], m.policy_store.keys())

        m.run()

        m.logger.info.assert_any_call(
            "Starting up the operation policy file monitor."
        )
        m.logger.info.assert_any_call(
            "Loading policies for file: {}".format(
                os.path.join(self.tmp_dir, "policy_1.json")
            )
        )
        m.logger.info.assert_any_call("Loading policy: policy_A")
        m.logger.info.assert_any_call(
            "Loading policies for file: {}".format(
                os.path.join(self.tmp_dir, "policy_2.json")
            )
        )
        m.logger.debug.assert_not_called()
        m.logger.info.assert_any_call("Loading policy: policy_B")
        m.logger.info.assert_any_call("Loading policy: policy_C")
        m.logger.info.assert_any_call(
            "Stopping the operation policy file monitor."
        )

        self.assertEqual(2, len(m.policy_files))
        path = os.path.join(self.tmp_dir, "policy_1.json")
        self.assertEqual(
            os.path.getmtime(path),
            m.file_timestamps.get(path, None)
        )
        self.assertIn(path, m.policy_files)
        self.assertEqual(path, m.policy_map.get("policy_A", None))

        path = os.path.join(self.tmp_dir, "policy_2.json")
        self.assertEqual(
            os.path.getmtime(path),
            m.file_timestamps.get(path, None)
        )
        self.assertIn(path, m.policy_files)
        self.assertEqual(path, m.policy_map.get("policy_B", None))
        self.assertEqual(path, m.policy_map.get("policy_C", None))

        self.assertEqual(
            {
                "policy_A": [],
                "policy_B": [],
                "policy_C": []
            },
            m.policy_cache
        )

        self.assertEqual(3, len(m.policy_store.keys()))
        self.assertEqual(
            {
                "groups": {
                    "group_A": {
                        enums.ObjectType.SYMMETRIC_KEY: {
                            enums.Operation.GET: enums.Policy.ALLOW_ALL,
                            enums.Operation.DESTROY: enums.Policy.ALLOW_ALL
                        }
                    }
                }
            },
            m.policy_store.get("policy_A", None)
        )
        self.assertEqual(
            {
                "groups": {
                    "group_B": {
                        enums.ObjectType.SYMMETRIC_KEY: {
                            enums.Operation.GET: enums.Policy.ALLOW_ALL,
                            enums.Operation.LOCATE: enums.Policy.ALLOW_ALL,
                            enums.Operation.DESTROY: enums.Policy.ALLOW_ALL
                        }
                    }
                }
            },
            m.policy_store.get("policy_B", None)
        )
        self.assertEqual(
            {
                "groups": {
                    "group_C": {
                        enums.ObjectType.SYMMETRIC_KEY: {
                            enums.Operation.GET: enums.Policy.ALLOW_ALL,
                            enums.Operation.DESTROY: enums.Policy.DISALLOW_ALL
                        }
                    }
                }
            },
            m.policy_store.get("policy_C", None)
        )
