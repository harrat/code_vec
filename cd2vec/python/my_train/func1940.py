@freeze_time("2018-01-01")
    def test_2sa_notification_without_smtp_login_and_tls(self):
        with vcr.use_cassette("tests/vcr_cassettes/auth_requires_2sa.yml"):
            with patch("smtplib.SMTP") as smtp:
                # Pass fixed client ID via environment variable
                os.environ["CLIENT_ID"] = "EC5646DE-9423-11E8-BF21-14109FE0B321"
                runner = CliRunner()
                result = runner.invoke(
                    main,
                    [
                        "--username",
                        "jdoe@gmail.com",
                        "--password",
                        "password1",
                        "--smtp-no-tls",
                        "--notification-email",
                        "jdoe+notifications@gmail.com",
                        "-d",
                        "tests/fixtures/Photos",
                    ],
                )
                print(result.output)
                assert result.exit_code == 1
            smtp_instance = smtp()
            smtp_instance.connect.assert_called_once()
            smtp_instance.starttls.assert_not_called()
            smtp_instance.login.assert_not_called()
            smtp_instance.sendmail.assert_called_once_with(
                "jdoe+notifications@gmail.com",
                "jdoe+notifications@gmail.com",
                "From: iCloud Photos Downloader <jdoe+notifications@gmail.com>\n"
                "To: jdoe+notifications@gmail.com\n"
                "Subject: icloud_photos_downloader: Two step authentication has expired\n"
                "Date: 01/01/2018 00:00\n\nHello,\n\n"
                "Two-step authentication has expired for the icloud_photos_downloader script.\n"
                "Please log in to your server and run the script manually to update two-step "
                "authentication.",
            )
