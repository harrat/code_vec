def test_rvalue_fails(self):
        self.idf.initreadtxt(expected_failure)
        c = self.idf.getobject("CONSTRUCTION", "TestConstruction")
        try:
            c.rvalue
            assert False
        except AttributeError as e:
            assert str(e) == "Skyhooks material not found in IDF"
