def test_sslv2_enabled(self):
        # Given a server to scan that supports SSL 2.0
        with LegacyOpenSslServer(openssl_cipher_string="ALL:COMPLEMENTOFALL") as server:
            server_location = ServerNetworkLocationViaDirectConnection(
                hostname=server.hostname, ip_address=server.ip_address, port=server.port
            )
            server_info = ServerConnectivityTester().perform(server_location)

            # When scanning for cipher suites, it succeeds
            result: CipherSuitesScanResult = Sslv20ScanImplementation.scan_server(server_info)

        # The right cipher suites were detected
        assert len(result.accepted_cipher_suites) == 7
        assert not result.rejected_cipher_suites

        # And the embedded server does not have a preference by default
        assert not result.cipher_suite_preferred_by_server
