def test_rwlock():

    main_ctrl = threading.Event()
    reader_ctrl = threading.Event()
    writer_ctrl = threading.Event()
    lock = RWLock("test")

    state = Bunch(reading=False, writing=False)

    def read():
        logging.info("Before read")
        reader_ctrl.wait()
        reader_ctrl.clear()

        with lock:
            logging.info("Reading...")
            state.reading = True
            main_ctrl.set()
            reader_ctrl.wait()
            reader_ctrl.clear()
            state.reading = False
            logging.info("Done reading")

        logging.info("After read")

    def write():
        logging.info("Before write")
        writer_ctrl.wait()
        writer_ctrl.clear()

        with lock.exclusive():
            logging.info("Writing...")
            state.writing = True
            main_ctrl.set()
            writer_ctrl.wait()
            writer_ctrl.clear()
            state.writing = False
            logging.info("Done writing")

        logging.info("After write")
        main_ctrl.set()

    reader = concurrent(read, threadname='read')
    writer = concurrent(write, threadname='write')

    with reader, writer:
        assert not state.reading and not state.writing
