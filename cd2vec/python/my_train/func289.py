@autodoc.generate('var/test_post.rst')
    @autodoc.describe('POST /')
    def test_post(self):
        """ POST / """
        res = self.client.post_json('/', params={'id': 1, 'message': 'foo'})
        self.assertEqual(res.status_code, 200)

        return res
