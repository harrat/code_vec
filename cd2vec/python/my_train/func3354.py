@pytest.mark.skipif(not (hasattr(os, 'fork') and hasattr(socket, 'AF_UNIX')),
                    reason="test requires fork and unix sockets")
def test_forkserver(tmpdir):
    tmpdir = six.text_type(tmpdir)
    os.chdir(tmpdir)

    shutil.copytree(BENCHMARK_DIR, 'benchmark')

    d = {}
    d.update(ASV_CONF_JSON)
    d['env_dir'] = "env"
    d['benchmark_dir'] = 'benchmark'
    d['repo'] = 'None'
    conf = config.Config.from_json(d)

    with open(os.path.join('benchmark', '__init__.py'), 'w') as f:
        f.write("import sys; sys.stdout.write('import-time print')")

    with open(os.path.join('benchmark', 'unimportable.py'), 'w') as f:
        f.write("raise RuntimeError('not importable')")

    env = environment.ExistingEnvironment(conf, sys.executable, {}, {})
    spawner = runner.ForkServer(env, os.path.abspath('benchmark'))

    result_file = os.path.join(tmpdir, 'run-result')

    try:
        out, errcode = spawner.run('time_examples.TimeWithRepeat.time_it', '{}',
                                   None,
                                   result_file,
                                   60,
                                   os.getcwd())
    finally:
        spawner.close()

    assert out.startswith("import-time print<1>")
    assert errcode == 0

    with open(result_file, 'r') as f:
        data = json.load(f)
    assert len(data['samples']) >= 1

