def test_mock_contructors(engine):
    assert engine.active is False
    assert engine.isdone() is True

    api.mock('foo.com')
    assert engine.isdone() is False
    assert len(engine.mocks) == 1
    api.off()

    assert len(engine.mocks) == 0
    assert engine.active is False

