def test_empty_data(self):
        random_data = generate_nonce(0).encode()
        attachment = self.member.create_blob(self.member.member_id, self.file_type, self.file_name, random_data)
        out_blob = self.member.get_blob(attachment.blob_id)
        assert out_blob.id == attachment.blob_id
        assert out_blob.payload.data == random_data
        assert out_blob.payload.owner_id == self.member.member_id
