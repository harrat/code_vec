def test_fetch_datasets(self):

        import lazygrid as lg

        datasets = lg.datasets.fetch_datasets(task="regression", max_samples=200, max_features=10)

        datasets = lg.datasets.fetch_datasets(task="classification", min_classes=2,
                                              max_samples=200, max_features=10, update_data=True)

        self.assertEqual(datasets.loc["iris"].version, 45)
        self.assertEqual(datasets.loc["iris"].did, 42098)
        self.assertEqual(datasets.loc["iris"].n_samples, 150)
        self.assertEqual(datasets.loc["iris"].n_features, 4)
        self.assertEqual(datasets.loc["iris"].n_classes, 3)

        datasets = lg.datasets.fetch_datasets(task="regression", max_samples=200, max_features=10, update_data=True)
        datasets = lg.datasets.fetch_datasets(task="random_task", max_samples=200, max_features=10, update_data=True)
