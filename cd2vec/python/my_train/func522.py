def test_get_response_body_json(self):
        self._make_request('https://radiopaedia.org/api/v1/countries/current')
        last_request = self.client.get_last_request()

        body = self.client.get_response_body(last_request['id'])

        self.assertIsInstance(body, bytes)
