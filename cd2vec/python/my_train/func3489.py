def test_multiple_Models_training():
    """
    Here, we compose a model multilinear = A + B + C, where A, B, and C represent
    different components of the overall model.
    """
    A = DummyModel({'m': [0.]})
    A.freeze('b')
    B = model_from_module(LinearModule)()
    B.freeze('b')
    C = DummyModel({'m': [0.]})
    C.freeze('m')
    multilinear = DummyMultilinearModel({'m1':A.m, 'm2': B.m, 'b': C.b})
    assert multilinear.m1 is A.m
    assert multilinear.m2 is B.m
    assert multilinear.b is C.b
    training_data = generate_multilinear_model_data()
    m1 = training_data[1]['m1']
    m2 = training_data[1]['m2']
    b = training_data[1]['b']
    errors = training_data[1]['errors']
    minibatcher = get_minibatcher(training_data[0])
    train_model(multilinear, minibatcher)
    assert (A.m - m1 < .6).all()
    assert (B.m - m2 < .6).all()
    assert (A.b == 0).all()
    assert (C.m == 0.).all()

def test_setattr_in_pipeline():
    """
    Setattribute should not trigger any recursive actions.
    """
    A = DummyModel({'m': [3.]}, out_column='y1')
    B = DummyModel({'m': [1.], 'b': [2.]}, input=A, in_column='y1', out_column='y')
    A.ok = 0
    B.ok = 1
    assert A.ok == 0
    assert B.ok == 1
    assert (A.m == 3.).all()
    assert (B.m == 1.).all()
    # Test that Parameters and subModules automatically get added as components.
    A.yes = Parameter(torch.Tensor([3.,4.]))
    assert 'yes' in A.components
    assert 'yes' not in B.components
    assert B.yes is A.yes
