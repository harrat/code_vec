def test_write_compressed_file(compressed_file):
    """Test that wkr.open can write to compressed file formats."""
    contents = b'new file\n' + BINARY_DATA
    with wkr.open(compressed_file, 'wb') as output_file:
        output_file.write(contents)
    ext = os.path.splitext(compressed_file)[1]
    open_fn = {'.bin': open, '.xz': lzma.LZMAFile, '.gz': gzip.open}.get(ext)
    input_file = open_fn(compressed_file, 'rb')
    data = input_file.read()
    assert isinstance(data, binary_type)
    assert data == contents
    input_file.close()

