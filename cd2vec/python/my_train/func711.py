def test_download_file_runtime_error():
    url = "https://raw.githubusercontent.com/nltk/nltk_data/gh-pages/packages/corpora/stopword.zip"
    dirpath = "/tmp/ruts_download"
    with pytest.raises(RuntimeError):
        download_file(url, dirpath=dirpath, force=True)
