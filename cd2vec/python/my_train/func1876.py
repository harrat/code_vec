def test_select_distinct():
    """
    Find distinct genes
    """
    results = select_distinct_subjects(subject_category='gene',
                                       object_category='phenotype',
                                       subject_taxon='NCBITaxon:9606')
    print("DISTINCT SUBJECTS={}".format(results))
    assert len(results) > 0

def test_go_assocs():
    """
    Test basic association search functionality
    """
    payload = search_associations(subject=TWIST_ZFIN,
                                  object_category='function'
    )
    assocs = payload['associations']
    assert len(assocs) > 0
