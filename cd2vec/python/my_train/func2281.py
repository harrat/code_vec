def test_3(self):
        self.z[5, 3] = 0.1
        result = np.ones(len(self.time))
        result[3:7] = 1
        assert_array_equal(classify._find_rain(self.z, self.time,
                                               time_buffer=1), result)
