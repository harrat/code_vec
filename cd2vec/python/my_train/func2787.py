@mock.patch('urllib3.PoolManager.request')
def test_runner_duration(_wrapped_post):
    runner = RunnerEventMock()
    runner.terminated = False
    trace = trace_factory.get_or_create_trace()
    trace.token = 'a'
    trace.set_runner(runner)
    time.sleep(0.2)
    trace_factory.send_traces()

    assert 0.2 < runner.duration < 0.3

