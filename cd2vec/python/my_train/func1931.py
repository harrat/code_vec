def test_train(self):
        model_config = core.ModelConfig(_lineworld_name)
        tc = core.TrainContext()
        random_agent = tfagents.TfRandomAgent(model_config=model_config)
        random_agent.train(train_context=tc, callbacks=[duration.Fast(), log.Iteration()])
        assert tc.episodes_done_in_iteration == 1
