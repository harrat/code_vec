def test_empty(self):
        with self.assertRaises(AudioFileUnsupportedFormatError):
            self.perform(self.EMPTY_FILE_PATH, 0, 0)
