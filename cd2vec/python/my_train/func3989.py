def test_reader_chart_range(self):
        from datetime import date, timedelta

        syms = ["AAPL"]

        # source: pandas datareader

        assert (
            HistoricalReader(
                symbols=syms, start=date.today() - timedelta(days=5), end=date.today()
            ).chart_range
            == "5d"
        )
        assert (
            HistoricalReader(
                symbols=syms, start=date.today() - timedelta(days=27), end=date.today()
            ).chart_range
            == "1m"
        )
        assert (
            HistoricalReader(
                symbols=syms, start=date.today() - timedelta(days=83), end=date.today()
            ).chart_range
            == "3m"
        )
        assert (
            HistoricalReader(
                symbols=syms, start=date.today() - timedelta(days=167), end=date.today()
            ).chart_range
            == "6m"
        )
        assert (
            HistoricalReader(
                symbols=syms, start=date.today() - timedelta(days=170), end=date.today()
            ).chart_range
            == "1y"
        )
        assert (
            HistoricalReader(
                symbols=syms, start=date.today() - timedelta(days=365), end=date.today()
            ).chart_range
            == "2y"
        )
        assert (
            HistoricalReader(
                symbols=syms, start=date.today() - timedelta(days=730), end=date.today()
            ).chart_range
            == "5y"
        )
        assert (
            HistoricalReader(
                symbols=syms,
                start=date.today() - timedelta(days=1826),
                end=date.today(),
            ).chart_range
            == "max"
        )
