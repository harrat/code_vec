def test_xlsx_to_file_saves_xlsx_as_file_attachment(xlsx: Workbook) -> None:
    xlsx_file = xlsx_to_file(xlsx, "sam.xlsx")

    assert xlsx_file.name == "sam.xlsx"
    assert xlsx_file.content == xlsx_to_bytes(xlsx)

