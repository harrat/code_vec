def test_listen_alarms(self):
        """Test that listen alarms."""
        alarms = [
            "2019-07-28;00:57:27;MotionDetection;1;0",
        ]
        ex_log = ["2019-07-28 00:57:27;MotionDetection;True"]
        self._listen_alarms_tester(alarms, ex_log)

        alarms.append("2019-07-28;00:57:28;MotionDetection;0;0")
        ex_log.append("2019-07-28 00:57:28;MotionDetection;False")
        self._listen_alarms_tester(alarms, ex_log)

        alarms.append("2019-07-28;15:51:52;SensorAlarm;1;0")
        ex_log.append("2019-07-28 15:51:52;SensorAlarm;True")
        self._listen_alarms_tester(alarms, ex_log)

        alarms.append("2019-07-28;15:51:53;SensorAlarm;0;0")
        ex_log.append("2019-07-28 15:51:53;SensorAlarm;False")
        self._listen_alarms_tester(alarms, ex_log)
