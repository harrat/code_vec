def test_use_callback_function_from_request_arguments_to_wrap_output(self, app):
        with app.test_request_context('?callback=console.log'):
            assert self.serializer('42') == 'console.log(42);'
