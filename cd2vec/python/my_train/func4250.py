def test_nfl_integration_returns_correct_attributes_for_team(self):
        kansas = self.teams('KAN')

        for attribute, value in self.results.items():
            assert getattr(kansas, attribute) == value
