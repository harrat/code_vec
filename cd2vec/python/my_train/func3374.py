def test_gecko_manager_with_wrong_version():
    with pytest.raises(ValueError) as ex:
        driver_path = GeckoDriverManager("0.2").install()
        ff = webdriver.Firefox(executable_path=driver_path)
        ff.quit()
    assert "There is no such driver by url "\
        "https://api.github.com/repos/mozilla/geckodriver/releases/tags/0.2" \
        in ex.value.args[0]
