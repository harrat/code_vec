def test_basic_search_with_card():
    from_date = "{} 10:00".format(_TOMORROW)
    to_date = "{} 12:00".format(_TOMORROW)
    departure_station = "Toulouse Matabiau"
    arrival_station = "Bordeaux St-Jean"

    p1 = Passenger(birthdate="01/01/1980", cards=[trainline.AVANTAGE_FAMILLE])

    results = trainline.search(
        departure_station=departure_station,
        arrival_station=arrival_station,
        from_date=from_date,
        to_date=to_date
    )
    print()
    print("Search trips for {} to {}, between {} and {}".format(
        departure_station, arrival_station, from_date, to_date))
    print("{} results".format(len(results)))
    assert len(results) > 0

    display_trips(results)

