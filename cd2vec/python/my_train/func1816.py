def test_to_list(self):
        num_experiments = 10
        tic = clock()
        for i in range(0, num_experiments):
            e = (
                Enumerable(({"id": x, "value": x} for x in range(0, 80000)))
                .select(lambda b: {"id": b["id"]})
                .to_list()
            )
        toc = clock()
        py_linq_time = (toc - tic) / num_experiments

        tic = clock()
        for i in range(0, num_experiments):
            l = list(
                map(
                    lambda b: {"id": b["id"]},
                    ({"id": x, "value": x} for x in range(0, 80000)),
                )
            )
        toc = clock()
        python_list_time = (toc - tic) / num_experiments

        tic = clock()
        for i in range(0, num_experiments):
            l = [
                i
                for i in map(
                    lambda b: {"id": b["id"]},
                    ({"id": x, "value": x} for x in range(0, 80000)),
                )
            ]
        toc = clock()
        python_list_comp_time = (toc - tic) / num_experiments

        self.assertTrue(py_linq_time < python_list_comp_time * 10)
