def test_get_issue_metadata(talker, fake_comic, mock_fetch):
    # expected = MockFetchIssueResponse.issue_response()
    res = get_issue_metadata(fake_comic, 1, talker)

    # Check to see the zipfile had metadata written
    comic = ComicArchive(fake_comic)
    file_md = comic.read_metadata()

    assert res is True
    assert file_md is not None
    assert file_md.series == "Aquaman"
    assert file_md.issue == "1"
    assert file_md.year == "1993"

