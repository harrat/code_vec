def test_decorated_class_restores_file_contents(self):
        the_codeumentary = TestFileGuardClassDecorator.TheCodeumentary('value_1', 'value_2', self)
        self._assert_file_content_equals(TestFileGuardClassDecorator.TEST_FILE_CONTENTS)

        the_codeumentary.change_the_file()
        self._assert_file_content_equals(TestFileGuardClassDecorator.TEST_FILE_CONTENTS)

        the_codeumentary.change_the_file_again()
        self._assert_file_content_equals(TestFileGuardClassDecorator.TEST_FILE_CONTENTS)

        the_codeumentary.do_not_change_the_file('hello')
        self._assert_file_content_equals(TestFileGuardClassDecorator.TEST_FILE_CONTENTS)

        the_codeumentary.value_1
        self._assert_file_content_equals(TestFileGuardClassDecorator.TEST_FILE_CONTENTS)

        the_codeumentary._value_2 = 'value_3'
        self._assert_file_content_equals(TestFileGuardClassDecorator.TEST_FILE_CONTENTS)
