@staticmethod
    @pytest.mark.usefixtures('import-remove', 'update-forced-utc')
    def test_cli_update_snippet_014(snippy, editor_data):
        """Update snippet from command line.

        Update existing snippet directly from command line. In this case,
        editor is not used because of '--no-editor' option which updates
        given content directly without user interaction.
        """

        content = {
            'data': [
                Content.deepcopy(Snippet.REMOVE)
            ]
        }
        content['data'][0]['brief'] = 'brief cli'
        content['data'][0]['groups'] = ('cli-group',)
        content['data'][0]['tags'] = ('cli-tag',)
        content['data'][0]['links'] = ('https://cli-link',)
        content['data'][0]['digest'] = '613e163028a17645a7dfabbe159f05d14db7588259229dd8d08e949cdc668373'
        cause = snippy.run(['snippy', 'update', '-d', '54e41e9b52a02b63', '-t', 'cli-tag', '-b', 'brief cli', '-g', 'cli-group', '-l', 'https://cli-link', '--no-editor'])  # pylint: disable=line-too-long
        assert cause == Cause.ALL_OK
        editor_data.assert_not_called()
        Content.assert_storage(content)
