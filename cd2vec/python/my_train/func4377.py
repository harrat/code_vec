@pytest.mark.vcr()
def test_workbench_export_chapters_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.export(format='html', chapters='diff')
