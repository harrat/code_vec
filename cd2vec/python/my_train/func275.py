@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'yaml', 'import-remove', 'update-remove-utc')
    def test_cli_import_snippet_010(snippy):
        """Import defined snippet.

        Import defined snippet based on message digest. File name is defined
        from command line as yaml file which contain one snippet. Content
        tags were updated.
        """

        content = {
            'data': [
                Content.deepcopy(Snippet.REMOVE)
            ]
        }
        content['data'][0]['tags'] = ('new', 'set', 'tags')
        content['data'][0]['digest'] = '4525613eaecd52970316d7d6495f091fad1fd027834d7d82a523cbccc4aa3582'
        file_content = Content.get_file_content(Content.YAML, content)
        with mock.patch('snippy.content.migrate.io.open') as mock_file:
            yaml.safe_load.return_value = file_content
            cause = snippy.run(['snippy', 'import', '-d', '54e41e9b52a02b63', '-f', 'one-snippet.yaml'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-snippet.yaml', mode='r', encoding='utf-8')
