@timeout_decorator.timeout(5)
    def test_channel_expire(self):
        self.bus.rpc_channel_expire = 1
        called = threading.Event()

        def remotefn():
            time.sleep(1.25)
            called.set()

        pymq.expose(remotefn)

        stub = pymq.stub(remotefn, timeout=1)
        stub.rpc()
        keys = self.redis.rds.keys('*rpc*')
        self.assertEqual(0, len(keys), 'Expected no rpc results yet %s' % keys)

        called.wait()

        keys = self.redis.rds.keys('*rpc*')
        self.assertEqual(1, len(keys))

        # wait for key to expire
        time.sleep(1.25)

        keys = self.redis.rds.keys('*rpc*')
        self.assertEqual(0, len(keys), 'key did not expire')
