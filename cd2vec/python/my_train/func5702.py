def test_write_to_logfile_and_stderr(capsys):
    """
    Should log to a file.
    """
    logzero.reset_default_logger()
    temp = tempfile.NamedTemporaryFile()
    try:
        logger = logzero.setup_logger(logfile=temp.name)
        logger.info("test log output")
