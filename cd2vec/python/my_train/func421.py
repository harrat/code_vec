def test_object_from_id_class(self):
        """ Ensure we get an object from just the class name """
        dc = MappedClass("TestDOM", (self.DataObject,), dict())
        self.mapper.add_class(dc)
        self.mapper.remap()
        g = self.mapper.oid(dc.rdf_type)
        self.assertIsInstance(g, dc)
