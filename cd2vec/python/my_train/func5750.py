def test_image_file_content_restored_on_change(self):

        image_frozen = os.path.join(self.FROZEN_RESOURCES_PATH, self.IMAGE_NAME)
        image_path = os.path.join(self.RESOURCES_PATH, self.IMAGE_NAME)

        self._assert_files_equal(image_path, image_frozen)

        @guard(image_path)
        def function_that_changes_the_file():
            lines_to_write = ['hello, world\n']
            with open(image_path, 'w') as file:
                file.writelines(lines_to_write)

            self._assert_file_content_equals(image_path, lines_to_write)

        function_that_changes_the_file()
        self._assert_files_equal(image_path, image_frozen)
