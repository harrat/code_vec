@staticmethod
    @pytest.mark.usefixtures('import-kafka-mkdn', 'export-time')
    def test_cli_export_solution_042(snippy):
        """Export defined solution with digest.

        Export Markdown native content when the ``filename`` attribute defines
        the default file as Markdown file. In this case the ``--format`` option
        defines  that the exported file must be text file. Command line option
        must always override other configuration.
        """

        content = {
            'meta': Content.get_cli_meta(),
            'data': [
                Solution.KAFKA_MKDN
            ]
        }
        with mock.patch('snippy.content.migrate.io.open', autospec=True) as mock_file:
            cause = snippy.run(['snippy', 'export', '-d', 'c54c8a896b94ea35', '--format', 'text'])
            assert cause == Cause.ALL_OK
            Content.assert_text(mock_file, 'kubernetes-docker-log-driver-kafka.text', content)
