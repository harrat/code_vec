def test_only_one_access_token_allowed(self):
        member = self.client.create_member(utils.generate_alias(type=Alias.DOMAIN))
        address = member.add_address(generate_nonce(), utils.generate_address())
        payload = AccessTokenBuilder.create_with_alias(member.get_first_alias()).for_address(address.id).build()
        member.create_access_token(payload)
        payload = AccessTokenBuilder.create_with_alias(member.get_first_alias()).for_address(address.id).build()
        with pytest.raises(RequestError) as exec_info:
            member.create_access_token(payload)
        assert exec_info.value.details == "Token from {} to {} already exists".format(member.member_id,
                                                                                      member.member_id)
        assert exec_info.value.code == StatusCode.ALREADY_EXISTS
