def test_drill(self):
        drill = Drill.parse('(drill 0.8)')
        assert drill.size == 0.8
        assert Drill.parse(drill.to_string()) == drill
