def test_kbd_interrupt_in_wait_with_concurrency(async_runner, make_cases,
                                                make_async_exec_ctx):
    ctx = make_async_exec_ctx(4)
    next(ctx)

    runner, _ = async_runner
    with pytest.raises(KeyboardInterrupt):
        runner.runall(make_cases([
            KeyboardInterruptCheck(), SleepCheck(10),
            SleepCheck(10), SleepCheck(10)
        ]))

    assert_interrupted_run(runner)

