def test_pathspec(self):
        level1 = self.root.make_directory(name='ABC123')
        level2 = self.root.make_directory(parent=level1, name='AAA')
        path = self.root.make_file(parent=level2, name='FFF555.txt')
        base = level1.parent

        spec = PathSpec(
            DirSpec(
                PathPathVar('root'),
                StrPathVar('subdir', pattern='[A-Z0-9_]+', invalid=('XYZ999',)),
                StrPathVar('leaf', pattern='[^_]+', valid=('AAA', 'BBB')),
                template=os.path.join('{root}', '{subdir}', '{leaf}')),
            FileSpec(
                StrPathVar('id', pattern='[A-Z0-9_]+', invalid=('ABC123',)),
                StrPathVar('ext', pattern='[^\.]+', valid=('txt', 'exe')),
                template='{id}.{ext}'))

        path_var_values = dict(root=base, subdir='ABC123', leaf='AAA',
                               id='FFF555', ext='txt')
        pathinst = spec(**path_var_values)
        assert path_inst(path, path_var_values) == pathinst
        assert base == pathinst['root']
        assert 'ABC123' == pathinst['subdir']
        assert 'AAA' == pathinst['leaf']
        assert 'FFF555' == pathinst['id']
        assert 'txt' == pathinst['ext']

        fail1 = dict(path_var_values)
        # should fail because expecting all caps
        fail1['id'] = 'abc123'
        with self.assertRaises(ValueError):
            spec(**fail1)

        fail2 = dict(path_var_values)
        # should fail because foo is not in the valid list
        fail2['ext'] = 'foo'
        with self.assertRaises(ValueError):
            spec(**fail2)

        fail3 = dict(path_var_values)
        # should fail because ABC123 is in the invalid list
        fail3['id'] = 'ABC123'
        with self.assertRaises(ValueError):
            spec(**fail3)

        pathinst = spec.parse(path)
        assert path_inst(path, path_var_values) == pathinst

        path2 = self.root.make_file(parent=level2, name='fff555.txt')
        with self.assertRaises(ValueError):
            spec.parse(path2)

        all_paths = spec.find(base, recursive=True)
        assert 1 == len(all_paths)
        assert path_inst(path, path_var_values) == all_paths[0]

        # make sure it works with plain paths
        spec = PathSpec(
            level2,
            FileSpec(
                StrPathVar('id', pattern='[A-Z0-9_]+', invalid=('ABC123',)),
                StrPathVar('ext', pattern='[^\.]+', valid=('txt', 'exe')),
                template='{id}.{ext}'))
        assert path_inst(path, dict(id='FFF555', ext='txt')) == spec.parse(path)
        with self.assertRaises(ValueError):
            bad_path = Path(get_root()) / 'foo' / 'bar' / path.name
            spec.parse(bad_path)

        spec = PathSpec(
            DirSpec(
                PathPathVar('root'),
                StrPathVar('subdir', pattern='[A-Z0-9_]+', invalid=('XYZ999',)),
                StrPathVar('leaf', pattern='[^_]+', valid=('AAA', 'BBB')),
                template=os.path.join('{root}', '{subdir}', '{leaf}')),
            path.name)
        assert \
            path_inst(path, dict(root=base, subdir='ABC123', leaf='AAA')) == \
            spec.parse(path)

        spec = PathSpec(level2, path.name)
        all_paths = spec.find()
        assert 1 == len(all_paths)
        assert path_inst(path) == all_paths[0]
