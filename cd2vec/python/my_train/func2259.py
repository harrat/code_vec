def test_invalid_timestamp(self):
        report = copy.deepcopy(self.valid_report)
        report['published'] = "2016-11-31T08:17:27.000000Z"
        self.assertFalseWithOptions(report)
