def test_via_threadpool():
    numbers = [1, 2, 3, 4, 5]
    squared_numbers = calculate_parallel(numbers, 4)
    expected_result = [1, 4, 9, 16, 25]

    assert squared_numbers == expected_result

    tracer = ThundraTracer.get_instance()
    nodes = tracer.get_spans()
    active_span = None
    for key in nodes:
        if key.operation_name == 'calculate_parallel':
            active_span = key

    args = active_span.get_tag('method.args')
    assert args[0]['name'] == 'arg-0'
    assert args[0]['value'] == numbers
    assert args[0]['type'] == 'list'
    assert args[1]['name'] == 'arg-1'
    assert args[1]['value'] == 4
    assert args[1]['type'] == 'int'

    return_value = active_span.get_tag('method.return_value')
    assert return_value['value'] == squared_numbers
    assert return_value['type'] == 'list'

    error = active_span.get_tag('thrownError')
    assert error is None

