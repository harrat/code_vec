@responses.activate
    def test_live_hunt_start_with_invalid_yara_file(self):
        url = 'https://api.polyswarm.network/v2/hunt/live'
        responses.add(responses.POST, url, body=self.mock_hunt_response)

        broken_yara_file = self._get_test_resource_file_path('broken.yara')
        result = self._run_cli(['--output-format', 'json', 'live', 'create', broken_yara_file])

        self._assert_text_result(
            result,
            expected_output='error [polyswarm.client.polyswarm]: Malformed yara file: line 1: syntax error, unexpected identifier',
            expected_return_code=2,
        )
