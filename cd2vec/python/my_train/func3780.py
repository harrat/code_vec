def test_task_filtering(todolist_app):
	with todolist_app.test_client() as client:
		resp = client.jget('/tasks?user_email=tweety@acme.com')
		assert resp['status'] == 'success'
		assert resp['result'][0]['title'] == "Solve a mystery"
