def test_loadbalanced_getitem_method(mocked_loadbalanced):
    """Test FlaskMultiRedis loadbalanced __getitem__ method."""

    assert mocked_loadbalanced['pattern'] in ['node1', 'node2', 'node3']
    mocked_loadbalanced._redis_nodes = []
    assert mocked_loadbalanced['pattern'] is None

