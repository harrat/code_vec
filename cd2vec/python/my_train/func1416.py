def test_twotag_pctg(self, capsys):
        with timemarker.TimeMarker() as timer:
            timer.tag("sleep_100ms_1")
            sleep(.1)
            timer.tag("sleep_100ms_2")
            sleep(.1)
        timer.stats()

        out, err = capsys.readouterr()
        assert err == ''
        assert out != ''
        out_dict = split_oneline(out)

        assert out_dict['TIME'] == pytest.approx(0.2, abs=1e-2)
        assert out_dict['start'] == pytest.approx(0.0, abs=1e-2)
        assert out_dict['sleep_100ms_1'] == pytest.approx(.5, abs=1e-2)
        assert out_dict['sleep_100ms_2'] == pytest.approx(.5, abs=1e-2)
