def test_have_tags(self):
        assert 2 == self.count_checks(filters.have_tag('a|c'))
        assert 0 == self.count_checks(filters.have_tag('p|q'))
        assert 2 == self.count_checks(filters.have_tag('z'))
