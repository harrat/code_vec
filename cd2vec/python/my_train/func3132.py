def test_observe(self, trig, echo, func_in, func_out, offset):
        watch = Watch(gpio=GPIO, trig=trig, echo=echo, func_in=func_in, func_out=func_out, offset=offset)
        watch.observe()
        time.sleep(10)
        watch.stop()
        assert stack[-1].startswith("func_in_usage_") or stack[-1].startswith("func_out_usage_")
