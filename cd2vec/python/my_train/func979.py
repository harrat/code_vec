def test_utils_get_browser_versions():
    browser_names = [browser[0] for browser in utils.get_browsers()]

    for browser in browser_names:
        count = len(utils.get_browser_versions(browser))
        assert count == settings.BROWSERS_COUNT_LIMIT

        if urlopen_has_ssl_context:
            with mock.patch(
                'fake_useragent.utils.Request',
                side_effect=partial(
                    _request,
                    response_url='https://expired.badssl.com/',
                ),
            ):
                with pytest.raises(errors.FakeUserAgentError):
                    utils.get_browser_versions(browser)
