def test_src_wordacc_cache(self):
    cached_stats1 = compare_mt_main.generate_src_word_accuracy_report(self.ref, [self.out1], self.src, ref_align_file=self.ref_align_file, to_cache=True)
    cached_stats2 = compare_mt_main.generate_src_word_accuracy_report(self.ref, [self.out2], self.src, ref_align_file=self.ref_align_file, to_cache=True)
    self.assertTrue('statistics' in cached_stats1 and 'my_ref_total_list' in cached_stats1 and 'my_out_matches_list' in cached_stats1)
    self.assertTrue('statistics' in cached_stats2 and 'my_ref_total_list' in cached_stats2 and 'my_out_matches_list' in cached_stats2)
    ori_report = compare_mt_main.generate_src_word_accuracy_report(self.ref, [self.out1, self.out2], self.src, ref_align_file=self.ref_align_file)
    cached_report = compare_mt_main.generate_src_word_accuracy_report(self.ref, [self.out1, self.out2], self.src, ref_align_file=self.ref_align_file, cache_dicts=[cached_stats1, cached_stats2])
    self.assertTrue(cached_report.statistics == ori_report.statistics)
    self.assertTrue(cached_report.examples == ori_report.examples)
