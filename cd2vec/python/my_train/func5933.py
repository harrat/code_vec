def test_country(self):
        intrusion_set = copy.deepcopy(self.valid_intrusion_set)
        intrusion_set['country'] = "USA"
        results = validate_parsed_json(intrusion_set, self.options)
        self.assertEqual(results.is_valid, False)
