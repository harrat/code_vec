@pytest.mark.vcr()
def test_agentgroups_delete_agent_from_group(api, agent, agentgroup):
    api.agent_groups.add_agent(agentgroup['id'], agent['id'])
    api.agent_groups.delete_agent(agentgroup['id'], agent['id'])

@pytest.mark.vcr()
def test_agentgroups_delete_mult_agents_from_group(api, agentgroup):
    agents = api.agents.list()
    alist = [agents.next()['id'], agents.next()['id']]
    api.agent_groups.add_agent(agentgroup['id'], *alist)
    time.sleep(1)
    task = api.agent_groups.delete_agent(agentgroup['id'], *alist)
    assert isinstance(task, dict)
    check(task, 'container_uuid', str)
    check(task, 'status', str)
    check(task, 'task_id', str)
