def test_current_fails_with_exit_code_1_when_no_current_entry(cli, config, data_dir):
    efg = EntriesFileGenerator(data_dir, '%m_%Y.txt')
    efg.expand(datetime.date(2014, 1, 21)).write(
        "20/01/2014\nalias_1 5 hello world"
    )
    efg.patch_config(config)

    with freeze_time('2014-01-20 10:45:00'):
        result = cli('current', return_stdout=False)

    assert result.exit_code == 1
    assert result.output.strip() == "No entry in progress."

