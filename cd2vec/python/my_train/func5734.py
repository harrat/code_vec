def test_pull_player_list_throws_error_for_season_that_has_not_yet_happened():
	nhl = NHLScraper()
	with pytest.raises(ValueError):
		nhl._pull_player_list("30013002")
