def test_deploy_monthly(self):
        """
        deploy_monthly
        """
        cronpi.run_every_month("ls", isOverwrite=True).on(2, time="7:30")
        self.assertEqual(get_job_list()[0], "30 7 2 * * ls")

        cronpi.run_every_month("ls", isOverwrite=True).on(2, time="7:30am")
        self.assertEqual(get_job_list()[0], "30 7 2 * * ls")

        cronpi.run_every_month("ls", isOverwrite=True).on([2, 5], time="7:30pm")
        self.assertEqual(get_job_list()[0], "30 19 2,5 * * ls")

        cronpi.run_every_month("ls", isOverwrite=True).on([2, 5], time="17:30")
        self.assertEqual(get_job_list()[0], "30 17 2,5 * * ls")
