def test_fake_user_agent_browsers():
    ua = UserAgent(cache=False, use_cache_server=False)

    _probe(ua)

    with pytest.raises(FakeUserAgentError):
        ua.non_existing

    with pytest.raises(FakeUserAgentError):
        ua['non_existing']

    data1 = ua.data

    ua.update()

    data2 = ua.data

    assert data1 == data2

    assert data1 is not data2

