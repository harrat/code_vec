def test_load_with_pysettings_file_loads_uppercased_locales(self, pysettings_file):
        fadapter = File(pysettings_file.name)
        fadapter.load()

        assert "ABC" in fadapter.data
        assert "BUH_BUH" in fadapter.data
        assert "EASY_AS" in fadapter.data
        assert "OR_SIMPLE_AS" in fadapter.data

        assert isinstance(fadapter.data["ABC"], int)
        assert isinstance(fadapter.data["BUH_BUH"], str)
        assert isinstance(fadapter.data["EASY_AS"], list)
        assert isinstance(fadapter.data["OR_SIMPLE_AS"], dict)

        assert fadapter.data["ABC"] == 123
        assert fadapter.data["BUH_BUH"] == "BUH"
        assert fadapter.data["EASY_AS"] == ["do", "re", "mi"]
        assert fadapter.data["OR_SIMPLE_AS"] == {"do re mi": "abc, one, two, three"}
