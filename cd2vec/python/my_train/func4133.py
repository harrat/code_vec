def test_children_are_deregistered(self):
        """ Ensure that, on deregistration, DataObject types are cleared from
            the module namespace
        """
        self.mapper.deregister_all()
        self.assertEqual(len(self.mapper.MappedClasses), 0,
                         msg="No mapped classes")
