def test_load_config(self):
        #: :type port: xenavalkyrie.xena_port.XenaPort
        port = self.xm.session.reserve_ports([self.port2])[self.port2]
        port.load_config(path.join(path.dirname(__file__), 'configs', 'test_config_1.xpc'))

        assert(len(port.streams) == 2)
        assert(XenaStream.next_tpld_id == 2)

        packet = port.streams[0].get_packet_headers()
        print(packet)
        assert(packet.dst_s == '22:22:22:22:22:11')
        assert(packet.ip.dst_s == '2.2.2.1')
        packet.dst_s = '33:33:33:33:33:33'
        packet.ip.dst_s = '3.3.3.3'
        port.streams[0].set_packet_headers(packet)
        packet = port.streams[0].get_packet_headers()
        print(packet)
        assert(packet.dst_s == '33:33:33:33:33:33')
        assert(packet.ip.dst_s == '3.3.3.3')

        packet = port.streams[1].get_packet_headers()
        print(packet)
        assert(packet.dst_s == '22:22:22:22:22:22')
        assert(packet.ip6.dst_s == '22::22')
        packet.ip6.dst_s = u'33::33'
        port.streams[1].set_packet_headers(packet)
        packet = port.streams[1].get_packet_headers()
        print(packet)
        assert(packet.ip6.dst_s == '33::33')

        assert(len(port.streams[0].modifiers) == 1)
        assert(port.streams[0].modifiers[0].action == XenaModifierAction.increment)
        print(port.streams[0].modifiers[0].get_attributes())
        assert(len(port.streams[1].modifiers) == 1)
        assert(port.streams[1].modifiers[0].action == XenaModifierAction.random)
        print(port.streams[1].modifiers[0].get_attributes())
        #: :type modifier1: xenavalkyrie.xena_strea.XenaModifier
        modifier1 = port.streams[0].modifiers[0]
        assert(modifier1.min_val == 0)
        print(modifier1)
        #: :type modifier2: xenavalkyrie.xena_strea.XenaModifier
        modifier2 = port.streams[0].add_modifier(position=12)
        assert(len(port.streams[0].modifiers) == 2)
        assert(modifier2.position == 12)
        print(modifier2)
        print(port.streams[0].modifiers)

        port.streams[0].remove_modifier(0)
        assert(port.streams[0].modifiers[0].max_val == 65535)
