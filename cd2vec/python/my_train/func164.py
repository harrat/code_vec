@staticmethod
    @pytest.mark.usefixtures('isfile_true', 'import-gitlog', 'update-pytest-utc')
    def test_cli_import_reference_012(snippy):
        """Import reference based on message digest.

        Import defined reference based on message digest. In this case the
        content category is accidentally specified as 'snippet'. This should
        still import the content in reference category.
        """

        content = {
            'data': [
                Content.deepcopy(Reference.GITLOG)
            ]
        }
        content['data'][0]['links'] = ('https://updated-link.html',)
        content['data'][0]['updated'] = Content.PYTEST_TIME
        content['data'][0]['digest'] = 'fafd46eca7ca239bcbff8f1ba3e8cf806cadfbc9e267cdf6ccd3e23e356f9f8d'
        file_content = Content.get_file_content(Content.TEXT, content)
        with mock.patch('snippy.content.migrate.io.open', file_content) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'snippet', '-d', '5c2071094dbfaa33', '-f', 'one-reference.text'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, 'one-reference.text', mode='r', encoding='utf-8')
