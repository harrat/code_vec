def test_can_ignore_email_module():
    from email.utils import formatdate
    with freeze_time('2012-01-14'):
        faked_date_str = formatdate()

    before_date_str = formatdate()
    with freeze_time('2012-01-14', ignore=['email']):
        date_str = formatdate()

    after_date_str = formatdate()
    assert date_str != faked_date_str
    assert before_date_str <= date_str <= after_date_str

