@mock.patch('requests.get', side_effect=mock_pyquery)
    def test_roster_class_pulls_all_player_stats(self, *args, **kwargs):
        flexmock(utils) \
            .should_receive('_find_year_for_season') \
            .and_return('2017')
        roster = Roster('HOU')

        assert len(roster.players) == 3

        for player in roster.players:
            assert player.name in [u'Jos� Altuve', 'Justin Verlander',
                                   'Charlie Morton']
