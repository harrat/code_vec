def test_print_format(self):
        """
        Tests the dockerprettyps.print_format() method, primarily checking that the method doesnt fail, since it mostly
        just prints to the console.

        """
        assert dockerprettyps.print_format(test_ps_data.ps_containers, 6, 5, CliArgs())
