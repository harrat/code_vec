@staticmethod
    def test_flight():
        flight_id = 'TK1'
        fr = flightradar24.Api()
        flight = fr.get_flight(flight_id)
        assert flight['result']['response']['data'] is not None
        assert len(flight['result']['response']['data']) > 1  # Expect more than 100 airports
        assert flight['result']['response']['data'][0]['identification']['number']['default'] == flight_id
