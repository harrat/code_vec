def test_get_targets(self):
        # Test that it shows up when searching
        d = tempfile.mkdtemp(prefix='tmp_bupper_test_')
        os.makedirs(join(d, 'some/other'))

        # ensure it picks up on nothing
        assert list(bupper.get_targets(join(d))) == []

        # now check that it gets it correctly
        open(join(d, 'some/other/_BACKUP_THIS'), 'w+').write(' ')
        assert list(bupper.get_targets(join(d))) == [join(d, 'some/other')]

        # clean up test
        os.remove(join(d, 'some/other/_BACKUP_THIS'))
        os.removedirs(join(d, 'some/other'))
