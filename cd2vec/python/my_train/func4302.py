@staticmethod
    @pytest.mark.usefixtures('import-nginx')
    def test_cli_delete_solution_005(snippy):
        """Delete solution with digest.

        Delete solution with empty message digest when there is only one
        content stored. In this case the last content can be deleted with
        empty digest.
        """

        Content.assert_storage_size(1)
        cause = snippy.run(['snippy', 'delete', '--scat', 'solution', '-d', ''])
        assert cause == Cause.ALL_OK
        Content.assert_storage(None)
