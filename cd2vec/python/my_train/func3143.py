def test_sign_and_collect(cert_and_key, ip_address):
    """Sign call and then collect with the returned orderRef UUID."""

    c = bankid.BankIDJSONClient(certificates=cert_and_key, test_server=True)
    out = c.sign(
        ip_address,
        "The data to be signed",
        personal_number=_get_random_personal_number(),
        user_non_visible_data="Non visible data",
    )
    assert isinstance(out, dict)
    # UUID.__init__ performs the UUID compliance assertion.
    order_ref = uuid.UUID(out.get("orderRef"), version=4)
    collect_status = c.collect(out.get("orderRef"))
    assert collect_status.get("status") == "pending"
    assert collect_status.get("hintCode") in ("outstandingTransaction", "noClient")

