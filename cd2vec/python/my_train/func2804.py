def test_save_load_file(query):
    """
    Test saving and reloading a query as a pickle file.
    """

    # test exception handling
    testfilebad = '/jkshfdjfd/jkgsdfjkj/kgskfd.jhfd'

    with pytest.raises(IOError):
        query.save(testfilebad)

    # test exception handling
    with pytest.raises(IOError):
        querynew = QueryATNF(loadquery=testfilebad)

    testfile = os.path.join(os.getcwd(), 'query.pkl')
    query.save(testfile)

    # re-load in as a new query
    querynew = QueryATNF(loadquery=testfile)

    assert query.num_pulsars == querynew.num_pulsars

