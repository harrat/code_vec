def test_fixup_pth_file(self):
        '''Prior to version 0.2.7, the method fixup_pth_file() wrongly wrote unicode files'''

        content = os.linesep.join(('/usr/local/bin/someFile', '/tmp/xyzzy', '/usr/local/someOtherFile', ''))
        pth = '/tmp/test_fixup_pth_file.pth'

        with open(pth, 'w') as fd:
            fd.write(content)

        # Find size of file - should be plain ASCII/UTF-8...

        org_stat = os.stat(pth)
        assert len(content) == org_stat[stat.ST_SIZE]

        # Now sleep for 2 seconds, then call fixup_pth_file(). This should ensure that the stat.ST_MTIME has
        # changed if the file has been changed/rewritten

        time.sleep(2)
        clonevirtualenv.fixup_pth_file(pth, '/usr/local', '/usr/xyzzy')
        new_stat = os.stat(pth)
        assert org_stat[stat.ST_MTIME] != new_stat[stat.ST_MTIME]  # File should have changed
        assert org_stat[stat.ST_SIZE] == new_stat[stat.ST_SIZE]  # Substituting local->xyzzy - size should be the same

        os.remove(pth)
