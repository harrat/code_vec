@staticmethod
    @pytest.mark.usefixtures('import-kafka', 'update-kafka-utc')
    def test_cli_update_solution_013(snippy, editor_data):
        """Update solution with editor.

        Update existing solution by explicitly defining content format as
        Markdown. In this case the content is not changed at all. In this
        case the solution is stored originally in text format. The content
        must be convertd to Markdown format when displayed in editor.
        """

        content = {
            'data': [
                Solution.KAFKA
            ]
        }
        template = Content.dump_mkdn(content['data'][0])
        editor_data.return_value = template
        cause = snippy.run(['snippy', 'update', '-d', 'ee3f2ab7c63d6965', '--format', 'mkdn'])
        editor_data.assert_called_with(template)
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
