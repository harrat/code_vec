def test_get_filepath_with_config_path(self):
        """Test _get_filepath method with TEST_CONFIG_PATH env var set."""
        gbr_config_path = '/var/lib/gbr/'
        os.environ['TEST_CONFIG_PATH'] = gbr_config_path
        conf = TESTConfig()
        with mock.patch('yamlconf.config.os.path.exists') as mock_exists:
            # TEST_CONFIG_PATH env var set, no filename, not found in basepath
            mock_exists.side_effect = [False, True]
            expected = os.path.join(TEST_CONFIG_ROOT, 'config.yaml')
            result = conf._get_filepath()
            self.assertEqual(expected, result)
