def test_patch_existing_resource(self):
        """Send HTTP PATCH for an existing resource (should be updated)."""
        response = self.app.patch('/artists/275',
                content_type='application/json',
                data=json.dumps({u'Name': u'Jeff Knupp'}))
        assert response.status_code == 204
        response = self.get_response('/artists/275', 200)
        assert json.loads(
                response.get_data(as_text=True))[u'Name'] == u'Jeff Knupp'
        assert json.loads(
                response.get_data(as_text=True))[u'ArtistId'] == 275
