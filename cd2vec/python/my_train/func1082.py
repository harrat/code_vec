def test_populate_template_snapshot_false(monkeypatch):
    monkeypatch.setattr('processor.template_processor.base.base_template_processor.config_value', mock_config_value)
    monkeypatch.setattr('processor.template_processor.base.base_template_processor.get_collection_size', mock_get_collection_size)
    monkeypatch.setattr('processor.template_processor.base.base_template_processor.create_indexes', mock_create_indexes)
    monkeypatch.setattr('processor.template_processor.base.base_template_processor.insert_one_document', mock_insert_one_document)
    from processor.template_processor.base.base_template_processor import TemplateProcessor

    template_processor = TemplateProcessor(node_data, **template_processor_kwargs)
    snapshot_data = template_processor.populate_template_snapshot()
    assert snapshot_data == {
		'SNAPSHOT_1': False
	}
