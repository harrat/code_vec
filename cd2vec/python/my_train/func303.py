@staticmethod
    @pytest.mark.parametrize('snippy', [['-vv', '--log-msg-max', '200']], indirect=True)
    def test_very_verbose_option_002(snippy, caplog, capsys):
        """Test printing logs with the very verbose option.

        Enable verbose logging with ``-vv`` option. In this case the message
        lenght is defined from command line. Test checks that there are more
        than a randomly picked largish number of logs to avoid matching log
        count explicitly.

        Nothing must be printed to stderr.
        """

        cause = snippy.run(['snippy', 'search', '--sall', '.', '-vv', '--log-msg-max', '200'])
        snippy.release()
        out, err = capsys.readouterr()
        assert cause == 'NOK: cannot find content with given search criteria'
        assert 'log msg max: 200' in out
        assert len(out.splitlines()) > 10
        assert len(caplog.records) > 10
        assert not err
