def test_flag_resolve(mock_dng, tmp_path):
    dng = pydng.DNGConverter(tmp_path)
    mock_flag = True
    result = dng.resolve_flag("mf", mock_flag)
    assert result == "-mf"
    mock_flag = False
    result = dng.resolve_flag("mf", mock_flag)
    assert result == ""
    mock_flag = True
    result = dng.resolve_flag("mf", mock_flag, on_true=lambda x: "MOCK")
    assert result == "MOCK"

