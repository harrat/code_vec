@staticmethod
    @pytest.mark.usefixtures('snippy', 'create-beats-utc')
    def test_cli_create_solution_010(snippy):
        """Create solution from command line.

        Create new solution by defining all content parameters from command
        line. In this case content field values contain duplicates that must
        not be used when the content is stored. Only unique values must be
        used.
        """

        content = {
            'data': [
                Content.deepcopy(Solution.BEATS)
            ]
        }
        content['data'][0]['description'] = ''
        content['data'][0]['filename'] = ''
        content['data'][0]['uuid'] = Content.UUID1
        content['data'][0]['digest'] = '509c7e11b568283ae985ff038cbabbe94e9cea8b058215b243ae0463b0497e5c'
        data = Const.DELIMITER_DATA.join(content['data'][0]['data'])
        brief = content['data'][0]['brief']
        groups = Const.DELIMITER_GROUPS.join(content['data'][0]['groups']) + ',beats'
        tags = Const.DELIMITER_TAGS.join(content['data'][0]['tags']) + ',howto,filebeat'
        links = Const.DELIMITER_LINKS.join(content['data'][0]['links']) + ' https://www.elastic.co/guide/en/beats/filebeat/master/enable-filebeat-debugging.html'  # pylint: disable=line-too-long
        cause = snippy.run(['snippy', 'create', '--scat', 'solution', '--content', data, '--brief', brief, '--groups', groups, '--tags', tags, '--links', links, '--format', 'text'])  # pylint: disable=line-too-long
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
