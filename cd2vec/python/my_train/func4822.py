@pytest.mark.vcr()
def test_scan_export_filter_type_typeerror(api):
    with pytest.raises(TypeError):
        api.scans.export(1, filter_type=1)
