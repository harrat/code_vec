def test_bad_keywords(self):
        with self.assertRaises(TypeError):
            self._call_function_under_test(huh='bad-kw')
