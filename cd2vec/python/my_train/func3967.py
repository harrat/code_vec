def test_duplicated_edge(self):
        with pytest.raises(EdgeDuplicatedException):
            assert self.graph.add_edge("b", "c")
