def test_record_scraps_collection_dataframe(notebook_backwards_result):
    expected_df = pd.DataFrame(
        [
            ("hello", "world", "json", None),
            ("number", 123, "json", None),
            ("some_list", [1, 3, 5], "json", None),
            ("some_dict", {"a": 1, "b": 2}, "json", None),
            ("some_display", None, "display", AnyDict()),
        ],
        columns=["name", "data", "encoder", "display"],
    )
    print(notebook_backwards_result.scraps.dataframe)
    assert_frame_equal(notebook_backwards_result.scraps.dataframe, expected_df, check_exact=True)

