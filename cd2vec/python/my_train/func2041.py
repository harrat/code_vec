def test_mountebank_multiple_simple_impostors():
    test_port = port_for.select_random()
    test_response_1 = 'Just some response body (that I used to know)'
    test_response_2 = '{"Hey": "a JSON!"}'
    stub_1 = HttpStub(method='PUT', path='/path-1', status_code=201, response=test_response_1)
    stub_2 = HttpStub(method='POST', path='/path-2', status_code=202, response=test_response_2)

    with Mountebank() as mb:
        mb.add_multi_stub_imposter_simple(
            port=test_port,
            stubs=[stub_1, stub_2]
        )
