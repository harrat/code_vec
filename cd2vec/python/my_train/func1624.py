@staticmethod
    @pytest.mark.usefixtures('isfile_true')
    def test_cli_import_reference_020(snippy):
        """Import all reference resources.

        Import all references from Markdown formatted file.
        """

        content = {
            'data': [
                Reference.GITLOG,
                Reference.REGEXP
            ]
        }
        file_content = Content.get_file_content(Content.MKDN, content)
        with mock.patch('snippy.content.migrate.io.open', file_content, create=True) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'reference', '-f', './all-references.mkdn'])
            assert cause == Cause.ALL_OK
            Content.assert_storage(content)
            Content.assert_arglist(mock_file, './all-references.mkdn', mode='r', encoding='utf-8')
