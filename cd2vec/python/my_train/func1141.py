@staticmethod
    @pytest.mark.usefixtures('default-solutions')
    def test_cli_delete_solution_009(snippy):
        """Delete solution with data.

        Delete solution based on content data.
        """

        content = {
            'data': [
                Solution.BEATS
            ]
        }
        Content.assert_storage_size(2)
        data = '\n'.join(Solution.NGINX['data'])
        cause = snippy.run(['snippy', 'delete', '--scat', 'solution', '--content', data])
        assert cause == Cause.ALL_OK
        Content.assert_storage(content)
