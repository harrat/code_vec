def test_le(self):
        r = ruler.FileSizeRule("<= 1.5m")
        self.assertTrue(r.match(self.f1))
        self.assertTrue(r.match(self.f2))
        self.assertFalse(r.match(self.f3))
