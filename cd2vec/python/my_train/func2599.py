@staticmethod
    @pytest.mark.usefixtures('import-pytest', 'update-regexp-utc')
    def test_cli_import_reference_015(snippy):
        """Import reference based on message uuid.

        Try to import defined reference with uuid that cannot be found.
        """

        content = {
            'data': [
                Reference.PYTEST
            ]
        }
        updates = {
            'data': [
                Reference.GITLOG
            ]
        }
        file_content = Content.get_file_content(Content.TEXT, updates)
        with mock.patch('snippy.content.migrate.io.open', file_content, create=True) as mock_file:
            cause = snippy.run(['snippy', 'import', '--scat', 'reference', '-u', '1234567', '-f', 'one-reference.text'])
            assert cause == 'NOK: cannot find content with content uuid: 1234567'
            Content.assert_storage(content)
            mock_file.assert_not_called()
