def test_not_implements_protected_access():
    """
    Case: do not implement interface member with mismatched protected access.
    Expect: class mismatches interface member access modifier error message.
    """
    class HumanSoulInterface:

        @protected
        @classmethod
        @custom_decorator
        def die(cls, when, *args, **kwargs):
            pass

    with pytest.raises(ImplementedInterfaceMemberHasIncorrectAccessModifierException) as error:

        @implements(HumanSoulInterface)
        class HumanSoul:

            @classmethod
            @custom_decorator
            def die(cls, when, *args, **kwargs):
                pass

    assert 'HumanSoul.die(cls, when, args, kwargs) mismatches ' \
           'HumanSoulInterface.die() member access modifier.' == error.value.message
