def test_save_load_play(self):
        oldseed = agents.seed
        agents.seed = 123
        p1 = PpoAgent(gym_env_name=_line_world_name, fc_layers=(10, 20, 30), backend='tfagents')
        p1.train(callbacks=[duration._SingleEpisode()], default_plots=False)
        d = p1.save()
        agents.seed = oldseed
        p2: EasyAgent = agents.load(d)
        self.assert_are_equal(p1, p2)
        p2.play(default_plots=False, num_episodes=1)
