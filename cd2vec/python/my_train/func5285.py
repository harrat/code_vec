def test_locale(self):
        # confirm that files will load properly with non-US locales
        locale.setlocale(locale.LC_ALL, 'de_DE.utf8')

        gp = parser.GamryParser(filename='tests/chronoa_de_data.dta')
        gp.load()
        curve = gp.get_curve_data()
        self.assertEqual(curve['T'].iloc[-1], 270)
        self.assertEqual(curve['Vf'].iloc[0], -5e-004)
        self.assertEqual(curve['Vf'].iloc[-1], 4e-001)
        self.assertEqual(curve['Im'].iloc[0], -2e-008)
        self.assertEqual(curve['Im'].iloc[-1], 3e-009)

        # confirm that files can load with mismatched locales..
        # pandas.read_csv() will handle 1,234,567.890 notation even in the wrong locale.
        gp = parser.GamryParser(filename='tests/chronoa_data.dta')
        gp.load()
        curve = gp.get_curve_data()
        self.assertEqual(curve['T'].iloc[-1], 270)
        self.assertEqual(curve['Vf'].iloc[0], -5.4e-004)
        self.assertEqual(curve['Vf'].iloc[-1], 4e-001)
        self.assertEqual(curve['Im'].iloc[-1], 3e-009)

        # pandas.read_csv() assumes 1,234,567.890 notation by default.
        # If parsed with the wrong locale, we should expect data corruption
        # in the resulting dataframe.
        locale.setlocale(locale.LC_ALL, 'en_US.utf8')
        gp = parser.GamryParser(filename='tests/chronoa_de_data.dta')
        gp.load()
        curve = gp.get_curve_data()
        self.assertEqual(curve['Vf'].iloc[0], -50)
        self.assertEqual(curve['Vf'].iloc[-1], 40000)
        self.assertEqual(curve['Im'].iloc[0], -0.002)
