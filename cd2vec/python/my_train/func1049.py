def test_delete_attribute_with_invalid_attribute_index(self):
        """
        Test that an ItemNotFound error is raised when attempting to delete
        an attribute when specifying an invalid attribute index.
        """
        e = engine.KmipEngine()
        e._data_store = self.engine
        e._data_store_session_factory = self.session_factory
        e._data_session = e._data_store_session_factory()
        e._is_allowed_by_operation_policy = mock.Mock(return_value=True)
        e._logger = mock.MagicMock()

        secret = pie_objects.SymmetricKey(
            enums.CryptographicAlgorithm.AES,
            0,
            b''
        )

        e._data_session.add(secret)
        e._data_session.commit()
        e._data_session = e._data_store_session_factory()

        args = (
            payloads.DeleteAttributeRequestPayload(
                unique_identifier="1",
                attribute_name="Name",
                attribute_index=20),
        )

        self.assertRaisesRegex(
            exceptions.ItemNotFound,
            "Could not locate the attribute instance with the specified "
            "index: 20",
            e._process_delete_attribute,
            *args
        )
