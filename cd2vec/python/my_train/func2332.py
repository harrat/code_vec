def test_fetch_all_datasets(self):

        import lazygrid as lg

        datasets = lg.datasets.fetch_datasets(task="classification", update_data=True)
        self.assertTrue(len(datasets) >= 717)
