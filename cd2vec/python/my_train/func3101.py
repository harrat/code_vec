def test_copy_positive02(self):
        train_texts = [
            'add Stani, stani Ibar vodo songs in my playlist m�sica libre',
            'add this album to my Blues playlist',
            'Add the tune to the Rage Radio playlist.',
            'Add WC Handy to my Sax and the City playlist',
            'Add BSlade to women of k-pop playlist',
            'Book a reservation for seven people at a bakery in Osage City',
            'Book spot for three at Maid-Rite Sandwich Shop in Antigua and Barbuda',
            'I need a table for breakfast in MI at the pizzeria',
            'Book a restaurant reservation for me and my child for 2 Pm in Faysville',
            'I want to book a highly rated churrascaria ten months from now.',
            'How\'s the weather in Munchique National Natural Park',
            'Tell me the weather forecast for France',
            'Will there be wind in Hornitos DC?',
            'Is it warm here now?',
            'what is the forecast for Roulo for foggy conditions on February the eighteenth, 2018',
            'I\'d like to hear music that\'s popular from Trick-trick on the Slacker service',
            'Play Making Out by Alexander Rosenbaum off Google Music.',
            'I want to hear Pamela Jintana Racine from 1986 on Lastfm',
            'is there something new you can play by Lola Monroe',
            'I want to hear something from Post-punk Revival',
            'Rate All That Remains a five Give this album 4 points',
            'Give The Best Mysteries of Isaac Asimov four stars out of 6.',
            'Rate this current novel 1 out of 6 points.',
            'Give this textbook 5 points',
            'Give this series 0 out of 6 stars',
            'Please help me find the Bloom: Remix Album song.',
            'Find me the soundtrack called Enter the Chicken',
            'Can you please search Ellington at Newport?',
            'Please find me the Youth Against Fascism television show.',
            'Find me the book called Suffer',
            'Find movie times for Landmark Theatres.',
            'What are the movie times for Amco Entertainment',
            'what films are showing at Bow Tie Cinemas',
            'Show me the movies close by',
            'I want to see The Da Vinci Code',
            'Paleo-Indians migrated from Siberia to the North American mainland at least 12,000 years ago.',
            'Hello, world!',
            'Originating in U.S. defense networks, the Internet spread to international academic networks',
            'The WHO is a member of the United Nations Development Group.',
            'In 443, Geneva was taken by Burgundy.',
            'How are you?',
            'Don\'t mention it!',
            'I communicate a lot with advertising and media agencies.',
            'Hey, good morning, peasant!',
            'Neural networks can actually escalate or amplify the intensity of the initial signal.',
            'I was an artist.',
            'He\'s a con artist?among other things.',
            'Application area: growth factors study, cell biology.',
            'Have you taken physical chemistry?',
            'London is the capital of Great Britain'
        ]
        train_labels = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6,
                        6, 6, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
        valid_texts = [
            "I'd like to have this track onto my Classical Relaxations playlist.",
            'Add the album to my Flow Espa�ol playlist.',
            'Book a reservation for my babies and I',
            'need a table somewhere in Quarryville 14 hours from now',
            'what is the weather here',
            'What kind of weather is forecast in MS now?',
            'Please play something catchy on Youtube',
            'The East Slavs emerged as a recognizable group in Europe between the 3rd and 8th centuries AD.',
            'The Soviet Union played a decisive role in the Allied victory in World War II.',
            'Most of Northern European Russia and Siberia has a subarctic climate'
        ]
        valid_labels = [0, 0, 1, 1, 2, 2, 4, -1, -1, -1]
        self.cls = ImpartialTextClassifier(random_seed=0, batch_size=4)
        self.cls.fit(train_texts, train_labels, validation_data=(valid_texts, valid_labels))
        self.another_cls = copy.copy(self.cls)
        self.assertIsInstance(self.another_cls, ImpartialTextClassifier)
        self.assertIsNot(self.cls, self.another_cls)
        self.assertTrue(hasattr(self.another_cls, 'batch_size'))
        self.assertTrue(hasattr(self.another_cls, 'filters_for_conv1'))
        self.assertTrue(hasattr(self.another_cls, 'filters_for_conv2'))
        self.assertTrue(hasattr(self.another_cls, 'filters_for_conv3'))
        self.assertTrue(hasattr(self.another_cls, 'filters_for_conv4'))
        self.assertTrue(hasattr(self.another_cls, 'filters_for_conv5'))
        self.assertTrue(hasattr(self.another_cls, 'hidden_layer_size'))
        self.assertTrue(hasattr(self.another_cls, 'n_hidden_layers'))
        self.assertTrue(hasattr(self.another_cls, 'num_monte_carlo'))
        self.assertTrue(hasattr(self.another_cls, 'bert_hub_module_handle'))
        self.assertTrue(hasattr(self.another_cls, 'max_epochs'))
        self.assertTrue(hasattr(self.another_cls, 'patience'))
        self.assertTrue(hasattr(self.another_cls, 'random_seed'))
        self.assertTrue(hasattr(self.another_cls, 'gpu_memory_frac'))
        self.assertTrue(hasattr(self.another_cls, 'validation_fraction'))
        self.assertTrue(hasattr(self.another_cls, 'verbose'))
        self.assertTrue(hasattr(self.another_cls, 'multioutput'))
        self.assertTrue(hasattr(self.another_cls, 'bayesian'))
        self.assertTrue(hasattr(self.another_cls, 'kl_weight_init'))
        self.assertTrue(hasattr(self.another_cls, 'kl_weight_fin'))
        self.assertTrue(hasattr(self.another_cls, 'tokenizer_'))
        self.assertTrue(hasattr(self.another_cls, 'classes_'))
        self.assertTrue(hasattr(self.another_cls, 'classes_reverse_index_'))
        self.assertTrue(hasattr(self.another_cls, 'sess_'))
        self.assertTrue(hasattr(self.another_cls, 'certainty_threshold_'))
        self.assertEqual(self.cls.batch_size, self.another_cls.batch_size)
        self.assertEqual(self.cls.num_monte_carlo, self.another_cls.num_monte_carlo)
        self.assertAlmostEqual(self.cls.filters_for_conv1, self.another_cls.filters_for_conv1)
        self.assertAlmostEqual(self.cls.filters_for_conv2, self.another_cls.filters_for_conv2)
        self.assertAlmostEqual(self.cls.filters_for_conv3, self.another_cls.filters_for_conv3)
        self.assertAlmostEqual(self.cls.filters_for_conv4, self.another_cls.filters_for_conv4)
        self.assertAlmostEqual(self.cls.filters_for_conv5, self.another_cls.filters_for_conv5)
        self.assertAlmostEqual(self.cls.hidden_layer_size, self.another_cls.hidden_layer_size)
        self.assertAlmostEqual(self.cls.n_hidden_layers, self.another_cls.n_hidden_layers)
        self.assertEqual(self.cls.bert_hub_module_handle, self.another_cls.bert_hub_module_handle)
        self.assertEqual(self.cls.max_epochs, self.another_cls.max_epochs)
        self.assertEqual(self.cls.patience, self.another_cls.patience)
        self.assertEqual(self.cls.random_seed, self.another_cls.random_seed)
        self.assertAlmostEqual(self.cls.gpu_memory_frac, self.another_cls.gpu_memory_frac)
        self.assertAlmostEqual(self.cls.validation_fraction, self.another_cls.validation_fraction)
        self.assertEqual(self.cls.verbose, self.another_cls.verbose)
        self.assertEqual(self.cls.multioutput, self.another_cls.multioutput)
        self.assertEqual(self.cls.bayesian, self.another_cls.bayesian)
        self.assertAlmostEqual(self.cls.kl_weight_init, self.another_cls.kl_weight_init)
        self.assertAlmostEqual(self.cls.kl_weight_fin, self.another_cls.kl_weight_fin)
        self.assertAlmostEqual(self.cls.certainty_threshold_, self.another_cls.certainty_threshold_, places=9)
        self.assertEqual(self.cls.classes_, self.another_cls.classes_)
        self.assertEqual(self.cls.classes_reverse_index_, self.another_cls.classes_reverse_index_)
        y_pred = self.cls.predict(valid_texts)
        y_pred_another = self.another_cls.predict(valid_texts)
        self.assertIsInstance(y_pred, list)
        self.assertIsInstance(y_pred_another, list)
        self.assertEqual(len(y_pred), len(y_pred_another))
