def test_get_config_data():
    configdata = get_config_data(None)
    assert configdata is None
    configdata = get_config_data('/tmp/asdxz.ini')
    assert configdata is None
    tests_configini = '%s/config.ini' % TESTSDIR
    tests_configdata = get_config_data(tests_configini)
    assert tests_configdata is not None
    prod_configdata = get_config_data(framework_config())
    assert prod_configdata is not None
    assert tests_configdata == prod_configdata

