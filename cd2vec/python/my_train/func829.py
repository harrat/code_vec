def test_run_continuous_raise(
    selector: Type[MockSelector], proc: ContinuousSSH, caplog: Any, popen: Type[MockPopen]
) -> None:
    popen._raise = True
    selector.events = [selectors.EVENT_READ] * 3
    popen._stdout = b"Entering interactive session\ndebug1: some other message\ndone"
    with pytest.raises(MockPopenException):
        proc._run_once()

    assert list(get_transitions(proc)) == ["connecting", "connected", "disconnected"]

