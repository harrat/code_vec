def test_remove(mbed, testrepos):
    with cd('test1'):
        popen(['python', mbed, 'remove', 'test2'])

    assertls(mbed, 'test1', [
        "[mbed]",
        "test1",
    ])
