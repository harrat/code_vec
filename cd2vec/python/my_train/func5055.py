def test_no_ident(self):

		model.factory.auto_assign_id = True
		p1 = model.Person()	# auto assigned	 
		p2 = model.Person(ident=None) # auto assigned
		p3 = model.Person(ident="") # bnode explicitly

		self.assertTrue(p1.id.startswith('http'))
		self.assertTrue(p2.id.startswith('http'))
		self.assertEqual(p3.id, '')

		model.factory.auto_assign_id = False
		p4 = model.Person() # bnode is default
		p5 = model.Person(ident=None) # bnode is default
		p6 = model.Person(ident="") # bnode explicitly

		self.assertEqual(p4.id, '')
		self.assertEqual(p5.id, '')
		self.assertEqual(p6.id, '')
