def test_create_from_element(self):
        element = BeautifulSoup(PAGE.strip(), "html.parser")
        instance = Paper.create_from_page(element, with_arxiv=True)
        self._validate(instance)
