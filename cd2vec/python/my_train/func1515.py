def test_cast_attr(person: Person):
    assert isinstance(person.cast, list)
    item, credit = person.cast[0]
    assert isinstance(item, (Show, Movie))
    assert isinstance(credit, Credit)

