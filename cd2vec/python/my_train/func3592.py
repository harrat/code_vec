def test_init_already_initialized(self, test_cli_stash):
        result = _invoke('init_stash "{0}" -p {1}'.format(
            os.environ['GHOST_STASH_PATH'], test_cli_stash.passphrase))
        assert 'Stash already initialized' in result.output
        assert result.exit_code == 0
