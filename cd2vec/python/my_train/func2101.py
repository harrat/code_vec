def test_get_penn_treebank(self):
        raw = get_penn_treebank()
        params = [('train', 42_068), ('dev', 3_370), ('test', 3_761)]
        for key, size in params:
            with self.subTest(key=key, size=size):
                self.assertIn(key, raw)
                self.assertEqual(len(raw[key]), size)
                self.assertEqual(len(PennTreebank(split=key)), size)
