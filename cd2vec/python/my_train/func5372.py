def test_extract_archive():
    archive_file_zip = "/tmp/ruts_download/stopwords.zip"
    archive_file_tar = "/tmp/ruts_download/razdel.tar.gz"
    extract_dir = "/tmp/ruts_extract"
    assert extract_archive(archive_file_zip, extract_dir=extract_dir) == "/tmp/ruts_extract/stopwords"
    assert extract_archive(archive_file_tar) == "/tmp/ruts_download/razdel"
    assert extract_archive("/tmp/ruts_extract/stopwords/russian") == "/tmp/ruts_extract/stopwords"
    shutil.rmtree("/tmp/ruts_extract", ignore_errors=True)
    shutil.rmtree("/tmp/ruts_download", ignore_errors=True)

def test_safe_divide():
    assert safe_divide(1, 5) == 0.2
    assert safe_divide(1, 0) == 0
    assert safe_divide(1, '', -1) == -1
