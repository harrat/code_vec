def test_aggregator_keys_method_with_empty_nodes(mocked_aggregated):
    """Test aggregator keys method with empty nodes."""

    assert mocked_aggregated.keys('empty') == []

