def test_xiami_music_link():

    result = XiaMi(url="https://www.xiami.com/song/1459299", use_id=False).__repr__()

    assert "Jo Dee Messina" in result

    assert "I Know a Heartache When I See One" in result

    assert 'http://m128.xiami.net' in result  # domain

