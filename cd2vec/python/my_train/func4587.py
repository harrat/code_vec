def test_get_features_feature_metric():
    # "voltage traces" that are constant at -70*mV, -60mV, -50mV, -40mV for
    # 50ms each.
    voltage_target = np.ones((2, 200)) * np.repeat([-70, -60, -50, -40], 50) * mV
    dt = 1*ms
    # The results for the first and last "parameter set" are too high/low, the
    # middle one is perfect
    voltage_model = np.ones((3, 2, 200)) * np.repeat([-70, -60, -50, -40], 50) * mV
    voltage_model[0, 0, :] += 2.5*mV
    voltage_model[0, 1, :] += 5*mV
    voltage_model[2, 0, :] -= 2.5*mV
    voltage_model[2, 1, :] -= 5*mV

    inp_times = [[99 * ms, 150 * ms], [49 * ms, 150 * ms]]

    # Default comparison: absolute difference
    # Check that FeatureMetric rejects the normalization argument
    with pytest.raises(ValueError):
        feature_metric = FeatureMetric(inp_times, ['voltage_base'],
                                       normalization=2)
    feature_metric = FeatureMetric(inp_times, ['voltage_base'])
    results = feature_metric.get_features(voltage_model, voltage_target, dt=dt)
    assert len(results) == 3
    assert all(isinstance(r, dict) for r in results)
    assert all(r.keys() == {'voltage_base'} for r in results)
    assert_almost_equal(results[0]['voltage_base'], np.array([2.5*mV, 5*mV]))
    assert_almost_equal(results[1]['voltage_base'], [0, 0])
    assert_almost_equal(results[2]['voltage_base'], np.array([2.5*mV, 5*mV]))

    # Custom comparison: squared difference
    feature_metric = FeatureMetric(inp_times, ['voltage_base'],
                                   combine=lambda x, y: (x - y)**2)
    results = feature_metric.get_features(voltage_model, voltage_target, dt=dt)
    assert len(results) == 3
    assert all(isinstance(r, dict) for r in results)
    assert all(r.keys() == {'voltage_base'} for r in results)
    assert_almost_equal(results[0]['voltage_base'], np.array([(2.5*mV)**2, (5*mV)**2]))
    assert_almost_equal(results[1]['voltage_base'], [0, 0])
    assert_almost_equal(results[2]['voltage_base'], np.array([(2.5*mV)**2, (5*mV)**2]))

