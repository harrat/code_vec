def test_getters(self):
        gp = parser.VFP600()
        gp.load('tests/vfp600_data.dta')

        curve = gp.get_curve_data()
        self.assertTrue((curve.columns == ['T', 'Voltage', 'Current']).all())

        self.assertEqual(curve['T'][0], 0)
        self.assertEqual(round(curve['T'].iloc[-1] * 100), 127)
        self.assertEqual(curve['Voltage'].iloc[-1], 0.033333)
        self.assertEqual(round(curve['Current'].iloc[-1] * 1E13), 5125)
