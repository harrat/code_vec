def test_get_python_runtime_image(valid_template, namespace):
    template_path, obj, metadata = handler.load(valid_template, namespace)
    runtime, image = get_python_runtime_image(obj)

    assert runtime == "python3.7"
    assert image == "lambci/lambda:build-python3.7"

