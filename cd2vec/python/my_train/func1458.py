def test_consumer_with_dynamodb(self):

        async def read():
            cnt = 0
            async for shard_reader in self.consumer.get_shard_readers():
                print('Got shard reader: {}'.format(shard_reader))
                try:
                    async for record in shard_reader.get_records():
                        self.test_data.append(record[0]['Data'])
                except ShardClosedException:
                    print('Got ShardClosedException; cnt={}'.format(cnt))
                    if cnt > 1:
                        # We should get second shard reader after first one gets closed
                        # However we signal mocked method to stop returning shards after that
                        self.consumer_mock.shard_closed = True
                    cnt += 1

        async def stop_test():
            await asyncio.sleep(4)
            self.consumer.stop()
            self.assertEqual(3, len(self.test_data))
            self.assertEqual('xxxx', self.test_data[0])
            self.assertTrue(self.consumer_mock.shard_closed)

            dynamo_record = self.consumer_mock.dynamodb_mock.items.get('Shard-0000')
            self.assertEqual(self.host_key, dynamo_record.get('fqdn'))
            self.assertEqual(100000000000000000000000000, dynamo_record.get('superseq'))
            self.assertEqual(2, dynamo_record.get('subseq'))
            self.assertTrue(int(time.time()) <= dynamo_record.get('expires'))
            self.assertEqual('AT_SEQUENCE_NUMBER', self.consumer_mock.iterator_kwargs.get('ShardIteratorType'))
            self.assertEqual(
                '100000000000000000000000000001', self.consumer_mock.iterator_kwargs.get('StartingSequenceNumber'))

        async def test():
            await asyncio.gather(
                asyncio.ensure_future(stop_test()),
                asyncio.ensure_future(read())
            )

        self.event_loop.run_until_complete(test())
