def test_allows_multiple(self):
		p = model.Person()
		self.assertTrue(p.allows_multiple('classified_as'))
		self.assertFalse(p.allows_multiple('born'))
		self.assertRaises(model.DataError, p.allows_multiple, 'fish')
