def test_command_line_interface(self, tmp_path):
        note_1 = Note("Note 1")
        expected_test_value = "expected_test_value"
        note_1.parameters["find_this_value"] = expected_test_value
        note_2 = Note("Note 2")
        dt_1 = datetime(2019, 1, 3, 10, 0, 1)
        dt_2 = datetime(2019, 2, 25, 12, 10, 0)
        note_2.info["some_dates"] = [dt_1, dt_2]
        store_path = tmp_path / "test_store.json"
        store = Store(store_path)
        store.add(note_1)
        store.add(note_2)

        port = 8080
        p = mp.Process(
            target=main, args=([str(store_path), "--port", str(port), "--no-browser"],)
        )
        try:
            p.start()
            time.sleep(1)
            html = requests.get(f"http://localhost:{port}").text
            self.validate_html(
                html,
                [expected_test_value, _format_datetime(dt_1), _format_datetime(dt_2)],
            )
        finally:
            p.terminate()
