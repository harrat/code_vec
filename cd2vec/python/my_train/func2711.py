@staticmethod
    @pytest.mark.usefixtures('default-snippets')
    def test_cli_update_snippet_006(snippy):
        """Update snippet with ``--digest`` option.

        Try to update snippet with message digest that cannot be found. No
        changes must be made to stored content.
        """

        content = {
            'data': [
                Snippet.REMOVE,
                Snippet.FORCED
            ]
        }
        cause = snippy.run(['snippy', 'update', '-d', '123456789abcdef0', '--format', 'text'])
        assert cause == 'NOK: cannot find content with message digest: 123456789abcdef0'
        Content.assert_storage(content)
