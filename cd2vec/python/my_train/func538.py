def test_get_response_body_empty(self):
        # Redirects to https with empty body
        self._make_request('http://www.python.org')
        redirect_request = self.client.get_requests()[0]

        body = self.client.get_response_body(redirect_request['id'])

        self.assertEqual(b'', body)
