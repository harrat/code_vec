@staticmethod
    @pytest.mark.usefixtures('import-forced', 'update-remove-utc')
    def test_api_create_snippet_014(server):
        """Update snippet with POST that maps to PATCH.

        Send POST /snippets with the ``X-HTTP-Method-Override`` header to
        update a resource. In this case the resource exists and the content
        is updated.

        In this case the resource ``created`` attribute must remain in the
        initial value and the ``updated`` attribute must be set to reflect
        the update time.

        The ``uuid`` attribute must not be changed from it's initial value.
        """

        storage = {
            'data': [
                Storage.forced
            ]
        }
        storage['data'][0]['data'] = Snippet.REMOVE['data']
        storage['data'][0]['updated'] = Content.FORCED_TIME
        storage['data'][0]['uuid'] = Snippet.FORCED_UUID
        storage['data'][0]['digest'] = 'a9e137c08aee09852797a974ef91b871c48915fecf25b2e89c5bdba4885b2bd2'
        request_body = {
            'data': {
                'type': 'snippet',
                'attributes': {
                    'data': Snippet.REMOVE['data']
                }
            }
        }
        expect_headers = {
            'content-type': 'application/vnd.api+json; charset=UTF-8',
            'content-length': '903'
        }
        expect_body = {
            'links': {
                'self': 'http://falconframework.org/api/snippy/rest/snippets/' + Snippet.FORCED_UUID
            },
            'data': {
                'type': 'snippet',
                'id': Snippet.FORCED_UUID,
                'attributes': storage['data'][0]
            }
        }
        result = testing.TestClient(server.server.api).simulate_post(
            path='/api/snippy/rest/snippets/53908d68425c61dc',
            headers={'accept': 'application/vnd.api+json', 'X-HTTP-Method-Override': 'PATCH'},
            body=json.dumps(request_body))
        assert result.status == falcon.HTTP_200
        assert result.headers == expect_headers
        Content.assert_restapi(result.json, expect_body)
        Content.assert_storage(storage)
