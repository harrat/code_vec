def test_read_file_given_file_like_object(self):
        """filereader: can read using a file-like (BytesIO) file...."""
        with open(ct_name, "rb") as f:
            file_like = BytesIO(f.read())
        ct = dcmread(file_like)
        # Tests here simply repeat some of testCT test
        got = ct.ImagePositionPatient
        DS = pydicom.valuerep.DS

        if have_numpy and config.use_DS_numpy:
            expected = numpy.array([-158.135803, -179.035797, -75.699997])
            assert numpy.allclose(got, expected)
        else:
            expected = [DS("-158.135803"), DS("-179.035797"), DS("-75.699997")]
            assert expected == got

        assert 128 * 128 * 2 == len(ct.PixelData)

        # Should also be able to close the file ourselves without
        # exception raised:
        file_like.close()
