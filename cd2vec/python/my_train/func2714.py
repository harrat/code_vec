@pytest.mark.vcr()
def test_workbench_vuln_info_age_typeerror(api):
    with pytest.raises(TypeError):
        api.workbenches.vuln_info(19506, age='none')
