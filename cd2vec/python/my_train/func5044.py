@patch("clinner.run.base.CLI")
    def test_explicit_commands(self, cli, main_cls):
        class BarMain(Main):
            commands = ("tests.run.conftest.foobar",)

        args = ["foobar"]
        main = BarMain(args)
        queue = Queue()
        main.run(q=queue)

        assert queue.get() == 42
        assert "foobar" in main._commands
        assert len(main._commands) == 1
